// Generated by data binding compiler. Do not edit!
package com.impaktsoft.globeup.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.viewmodels.InitUserViewModel;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class ActivityInitUserBinding extends ViewDataBinding {
  @NonNull
  public final TextView placeholderMessage;

  @NonNull
  public final ProgressBar progressBar;

  @Bindable
  protected InitUserViewModel mViewModel;

  protected ActivityInitUserBinding(Object _bindingComponent, View _root, int _localFieldCount,
      TextView placeholderMessage, ProgressBar progressBar) {
    super(_bindingComponent, _root, _localFieldCount);
    this.placeholderMessage = placeholderMessage;
    this.progressBar = progressBar;
  }

  public abstract void setViewModel(@Nullable InitUserViewModel viewModel);

  @Nullable
  public InitUserViewModel getViewModel() {
    return mViewModel;
  }

  @NonNull
  public static ActivityInitUserBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.activity_init_user, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static ActivityInitUserBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<ActivityInitUserBinding>inflateInternal(inflater, R.layout.activity_init_user, root, attachToRoot, component);
  }

  @NonNull
  public static ActivityInitUserBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.activity_init_user, null, false, component)
   */
  @NonNull
  @Deprecated
  public static ActivityInitUserBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<ActivityInitUserBinding>inflateInternal(inflater, R.layout.activity_init_user, null, false, component);
  }

  public static ActivityInitUserBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static ActivityInitUserBinding bind(@NonNull View view, @Nullable Object component) {
    return (ActivityInitUserBinding)bind(component, view, R.layout.activity_init_user);
  }
}
