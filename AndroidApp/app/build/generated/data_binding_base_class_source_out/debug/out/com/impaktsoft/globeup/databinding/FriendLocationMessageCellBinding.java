// Generated by data binding compiler. Do not edit!
package com.impaktsoft.globeup.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.models.MessagePOJO;
import com.impaktsoft.globeup.models.UserInfo;
import de.hdodenhof.circleimageview.CircleImageView;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FriendLocationMessageCellBinding extends ViewDataBinding {
  @NonNull
  public final CardView imageContentFriend;

  @NonNull
  public final RelativeLayout locationCellLayout;

  @NonNull
  public final ImageView locationImage;

  @NonNull
  public final RelativeLayout locationLayout;

  @NonNull
  public final CircleImageView userImage;

  @NonNull
  public final TextView userName;

  @Bindable
  protected UserInfo mUserInfo;

  @Bindable
  protected MessagePOJO mMessageItem;

  protected FriendLocationMessageCellBinding(Object _bindingComponent, View _root,
      int _localFieldCount, CardView imageContentFriend, RelativeLayout locationCellLayout,
      ImageView locationImage, RelativeLayout locationLayout, CircleImageView userImage,
      TextView userName) {
    super(_bindingComponent, _root, _localFieldCount);
    this.imageContentFriend = imageContentFriend;
    this.locationCellLayout = locationCellLayout;
    this.locationImage = locationImage;
    this.locationLayout = locationLayout;
    this.userImage = userImage;
    this.userName = userName;
  }

  public abstract void setUserInfo(@Nullable UserInfo userInfo);

  @Nullable
  public UserInfo getUserInfo() {
    return mUserInfo;
  }

  public abstract void setMessageItem(@Nullable MessagePOJO messageItem);

  @Nullable
  public MessagePOJO getMessageItem() {
    return mMessageItem;
  }

  @NonNull
  public static FriendLocationMessageCellBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.friend_location_message_cell, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FriendLocationMessageCellBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FriendLocationMessageCellBinding>inflateInternal(inflater, R.layout.friend_location_message_cell, root, attachToRoot, component);
  }

  @NonNull
  public static FriendLocationMessageCellBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.friend_location_message_cell, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FriendLocationMessageCellBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FriendLocationMessageCellBinding>inflateInternal(inflater, R.layout.friend_location_message_cell, null, false, component);
  }

  public static FriendLocationMessageCellBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FriendLocationMessageCellBinding bind(@NonNull View view,
      @Nullable Object component) {
    return (FriendLocationMessageCellBinding)bind(component, view, R.layout.friend_location_message_cell);
  }
}
