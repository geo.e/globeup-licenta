// Generated by data binding compiler. Do not edit!
package com.impaktsoft.globeup.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.viewmodels.SearchEventsByNameViewModel;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class ActivitySearchEventsBinding extends ViewDataBinding {
  @NonNull
  public final RecyclerView eventsList;

  @NonNull
  public final TextView placeholderMessage;

  @NonNull
  public final LinearLayout searchView;

  @NonNull
  public final RelativeLayout spinnerView;

  @Bindable
  protected SearchEventsByNameViewModel mViewModel;

  protected ActivitySearchEventsBinding(Object _bindingComponent, View _root, int _localFieldCount,
      RecyclerView eventsList, TextView placeholderMessage, LinearLayout searchView,
      RelativeLayout spinnerView) {
    super(_bindingComponent, _root, _localFieldCount);
    this.eventsList = eventsList;
    this.placeholderMessage = placeholderMessage;
    this.searchView = searchView;
    this.spinnerView = spinnerView;
  }

  public abstract void setViewModel(@Nullable SearchEventsByNameViewModel viewModel);

  @Nullable
  public SearchEventsByNameViewModel getViewModel() {
    return mViewModel;
  }

  @NonNull
  public static ActivitySearchEventsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.activity_search_events, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static ActivitySearchEventsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<ActivitySearchEventsBinding>inflateInternal(inflater, R.layout.activity_search_events, root, attachToRoot, component);
  }

  @NonNull
  public static ActivitySearchEventsBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.activity_search_events, null, false, component)
   */
  @NonNull
  @Deprecated
  public static ActivitySearchEventsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<ActivitySearchEventsBinding>inflateInternal(inflater, R.layout.activity_search_events, null, false, component);
  }

  public static ActivitySearchEventsBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static ActivitySearchEventsBinding bind(@NonNull View view, @Nullable Object component) {
    return (ActivitySearchEventsBinding)bind(component, view, R.layout.activity_search_events);
  }
}
