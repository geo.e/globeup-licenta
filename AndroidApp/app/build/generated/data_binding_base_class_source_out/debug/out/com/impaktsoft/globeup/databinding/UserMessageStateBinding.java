// Generated by data binding compiler. Do not edit!
package com.impaktsoft.globeup.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.models.LastMessageState;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class UserMessageStateBinding extends ViewDataBinding {
  @NonNull
  public final ImageView messageStateImage;

  @Bindable
  protected LastMessageState mItemMessage;

  protected UserMessageStateBinding(Object _bindingComponent, View _root, int _localFieldCount,
      ImageView messageStateImage) {
    super(_bindingComponent, _root, _localFieldCount);
    this.messageStateImage = messageStateImage;
  }

  public abstract void setItemMessage(@Nullable LastMessageState itemMessage);

  @Nullable
  public LastMessageState getItemMessage() {
    return mItemMessage;
  }

  @NonNull
  public static UserMessageStateBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.user_message_state, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static UserMessageStateBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<UserMessageStateBinding>inflateInternal(inflater, R.layout.user_message_state, root, attachToRoot, component);
  }

  @NonNull
  public static UserMessageStateBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.user_message_state, null, false, component)
   */
  @NonNull
  @Deprecated
  public static UserMessageStateBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<UserMessageStateBinding>inflateInternal(inflater, R.layout.user_message_state, null, false, component);
  }

  public static UserMessageStateBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static UserMessageStateBinding bind(@NonNull View view, @Nullable Object component) {
    return (UserMessageStateBinding)bind(component, view, R.layout.user_message_state);
  }
}
