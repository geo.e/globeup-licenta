// Generated by data binding compiler. Do not edit!
package com.impaktsoft.globeup.databinding;

import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.viewmodels.ImageCaptureViewModel;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentImageCaptureBinding extends ViewDataBinding {
  @NonNull
  public final ImageButton acceptButton;

  @NonNull
  public final ImageButton captureButton;

  @NonNull
  public final ImageView imagePreview;

  @NonNull
  public final RelativeLayout previewImageLayout;

  @NonNull
  public final ImageButton retryButton;

  @NonNull
  public final RelativeLayout takePictureLayout;

  @NonNull
  public final TextureView textureView;

  @Bindable
  protected ImageCaptureViewModel mViewModel;

  protected FragmentImageCaptureBinding(Object _bindingComponent, View _root, int _localFieldCount,
      ImageButton acceptButton, ImageButton captureButton, ImageView imagePreview,
      RelativeLayout previewImageLayout, ImageButton retryButton, RelativeLayout takePictureLayout,
      TextureView textureView) {
    super(_bindingComponent, _root, _localFieldCount);
    this.acceptButton = acceptButton;
    this.captureButton = captureButton;
    this.imagePreview = imagePreview;
    this.previewImageLayout = previewImageLayout;
    this.retryButton = retryButton;
    this.takePictureLayout = takePictureLayout;
    this.textureView = textureView;
  }

  public abstract void setViewModel(@Nullable ImageCaptureViewModel viewModel);

  @Nullable
  public ImageCaptureViewModel getViewModel() {
    return mViewModel;
  }

  @NonNull
  public static FragmentImageCaptureBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_image_capture, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentImageCaptureBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentImageCaptureBinding>inflateInternal(inflater, R.layout.fragment_image_capture, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentImageCaptureBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_image_capture, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentImageCaptureBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentImageCaptureBinding>inflateInternal(inflater, R.layout.fragment_image_capture, null, false, component);
  }

  public static FragmentImageCaptureBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentImageCaptureBinding bind(@NonNull View view, @Nullable Object component) {
    return (FragmentImageCaptureBinding)bind(component, view, R.layout.fragment_image_capture);
  }
}
