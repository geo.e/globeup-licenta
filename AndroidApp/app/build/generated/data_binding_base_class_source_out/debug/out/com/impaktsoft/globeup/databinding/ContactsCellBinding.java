// Generated by data binding compiler. Do not edit!
package com.impaktsoft.globeup.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.models.UserPOJO;
import de.hdodenhof.circleimageview.CircleImageView;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class ContactsCellBinding extends ViewDataBinding {
  @NonNull
  public final RelativeLayout contactLayout;

  @NonNull
  public final CircleImageView conversationImage;

  @NonNull
  public final RelativeLayout conversationLayoutImage;

  @Bindable
  protected UserPOJO mUser;

  protected ContactsCellBinding(Object _bindingComponent, View _root, int _localFieldCount,
      RelativeLayout contactLayout, CircleImageView conversationImage,
      RelativeLayout conversationLayoutImage) {
    super(_bindingComponent, _root, _localFieldCount);
    this.contactLayout = contactLayout;
    this.conversationImage = conversationImage;
    this.conversationLayoutImage = conversationLayoutImage;
  }

  public abstract void setUser(@Nullable UserPOJO user);

  @Nullable
  public UserPOJO getUser() {
    return mUser;
  }

  @NonNull
  public static ContactsCellBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.contacts_cell, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static ContactsCellBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<ContactsCellBinding>inflateInternal(inflater, R.layout.contacts_cell, root, attachToRoot, component);
  }

  @NonNull
  public static ContactsCellBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.contacts_cell, null, false, component)
   */
  @NonNull
  @Deprecated
  public static ContactsCellBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<ContactsCellBinding>inflateInternal(inflater, R.layout.contacts_cell, null, false, component);
  }

  public static ContactsCellBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static ContactsCellBinding bind(@NonNull View view, @Nullable Object component) {
    return (ContactsCellBinding)bind(component, view, R.layout.contacts_cell);
  }
}
