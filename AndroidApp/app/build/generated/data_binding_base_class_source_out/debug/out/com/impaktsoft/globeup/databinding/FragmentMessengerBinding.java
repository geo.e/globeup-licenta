// Generated by data binding compiler. Do not edit!
package com.impaktsoft.globeup.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.viewmodels.MessengerViewModel;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentMessengerBinding extends ViewDataBinding {
  @NonNull
  public final Button btnSend;

  @NonNull
  public final RelativeLayout chatLayout;

  @NonNull
  public final Button chatOptionsButton;

  @NonNull
  public final RecyclerView messagesList;

  @NonNull
  public final EditText textSend;

  @Bindable
  protected MessengerViewModel mViewModel;

  protected FragmentMessengerBinding(Object _bindingComponent, View _root, int _localFieldCount,
      Button btnSend, RelativeLayout chatLayout, Button chatOptionsButton,
      RecyclerView messagesList, EditText textSend) {
    super(_bindingComponent, _root, _localFieldCount);
    this.btnSend = btnSend;
    this.chatLayout = chatLayout;
    this.chatOptionsButton = chatOptionsButton;
    this.messagesList = messagesList;
    this.textSend = textSend;
  }

  public abstract void setViewModel(@Nullable MessengerViewModel viewModel);

  @Nullable
  public MessengerViewModel getViewModel() {
    return mViewModel;
  }

  @NonNull
  public static FragmentMessengerBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_messenger, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentMessengerBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentMessengerBinding>inflateInternal(inflater, R.layout.fragment_messenger, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentMessengerBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_messenger, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentMessengerBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentMessengerBinding>inflateInternal(inflater, R.layout.fragment_messenger, null, false, component);
  }

  public static FragmentMessengerBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentMessengerBinding bind(@NonNull View view, @Nullable Object component) {
    return (FragmentMessengerBinding)bind(component, view, R.layout.fragment_messenger);
  }
}
