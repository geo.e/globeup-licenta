// Generated by data binding compiler. Do not edit!
package com.impaktsoft.globeup.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.models.TransactionPOJO;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class TransactionCellBinding extends ViewDataBinding {
  @NonNull
  public final ImageView globalProfileImage;

  @Bindable
  protected TransactionPOJO mTransactionItem;

  protected TransactionCellBinding(Object _bindingComponent, View _root, int _localFieldCount,
      ImageView globalProfileImage) {
    super(_bindingComponent, _root, _localFieldCount);
    this.globalProfileImage = globalProfileImage;
  }

  public abstract void setTransactionItem(@Nullable TransactionPOJO transactionItem);

  @Nullable
  public TransactionPOJO getTransactionItem() {
    return mTransactionItem;
  }

  @NonNull
  public static TransactionCellBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.transaction_cell, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static TransactionCellBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<TransactionCellBinding>inflateInternal(inflater, R.layout.transaction_cell, root, attachToRoot, component);
  }

  @NonNull
  public static TransactionCellBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.transaction_cell, null, false, component)
   */
  @NonNull
  @Deprecated
  public static TransactionCellBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<TransactionCellBinding>inflateInternal(inflater, R.layout.transaction_cell, null, false, component);
  }

  public static TransactionCellBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static TransactionCellBinding bind(@NonNull View view, @Nullable Object component) {
    return (TransactionCellBinding)bind(component, view, R.layout.transaction_cell);
  }
}
