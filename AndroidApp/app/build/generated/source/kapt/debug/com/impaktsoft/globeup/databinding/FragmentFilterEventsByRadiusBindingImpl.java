package com.impaktsoft.globeup.databinding;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentFilterEventsByRadiusBindingImpl extends FragmentFilterEventsByRadiusBinding implements com.impaktsoft.globeup.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.filter_events_parent_layout, 6);
        sViewsWithIds.put(R.id.filter_events_by_radius_title, 7);
        sViewsWithIds.put(R.id.filter_events_by_radius_sub_title, 8);
        sViewsWithIds.put(R.id.mapLayout, 9);
        sViewsWithIds.put(R.id.filter_events_by_radius_map, 10);
    }
    // views
    @NonNull
    private final android.widget.ScrollView mboundView0;
    @NonNull
    private final com.impaktsoft.globeup.components.CustomSeekBarWithTextView mboundView4;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback39;
    @Nullable
    private final android.view.View.OnClickListener mCallback40;
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener mboundView4cbProgressAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.progress.getValue()
            //         is viewModel.progress.setValue((java.lang.Integer) callbackArg_0)
            java.lang.Integer callbackArg_0 = com.impaktsoft.globeup.bindingadapters.SeekBarBindingAdapters.getProgress(mboundView4);
            // localize variables for thread safety
            // viewModel.progress
            androidx.lifecycle.MutableLiveData<java.lang.Integer> viewModelProgress = null;
            // viewModel
            com.impaktsoft.globeup.viewmodels.FilterEventsByRadiusViewModel viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.progress != null
            boolean viewModelProgressJavaLangObjectNull = false;
            // viewModel.progress.getValue()
            java.lang.Integer viewModelProgressGetValue = null;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelProgress = viewModel.getProgress();

                viewModelProgressJavaLangObjectNull = (viewModelProgress) != (null);
                if (viewModelProgressJavaLangObjectNull) {




                    viewModelProgress.setValue(((java.lang.Integer) (callbackArg_0)));
                }
            }
        }
    };

    public FragmentFilterEventsByRadiusBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 11, sIncludes, sViewsWithIds));
    }
    private FragmentFilterEventsByRadiusBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 3
            , (androidx.cardview.widget.CardView) bindings[2]
            , (android.widget.ImageView) bindings[1]
            , (com.impaktsoft.globeup.components.MapViewWithRadius) bindings[10]
            , (android.widget.TextView) bindings[8]
            , (android.widget.TextView) bindings[7]
            , (android.widget.Button) bindings[5]
            , (android.widget.RelativeLayout) bindings[6]
            , (android.widget.LinearLayout) bindings[9]
            , (android.widget.ProgressBar) bindings[3]
            );
        this.filterEventsByRadiusCardViewMap.setTag(null);
        this.filterEventsByRadiusExit.setTag(null);
        this.filterEventsDoneButton.setTag(null);
        this.mapProgressBar.setTag(null);
        this.mboundView0 = (android.widget.ScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView4 = (com.impaktsoft.globeup.components.CustomSeekBarWithTextView) bindings[4];
        this.mboundView4.setTag(null);
        setRootTag(root);
        // listeners
        mCallback39 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 1);
        mCallback40 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 2);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x10L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((com.impaktsoft.globeup.viewmodels.FilterEventsByRadiusViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable com.impaktsoft.globeup.viewmodels.FilterEventsByRadiusViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x8L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelProgress((androidx.lifecycle.MutableLiveData<java.lang.Integer>) object, fieldId);
            case 1 :
                return onChangeViewModelSpinnerVisibility((androidx.lifecycle.MutableLiveData<java.lang.Integer>) object, fieldId);
            case 2 :
                return onChangeViewModelMapVisibility((androidx.lifecycle.MutableLiveData<java.lang.Integer>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelProgress(androidx.lifecycle.MutableLiveData<java.lang.Integer> ViewModelProgress, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelSpinnerVisibility(androidx.lifecycle.MutableLiveData<java.lang.Integer> ViewModelSpinnerVisibility, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelMapVisibility(androidx.lifecycle.MutableLiveData<java.lang.Integer> ViewModelMapVisibility, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        androidx.lifecycle.MutableLiveData<java.lang.Integer> viewModelProgress = null;
        int androidxDatabindingViewDataBindingSafeUnboxViewModelProgressGetValue = 0;
        int androidxDatabindingViewDataBindingSafeUnboxViewModelSpinnerVisibilityGetValue = 0;
        java.lang.Integer viewModelSpinnerVisibilityGetValue = null;
        java.lang.Integer viewModelProgressGetValue = null;
        com.impaktsoft.globeup.enums.ScaleTypeEnum viewModelScaleType = null;
        int androidxDatabindingViewDataBindingSafeUnboxViewModelMapVisibilityGetValue = 0;
        androidx.lifecycle.MutableLiveData<java.lang.Integer> viewModelSpinnerVisibility = null;
        java.lang.Integer viewModelMapVisibilityGetValue = null;
        com.impaktsoft.globeup.viewmodels.FilterEventsByRadiusViewModel viewModel = mViewModel;
        androidx.lifecycle.MutableLiveData<java.lang.Integer> viewModelMapVisibility = null;

        if ((dirtyFlags & 0x1fL) != 0) {


            if ((dirtyFlags & 0x19L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.progress
                        viewModelProgress = viewModel.getProgress();
                    }
                    updateLiveDataRegistration(0, viewModelProgress);


                    if (viewModelProgress != null) {
                        // read viewModel.progress.getValue()
                        viewModelProgressGetValue = viewModelProgress.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.progress.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelProgressGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelProgressGetValue);
            }
            if ((dirtyFlags & 0x18L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.scaleType
                        viewModelScaleType = viewModel.getScaleType();
                    }
            }
            if ((dirtyFlags & 0x1aL) != 0) {

                    if (viewModel != null) {
                        // read viewModel.spinnerVisibility
                        viewModelSpinnerVisibility = viewModel.getSpinnerVisibility();
                    }
                    updateLiveDataRegistration(1, viewModelSpinnerVisibility);


                    if (viewModelSpinnerVisibility != null) {
                        // read viewModel.spinnerVisibility.getValue()
                        viewModelSpinnerVisibilityGetValue = viewModelSpinnerVisibility.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.spinnerVisibility.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelSpinnerVisibilityGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelSpinnerVisibilityGetValue);
            }
            if ((dirtyFlags & 0x1cL) != 0) {

                    if (viewModel != null) {
                        // read viewModel.mapVisibility
                        viewModelMapVisibility = viewModel.getMapVisibility();
                    }
                    updateLiveDataRegistration(2, viewModelMapVisibility);


                    if (viewModelMapVisibility != null) {
                        // read viewModel.mapVisibility.getValue()
                        viewModelMapVisibilityGetValue = viewModelMapVisibility.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.mapVisibility.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelMapVisibilityGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelMapVisibilityGetValue);
            }
        }
        // batch finished
        if ((dirtyFlags & 0x1cL) != 0) {
            // api target 1

            this.filterEventsByRadiusCardViewMap.setVisibility(androidxDatabindingViewDataBindingSafeUnboxViewModelMapVisibilityGetValue);
        }
        if ((dirtyFlags & 0x10L) != 0) {
            // api target 1

            this.filterEventsByRadiusExit.setOnClickListener(mCallback39);
            this.filterEventsDoneButton.setOnClickListener(mCallback40);
            com.impaktsoft.globeup.bindingadapters.SeekBarBindingAdapters.setListener(this.mboundView4, mboundView4cbProgressAttrChanged);
        }
        if ((dirtyFlags & 0x1aL) != 0) {
            // api target 1

            this.mapProgressBar.setVisibility(androidxDatabindingViewDataBindingSafeUnboxViewModelSpinnerVisibilityGetValue);
        }
        if ((dirtyFlags & 0x18L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.SeekBarBindingAdapters.setScaleType(this.mboundView4, viewModelScaleType);
        }
        if ((dirtyFlags & 0x19L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.SeekBarBindingAdapters.setProgress(this.mboundView4, androidxDatabindingViewDataBindingSafeUnboxViewModelProgressGetValue);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 1: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.FilterEventsByRadiusViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.exitClick();
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.FilterEventsByRadiusViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.doneClick();
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.progress
        flag 1 (0x2L): viewModel.spinnerVisibility
        flag 2 (0x3L): viewModel.mapVisibility
        flag 3 (0x4L): viewModel
        flag 4 (0x5L): null
    flag mapping end*/
    //end
}