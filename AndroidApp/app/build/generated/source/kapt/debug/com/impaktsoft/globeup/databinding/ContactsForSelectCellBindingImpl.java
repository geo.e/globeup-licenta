package com.impaktsoft.globeup.databinding;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ContactsForSelectCellBindingImpl extends ContactsForSelectCellBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.conversation_layout_image, 4);
    }
    // views
    @NonNull
    private final android.widget.ImageView mboundView2;
    @NonNull
    private final android.widget.TextView mboundView3;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ContactsForSelectCellBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 5, sIncludes, sViewsWithIds));
    }
    private ContactsForSelectCellBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.RelativeLayout) bindings[0]
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[1]
            , (android.widget.RelativeLayout) bindings[4]
            );
        this.contactLayout.setTag(null);
        this.conversationImage.setTag(null);
        this.mboundView2 = (android.widget.ImageView) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (android.widget.TextView) bindings[3];
        this.mboundView3.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.contact == variableId) {
            setContact((com.impaktsoft.globeup.models.ContactPOJO) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setContact(@Nullable com.impaktsoft.globeup.models.ContactPOJO Contact) {
        this.mContact = Contact;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.contact);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        int bindingConvertIsSelectedContactSelectedViewVISIBLEViewGONE = 0;
        java.lang.String contactUserImgUrl = null;
        com.impaktsoft.globeup.models.ContactPOJO contact = mContact;
        com.impaktsoft.globeup.enums.ContactState contactSelected = null;
        boolean bindingConvertIsSelectedContactSelected = false;
        java.lang.String contactUserName = null;
        com.impaktsoft.globeup.models.UserPOJO contactUser = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (contact != null) {
                    // read contact.selected
                    contactSelected = contact.getSelected();
                    // read contact.user
                    contactUser = contact.getUser();
                }


                // read BindingConvert.isSelected(contact.selected)
                bindingConvertIsSelectedContactSelected = com.impaktsoft.globeup.bindingadapters.BindingConvert.isSelected(contactSelected);
            if((dirtyFlags & 0x3L) != 0) {
                if(bindingConvertIsSelectedContactSelected) {
                        dirtyFlags |= 0x8L;
                }
                else {
                        dirtyFlags |= 0x4L;
                }
            }
                if (contactUser != null) {
                    // read contact.user.imgUrl
                    contactUserImgUrl = contactUser.getImgUrl();
                    // read contact.user.name
                    contactUserName = contactUser.getName();
                }


                // read BindingConvert.isSelected(contact.selected) ? View.VISIBLE : View.GONE
                bindingConvertIsSelectedContactSelectedViewVISIBLEViewGONE = ((bindingConvertIsSelectedContactSelected) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.ImageViewBindingAdapters.imageUrl(this.conversationImage, contactUserImgUrl, androidx.appcompat.content.res.AppCompatResources.getDrawable(conversationImage.getContext(), R.drawable.profile_filled_svg));
            this.mboundView2.setVisibility(bindingConvertIsSelectedContactSelectedViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, contactUserName);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): contact
        flag 1 (0x2L): null
        flag 2 (0x3L): BindingConvert.isSelected(contact.selected) ? View.VISIBLE : View.GONE
        flag 3 (0x4L): BindingConvert.isSelected(contact.selected) ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}