package com.impaktsoft.globeup.databinding;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivitySearchEventsBindingImpl extends ActivitySearchEventsBinding implements com.impaktsoft.globeup.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.searchView, 6);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    @NonNull
    private final android.widget.ImageView mboundView1;
    @NonNull
    private final android.widget.EditText mboundView2;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback53;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivitySearchEventsBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 7, sIncludes, sViewsWithIds));
    }
    private ActivitySearchEventsBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 3
            , (androidx.recyclerview.widget.RecyclerView) bindings[3]
            , (android.widget.TextView) bindings[5]
            , (android.widget.LinearLayout) bindings[6]
            , (android.widget.RelativeLayout) bindings[4]
            );
        this.eventsList.setTag(null);
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.ImageView) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView2 = (android.widget.EditText) bindings[2];
        this.mboundView2.setTag(null);
        this.placeholderMessage.setTag(null);
        this.spinnerView.setTag(null);
        setRootTag(root);
        // listeners
        mCallback53 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x10L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((com.impaktsoft.globeup.viewmodels.SearchEventsByNameViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable com.impaktsoft.globeup.viewmodels.SearchEventsByNameViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x8L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelIsFetchingProcessRunning((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 1 :
                return onChangeViewModelLoadingPosition((androidx.lifecycle.MutableLiveData<java.lang.Integer>) object, fieldId);
            case 2 :
                return onChangeViewModelIsPlaceHolderMessageVisible((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelIsFetchingProcessRunning(androidx.lifecycle.MutableLiveData<java.lang.Boolean> ViewModelIsFetchingProcessRunning, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelLoadingPosition(androidx.lifecycle.MutableLiveData<java.lang.Integer> ViewModelLoadingPosition, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsPlaceHolderMessageVisible(androidx.lifecycle.MutableLiveData<java.lang.Boolean> ViewModelIsPlaceHolderMessageVisible, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        int viewModelIsPlaceHolderMessageVisibleViewVISIBLEViewGONE = 0;
        java.lang.Integer viewModelLoadingPositionGetValue = null;
        int androidxDatabindingViewDataBindingSafeUnboxViewModelLoadingPositionGetValue = 0;
        int viewModelIsFetchingProcessRunningViewVISIBLEViewGONE = 0;
        java.lang.Boolean viewModelIsPlaceHolderMessageVisibleGetValue = null;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> viewModelIsFetchingProcessRunning = null;
        androidx.lifecycle.MutableLiveData<java.lang.Integer> viewModelLoadingPosition = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxViewModelIsFetchingProcessRunningGetValue = false;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> viewModelIsPlaceHolderMessageVisible = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxViewModelIsPlaceHolderMessageVisibleGetValue = false;
        java.lang.Boolean viewModelIsFetchingProcessRunningGetValue = null;
        com.impaktsoft.globeup.viewmodels.SearchEventsByNameViewModel viewModel = mViewModel;

        if ((dirtyFlags & 0x1fL) != 0) {


            if ((dirtyFlags & 0x19L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isFetchingProcessRunning
                        viewModelIsFetchingProcessRunning = viewModel.isFetchingProcessRunning();
                    }
                    updateLiveDataRegistration(0, viewModelIsFetchingProcessRunning);


                    if (viewModelIsFetchingProcessRunning != null) {
                        // read viewModel.isFetchingProcessRunning.getValue()
                        viewModelIsFetchingProcessRunningGetValue = viewModelIsFetchingProcessRunning.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isFetchingProcessRunning.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelIsFetchingProcessRunningGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelIsFetchingProcessRunningGetValue);
                if((dirtyFlags & 0x19L) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxViewModelIsFetchingProcessRunningGetValue) {
                            dirtyFlags |= 0x100L;
                    }
                    else {
                            dirtyFlags |= 0x80L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isFetchingProcessRunning.getValue()) ? View.VISIBLE : View.GONE
                    viewModelIsFetchingProcessRunningViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxViewModelIsFetchingProcessRunningGetValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0x1aL) != 0) {

                    if (viewModel != null) {
                        // read viewModel.loadingPosition
                        viewModelLoadingPosition = viewModel.getLoadingPosition();
                    }
                    updateLiveDataRegistration(1, viewModelLoadingPosition);


                    if (viewModelLoadingPosition != null) {
                        // read viewModel.loadingPosition.getValue()
                        viewModelLoadingPositionGetValue = viewModelLoadingPosition.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.loadingPosition.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelLoadingPositionGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelLoadingPositionGetValue);
            }
            if ((dirtyFlags & 0x1cL) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isPlaceHolderMessageVisible
                        viewModelIsPlaceHolderMessageVisible = viewModel.isPlaceHolderMessageVisible();
                    }
                    updateLiveDataRegistration(2, viewModelIsPlaceHolderMessageVisible);


                    if (viewModelIsPlaceHolderMessageVisible != null) {
                        // read viewModel.isPlaceHolderMessageVisible.getValue()
                        viewModelIsPlaceHolderMessageVisibleGetValue = viewModelIsPlaceHolderMessageVisible.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isPlaceHolderMessageVisible.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelIsPlaceHolderMessageVisibleGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelIsPlaceHolderMessageVisibleGetValue);
                if((dirtyFlags & 0x1cL) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxViewModelIsPlaceHolderMessageVisibleGetValue) {
                            dirtyFlags |= 0x40L;
                    }
                    else {
                            dirtyFlags |= 0x20L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isPlaceHolderMessageVisible.getValue()) ? View.VISIBLE : View.GONE
                    viewModelIsPlaceHolderMessageVisibleViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxViewModelIsPlaceHolderMessageVisibleGetValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
        }
        // batch finished
        if ((dirtyFlags & 0x19L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.RecyclerViewBindingAdapter.clearPool(this.eventsList, androidxDatabindingViewDataBindingSafeUnboxViewModelIsFetchingProcessRunningGetValue);
            this.spinnerView.setVisibility(viewModelIsFetchingProcessRunningViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x18L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.RecyclerViewBindingAdapter.setScrollMethodInFilterByName(this.eventsList, viewModel);
            com.impaktsoft.globeup.bindingadapters.EditTextBindingAdapter.searchByName(this.mboundView2, viewModel);
        }
        if ((dirtyFlags & 0x1aL) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.RecyclerViewBindingAdapter.scrollToPosition(this.eventsList, androidxDatabindingViewDataBindingSafeUnboxViewModelLoadingPositionGetValue);
        }
        if ((dirtyFlags & 0x10L) != 0) {
            // api target 1

            this.mboundView1.setOnClickListener(mCallback53);
        }
        if ((dirtyFlags & 0x1cL) != 0) {
            // api target 1

            this.placeholderMessage.setVisibility(viewModelIsPlaceHolderMessageVisibleViewVISIBLEViewGONE);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // viewModel
        com.impaktsoft.globeup.viewmodels.SearchEventsByNameViewModel viewModel = mViewModel;
        // viewModel != null
        boolean viewModelJavaLangObjectNull = false;



        viewModelJavaLangObjectNull = (viewModel) != (null);
        if (viewModelJavaLangObjectNull) {


            viewModel.backClick();
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.isFetchingProcessRunning
        flag 1 (0x2L): viewModel.loadingPosition
        flag 2 (0x3L): viewModel.isPlaceHolderMessageVisible
        flag 3 (0x4L): viewModel
        flag 4 (0x5L): null
        flag 5 (0x6L): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isPlaceHolderMessageVisible.getValue()) ? View.VISIBLE : View.GONE
        flag 6 (0x7L): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isPlaceHolderMessageVisible.getValue()) ? View.VISIBLE : View.GONE
        flag 7 (0x8L): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isFetchingProcessRunning.getValue()) ? View.VISIBLE : View.GONE
        flag 8 (0x9L): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isFetchingProcessRunning.getValue()) ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}