package com.impaktsoft.globeup.databinding;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityInitUserBindingImpl extends ActivityInitUserBinding implements com.impaktsoft.globeup.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    @NonNull
    private final android.widget.Button mboundView2;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback3;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityInitUserBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 4, sIncludes, sViewsWithIds));
    }
    private ActivityInitUserBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 4
            , (android.widget.TextView) bindings[1]
            , (android.widget.ProgressBar) bindings[3]
            );
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView2 = (android.widget.Button) bindings[2];
        this.mboundView2.setTag(null);
        this.placeholderMessage.setTag(null);
        this.progressBar.setTag(null);
        setRootTag(root);
        // listeners
        mCallback3 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x20L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((com.impaktsoft.globeup.viewmodels.InitUserViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable com.impaktsoft.globeup.viewmodels.InitUserViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x10L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelIsRetryButtonVisible((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 1 :
                return onChangeViewModelIsPlaceholderVisible((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 2 :
                return onChangeViewModelIsProgressBarVisible((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 3 :
                return onChangeViewModelPlaceholderText((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelIsRetryButtonVisible(androidx.lifecycle.MutableLiveData<java.lang.Boolean> ViewModelIsRetryButtonVisible, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsPlaceholderVisible(androidx.lifecycle.MutableLiveData<java.lang.Boolean> ViewModelIsPlaceholderVisible, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsProgressBarVisible(androidx.lifecycle.MutableLiveData<java.lang.Boolean> ViewModelIsProgressBarVisible, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelPlaceholderText(androidx.lifecycle.MutableLiveData<java.lang.String> ViewModelPlaceholderText, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String viewModelPlaceholderTextGetValue = null;
        int viewModelIsProgressBarVisibleViewVISIBLEViewGONE = 0;
        boolean androidxDatabindingViewDataBindingSafeUnboxViewModelIsProgressBarVisibleGetValue = false;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> viewModelIsRetryButtonVisible = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxViewModelIsPlaceholderVisibleGetValue = false;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> viewModelIsPlaceholderVisible = null;
        int viewModelIsPlaceholderVisibleViewVISIBLEViewGONE = 0;
        int viewModelIsRetryButtonVisibleViewVISIBLEViewGONE = 0;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> viewModelIsProgressBarVisible = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> viewModelPlaceholderText = null;
        java.lang.Boolean viewModelIsPlaceholderVisibleGetValue = null;
        java.lang.Boolean viewModelIsRetryButtonVisibleGetValue = null;
        com.impaktsoft.globeup.viewmodels.InitUserViewModel viewModel = mViewModel;
        java.lang.Boolean viewModelIsProgressBarVisibleGetValue = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxViewModelIsRetryButtonVisibleGetValue = false;

        if ((dirtyFlags & 0x3fL) != 0) {


            if ((dirtyFlags & 0x31L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isRetryButtonVisible
                        viewModelIsRetryButtonVisible = viewModel.isRetryButtonVisible();
                    }
                    updateLiveDataRegistration(0, viewModelIsRetryButtonVisible);


                    if (viewModelIsRetryButtonVisible != null) {
                        // read viewModel.isRetryButtonVisible.getValue()
                        viewModelIsRetryButtonVisibleGetValue = viewModelIsRetryButtonVisible.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isRetryButtonVisible.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelIsRetryButtonVisibleGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelIsRetryButtonVisibleGetValue);
                if((dirtyFlags & 0x31L) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxViewModelIsRetryButtonVisibleGetValue) {
                            dirtyFlags |= 0x800L;
                    }
                    else {
                            dirtyFlags |= 0x400L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isRetryButtonVisible.getValue()) ? View.VISIBLE : View.GONE
                    viewModelIsRetryButtonVisibleViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxViewModelIsRetryButtonVisibleGetValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0x32L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isPlaceholderVisible
                        viewModelIsPlaceholderVisible = viewModel.isPlaceholderVisible();
                    }
                    updateLiveDataRegistration(1, viewModelIsPlaceholderVisible);


                    if (viewModelIsPlaceholderVisible != null) {
                        // read viewModel.isPlaceholderVisible.getValue()
                        viewModelIsPlaceholderVisibleGetValue = viewModelIsPlaceholderVisible.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isPlaceholderVisible.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelIsPlaceholderVisibleGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelIsPlaceholderVisibleGetValue);
                if((dirtyFlags & 0x32L) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxViewModelIsPlaceholderVisibleGetValue) {
                            dirtyFlags |= 0x200L;
                    }
                    else {
                            dirtyFlags |= 0x100L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isPlaceholderVisible.getValue()) ? View.VISIBLE : View.GONE
                    viewModelIsPlaceholderVisibleViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxViewModelIsPlaceholderVisibleGetValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0x34L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isProgressBarVisible
                        viewModelIsProgressBarVisible = viewModel.isProgressBarVisible();
                    }
                    updateLiveDataRegistration(2, viewModelIsProgressBarVisible);


                    if (viewModelIsProgressBarVisible != null) {
                        // read viewModel.isProgressBarVisible.getValue()
                        viewModelIsProgressBarVisibleGetValue = viewModelIsProgressBarVisible.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isProgressBarVisible.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelIsProgressBarVisibleGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelIsProgressBarVisibleGetValue);
                if((dirtyFlags & 0x34L) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxViewModelIsProgressBarVisibleGetValue) {
                            dirtyFlags |= 0x80L;
                    }
                    else {
                            dirtyFlags |= 0x40L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isProgressBarVisible.getValue()) ? View.VISIBLE : View.GONE
                    viewModelIsProgressBarVisibleViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxViewModelIsProgressBarVisibleGetValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0x38L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.placeholderText
                        viewModelPlaceholderText = viewModel.getPlaceholderText();
                    }
                    updateLiveDataRegistration(3, viewModelPlaceholderText);


                    if (viewModelPlaceholderText != null) {
                        // read viewModel.placeholderText.getValue()
                        viewModelPlaceholderTextGetValue = viewModelPlaceholderText.getValue();
                    }
            }
        }
        // batch finished
        if ((dirtyFlags & 0x20L) != 0) {
            // api target 1

            this.mboundView2.setOnClickListener(mCallback3);
        }
        if ((dirtyFlags & 0x31L) != 0) {
            // api target 1

            this.mboundView2.setVisibility(viewModelIsRetryButtonVisibleViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x38L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.placeholderMessage, viewModelPlaceholderTextGetValue);
        }
        if ((dirtyFlags & 0x32L) != 0) {
            // api target 1

            this.placeholderMessage.setVisibility(viewModelIsPlaceholderVisibleViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x34L) != 0) {
            // api target 1

            this.progressBar.setVisibility(viewModelIsProgressBarVisibleViewVISIBLEViewGONE);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // viewModel
        com.impaktsoft.globeup.viewmodels.InitUserViewModel viewModel = mViewModel;
        // viewModel != null
        boolean viewModelJavaLangObjectNull = false;



        viewModelJavaLangObjectNull = (viewModel) != (null);
        if (viewModelJavaLangObjectNull) {


            viewModel.retryConnection();
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.isRetryButtonVisible
        flag 1 (0x2L): viewModel.isPlaceholderVisible
        flag 2 (0x3L): viewModel.isProgressBarVisible
        flag 3 (0x4L): viewModel.placeholderText
        flag 4 (0x5L): viewModel
        flag 5 (0x6L): null
        flag 6 (0x7L): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isProgressBarVisible.getValue()) ? View.VISIBLE : View.GONE
        flag 7 (0x8L): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isProgressBarVisible.getValue()) ? View.VISIBLE : View.GONE
        flag 8 (0x9L): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isPlaceholderVisible.getValue()) ? View.VISIBLE : View.GONE
        flag 9 (0xaL): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isPlaceholderVisible.getValue()) ? View.VISIBLE : View.GONE
        flag 10 (0xbL): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isRetryButtonVisible.getValue()) ? View.VISIBLE : View.GONE
        flag 11 (0xcL): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isRetryButtonVisible.getValue()) ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}