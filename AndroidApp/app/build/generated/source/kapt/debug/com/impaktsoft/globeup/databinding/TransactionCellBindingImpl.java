package com.impaktsoft.globeup.databinding;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class TransactionCellBindingImpl extends TransactionCellBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final androidx.cardview.widget.CardView mboundView0;
    @NonNull
    private final android.widget.TextView mboundView2;
    @NonNull
    private final android.widget.TextView mboundView3;
    @NonNull
    private final android.widget.TextView mboundView4;
    @NonNull
    private final android.widget.TextView mboundView5;
    @NonNull
    private final android.widget.TextView mboundView6;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public TransactionCellBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 7, sIncludes, sViewsWithIds));
    }
    private TransactionCellBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.ImageView) bindings[1]
            );
        this.globalProfileImage.setTag(null);
        this.mboundView0 = (androidx.cardview.widget.CardView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView2 = (android.widget.TextView) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (android.widget.TextView) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView4 = (android.widget.TextView) bindings[4];
        this.mboundView4.setTag(null);
        this.mboundView5 = (android.widget.TextView) bindings[5];
        this.mboundView5.setTag(null);
        this.mboundView6 = (android.widget.TextView) bindings[6];
        this.mboundView6.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.transactionItem == variableId) {
            setTransactionItem((com.impaktsoft.globeup.models.TransactionPOJO) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setTransactionItem(@Nullable com.impaktsoft.globeup.models.TransactionPOJO TransactionItem) {
        this.mTransactionItem = TransactionItem;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.transactionItem);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String transactionItemTransactionDate = null;
        java.lang.Integer transactionItemCash = null;
        java.lang.String transactionItemCashToString = null;
        com.impaktsoft.globeup.models.TransactionPOJO transactionItem = mTransactionItem;
        java.lang.String transactionItemTransactionTypeToString = null;
        java.lang.String transactionItemGcoinsToString = null;
        java.lang.Integer transactionItemGcoins = null;
        com.impaktsoft.globeup.enums.TransactionType transactionItemTransactionType = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (transactionItem != null) {
                    // read transactionItem.transactionDate
                    transactionItemTransactionDate = transactionItem.getTransactionDate();
                    // read transactionItem.cash
                    transactionItemCash = transactionItem.getCash();
                    // read transactionItem.gcoins
                    transactionItemGcoins = transactionItem.getGcoins();
                    // read transactionItem.transactionType
                    transactionItemTransactionType = transactionItem.getTransactionType();
                }


                if (transactionItemCash != null) {
                    // read transactionItem.cash.toString()
                    transactionItemCashToString = transactionItemCash.toString();
                }
                if (transactionItemGcoins != null) {
                    // read transactionItem.gcoins.toString()
                    transactionItemGcoinsToString = transactionItemGcoins.toString();
                }
                if (transactionItemTransactionType != null) {
                    // read transactionItem.transactionType.toString()
                    transactionItemTransactionTypeToString = transactionItemTransactionType.toString();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.ImageViewBindingAdapters.setTransactionImage(this.globalProfileImage, transactionItemTransactionType);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, transactionItemTransactionTypeToString);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, transactionItemTransactionDate);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView4, transactionItemGcoinsToString);
            com.impaktsoft.globeup.bindingadapters.TextViewBindingAdapters.setTranscationTextColor(this.mboundView4, transactionItemTransactionType);
            com.impaktsoft.globeup.bindingadapters.TextViewBindingAdapters.setTranscationTextColor(this.mboundView5, transactionItemTransactionType);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView6, transactionItemCashToString);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): transactionItem
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}