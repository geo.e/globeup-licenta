package com.impaktsoft.globeup.databinding;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityEventBindingImpl extends ActivityEventBinding implements com.impaktsoft.globeup.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.eventAppBar, 19);
        sViewsWithIds.put(R.id.upcming_event_status_layout, 20);
        sViewsWithIds.put(R.id.event_cell_invite_image, 21);
        sViewsWithIds.put(R.id.event_cell_invite_text, 22);
        sViewsWithIds.put(R.id.event_map_text, 23);
        sViewsWithIds.put(R.id.event_map_location, 24);
    }
    // views
    @NonNull
    private final androidx.coordinatorlayout.widget.CoordinatorLayout mboundView0;
    @NonNull
    private final android.widget.TextView mboundView11;
    @NonNull
    private final android.widget.TextView mboundView12;
    @NonNull
    private final android.widget.RelativeLayout mboundView13;
    @NonNull
    private final android.widget.RelativeLayout mboundView14;
    @NonNull
    private final android.widget.ImageView mboundView15;
    @NonNull
    private final android.widget.TextView mboundView16;
    @NonNull
    private final android.widget.Button mboundView17;
    @NonNull
    private final android.widget.TextView mboundView3;
    @NonNull
    private final android.widget.TextView mboundView4;
    @NonNull
    private final android.widget.RelativeLayout mboundView5;
    @NonNull
    private final android.widget.RelativeLayout mboundView8;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback67;
    @Nullable
    private final android.view.View.OnClickListener mCallback62;
    @Nullable
    private final android.view.View.OnClickListener mCallback63;
    @Nullable
    private final android.view.View.OnClickListener mCallback65;
    @Nullable
    private final android.view.View.OnClickListener mCallback64;
    @Nullable
    private final android.view.View.OnClickListener mCallback66;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityEventBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 25, sIncludes, sViewsWithIds));
    }
    private ActivityEventBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 4
            , (android.widget.ImageView) bindings[2]
            , (com.google.android.material.appbar.AppBarLayout) bindings[19]
            , (android.widget.ImageView) bindings[6]
            , (android.widget.TextView) bindings[7]
            , (android.widget.ImageView) bindings[21]
            , (android.widget.TextView) bindings[22]
            , (android.widget.ImageView) bindings[9]
            , (android.widget.TextView) bindings[10]
            , (com.google.android.gms.maps.MapView) bindings[24]
            , (android.widget.TextView) bindings[23]
            , (android.widget.ImageView) bindings[1]
            , (android.widget.RelativeLayout) bindings[18]
            , (android.widget.LinearLayout) bindings[20]
            );
        this.backEvent.setTag(null);
        this.eventCellInterestedImage.setTag(null);
        this.eventCellInterestedText.setTag(null);
        this.eventCellJoinImage.setTag(null);
        this.eventCellJoinText.setTag(null);
        this.headerImage.setTag(null);
        this.loadingView.setTag(null);
        this.mboundView0 = (androidx.coordinatorlayout.widget.CoordinatorLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView11 = (android.widget.TextView) bindings[11];
        this.mboundView11.setTag(null);
        this.mboundView12 = (android.widget.TextView) bindings[12];
        this.mboundView12.setTag(null);
        this.mboundView13 = (android.widget.RelativeLayout) bindings[13];
        this.mboundView13.setTag(null);
        this.mboundView14 = (android.widget.RelativeLayout) bindings[14];
        this.mboundView14.setTag(null);
        this.mboundView15 = (android.widget.ImageView) bindings[15];
        this.mboundView15.setTag(null);
        this.mboundView16 = (android.widget.TextView) bindings[16];
        this.mboundView16.setTag(null);
        this.mboundView17 = (android.widget.Button) bindings[17];
        this.mboundView17.setTag(null);
        this.mboundView3 = (android.widget.TextView) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView4 = (android.widget.TextView) bindings[4];
        this.mboundView4.setTag(null);
        this.mboundView5 = (android.widget.RelativeLayout) bindings[5];
        this.mboundView5.setTag(null);
        this.mboundView8 = (android.widget.RelativeLayout) bindings[8];
        this.mboundView8.setTag(null);
        setRootTag(root);
        // listeners
        mCallback67 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 6);
        mCallback62 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 1);
        mCallback63 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 2);
        mCallback65 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 4);
        mCallback64 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 3);
        mCallback66 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 5);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x20L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((com.impaktsoft.globeup.viewmodels.EventDetailViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable com.impaktsoft.globeup.viewmodels.EventDetailViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x10L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelIsSpinnerVisible((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 1 :
                return onChangeViewModelCurrentUserEventState((androidx.lifecycle.MutableLiveData<com.impaktsoft.globeup.enums.EventState>) object, fieldId);
            case 2 :
                return onChangeViewModelCurrentEventMembers((androidx.lifecycle.MutableLiveData<java.lang.Integer>) object, fieldId);
            case 3 :
                return onChangeViewModelMapVisible((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelIsSpinnerVisible(androidx.lifecycle.MutableLiveData<java.lang.Boolean> ViewModelIsSpinnerVisible, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelCurrentUserEventState(androidx.lifecycle.MutableLiveData<com.impaktsoft.globeup.enums.EventState> ViewModelCurrentUserEventState, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelCurrentEventMembers(androidx.lifecycle.MutableLiveData<java.lang.Integer> ViewModelCurrentEventMembers, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelMapVisible(androidx.lifecycle.MutableLiveData<java.lang.Boolean> ViewModelMapVisible, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        boolean viewModelCurrentUserEventStateEventStateHOSTING = false;
        java.lang.Boolean viewModelIsSpinnerVisibleGetValue = null;
        double viewModelCurrentEventItemLocationLongitude = 0.0;
        java.lang.Integer viewModelCurrentEventMembersGetValue = null;
        java.lang.String viewModelCurrentEventItemName = null;
        com.impaktsoft.globeup.models.EventPOJO viewModelCurrentEventItem = null;
        java.lang.String viewModelCurrentEventItemDesciption = null;
        int androidxDatabindingViewDataBindingSafeUnboxViewModelCurrentEventItemImage = 0;
        boolean viewModelCurrentUserEventStateEventStateJOINEDBooleanTrueViewModelCurrentUserEventStateEventStateHOSTING = false;
        java.lang.Long viewModelCurrentEventItemEventDate = null;
        java.lang.Integer viewModelCurrentEventItemImage = null;
        java.lang.Boolean viewModelMapVisibleGetValue = null;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> viewModelIsSpinnerVisible = null;
        androidx.lifecycle.MutableLiveData<com.impaktsoft.globeup.enums.EventState> viewModelCurrentUserEventState = null;
        int viewModelIsSpinnerVisibleViewVISIBLEViewGONE = 0;
        androidx.lifecycle.MutableLiveData<java.lang.Integer> viewModelCurrentEventMembers = null;
        boolean bindingConvertIsHosterViewModelCurrentUserEventState = false;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> viewModelMapVisible = null;
        double viewModelCurrentEventItemLocationLatitude = 0.0;
        boolean androidxDatabindingViewDataBindingSafeUnboxViewModelIsSpinnerVisibleGetValue = false;
        int viewModelMapVisibleViewVISIBLEViewGONE = 0;
        long androidxDatabindingViewDataBindingSafeUnboxViewModelCurrentEventItemEventDate = 0;
        com.google.firebase.firestore.GeoPoint viewModelCurrentEventItemLocation = null;
        int bindingConvertIsHosterViewModelCurrentUserEventStateViewGONEViewVISIBLE = 0;
        int viewModelCurrentUserEventStateEventStateJOINEDBooleanTrueViewModelCurrentUserEventStateEventStateHOSTINGViewVISIBLEViewGONE = 0;
        boolean androidxDatabindingViewDataBindingSafeUnboxViewModelMapVisibleGetValue = false;
        int androidxDatabindingViewDataBindingSafeUnboxViewModelCurrentEventMembersGetValue = 0;
        boolean viewModelCurrentUserEventStateEventStateJOINED = false;
        com.impaktsoft.globeup.enums.EventState viewModelCurrentUserEventStateGetValue = null;
        com.impaktsoft.globeup.viewmodels.EventDetailViewModel viewModel = mViewModel;

        if ((dirtyFlags & 0x3fL) != 0) {


            if ((dirtyFlags & 0x30L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.currentEventItem
                        viewModelCurrentEventItem = viewModel.getCurrentEventItem();
                    }


                    if (viewModelCurrentEventItem != null) {
                        // read viewModel.currentEventItem.name
                        viewModelCurrentEventItemName = viewModelCurrentEventItem.getName();
                        // read viewModel.currentEventItem.desciption
                        viewModelCurrentEventItemDesciption = viewModelCurrentEventItem.getDesciption();
                        // read viewModel.currentEventItem.eventDate
                        viewModelCurrentEventItemEventDate = viewModelCurrentEventItem.getEventDate();
                        // read viewModel.currentEventItem.image
                        viewModelCurrentEventItemImage = viewModelCurrentEventItem.getImage();
                        // read viewModel.currentEventItem.location
                        viewModelCurrentEventItemLocation = viewModelCurrentEventItem.getLocation();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.currentEventItem.eventDate)
                    androidxDatabindingViewDataBindingSafeUnboxViewModelCurrentEventItemEventDate = androidx.databinding.ViewDataBinding.safeUnbox(viewModelCurrentEventItemEventDate);
                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.currentEventItem.image)
                    androidxDatabindingViewDataBindingSafeUnboxViewModelCurrentEventItemImage = androidx.databinding.ViewDataBinding.safeUnbox(viewModelCurrentEventItemImage);
                    if (viewModelCurrentEventItemLocation != null) {
                        // read viewModel.currentEventItem.location.longitude
                        viewModelCurrentEventItemLocationLongitude = viewModelCurrentEventItemLocation.getLongitude();
                        // read viewModel.currentEventItem.location.latitude
                        viewModelCurrentEventItemLocationLatitude = viewModelCurrentEventItemLocation.getLatitude();
                    }
            }
            if ((dirtyFlags & 0x31L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isSpinnerVisible
                        viewModelIsSpinnerVisible = viewModel.isSpinnerVisible();
                    }
                    updateLiveDataRegistration(0, viewModelIsSpinnerVisible);


                    if (viewModelIsSpinnerVisible != null) {
                        // read viewModel.isSpinnerVisible.getValue()
                        viewModelIsSpinnerVisibleGetValue = viewModelIsSpinnerVisible.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isSpinnerVisible.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelIsSpinnerVisibleGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelIsSpinnerVisibleGetValue);
                if((dirtyFlags & 0x31L) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxViewModelIsSpinnerVisibleGetValue) {
                            dirtyFlags |= 0x200L;
                    }
                    else {
                            dirtyFlags |= 0x100L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isSpinnerVisible.getValue()) ? View.VISIBLE : View.GONE
                    viewModelIsSpinnerVisibleViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxViewModelIsSpinnerVisibleGetValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0x32L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.currentUserEventState
                        viewModelCurrentUserEventState = viewModel.getCurrentUserEventState();
                    }
                    updateLiveDataRegistration(1, viewModelCurrentUserEventState);


                    if (viewModelCurrentUserEventState != null) {
                        // read viewModel.currentUserEventState.getValue()
                        viewModelCurrentUserEventStateGetValue = viewModelCurrentUserEventState.getValue();
                    }


                    // read BindingConvert.isHoster(viewModel.currentUserEventState.getValue())
                    bindingConvertIsHosterViewModelCurrentUserEventState = com.impaktsoft.globeup.bindingadapters.BindingConvert.isHoster(viewModelCurrentUserEventStateGetValue);
                    // read viewModel.currentUserEventState.getValue() == EventState.JOINED
                    viewModelCurrentUserEventStateEventStateJOINED = (viewModelCurrentUserEventStateGetValue) == (com.impaktsoft.globeup.enums.EventState.JOINED);
                if((dirtyFlags & 0x32L) != 0) {
                    if(bindingConvertIsHosterViewModelCurrentUserEventState) {
                            dirtyFlags |= 0x2000L;
                    }
                    else {
                            dirtyFlags |= 0x1000L;
                    }
                }
                if((dirtyFlags & 0x32L) != 0) {
                    if(viewModelCurrentUserEventStateEventStateJOINED) {
                            dirtyFlags |= 0x80L;
                    }
                    else {
                            dirtyFlags |= 0x40L;
                    }
                }


                    // read BindingConvert.isHoster(viewModel.currentUserEventState.getValue()) ? View.GONE : View.VISIBLE
                    bindingConvertIsHosterViewModelCurrentUserEventStateViewGONEViewVISIBLE = ((bindingConvertIsHosterViewModelCurrentUserEventState) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
            }
            if ((dirtyFlags & 0x34L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.currentEventMembers
                        viewModelCurrentEventMembers = viewModel.getCurrentEventMembers();
                    }
                    updateLiveDataRegistration(2, viewModelCurrentEventMembers);


                    if (viewModelCurrentEventMembers != null) {
                        // read viewModel.currentEventMembers.getValue()
                        viewModelCurrentEventMembersGetValue = viewModelCurrentEventMembers.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.currentEventMembers.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelCurrentEventMembersGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelCurrentEventMembersGetValue);
            }
            if ((dirtyFlags & 0x38L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.mapVisible
                        viewModelMapVisible = viewModel.getMapVisible();
                    }
                    updateLiveDataRegistration(3, viewModelMapVisible);


                    if (viewModelMapVisible != null) {
                        // read viewModel.mapVisible.getValue()
                        viewModelMapVisibleGetValue = viewModelMapVisible.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.mapVisible.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelMapVisibleGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelMapVisibleGetValue);
                if((dirtyFlags & 0x38L) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxViewModelMapVisibleGetValue) {
                            dirtyFlags |= 0x800L;
                    }
                    else {
                            dirtyFlags |= 0x400L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.mapVisible.getValue()) ? View.VISIBLE : View.GONE
                    viewModelMapVisibleViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxViewModelMapVisibleGetValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
        }
        // batch finished

        if ((dirtyFlags & 0x40L) != 0) {

                // read viewModel.currentUserEventState.getValue() == EventState.HOSTING
                viewModelCurrentUserEventStateEventStateHOSTING = (viewModelCurrentUserEventStateGetValue) == (com.impaktsoft.globeup.enums.EventState.HOSTING);
        }

        if ((dirtyFlags & 0x32L) != 0) {

                // read viewModel.currentUserEventState.getValue() == EventState.JOINED ? true : viewModel.currentUserEventState.getValue() == EventState.HOSTING
                viewModelCurrentUserEventStateEventStateJOINEDBooleanTrueViewModelCurrentUserEventStateEventStateHOSTING = ((viewModelCurrentUserEventStateEventStateJOINED) ? (true) : (viewModelCurrentUserEventStateEventStateHOSTING));
            if((dirtyFlags & 0x32L) != 0) {
                if(viewModelCurrentUserEventStateEventStateJOINEDBooleanTrueViewModelCurrentUserEventStateEventStateHOSTING) {
                        dirtyFlags |= 0x8000L;
                }
                else {
                        dirtyFlags |= 0x4000L;
                }
            }


                // read viewModel.currentUserEventState.getValue() == EventState.JOINED ? true : viewModel.currentUserEventState.getValue() == EventState.HOSTING ? View.VISIBLE : View.GONE
                viewModelCurrentUserEventStateEventStateJOINEDBooleanTrueViewModelCurrentUserEventStateEventStateHOSTINGViewVISIBLEViewGONE = ((viewModelCurrentUserEventStateEventStateJOINEDBooleanTrueViewModelCurrentUserEventStateEventStateHOSTING) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
        }
        // batch finished
        if ((dirtyFlags & 0x20L) != 0) {
            // api target 1

            this.backEvent.setOnClickListener(mCallback62);
            this.mboundView13.setOnClickListener(mCallback65);
            this.mboundView15.setOnClickListener(mCallback66);
            this.mboundView17.setOnClickListener(mCallback67);
            this.mboundView5.setOnClickListener(mCallback63);
            this.mboundView8.setOnClickListener(mCallback64);
        }
        if ((dirtyFlags & 0x32L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.ImageViewBindingAdapters.setEventImageTint(this.eventCellInterestedImage, viewModelCurrentUserEventStateGetValue, com.impaktsoft.globeup.enums.EventState.INTERESTED);
            com.impaktsoft.globeup.bindingadapters.TextViewBindingAdapters.setEventStateTextColor(this.eventCellInterestedText, viewModelCurrentUserEventStateGetValue, com.impaktsoft.globeup.enums.EventState.INTERESTED);
            com.impaktsoft.globeup.bindingadapters.ImageViewBindingAdapters.setEventImageTint(this.eventCellJoinImage, viewModelCurrentUserEventStateGetValue, com.impaktsoft.globeup.enums.EventState.JOINED);
            com.impaktsoft.globeup.bindingadapters.TextViewBindingAdapters.setEventStateTextColor(this.eventCellJoinText, viewModelCurrentUserEventStateGetValue, com.impaktsoft.globeup.enums.EventState.JOINED);
            this.mboundView17.setVisibility(viewModelCurrentUserEventStateEventStateJOINEDBooleanTrueViewModelCurrentUserEventStateEventStateHOSTINGViewVISIBLEViewGONE);
            this.mboundView5.setVisibility(bindingConvertIsHosterViewModelCurrentUserEventStateViewGONEViewVISIBLE);
            this.mboundView8.setVisibility(bindingConvertIsHosterViewModelCurrentUserEventStateViewGONEViewVISIBLE);
        }
        if ((dirtyFlags & 0x30L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.ImageViewBindingAdapters.setImage(this.headerImage, androidxDatabindingViewDataBindingSafeUnboxViewModelCurrentEventItemImage);
            com.impaktsoft.globeup.bindingadapters.TextViewBindingAdapters.setAddress(this.mboundView11, viewModelCurrentEventItemLocationLatitude, viewModelCurrentEventItemLocationLongitude);
            com.impaktsoft.globeup.bindingadapters.TextViewBindingAdapters.setEventDateText(this.mboundView12, androidxDatabindingViewDataBindingSafeUnboxViewModelCurrentEventItemEventDate);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView16, viewModelCurrentEventItemDesciption);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, viewModelCurrentEventItemName);
        }
        if ((dirtyFlags & 0x31L) != 0) {
            // api target 1

            this.loadingView.setVisibility(viewModelIsSpinnerVisibleViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x38L) != 0) {
            // api target 1

            this.mboundView14.setVisibility(viewModelMapVisibleViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x34L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.TextViewBindingAdapters.setUsersCount(this.mboundView4, androidxDatabindingViewDataBindingSafeUnboxViewModelCurrentEventMembersGetValue);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 6: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.EventDetailViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.openChat();
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.EventDetailViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.backImageClicked();
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // viewModel.currentEventItem
                com.impaktsoft.globeup.models.EventPOJO viewModelCurrentEventItem = null;
                // viewModel
                com.impaktsoft.globeup.viewmodels.EventDetailViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {




                    viewModelCurrentEventItem = viewModel.getCurrentEventItem();

                    viewModel.interestedForEvent(viewModelCurrentEventItem);
                }
                break;
            }
            case 4: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.EventDetailViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {



                    viewModel.setMapVisibility(true);
                }
                break;
            }
            case 3: {
                // localize variables for thread safety
                // viewModel.currentEventItem
                com.impaktsoft.globeup.models.EventPOJO viewModelCurrentEventItem = null;
                // viewModel
                com.impaktsoft.globeup.viewmodels.EventDetailViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {




                    viewModelCurrentEventItem = viewModel.getCurrentEventItem();

                    viewModel.joinEvent(viewModelCurrentEventItem);
                }
                break;
            }
            case 5: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.EventDetailViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {



                    viewModel.setMapVisibility(false);
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.isSpinnerVisible
        flag 1 (0x2L): viewModel.currentUserEventState
        flag 2 (0x3L): viewModel.currentEventMembers
        flag 3 (0x4L): viewModel.mapVisible
        flag 4 (0x5L): viewModel
        flag 5 (0x6L): null
        flag 6 (0x7L): viewModel.currentUserEventState.getValue() == EventState.JOINED ? true : viewModel.currentUserEventState.getValue() == EventState.HOSTING
        flag 7 (0x8L): viewModel.currentUserEventState.getValue() == EventState.JOINED ? true : viewModel.currentUserEventState.getValue() == EventState.HOSTING
        flag 8 (0x9L): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isSpinnerVisible.getValue()) ? View.VISIBLE : View.GONE
        flag 9 (0xaL): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isSpinnerVisible.getValue()) ? View.VISIBLE : View.GONE
        flag 10 (0xbL): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.mapVisible.getValue()) ? View.VISIBLE : View.GONE
        flag 11 (0xcL): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.mapVisible.getValue()) ? View.VISIBLE : View.GONE
        flag 12 (0xdL): BindingConvert.isHoster(viewModel.currentUserEventState.getValue()) ? View.GONE : View.VISIBLE
        flag 13 (0xeL): BindingConvert.isHoster(viewModel.currentUserEventState.getValue()) ? View.GONE : View.VISIBLE
        flag 14 (0xfL): viewModel.currentUserEventState.getValue() == EventState.JOINED ? true : viewModel.currentUserEventState.getValue() == EventState.HOSTING ? View.VISIBLE : View.GONE
        flag 15 (0x10L): viewModel.currentUserEventState.getValue() == EventState.JOINED ? true : viewModel.currentUserEventState.getValue() == EventState.HOSTING ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}