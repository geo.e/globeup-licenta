package com.impaktsoft.globeup.databinding;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentDashboardBindingImpl extends FragmentDashboardBinding implements com.impaktsoft.globeup.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.appBar, 9);
        sViewsWithIds.put(R.id.collapsingToolbarLayout, 10);
        sViewsWithIds.put(R.id.eventsList, 11);
        sViewsWithIds.put(R.id.progressBar, 12);
    }
    // views
    @NonNull
    private final androidx.coordinatorlayout.widget.CoordinatorLayout mboundView0;
    @NonNull
    private final android.widget.Button mboundView4;
    @NonNull
    private final android.widget.RelativeLayout mboundView6;
    @NonNull
    private final com.google.android.material.floatingactionbutton.FloatingActionButton mboundView8;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback29;
    @Nullable
    private final android.view.View.OnClickListener mCallback30;
    @Nullable
    private final android.view.View.OnClickListener mCallback31;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentDashboardBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 13, sIncludes, sViewsWithIds));
    }
    private FragmentDashboardBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 7
            , (com.google.android.material.appbar.AppBarLayout) bindings[9]
            , (com.google.android.material.appbar.CollapsingToolbarLayout) bindings[10]
            , (android.widget.TextView) bindings[3]
            , (androidx.recyclerview.widget.RecyclerView) bindings[11]
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[1]
            , (android.widget.ImageView) bindings[2]
            , (androidx.recyclerview.widget.RecyclerView) bindings[7]
            , (android.widget.TextView) bindings[5]
            , (android.widget.ProgressBar) bindings[12]
            );
        this.dashboardUserName.setTag(null);
        this.fragmentImage.setTag(null);
        this.mboundView0 = (androidx.coordinatorlayout.widget.CoordinatorLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView4 = (android.widget.Button) bindings[4];
        this.mboundView4.setTag(null);
        this.mboundView6 = (android.widget.RelativeLayout) bindings[6];
        this.mboundView6.setTag(null);
        this.mboundView8 = (com.google.android.material.floatingactionbutton.FloatingActionButton) bindings[8];
        this.mboundView8.setTag(null);
        this.medalImage.setTag(null);
        this.optinsList.setTag(null);
        this.placeholderMessage.setTag(null);
        setRootTag(root);
        // listeners
        mCallback29 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 1);
        mCallback30 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 2);
        mCallback31 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 3);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x100L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((com.impaktsoft.globeup.viewmodels.DashboardViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable com.impaktsoft.globeup.viewmodels.DashboardViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x80L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelPlaceholderText((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 1 :
                return onChangeViewModelUserImageUrl((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 2 :
                return onChangeViewModelIsPlaceHolderMessageVisible((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 3 :
                return onChangeViewModelIsShowedActionList((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 4 :
                return onChangeViewModelIsUserRegistered((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 5 :
                return onChangeViewModelUserName((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 6 :
                return onChangeViewModelIsProgressBarVisible((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelPlaceholderText(androidx.lifecycle.MutableLiveData<java.lang.String> ViewModelPlaceholderText, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelUserImageUrl(androidx.lifecycle.MutableLiveData<java.lang.String> ViewModelUserImageUrl, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsPlaceHolderMessageVisible(androidx.lifecycle.MutableLiveData<java.lang.Boolean> ViewModelIsPlaceHolderMessageVisible, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsShowedActionList(androidx.lifecycle.MutableLiveData<java.lang.Boolean> ViewModelIsShowedActionList, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsUserRegistered(androidx.lifecycle.MutableLiveData<java.lang.Boolean> ViewModelIsUserRegistered, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelUserName(androidx.lifecycle.MutableLiveData<java.lang.String> ViewModelUserName, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x20L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsProgressBarVisible(androidx.lifecycle.MutableLiveData<java.lang.Boolean> ViewModelIsProgressBarVisible, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x40L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.Boolean viewModelIsPlaceHolderMessageVisibleGetValue = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> viewModelPlaceholderText = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> viewModelUserImageUrl = null;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> viewModelIsPlaceHolderMessageVisible = null;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> viewModelIsShowedActionList = null;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> viewModelIsUserRegistered = null;
        java.lang.String viewModelUserImageUrlGetValue = null;
        int viewModelIsPlaceHolderMessageVisibleViewVISIBLEViewGONE = 0;
        int viewModelIsShowedActionListViewVISIBLEViewGONE = 0;
        java.lang.String viewModelPlaceholderTextGetValue = null;
        int viewModelIsUserRegisteredViewVISIBLEViewGONE = 0;
        int viewModelIsProgressBarVisibleViewVISIBLEViewGONE = 0;
        boolean androidxDatabindingViewDataBindingSafeUnboxViewModelIsProgressBarVisibleGetValue = false;
        androidx.lifecycle.MutableLiveData<java.lang.String> viewModelUserName = null;
        java.lang.String viewModelUserNameGetValue = null;
        java.lang.Boolean viewModelIsUserRegisteredGetValue = null;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> viewModelIsProgressBarVisible = null;
        java.lang.Boolean viewModelIsShowedActionListGetValue = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxViewModelIsPlaceHolderMessageVisibleGetValue = false;
        boolean androidxDatabindingViewDataBindingSafeUnboxViewModelIsUserRegisteredGetValue = false;
        com.impaktsoft.globeup.viewmodels.DashboardViewModel viewModel = mViewModel;
        java.lang.Boolean viewModelIsProgressBarVisibleGetValue = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxViewModelIsShowedActionListGetValue = false;

        if ((dirtyFlags & 0x1ffL) != 0) {


            if ((dirtyFlags & 0x181L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.placeholderText
                        viewModelPlaceholderText = viewModel.getPlaceholderText();
                    }
                    updateLiveDataRegistration(0, viewModelPlaceholderText);


                    if (viewModelPlaceholderText != null) {
                        // read viewModel.placeholderText.getValue()
                        viewModelPlaceholderTextGetValue = viewModelPlaceholderText.getValue();
                    }
            }
            if ((dirtyFlags & 0x182L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.userImageUrl
                        viewModelUserImageUrl = viewModel.getUserImageUrl();
                    }
                    updateLiveDataRegistration(1, viewModelUserImageUrl);


                    if (viewModelUserImageUrl != null) {
                        // read viewModel.userImageUrl.getValue()
                        viewModelUserImageUrlGetValue = viewModelUserImageUrl.getValue();
                    }
            }
            if ((dirtyFlags & 0x184L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isPlaceHolderMessageVisible
                        viewModelIsPlaceHolderMessageVisible = viewModel.isPlaceHolderMessageVisible();
                    }
                    updateLiveDataRegistration(2, viewModelIsPlaceHolderMessageVisible);


                    if (viewModelIsPlaceHolderMessageVisible != null) {
                        // read viewModel.isPlaceHolderMessageVisible.getValue()
                        viewModelIsPlaceHolderMessageVisibleGetValue = viewModelIsPlaceHolderMessageVisible.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isPlaceHolderMessageVisible.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelIsPlaceHolderMessageVisibleGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelIsPlaceHolderMessageVisibleGetValue);
                if((dirtyFlags & 0x184L) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxViewModelIsPlaceHolderMessageVisibleGetValue) {
                            dirtyFlags |= 0x400L;
                    }
                    else {
                            dirtyFlags |= 0x200L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isPlaceHolderMessageVisible.getValue()) ? View.VISIBLE : View.GONE
                    viewModelIsPlaceHolderMessageVisibleViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxViewModelIsPlaceHolderMessageVisibleGetValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0x188L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isShowedActionList
                        viewModelIsShowedActionList = viewModel.isShowedActionList();
                    }
                    updateLiveDataRegistration(3, viewModelIsShowedActionList);


                    if (viewModelIsShowedActionList != null) {
                        // read viewModel.isShowedActionList.getValue()
                        viewModelIsShowedActionListGetValue = viewModelIsShowedActionList.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isShowedActionList.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelIsShowedActionListGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelIsShowedActionListGetValue);
                if((dirtyFlags & 0x188L) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxViewModelIsShowedActionListGetValue) {
                            dirtyFlags |= 0x1000L;
                    }
                    else {
                            dirtyFlags |= 0x800L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isShowedActionList.getValue()) ? View.VISIBLE : View.GONE
                    viewModelIsShowedActionListViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxViewModelIsShowedActionListGetValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0x190L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isUserRegistered
                        viewModelIsUserRegistered = viewModel.isUserRegistered();
                    }
                    updateLiveDataRegistration(4, viewModelIsUserRegistered);


                    if (viewModelIsUserRegistered != null) {
                        // read viewModel.isUserRegistered.getValue()
                        viewModelIsUserRegisteredGetValue = viewModelIsUserRegistered.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isUserRegistered.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelIsUserRegisteredGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelIsUserRegisteredGetValue);
                if((dirtyFlags & 0x190L) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxViewModelIsUserRegisteredGetValue) {
                            dirtyFlags |= 0x4000L;
                    }
                    else {
                            dirtyFlags |= 0x2000L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isUserRegistered.getValue()) ? View.VISIBLE : View.GONE
                    viewModelIsUserRegisteredViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxViewModelIsUserRegisteredGetValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0x1a0L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.userName
                        viewModelUserName = viewModel.getUserName();
                    }
                    updateLiveDataRegistration(5, viewModelUserName);


                    if (viewModelUserName != null) {
                        // read viewModel.userName.getValue()
                        viewModelUserNameGetValue = viewModelUserName.getValue();
                    }
            }
            if ((dirtyFlags & 0x1c0L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isProgressBarVisible
                        viewModelIsProgressBarVisible = viewModel.isProgressBarVisible();
                    }
                    updateLiveDataRegistration(6, viewModelIsProgressBarVisible);


                    if (viewModelIsProgressBarVisible != null) {
                        // read viewModel.isProgressBarVisible.getValue()
                        viewModelIsProgressBarVisibleGetValue = viewModelIsProgressBarVisible.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isProgressBarVisible.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelIsProgressBarVisibleGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelIsProgressBarVisibleGetValue);
                if((dirtyFlags & 0x1c0L) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxViewModelIsProgressBarVisibleGetValue) {
                            dirtyFlags |= 0x10000L;
                    }
                    else {
                            dirtyFlags |= 0x8000L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isProgressBarVisible.getValue()) ? View.VISIBLE : View.GONE
                    viewModelIsProgressBarVisibleViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxViewModelIsProgressBarVisibleGetValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
        }
        // batch finished
        if ((dirtyFlags & 0x1a0L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.dashboardUserName, viewModelUserNameGetValue);
        }
        if ((dirtyFlags & 0x182L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.ImageViewBindingAdapters.imageUrl(this.fragmentImage, viewModelUserImageUrlGetValue, androidx.appcompat.content.res.AppCompatResources.getDrawable(fragmentImage.getContext(), R.drawable.profile_filled_svg));
        }
        if ((dirtyFlags & 0x100L) != 0) {
            // api target 1

            this.mboundView4.setOnClickListener(mCallback30);
            this.mboundView8.setOnClickListener(mCallback31);
            this.medalImage.setOnClickListener(mCallback29);
        }
        if ((dirtyFlags & 0x190L) != 0) {
            // api target 1

            this.mboundView4.setVisibility(viewModelIsUserRegisteredViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x1c0L) != 0) {
            // api target 1

            this.mboundView6.setVisibility(viewModelIsProgressBarVisibleViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x188L) != 0) {
            // api target 1

            this.optinsList.setVisibility(viewModelIsShowedActionListViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x181L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.placeholderMessage, viewModelPlaceholderTextGetValue);
        }
        if ((dirtyFlags & 0x184L) != 0) {
            // api target 1

            this.placeholderMessage.setVisibility(viewModelIsPlaceHolderMessageVisibleViewVISIBLEViewGONE);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 1: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.DashboardViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.levelClicked();
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.DashboardViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.loginClicked();
                }
                break;
            }
            case 3: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.DashboardViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.mainActionButtonClicked();
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.placeholderText
        flag 1 (0x2L): viewModel.userImageUrl
        flag 2 (0x3L): viewModel.isPlaceHolderMessageVisible
        flag 3 (0x4L): viewModel.isShowedActionList
        flag 4 (0x5L): viewModel.isUserRegistered
        flag 5 (0x6L): viewModel.userName
        flag 6 (0x7L): viewModel.isProgressBarVisible
        flag 7 (0x8L): viewModel
        flag 8 (0x9L): null
        flag 9 (0xaL): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isPlaceHolderMessageVisible.getValue()) ? View.VISIBLE : View.GONE
        flag 10 (0xbL): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isPlaceHolderMessageVisible.getValue()) ? View.VISIBLE : View.GONE
        flag 11 (0xcL): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isShowedActionList.getValue()) ? View.VISIBLE : View.GONE
        flag 12 (0xdL): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isShowedActionList.getValue()) ? View.VISIBLE : View.GONE
        flag 13 (0xeL): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isUserRegistered.getValue()) ? View.VISIBLE : View.GONE
        flag 14 (0xfL): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isUserRegistered.getValue()) ? View.VISIBLE : View.GONE
        flag 15 (0x10L): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isProgressBarVisible.getValue()) ? View.VISIBLE : View.GONE
        flag 16 (0x11L): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isProgressBarVisible.getValue()) ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}