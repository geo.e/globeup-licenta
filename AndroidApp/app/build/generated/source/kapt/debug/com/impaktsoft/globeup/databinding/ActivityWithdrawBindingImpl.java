package com.impaktsoft.globeup.databinding;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityWithdrawBindingImpl extends ActivityWithdrawBinding implements com.impaktsoft.globeup.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.withdraw_gcoin_layout, 6);
        sViewsWithIds.put(R.id.withdraw_medal_image, 7);
        sViewsWithIds.put(R.id.withdraw_text, 8);
        sViewsWithIds.put(R.id.withdraw_edit_text_layout, 9);
        sViewsWithIds.put(R.id.withdraw_input_layout, 10);
        sViewsWithIds.put(R.id.withdraw_choose_method_text, 11);
        sViewsWithIds.put(R.id.withdraw_method_list, 12);
        sViewsWithIds.put(R.id.price_layout, 13);
        sViewsWithIds.put(R.id.total_price_layout, 14);
        sViewsWithIds.put(R.id.total_price_text, 15);
    }
    // views
    @NonNull
    private final android.widget.ScrollView mboundView0;
    @NonNull
    private final android.widget.TextView mboundView1;
    @NonNull
    private final android.widget.TextView mboundView3;
    @NonNull
    private final android.widget.TextView mboundView4;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback2;
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener mboundView4androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.total_value_string.getValue()
            //         is viewModel.total_value_string.setValue((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView4);
            // localize variables for thread safety
            // viewModel.total_value_string
            androidx.lifecycle.MutableLiveData<java.lang.String> viewModelTotalValueString = null;
            // viewModel.total_value_string.getValue()
            java.lang.String viewModelTotalValueStringGetValue = null;
            // viewModel
            com.impaktsoft.globeup.viewmodels.WithdrawViewModel viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.total_value_string != null
            boolean viewModelTotalValueStringJavaLangObjectNull = false;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelTotalValueString = viewModel.getTotal_value_string();

                viewModelTotalValueStringJavaLangObjectNull = (viewModelTotalValueString) != (null);
                if (viewModelTotalValueStringJavaLangObjectNull) {




                    viewModelTotalValueString.setValue(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public ActivityWithdrawBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 16, sIncludes, sViewsWithIds));
    }
    private ActivityWithdrawBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (android.widget.RelativeLayout) bindings[13]
            , (android.widget.RelativeLayout) bindings[14]
            , (android.widget.TextView) bindings[15]
            , (android.widget.Button) bindings[5]
            , (android.widget.TextView) bindings[11]
            , (android.widget.EditText) bindings[2]
            , (android.widget.RelativeLayout) bindings[9]
            , (android.widget.LinearLayout) bindings[6]
            , (com.google.android.material.textfield.TextInputLayout) bindings[10]
            , (android.widget.ImageView) bindings[7]
            , (androidx.recyclerview.widget.RecyclerView) bindings[12]
            , (android.widget.TextView) bindings[8]
            );
        this.mboundView0 = (android.widget.ScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.TextView) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView3 = (android.widget.TextView) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView4 = (android.widget.TextView) bindings[4];
        this.mboundView4.setTag(null);
        this.withdrawButton.setTag(null);
        this.withdrawEditText.setTag(null);
        setRootTag(root);
        // listeners
        mCallback2 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((com.impaktsoft.globeup.viewmodels.WithdrawViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable com.impaktsoft.globeup.viewmodels.WithdrawViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelTotalValueString((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelTotalValueString(androidx.lifecycle.MutableLiveData<java.lang.String> ViewModelTotalValueString, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        androidx.lifecycle.MutableLiveData<java.lang.String> viewModelTotalValueString = null;
        java.lang.String viewModelTotalValueStringGetValue = null;
        java.lang.String viewModelGcoinValueString = null;
        com.impaktsoft.globeup.viewmodels.WithdrawViewModel viewModel = mViewModel;
        java.lang.String viewModelCurrentGcoins = null;

        if ((dirtyFlags & 0x7L) != 0) {



                if (viewModel != null) {
                    // read viewModel.total_value_string
                    viewModelTotalValueString = viewModel.getTotal_value_string();
                }
                updateLiveDataRegistration(0, viewModelTotalValueString);


                if (viewModelTotalValueString != null) {
                    // read viewModel.total_value_string.getValue()
                    viewModelTotalValueStringGetValue = viewModelTotalValueString.getValue();
                }
            if ((dirtyFlags & 0x6L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.gcoinValueString
                        viewModelGcoinValueString = viewModel.getGcoinValueString();
                        // read viewModel.currentGcoins
                        viewModelCurrentGcoins = viewModel.getCurrentGcoins();
                    }
            }
        }
        // batch finished
        if ((dirtyFlags & 0x6L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView1, viewModelCurrentGcoins);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, viewModelGcoinValueString);
            com.impaktsoft.globeup.bindingadapters.EditTextBindingAdapter.getWithdraw(this.withdrawEditText, viewModel);
        }
        if ((dirtyFlags & 0x7L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView4, viewModelTotalValueStringGetValue);
        }
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView4, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView4androidTextAttrChanged);
            this.withdrawButton.setOnClickListener(mCallback2);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // viewModel
        com.impaktsoft.globeup.viewmodels.WithdrawViewModel viewModel = mViewModel;
        // viewModel != null
        boolean viewModelJavaLangObjectNull = false;



        viewModelJavaLangObjectNull = (viewModel) != (null);
        if (viewModelJavaLangObjectNull) {


            viewModel.withdrawClick();
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.total_value_string
        flag 1 (0x2L): viewModel
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}