package com.impaktsoft.globeup.databinding;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class DashboardEventCellBindingImpl extends DashboardEventCellBinding implements com.impaktsoft.globeup.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.event_cell_click_layout, 17);
        sViewsWithIds.put(R.id.rl, 18);
        sViewsWithIds.put(R.id.event_cell_invite_image, 19);
        sViewsWithIds.put(R.id.event_cell_invite_text, 20);
    }
    // views
    @NonNull
    private final android.widget.LinearLayout mboundView11;
    @NonNull
    private final android.widget.LinearLayout mboundView14;
    @NonNull
    private final android.widget.LinearLayout mboundView15;
    @NonNull
    private final android.widget.TextView mboundView4;
    @NonNull
    private final android.widget.LinearLayout mboundView8;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback35;
    @Nullable
    private final android.view.View.OnClickListener mCallback36;
    @Nullable
    private final android.view.View.OnClickListener mCallback32;
    @Nullable
    private final android.view.View.OnClickListener mCallback33;
    @Nullable
    private final android.view.View.OnClickListener mCallback34;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public DashboardEventCellBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 21, sIncludes, sViewsWithIds));
    }
    private DashboardEventCellBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.RelativeLayout) bindings[6]
            , (android.widget.TextView) bindings[5]
            , (androidx.cardview.widget.CardView) bindings[0]
            , (android.widget.LinearLayout) bindings[17]
            , (android.widget.ImageView) bindings[1]
            , (android.widget.ImageView) bindings[9]
            , (android.widget.TextView) bindings[10]
            , (android.widget.ImageView) bindings[19]
            , (android.widget.TextView) bindings[20]
            , (android.widget.ImageView) bindings[12]
            , (android.widget.TextView) bindings[13]
            , (android.widget.RelativeLayout) bindings[16]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[3]
            , (android.widget.RelativeLayout) bindings[18]
            , (android.widget.LinearLayout) bindings[7]
            );
        this.actualEventStatusLayout.setTag(null);
        this.distanceFrom.setTag(null);
        this.eventCellCarview.setTag(null);
        this.eventCellImage.setTag(null);
        this.eventCellInterestedImage.setTag(null);
        this.eventCellInterestedText.setTag(null);
        this.eventCellJoinImage.setTag(null);
        this.eventCellJoinText.setTag(null);
        this.eventCellLoadingView.setTag(null);
        this.eventDate.setTag(null);
        this.eventName.setTag(null);
        this.mboundView11 = (android.widget.LinearLayout) bindings[11];
        this.mboundView11.setTag(null);
        this.mboundView14 = (android.widget.LinearLayout) bindings[14];
        this.mboundView14.setTag(null);
        this.mboundView15 = (android.widget.LinearLayout) bindings[15];
        this.mboundView15.setTag(null);
        this.mboundView4 = (android.widget.TextView) bindings[4];
        this.mboundView4.setTag(null);
        this.mboundView8 = (android.widget.LinearLayout) bindings[8];
        this.mboundView8.setTag(null);
        this.upcmingEventStatusLayout.setTag(null);
        setRootTag(root);
        // listeners
        mCallback35 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 4);
        mCallback36 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 5);
        mCallback32 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 1);
        mCallback33 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 2);
        mCallback34 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 3);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x8L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.item == variableId) {
            setItem((com.impaktsoft.globeup.models.EventPOJO) variable);
        }
        else if (BR.eventState == variableId) {
            setEventState((com.impaktsoft.globeup.enums.EventState) variable);
        }
        else if (BR.viewModel == variableId) {
            setViewModel((com.impaktsoft.globeup.viewmodels.DashboardViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setItem(@Nullable com.impaktsoft.globeup.models.EventPOJO Item) {
        this.mItem = Item;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.item);
        super.requestRebind();
    }
    public void setEventState(@Nullable com.impaktsoft.globeup.enums.EventState EventState) {
        this.mEventState = EventState;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.eventState);
        super.requestRebind();
    }
    public void setViewModel(@Nullable com.impaktsoft.globeup.viewmodels.DashboardViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        com.impaktsoft.globeup.models.EventPOJO item = mItem;
        java.lang.Long itemEventDate = null;
        java.lang.String itemUid = null;
        int androidxDatabindingViewDataBindingSafeUnboxItemImage = 0;
        com.impaktsoft.globeup.enums.ScaleTypeEnum viewModelGetScaleType = null;
        com.google.firebase.firestore.GeoPoint itemLocation = null;
        int bindingConvertIsHosterEventStateViewVISIBLEViewGONE = 0;
        int bindingConvertIsHosterEventStateViewGONEViewVISIBLE = 0;
        java.lang.Integer itemImage = null;
        java.lang.String itemDesciption = null;
        boolean bindingConvertIsHosterEventState = false;
        com.google.android.gms.maps.model.LatLng viewModelGetUserCurrentLocation = null;
        double itemLocationLatitude = 0.0;
        double itemLocationLongitude = 0.0;
        int viewModelIsEventModifyingItemUidViewVISIBLEViewGONE = 0;
        boolean androidxDatabindingViewDataBindingSafeUnboxViewModelIsEventModifyingItemUid = false;
        com.impaktsoft.globeup.enums.EventState eventState = mEventState;
        long androidxDatabindingViewDataBindingSafeUnboxItemEventDate = 0;
        java.lang.String itemName = null;
        java.lang.Boolean viewModelIsEventModifyingItemUid = null;
        com.impaktsoft.globeup.viewmodels.DashboardViewModel viewModel = mViewModel;

        if ((dirtyFlags & 0xdL) != 0) {


            if ((dirtyFlags & 0x9L) != 0) {

                    if (item != null) {
                        // read item.eventDate
                        itemEventDate = item.getEventDate();
                        // read item.image
                        itemImage = item.getImage();
                        // read item.desciption
                        itemDesciption = item.getDesciption();
                        // read item.name
                        itemName = item.getName();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(item.eventDate)
                    androidxDatabindingViewDataBindingSafeUnboxItemEventDate = androidx.databinding.ViewDataBinding.safeUnbox(itemEventDate);
                    // read androidx.databinding.ViewDataBinding.safeUnbox(item.image)
                    androidxDatabindingViewDataBindingSafeUnboxItemImage = androidx.databinding.ViewDataBinding.safeUnbox(itemImage);
            }

                if (item != null) {
                    // read item.uid
                    itemUid = item.getUid();
                    // read item.location
                    itemLocation = item.getLocation();
                }
                if (viewModel != null) {
                    // read viewModel.getScaleType()
                    viewModelGetScaleType = viewModel.getScaleType();
                    // read viewModel.getUserCurrentLocation()
                    viewModelGetUserCurrentLocation = viewModel.getUserCurrentLocation();
                }


                if (viewModel != null) {
                    // read viewModel.isEventModifying(item.uid)
                    viewModelIsEventModifyingItemUid = viewModel.isEventModifying(itemUid);
                }


                // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isEventModifying(item.uid))
                androidxDatabindingViewDataBindingSafeUnboxViewModelIsEventModifyingItemUid = androidx.databinding.ViewDataBinding.safeUnbox(viewModelIsEventModifyingItemUid);
            if((dirtyFlags & 0xdL) != 0) {
                if(androidxDatabindingViewDataBindingSafeUnboxViewModelIsEventModifyingItemUid) {
                        dirtyFlags |= 0x200L;
                }
                else {
                        dirtyFlags |= 0x100L;
                }
            }


                // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isEventModifying(item.uid)) ? View.VISIBLE : View.GONE
                viewModelIsEventModifyingItemUidViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxViewModelIsEventModifyingItemUid) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            if ((dirtyFlags & 0x9L) != 0) {

                    if (itemLocation != null) {
                        // read item.location.latitude
                        itemLocationLatitude = itemLocation.getLatitude();
                        // read item.location.longitude
                        itemLocationLongitude = itemLocation.getLongitude();
                    }
            }
        }
        if ((dirtyFlags & 0xaL) != 0) {



                // read BindingConvert.isHoster(eventState)
                bindingConvertIsHosterEventState = com.impaktsoft.globeup.bindingadapters.BindingConvert.isHoster(eventState);
            if((dirtyFlags & 0xaL) != 0) {
                if(bindingConvertIsHosterEventState) {
                        dirtyFlags |= 0x20L;
                        dirtyFlags |= 0x80L;
                }
                else {
                        dirtyFlags |= 0x10L;
                        dirtyFlags |= 0x40L;
                }
            }


                // read BindingConvert.isHoster(eventState) ? View.VISIBLE : View.GONE
                bindingConvertIsHosterEventStateViewVISIBLEViewGONE = ((bindingConvertIsHosterEventState) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                // read BindingConvert.isHoster(eventState) ? View.GONE : View.VISIBLE
                bindingConvertIsHosterEventStateViewGONEViewVISIBLE = ((bindingConvertIsHosterEventState) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
        }
        // batch finished
        if ((dirtyFlags & 0x8L) != 0) {
            // api target 1

            this.actualEventStatusLayout.setOnClickListener(mCallback32);
            this.mboundView11.setOnClickListener(mCallback34);
            this.mboundView14.setOnClickListener(mCallback35);
            this.mboundView15.setOnClickListener(mCallback36);
            this.mboundView8.setOnClickListener(mCallback33);
        }
        if ((dirtyFlags & 0xaL) != 0) {
            // api target 1

            this.actualEventStatusLayout.setVisibility(bindingConvertIsHosterEventStateViewVISIBLEViewGONE);
            com.impaktsoft.globeup.bindingadapters.ImageViewBindingAdapters.setEventImageTint(this.eventCellInterestedImage, eventState, com.impaktsoft.globeup.enums.EventState.INTERESTED);
            com.impaktsoft.globeup.bindingadapters.TextViewBindingAdapters.setEventStateTextColor(this.eventCellInterestedText, eventState, com.impaktsoft.globeup.enums.EventState.INTERESTED);
            com.impaktsoft.globeup.bindingadapters.ImageViewBindingAdapters.setEventImageTint(this.eventCellJoinImage, eventState, com.impaktsoft.globeup.enums.EventState.JOINED);
            com.impaktsoft.globeup.bindingadapters.TextViewBindingAdapters.setEventStateTextColor(this.eventCellJoinText, eventState, com.impaktsoft.globeup.enums.EventState.JOINED);
            this.upcmingEventStatusLayout.setVisibility(bindingConvertIsHosterEventStateViewGONEViewVISIBLE);
        }
        if ((dirtyFlags & 0xdL) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.TextViewBindingAdapters.setDistanceText(this.distanceFrom, itemLocation, viewModelGetUserCurrentLocation, viewModelGetScaleType);
            this.eventCellLoadingView.setVisibility(viewModelIsEventModifyingItemUidViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x9L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.ImageViewBindingAdapters.setImage(this.eventCellImage, androidxDatabindingViewDataBindingSafeUnboxItemImage);
            com.impaktsoft.globeup.bindingadapters.TextViewBindingAdapters.setEventDateText(this.eventDate, androidxDatabindingViewDataBindingSafeUnboxItemEventDate);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.eventName, itemName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView4, itemDesciption);
            com.impaktsoft.globeup.bindingadapters.TextViewBindingAdapters.setAddress(this.mboundView4, itemLocationLatitude, itemLocationLongitude);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 4: {
                // localize variables for thread safety
                // item
                com.impaktsoft.globeup.models.EventPOJO item = mItem;
                // viewModel
                com.impaktsoft.globeup.viewmodels.DashboardViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {



                    viewModel.inviteToEvent(item);
                }
                break;
            }
            case 5: {
                // localize variables for thread safety
                // item
                com.impaktsoft.globeup.models.EventPOJO item = mItem;
                // viewModel
                com.impaktsoft.globeup.viewmodels.DashboardViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {



                    viewModel.donateForEvent(item);
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // item
                com.impaktsoft.globeup.models.EventPOJO item = mItem;
                // viewModel
                com.impaktsoft.globeup.viewmodels.DashboardViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {



                    viewModel.editClick(item);
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // item
                com.impaktsoft.globeup.models.EventPOJO item = mItem;
                // viewModel
                com.impaktsoft.globeup.viewmodels.DashboardViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {



                    viewModel.interestedForEvent(item);
                }
                break;
            }
            case 3: {
                // localize variables for thread safety
                // item
                com.impaktsoft.globeup.models.EventPOJO item = mItem;
                // viewModel
                com.impaktsoft.globeup.viewmodels.DashboardViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {



                    viewModel.joinEvent(item);
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): item
        flag 1 (0x2L): eventState
        flag 2 (0x3L): viewModel
        flag 3 (0x4L): null
        flag 4 (0x5L): BindingConvert.isHoster(eventState) ? View.VISIBLE : View.GONE
        flag 5 (0x6L): BindingConvert.isHoster(eventState) ? View.VISIBLE : View.GONE
        flag 6 (0x7L): BindingConvert.isHoster(eventState) ? View.GONE : View.VISIBLE
        flag 7 (0x8L): BindingConvert.isHoster(eventState) ? View.GONE : View.VISIBLE
        flag 8 (0x9L): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isEventModifying(item.uid)) ? View.VISIBLE : View.GONE
        flag 9 (0xaL): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isEventModifying(item.uid)) ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}