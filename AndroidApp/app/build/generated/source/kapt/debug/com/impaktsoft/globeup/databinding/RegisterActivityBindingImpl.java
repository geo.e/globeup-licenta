package com.impaktsoft.globeup.databinding;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class RegisterActivityBindingImpl extends RegisterActivityBinding implements com.impaktsoft.globeup.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.register_fields_layout, 13);
        sViewsWithIds.put(R.id.already_registered_layout, 14);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    @NonNull
    private final android.widget.FrameLayout mboundView1;
    @NonNull
    private final android.widget.ImageView mboundView10;
    @NonNull
    private final android.widget.TextView mboundView12;
    @NonNull
    private final android.widget.ImageView mboundView8;
    @NonNull
    private final android.widget.ImageView mboundView9;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback27;
    @Nullable
    private final android.view.View.OnClickListener mCallback28;
    @Nullable
    private final android.view.View.OnClickListener mCallback26;
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener editConfirmPasswordEditTextandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.passwordRepeat.getValue()
            //         is viewModel.passwordRepeat.setValue((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(editConfirmPasswordEditText);
            // localize variables for thread safety
            // viewModel.passwordRepeat
            androidx.lifecycle.MutableLiveData<java.lang.String> viewModelPasswordRepeat = null;
            // viewModel.passwordRepeat.getValue()
            java.lang.String viewModelPasswordRepeatGetValue = null;
            // viewModel
            com.impaktsoft.globeup.viewmodels.RegisterViewModel viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.passwordRepeat != null
            boolean viewModelPasswordRepeatJavaLangObjectNull = false;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelPasswordRepeat = viewModel.getPasswordRepeat();

                viewModelPasswordRepeatJavaLangObjectNull = (viewModelPasswordRepeat) != (null);
                if (viewModelPasswordRepeatJavaLangObjectNull) {




                    viewModelPasswordRepeat.setValue(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener editEmailEditTextandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.email.getValue()
            //         is viewModel.email.setValue((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(editEmailEditText);
            // localize variables for thread safety
            // viewModel.email.getValue()
            java.lang.String viewModelEmailGetValue = null;
            // viewModel.email
            androidx.lifecycle.MutableLiveData<java.lang.String> viewModelEmail = null;
            // viewModel
            com.impaktsoft.globeup.viewmodels.RegisterViewModel viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.email != null
            boolean viewModelEmailJavaLangObjectNull = false;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelEmail = viewModel.getEmail();

                viewModelEmailJavaLangObjectNull = (viewModelEmail) != (null);
                if (viewModelEmailJavaLangObjectNull) {




                    viewModelEmail.setValue(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener editNameEditTextandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.name.getValue()
            //         is viewModel.name.setValue((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(editNameEditText);
            // localize variables for thread safety
            // viewModel.name != null
            boolean viewModelNameJavaLangObjectNull = false;
            // viewModel.name
            androidx.lifecycle.MutableLiveData<java.lang.String> viewModelName = null;
            // viewModel
            com.impaktsoft.globeup.viewmodels.RegisterViewModel viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.name.getValue()
            java.lang.String viewModelNameGetValue = null;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelName = viewModel.getName();

                viewModelNameJavaLangObjectNull = (viewModelName) != (null);
                if (viewModelNameJavaLangObjectNull) {




                    viewModelName.setValue(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener editPasswordEditTextandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.password.getValue()
            //         is viewModel.password.setValue((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(editPasswordEditText);
            // localize variables for thread safety
            // viewModel.password != null
            boolean viewModelPasswordJavaLangObjectNull = false;
            // viewModel.password.getValue()
            java.lang.String viewModelPasswordGetValue = null;
            // viewModel
            com.impaktsoft.globeup.viewmodels.RegisterViewModel viewModel = mViewModel;
            // viewModel.password
            androidx.lifecycle.MutableLiveData<java.lang.String> viewModelPassword = null;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelPassword = viewModel.getPassword();

                viewModelPasswordJavaLangObjectNull = (viewModelPassword) != (null);
                if (viewModelPasswordJavaLangObjectNull) {




                    viewModelPassword.setValue(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public RegisterActivityBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 15, sIncludes, sViewsWithIds));
    }
    private RegisterActivityBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 9
            , (android.widget.LinearLayout) bindings[14]
            , (android.widget.EditText) bindings[7]
            , (android.widget.EditText) bindings[5]
            , (android.widget.EditText) bindings[4]
            , (android.widget.EditText) bindings[6]
            , (android.widget.Button) bindings[11]
            , (android.widget.LinearLayout) bindings[13]
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[3]
            , (android.widget.RelativeLayout) bindings[2]
            );
        this.editConfirmPasswordEditText.setTag(null);
        this.editEmailEditText.setTag(null);
        this.editNameEditText.setTag(null);
        this.editPasswordEditText.setTag(null);
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.FrameLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView10 = (android.widget.ImageView) bindings[10];
        this.mboundView10.setTag(null);
        this.mboundView12 = (android.widget.TextView) bindings[12];
        this.mboundView12.setTag(null);
        this.mboundView8 = (android.widget.ImageView) bindings[8];
        this.mboundView8.setTag(null);
        this.mboundView9 = (android.widget.ImageView) bindings[9];
        this.mboundView9.setTag(null);
        this.registerButton.setTag(null);
        this.registerProfileImage.setTag(null);
        this.registerProfileImageLayout.setTag(null);
        setRootTag(root);
        // listeners
        mCallback27 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 2);
        mCallback28 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 3);
        mCallback26 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x400L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((com.impaktsoft.globeup.viewmodels.RegisterViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable com.impaktsoft.globeup.viewmodels.RegisterViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x200L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelCharactersField((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 1 :
                return onChangeViewModelAlphaNumeric((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 2 :
                return onChangeViewModelPasswordRepeat((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 3 :
                return onChangeViewModelEmail((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 4 :
                return onChangeViewModelPassword((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 5 :
                return onChangeViewModelIsHudVisible((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 6 :
                return onChangeViewModelProfileImageFilePath((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 7 :
                return onChangeViewModelName((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 8 :
                return onChangeViewModelRepeatedCorrect((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelCharactersField(androidx.lifecycle.MutableLiveData<java.lang.Boolean> ViewModelCharactersField, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelAlphaNumeric(androidx.lifecycle.MutableLiveData<java.lang.Boolean> ViewModelAlphaNumeric, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelPasswordRepeat(androidx.lifecycle.MutableLiveData<java.lang.String> ViewModelPasswordRepeat, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelEmail(androidx.lifecycle.MutableLiveData<java.lang.String> ViewModelEmail, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelPassword(androidx.lifecycle.MutableLiveData<java.lang.String> ViewModelPassword, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsHudVisible(androidx.lifecycle.MutableLiveData<java.lang.Boolean> ViewModelIsHudVisible, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x20L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelProfileImageFilePath(androidx.lifecycle.MutableLiveData<java.lang.String> ViewModelProfileImageFilePath, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x40L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelName(androidx.lifecycle.MutableLiveData<java.lang.String> ViewModelName, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x80L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelRepeatedCorrect(androidx.lifecycle.MutableLiveData<java.lang.Boolean> ViewModelRepeatedCorrect, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x100L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String viewModelEmailGetValue = null;
        java.lang.Boolean viewModelAlphaNumericGetValue = null;
        java.lang.String viewModelPasswordGetValue = null;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> viewModelCharactersField = null;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> viewModelAlphaNumeric = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> viewModelPasswordRepeat = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> viewModelEmail = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxViewModelRepeatedCorrectGetValue = false;
        androidx.lifecycle.MutableLiveData<java.lang.String> viewModelPassword = null;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> viewModelIsHudVisible = null;
        java.lang.String viewModelNameGetValue = null;
        java.lang.String viewModelProfileImageFilePathGetValue = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> viewModelProfileImageFilePath = null;
        int viewModelIsHudVisibleViewVISIBLEViewGONE = 0;
        java.lang.String viewModelPasswordRepeatGetValue = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxViewModelIsHudVisibleGetValue = false;
        java.lang.Boolean viewModelIsHudVisibleGetValue = null;
        java.lang.Boolean viewModelCharactersFieldGetValue = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxViewModelAlphaNumericGetValue = false;
        boolean androidxDatabindingViewDataBindingSafeUnboxViewModelCharactersFieldGetValue = false;
        androidx.lifecycle.MutableLiveData<java.lang.String> viewModelName = null;
        java.lang.Boolean viewModelRepeatedCorrectGetValue = null;
        com.impaktsoft.globeup.viewmodels.RegisterViewModel viewModel = mViewModel;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> viewModelRepeatedCorrect = null;

        if ((dirtyFlags & 0x7ffL) != 0) {


            if ((dirtyFlags & 0x601L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.charactersField
                        viewModelCharactersField = viewModel.getCharactersField();
                    }
                    updateLiveDataRegistration(0, viewModelCharactersField);


                    if (viewModelCharactersField != null) {
                        // read viewModel.charactersField.getValue()
                        viewModelCharactersFieldGetValue = viewModelCharactersField.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.charactersField.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelCharactersFieldGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelCharactersFieldGetValue);
            }
            if ((dirtyFlags & 0x602L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.alphaNumeric
                        viewModelAlphaNumeric = viewModel.getAlphaNumeric();
                    }
                    updateLiveDataRegistration(1, viewModelAlphaNumeric);


                    if (viewModelAlphaNumeric != null) {
                        // read viewModel.alphaNumeric.getValue()
                        viewModelAlphaNumericGetValue = viewModelAlphaNumeric.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.alphaNumeric.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelAlphaNumericGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelAlphaNumericGetValue);
            }
            if ((dirtyFlags & 0x604L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.passwordRepeat
                        viewModelPasswordRepeat = viewModel.getPasswordRepeat();
                    }
                    updateLiveDataRegistration(2, viewModelPasswordRepeat);


                    if (viewModelPasswordRepeat != null) {
                        // read viewModel.passwordRepeat.getValue()
                        viewModelPasswordRepeatGetValue = viewModelPasswordRepeat.getValue();
                    }
            }
            if ((dirtyFlags & 0x608L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.email
                        viewModelEmail = viewModel.getEmail();
                    }
                    updateLiveDataRegistration(3, viewModelEmail);


                    if (viewModelEmail != null) {
                        // read viewModel.email.getValue()
                        viewModelEmailGetValue = viewModelEmail.getValue();
                    }
            }
            if ((dirtyFlags & 0x610L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.password
                        viewModelPassword = viewModel.getPassword();
                    }
                    updateLiveDataRegistration(4, viewModelPassword);


                    if (viewModelPassword != null) {
                        // read viewModel.password.getValue()
                        viewModelPasswordGetValue = viewModelPassword.getValue();
                    }
            }
            if ((dirtyFlags & 0x620L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isHudVisible
                        viewModelIsHudVisible = viewModel.isHudVisible();
                    }
                    updateLiveDataRegistration(5, viewModelIsHudVisible);


                    if (viewModelIsHudVisible != null) {
                        // read viewModel.isHudVisible.getValue()
                        viewModelIsHudVisibleGetValue = viewModelIsHudVisible.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isHudVisible.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelIsHudVisibleGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelIsHudVisibleGetValue);
                if((dirtyFlags & 0x620L) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxViewModelIsHudVisibleGetValue) {
                            dirtyFlags |= 0x1000L;
                    }
                    else {
                            dirtyFlags |= 0x800L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isHudVisible.getValue()) ? View.VISIBLE : View.GONE
                    viewModelIsHudVisibleViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxViewModelIsHudVisibleGetValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0x640L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.profileImageFilePath
                        viewModelProfileImageFilePath = viewModel.getProfileImageFilePath();
                    }
                    updateLiveDataRegistration(6, viewModelProfileImageFilePath);


                    if (viewModelProfileImageFilePath != null) {
                        // read viewModel.profileImageFilePath.getValue()
                        viewModelProfileImageFilePathGetValue = viewModelProfileImageFilePath.getValue();
                    }
            }
            if ((dirtyFlags & 0x680L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.name
                        viewModelName = viewModel.getName();
                    }
                    updateLiveDataRegistration(7, viewModelName);


                    if (viewModelName != null) {
                        // read viewModel.name.getValue()
                        viewModelNameGetValue = viewModelName.getValue();
                    }
            }
            if ((dirtyFlags & 0x700L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.repeatedCorrect
                        viewModelRepeatedCorrect = viewModel.getRepeatedCorrect();
                    }
                    updateLiveDataRegistration(8, viewModelRepeatedCorrect);


                    if (viewModelRepeatedCorrect != null) {
                        // read viewModel.repeatedCorrect.getValue()
                        viewModelRepeatedCorrectGetValue = viewModelRepeatedCorrect.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.repeatedCorrect.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelRepeatedCorrectGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelRepeatedCorrectGetValue);
            }
        }
        // batch finished
        if ((dirtyFlags & 0x604L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.editConfirmPasswordEditText, viewModelPasswordRepeatGetValue);
        }
        if ((dirtyFlags & 0x400L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.editConfirmPasswordEditText, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, editConfirmPasswordEditTextandroidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.editEmailEditText, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, editEmailEditTextandroidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.editNameEditText, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, editNameEditTextandroidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.editPasswordEditText, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, editPasswordEditTextandroidTextAttrChanged);
            this.mboundView12.setOnClickListener(mCallback28);
            this.registerButton.setOnClickListener(mCallback27);
            this.registerProfileImageLayout.setOnClickListener(mCallback26);
        }
        if ((dirtyFlags & 0x608L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.editEmailEditText, viewModelEmailGetValue);
        }
        if ((dirtyFlags & 0x680L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.editNameEditText, viewModelNameGetValue);
        }
        if ((dirtyFlags & 0x610L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.editPasswordEditText, viewModelPasswordGetValue);
        }
        if ((dirtyFlags & 0x620L) != 0) {
            // api target 1

            this.mboundView1.setVisibility(viewModelIsHudVisibleViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x700L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.ImageViewBindingAdapters.setFieldImageTint(this.mboundView10, androidxDatabindingViewDataBindingSafeUnboxViewModelRepeatedCorrectGetValue);
        }
        if ((dirtyFlags & 0x601L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.ImageViewBindingAdapters.setFieldImageTint(this.mboundView8, androidxDatabindingViewDataBindingSafeUnboxViewModelCharactersFieldGetValue);
        }
        if ((dirtyFlags & 0x602L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.ImageViewBindingAdapters.setFieldImageTint(this.mboundView9, androidxDatabindingViewDataBindingSafeUnboxViewModelAlphaNumericGetValue);
        }
        if ((dirtyFlags & 0x640L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.ImageViewBindingAdapters.file(this.registerProfileImage, viewModelProfileImageFilePathGetValue);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 2: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.RegisterViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.registerClicked();
                }
                break;
            }
            case 3: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.RegisterViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.loginClicked();
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.RegisterViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.takePicture();
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.charactersField
        flag 1 (0x2L): viewModel.alphaNumeric
        flag 2 (0x3L): viewModel.passwordRepeat
        flag 3 (0x4L): viewModel.email
        flag 4 (0x5L): viewModel.password
        flag 5 (0x6L): viewModel.isHudVisible
        flag 6 (0x7L): viewModel.profileImageFilePath
        flag 7 (0x8L): viewModel.name
        flag 8 (0x9L): viewModel.repeatedCorrect
        flag 9 (0xaL): viewModel
        flag 10 (0xbL): null
        flag 11 (0xcL): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isHudVisible.getValue()) ? View.VISIBLE : View.GONE
        flag 12 (0xdL): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isHudVisible.getValue()) ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}