package com.impaktsoft.globeup.databinding;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentFeedbackBindingImpl extends FragmentFeedbackBinding implements com.impaktsoft.globeup.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.title_feedback_alert, 6);
        sViewsWithIds.put(R.id.subtitle_feedback_alert, 7);
        sViewsWithIds.put(R.id.tags_title_feedback, 8);
        sViewsWithIds.put(R.id.feedback_subjects_list, 9);
        sViewsWithIds.put(R.id.improve_question_feedback, 10);
        sViewsWithIds.put(R.id.progressBar, 11);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    @NonNull
    private final android.widget.RelativeLayout mboundView1;
    @NonNull
    private final android.widget.Button mboundView4;
    @NonNull
    private final android.widget.RelativeLayout mboundView5;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback37;
    @Nullable
    private final android.view.View.OnClickListener mCallback38;
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener notesEditTextFeedbackandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.feedbackDescription.getValue()
            //         is viewModel.feedbackDescription.setValue((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(notesEditTextFeedback);
            // localize variables for thread safety
            // viewModel.feedbackDescription.getValue()
            java.lang.String viewModelFeedbackDescriptionGetValue = null;
            // viewModel.feedbackDescription != null
            boolean viewModelFeedbackDescriptionJavaLangObjectNull = false;
            // viewModel
            com.impaktsoft.globeup.viewmodels.FeedbackViewModel viewModel = mViewModel;
            // viewModel.feedbackDescription
            androidx.lifecycle.MutableLiveData<java.lang.String> viewModelFeedbackDescription = null;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelFeedbackDescription = viewModel.getFeedbackDescription();

                viewModelFeedbackDescriptionJavaLangObjectNull = (viewModelFeedbackDescription) != (null);
                if (viewModelFeedbackDescriptionJavaLangObjectNull) {




                    viewModelFeedbackDescription.setValue(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public FragmentFeedbackBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 12, sIncludes, sViewsWithIds));
    }
    private FragmentFeedbackBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 2
            , (android.widget.ImageView) bindings[2]
            , (androidx.recyclerview.widget.RecyclerView) bindings[9]
            , (android.widget.TextView) bindings[10]
            , (android.widget.EditText) bindings[3]
            , (android.widget.ProgressBar) bindings[11]
            , (android.widget.TextView) bindings[7]
            , (android.widget.TextView) bindings[8]
            , (android.widget.TextView) bindings[6]
            );
        this.existFeedbackAlert.setTag(null);
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.RelativeLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView4 = (android.widget.Button) bindings[4];
        this.mboundView4.setTag(null);
        this.mboundView5 = (android.widget.RelativeLayout) bindings[5];
        this.mboundView5.setTag(null);
        this.notesEditTextFeedback.setTag(null);
        setRootTag(root);
        // listeners
        mCallback37 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 1);
        mCallback38 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 2);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x8L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((com.impaktsoft.globeup.viewmodels.FeedbackViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable com.impaktsoft.globeup.viewmodels.FeedbackViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelIsProgressVisible((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 1 :
                return onChangeViewModelFeedbackDescription((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelIsProgressVisible(androidx.lifecycle.MutableLiveData<java.lang.Boolean> ViewModelIsProgressVisible, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelFeedbackDescription(androidx.lifecycle.MutableLiveData<java.lang.String> ViewModelFeedbackDescription, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        int viewModelIsProgressVisibleViewGONEViewVISIBLE = 0;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> viewModelIsProgressVisible = null;
        java.lang.Boolean viewModelIsProgressVisibleGetValue = null;
        java.lang.String viewModelFeedbackDescriptionGetValue = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxViewModelIsProgressVisibleGetValue = false;
        com.impaktsoft.globeup.viewmodels.FeedbackViewModel viewModel = mViewModel;
        androidx.lifecycle.MutableLiveData<java.lang.String> viewModelFeedbackDescription = null;
        int viewModelIsProgressVisibleViewVISIBLEViewGONE = 0;

        if ((dirtyFlags & 0xfL) != 0) {


            if ((dirtyFlags & 0xdL) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isProgressVisible
                        viewModelIsProgressVisible = viewModel.isProgressVisible();
                    }
                    updateLiveDataRegistration(0, viewModelIsProgressVisible);


                    if (viewModelIsProgressVisible != null) {
                        // read viewModel.isProgressVisible.getValue()
                        viewModelIsProgressVisibleGetValue = viewModelIsProgressVisible.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isProgressVisible.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelIsProgressVisibleGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelIsProgressVisibleGetValue);
                if((dirtyFlags & 0xdL) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxViewModelIsProgressVisibleGetValue) {
                            dirtyFlags |= 0x20L;
                            dirtyFlags |= 0x80L;
                    }
                    else {
                            dirtyFlags |= 0x10L;
                            dirtyFlags |= 0x40L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isProgressVisible.getValue()) ? View.GONE : View.VISIBLE
                    viewModelIsProgressVisibleViewGONEViewVISIBLE = ((androidxDatabindingViewDataBindingSafeUnboxViewModelIsProgressVisibleGetValue) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isProgressVisible.getValue()) ? View.VISIBLE : View.GONE
                    viewModelIsProgressVisibleViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxViewModelIsProgressVisibleGetValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0xeL) != 0) {

                    if (viewModel != null) {
                        // read viewModel.feedbackDescription
                        viewModelFeedbackDescription = viewModel.getFeedbackDescription();
                    }
                    updateLiveDataRegistration(1, viewModelFeedbackDescription);


                    if (viewModelFeedbackDescription != null) {
                        // read viewModel.feedbackDescription.getValue()
                        viewModelFeedbackDescriptionGetValue = viewModelFeedbackDescription.getValue();
                    }
            }
        }
        // batch finished
        if ((dirtyFlags & 0x8L) != 0) {
            // api target 1

            this.existFeedbackAlert.setOnClickListener(mCallback37);
            this.mboundView4.setOnClickListener(mCallback38);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.notesEditTextFeedback, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, notesEditTextFeedbackandroidTextAttrChanged);
        }
        if ((dirtyFlags & 0xdL) != 0) {
            // api target 1

            this.mboundView1.setVisibility(viewModelIsProgressVisibleViewGONEViewVISIBLE);
            this.mboundView5.setVisibility(viewModelIsProgressVisibleViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0xeL) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.notesEditTextFeedback, viewModelFeedbackDescriptionGetValue);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 1: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.FeedbackViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.close();
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.FeedbackViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.sendFeedback();
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.isProgressVisible
        flag 1 (0x2L): viewModel.feedbackDescription
        flag 2 (0x3L): viewModel
        flag 3 (0x4L): null
        flag 4 (0x5L): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isProgressVisible.getValue()) ? View.GONE : View.VISIBLE
        flag 5 (0x6L): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isProgressVisible.getValue()) ? View.GONE : View.VISIBLE
        flag 6 (0x7L): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isProgressVisible.getValue()) ? View.VISIBLE : View.GONE
        flag 7 (0x8L): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isProgressVisible.getValue()) ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}