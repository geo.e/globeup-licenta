package com.impaktsoft.globeup.databinding;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentImageCaptureBindingImpl extends FragmentImageCaptureBinding implements com.impaktsoft.globeup.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.take_picture_layout, 4);
        sViewsWithIds.put(R.id.texture_view, 5);
        sViewsWithIds.put(R.id.capture_button, 6);
        sViewsWithIds.put(R.id.preview_image_layout, 7);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback23;
    @Nullable
    private final android.view.View.OnClickListener mCallback22;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentImageCaptureBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 8, sIncludes, sViewsWithIds));
    }
    private FragmentImageCaptureBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (android.widget.ImageButton) bindings[3]
            , (android.widget.ImageButton) bindings[6]
            , (android.widget.ImageView) bindings[1]
            , (android.widget.RelativeLayout) bindings[7]
            , (android.widget.ImageButton) bindings[2]
            , (android.widget.RelativeLayout) bindings[4]
            , (android.view.TextureView) bindings[5]
            );
        this.acceptButton.setTag(null);
        this.imagePreview.setTag(null);
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.retryButton.setTag(null);
        setRootTag(root);
        // listeners
        mCallback23 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 2);
        mCallback22 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((com.impaktsoft.globeup.viewmodels.ImageCaptureViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable com.impaktsoft.globeup.viewmodels.ImageCaptureViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelImagePath((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelImagePath(androidx.lifecycle.MutableLiveData<java.lang.String> ViewModelImagePath, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String viewModelImagePathGetValue = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> viewModelImagePath = null;
        com.impaktsoft.globeup.viewmodels.ImageCaptureViewModel viewModel = mViewModel;

        if ((dirtyFlags & 0x7L) != 0) {



                if (viewModel != null) {
                    // read viewModel.imagePath
                    viewModelImagePath = viewModel.getImagePath();
                }
                updateLiveDataRegistration(0, viewModelImagePath);


                if (viewModelImagePath != null) {
                    // read viewModel.imagePath.getValue()
                    viewModelImagePathGetValue = viewModelImagePath.getValue();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.acceptButton.setOnClickListener(mCallback23);
            this.retryButton.setOnClickListener(mCallback22);
        }
        if ((dirtyFlags & 0x7L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.ImageViewBindingAdapters.file(this.imagePreview, viewModelImagePathGetValue);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 2: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.ImageCaptureViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.accept();
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.ImageCaptureViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.retry();
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.imagePath
        flag 1 (0x2L): viewModel
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}