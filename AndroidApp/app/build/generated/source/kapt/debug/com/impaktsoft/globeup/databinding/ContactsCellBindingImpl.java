package com.impaktsoft.globeup.databinding;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ContactsCellBindingImpl extends ContactsCellBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.conversation_layout_image, 4);
    }
    // views
    @NonNull
    private final android.widget.ImageView mboundView2;
    @NonNull
    private final android.widget.TextView mboundView3;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ContactsCellBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 5, sIncludes, sViewsWithIds));
    }
    private ContactsCellBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.RelativeLayout) bindings[0]
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[1]
            , (android.widget.RelativeLayout) bindings[4]
            );
        this.contactLayout.setTag(null);
        this.conversationImage.setTag(null);
        this.mboundView2 = (android.widget.ImageView) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (android.widget.TextView) bindings[3];
        this.mboundView3.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.user == variableId) {
            setUser((com.impaktsoft.globeup.models.UserPOJO) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setUser(@Nullable com.impaktsoft.globeup.models.UserPOJO User) {
        this.mUser = User;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.user);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        com.impaktsoft.globeup.models.UserPOJO user = mUser;
        java.lang.String userImgUrl = null;
        boolean bindingConvertIsOnlineUser = false;
        int bindingConvertIsOnlineUserViewVISIBLEViewGONE = 0;
        java.lang.String userName = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (user != null) {
                    // read user.imgUrl
                    userImgUrl = user.getImgUrl();
                    // read user.name
                    userName = user.getName();
                }
                // read BindingConvert.isOnline(user)
                bindingConvertIsOnlineUser = com.impaktsoft.globeup.bindingadapters.BindingConvert.isOnline(user);
            if((dirtyFlags & 0x3L) != 0) {
                if(bindingConvertIsOnlineUser) {
                        dirtyFlags |= 0x8L;
                }
                else {
                        dirtyFlags |= 0x4L;
                }
            }


                // read BindingConvert.isOnline(user) ? View.VISIBLE : View.GONE
                bindingConvertIsOnlineUserViewVISIBLEViewGONE = ((bindingConvertIsOnlineUser) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.ImageViewBindingAdapters.imageUrl(this.conversationImage, userImgUrl, androidx.appcompat.content.res.AppCompatResources.getDrawable(conversationImage.getContext(), R.drawable.profile_filled_svg));
            this.mboundView2.setVisibility(bindingConvertIsOnlineUserViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, userName);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): user
        flag 1 (0x2L): null
        flag 2 (0x3L): BindingConvert.isOnline(user) ? View.VISIBLE : View.GONE
        flag 3 (0x4L): BindingConvert.isOnline(user) ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}