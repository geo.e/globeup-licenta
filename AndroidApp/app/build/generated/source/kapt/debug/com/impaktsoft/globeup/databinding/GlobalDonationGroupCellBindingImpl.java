package com.impaktsoft.globeup.databinding;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class GlobalDonationGroupCellBindingImpl extends GlobalDonationGroupCellBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.global_profile_image, 6);
    }
    // views
    @NonNull
    private final androidx.cardview.widget.CardView mboundView0;
    @NonNull
    private final android.widget.TextView mboundView2;
    @NonNull
    private final android.widget.TextView mboundView3;
    @NonNull
    private final android.widget.TextView mboundView4;
    @NonNull
    private final android.widget.TextView mboundView5;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public GlobalDonationGroupCellBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 7, sIncludes, sViewsWithIds));
    }
    private GlobalDonationGroupCellBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[6]
            , (android.widget.TextView) bindings[1]
            );
        this.globalRankNumber.setTag(null);
        this.mboundView0 = (androidx.cardview.widget.CardView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView2 = (android.widget.TextView) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (android.widget.TextView) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView4 = (android.widget.TextView) bindings[4];
        this.mboundView4.setTag(null);
        this.mboundView5 = (android.widget.TextView) bindings[5];
        this.mboundView5.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.group == variableId) {
            setGroup((com.impaktsoft.globeup.models.GlobalGroupPOJO) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setGroup(@Nullable com.impaktsoft.globeup.models.GlobalGroupPOJO Group) {
        this.mGroup = Group;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.group);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String groupName = null;
        java.lang.String groupRankToString = null;
        com.impaktsoft.globeup.models.GlobalGroupPOJO group = mGroup;
        java.lang.String groupEventsToString = null;
        java.lang.Integer groupRank = null;
        java.lang.String groupExperienceToString = null;
        java.lang.Integer groupEvents = null;
        java.lang.Integer groupMembers = null;
        java.lang.String groupMembersToString = null;
        java.lang.Integer groupExperience = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (group != null) {
                    // read group.name
                    groupName = group.getName();
                    // read group.rank
                    groupRank = group.getRank();
                    // read group.events
                    groupEvents = group.getEvents();
                    // read group.members
                    groupMembers = group.getMembers();
                    // read group.experience
                    groupExperience = group.getExperience();
                }


                if (groupRank != null) {
                    // read group.rank.toString()
                    groupRankToString = groupRank.toString();
                }
                if (groupEvents != null) {
                    // read group.events.toString()
                    groupEventsToString = groupEvents.toString();
                }
                if (groupMembers != null) {
                    // read group.members.toString()
                    groupMembersToString = groupMembers.toString();
                }
                if (groupExperience != null) {
                    // read group.experience.toString()
                    groupExperienceToString = groupExperience.toString();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.globalRankNumber, groupRankToString);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, groupName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, groupMembersToString);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView4, groupExperienceToString);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView5, groupEventsToString);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): group
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}