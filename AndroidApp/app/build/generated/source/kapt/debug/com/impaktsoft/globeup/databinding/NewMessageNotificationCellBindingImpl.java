package com.impaktsoft.globeup.databinding;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class NewMessageNotificationCellBindingImpl extends NewMessageNotificationCellBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final android.widget.LinearLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public NewMessageNotificationCellBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 2, sIncludes, sViewsWithIds));
    }
    private NewMessageNotificationCellBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.TextView) bindings[1]
            );
        this.mboundView0 = (android.widget.LinearLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.notificationsBadge.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.notificationCount == variableId) {
            setNotificationCount((java.lang.Integer) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setNotificationCount(@Nullable java.lang.Integer NotificationCount) {
        this.mNotificationCount = NotificationCount;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.notificationCount);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.Integer notificationCount = mNotificationCount;
        boolean bindingConvertNotificationIsShowedNotificationCountIntValue = false;
        int bindingConvertNotificationIsShowedNotificationCountIntValueViewVISIBLEViewGONE = 0;
        java.lang.String notificationCountToString = null;
        int notificationCountIntValue = 0;

        if ((dirtyFlags & 0x3L) != 0) {



                if (notificationCount != null) {
                    // read notificationCount.toString()
                    notificationCountToString = notificationCount.toString();
                    // read notificationCount.intValue()
                    notificationCountIntValue = notificationCount.intValue();
                }


                // read BindingConvert.notificationIsShowed(notificationCount.intValue())
                bindingConvertNotificationIsShowedNotificationCountIntValue = com.impaktsoft.globeup.bindingadapters.BindingConvert.notificationIsShowed(notificationCountIntValue);
            if((dirtyFlags & 0x3L) != 0) {
                if(bindingConvertNotificationIsShowedNotificationCountIntValue) {
                        dirtyFlags |= 0x8L;
                }
                else {
                        dirtyFlags |= 0x4L;
                }
            }


                // read BindingConvert.notificationIsShowed(notificationCount.intValue()) ? View.VISIBLE : View.GONE
                bindingConvertNotificationIsShowedNotificationCountIntValueViewVISIBLEViewGONE = ((bindingConvertNotificationIsShowedNotificationCountIntValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.notificationsBadge, notificationCountToString);
            this.notificationsBadge.setVisibility(bindingConvertNotificationIsShowedNotificationCountIntValueViewVISIBLEViewGONE);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): notificationCount
        flag 1 (0x2L): null
        flag 2 (0x3L): BindingConvert.notificationIsShowed(notificationCount.intValue()) ? View.VISIBLE : View.GONE
        flag 3 (0x4L): BindingConvert.notificationIsShowed(notificationCount.intValue()) ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}