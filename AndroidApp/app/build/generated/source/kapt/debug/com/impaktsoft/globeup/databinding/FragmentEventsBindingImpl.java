package com.impaktsoft.globeup.databinding;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentEventsBindingImpl extends FragmentEventsBinding implements com.impaktsoft.globeup.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.events_fragment_filter_applyed_recyclerview, 8);
        sViewsWithIds.put(R.id.chooseCountryView, 9);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    @NonNull
    private final android.widget.TextView mboundView1;
    @NonNull
    private final android.widget.LinearLayout mboundView2;
    @NonNull
    private final android.widget.TextView mboundView3;
    @NonNull
    private final com.google.android.material.floatingactionbutton.FloatingActionButton mboundView7;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback25;
    @Nullable
    private final android.view.View.OnClickListener mCallback24;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentEventsBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 10, sIncludes, sViewsWithIds));
    }
    private FragmentEventsBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 4
            , (android.widget.LinearLayout) bindings[9]
            , (androidx.recyclerview.widget.RecyclerView) bindings[8]
            , (androidx.recyclerview.widget.RecyclerView) bindings[4]
            , (android.widget.TextView) bindings[5]
            , (android.widget.ProgressBar) bindings[6]
            );
        this.eventsFragmentRecyclerview.setTag(null);
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.TextView) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView2 = (android.widget.LinearLayout) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (android.widget.TextView) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView7 = (com.google.android.material.floatingactionbutton.FloatingActionButton) bindings[7];
        this.mboundView7.setTag(null);
        this.placeholderMessage.setTag(null);
        this.progressBar.setTag(null);
        setRootTag(root);
        // listeners
        mCallback25 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 2);
        mCallback24 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x20L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((com.impaktsoft.globeup.viewmodels.EventsViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable com.impaktsoft.globeup.viewmodels.EventsViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x10L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelIsPlaceHolderMessageVisible((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 1 :
                return onChangeViewModelCurrentCountryBinding((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 2 :
                return onChangeViewModelIsProgressBarVisible((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 3 :
                return onChangeViewModelLoadingPosition((androidx.lifecycle.MutableLiveData<java.lang.Integer>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelIsPlaceHolderMessageVisible(androidx.lifecycle.MutableLiveData<java.lang.Boolean> ViewModelIsPlaceHolderMessageVisible, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelCurrentCountryBinding(androidx.lifecycle.MutableLiveData<java.lang.String> ViewModelCurrentCountryBinding, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsProgressBarVisible(androidx.lifecycle.MutableLiveData<java.lang.Boolean> ViewModelIsProgressBarVisible, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelLoadingPosition(androidx.lifecycle.MutableLiveData<java.lang.Integer> ViewModelLoadingPosition, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.Boolean viewModelIsPlaceHolderMessageVisibleGetValue = null;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> viewModelIsPlaceHolderMessageVisible = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> viewModelCurrentCountryBinding = null;
        int viewModelIsPlaceHolderMessageVisibleViewVISIBLEViewGONE = 0;
        int viewModelIsProgressBarVisibleViewVISIBLEViewGONE = 0;
        boolean androidxDatabindingViewDataBindingSafeUnboxViewModelIsProgressBarVisibleGetValue = false;
        java.lang.Integer viewModelLoadingPositionGetValue = null;
        int androidxDatabindingViewDataBindingSafeUnboxViewModelLoadingPositionGetValue = 0;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> viewModelIsProgressBarVisible = null;
        androidx.lifecycle.MutableLiveData<java.lang.Integer> viewModelLoadingPosition = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxViewModelIsPlaceHolderMessageVisibleGetValue = false;
        java.lang.String viewModelCurrentCountryBindingGetValue = null;
        com.impaktsoft.globeup.viewmodels.EventsViewModel viewModel = mViewModel;
        java.lang.Boolean viewModelIsProgressBarVisibleGetValue = null;

        if ((dirtyFlags & 0x3fL) != 0) {


            if ((dirtyFlags & 0x31L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isPlaceHolderMessageVisible
                        viewModelIsPlaceHolderMessageVisible = viewModel.isPlaceHolderMessageVisible();
                    }
                    updateLiveDataRegistration(0, viewModelIsPlaceHolderMessageVisible);


                    if (viewModelIsPlaceHolderMessageVisible != null) {
                        // read viewModel.isPlaceHolderMessageVisible.getValue()
                        viewModelIsPlaceHolderMessageVisibleGetValue = viewModelIsPlaceHolderMessageVisible.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isPlaceHolderMessageVisible.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelIsPlaceHolderMessageVisibleGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelIsPlaceHolderMessageVisibleGetValue);
                if((dirtyFlags & 0x31L) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxViewModelIsPlaceHolderMessageVisibleGetValue) {
                            dirtyFlags |= 0x80L;
                    }
                    else {
                            dirtyFlags |= 0x40L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isPlaceHolderMessageVisible.getValue()) ? View.VISIBLE : View.GONE
                    viewModelIsPlaceHolderMessageVisibleViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxViewModelIsPlaceHolderMessageVisibleGetValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0x32L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.currentCountryBinding
                        viewModelCurrentCountryBinding = viewModel.getCurrentCountryBinding();
                    }
                    updateLiveDataRegistration(1, viewModelCurrentCountryBinding);


                    if (viewModelCurrentCountryBinding != null) {
                        // read viewModel.currentCountryBinding.getValue()
                        viewModelCurrentCountryBindingGetValue = viewModelCurrentCountryBinding.getValue();
                    }
            }
            if ((dirtyFlags & 0x34L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isProgressBarVisible
                        viewModelIsProgressBarVisible = viewModel.isProgressBarVisible();
                    }
                    updateLiveDataRegistration(2, viewModelIsProgressBarVisible);


                    if (viewModelIsProgressBarVisible != null) {
                        // read viewModel.isProgressBarVisible.getValue()
                        viewModelIsProgressBarVisibleGetValue = viewModelIsProgressBarVisible.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isProgressBarVisible.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelIsProgressBarVisibleGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelIsProgressBarVisibleGetValue);
                if((dirtyFlags & 0x34L) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxViewModelIsProgressBarVisibleGetValue) {
                            dirtyFlags |= 0x200L;
                    }
                    else {
                            dirtyFlags |= 0x100L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isProgressBarVisible.getValue()) ? View.VISIBLE : View.GONE
                    viewModelIsProgressBarVisibleViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxViewModelIsProgressBarVisibleGetValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0x38L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.loadingPosition
                        viewModelLoadingPosition = viewModel.getLoadingPosition();
                    }
                    updateLiveDataRegistration(3, viewModelLoadingPosition);


                    if (viewModelLoadingPosition != null) {
                        // read viewModel.loadingPosition.getValue()
                        viewModelLoadingPositionGetValue = viewModelLoadingPosition.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.loadingPosition.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelLoadingPositionGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelLoadingPositionGetValue);
            }
        }
        // batch finished
        if ((dirtyFlags & 0x30L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.RecyclerViewBindingAdapter.setScrollMethod(this.eventsFragmentRecyclerview, viewModel);
        }
        if ((dirtyFlags & 0x38L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.RecyclerViewBindingAdapter.scrollToPosition(this.eventsFragmentRecyclerview, androidxDatabindingViewDataBindingSafeUnboxViewModelLoadingPositionGetValue);
        }
        if ((dirtyFlags & 0x20L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView1, (mboundView1.getResources().getString(R.string.select_country)) + (':'));
            this.mboundView2.setOnClickListener(mCallback24);
            this.mboundView7.setOnClickListener(mCallback25);
        }
        if ((dirtyFlags & 0x32L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, viewModelCurrentCountryBindingGetValue);
        }
        if ((dirtyFlags & 0x31L) != 0) {
            // api target 1

            this.placeholderMessage.setVisibility(viewModelIsPlaceHolderMessageVisibleViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x34L) != 0) {
            // api target 1

            this.progressBar.setVisibility(viewModelIsProgressBarVisibleViewVISIBLEViewGONE);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 2: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.EventsViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.addNewEventClicked();
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.EventsViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.chooseCountryClick();
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.isPlaceHolderMessageVisible
        flag 1 (0x2L): viewModel.currentCountryBinding
        flag 2 (0x3L): viewModel.isProgressBarVisible
        flag 3 (0x4L): viewModel.loadingPosition
        flag 4 (0x5L): viewModel
        flag 5 (0x6L): null
        flag 6 (0x7L): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isPlaceHolderMessageVisible.getValue()) ? View.VISIBLE : View.GONE
        flag 7 (0x8L): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isPlaceHolderMessageVisible.getValue()) ? View.VISIBLE : View.GONE
        flag 8 (0x9L): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isProgressBarVisible.getValue()) ? View.VISIBLE : View.GONE
        flag 9 (0xaL): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isProgressBarVisible.getValue()) ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}