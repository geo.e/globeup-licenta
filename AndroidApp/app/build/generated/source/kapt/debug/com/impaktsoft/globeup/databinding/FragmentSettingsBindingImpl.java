package com.impaktsoft.globeup.databinding;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentSettingsBindingImpl extends FragmentSettingsBinding implements com.impaktsoft.globeup.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.metric_system_layout, 10);
        sViewsWithIds.put(R.id.metric_system, 11);
        sViewsWithIds.put(R.id.bottomView, 12);
        sViewsWithIds.put(R.id.globe_up_version, 13);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    @NonNull
    private final android.widget.TextView mboundView2;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback17;
    @Nullable
    private final android.view.View.OnClickListener mCallback15;
    @Nullable
    private final android.view.View.OnClickListener mCallback21;
    @Nullable
    private final android.view.View.OnClickListener mCallback19;
    @Nullable
    private final android.view.View.OnClickListener mCallback20;
    @Nullable
    private final android.view.View.OnClickListener mCallback16;
    @Nullable
    private final android.view.View.OnClickListener mCallback14;
    @Nullable
    private final android.view.View.OnClickListener mCallback18;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentSettingsBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 14, sIncludes, sViewsWithIds));
    }
    private FragmentSettingsBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.LinearLayout) bindings[12]
            , (android.widget.LinearLayout) bindings[5]
            , (android.widget.LinearLayout) bindings[1]
            , (android.widget.LinearLayout) bindings[4]
            , (android.widget.TextView) bindings[13]
            , (android.widget.LinearLayout) bindings[3]
            , (android.widget.RadioButton) bindings[7]
            , (android.widget.LinearLayout) bindings[9]
            , (android.widget.RadioGroup) bindings[11]
            , (android.widget.RelativeLayout) bindings[10]
            , (android.widget.RadioButton) bindings[6]
            , (android.widget.RelativeLayout) bindings[8]
            );
        this.deleteAccountLayout.setTag(null);
        this.editProfileLayout.setTag(null);
        this.feedbackLayout.setTag(null);
        this.inviteFriendsLayout.setTag(null);
        this.kilometersButton.setTag(null);
        this.logoutLayout.setTag(null);
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView2 = (android.widget.TextView) bindings[2];
        this.mboundView2.setTag(null);
        this.milesButton.setTag(null);
        this.rateLayout.setTag(null);
        setRootTag(root);
        // listeners
        mCallback17 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 4);
        mCallback15 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 2);
        mCallback21 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 8);
        mCallback19 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 6);
        mCallback20 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 7);
        mCallback16 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 3);
        mCallback14 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 1);
        mCallback18 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 5);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((com.impaktsoft.globeup.viewmodels.SettingsViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable com.impaktsoft.globeup.viewmodels.SettingsViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String viewModelGetEditOrRegisterText = null;
        boolean viewModelCurrentScaleTypeSelectedEqualsScaleTypeEnumMILES = false;
        boolean viewModelCurrentScaleTypeSelectedEqualsScaleTypeEnumKILOMETERS = false;
        boolean viewModelIsUserAnonymous = false;
        int viewModelIsUserAnonymousViewGONEViewVISIBLE = 0;
        com.impaktsoft.globeup.enums.ScaleTypeEnum viewModelCurrentScaleTypeSelected = null;
        com.impaktsoft.globeup.viewmodels.SettingsViewModel viewModel = mViewModel;

        if ((dirtyFlags & 0x3L) != 0) {



                if (viewModel != null) {
                    // read viewModel.getEditOrRegisterText
                    viewModelGetEditOrRegisterText = viewModel.getGetEditOrRegisterText();
                    // read viewModel.isUserAnonymous
                    viewModelIsUserAnonymous = viewModel.isUserAnonymous();
                    // read viewModel.currentScaleTypeSelected
                    viewModelCurrentScaleTypeSelected = viewModel.getCurrentScaleTypeSelected();
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(viewModelIsUserAnonymous) {
                        dirtyFlags |= 0x8L;
                }
                else {
                        dirtyFlags |= 0x4L;
                }
            }


                // read viewModel.isUserAnonymous ? View.GONE : View.VISIBLE
                viewModelIsUserAnonymousViewGONEViewVISIBLE = ((viewModelIsUserAnonymous) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
                if (viewModelCurrentScaleTypeSelected != null) {
                    // read viewModel.currentScaleTypeSelected.equals(ScaleTypeEnum.MILES)
                    viewModelCurrentScaleTypeSelectedEqualsScaleTypeEnumMILES = viewModelCurrentScaleTypeSelected.equals(com.impaktsoft.globeup.enums.ScaleTypeEnum.MILES);
                    // read viewModel.currentScaleTypeSelected.equals(ScaleTypeEnum.KILOMETERS)
                    viewModelCurrentScaleTypeSelectedEqualsScaleTypeEnumKILOMETERS = viewModelCurrentScaleTypeSelected.equals(com.impaktsoft.globeup.enums.ScaleTypeEnum.KILOMETERS);
                }
        }
        // batch finished
        if ((dirtyFlags & 0x2L) != 0) {
            // api target 1

            this.deleteAccountLayout.setOnClickListener(mCallback17);
            this.editProfileLayout.setOnClickListener(mCallback14);
            this.feedbackLayout.setOnClickListener(mCallback16);
            this.inviteFriendsLayout.setOnClickListener(mCallback15);
            this.kilometersButton.setOnClickListener(mCallback19);
            this.logoutLayout.setOnClickListener(mCallback21);
            this.milesButton.setOnClickListener(mCallback18);
            this.rateLayout.setOnClickListener(mCallback20);
        }
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            this.deleteAccountLayout.setVisibility(viewModelIsUserAnonymousViewGONEViewVISIBLE);
            androidx.databinding.adapters.CompoundButtonBindingAdapter.setChecked(this.kilometersButton, viewModelCurrentScaleTypeSelectedEqualsScaleTypeEnumKILOMETERS);
            this.logoutLayout.setVisibility(viewModelIsUserAnonymousViewGONEViewVISIBLE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, viewModelGetEditOrRegisterText);
            androidx.databinding.adapters.CompoundButtonBindingAdapter.setChecked(this.milesButton, viewModelCurrentScaleTypeSelectedEqualsScaleTypeEnumMILES);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 4: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.SettingsViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.showDeleteDialog();
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.SettingsViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.inviteFriendsClick();
                }
                break;
            }
            case 8: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.SettingsViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.logout();
                }
                break;
            }
            case 6: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.SettingsViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {




                    viewModel.setMetricsSystem(com.impaktsoft.globeup.enums.ScaleTypeEnum.KILOMETERS);
                }
                break;
            }
            case 7: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.SettingsViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.rateInPlayStore();
                }
                break;
            }
            case 3: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.SettingsViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.showFeedbackDialog();
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.SettingsViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.editClick();
                }
                break;
            }
            case 5: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.SettingsViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {




                    viewModel.setMetricsSystem(com.impaktsoft.globeup.enums.ScaleTypeEnum.MILES);
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel
        flag 1 (0x2L): null
        flag 2 (0x3L): viewModel.isUserAnonymous ? View.GONE : View.VISIBLE
        flag 3 (0x4L): viewModel.isUserAnonymous ? View.GONE : View.VISIBLE
    flag mapping end*/
    //end
}