package com.impaktsoft.globeup.databinding;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentDeleteAccountBindingImpl extends FragmentDeleteAccountBinding implements com.impaktsoft.globeup.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    @NonNull
    private final android.widget.Button mboundView2;
    @NonNull
    private final android.widget.Button mboundView3;
    @NonNull
    private final android.widget.Button mboundView7;
    @NonNull
    private final android.widget.Button mboundView8;
    @NonNull
    private final android.widget.RelativeLayout mboundView9;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback57;
    @Nullable
    private final android.view.View.OnClickListener mCallback55;
    @Nullable
    private final android.view.View.OnClickListener mCallback58;
    @Nullable
    private final android.view.View.OnClickListener mCallback56;
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener editConfirmPasswordEditTextandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.passwordRepeat.getValue()
            //         is viewModel.passwordRepeat.setValue((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(editConfirmPasswordEditText);
            // localize variables for thread safety
            // viewModel.passwordRepeat
            androidx.lifecycle.MutableLiveData<java.lang.String> viewModelPasswordRepeat = null;
            // viewModel.passwordRepeat.getValue()
            java.lang.String viewModelPasswordRepeatGetValue = null;
            // viewModel
            com.impaktsoft.globeup.viewmodels.DeleteAccountViewModel viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.passwordRepeat != null
            boolean viewModelPasswordRepeatJavaLangObjectNull = false;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelPasswordRepeat = viewModel.getPasswordRepeat();

                viewModelPasswordRepeatJavaLangObjectNull = (viewModelPasswordRepeat) != (null);
                if (viewModelPasswordRepeatJavaLangObjectNull) {




                    viewModelPasswordRepeat.setValue(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener editPasswordEditTextandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.password.getValue()
            //         is viewModel.password.setValue((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(editPasswordEditText);
            // localize variables for thread safety
            // viewModel.password != null
            boolean viewModelPasswordJavaLangObjectNull = false;
            // viewModel.password.getValue()
            java.lang.String viewModelPasswordGetValue = null;
            // viewModel
            com.impaktsoft.globeup.viewmodels.DeleteAccountViewModel viewModel = mViewModel;
            // viewModel.password
            androidx.lifecycle.MutableLiveData<java.lang.String> viewModelPassword = null;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelPassword = viewModel.getPassword();

                viewModelPasswordJavaLangObjectNull = (viewModelPassword) != (null);
                if (viewModelPasswordJavaLangObjectNull) {




                    viewModelPassword.setValue(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public FragmentDeleteAccountBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 10, sIncludes, sViewsWithIds));
    }
    private FragmentDeleteAccountBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 4
            , (android.widget.EditText) bindings[6]
            , (android.widget.EditText) bindings[5]
            , (android.widget.LinearLayout) bindings[1]
            , (android.widget.LinearLayout) bindings[4]
            );
        this.editConfirmPasswordEditText.setTag(null);
        this.editPasswordEditText.setTag(null);
        this.firstView.setTag(null);
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView2 = (android.widget.Button) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (android.widget.Button) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView7 = (android.widget.Button) bindings[7];
        this.mboundView7.setTag(null);
        this.mboundView8 = (android.widget.Button) bindings[8];
        this.mboundView8.setTag(null);
        this.mboundView9 = (android.widget.RelativeLayout) bindings[9];
        this.mboundView9.setTag(null);
        this.secondView.setTag(null);
        setRootTag(root);
        // listeners
        mCallback57 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 3);
        mCallback55 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 1);
        mCallback58 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 4);
        mCallback56 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 2);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x20L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((com.impaktsoft.globeup.viewmodels.DeleteAccountViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable com.impaktsoft.globeup.viewmodels.DeleteAccountViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x10L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelIsConfirmViewVisible((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 1 :
                return onChangeViewModelIsLoadingViewVisible((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 2 :
                return onChangeViewModelPasswordRepeat((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 3 :
                return onChangeViewModelPassword((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelIsConfirmViewVisible(androidx.lifecycle.MutableLiveData<java.lang.Boolean> ViewModelIsConfirmViewVisible, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsLoadingViewVisible(androidx.lifecycle.MutableLiveData<java.lang.Boolean> ViewModelIsLoadingViewVisible, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelPasswordRepeat(androidx.lifecycle.MutableLiveData<java.lang.String> ViewModelPasswordRepeat, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelPassword(androidx.lifecycle.MutableLiveData<java.lang.String> ViewModelPassword, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String viewModelPasswordGetValue = null;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> viewModelIsConfirmViewVisible = null;
        java.lang.Boolean viewModelIsLoadingViewVisibleGetValue = null;
        int viewModelIsConfirmViewVisibleViewVISIBLEViewGONE = 0;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> viewModelIsLoadingViewVisible = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> viewModelPasswordRepeat = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxViewModelIsConfirmViewVisibleGetValue = false;
        java.lang.String viewModelPasswordRepeatGetValue = null;
        int viewModelIsConfirmViewVisibleViewGONEViewVISIBLE = 0;
        java.lang.Boolean viewModelIsConfirmViewVisibleGetValue = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxViewModelIsLoadingViewVisibleGetValue = false;
        int viewModelIsLoadingViewVisibleViewVISIBLEViewGONE = 0;
        com.impaktsoft.globeup.viewmodels.DeleteAccountViewModel viewModel = mViewModel;
        androidx.lifecycle.MutableLiveData<java.lang.String> viewModelPassword = null;

        if ((dirtyFlags & 0x3fL) != 0) {


            if ((dirtyFlags & 0x31L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isConfirmViewVisible
                        viewModelIsConfirmViewVisible = viewModel.isConfirmViewVisible();
                    }
                    updateLiveDataRegistration(0, viewModelIsConfirmViewVisible);


                    if (viewModelIsConfirmViewVisible != null) {
                        // read viewModel.isConfirmViewVisible.getValue()
                        viewModelIsConfirmViewVisibleGetValue = viewModelIsConfirmViewVisible.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isConfirmViewVisible.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelIsConfirmViewVisibleGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelIsConfirmViewVisibleGetValue);
                if((dirtyFlags & 0x31L) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxViewModelIsConfirmViewVisibleGetValue) {
                            dirtyFlags |= 0x80L;
                            dirtyFlags |= 0x200L;
                    }
                    else {
                            dirtyFlags |= 0x40L;
                            dirtyFlags |= 0x100L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isConfirmViewVisible.getValue()) ? View.VISIBLE : View.GONE
                    viewModelIsConfirmViewVisibleViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxViewModelIsConfirmViewVisibleGetValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isConfirmViewVisible.getValue()) ? View.GONE : View.VISIBLE
                    viewModelIsConfirmViewVisibleViewGONEViewVISIBLE = ((androidxDatabindingViewDataBindingSafeUnboxViewModelIsConfirmViewVisibleGetValue) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
            }
            if ((dirtyFlags & 0x32L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isLoadingViewVisible
                        viewModelIsLoadingViewVisible = viewModel.isLoadingViewVisible();
                    }
                    updateLiveDataRegistration(1, viewModelIsLoadingViewVisible);


                    if (viewModelIsLoadingViewVisible != null) {
                        // read viewModel.isLoadingViewVisible.getValue()
                        viewModelIsLoadingViewVisibleGetValue = viewModelIsLoadingViewVisible.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isLoadingViewVisible.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelIsLoadingViewVisibleGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelIsLoadingViewVisibleGetValue);
                if((dirtyFlags & 0x32L) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxViewModelIsLoadingViewVisibleGetValue) {
                            dirtyFlags |= 0x800L;
                    }
                    else {
                            dirtyFlags |= 0x400L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isLoadingViewVisible.getValue()) ? View.VISIBLE : View.GONE
                    viewModelIsLoadingViewVisibleViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxViewModelIsLoadingViewVisibleGetValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0x34L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.passwordRepeat
                        viewModelPasswordRepeat = viewModel.getPasswordRepeat();
                    }
                    updateLiveDataRegistration(2, viewModelPasswordRepeat);


                    if (viewModelPasswordRepeat != null) {
                        // read viewModel.passwordRepeat.getValue()
                        viewModelPasswordRepeatGetValue = viewModelPasswordRepeat.getValue();
                    }
            }
            if ((dirtyFlags & 0x38L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.password
                        viewModelPassword = viewModel.getPassword();
                    }
                    updateLiveDataRegistration(3, viewModelPassword);


                    if (viewModelPassword != null) {
                        // read viewModel.password.getValue()
                        viewModelPasswordGetValue = viewModelPassword.getValue();
                    }
            }
        }
        // batch finished
        if ((dirtyFlags & 0x34L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.editConfirmPasswordEditText, viewModelPasswordRepeatGetValue);
        }
        if ((dirtyFlags & 0x20L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.editConfirmPasswordEditText, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, editConfirmPasswordEditTextandroidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.editPasswordEditText, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, editPasswordEditTextandroidTextAttrChanged);
            this.mboundView2.setOnClickListener(mCallback55);
            this.mboundView3.setOnClickListener(mCallback56);
            this.mboundView7.setOnClickListener(mCallback57);
            this.mboundView8.setOnClickListener(mCallback58);
        }
        if ((dirtyFlags & 0x38L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.editPasswordEditText, viewModelPasswordGetValue);
        }
        if ((dirtyFlags & 0x31L) != 0) {
            // api target 1

            this.firstView.setVisibility(viewModelIsConfirmViewVisibleViewGONEViewVISIBLE);
            this.secondView.setVisibility(viewModelIsConfirmViewVisibleViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x32L) != 0) {
            // api target 1

            this.mboundView9.setVisibility(viewModelIsLoadingViewVisibleViewVISIBLEViewGONE);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 3: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.DeleteAccountViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.keepAccountClicked();
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.DeleteAccountViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.keepAccountClicked();
                }
                break;
            }
            case 4: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.DeleteAccountViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.confirmDeleteAccountClicked();
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.DeleteAccountViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.deleteAccountClicked();
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.isConfirmViewVisible
        flag 1 (0x2L): viewModel.isLoadingViewVisible
        flag 2 (0x3L): viewModel.passwordRepeat
        flag 3 (0x4L): viewModel.password
        flag 4 (0x5L): viewModel
        flag 5 (0x6L): null
        flag 6 (0x7L): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isConfirmViewVisible.getValue()) ? View.VISIBLE : View.GONE
        flag 7 (0x8L): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isConfirmViewVisible.getValue()) ? View.VISIBLE : View.GONE
        flag 8 (0x9L): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isConfirmViewVisible.getValue()) ? View.GONE : View.VISIBLE
        flag 9 (0xaL): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isConfirmViewVisible.getValue()) ? View.GONE : View.VISIBLE
        flag 10 (0xbL): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isLoadingViewVisible.getValue()) ? View.VISIBLE : View.GONE
        flag 11 (0xcL): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isLoadingViewVisible.getValue()) ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}