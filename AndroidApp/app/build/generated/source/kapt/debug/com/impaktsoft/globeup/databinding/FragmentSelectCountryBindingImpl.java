package com.impaktsoft.globeup.databinding;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentSelectCountryBindingImpl extends FragmentSelectCountryBinding implements com.impaktsoft.globeup.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.select_country_title, 4);
        sViewsWithIds.put(R.id.select_country_edit_text, 5);
        sViewsWithIds.put(R.id.country_list, 6);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    @NonNull
    private final android.view.View mboundView3;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback59;
    @Nullable
    private final android.view.View.OnClickListener mCallback60;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentSelectCountryBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 7, sIncludes, sViewsWithIds));
    }
    private FragmentSelectCountryBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 3
            , (androidx.recyclerview.widget.RecyclerView) bindings[6]
            , (android.widget.ImageView) bindings[1]
            , (android.widget.EditText) bindings[5]
            , (android.widget.Button) bindings[2]
            , (android.widget.TextView) bindings[4]
            );
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView3 = (android.view.View) bindings[3];
        this.mboundView3.setTag(null);
        this.selectCountryCloseImage.setTag(null);
        this.selectCountrySaveButton.setTag(null);
        setRootTag(root);
        // listeners
        mCallback59 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 1);
        mCallback60 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 2);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x10L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((com.impaktsoft.globeup.viewmodels.SelectCountryViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable com.impaktsoft.globeup.viewmodels.SelectCountryViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x8L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelBlockActionIfCountryNotSelected((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 1 :
                return onChangeViewModelButtonText((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 2 :
                return onChangeViewModelIsCloseButtonVisible((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelBlockActionIfCountryNotSelected(androidx.lifecycle.MutableLiveData<java.lang.Boolean> ViewModelBlockActionIfCountryNotSelected, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelButtonText(androidx.lifecycle.MutableLiveData<java.lang.String> ViewModelButtonText, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsCloseButtonVisible(androidx.lifecycle.MutableLiveData<java.lang.Boolean> ViewModelIsCloseButtonVisible, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> viewModelBlockActionIfCountryNotSelected = null;
        int viewModelBlockActionIfCountryNotSelectedViewVISIBLEViewGONE = 0;
        int viewModelIsCloseButtonVisibleViewVISIBLEViewGONE = 0;
        java.lang.Boolean viewModelIsCloseButtonVisibleGetValue = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> viewModelButtonText = null;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> viewModelIsCloseButtonVisible = null;
        java.lang.Boolean viewModelBlockActionIfCountryNotSelectedGetValue = null;
        java.lang.String viewModelButtonTextGetValue = null;
        com.impaktsoft.globeup.viewmodels.SelectCountryViewModel viewModel = mViewModel;
        boolean androidxDatabindingViewDataBindingSafeUnboxViewModelIsCloseButtonVisibleGetValue = false;
        boolean androidxDatabindingViewDataBindingSafeUnboxViewModelBlockActionIfCountryNotSelectedGetValue = false;

        if ((dirtyFlags & 0x1fL) != 0) {


            if ((dirtyFlags & 0x19L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.blockActionIfCountryNotSelected
                        viewModelBlockActionIfCountryNotSelected = viewModel.getBlockActionIfCountryNotSelected();
                    }
                    updateLiveDataRegistration(0, viewModelBlockActionIfCountryNotSelected);


                    if (viewModelBlockActionIfCountryNotSelected != null) {
                        // read viewModel.blockActionIfCountryNotSelected.getValue()
                        viewModelBlockActionIfCountryNotSelectedGetValue = viewModelBlockActionIfCountryNotSelected.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.blockActionIfCountryNotSelected.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelBlockActionIfCountryNotSelectedGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelBlockActionIfCountryNotSelectedGetValue);
                if((dirtyFlags & 0x19L) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxViewModelBlockActionIfCountryNotSelectedGetValue) {
                            dirtyFlags |= 0x40L;
                    }
                    else {
                            dirtyFlags |= 0x20L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.blockActionIfCountryNotSelected.getValue()) ? View.VISIBLE : View.GONE
                    viewModelBlockActionIfCountryNotSelectedViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxViewModelBlockActionIfCountryNotSelectedGetValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0x1aL) != 0) {

                    if (viewModel != null) {
                        // read viewModel.buttonText
                        viewModelButtonText = viewModel.getButtonText();
                    }
                    updateLiveDataRegistration(1, viewModelButtonText);


                    if (viewModelButtonText != null) {
                        // read viewModel.buttonText.getValue()
                        viewModelButtonTextGetValue = viewModelButtonText.getValue();
                    }
            }
            if ((dirtyFlags & 0x1cL) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isCloseButtonVisible
                        viewModelIsCloseButtonVisible = viewModel.isCloseButtonVisible();
                    }
                    updateLiveDataRegistration(2, viewModelIsCloseButtonVisible);


                    if (viewModelIsCloseButtonVisible != null) {
                        // read viewModel.isCloseButtonVisible.getValue()
                        viewModelIsCloseButtonVisibleGetValue = viewModelIsCloseButtonVisible.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isCloseButtonVisible.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelIsCloseButtonVisibleGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelIsCloseButtonVisibleGetValue);
                if((dirtyFlags & 0x1cL) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxViewModelIsCloseButtonVisibleGetValue) {
                            dirtyFlags |= 0x100L;
                    }
                    else {
                            dirtyFlags |= 0x80L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isCloseButtonVisible.getValue()) ? View.VISIBLE : View.GONE
                    viewModelIsCloseButtonVisibleViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxViewModelIsCloseButtonVisibleGetValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
        }
        // batch finished
        if ((dirtyFlags & 0x19L) != 0) {
            // api target 1

            this.mboundView3.setVisibility(viewModelBlockActionIfCountryNotSelectedViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x1cL) != 0) {
            // api target 1

            this.selectCountryCloseImage.setVisibility(viewModelIsCloseButtonVisibleViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x10L) != 0) {
            // api target 1

            this.selectCountryCloseImage.setOnClickListener(mCallback59);
            this.selectCountrySaveButton.setOnClickListener(mCallback60);
        }
        if ((dirtyFlags & 0x1aL) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.selectCountrySaveButton, viewModelButtonTextGetValue);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 1: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.SelectCountryViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.close();
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.SelectCountryViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.saveSelectedCountry();
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.blockActionIfCountryNotSelected
        flag 1 (0x2L): viewModel.buttonText
        flag 2 (0x3L): viewModel.isCloseButtonVisible
        flag 3 (0x4L): viewModel
        flag 4 (0x5L): null
        flag 5 (0x6L): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.blockActionIfCountryNotSelected.getValue()) ? View.VISIBLE : View.GONE
        flag 6 (0x7L): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.blockActionIfCountryNotSelected.getValue()) ? View.VISIBLE : View.GONE
        flag 7 (0x8L): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isCloseButtonVisible.getValue()) ? View.VISIBLE : View.GONE
        flag 8 (0x9L): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isCloseButtonVisible.getValue()) ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}