package com.impaktsoft.globeup.databinding;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FriendTextMessageCellBindingImpl extends FriendTextMessageCellBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final android.widget.TextView mboundView4;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FriendTextMessageCellBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 5, sIncludes, sViewsWithIds));
    }
    private FriendTextMessageCellBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.RelativeLayout) bindings[0]
            , (android.widget.TextView) bindings[3]
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[1]
            , (android.widget.TextView) bindings[2]
            );
        this.layout1.setTag(null);
        this.mboundView4 = (android.widget.TextView) bindings[4];
        this.mboundView4.setTag(null);
        this.textContentUser.setTag(null);
        this.userImage.setTag(null);
        this.userName.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.messageItem == variableId) {
            setMessageItem((com.impaktsoft.globeup.models.MessagePOJO) variable);
        }
        else if (BR.userInfo == variableId) {
            setUserInfo((com.impaktsoft.globeup.models.UserInfo) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setMessageItem(@Nullable com.impaktsoft.globeup.models.MessagePOJO MessageItem) {
        this.mMessageItem = MessageItem;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.messageItem);
        super.requestRebind();
    }
    public void setUserInfo(@Nullable com.impaktsoft.globeup.models.UserInfo UserInfo) {
        this.mUserInfo = UserInfo;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.userInfo);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String userInfoName = null;
        com.impaktsoft.globeup.models.MessagePOJO messageItem = mMessageItem;
        com.impaktsoft.globeup.models.UserInfo userInfo = mUserInfo;
        java.lang.String userInfoProfileImage = null;

        if ((dirtyFlags & 0x5L) != 0) {
        }
        if ((dirtyFlags & 0x6L) != 0) {



                if (userInfo != null) {
                    // read userInfo.name
                    userInfoName = userInfo.getName();
                    // read userInfo.profileImage
                    userInfoProfileImage = userInfo.getProfileImage();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.TextViewBindingAdapters.setConversationTimeText(this.mboundView4, messageItem);
            com.impaktsoft.globeup.bindingadapters.TextViewBindingAdapters.setMessageText(this.textContentUser, messageItem);
        }
        if ((dirtyFlags & 0x6L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.ImageViewBindingAdapters.imageUrl(this.userImage, userInfoProfileImage, androidx.appcompat.content.res.AppCompatResources.getDrawable(userImage.getContext(), R.drawable.profile_filled_svg));
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.userName, userInfoName);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): messageItem
        flag 1 (0x2L): userInfo
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}