package com.impaktsoft.globeup.databinding;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityEditEventBindingImpl extends ActivityEditEventBinding implements com.impaktsoft.globeup.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    @NonNull
    private final android.widget.EditText mboundView2;
    @NonNull
    private final android.widget.EditText mboundView3;
    @NonNull
    private final android.widget.Spinner mboundView4;
    @NonNull
    private final android.widget.DatePicker mboundView5;
    @NonNull
    private final android.widget.TimePicker mboundView6;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback12;
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener mapWithPingcbGetPositionAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.eventLocation.getValue()
            //         is viewModel.eventLocation.setValue((com.google.android.gms.maps.model.LatLng) callbackArg_0)
            com.google.android.gms.maps.model.LatLng callbackArg_0 = com.impaktsoft.globeup.bindingadapters.MapBindingAdapters.getMyPosition(mapWithPing);
            // localize variables for thread safety
            // viewModel.eventLocation != null
            boolean viewModelEventLocationJavaLangObjectNull = false;
            // viewModel.eventLocation.getValue()
            com.google.android.gms.maps.model.LatLng viewModelEventLocationGetValue = null;
            // viewModel
            com.impaktsoft.globeup.viewmodels.EditEventViewModel viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.eventLocation
            androidx.lifecycle.MutableLiveData<com.google.android.gms.maps.model.LatLng> viewModelEventLocation = null;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelEventLocation = viewModel.getEventLocation();

                viewModelEventLocationJavaLangObjectNull = (viewModelEventLocation) != (null);
                if (viewModelEventLocationJavaLangObjectNull) {




                    viewModelEventLocation.setValue(((com.google.android.gms.maps.model.LatLng) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView2androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.eventName.getValue()
            //         is viewModel.eventName.setValue((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView2);
            // localize variables for thread safety
            // viewModel.eventName.getValue()
            java.lang.String viewModelEventNameGetValue = null;
            // viewModel.eventName != null
            boolean viewModelEventNameJavaLangObjectNull = false;
            // viewModel.eventName
            androidx.lifecycle.MutableLiveData<java.lang.String> viewModelEventName = null;
            // viewModel
            com.impaktsoft.globeup.viewmodels.EditEventViewModel viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelEventName = viewModel.getEventName();

                viewModelEventNameJavaLangObjectNull = (viewModelEventName) != (null);
                if (viewModelEventNameJavaLangObjectNull) {




                    viewModelEventName.setValue(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView3androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.eventDescription.getValue()
            //         is viewModel.eventDescription.setValue((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView3);
            // localize variables for thread safety
            // viewModel.eventDescription.getValue()
            java.lang.String viewModelEventDescriptionGetValue = null;
            // viewModel.eventDescription != null
            boolean viewModelEventDescriptionJavaLangObjectNull = false;
            // viewModel
            com.impaktsoft.globeup.viewmodels.EditEventViewModel viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.eventDescription
            androidx.lifecycle.MutableLiveData<java.lang.String> viewModelEventDescription = null;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelEventDescription = viewModel.getEventDescription();

                viewModelEventDescriptionJavaLangObjectNull = (viewModelEventDescription) != (null);
                if (viewModelEventDescriptionJavaLangObjectNull) {




                    viewModelEventDescription.setValue(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView4cbSelectedValueAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.eventType.getValue()
            //         is viewModel.eventType.setValue((com.impaktsoft.globeup.enums.EventType) callbackArg_0)
            java.lang.Object callbackArg_0 = com.impaktsoft.globeup.bindingadapters.SpinnerViewBindingAdapters.getSelectedValue(mboundView4);
            // localize variables for thread safety
            // viewModel.eventType
            androidx.lifecycle.MutableLiveData<com.impaktsoft.globeup.enums.EventType> viewModelEventType = null;
            // viewModel.eventType.getValue()
            com.impaktsoft.globeup.enums.EventType viewModelEventTypeGetValue = null;
            // viewModel
            com.impaktsoft.globeup.viewmodels.EditEventViewModel viewModel = mViewModel;
            // viewModel.eventType != null
            boolean viewModelEventTypeJavaLangObjectNull = false;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelEventType = viewModel.getEventType();

                viewModelEventTypeJavaLangObjectNull = (viewModelEventType) != (null);
                if (viewModelEventTypeJavaLangObjectNull) {




                    viewModelEventType.setValue(((com.impaktsoft.globeup.enums.EventType) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView5cbGetDateAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.datePickerCalendar.getValue()
            //         is viewModel.datePickerCalendar.setValue((java.util.Calendar) callbackArg_0)
            java.util.Calendar callbackArg_0 = com.impaktsoft.globeup.bindingadapters.DatePickerBindingAdapter.getMyDate(mboundView5);
            // localize variables for thread safety
            // viewModel.datePickerCalendar != null
            boolean viewModelDatePickerCalendarJavaLangObjectNull = false;
            // viewModel
            com.impaktsoft.globeup.viewmodels.EditEventViewModel viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.datePickerCalendar
            androidx.lifecycle.MutableLiveData<java.util.Calendar> viewModelDatePickerCalendar = null;
            // viewModel.datePickerCalendar.getValue()
            java.util.Calendar viewModelDatePickerCalendarGetValue = null;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelDatePickerCalendar = viewModel.getDatePickerCalendar();

                viewModelDatePickerCalendarJavaLangObjectNull = (viewModelDatePickerCalendar) != (null);
                if (viewModelDatePickerCalendarJavaLangObjectNull) {




                    viewModelDatePickerCalendar.setValue(((java.util.Calendar) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView6cbGetTimeAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.timePickerCalendar.getValue()
            //         is viewModel.timePickerCalendar.setValue((java.util.Calendar) callbackArg_0)
            java.util.Calendar callbackArg_0 = com.impaktsoft.globeup.bindingadapters.TimePickerBindingAdapter.getMyTime(mboundView6);
            // localize variables for thread safety
            // viewModel.timePickerCalendar
            androidx.lifecycle.MutableLiveData<java.util.Calendar> viewModelTimePickerCalendar = null;
            // viewModel.timePickerCalendar.getValue()
            java.util.Calendar viewModelTimePickerCalendarGetValue = null;
            // viewModel
            com.impaktsoft.globeup.viewmodels.EditEventViewModel viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.timePickerCalendar != null
            boolean viewModelTimePickerCalendarJavaLangObjectNull = false;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelTimePickerCalendar = viewModel.getTimePickerCalendar();

                viewModelTimePickerCalendarJavaLangObjectNull = (viewModelTimePickerCalendar) != (null);
                if (viewModelTimePickerCalendarJavaLangObjectNull) {




                    viewModelTimePickerCalendar.setValue(((java.util.Calendar) (callbackArg_0)));
                }
            }
        }
    };

    public ActivityEditEventBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 10, sIncludes, sViewsWithIds));
    }
    private ActivityEditEventBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 8
            , (androidx.viewpager.widget.ViewPager) bindings[1]
            , (android.widget.RelativeLayout) bindings[9]
            , (com.impaktsoft.globeup.components.MapViewWithPin) bindings[7]
            , (android.widget.Button) bindings[8]
            );
        this.imagesViewPager.setTag(null);
        this.loadingView.setTag(null);
        this.mapWithPing.setTag(null);
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView2 = (android.widget.EditText) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (android.widget.EditText) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView4 = (android.widget.Spinner) bindings[4];
        this.mboundView4.setTag(null);
        this.mboundView5 = (android.widget.DatePicker) bindings[5];
        this.mboundView5.setTag(null);
        this.mboundView6 = (android.widget.TimePicker) bindings[6];
        this.mboundView6.setTag(null);
        this.saveButton.setTag(null);
        setRootTag(root);
        // listeners
        mCallback12 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x200L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((com.impaktsoft.globeup.viewmodels.EditEventViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable com.impaktsoft.globeup.viewmodels.EditEventViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x100L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelEventName((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 1 :
                return onChangeViewModelCurrentImageIndex((androidx.lifecycle.MutableLiveData<java.lang.Integer>) object, fieldId);
            case 2 :
                return onChangeViewModelEventType((androidx.lifecycle.MutableLiveData<com.impaktsoft.globeup.enums.EventType>) object, fieldId);
            case 3 :
                return onChangeViewModelEventDescription((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 4 :
                return onChangeViewModelDatePickerCalendar((androidx.lifecycle.MutableLiveData<java.util.Calendar>) object, fieldId);
            case 5 :
                return onChangeViewModelEventLocation((androidx.lifecycle.MutableLiveData<com.google.android.gms.maps.model.LatLng>) object, fieldId);
            case 6 :
                return onChangeViewModelTimePickerCalendar((androidx.lifecycle.MutableLiveData<java.util.Calendar>) object, fieldId);
            case 7 :
                return onChangeViewModelIsLoading((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelEventName(androidx.lifecycle.MutableLiveData<java.lang.String> ViewModelEventName, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelCurrentImageIndex(androidx.lifecycle.MutableLiveData<java.lang.Integer> ViewModelCurrentImageIndex, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelEventType(androidx.lifecycle.MutableLiveData<com.impaktsoft.globeup.enums.EventType> ViewModelEventType, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelEventDescription(androidx.lifecycle.MutableLiveData<java.lang.String> ViewModelEventDescription, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelDatePickerCalendar(androidx.lifecycle.MutableLiveData<java.util.Calendar> ViewModelDatePickerCalendar, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelEventLocation(androidx.lifecycle.MutableLiveData<com.google.android.gms.maps.model.LatLng> ViewModelEventLocation, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x20L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelTimePickerCalendar(androidx.lifecycle.MutableLiveData<java.util.Calendar> ViewModelTimePickerCalendar, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x40L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsLoading(androidx.lifecycle.MutableLiveData<java.lang.Boolean> ViewModelIsLoading, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x80L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        boolean androidxDatabindingViewDataBindingSafeUnboxViewModelIsLoadingGetValue = false;
        java.util.ArrayList<java.lang.Integer> viewModelImages = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> viewModelEventName = null;
        androidx.lifecycle.MutableLiveData<java.lang.Integer> viewModelCurrentImageIndex = null;
        java.lang.String viewModelEventNameGetValue = null;
        java.util.Calendar viewModelDatePickerCalendarGetValue = null;
        java.util.List<com.impaktsoft.globeup.enums.EventType> viewModelSpinnerEntries = null;
        java.lang.String viewModelEventDescriptionGetValue = null;
        java.util.Calendar viewModelTimePickerCalendarGetValue = null;
        androidx.lifecycle.MutableLiveData<com.impaktsoft.globeup.enums.EventType> viewModelEventType = null;
        com.impaktsoft.globeup.enums.EventType viewModelEventTypeGetValue = null;
        com.google.android.gms.maps.model.LatLng viewModelEventLocationGetValue = null;
        int viewModelIsLoadingViewVISIBLEViewGONE = 0;
        java.lang.Boolean viewModelIsLoadingGetValue = null;
        java.lang.Integer viewModelCurrentImageIndexGetValue = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> viewModelEventDescription = null;
        androidx.lifecycle.MutableLiveData<java.util.Calendar> viewModelDatePickerCalendar = null;
        androidx.lifecycle.MutableLiveData<com.google.android.gms.maps.model.LatLng> viewModelEventLocation = null;
        androidx.lifecycle.MutableLiveData<java.util.Calendar> viewModelTimePickerCalendar = null;
        int androidxDatabindingViewDataBindingSafeUnboxViewModelCurrentImageIndexGetValue = 0;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> viewModelIsLoading = null;
        com.impaktsoft.globeup.viewmodels.EditEventViewModel viewModel = mViewModel;

        if ((dirtyFlags & 0x3ffL) != 0) {


            if ((dirtyFlags & 0x300L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.images
                        viewModelImages = viewModel.getImages();
                        // read viewModel.spinnerEntries
                        viewModelSpinnerEntries = viewModel.getSpinnerEntries();
                    }
            }
            if ((dirtyFlags & 0x301L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.eventName
                        viewModelEventName = viewModel.getEventName();
                    }
                    updateLiveDataRegistration(0, viewModelEventName);


                    if (viewModelEventName != null) {
                        // read viewModel.eventName.getValue()
                        viewModelEventNameGetValue = viewModelEventName.getValue();
                    }
            }
            if ((dirtyFlags & 0x302L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.currentImageIndex
                        viewModelCurrentImageIndex = viewModel.getCurrentImageIndex();
                    }
                    updateLiveDataRegistration(1, viewModelCurrentImageIndex);


                    if (viewModelCurrentImageIndex != null) {
                        // read viewModel.currentImageIndex.getValue()
                        viewModelCurrentImageIndexGetValue = viewModelCurrentImageIndex.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.currentImageIndex.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelCurrentImageIndexGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelCurrentImageIndexGetValue);
            }
            if ((dirtyFlags & 0x304L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.eventType
                        viewModelEventType = viewModel.getEventType();
                    }
                    updateLiveDataRegistration(2, viewModelEventType);


                    if (viewModelEventType != null) {
                        // read viewModel.eventType.getValue()
                        viewModelEventTypeGetValue = viewModelEventType.getValue();
                    }
            }
            if ((dirtyFlags & 0x308L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.eventDescription
                        viewModelEventDescription = viewModel.getEventDescription();
                    }
                    updateLiveDataRegistration(3, viewModelEventDescription);


                    if (viewModelEventDescription != null) {
                        // read viewModel.eventDescription.getValue()
                        viewModelEventDescriptionGetValue = viewModelEventDescription.getValue();
                    }
            }
            if ((dirtyFlags & 0x310L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.datePickerCalendar
                        viewModelDatePickerCalendar = viewModel.getDatePickerCalendar();
                    }
                    updateLiveDataRegistration(4, viewModelDatePickerCalendar);


                    if (viewModelDatePickerCalendar != null) {
                        // read viewModel.datePickerCalendar.getValue()
                        viewModelDatePickerCalendarGetValue = viewModelDatePickerCalendar.getValue();
                    }
            }
            if ((dirtyFlags & 0x320L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.eventLocation
                        viewModelEventLocation = viewModel.getEventLocation();
                    }
                    updateLiveDataRegistration(5, viewModelEventLocation);


                    if (viewModelEventLocation != null) {
                        // read viewModel.eventLocation.getValue()
                        viewModelEventLocationGetValue = viewModelEventLocation.getValue();
                    }
            }
            if ((dirtyFlags & 0x340L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.timePickerCalendar
                        viewModelTimePickerCalendar = viewModel.getTimePickerCalendar();
                    }
                    updateLiveDataRegistration(6, viewModelTimePickerCalendar);


                    if (viewModelTimePickerCalendar != null) {
                        // read viewModel.timePickerCalendar.getValue()
                        viewModelTimePickerCalendarGetValue = viewModelTimePickerCalendar.getValue();
                    }
            }
            if ((dirtyFlags & 0x380L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isLoading
                        viewModelIsLoading = viewModel.isLoading();
                    }
                    updateLiveDataRegistration(7, viewModelIsLoading);


                    if (viewModelIsLoading != null) {
                        // read viewModel.isLoading.getValue()
                        viewModelIsLoadingGetValue = viewModelIsLoading.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isLoading.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelIsLoadingGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelIsLoadingGetValue);
                if((dirtyFlags & 0x380L) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxViewModelIsLoadingGetValue) {
                            dirtyFlags |= 0x800L;
                    }
                    else {
                            dirtyFlags |= 0x400L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isLoading.getValue()) ? View.VISIBLE : View.GONE
                    viewModelIsLoadingViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxViewModelIsLoadingGetValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
        }
        // batch finished
        if ((dirtyFlags & 0x302L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.ViewPagerBindingAdapter.swipeTo(this.imagesViewPager, androidxDatabindingViewDataBindingSafeUnboxViewModelCurrentImageIndexGetValue);
            com.impaktsoft.globeup.bindingadapters.ViewPagerBindingAdapter.getCurrentItem(this.imagesViewPager, viewModelCurrentImageIndex);
        }
        if ((dirtyFlags & 0x300L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.ViewPagerBindingAdapter.setAdapter(this.imagesViewPager, viewModelImages);
            com.impaktsoft.globeup.bindingadapters.SpinnerViewBindingAdapters.setEntries(this.mboundView4, viewModelSpinnerEntries);
        }
        if ((dirtyFlags & 0x380L) != 0) {
            // api target 1

            this.loadingView.setVisibility(viewModelIsLoadingViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x320L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.MapBindingAdapters.setMyPosition(this.mapWithPing, viewModelEventLocationGetValue);
        }
        if ((dirtyFlags & 0x200L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.MapBindingAdapters.setListener(this.mapWithPing, mapWithPingcbGetPositionAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView2, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView2androidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView3, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView3androidTextAttrChanged);
            com.impaktsoft.globeup.bindingadapters.SpinnerViewBindingAdapters.setInverseBindingListener(this.mboundView4, mboundView4cbSelectedValueAttrChanged);
            com.impaktsoft.globeup.bindingadapters.DatePickerBindingAdapter.setListener(this.mboundView5, mboundView5cbGetDateAttrChanged);
            com.impaktsoft.globeup.bindingadapters.TimePickerBindingAdapter.setListener(this.mboundView6, mboundView6cbGetTimeAttrChanged);
            this.saveButton.setOnClickListener(mCallback12);
        }
        if ((dirtyFlags & 0x301L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, viewModelEventNameGetValue);
        }
        if ((dirtyFlags & 0x308L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, viewModelEventDescriptionGetValue);
        }
        if ((dirtyFlags & 0x304L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.SpinnerViewBindingAdapters.setSelectedValue(this.mboundView4, viewModelEventTypeGetValue);
        }
        if ((dirtyFlags & 0x310L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.DatePickerBindingAdapter.setMyDate(this.mboundView5, viewModelDatePickerCalendarGetValue);
        }
        if ((dirtyFlags & 0x340L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.TimePickerBindingAdapter.setMyTime(this.mboundView6, viewModelTimePickerCalendarGetValue);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // viewModel
        com.impaktsoft.globeup.viewmodels.EditEventViewModel viewModel = mViewModel;
        // viewModel != null
        boolean viewModelJavaLangObjectNull = false;



        viewModelJavaLangObjectNull = (viewModel) != (null);
        if (viewModelJavaLangObjectNull) {


            viewModel.editClick();
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.eventName
        flag 1 (0x2L): viewModel.currentImageIndex
        flag 2 (0x3L): viewModel.eventType
        flag 3 (0x4L): viewModel.eventDescription
        flag 4 (0x5L): viewModel.datePickerCalendar
        flag 5 (0x6L): viewModel.eventLocation
        flag 6 (0x7L): viewModel.timePickerCalendar
        flag 7 (0x8L): viewModel.isLoading
        flag 8 (0x9L): viewModel
        flag 9 (0xaL): null
        flag 10 (0xbL): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isLoading.getValue()) ? View.VISIBLE : View.GONE
        flag 11 (0xcL): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isLoading.getValue()) ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}