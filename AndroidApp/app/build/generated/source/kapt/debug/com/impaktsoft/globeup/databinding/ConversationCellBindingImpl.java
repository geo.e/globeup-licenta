package com.impaktsoft.globeup.databinding;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ConversationCellBindingImpl extends ConversationCellBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.conversation_layout_image, 6);
    }
    // views
    @NonNull
    private final android.widget.TextView mboundView2;
    @NonNull
    private final android.widget.TextView mboundView3;
    @NonNull
    private final android.widget.TextView mboundView4;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ConversationCellBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 7, sIncludes, sViewsWithIds));
    }
    private ConversationCellBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.RelativeLayout) bindings[0]
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[1]
            , (android.widget.RelativeLayout) bindings[6]
            , (android.widget.ImageView) bindings[5]
            );
        this.conversationCellLayout.setTag(null);
        this.conversationImage.setTag(null);
        this.mboundView2 = (android.widget.TextView) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (android.widget.TextView) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView4 = (android.widget.TextView) bindings[4];
        this.mboundView4.setTag(null);
        this.notificationIcon.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.isMessageSeen == variableId) {
            setIsMessageSeen((java.lang.Boolean) variable);
        }
        else if (BR.itemConversation == variableId) {
            setItemConversation((com.impaktsoft.globeup.models.ConversationPOJO) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setIsMessageSeen(@Nullable java.lang.Boolean IsMessageSeen) {
        this.mIsMessageSeen = IsMessageSeen;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.isMessageSeen);
        super.requestRebind();
    }
    public void setItemConversation(@Nullable com.impaktsoft.globeup.models.ConversationPOJO ItemConversation) {
        this.mItemConversation = ItemConversation;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.itemConversation);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.Integer itemConversationConversationImage = null;
        boolean bindingConvertMessageIsReadItemConversation = false;
        boolean androidxDatabindingViewDataBindingSafeUnboxIsMessageSeen = false;
        java.lang.Boolean isMessageSeen = mIsMessageSeen;
        java.lang.String itemConversationName = null;
        int androidxDatabindingViewDataBindingSafeUnboxItemConversationConversationImage = 0;
        com.impaktsoft.globeup.models.ConversationPOJO itemConversation = mItemConversation;
        int bindingConvertMessageIsReadItemConversationViewVISIBLEViewGONE = 0;
        com.impaktsoft.globeup.models.MessagePOJO itemConversationLastMessage = null;

        if ((dirtyFlags & 0x5L) != 0) {



                // read androidx.databinding.ViewDataBinding.safeUnbox(isMessageSeen)
                androidxDatabindingViewDataBindingSafeUnboxIsMessageSeen = androidx.databinding.ViewDataBinding.safeUnbox(isMessageSeen);
        }
        if ((dirtyFlags & 0x6L) != 0) {



                if (itemConversation != null) {
                    // read itemConversation.conversationImage
                    itemConversationConversationImage = itemConversation.getConversationImage();
                    // read itemConversation.name
                    itemConversationName = itemConversation.getName();
                    // read itemConversation.lastMessage
                    itemConversationLastMessage = itemConversation.getLastMessage();
                }
                // read BindingConvert.messageIsRead(itemConversation)
                bindingConvertMessageIsReadItemConversation = com.impaktsoft.globeup.bindingadapters.BindingConvert.messageIsRead(itemConversation);
            if((dirtyFlags & 0x6L) != 0) {
                if(bindingConvertMessageIsReadItemConversation) {
                        dirtyFlags |= 0x10L;
                }
                else {
                        dirtyFlags |= 0x8L;
                }
            }


                // read androidx.databinding.ViewDataBinding.safeUnbox(itemConversation.conversationImage)
                androidxDatabindingViewDataBindingSafeUnboxItemConversationConversationImage = androidx.databinding.ViewDataBinding.safeUnbox(itemConversationConversationImage);
                // read BindingConvert.messageIsRead(itemConversation) ? View.VISIBLE : View.GONE
                bindingConvertMessageIsReadItemConversationViewVISIBLEViewGONE = ((bindingConvertMessageIsReadItemConversation) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
        }
        // batch finished
        if ((dirtyFlags & 0x6L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.ImageViewBindingAdapters.setImage(this.conversationImage, androidxDatabindingViewDataBindingSafeUnboxItemConversationConversationImage);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, itemConversationName);
            com.impaktsoft.globeup.bindingadapters.TextViewBindingAdapters.setConversationLastMessageText(this.mboundView3, itemConversationLastMessage);
            com.impaktsoft.globeup.bindingadapters.TextViewBindingAdapters.setConversationTimeText(this.mboundView4, itemConversationLastMessage);
            this.notificationIcon.setVisibility(bindingConvertMessageIsReadItemConversationViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.TextViewBindingAdapters.setTextColor(this.mboundView3, androidxDatabindingViewDataBindingSafeUnboxIsMessageSeen);
            com.impaktsoft.globeup.bindingadapters.TextViewBindingAdapters.setTextStyle(this.mboundView3, androidxDatabindingViewDataBindingSafeUnboxIsMessageSeen);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): isMessageSeen
        flag 1 (0x2L): itemConversation
        flag 2 (0x3L): null
        flag 3 (0x4L): BindingConvert.messageIsRead(itemConversation) ? View.VISIBLE : View.GONE
        flag 4 (0x5L): BindingConvert.messageIsRead(itemConversation) ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}