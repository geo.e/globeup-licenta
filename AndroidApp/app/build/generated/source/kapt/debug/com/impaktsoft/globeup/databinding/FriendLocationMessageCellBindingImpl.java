package com.impaktsoft.globeup.databinding;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FriendLocationMessageCellBindingImpl extends FriendLocationMessageCellBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.locationLayout, 5);
        sViewsWithIds.put(R.id.imageContentFriend, 6);
    }
    // views
    @NonNull
    private final android.widget.TextView mboundView4;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FriendLocationMessageCellBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 7, sIncludes, sViewsWithIds));
    }
    private FriendLocationMessageCellBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.cardview.widget.CardView) bindings[6]
            , (android.widget.RelativeLayout) bindings[0]
            , (android.widget.ImageView) bindings[3]
            , (android.widget.RelativeLayout) bindings[5]
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[1]
            , (android.widget.TextView) bindings[2]
            );
        this.locationCellLayout.setTag(null);
        this.locationImage.setTag(null);
        this.mboundView4 = (android.widget.TextView) bindings[4];
        this.mboundView4.setTag(null);
        this.userImage.setTag(null);
        this.userName.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.userInfo == variableId) {
            setUserInfo((com.impaktsoft.globeup.models.UserInfo) variable);
        }
        else if (BR.messageItem == variableId) {
            setMessageItem((com.impaktsoft.globeup.models.MessagePOJO) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setUserInfo(@Nullable com.impaktsoft.globeup.models.UserInfo UserInfo) {
        this.mUserInfo = UserInfo;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.userInfo);
        super.requestRebind();
    }
    public void setMessageItem(@Nullable com.impaktsoft.globeup.models.MessagePOJO MessageItem) {
        this.mMessageItem = MessageItem;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.messageItem);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        com.impaktsoft.globeup.models.UserInfo userInfo = mUserInfo;
        com.impaktsoft.globeup.models.MessagePOJO messageItem = mMessageItem;
        java.lang.String userInfoProfileImage = null;
        java.lang.String userInfoName = null;

        if ((dirtyFlags & 0x5L) != 0) {



                if (userInfo != null) {
                    // read userInfo.profileImage
                    userInfoProfileImage = userInfo.getProfileImage();
                    // read userInfo.name
                    userInfoName = userInfo.getName();
                }
        }
        if ((dirtyFlags & 0x6L) != 0) {
        }
        // batch finished
        if ((dirtyFlags & 0x6L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.ImageViewBindingAdapters.messageImageUrl(this.locationImage, messageItem);
            com.impaktsoft.globeup.bindingadapters.TextViewBindingAdapters.setConversationTimeText(this.mboundView4, messageItem);
        }
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.ImageViewBindingAdapters.imageUrl(this.userImage, userInfoProfileImage, androidx.appcompat.content.res.AppCompatResources.getDrawable(userImage.getContext(), R.drawable.profile_filled_svg));
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.userName, userInfoName);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): userInfo
        flag 1 (0x2L): messageItem
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}