package com.impaktsoft.globeup;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.impaktsoft.globeup.databinding.ActivityAddEventBindingImpl;
import com.impaktsoft.globeup.databinding.ActivityBuyGcoinBindingImpl;
import com.impaktsoft.globeup.databinding.ActivityContactsBindingImpl;
import com.impaktsoft.globeup.databinding.ActivityDonationBindingImpl;
import com.impaktsoft.globeup.databinding.ActivityEditEventBindingImpl;
import com.impaktsoft.globeup.databinding.ActivityEventBindingImpl;
import com.impaktsoft.globeup.databinding.ActivityExperienceBindingImpl;
import com.impaktsoft.globeup.databinding.ActivityGetCouponBindingImpl;
import com.impaktsoft.globeup.databinding.ActivityInitUserBindingImpl;
import com.impaktsoft.globeup.databinding.ActivityLoginBindingImpl;
import com.impaktsoft.globeup.databinding.ActivityPreviewImageBindingImpl;
import com.impaktsoft.globeup.databinding.ActivitySearchEventsBindingImpl;
import com.impaktsoft.globeup.databinding.ActivitySettingsBindingImpl;
import com.impaktsoft.globeup.databinding.ActivityWithdrawBindingImpl;
import com.impaktsoft.globeup.databinding.ContactsCellBindingImpl;
import com.impaktsoft.globeup.databinding.ContactsForSelectCellBindingImpl;
import com.impaktsoft.globeup.databinding.ConversationCellBindingImpl;
import com.impaktsoft.globeup.databinding.CountryCellBindingImpl;
import com.impaktsoft.globeup.databinding.DashboardEventCellBindingImpl;
import com.impaktsoft.globeup.databinding.EventCellBindingImpl;
import com.impaktsoft.globeup.databinding.EventCellSectionBindingImpl;
import com.impaktsoft.globeup.databinding.ExperienceCellBindingImpl;
import com.impaktsoft.globeup.databinding.ExperienceCellLayoutBindingImpl;
import com.impaktsoft.globeup.databinding.FeedbackCellBindingImpl;
import com.impaktsoft.globeup.databinding.FilterAppliedCellBindingImpl;
import com.impaktsoft.globeup.databinding.FilterEventCellBindingImpl;
import com.impaktsoft.globeup.databinding.FloatingButtonOptionCellBindingImpl;
import com.impaktsoft.globeup.databinding.FragmentChooseContactsBindingImpl;
import com.impaktsoft.globeup.databinding.FragmentCreateGroupBindingImpl;
import com.impaktsoft.globeup.databinding.FragmentDashboardBindingImpl;
import com.impaktsoft.globeup.databinding.FragmentDeleteAccountBindingImpl;
import com.impaktsoft.globeup.databinding.FragmentDonationDoneBindingImpl;
import com.impaktsoft.globeup.databinding.FragmentEditProfileBindingImpl;
import com.impaktsoft.globeup.databinding.FragmentEventsBindingImpl;
import com.impaktsoft.globeup.databinding.FragmentFeedbackBindingImpl;
import com.impaktsoft.globeup.databinding.FragmentFilterEventsBindingImpl;
import com.impaktsoft.globeup.databinding.FragmentFilterEventsByRadiusBindingImpl;
import com.impaktsoft.globeup.databinding.FragmentForgetPasswordBindingImpl;
import com.impaktsoft.globeup.databinding.FragmentGlobalBindingImpl;
import com.impaktsoft.globeup.databinding.FragmentImageCaptureBindingImpl;
import com.impaktsoft.globeup.databinding.FragmentMessagesBindingImpl;
import com.impaktsoft.globeup.databinding.FragmentMessengerBindingImpl;
import com.impaktsoft.globeup.databinding.FragmentSelectCountryBindingImpl;
import com.impaktsoft.globeup.databinding.FragmentSelectGroupMembersBindingImpl;
import com.impaktsoft.globeup.databinding.FragmentSettingsBindingImpl;
import com.impaktsoft.globeup.databinding.FragmentWalletBindingImpl;
import com.impaktsoft.globeup.databinding.FriendImageMessageCellBindingImpl;
import com.impaktsoft.globeup.databinding.FriendLocationMessageCellBindingImpl;
import com.impaktsoft.globeup.databinding.FriendTextMessageCellBindingImpl;
import com.impaktsoft.globeup.databinding.GcoinOffertCellBindingImpl;
import com.impaktsoft.globeup.databinding.GlobalDonationGroupCellBindingImpl;
import com.impaktsoft.globeup.databinding.GlobalDonationUserCellBindingImpl;
import com.impaktsoft.globeup.databinding.GlobalSectionCellBindingImpl;
import com.impaktsoft.globeup.databinding.LevelCellBindingImpl;
import com.impaktsoft.globeup.databinding.MessangerTimeSectionBindingImpl;
import com.impaktsoft.globeup.databinding.NewMessageNotificationCellBindingImpl;
import com.impaktsoft.globeup.databinding.RegisterActivityBindingImpl;
import com.impaktsoft.globeup.databinding.SelectedUsersCellBindingImpl;
import com.impaktsoft.globeup.databinding.TransactionCellBindingImpl;
import com.impaktsoft.globeup.databinding.UserImageMessageCellBindingImpl;
import com.impaktsoft.globeup.databinding.UserLocationMessageCellBindingImpl;
import com.impaktsoft.globeup.databinding.UserMessageStateBindingImpl;
import com.impaktsoft.globeup.databinding.UserTextMessageCellBindingImpl;
import com.impaktsoft.globeup.databinding.WithdrawCellBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_ACTIVITYADDEVENT = 1;

  private static final int LAYOUT_ACTIVITYBUYGCOIN = 2;

  private static final int LAYOUT_ACTIVITYCONTACTS = 3;

  private static final int LAYOUT_ACTIVITYDONATION = 4;

  private static final int LAYOUT_ACTIVITYEDITEVENT = 5;

  private static final int LAYOUT_ACTIVITYEVENT = 6;

  private static final int LAYOUT_ACTIVITYEXPERIENCE = 7;

  private static final int LAYOUT_ACTIVITYGETCOUPON = 8;

  private static final int LAYOUT_ACTIVITYINITUSER = 9;

  private static final int LAYOUT_ACTIVITYLOGIN = 10;

  private static final int LAYOUT_ACTIVITYPREVIEWIMAGE = 11;

  private static final int LAYOUT_ACTIVITYSEARCHEVENTS = 12;

  private static final int LAYOUT_ACTIVITYSETTINGS = 13;

  private static final int LAYOUT_ACTIVITYWITHDRAW = 14;

  private static final int LAYOUT_CONTACTSCELL = 15;

  private static final int LAYOUT_CONTACTSFORSELECTCELL = 16;

  private static final int LAYOUT_CONVERSATIONCELL = 17;

  private static final int LAYOUT_COUNTRYCELL = 18;

  private static final int LAYOUT_DASHBOARDEVENTCELL = 19;

  private static final int LAYOUT_EVENTCELL = 20;

  private static final int LAYOUT_EVENTCELLSECTION = 21;

  private static final int LAYOUT_EXPERIENCECELL = 22;

  private static final int LAYOUT_EXPERIENCECELLLAYOUT = 23;

  private static final int LAYOUT_FEEDBACKCELL = 24;

  private static final int LAYOUT_FILTERAPPLIEDCELL = 25;

  private static final int LAYOUT_FILTEREVENTCELL = 26;

  private static final int LAYOUT_FLOATINGBUTTONOPTIONCELL = 27;

  private static final int LAYOUT_FRAGMENTCHOOSECONTACTS = 28;

  private static final int LAYOUT_FRAGMENTCREATEGROUP = 29;

  private static final int LAYOUT_FRAGMENTDASHBOARD = 30;

  private static final int LAYOUT_FRAGMENTDELETEACCOUNT = 31;

  private static final int LAYOUT_FRAGMENTDONATIONDONE = 32;

  private static final int LAYOUT_FRAGMENTEDITPROFILE = 33;

  private static final int LAYOUT_FRAGMENTEVENTS = 34;

  private static final int LAYOUT_FRAGMENTFEEDBACK = 35;

  private static final int LAYOUT_FRAGMENTFILTEREVENTS = 36;

  private static final int LAYOUT_FRAGMENTFILTEREVENTSBYRADIUS = 37;

  private static final int LAYOUT_FRAGMENTFORGETPASSWORD = 38;

  private static final int LAYOUT_FRAGMENTGLOBAL = 39;

  private static final int LAYOUT_FRAGMENTIMAGECAPTURE = 40;

  private static final int LAYOUT_FRAGMENTMESSAGES = 41;

  private static final int LAYOUT_FRAGMENTMESSENGER = 42;

  private static final int LAYOUT_FRAGMENTSELECTCOUNTRY = 43;

  private static final int LAYOUT_FRAGMENTSELECTGROUPMEMBERS = 44;

  private static final int LAYOUT_FRAGMENTSETTINGS = 45;

  private static final int LAYOUT_FRAGMENTWALLET = 46;

  private static final int LAYOUT_FRIENDIMAGEMESSAGECELL = 47;

  private static final int LAYOUT_FRIENDLOCATIONMESSAGECELL = 48;

  private static final int LAYOUT_FRIENDTEXTMESSAGECELL = 49;

  private static final int LAYOUT_GCOINOFFERTCELL = 50;

  private static final int LAYOUT_GLOBALDONATIONGROUPCELL = 51;

  private static final int LAYOUT_GLOBALDONATIONUSERCELL = 52;

  private static final int LAYOUT_GLOBALSECTIONCELL = 53;

  private static final int LAYOUT_LEVELCELL = 54;

  private static final int LAYOUT_MESSANGERTIMESECTION = 55;

  private static final int LAYOUT_NEWMESSAGENOTIFICATIONCELL = 56;

  private static final int LAYOUT_REGISTERACTIVITY = 57;

  private static final int LAYOUT_SELECTEDUSERSCELL = 58;

  private static final int LAYOUT_TRANSACTIONCELL = 59;

  private static final int LAYOUT_USERIMAGEMESSAGECELL = 60;

  private static final int LAYOUT_USERLOCATIONMESSAGECELL = 61;

  private static final int LAYOUT_USERMESSAGESTATE = 62;

  private static final int LAYOUT_USERTEXTMESSAGECELL = 63;

  private static final int LAYOUT_WITHDRAWCELL = 64;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(64);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.activity_add_event, LAYOUT_ACTIVITYADDEVENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.activity_buy_gcoin, LAYOUT_ACTIVITYBUYGCOIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.activity_contacts, LAYOUT_ACTIVITYCONTACTS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.activity_donation, LAYOUT_ACTIVITYDONATION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.activity_edit_event, LAYOUT_ACTIVITYEDITEVENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.activity_event, LAYOUT_ACTIVITYEVENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.activity_experience, LAYOUT_ACTIVITYEXPERIENCE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.activity_get_coupon, LAYOUT_ACTIVITYGETCOUPON);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.activity_init_user, LAYOUT_ACTIVITYINITUSER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.activity_login, LAYOUT_ACTIVITYLOGIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.activity_preview_image, LAYOUT_ACTIVITYPREVIEWIMAGE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.activity_search_events, LAYOUT_ACTIVITYSEARCHEVENTS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.activity_settings, LAYOUT_ACTIVITYSETTINGS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.activity_withdraw, LAYOUT_ACTIVITYWITHDRAW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.contacts_cell, LAYOUT_CONTACTSCELL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.contacts_for_select_cell, LAYOUT_CONTACTSFORSELECTCELL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.conversation_cell, LAYOUT_CONVERSATIONCELL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.country_cell, LAYOUT_COUNTRYCELL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.dashboard_event_cell, LAYOUT_DASHBOARDEVENTCELL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.event_cell, LAYOUT_EVENTCELL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.event_cell_section, LAYOUT_EVENTCELLSECTION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.experience_cell, LAYOUT_EXPERIENCECELL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.experience_cell_layout, LAYOUT_EXPERIENCECELLLAYOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.feedback_cell, LAYOUT_FEEDBACKCELL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.filter_applied_cell, LAYOUT_FILTERAPPLIEDCELL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.filter_event_cell, LAYOUT_FILTEREVENTCELL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.floating_button_option_cell, LAYOUT_FLOATINGBUTTONOPTIONCELL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.fragment_choose_contacts, LAYOUT_FRAGMENTCHOOSECONTACTS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.fragment_create_group, LAYOUT_FRAGMENTCREATEGROUP);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.fragment_dashboard, LAYOUT_FRAGMENTDASHBOARD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.fragment_delete_account, LAYOUT_FRAGMENTDELETEACCOUNT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.fragment_donation_done, LAYOUT_FRAGMENTDONATIONDONE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.fragment_edit_profile, LAYOUT_FRAGMENTEDITPROFILE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.fragment_events, LAYOUT_FRAGMENTEVENTS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.fragment_feedback, LAYOUT_FRAGMENTFEEDBACK);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.fragment_filter_events, LAYOUT_FRAGMENTFILTEREVENTS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.fragment_filter_events_by_radius, LAYOUT_FRAGMENTFILTEREVENTSBYRADIUS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.fragment_forget_password, LAYOUT_FRAGMENTFORGETPASSWORD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.fragment_global, LAYOUT_FRAGMENTGLOBAL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.fragment_image_capture, LAYOUT_FRAGMENTIMAGECAPTURE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.fragment_messages, LAYOUT_FRAGMENTMESSAGES);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.fragment_messenger, LAYOUT_FRAGMENTMESSENGER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.fragment_select_country, LAYOUT_FRAGMENTSELECTCOUNTRY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.fragment_select_group_members, LAYOUT_FRAGMENTSELECTGROUPMEMBERS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.fragment_settings, LAYOUT_FRAGMENTSETTINGS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.fragment_wallet, LAYOUT_FRAGMENTWALLET);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.friend_image_message_cell, LAYOUT_FRIENDIMAGEMESSAGECELL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.friend_location_message_cell, LAYOUT_FRIENDLOCATIONMESSAGECELL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.friend_text_message_cell, LAYOUT_FRIENDTEXTMESSAGECELL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.gcoin_offert_cell, LAYOUT_GCOINOFFERTCELL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.global_donation_group_cell, LAYOUT_GLOBALDONATIONGROUPCELL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.global_donation_user_cell, LAYOUT_GLOBALDONATIONUSERCELL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.global_section_cell, LAYOUT_GLOBALSECTIONCELL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.level_cell, LAYOUT_LEVELCELL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.messanger_time_section, LAYOUT_MESSANGERTIMESECTION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.new_message_notification_cell, LAYOUT_NEWMESSAGENOTIFICATIONCELL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.register_activity, LAYOUT_REGISTERACTIVITY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.selected_users_cell, LAYOUT_SELECTEDUSERSCELL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.transaction_cell, LAYOUT_TRANSACTIONCELL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.user_image_message_cell, LAYOUT_USERIMAGEMESSAGECELL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.user_location_message_cell, LAYOUT_USERLOCATIONMESSAGECELL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.user_message_state, LAYOUT_USERMESSAGESTATE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.user_text_message_cell, LAYOUT_USERTEXTMESSAGECELL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.impaktsoft.globeup.R.layout.withdraw_cell, LAYOUT_WITHDRAWCELL);
  }

  private final ViewDataBinding internalGetViewDataBinding0(DataBindingComponent component,
      View view, int internalId, Object tag) {
    switch(internalId) {
      case  LAYOUT_ACTIVITYADDEVENT: {
        if ("layout/activity_add_event_0".equals(tag)) {
          return new ActivityAddEventBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_add_event is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYBUYGCOIN: {
        if ("layout/activity_buy_gcoin_0".equals(tag)) {
          return new ActivityBuyGcoinBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_buy_gcoin is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYCONTACTS: {
        if ("layout/activity_contacts_0".equals(tag)) {
          return new ActivityContactsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_contacts is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYDONATION: {
        if ("layout/activity_donation_0".equals(tag)) {
          return new ActivityDonationBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_donation is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYEDITEVENT: {
        if ("layout/activity_edit_event_0".equals(tag)) {
          return new ActivityEditEventBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_edit_event is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYEVENT: {
        if ("layout/activity_event_0".equals(tag)) {
          return new ActivityEventBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_event is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYEXPERIENCE: {
        if ("layout/activity_experience_0".equals(tag)) {
          return new ActivityExperienceBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_experience is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYGETCOUPON: {
        if ("layout/activity_get_coupon_0".equals(tag)) {
          return new ActivityGetCouponBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_get_coupon is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYINITUSER: {
        if ("layout/activity_init_user_0".equals(tag)) {
          return new ActivityInitUserBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_init_user is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYLOGIN: {
        if ("layout/activity_login_0".equals(tag)) {
          return new ActivityLoginBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_login is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYPREVIEWIMAGE: {
        if ("layout/activity_preview_image_0".equals(tag)) {
          return new ActivityPreviewImageBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_preview_image is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYSEARCHEVENTS: {
        if ("layout/activity_search_events_0".equals(tag)) {
          return new ActivitySearchEventsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_search_events is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYSETTINGS: {
        if ("layout/activity_settings_0".equals(tag)) {
          return new ActivitySettingsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_settings is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYWITHDRAW: {
        if ("layout/activity_withdraw_0".equals(tag)) {
          return new ActivityWithdrawBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_withdraw is invalid. Received: " + tag);
      }
      case  LAYOUT_CONTACTSCELL: {
        if ("layout/contacts_cell_0".equals(tag)) {
          return new ContactsCellBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for contacts_cell is invalid. Received: " + tag);
      }
      case  LAYOUT_CONTACTSFORSELECTCELL: {
        if ("layout/contacts_for_select_cell_0".equals(tag)) {
          return new ContactsForSelectCellBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for contacts_for_select_cell is invalid. Received: " + tag);
      }
      case  LAYOUT_CONVERSATIONCELL: {
        if ("layout/conversation_cell_0".equals(tag)) {
          return new ConversationCellBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for conversation_cell is invalid. Received: " + tag);
      }
      case  LAYOUT_COUNTRYCELL: {
        if ("layout/country_cell_0".equals(tag)) {
          return new CountryCellBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for country_cell is invalid. Received: " + tag);
      }
      case  LAYOUT_DASHBOARDEVENTCELL: {
        if ("layout/dashboard_event_cell_0".equals(tag)) {
          return new DashboardEventCellBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for dashboard_event_cell is invalid. Received: " + tag);
      }
      case  LAYOUT_EVENTCELL: {
        if ("layout/event_cell_0".equals(tag)) {
          return new EventCellBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for event_cell is invalid. Received: " + tag);
      }
      case  LAYOUT_EVENTCELLSECTION: {
        if ("layout/event_cell_section_0".equals(tag)) {
          return new EventCellSectionBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for event_cell_section is invalid. Received: " + tag);
      }
      case  LAYOUT_EXPERIENCECELL: {
        if ("layout/experience_cell_0".equals(tag)) {
          return new ExperienceCellBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for experience_cell is invalid. Received: " + tag);
      }
      case  LAYOUT_EXPERIENCECELLLAYOUT: {
        if ("layout/experience_cell_layout_0".equals(tag)) {
          return new ExperienceCellLayoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for experience_cell_layout is invalid. Received: " + tag);
      }
      case  LAYOUT_FEEDBACKCELL: {
        if ("layout/feedback_cell_0".equals(tag)) {
          return new FeedbackCellBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for feedback_cell is invalid. Received: " + tag);
      }
      case  LAYOUT_FILTERAPPLIEDCELL: {
        if ("layout/filter_applied_cell_0".equals(tag)) {
          return new FilterAppliedCellBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for filter_applied_cell is invalid. Received: " + tag);
      }
      case  LAYOUT_FILTEREVENTCELL: {
        if ("layout/filter_event_cell_0".equals(tag)) {
          return new FilterEventCellBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for filter_event_cell is invalid. Received: " + tag);
      }
      case  LAYOUT_FLOATINGBUTTONOPTIONCELL: {
        if ("layout/floating_button_option_cell_0".equals(tag)) {
          return new FloatingButtonOptionCellBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for floating_button_option_cell is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCHOOSECONTACTS: {
        if ("layout/fragment_choose_contacts_0".equals(tag)) {
          return new FragmentChooseContactsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_choose_contacts is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCREATEGROUP: {
        if ("layout/fragment_create_group_0".equals(tag)) {
          return new FragmentCreateGroupBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_create_group is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTDASHBOARD: {
        if ("layout/fragment_dashboard_0".equals(tag)) {
          return new FragmentDashboardBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_dashboard is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTDELETEACCOUNT: {
        if ("layout/fragment_delete_account_0".equals(tag)) {
          return new FragmentDeleteAccountBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_delete_account is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTDONATIONDONE: {
        if ("layout/fragment_donation_done_0".equals(tag)) {
          return new FragmentDonationDoneBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_donation_done is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTEDITPROFILE: {
        if ("layout/fragment_edit_profile_0".equals(tag)) {
          return new FragmentEditProfileBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_edit_profile is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTEVENTS: {
        if ("layout/fragment_events_0".equals(tag)) {
          return new FragmentEventsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_events is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTFEEDBACK: {
        if ("layout/fragment_feedback_0".equals(tag)) {
          return new FragmentFeedbackBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_feedback is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTFILTEREVENTS: {
        if ("layout/fragment_filter_events_0".equals(tag)) {
          return new FragmentFilterEventsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_filter_events is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTFILTEREVENTSBYRADIUS: {
        if ("layout/fragment_filter_events_by_radius_0".equals(tag)) {
          return new FragmentFilterEventsByRadiusBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_filter_events_by_radius is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTFORGETPASSWORD: {
        if ("layout/fragment_forget_password_0".equals(tag)) {
          return new FragmentForgetPasswordBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_forget_password is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTGLOBAL: {
        if ("layout/fragment_global_0".equals(tag)) {
          return new FragmentGlobalBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_global is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTIMAGECAPTURE: {
        if ("layout/fragment_image_capture_0".equals(tag)) {
          return new FragmentImageCaptureBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_image_capture is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTMESSAGES: {
        if ("layout/fragment_messages_0".equals(tag)) {
          return new FragmentMessagesBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_messages is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTMESSENGER: {
        if ("layout/fragment_messenger_0".equals(tag)) {
          return new FragmentMessengerBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_messenger is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSELECTCOUNTRY: {
        if ("layout/fragment_select_country_0".equals(tag)) {
          return new FragmentSelectCountryBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_select_country is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSELECTGROUPMEMBERS: {
        if ("layout/fragment_select_group_members_0".equals(tag)) {
          return new FragmentSelectGroupMembersBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_select_group_members is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSETTINGS: {
        if ("layout/fragment_settings_0".equals(tag)) {
          return new FragmentSettingsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_settings is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTWALLET: {
        if ("layout/fragment_wallet_0".equals(tag)) {
          return new FragmentWalletBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_wallet is invalid. Received: " + tag);
      }
      case  LAYOUT_FRIENDIMAGEMESSAGECELL: {
        if ("layout/friend_image_message_cell_0".equals(tag)) {
          return new FriendImageMessageCellBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for friend_image_message_cell is invalid. Received: " + tag);
      }
      case  LAYOUT_FRIENDLOCATIONMESSAGECELL: {
        if ("layout/friend_location_message_cell_0".equals(tag)) {
          return new FriendLocationMessageCellBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for friend_location_message_cell is invalid. Received: " + tag);
      }
      case  LAYOUT_FRIENDTEXTMESSAGECELL: {
        if ("layout/friend_text_message_cell_0".equals(tag)) {
          return new FriendTextMessageCellBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for friend_text_message_cell is invalid. Received: " + tag);
      }
      case  LAYOUT_GCOINOFFERTCELL: {
        if ("layout/gcoin_offert_cell_0".equals(tag)) {
          return new GcoinOffertCellBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for gcoin_offert_cell is invalid. Received: " + tag);
      }
    }
    return null;
  }

  private final ViewDataBinding internalGetViewDataBinding1(DataBindingComponent component,
      View view, int internalId, Object tag) {
    switch(internalId) {
      case  LAYOUT_GLOBALDONATIONGROUPCELL: {
        if ("layout/global_donation_group_cell_0".equals(tag)) {
          return new GlobalDonationGroupCellBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for global_donation_group_cell is invalid. Received: " + tag);
      }
      case  LAYOUT_GLOBALDONATIONUSERCELL: {
        if ("layout/global_donation_user_cell_0".equals(tag)) {
          return new GlobalDonationUserCellBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for global_donation_user_cell is invalid. Received: " + tag);
      }
      case  LAYOUT_GLOBALSECTIONCELL: {
        if ("layout/global_section_cell_0".equals(tag)) {
          return new GlobalSectionCellBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for global_section_cell is invalid. Received: " + tag);
      }
      case  LAYOUT_LEVELCELL: {
        if ("layout/level_cell_0".equals(tag)) {
          return new LevelCellBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for level_cell is invalid. Received: " + tag);
      }
      case  LAYOUT_MESSANGERTIMESECTION: {
        if ("layout/messanger_time_section_0".equals(tag)) {
          return new MessangerTimeSectionBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for messanger_time_section is invalid. Received: " + tag);
      }
      case  LAYOUT_NEWMESSAGENOTIFICATIONCELL: {
        if ("layout/new_message_notification_cell_0".equals(tag)) {
          return new NewMessageNotificationCellBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for new_message_notification_cell is invalid. Received: " + tag);
      }
      case  LAYOUT_REGISTERACTIVITY: {
        if ("layout/register_activity_0".equals(tag)) {
          return new RegisterActivityBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for register_activity is invalid. Received: " + tag);
      }
      case  LAYOUT_SELECTEDUSERSCELL: {
        if ("layout/selected_users_cell_0".equals(tag)) {
          return new SelectedUsersCellBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for selected_users_cell is invalid. Received: " + tag);
      }
      case  LAYOUT_TRANSACTIONCELL: {
        if ("layout/transaction_cell_0".equals(tag)) {
          return new TransactionCellBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for transaction_cell is invalid. Received: " + tag);
      }
      case  LAYOUT_USERIMAGEMESSAGECELL: {
        if ("layout/user_image_message_cell_0".equals(tag)) {
          return new UserImageMessageCellBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for user_image_message_cell is invalid. Received: " + tag);
      }
      case  LAYOUT_USERLOCATIONMESSAGECELL: {
        if ("layout/user_location_message_cell_0".equals(tag)) {
          return new UserLocationMessageCellBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for user_location_message_cell is invalid. Received: " + tag);
      }
      case  LAYOUT_USERMESSAGESTATE: {
        if ("layout/user_message_state_0".equals(tag)) {
          return new UserMessageStateBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for user_message_state is invalid. Received: " + tag);
      }
      case  LAYOUT_USERTEXTMESSAGECELL: {
        if ("layout/user_text_message_cell_0".equals(tag)) {
          return new UserTextMessageCellBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for user_text_message_cell is invalid. Received: " + tag);
      }
      case  LAYOUT_WITHDRAWCELL: {
        if ("layout/withdraw_cell_0".equals(tag)) {
          return new WithdrawCellBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for withdraw_cell is invalid. Received: " + tag);
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      // find which method will have it. -1 is necessary becausefirst id starts with 1;
      int methodIndex = (localizedLayoutId - 1) / 50;
      switch(methodIndex) {
        case 0: {
          return internalGetViewDataBinding0(component, view, localizedLayoutId, tag);
        }
        case 1: {
          return internalGetViewDataBinding1(component, view, localizedLayoutId, tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(1);
    result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(27);

    static {
      sKeys.put(0, "_all");
      sKeys.put(1, "checked");
      sKeys.put(2, "contact");
      sKeys.put(3, "countryItem");
      sKeys.put(4, "eventState");
      sKeys.put(5, "filterItem");
      sKeys.put(6, "globalSection");
      sKeys.put(7, "group");
      sKeys.put(8, "isMessageSeen");
      sKeys.put(9, "item");
      sKeys.put(10, "itemConversation");
      sKeys.put(11, "itemGcoin");
      sKeys.put(12, "itemMessage");
      sKeys.put(13, "itemName");
      sKeys.put(14, "itemWithdraw");
      sKeys.put(15, "levelItem");
      sKeys.put(16, "messageItem");
      sKeys.put(17, "messageSection");
      sKeys.put(18, "notificationCount");
      sKeys.put(19, "obj");
      sKeys.put(20, "optionItem");
      sKeys.put(21, "sectionName");
      sKeys.put(22, "transactionItem");
      sKeys.put(23, "user");
      sKeys.put(24, "userInfo");
      sKeys.put(25, "userItem");
      sKeys.put(26, "viewModel");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(64);

    static {
      sKeys.put("layout/activity_add_event_0", com.impaktsoft.globeup.R.layout.activity_add_event);
      sKeys.put("layout/activity_buy_gcoin_0", com.impaktsoft.globeup.R.layout.activity_buy_gcoin);
      sKeys.put("layout/activity_contacts_0", com.impaktsoft.globeup.R.layout.activity_contacts);
      sKeys.put("layout/activity_donation_0", com.impaktsoft.globeup.R.layout.activity_donation);
      sKeys.put("layout/activity_edit_event_0", com.impaktsoft.globeup.R.layout.activity_edit_event);
      sKeys.put("layout/activity_event_0", com.impaktsoft.globeup.R.layout.activity_event);
      sKeys.put("layout/activity_experience_0", com.impaktsoft.globeup.R.layout.activity_experience);
      sKeys.put("layout/activity_get_coupon_0", com.impaktsoft.globeup.R.layout.activity_get_coupon);
      sKeys.put("layout/activity_init_user_0", com.impaktsoft.globeup.R.layout.activity_init_user);
      sKeys.put("layout/activity_login_0", com.impaktsoft.globeup.R.layout.activity_login);
      sKeys.put("layout/activity_preview_image_0", com.impaktsoft.globeup.R.layout.activity_preview_image);
      sKeys.put("layout/activity_search_events_0", com.impaktsoft.globeup.R.layout.activity_search_events);
      sKeys.put("layout/activity_settings_0", com.impaktsoft.globeup.R.layout.activity_settings);
      sKeys.put("layout/activity_withdraw_0", com.impaktsoft.globeup.R.layout.activity_withdraw);
      sKeys.put("layout/contacts_cell_0", com.impaktsoft.globeup.R.layout.contacts_cell);
      sKeys.put("layout/contacts_for_select_cell_0", com.impaktsoft.globeup.R.layout.contacts_for_select_cell);
      sKeys.put("layout/conversation_cell_0", com.impaktsoft.globeup.R.layout.conversation_cell);
      sKeys.put("layout/country_cell_0", com.impaktsoft.globeup.R.layout.country_cell);
      sKeys.put("layout/dashboard_event_cell_0", com.impaktsoft.globeup.R.layout.dashboard_event_cell);
      sKeys.put("layout/event_cell_0", com.impaktsoft.globeup.R.layout.event_cell);
      sKeys.put("layout/event_cell_section_0", com.impaktsoft.globeup.R.layout.event_cell_section);
      sKeys.put("layout/experience_cell_0", com.impaktsoft.globeup.R.layout.experience_cell);
      sKeys.put("layout/experience_cell_layout_0", com.impaktsoft.globeup.R.layout.experience_cell_layout);
      sKeys.put("layout/feedback_cell_0", com.impaktsoft.globeup.R.layout.feedback_cell);
      sKeys.put("layout/filter_applied_cell_0", com.impaktsoft.globeup.R.layout.filter_applied_cell);
      sKeys.put("layout/filter_event_cell_0", com.impaktsoft.globeup.R.layout.filter_event_cell);
      sKeys.put("layout/floating_button_option_cell_0", com.impaktsoft.globeup.R.layout.floating_button_option_cell);
      sKeys.put("layout/fragment_choose_contacts_0", com.impaktsoft.globeup.R.layout.fragment_choose_contacts);
      sKeys.put("layout/fragment_create_group_0", com.impaktsoft.globeup.R.layout.fragment_create_group);
      sKeys.put("layout/fragment_dashboard_0", com.impaktsoft.globeup.R.layout.fragment_dashboard);
      sKeys.put("layout/fragment_delete_account_0", com.impaktsoft.globeup.R.layout.fragment_delete_account);
      sKeys.put("layout/fragment_donation_done_0", com.impaktsoft.globeup.R.layout.fragment_donation_done);
      sKeys.put("layout/fragment_edit_profile_0", com.impaktsoft.globeup.R.layout.fragment_edit_profile);
      sKeys.put("layout/fragment_events_0", com.impaktsoft.globeup.R.layout.fragment_events);
      sKeys.put("layout/fragment_feedback_0", com.impaktsoft.globeup.R.layout.fragment_feedback);
      sKeys.put("layout/fragment_filter_events_0", com.impaktsoft.globeup.R.layout.fragment_filter_events);
      sKeys.put("layout/fragment_filter_events_by_radius_0", com.impaktsoft.globeup.R.layout.fragment_filter_events_by_radius);
      sKeys.put("layout/fragment_forget_password_0", com.impaktsoft.globeup.R.layout.fragment_forget_password);
      sKeys.put("layout/fragment_global_0", com.impaktsoft.globeup.R.layout.fragment_global);
      sKeys.put("layout/fragment_image_capture_0", com.impaktsoft.globeup.R.layout.fragment_image_capture);
      sKeys.put("layout/fragment_messages_0", com.impaktsoft.globeup.R.layout.fragment_messages);
      sKeys.put("layout/fragment_messenger_0", com.impaktsoft.globeup.R.layout.fragment_messenger);
      sKeys.put("layout/fragment_select_country_0", com.impaktsoft.globeup.R.layout.fragment_select_country);
      sKeys.put("layout/fragment_select_group_members_0", com.impaktsoft.globeup.R.layout.fragment_select_group_members);
      sKeys.put("layout/fragment_settings_0", com.impaktsoft.globeup.R.layout.fragment_settings);
      sKeys.put("layout/fragment_wallet_0", com.impaktsoft.globeup.R.layout.fragment_wallet);
      sKeys.put("layout/friend_image_message_cell_0", com.impaktsoft.globeup.R.layout.friend_image_message_cell);
      sKeys.put("layout/friend_location_message_cell_0", com.impaktsoft.globeup.R.layout.friend_location_message_cell);
      sKeys.put("layout/friend_text_message_cell_0", com.impaktsoft.globeup.R.layout.friend_text_message_cell);
      sKeys.put("layout/gcoin_offert_cell_0", com.impaktsoft.globeup.R.layout.gcoin_offert_cell);
      sKeys.put("layout/global_donation_group_cell_0", com.impaktsoft.globeup.R.layout.global_donation_group_cell);
      sKeys.put("layout/global_donation_user_cell_0", com.impaktsoft.globeup.R.layout.global_donation_user_cell);
      sKeys.put("layout/global_section_cell_0", com.impaktsoft.globeup.R.layout.global_section_cell);
      sKeys.put("layout/level_cell_0", com.impaktsoft.globeup.R.layout.level_cell);
      sKeys.put("layout/messanger_time_section_0", com.impaktsoft.globeup.R.layout.messanger_time_section);
      sKeys.put("layout/new_message_notification_cell_0", com.impaktsoft.globeup.R.layout.new_message_notification_cell);
      sKeys.put("layout/register_activity_0", com.impaktsoft.globeup.R.layout.register_activity);
      sKeys.put("layout/selected_users_cell_0", com.impaktsoft.globeup.R.layout.selected_users_cell);
      sKeys.put("layout/transaction_cell_0", com.impaktsoft.globeup.R.layout.transaction_cell);
      sKeys.put("layout/user_image_message_cell_0", com.impaktsoft.globeup.R.layout.user_image_message_cell);
      sKeys.put("layout/user_location_message_cell_0", com.impaktsoft.globeup.R.layout.user_location_message_cell);
      sKeys.put("layout/user_message_state_0", com.impaktsoft.globeup.R.layout.user_message_state);
      sKeys.put("layout/user_text_message_cell_0", com.impaktsoft.globeup.R.layout.user_text_message_cell);
      sKeys.put("layout/withdraw_cell_0", com.impaktsoft.globeup.R.layout.withdraw_cell);
    }
  }
}
