package com.impaktsoft.globeup.databinding;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentMessengerBindingImpl extends FragmentMessengerBinding implements com.impaktsoft.globeup.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.chat_layout, 6);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    @NonNull
    private final androidx.swiperefreshlayout.widget.SwipeRefreshLayout mboundView1;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback41;
    @Nullable
    private final android.view.View.OnClickListener mCallback42;
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener textSendandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.messageText.getValue()
            //         is viewModel.messageText.setValue((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(textSend);
            // localize variables for thread safety
            // viewModel.messageText
            androidx.lifecycle.MutableLiveData<java.lang.String> viewModelMessageText = null;
            // viewModel.messageText != null
            boolean viewModelMessageTextJavaLangObjectNull = false;
            // viewModel.messageText.getValue()
            java.lang.String viewModelMessageTextGetValue = null;
            // viewModel
            com.impaktsoft.globeup.viewmodels.MessengerViewModel viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelMessageText = viewModel.getMessageText();

                viewModelMessageTextJavaLangObjectNull = (viewModelMessageText) != (null);
                if (viewModelMessageTextJavaLangObjectNull) {




                    viewModelMessageText.setValue(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public FragmentMessengerBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 7, sIncludes, sViewsWithIds));
    }
    private FragmentMessengerBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 3
            , (android.widget.Button) bindings[5]
            , (android.widget.RelativeLayout) bindings[6]
            , (android.widget.Button) bindings[3]
            , (androidx.recyclerview.widget.RecyclerView) bindings[2]
            , (android.widget.EditText) bindings[4]
            );
        this.btnSend.setTag(null);
        this.chatOptionsButton.setTag(null);
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (androidx.swiperefreshlayout.widget.SwipeRefreshLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.messagesList.setTag(null);
        this.textSend.setTag(null);
        setRootTag(root);
        // listeners
        mCallback41 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 1);
        mCallback42 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 2);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x10L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((com.impaktsoft.globeup.viewmodels.MessengerViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable com.impaktsoft.globeup.viewmodels.MessengerViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x8L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelRecyclerViewPosition((androidx.lifecycle.MutableLiveData<java.lang.Integer>) object, fieldId);
            case 1 :
                return onChangeViewModelMessageText((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 2 :
                return onChangeViewModelIsFetchingMessages((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelRecyclerViewPosition(androidx.lifecycle.MutableLiveData<java.lang.Integer> ViewModelRecyclerViewPosition, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelMessageText(androidx.lifecycle.MutableLiveData<java.lang.String> ViewModelMessageText, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsFetchingMessages(androidx.lifecycle.MutableLiveData<java.lang.Boolean> ViewModelIsFetchingMessages, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        androidx.lifecycle.MutableLiveData<java.lang.Integer> viewModelRecyclerViewPosition = null;
        int androidxDatabindingViewDataBindingSafeUnboxViewModelRecyclerViewPositionGetValue = 0;
        androidx.lifecycle.MutableLiveData<java.lang.String> viewModelMessageText = null;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> viewModelIsFetchingMessages = null;
        java.lang.String viewModelMessageTextGetValue = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxViewModelIsFetchingMessagesGetValue = false;
        com.impaktsoft.globeup.viewmodels.MessengerViewModel viewModel = mViewModel;
        java.lang.Boolean viewModelIsFetchingMessagesGetValue = null;
        java.lang.Integer viewModelRecyclerViewPositionGetValue = null;

        if ((dirtyFlags & 0x1fL) != 0) {


            if ((dirtyFlags & 0x19L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.recyclerViewPosition
                        viewModelRecyclerViewPosition = viewModel.getRecyclerViewPosition();
                    }
                    updateLiveDataRegistration(0, viewModelRecyclerViewPosition);


                    if (viewModelRecyclerViewPosition != null) {
                        // read viewModel.recyclerViewPosition.getValue()
                        viewModelRecyclerViewPositionGetValue = viewModelRecyclerViewPosition.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.recyclerViewPosition.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelRecyclerViewPositionGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelRecyclerViewPositionGetValue);
            }
            if ((dirtyFlags & 0x1aL) != 0) {

                    if (viewModel != null) {
                        // read viewModel.messageText
                        viewModelMessageText = viewModel.getMessageText();
                    }
                    updateLiveDataRegistration(1, viewModelMessageText);


                    if (viewModelMessageText != null) {
                        // read viewModel.messageText.getValue()
                        viewModelMessageTextGetValue = viewModelMessageText.getValue();
                    }
            }
            if ((dirtyFlags & 0x1cL) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isFetchingMessages
                        viewModelIsFetchingMessages = viewModel.isFetchingMessages();
                    }
                    updateLiveDataRegistration(2, viewModelIsFetchingMessages);


                    if (viewModelIsFetchingMessages != null) {
                        // read viewModel.isFetchingMessages.getValue()
                        viewModelIsFetchingMessagesGetValue = viewModelIsFetchingMessages.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isFetchingMessages.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelIsFetchingMessagesGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelIsFetchingMessagesGetValue);
            }
        }
        // batch finished
        if ((dirtyFlags & 0x10L) != 0) {
            // api target 1

            this.btnSend.setOnClickListener(mCallback42);
            this.chatOptionsButton.setOnClickListener(mCallback41);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.textSend, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, textSendandroidTextAttrChanged);
        }
        if ((dirtyFlags & 0x1cL) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.RecyclerViewBindingAdapter.isRefreshing(this.mboundView1, androidxDatabindingViewDataBindingSafeUnboxViewModelIsFetchingMessagesGetValue);
        }
        if ((dirtyFlags & 0x18L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.RecyclerViewBindingAdapter.OnRefresh(this.mboundView1, viewModel);
        }
        if ((dirtyFlags & 0x19L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.RecyclerViewBindingAdapter.scrollToPosition(this.messagesList, androidxDatabindingViewDataBindingSafeUnboxViewModelRecyclerViewPositionGetValue);
        }
        if ((dirtyFlags & 0x1aL) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textSend, viewModelMessageTextGetValue);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 1: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.MessengerViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.showMenu();
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.MessengerViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.sendMessages();
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.recyclerViewPosition
        flag 1 (0x2L): viewModel.messageText
        flag 2 (0x3L): viewModel.isFetchingMessages
        flag 3 (0x4L): viewModel
        flag 4 (0x5L): null
    flag mapping end*/
    //end
}