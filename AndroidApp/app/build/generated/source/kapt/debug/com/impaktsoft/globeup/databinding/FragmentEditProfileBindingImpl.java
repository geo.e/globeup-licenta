package com.impaktsoft.globeup.databinding;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentEditProfileBindingImpl extends FragmentEditProfileBinding implements com.impaktsoft.globeup.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.edit_profile_image_layout, 10);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    @NonNull
    private final android.widget.FrameLayout mboundView1;
    @NonNull
    private final android.widget.ImageView mboundView3;
    @NonNull
    private final android.widget.TextView mboundView6;
    @NonNull
    private final android.widget.LinearLayout mboundView7;
    @NonNull
    private final android.widget.TextView mboundView8;
    @NonNull
    private final android.widget.Button mboundView9;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback6;
    @Nullable
    private final android.view.View.OnClickListener mCallback4;
    @Nullable
    private final android.view.View.OnClickListener mCallback7;
    @Nullable
    private final android.view.View.OnClickListener mCallback5;
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener editNameEditTextandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.userName.getValue()
            //         is viewModel.userName.setValue((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(editNameEditText);
            // localize variables for thread safety
            // viewModel.userName
            androidx.lifecycle.MutableLiveData<java.lang.String> viewModelUserName = null;
            // viewModel.userName.getValue()
            java.lang.String viewModelUserNameGetValue = null;
            // viewModel.userName != null
            boolean viewModelUserNameJavaLangObjectNull = false;
            // viewModel
            com.impaktsoft.globeup.viewmodels.EditProfileViewModel viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelUserName = viewModel.getUserName();

                viewModelUserNameJavaLangObjectNull = (viewModelUserName) != (null);
                if (viewModelUserNameJavaLangObjectNull) {




                    viewModelUserName.setValue(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public FragmentEditProfileBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 11, sIncludes, sViewsWithIds));
    }
    private FragmentEditProfileBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 5
            , (android.widget.LinearLayout) bindings[5]
            , (android.widget.EditText) bindings[4]
            , (android.widget.RelativeLayout) bindings[10]
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[2]
            );
        this.chooseCountryView.setTag(null);
        this.editNameEditText.setTag(null);
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.FrameLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView3 = (android.widget.ImageView) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView6 = (android.widget.TextView) bindings[6];
        this.mboundView6.setTag(null);
        this.mboundView7 = (android.widget.LinearLayout) bindings[7];
        this.mboundView7.setTag(null);
        this.mboundView8 = (android.widget.TextView) bindings[8];
        this.mboundView8.setTag(null);
        this.mboundView9 = (android.widget.Button) bindings[9];
        this.mboundView9.setTag(null);
        this.p.setTag(null);
        setRootTag(root);
        // listeners
        mCallback6 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 3);
        mCallback4 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 1);
        mCallback7 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 4);
        mCallback5 = new com.impaktsoft.globeup.generated.callback.OnClickListener(this, 2);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x40L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((com.impaktsoft.globeup.viewmodels.EditProfileViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable com.impaktsoft.globeup.viewmodels.EditProfileViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x20L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelIsHudVisible((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 1 :
                return onChangeViewModelUserImageUrl((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 2 :
                return onChangeViewModelCountry((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 3 :
                return onChangeViewModelProfileImageFilePath((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 4 :
                return onChangeViewModelUserName((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelIsHudVisible(androidx.lifecycle.MutableLiveData<java.lang.Boolean> ViewModelIsHudVisible, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelUserImageUrl(androidx.lifecycle.MutableLiveData<java.lang.String> ViewModelUserImageUrl, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelCountry(androidx.lifecycle.MutableLiveData<java.lang.String> ViewModelCountry, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelProfileImageFilePath(androidx.lifecycle.MutableLiveData<java.lang.String> ViewModelProfileImageFilePath, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelUserName(androidx.lifecycle.MutableLiveData<java.lang.String> ViewModelUserName, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String viewModelCountryGetValue = null;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> viewModelIsHudVisible = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> viewModelUserImageUrl = null;
        java.lang.String viewModelProfileImageFilePathGetValue = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> viewModelCountry = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> viewModelProfileImageFilePath = null;
        java.lang.String viewModelUserImageUrlGetValue = null;
        int viewModelIsHudVisibleViewVISIBLEViewGONE = 0;
        boolean androidxDatabindingViewDataBindingSafeUnboxViewModelIsHudVisibleGetValue = false;
        androidx.lifecycle.MutableLiveData<java.lang.String> viewModelUserName = null;
        java.lang.Boolean viewModelIsHudVisibleGetValue = null;
        java.lang.String viewModelUserNameGetValue = null;
        com.impaktsoft.globeup.viewmodels.EditProfileViewModel viewModel = mViewModel;

        if ((dirtyFlags & 0x7fL) != 0) {


            if ((dirtyFlags & 0x61L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isHudVisible
                        viewModelIsHudVisible = viewModel.isHudVisible();
                    }
                    updateLiveDataRegistration(0, viewModelIsHudVisible);


                    if (viewModelIsHudVisible != null) {
                        // read viewModel.isHudVisible.getValue()
                        viewModelIsHudVisibleGetValue = viewModelIsHudVisible.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isHudVisible.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelIsHudVisibleGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelIsHudVisibleGetValue);
                if((dirtyFlags & 0x61L) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxViewModelIsHudVisibleGetValue) {
                            dirtyFlags |= 0x100L;
                    }
                    else {
                            dirtyFlags |= 0x80L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isHudVisible.getValue()) ? View.VISIBLE : View.GONE
                    viewModelIsHudVisibleViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxViewModelIsHudVisibleGetValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0x62L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.userImageUrl
                        viewModelUserImageUrl = viewModel.getUserImageUrl();
                    }
                    updateLiveDataRegistration(1, viewModelUserImageUrl);


                    if (viewModelUserImageUrl != null) {
                        // read viewModel.userImageUrl.getValue()
                        viewModelUserImageUrlGetValue = viewModelUserImageUrl.getValue();
                    }
            }
            if ((dirtyFlags & 0x64L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.country
                        viewModelCountry = viewModel.getCountry();
                    }
                    updateLiveDataRegistration(2, viewModelCountry);


                    if (viewModelCountry != null) {
                        // read viewModel.country.getValue()
                        viewModelCountryGetValue = viewModelCountry.getValue();
                    }
            }
            if ((dirtyFlags & 0x68L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.profileImageFilePath
                        viewModelProfileImageFilePath = viewModel.getProfileImageFilePath();
                    }
                    updateLiveDataRegistration(3, viewModelProfileImageFilePath);


                    if (viewModelProfileImageFilePath != null) {
                        // read viewModel.profileImageFilePath.getValue()
                        viewModelProfileImageFilePathGetValue = viewModelProfileImageFilePath.getValue();
                    }
            }
            if ((dirtyFlags & 0x70L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.userName
                        viewModelUserName = viewModel.getUserName();
                    }
                    updateLiveDataRegistration(4, viewModelUserName);


                    if (viewModelUserName != null) {
                        // read viewModel.userName.getValue()
                        viewModelUserNameGetValue = viewModelUserName.getValue();
                    }
            }
        }
        // batch finished
        if ((dirtyFlags & 0x40L) != 0) {
            // api target 1

            this.chooseCountryView.setOnClickListener(mCallback5);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.editNameEditText, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, editNameEditTextandroidTextAttrChanged);
            this.mboundView3.setOnClickListener(mCallback4);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView6, (mboundView6.getResources().getString(R.string.select_country)) + (':'));
            this.mboundView7.setOnClickListener(mCallback6);
            this.mboundView9.setOnClickListener(mCallback7);
        }
        if ((dirtyFlags & 0x70L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.editNameEditText, viewModelUserNameGetValue);
        }
        if ((dirtyFlags & 0x61L) != 0) {
            // api target 1

            this.mboundView1.setVisibility(viewModelIsHudVisibleViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x64L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView8, viewModelCountryGetValue);
        }
        if ((dirtyFlags & 0x68L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.ImageViewBindingAdapters.file(this.p, viewModelProfileImageFilePathGetValue);
        }
        if ((dirtyFlags & 0x62L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.ImageViewBindingAdapters.imageUrl(this.p, viewModelUserImageUrlGetValue, androidx.appcompat.content.res.AppCompatResources.getDrawable(p.getContext(), R.drawable.profile_filled_svg));
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 3: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.EditProfileViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.chooseCountry();
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.EditProfileViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.takePicture();
                }
                break;
            }
            case 4: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.EditProfileViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.saveProfileChangesClick();
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // viewModel
                com.impaktsoft.globeup.viewmodels.EditProfileViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.chooseCountry();
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.isHudVisible
        flag 1 (0x2L): viewModel.userImageUrl
        flag 2 (0x3L): viewModel.country
        flag 3 (0x4L): viewModel.profileImageFilePath
        flag 4 (0x5L): viewModel.userName
        flag 5 (0x6L): viewModel
        flag 6 (0x7L): null
        flag 7 (0x8L): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isHudVisible.getValue()) ? View.VISIBLE : View.GONE
        flag 8 (0x9L): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isHudVisible.getValue()) ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}