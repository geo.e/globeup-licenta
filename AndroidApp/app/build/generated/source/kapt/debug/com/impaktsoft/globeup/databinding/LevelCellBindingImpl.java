package com.impaktsoft.globeup.databinding;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class LevelCellBindingImpl extends LevelCellBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public LevelCellBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 5, sIncludes, sViewsWithIds));
    }
    private LevelCellBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[4]
            , (android.widget.TextView) bindings[2]
            , (android.widget.ImageView) bindings[1]
            );
        this.levelDescription.setTag(null);
        this.levelExp.setTag(null);
        this.levelName.setTag(null);
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.medalImage.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.levelItem == variableId) {
            setLevelItem((com.impaktsoft.globeup.models.LevelPOJO) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setLevelItem(@Nullable com.impaktsoft.globeup.models.LevelPOJO LevelItem) {
        this.mLevelItem = LevelItem;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.levelItem);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        com.impaktsoft.globeup.models.LevelPOJO levelItem = mLevelItem;
        java.lang.String levelItemLevelName = null;
        int levelItemHasDescriptionViewGONEViewVISIBLE = 0;
        java.lang.String levelItemLevelExp = null;
        java.lang.String levelItemLevelDescription = null;
        boolean levelItemHasDescription = false;

        if ((dirtyFlags & 0x3L) != 0) {



                if (levelItem != null) {
                    // read levelItem.levelName
                    levelItemLevelName = levelItem.getLevelName();
                    // read levelItem.levelExp
                    levelItemLevelExp = levelItem.getLevelExp();
                    // read levelItem.levelDescription
                    levelItemLevelDescription = levelItem.getLevelDescription();
                    // read levelItem.hasDescription()
                    levelItemHasDescription = levelItem.hasDescription();
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(levelItemHasDescription) {
                        dirtyFlags |= 0x8L;
                }
                else {
                        dirtyFlags |= 0x4L;
                }
            }


                // read levelItem.hasDescription() ? View.GONE : View.VISIBLE
                levelItemHasDescriptionViewGONEViewVISIBLE = ((levelItemHasDescription) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            this.levelDescription.setVisibility(levelItemHasDescriptionViewGONEViewVISIBLE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.levelDescription, levelItemLevelDescription);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.levelExp, levelItemLevelExp);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.levelName, levelItemLevelName);
            com.impaktsoft.globeup.bindingadapters.ImageViewBindingAdapters.setLevelImage(this.medalImage, levelItem);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): levelItem
        flag 1 (0x2L): null
        flag 2 (0x3L): levelItem.hasDescription() ? View.GONE : View.VISIBLE
        flag 3 (0x4L): levelItem.hasDescription() ? View.GONE : View.VISIBLE
    flag mapping end*/
    //end
}