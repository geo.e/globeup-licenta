package com.impaktsoft.globeup.databinding;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class GcoinOffertCellBindingImpl extends GcoinOffertCellBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final android.widget.TextView mboundView1;
    @NonNull
    private final android.widget.TextView mboundView2;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public GcoinOffertCellBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 3, sIncludes, sViewsWithIds));
    }
    private GcoinOffertCellBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.LinearLayout) bindings[0]
            );
        this.gcoinLayout.setTag(null);
        this.mboundView1 = (android.widget.TextView) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView2 = (android.widget.TextView) bindings[2];
        this.mboundView2.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.itemGcoin == variableId) {
            setItemGcoin((com.impaktsoft.globeup.models.GCoinPOJO) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setItemGcoin(@Nullable com.impaktsoft.globeup.models.GCoinPOJO ItemGcoin) {
        this.mItemGcoin = ItemGcoin;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.itemGcoin);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.Integer itemGcoinValue = null;
        java.lang.String itemGcoinValueToString = null;
        com.impaktsoft.globeup.models.GCoinPOJO itemGcoin = mItemGcoin;

        if ((dirtyFlags & 0x3L) != 0) {



                if (itemGcoin != null) {
                    // read itemGcoin.value
                    itemGcoinValue = itemGcoin.getValue();
                }


                if (itemGcoinValue != null) {
                    // read itemGcoin.value.toString()
                    itemGcoinValueToString = itemGcoinValue.toString();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            com.impaktsoft.globeup.bindingadapters.LinearLayoutBindingAdapters.setBackGround(this.gcoinLayout, itemGcoin);
            com.impaktsoft.globeup.bindingadapters.TextViewBindingAdapters.setGcoinTextColor(this.mboundView1, itemGcoin);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView1, itemGcoinValueToString);
            com.impaktsoft.globeup.bindingadapters.TextViewBindingAdapters.setGcoinTextColor(this.mboundView2, itemGcoin);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): itemGcoin
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}