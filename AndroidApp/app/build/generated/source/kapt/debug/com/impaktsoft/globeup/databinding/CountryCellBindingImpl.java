package com.impaktsoft.globeup.databinding;
import com.impaktsoft.globeup.R;
import com.impaktsoft.globeup.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class CountryCellBindingImpl extends CountryCellBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final android.widget.TextView mboundView1;
    @NonNull
    private final android.widget.ImageView mboundView2;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public CountryCellBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 3, sIncludes, sViewsWithIds));
    }
    private CountryCellBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.RelativeLayout) bindings[0]
            );
        this.countryCellLayout.setTag(null);
        this.mboundView1 = (android.widget.TextView) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView2 = (android.widget.ImageView) bindings[2];
        this.mboundView2.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.countryItem == variableId) {
            setCountryItem((com.impaktsoft.globeup.models.CountryPOJO) variable);
        }
        else if (BR.viewModel == variableId) {
            setViewModel((com.impaktsoft.globeup.viewmodels.SelectCountryViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setCountryItem(@Nullable com.impaktsoft.globeup.models.CountryPOJO CountryItem) {
        this.mCountryItem = CountryItem;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.countryItem);
        super.requestRebind();
    }
    public void setViewModel(@Nullable com.impaktsoft.globeup.viewmodels.SelectCountryViewModel ViewModel) {
        this.mViewModel = ViewModel;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        com.impaktsoft.globeup.models.CountryPOJO countryItem = mCountryItem;
        java.lang.String countryItemCountryName = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxCountryItemIsSelected = false;
        java.lang.Boolean countryItemIsSelected = null;
        int countryItemIsSelectedViewVISIBLEViewGONE = 0;

        if ((dirtyFlags & 0x5L) != 0) {



                if (countryItem != null) {
                    // read countryItem.countryName
                    countryItemCountryName = countryItem.getCountryName();
                    // read countryItem.isSelected
                    countryItemIsSelected = countryItem.isSelected();
                }


                // read androidx.databinding.ViewDataBinding.safeUnbox(countryItem.isSelected)
                androidxDatabindingViewDataBindingSafeUnboxCountryItemIsSelected = androidx.databinding.ViewDataBinding.safeUnbox(countryItemIsSelected);
            if((dirtyFlags & 0x5L) != 0) {
                if(androidxDatabindingViewDataBindingSafeUnboxCountryItemIsSelected) {
                        dirtyFlags |= 0x10L;
                }
                else {
                        dirtyFlags |= 0x8L;
                }
            }


                // read androidx.databinding.ViewDataBinding.safeUnbox(countryItem.isSelected) ? View.VISIBLE : View.GONE
                countryItemIsSelectedViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxCountryItemIsSelected) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
        }
        // batch finished
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView1, countryItemCountryName);
            this.mboundView2.setVisibility(countryItemIsSelectedViewVISIBLEViewGONE);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): countryItem
        flag 1 (0x2L): viewModel
        flag 2 (0x3L): null
        flag 3 (0x4L): androidx.databinding.ViewDataBinding.safeUnbox(countryItem.isSelected) ? View.VISIBLE : View.GONE
        flag 4 (0x5L): androidx.databinding.ViewDataBinding.safeUnbox(countryItem.isSelected) ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}