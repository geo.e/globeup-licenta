package androidx.databinding.library.baseAdapters;

public class BR {
  public static final int _all = 0;

  public static final int checked = 1;

  public static final int contact = 2;

  public static final int countryItem = 3;

  public static final int eventState = 4;

  public static final int filterItem = 5;

  public static final int globalSection = 6;

  public static final int group = 7;

  public static final int isMessageSeen = 8;

  public static final int item = 9;

  public static final int itemConversation = 10;

  public static final int itemGcoin = 11;

  public static final int itemMessage = 12;

  public static final int itemName = 13;

  public static final int itemWithdraw = 14;

  public static final int levelItem = 15;

  public static final int messageItem = 16;

  public static final int messageSection = 17;

  public static final int notificationCount = 18;

  public static final int obj = 19;

  public static final int optionItem = 20;

  public static final int sectionName = 21;

  public static final int transactionItem = 22;

  public static final int user = 23;

  public static final int userInfo = 24;

  public static final int userItem = 25;

  public static final int viewModel = 26;
}
