package com.impaktsoft.globeup.views.activities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0005\u00a2\u0006\u0002\u0010\u0004J\"\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00062\u0006\u0010\u0012\u001a\u00020\u00062\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0014J\u0010\u0010\u0015\u001a\u00020\u00102\u0006\u0010\u0016\u001a\u00020\u0003H\u0014R(\u0010\u0007\u001a\u0004\u0018\u00010\u00062\b\u0010\u0005\u001a\u0004\u0018\u00010\u00068T@TX\u0094\u000e\u00a2\u0006\f\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR\u0014\u0010\f\u001a\u00020\u00068TX\u0094\u0004\u00a2\u0006\u0006\u001a\u0004\b\r\u0010\u000e\u00a8\u0006\u0017"}, d2 = {"Lcom/impaktsoft/globeup/views/activities/LoginActivity;", "Lcom/impaktsoft/globeup/views/activities/BaseBoundActivity;", "Lcom/impaktsoft/globeup/viewmodels/LoginViewModel;", "Lcom/impaktsoft/globeup/databinding/ActivityLoginBinding;", "()V", "value", "", "activityTitleResourceId", "getActivityTitleResourceId", "()Ljava/lang/Integer;", "setActivityTitleResourceId", "(Ljava/lang/Integer;)V", "layoutId", "getLayoutId", "()I", "onActivityResult", "", "requestCode", "resultCode", "data", "Landroid/content/Intent;", "setupDataBinding", "binding", "app_debug"})
public final class LoginActivity extends com.impaktsoft.globeup.views.activities.BaseBoundActivity<com.impaktsoft.globeup.viewmodels.LoginViewModel, com.impaktsoft.globeup.databinding.ActivityLoginBinding> {
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected int getLayoutId() {
        return 0;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    protected java.lang.Integer getActivityTitleResourceId() {
        return null;
    }
    
    @java.lang.Override()
    protected void setActivityTitleResourceId(@org.jetbrains.annotations.Nullable()
    java.lang.Integer value) {
    }
    
    @java.lang.Override()
    protected void setupDataBinding(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.databinding.ActivityLoginBinding binding) {
    }
    
    @java.lang.Override()
    protected void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    public LoginActivity() {
        super(null);
    }
}