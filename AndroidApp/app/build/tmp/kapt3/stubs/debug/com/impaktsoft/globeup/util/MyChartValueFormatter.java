package com.impaktsoft.globeup.util;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\u0010\u0010\u0012\u001a\u00020\u000f2\u0006\u0010\u0013\u001a\u00020\tH\u0002R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u001a\u0010\b\u001a\u00020\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\r\u00a8\u0006\u0014"}, d2 = {"Lcom/impaktsoft/globeup/util/MyChartValueFormatter;", "Lcom/github/mikephil/charting/formatter/ValueFormatter;", "data", "Ljava/util/ArrayList;", "Lcom/github/mikephil/charting/data/Entry;", "(Ljava/util/ArrayList;)V", "getData", "()Ljava/util/ArrayList;", "t", "", "getT", "()I", "setT", "(I)V", "getFormattedValue", "", "value", "", "getMonthForInt", "num", "app_debug"})
public final class MyChartValueFormatter extends com.github.mikephil.charting.formatter.ValueFormatter {
    private int t;
    @org.jetbrains.annotations.NotNull()
    private final java.util.ArrayList<com.github.mikephil.charting.data.Entry> data = null;
    
    public final int getT() {
        return 0;
    }
    
    public final void setT(int p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String getFormattedValue(float value) {
        return null;
    }
    
    private final java.lang.String getMonthForInt(int num) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.github.mikephil.charting.data.Entry> getData() {
        return null;
    }
    
    public MyChartValueFormatter(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.github.mikephil.charting.data.Entry> data) {
        super();
    }
}