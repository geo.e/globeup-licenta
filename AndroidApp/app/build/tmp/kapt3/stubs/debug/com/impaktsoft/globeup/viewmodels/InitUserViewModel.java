package com.impaktsoft.globeup.viewmodels;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\t\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0011\u0010\u0018\u001a\u00020\u0019H\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u001aJ\b\u0010\u001b\u001a\u00020\u0019H\u0002J\b\u0010\u001c\u001a\u00020\u0019H\u0002J\u0006\u0010\u001d\u001a\u00020\u0019J+\u0010\u001e\u001a\u00020\u00192\u0006\u0010\u001f\u001a\u00020\u00042\b\u0010 \u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0004H\u0002\u00a2\u0006\u0002\u0010!R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R \u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0007\"\u0004\b\b\u0010\tR \u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00040\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u0007\"\u0004\b\u000b\u0010\tR \u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00040\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\u0007\"\u0004\b\r\u0010\tR \u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0007\"\u0004\b\u0011\u0010\tR\u001c\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\""}, d2 = {"Lcom/impaktsoft/globeup/viewmodels/InitUserViewModel;", "Lcom/impaktsoft/globeup/viewmodels/BaseViewModel;", "()V", "addInDbHasFailed", "", "isPlaceholderVisible", "Landroidx/lifecycle/MutableLiveData;", "()Landroidx/lifecycle/MutableLiveData;", "setPlaceholderVisible", "(Landroidx/lifecycle/MutableLiveData;)V", "isProgressBarVisible", "setProgressBarVisible", "isRetryButtonVisible", "setRetryButtonVisible", "placeholderText", "", "getPlaceholderText", "setPlaceholderText", "userCountry", "Lcom/impaktsoft/globeup/models/CountryPOJO;", "getUserCountry", "()Lcom/impaktsoft/globeup/models/CountryPOJO;", "setUserCountry", "(Lcom/impaktsoft/globeup/models/CountryPOJO;)V", "addUserInDB", "", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "checkUserConnection", "navigateToDashboard", "retryConnection", "showPlaceholder", "isPlaceHolderVisible", "placeHolderString", "(ZLjava/lang/String;Ljava/lang/Boolean;)V", "app_debug"})
public final class InitUserViewModel extends com.impaktsoft.globeup.viewmodels.BaseViewModel {
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> placeholderText;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isPlaceholderVisible;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isProgressBarVisible;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isRetryButtonVisible;
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.models.CountryPOJO userCountry;
    private boolean addInDbHasFailed;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getPlaceholderText() {
        return null;
    }
    
    public final void setPlaceholderText(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isPlaceholderVisible() {
        return null;
    }
    
    public final void setPlaceholderVisible(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isProgressBarVisible() {
        return null;
    }
    
    public final void setProgressBarVisible(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isRetryButtonVisible() {
        return null;
    }
    
    public final void setRetryButtonVisible(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.models.CountryPOJO getUserCountry() {
        return null;
    }
    
    public final void setUserCountry(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.models.CountryPOJO p0) {
    }
    
    public final void retryConnection() {
    }
    
    private final void checkUserConnection() {
    }
    
    private final void showPlaceholder(boolean isPlaceHolderVisible, java.lang.String placeHolderString, java.lang.Boolean isRetryButtonVisible) {
    }
    
    private final void navigateToDashboard() {
    }
    
    public InitUserViewModel() {
        super();
    }
}