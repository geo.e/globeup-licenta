package com.impaktsoft.globeup.views.fragments;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0005\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u0014\u001a\u00020\u0015J\u0010\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u0017\u001a\u00020\u0018H\u0014J\u001a\u0010\u0019\u001a\u00020\u00152\u0006\u0010\u001a\u001a\u00020\u00182\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016J\u0010\u0010\u001d\u001a\u00020\u00152\u0006\u0010\u001e\u001a\u00020\u0003H\u0014J\u0006\u0010\u001f\u001a\u00020\u0015R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u00020\bX\u0094D\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR$\u0010\f\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\b@VX\u0094\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\n\"\u0004\b\u000e\u0010\u000fR\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "}, d2 = {"Lcom/impaktsoft/globeup/views/fragments/ImageCaptureFragment;", "Lcom/impaktsoft/globeup/views/fragments/BaseBoundFragment;", "Lcom/impaktsoft/globeup/viewmodels/ImageCaptureViewModel;", "Lcom/impaktsoft/globeup/databinding/FragmentImageCaptureBinding;", "()V", "captureButton", "Landroid/widget/ImageButton;", "fragmentNameResourceID", "", "getFragmentNameResourceID", "()I", "<set-?>", "layoutId", "getLayoutId", "setLayoutId", "(I)V", "previewTexture", "Landroid/view/TextureView;", "selfieCamera", "Lcom/impaktsoft/globeup/util/camera/ProfileSelfieCamera;", "hidePreviewImageLayout", "", "layoutInflated", "root", "Landroid/view/View;", "onViewCreated", "view", "savedInstanceState", "Landroid/os/Bundle;", "setupDataBinding", "binding", "showPreviewImageLayout", "app_debug"})
public final class ImageCaptureFragment extends com.impaktsoft.globeup.views.fragments.BaseBoundFragment<com.impaktsoft.globeup.viewmodels.ImageCaptureViewModel, com.impaktsoft.globeup.databinding.FragmentImageCaptureBinding> {
    private android.view.TextureView previewTexture;
    private android.widget.ImageButton captureButton;
    private com.impaktsoft.globeup.util.camera.ProfileSelfieCamera selfieCamera;
    private int layoutId;
    private final int fragmentNameResourceID = com.impaktsoft.globeup.R.string.image_capture_name;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected int getLayoutId() {
        return 0;
    }
    
    public void setLayoutId(int p0) {
    }
    
    @java.lang.Override()
    protected int getFragmentNameResourceID() {
        return 0;
    }
    
    @java.lang.Override()
    protected void setupDataBinding(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.databinding.FragmentImageCaptureBinding binding) {
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    protected void layoutInflated(@org.jetbrains.annotations.NotNull()
    android.view.View root) {
    }
    
    public final void showPreviewImageLayout() {
    }
    
    public final void hidePreviewImageLayout() {
    }
    
    public ImageCaptureFragment() {
        super(null);
    }
}