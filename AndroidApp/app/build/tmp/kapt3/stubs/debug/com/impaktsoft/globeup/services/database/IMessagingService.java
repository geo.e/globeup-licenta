package com.impaktsoft.globeup.services.database;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\bH&J\u0010\u0010\t\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\bH&\u00a8\u0006\n"}, d2 = {"Lcom/impaktsoft/globeup/services/database/IMessagingService;", "", "sendNotificationMessage", "", "notificationMessage", "Lcom/impaktsoft/globeup/models/NotificationMessagePOJO;", "subscribeToTopic", "topicId", "", "unsubscribeFromATopic", "app_debug"})
public abstract interface IMessagingService {
    
    public abstract void subscribeToTopic(@org.jetbrains.annotations.NotNull()
    java.lang.String topicId);
    
    public abstract void unsubscribeFromATopic(@org.jetbrains.annotations.NotNull()
    java.lang.String topicId);
    
    public abstract void sendNotificationMessage(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.NotificationMessagePOJO notificationMessage);
}