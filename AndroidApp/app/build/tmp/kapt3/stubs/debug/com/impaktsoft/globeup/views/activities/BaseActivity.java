package com.impaktsoft.globeup.views.activities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000e\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b&\u0018\u0000*\n\b\u0000\u0010\u0001 \u0001*\u00020\u00022\u00020\u0003B\u0013\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005\u00a2\u0006\u0002\u0010\u0006J\u0012\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u0014J\b\u0010\u001a\u001a\u00020\u0017H\u0014J\b\u0010\u001b\u001a\u00020\u0017H\u0002R\u001e\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0084\u000e\u00a2\u0006\u0010\n\u0002\u0010\r\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\u001e\u0010\u000e\u001a\u0004\u0018\u00010\bX\u0094\u000e\u00a2\u0006\u0010\n\u0002\u0010\r\u001a\u0004\b\u000f\u0010\n\"\u0004\b\u0010\u0010\fR\u001b\u0010\u0011\u001a\u00028\u00008DX\u0084\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0012\u0010\u0013\u00a8\u0006\u001c"}, d2 = {"Lcom/impaktsoft/globeup/views/activities/BaseActivity;", "T", "Lcom/impaktsoft/globeup/viewmodels/BaseViewModel;", "Landroidx/appcompat/app/AppCompatActivity;", "vmClass", "Lkotlin/reflect/KClass;", "(Lkotlin/reflect/KClass;)V", "activitySubtitleResourceId", "", "getActivitySubtitleResourceId", "()Ljava/lang/Integer;", "setActivitySubtitleResourceId", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "activityTitleResourceId", "getActivityTitleResourceId", "setActivityTitleResourceId", "mViewModel", "getMViewModel", "()Lcom/impaktsoft/globeup/viewmodels/BaseViewModel;", "mViewModel$delegate", "Lkotlin/Lazy;", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "setupCustomActionBar", "app_debug"})
public abstract class BaseActivity<T extends com.impaktsoft.globeup.viewmodels.BaseViewModel> extends androidx.appcompat.app.AppCompatActivity {
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy mViewModel$delegate = null;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Integer activityTitleResourceId;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Integer activitySubtitleResourceId;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    protected final T getMViewModel() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    protected java.lang.Integer getActivityTitleResourceId() {
        return null;
    }
    
    protected void setActivityTitleResourceId(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    protected final java.lang.Integer getActivitySubtitleResourceId() {
        return null;
    }
    
    protected final void setActivitySubtitleResourceId(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void setupCustomActionBar() {
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    public BaseActivity(@org.jetbrains.annotations.NotNull()
    kotlin.reflect.KClass<T> vmClass) {
        super();
    }
}