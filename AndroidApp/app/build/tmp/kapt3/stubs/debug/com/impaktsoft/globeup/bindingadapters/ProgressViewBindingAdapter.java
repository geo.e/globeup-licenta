package com.impaktsoft.globeup.bindingadapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\u00020\u0004*\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0007\u00a8\u0006\b"}, d2 = {"Lcom/impaktsoft/globeup/bindingadapters/ProgressViewBindingAdapter;", "", "()V", "setVisibilityDependingOnState", "", "Landroid/widget/ProgressBar;", "messagePOJO", "Lcom/impaktsoft/globeup/models/MessagePOJO;", "app_debug"})
public final class ProgressViewBindingAdapter {
    public static final com.impaktsoft.globeup.bindingadapters.ProgressViewBindingAdapter INSTANCE = null;
    
    @androidx.databinding.BindingAdapter(value = {"cb_isProgressVisible"})
    public static final void setVisibilityDependingOnState(@org.jetbrains.annotations.NotNull()
    android.widget.ProgressBar $this$setVisibilityDependingOnState, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.MessagePOJO messagePOJO) {
    }
    
    private ProgressViewBindingAdapter() {
        super();
    }
}