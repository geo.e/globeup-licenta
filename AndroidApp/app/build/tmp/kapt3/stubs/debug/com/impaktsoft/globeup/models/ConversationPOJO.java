package com.impaktsoft.globeup.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0012\u0018\u00002\u00020\u0001B\'\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\tB\u0007\b\u0016\u00a2\u0006\u0002\u0010\nR\u001e\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u000f\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013R\u001c\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\u001a\u0010\u0004\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0011\"\u0004\b\u0019\u0010\u0013\u00a8\u0006\u001a"}, d2 = {"Lcom/impaktsoft/globeup/models/ConversationPOJO;", "", "key", "", "name", "lastMessage", "Lcom/impaktsoft/globeup/models/MessagePOJO;", "conversationImage", "", "(Ljava/lang/String;Ljava/lang/String;Lcom/impaktsoft/globeup/models/MessagePOJO;I)V", "()V", "getConversationImage", "()Ljava/lang/Integer;", "setConversationImage", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "getKey", "()Ljava/lang/String;", "setKey", "(Ljava/lang/String;)V", "getLastMessage", "()Lcom/impaktsoft/globeup/models/MessagePOJO;", "setLastMessage", "(Lcom/impaktsoft/globeup/models/MessagePOJO;)V", "getName", "setName", "app_debug"})
public final class ConversationPOJO {
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.models.MessagePOJO lastMessage;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String key;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String name;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Integer conversationImage;
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.models.MessagePOJO getLastMessage() {
        return null;
    }
    
    public final void setLastMessage(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.models.MessagePOJO p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getKey() {
        return null;
    }
    
    public final void setKey(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getName() {
        return null;
    }
    
    public final void setName(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getConversationImage() {
        return null;
    }
    
    public final void setConversationImage(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    public ConversationPOJO(@org.jetbrains.annotations.NotNull()
    java.lang.String key, @org.jetbrains.annotations.NotNull()
    java.lang.String name, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.MessagePOJO lastMessage, int conversationImage) {
        super();
    }
    
    public ConversationPOJO() {
        super();
    }
}