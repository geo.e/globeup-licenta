package com.impaktsoft.globeup.services;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\b\u0010\n\u001a\u00020\u000bH\u0016R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\u0004R\u000e\u0010\b\u001a\u00020\tX\u0082D\u00a2\u0006\u0002\n\u0000\u00a8\u0006\f"}, d2 = {"Lcom/impaktsoft/globeup/services/RateInPlayStoreService;", "Lcom/impaktsoft/globeup/services/IRateInPlayStore;", "activityService", "Lcom/impaktsoft/globeup/services/ICurrentActivityService;", "(Lcom/impaktsoft/globeup/services/ICurrentActivityService;)V", "getActivityService", "()Lcom/impaktsoft/globeup/services/ICurrentActivityService;", "setActivityService", "packageName", "", "askRating", "", "app_debug"})
public final class RateInPlayStoreService implements com.impaktsoft.globeup.services.IRateInPlayStore {
    private final java.lang.String packageName = "com.impaktsoft.globeup";
    @org.jetbrains.annotations.NotNull()
    private com.impaktsoft.globeup.services.ICurrentActivityService activityService;
    
    @java.lang.Override()
    public void askRating() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.impaktsoft.globeup.services.ICurrentActivityService getActivityService() {
        return null;
    }
    
    public final void setActivityService(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.services.ICurrentActivityService p0) {
    }
    
    public RateInPlayStoreService(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.services.ICurrentActivityService activityService) {
        super();
    }
}