package com.impaktsoft.globeup.viewmodels;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0018\u001a\u00020\u0019J\u000e\u0010\u001a\u001a\u00020\u00192\u0006\u0010\u001b\u001a\u00020\u0005J\u0011\u0010\u001c\u001a\u00020\u0019H\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u001dJ\b\u0010\u001e\u001a\u00020\u0019H\u0002J\u0006\u0010\u001f\u001a\u00020\u0019R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\t\u001a\u00020\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\u000b\"\u0004\b\f\u0010\rR \u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\n0\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R \u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\n0\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u000f\"\u0004\b\u0013\u0010\u0011R \u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00150\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u000f\"\u0004\b\u0017\u0010\u0011\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006 "}, d2 = {"Lcom/impaktsoft/globeup/viewmodels/SearchEventsByNameViewModel;", "Lcom/impaktsoft/globeup/viewmodels/BaseEventsViewModel;", "()V", "currentCountryBinding", "Landroidx/lifecycle/MutableLiveData;", "", "currentCountryCode", "currentSearchString", "currentUserId", "isFetchMoreEventsEnable", "", "()Z", "setFetchMoreEventsEnable", "(Z)V", "isFetchingProcessRunning", "()Landroidx/lifecycle/MutableLiveData;", "setFetchingProcessRunning", "(Landroidx/lifecycle/MutableLiveData;)V", "isPlaceHolderMessageVisible", "setPlaceHolderMessageVisible", "loadingPosition", "", "getLoadingPosition", "setLoadingPosition", "backClick", "", "fetchEventsByString", "searchName", "initCurrentInfo", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "initUserEventsList", "loadMoreEvents", "app_debug"})
public final class SearchEventsByNameViewModel extends com.impaktsoft.globeup.viewmodels.BaseEventsViewModel {
    private boolean isFetchMoreEventsEnable;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isFetchingProcessRunning;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isPlaceHolderMessageVisible;
    private androidx.lifecycle.MutableLiveData<java.lang.String> currentCountryBinding;
    private java.lang.String currentUserId;
    private java.lang.String currentCountryCode;
    private java.lang.String currentSearchString;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Integer> loadingPosition;
    
    public final boolean isFetchMoreEventsEnable() {
        return false;
    }
    
    public final void setFetchMoreEventsEnable(boolean p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isFetchingProcessRunning() {
        return null;
    }
    
    public final void setFetchingProcessRunning(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isPlaceHolderMessageVisible() {
        return null;
    }
    
    public final void setPlaceHolderMessageVisible(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Integer> getLoadingPosition() {
        return null;
    }
    
    public final void setLoadingPosition(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Integer> p0) {
    }
    
    private final void initUserEventsList() {
    }
    
    public final void fetchEventsByString(@org.jetbrains.annotations.NotNull()
    java.lang.String searchName) {
    }
    
    public final void loadMoreEvents() {
    }
    
    public final void backClick() {
    }
    
    public SearchEventsByNameViewModel() {
        super();
    }
}