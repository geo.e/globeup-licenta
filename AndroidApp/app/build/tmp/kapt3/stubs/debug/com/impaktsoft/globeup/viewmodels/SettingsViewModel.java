package com.impaktsoft.globeup.viewmodels;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\t\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0012\u001a\u00020\u0013J\u0006\u0010\u0014\u001a\u00020\u0013J\u0006\u0010\u0015\u001a\u00020\u0013J\b\u0010\u0016\u001a\u00020\u0013H\u0016J\u0006\u0010\u0017\u001a\u00020\u0013J\u000e\u0010\u0018\u001a\u00020\u00132\u0006\u0010\u0019\u001a\u00020\u0004J\u0006\u0010\u001a\u001a\u00020\u0013J\u0006\u0010\u001b\u001a\u00020\u0013R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u0011\u0010\t\u001a\u00020\n8F\u00a2\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u001a\u0010\r\u001a\u00020\u000eX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000f\"\u0004\b\u0010\u0010\u0011\u00a8\u0006\u001c"}, d2 = {"Lcom/impaktsoft/globeup/viewmodels/SettingsViewModel;", "Lcom/impaktsoft/globeup/viewmodels/BaseViewModel;", "()V", "currentScaleTypeSelected", "Lcom/impaktsoft/globeup/enums/ScaleTypeEnum;", "getCurrentScaleTypeSelected", "()Lcom/impaktsoft/globeup/enums/ScaleTypeEnum;", "setCurrentScaleTypeSelected", "(Lcom/impaktsoft/globeup/enums/ScaleTypeEnum;)V", "getEditOrRegisterText", "", "getGetEditOrRegisterText", "()Ljava/lang/String;", "isUserAnonymous", "", "()Z", "setUserAnonymous", "(Z)V", "editClick", "", "inviteFriendsClick", "logout", "onResume", "rateInPlayStore", "setMetricsSystem", "scaleTypeEnum", "showDeleteDialog", "showFeedbackDialog", "app_debug"})
public final class SettingsViewModel extends com.impaktsoft.globeup.viewmodels.BaseViewModel {
    private boolean isUserAnonymous;
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.enums.ScaleTypeEnum currentScaleTypeSelected;
    
    public final boolean isUserAnonymous() {
        return false;
    }
    
    public final void setUserAnonymous(boolean p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.enums.ScaleTypeEnum getCurrentScaleTypeSelected() {
        return null;
    }
    
    public final void setCurrentScaleTypeSelected(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.enums.ScaleTypeEnum p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getGetEditOrRegisterText() {
        return null;
    }
    
    @java.lang.Override()
    public void onResume() {
    }
    
    public final void setMetricsSystem(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.enums.ScaleTypeEnum scaleTypeEnum) {
    }
    
    public final void editClick() {
    }
    
    public final void inviteFriendsClick() {
    }
    
    public final void showFeedbackDialog() {
    }
    
    public final void showDeleteDialog() {
    }
    
    public final void rateInPlayStore() {
    }
    
    public final void logout() {
    }
    
    public SettingsViewModel() {
        super();
    }
}