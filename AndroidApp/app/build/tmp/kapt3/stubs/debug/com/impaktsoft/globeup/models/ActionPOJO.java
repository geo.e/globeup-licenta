package com.impaktsoft.globeup.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u00002\u00020\u0001B\u001b\b\u0016\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006R\u001c\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001c\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000e\u00a8\u0006\u000f"}, d2 = {"Lcom/impaktsoft/globeup/models/ActionPOJO;", "", "actionName", "", "actionType", "Lcom/impaktsoft/globeup/enums/ActionType;", "(Ljava/lang/String;Lcom/impaktsoft/globeup/enums/ActionType;)V", "getActionName", "()Ljava/lang/String;", "setActionName", "(Ljava/lang/String;)V", "getActionType", "()Lcom/impaktsoft/globeup/enums/ActionType;", "setActionType", "(Lcom/impaktsoft/globeup/enums/ActionType;)V", "app_debug"})
public final class ActionPOJO {
    @org.jetbrains.annotations.Nullable()
    private java.lang.String actionName;
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.enums.ActionType actionType;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getActionName() {
        return null;
    }
    
    public final void setActionName(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.enums.ActionType getActionType() {
        return null;
    }
    
    public final void setActionType(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.enums.ActionType p0) {
    }
    
    public ActionPOJO(@org.jetbrains.annotations.Nullable()
    java.lang.String actionName, @org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.enums.ActionType actionType) {
        super();
    }
}