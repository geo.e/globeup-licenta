package com.impaktsoft.globeup.views.activities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\f\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0005\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0003H\u0014R\u001e\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0094\u000e\u00a2\u0006\u0010\n\u0002\u0010\u000b\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR$\u0010\r\u001a\u00020\u00062\u0006\u0010\f\u001a\u00020\u0006@VX\u0094\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011\u00a8\u0006\u0015"}, d2 = {"Lcom/impaktsoft/globeup/views/activities/ForgetPasswordActivity;", "Lcom/impaktsoft/globeup/views/activities/BaseBoundActivity;", "Lcom/impaktsoft/globeup/viewmodels/ForgetPasswordViewModel;", "Lcom/impaktsoft/globeup/databinding/FragmentForgetPasswordBinding;", "()V", "activityTitleResourceId", "", "getActivityTitleResourceId", "()Ljava/lang/Integer;", "setActivityTitleResourceId", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "<set-?>", "layoutId", "getLayoutId", "()I", "setLayoutId", "(I)V", "setupDataBinding", "", "binding", "app_debug"})
public final class ForgetPasswordActivity extends com.impaktsoft.globeup.views.activities.BaseBoundActivity<com.impaktsoft.globeup.viewmodels.ForgetPasswordViewModel, com.impaktsoft.globeup.databinding.FragmentForgetPasswordBinding> {
    private int layoutId;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Integer activityTitleResourceId;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected int getLayoutId() {
        return 0;
    }
    
    public void setLayoutId(int p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    protected java.lang.Integer getActivityTitleResourceId() {
        return null;
    }
    
    @java.lang.Override()
    protected void setActivityTitleResourceId(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @java.lang.Override()
    protected void setupDataBinding(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.databinding.FragmentForgetPasswordBinding binding) {
    }
    
    public ForgetPasswordActivity() {
        super(null);
    }
}