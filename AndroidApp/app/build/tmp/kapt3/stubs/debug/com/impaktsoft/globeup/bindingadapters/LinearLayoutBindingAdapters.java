package com.impaktsoft.globeup.bindingadapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\u00020\u0004*\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0007J\u0014\u0010\u0003\u001a\u00020\u0004*\u00020\u00052\u0006\u0010\b\u001a\u00020\tH\u0007\u00a8\u0006\n"}, d2 = {"Lcom/impaktsoft/globeup/bindingadapters/LinearLayoutBindingAdapters;", "", "()V", "setBackGround", "", "Landroid/widget/LinearLayout;", "gCoinPOJO", "Lcom/impaktsoft/globeup/models/GCoinPOJO;", "withdrawPOJO", "Lcom/impaktsoft/globeup/models/WithdrawPOJO;", "app_debug"})
public final class LinearLayoutBindingAdapters {
    public static final com.impaktsoft.globeup.bindingadapters.LinearLayoutBindingAdapters INSTANCE = null;
    
    @android.annotation.TargetApi(value = android.os.Build.VERSION_CODES.M)
    @androidx.databinding.BindingAdapter(value = {"cb_background"})
    public static final void setBackGround(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout $this$setBackGround, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.GCoinPOJO gCoinPOJO) {
    }
    
    @android.annotation.TargetApi(value = android.os.Build.VERSION_CODES.M)
    @androidx.databinding.BindingAdapter(value = {"cb_background"})
    public static final void setBackGround(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout $this$setBackGround, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.WithdrawPOJO withdrawPOJO) {
    }
    
    private LinearLayoutBindingAdapters() {
        super();
    }
}