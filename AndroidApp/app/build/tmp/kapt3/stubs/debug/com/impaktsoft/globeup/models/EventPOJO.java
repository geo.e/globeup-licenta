package com.impaktsoft.globeup.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\"\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\b\u0016\u0018\u00002\u00020\u0001B\u0007\b\u0016\u00a2\u0006\u0002\u0010\u0002BK\b\u0016\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\u0006\u0012\b\u0010\b\u001a\u0004\u0018\u00010\t\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u000fJ\u0013\u0010/\u001a\u0002002\b\u00101\u001a\u0004\u0018\u000102H\u0096\u0002R\u001c\u0010\u0010\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u001c\u0010\u000e\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0012\"\u0004\b\u0016\u0010\u0014R\u001e\u0010\u0017\u001a\u0004\u0018\u00010\u000bX\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u001c\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001bR\u001c\u0010\b\u001a\u0004\u0018\u00010\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\u001e\"\u0004\b\u001f\u0010 R\u001e\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010%\u001a\u0004\b!\u0010\"\"\u0004\b#\u0010$R\u001c\u0010\f\u001a\u0004\u0018\u00010\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b&\u0010\'\"\u0004\b(\u0010)R\u001c\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b*\u0010\u0012\"\u0004\b+\u0010\u0014R\u001c\u0010,\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b-\u0010\u0012\"\u0004\b.\u0010\u0014\u00a8\u00063"}, d2 = {"Lcom/impaktsoft/globeup/models/EventPOJO;", "Lcom/impaktsoft/globeup/models/IEventsItem;", "()V", "image", "", "name", "", "description", "eventType", "Lcom/impaktsoft/globeup/enums/EventType;", "time", "", "location", "Lcom/google/firebase/firestore/GeoPoint;", "eventCountry", "(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Lcom/impaktsoft/globeup/enums/EventType;Ljava/lang/Long;Lcom/google/firebase/firestore/GeoPoint;Ljava/lang/String;)V", "desciption", "getDesciption", "()Ljava/lang/String;", "setDesciption", "(Ljava/lang/String;)V", "getEventCountry", "setEventCountry", "eventDate", "getEventDate", "()Ljava/lang/Long;", "setEventDate", "(Ljava/lang/Long;)V", "Ljava/lang/Long;", "getEventType", "()Lcom/impaktsoft/globeup/enums/EventType;", "setEventType", "(Lcom/impaktsoft/globeup/enums/EventType;)V", "getImage", "()Ljava/lang/Integer;", "setImage", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "getLocation", "()Lcom/google/firebase/firestore/GeoPoint;", "setLocation", "(Lcom/google/firebase/firestore/GeoPoint;)V", "getName", "setName", "uid", "getUid", "setUid", "equals", "", "other", "", "app_debug"})
public class EventPOJO implements com.impaktsoft.globeup.models.IEventsItem {
    @org.jetbrains.annotations.Nullable()
    private java.lang.String name;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Integer image;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String desciption;
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.enums.EventType eventType;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Long eventDate;
    @org.jetbrains.annotations.Nullable()
    private com.google.firebase.firestore.GeoPoint location;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String uid;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String eventCountry;
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.String getName() {
        return null;
    }
    
    @java.lang.Override()
    public void setName(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getImage() {
        return null;
    }
    
    public final void setImage(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDesciption() {
        return null;
    }
    
    public final void setDesciption(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.enums.EventType getEventType() {
        return null;
    }
    
    public final void setEventType(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.enums.EventType p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getEventDate() {
        return null;
    }
    
    public final void setEventDate(@org.jetbrains.annotations.Nullable()
    java.lang.Long p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.google.firebase.firestore.GeoPoint getLocation() {
        return null;
    }
    
    public final void setLocation(@org.jetbrains.annotations.Nullable()
    com.google.firebase.firestore.GeoPoint p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getUid() {
        return null;
    }
    
    public final void setUid(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getEventCountry() {
        return null;
    }
    
    public final void setEventCountry(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    public EventPOJO() {
        super();
    }
    
    public EventPOJO(@org.jetbrains.annotations.Nullable()
    java.lang.Integer image, @org.jetbrains.annotations.Nullable()
    java.lang.String name, @org.jetbrains.annotations.Nullable()
    java.lang.String description, @org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.enums.EventType eventType, @org.jetbrains.annotations.Nullable()
    java.lang.Long time, @org.jetbrains.annotations.NotNull()
    com.google.firebase.firestore.GeoPoint location, @org.jetbrains.annotations.Nullable()
    java.lang.String eventCountry) {
        super();
    }
}