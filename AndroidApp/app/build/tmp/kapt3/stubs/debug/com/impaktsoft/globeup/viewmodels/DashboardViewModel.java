package com.impaktsoft.globeup.viewmodels;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010-\u001a\u00020.2\u0006\u0010/\u001a\u000200J\u0012\u00101\u001a\u00020.2\b\u00102\u001a\u0004\u0018\u00010\tH\u0002J\u0010\u00103\u001a\u00020.2\b\u00102\u001a\u0004\u0018\u00010\tJ\u0006\u00104\u001a\u00020.J\u0011\u00105\u001a\u00020.H\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u00106J\b\u00107\u001a\u00020.H\u0002J\u000e\u00108\u001a\u0002092\u0006\u0010:\u001a\u00020\rJ8\u0010;\u001a\u0016\u0012\u0004\u0012\u00020\r\u0018\u00010\bj\n\u0012\u0004\u0012\u00020\r\u0018\u0001`<2\u001a\u0010=\u001a\u0016\u0012\u0004\u0012\u00020>\u0018\u00010\bj\n\u0012\u0004\u0012\u00020>\u0018\u0001`<H\u0002J\u0006\u0010?\u001a\u00020.J\u0006\u0010@\u001a\u00020.J\u0006\u0010A\u001a\u00020.J\b\u0010B\u001a\u00020.H\u0016J\u0011\u0010C\u001a\u00020.H\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u00106J\u0006\u0010D\u001a\u00020.J\u001a\u0010E\u001a\u00020.2\u0006\u0010F\u001a\u00020\u00112\b\u0010G\u001a\u0004\u0018\u00010\rH\u0002J\b\u0010H\u001a\u00020.H\u0002J\u0011\u0010I\u001a\u00020.H\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u00106R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R \u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0012\"\u0004\b\u0013\u0010\u0014R \u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0012\"\u0004\b\u0016\u0010\u0014R(\u0010\u0017\u001a\u0010\u0012\f\u0012\n \u0018*\u0004\u0018\u00010\u00110\u00110\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0012\"\u0004\b\u0019\u0010\u0014R \u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\u0012\"\u0004\b\u001b\u0010\u0014R\u000e\u0010\u001c\u001a\u00020\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001d\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001e\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020 0\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R \u0010!\u001a\b\u0012\u0004\u0012\u00020\r0\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010\u0012\"\u0004\b#\u0010\u0014R \u0010$\u001a\b\u0012\u0004\u0012\u00020\r0\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b%\u0010\u0012\"\u0004\b&\u0010\u0014R \u0010\'\u001a\b\u0012\u0004\u0012\u00020\r0\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010\u0012\"\u0004\b)\u0010\u0014R \u0010*\u001a\b\u0012\u0004\u0012\u00020\r0\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b+\u0010\u0012\"\u0004\b,\u0010\u0014\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006J"}, d2 = {"Lcom/impaktsoft/globeup/viewmodels/DashboardViewModel;", "Lcom/impaktsoft/globeup/viewmodels/BaseEventsViewModel;", "()V", "actionAdapter", "Lcom/impaktsoft/globeup/listadapters/ActionAdapter;", "getActionAdapter", "()Lcom/impaktsoft/globeup/listadapters/ActionAdapter;", "eventsIHost", "Ljava/util/ArrayList;", "Lcom/impaktsoft/globeup/models/EventPOJO;", "eventsIJoin", "eventsImInteresting", "hostListTitle", "", "interestedListTitle", "isPlaceHolderMessageVisible", "Landroidx/lifecycle/MutableLiveData;", "", "()Landroidx/lifecycle/MutableLiveData;", "setPlaceHolderMessageVisible", "(Landroidx/lifecycle/MutableLiveData;)V", "isProgressBarVisible", "setProgressBarVisible", "isShowedActionList", "kotlin.jvm.PlatformType", "setShowedActionList", "isUserRegistered", "setUserRegistered", "isUserSubscribedToChat", "joinListTitle", "lastVMVisited", "listOfAllEvents", "Lcom/impaktsoft/globeup/models/IEventsItem;", "placeholderText", "getPlaceholderText", "setPlaceholderText", "userExperienceString", "getUserExperienceString", "setUserExperienceString", "userImageUrl", "getUserImageUrl", "setUserImageUrl", "userName", "getUserName", "setUserName", "actionClicked", "", "option", "Lcom/impaktsoft/globeup/enums/ActionType;", "addEventToHisCategory", "event", "editClick", "experienceClick", "fetchEvents", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "formatListWithCategories", "getEventState", "Lcom/impaktsoft/globeup/enums/EventState;", "eventUID", "getListOfEventsKeys", "Lkotlin/collections/ArrayList;", "listOfUserEvents", "Lcom/impaktsoft/globeup/models/UserEventPOJO;", "levelClicked", "loginClicked", "mainActionButtonClicked", "onResume", "setCurrentUserInfo", "settingsClick", "showPlaceholder", "isPlaceHolderVisible", "placeHolderString", "subscribeToEventsChat", "uploadInfo", "app_debug"})
public final class DashboardViewModel extends com.impaktsoft.globeup.viewmodels.BaseEventsViewModel {
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> userImageUrl;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> userName;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> userExperienceString;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isShowedActionList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isUserRegistered;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isProgressBarVisible;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isPlaceHolderMessageVisible;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> placeholderText;
    @org.jetbrains.annotations.NotNull()
    private final com.impaktsoft.globeup.listadapters.ActionAdapter actionAdapter = null;
    private java.util.ArrayList<com.impaktsoft.globeup.models.IEventsItem> listOfAllEvents;
    private java.util.ArrayList<com.impaktsoft.globeup.models.EventPOJO> eventsIHost;
    private java.util.ArrayList<com.impaktsoft.globeup.models.EventPOJO> eventsImInteresting;
    private java.util.ArrayList<com.impaktsoft.globeup.models.EventPOJO> eventsIJoin;
    private java.lang.String hostListTitle;
    private java.lang.String interestedListTitle;
    private java.lang.String joinListTitle;
    private java.lang.String lastVMVisited;
    private boolean isUserSubscribedToChat;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getUserImageUrl() {
        return null;
    }
    
    public final void setUserImageUrl(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getUserName() {
        return null;
    }
    
    public final void setUserName(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getUserExperienceString() {
        return null;
    }
    
    public final void setUserExperienceString(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isShowedActionList() {
        return null;
    }
    
    public final void setShowedActionList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isUserRegistered() {
        return null;
    }
    
    public final void setUserRegistered(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isProgressBarVisible() {
        return null;
    }
    
    public final void setProgressBarVisible(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isPlaceHolderMessageVisible() {
        return null;
    }
    
    public final void setPlaceHolderMessageVisible(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getPlaceholderText() {
        return null;
    }
    
    public final void setPlaceholderText(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.impaktsoft.globeup.listadapters.ActionAdapter getActionAdapter() {
        return null;
    }
    
    @java.lang.Override()
    public void onResume() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.impaktsoft.globeup.enums.EventState getEventState(@org.jetbrains.annotations.NotNull()
    java.lang.String eventUID) {
        return null;
    }
    
    public final void levelClicked() {
    }
    
    public final void loginClicked() {
    }
    
    public final void settingsClick() {
    }
    
    public final void experienceClick() {
    }
    
    public final void mainActionButtonClicked() {
    }
    
    public final void actionClicked(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.enums.ActionType option) {
    }
    
    public final void editClick(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.models.EventPOJO event) {
    }
    
    private final java.util.ArrayList<java.lang.String> getListOfEventsKeys(java.util.ArrayList<com.impaktsoft.globeup.models.UserEventPOJO> listOfUserEvents) {
        return null;
    }
    
    private final void formatListWithCategories() {
    }
    
    private final void addEventToHisCategory(com.impaktsoft.globeup.models.EventPOJO event) {
    }
    
    private final void showPlaceholder(boolean isPlaceHolderVisible, java.lang.String placeHolderString) {
    }
    
    private final void subscribeToEventsChat() {
    }
    
    public DashboardViewModel() {
        super();
    }
}