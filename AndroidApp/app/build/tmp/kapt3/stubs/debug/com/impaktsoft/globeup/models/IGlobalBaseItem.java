package com.impaktsoft.globeup.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\b\bf\u0018\u00002\u00020\u0001R\u001a\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u00a6\u000e\u00a2\u0006\f\u001a\u0004\b\u0004\u0010\u0005\"\u0004\b\u0006\u0010\u0007R\u001a\u0010\b\u001a\u0004\u0018\u00010\u0003X\u00a6\u000e\u00a2\u0006\f\u001a\u0004\b\t\u0010\u0005\"\u0004\b\n\u0010\u0007R\u001a\u0010\u000b\u001a\u0004\u0018\u00010\fX\u00a6\u000e\u00a2\u0006\f\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u001a\u0010\u0011\u001a\u0004\u0018\u00010\u0003X\u00a6\u000e\u00a2\u0006\f\u001a\u0004\b\u0012\u0010\u0005\"\u0004\b\u0013\u0010\u0007\u00a8\u0006\u0014"}, d2 = {"Lcom/impaktsoft/globeup/models/IGlobalBaseItem;", "", "events", "", "getEvents", "()Ljava/lang/Integer;", "setEvents", "(Ljava/lang/Integer;)V", "experience", "getExperience", "setExperience", "name", "", "getName", "()Ljava/lang/String;", "setName", "(Ljava/lang/String;)V", "rank", "getRank", "setRank", "app_debug"})
public abstract interface IGlobalBaseItem {
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.String getName();
    
    public abstract void setName(@org.jetbrains.annotations.Nullable()
    java.lang.String p0);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Integer getExperience();
    
    public abstract void setExperience(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Integer getEvents();
    
    public abstract void setEvents(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Integer getRank();
    
    public abstract void setRank(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0);
}