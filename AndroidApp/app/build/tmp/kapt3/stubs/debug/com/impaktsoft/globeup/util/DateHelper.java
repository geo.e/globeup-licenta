package com.impaktsoft.globeup.util;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lcom/impaktsoft/globeup/util/DateHelper;", "", "()V", "Companion", "app_debug"})
public final class DateHelper {
    public static final com.impaktsoft.globeup.util.DateHelper.Companion Companion = null;
    
    public DateHelper() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bJ\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fJ\u000e\u0010\r\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fJ\u000e\u0010\u000e\u001a\u00020\n2\u0006\u0010\u000f\u001a\u00020\u0010J\u0006\u0010\u0011\u001a\u00020\u0010\u00a8\u0006\u0012"}, d2 = {"Lcom/impaktsoft/globeup/util/DateHelper$Companion;", "", "()V", "eventWillHappendAtTime", "", "eventPOJO", "Lcom/impaktsoft/globeup/models/EventPOJO;", "eventTimeFilterEnum", "Lcom/impaktsoft/globeup/enums/EventTimeFilterEnum;", "getDateFormatForSections", "", "date", "Ljava/util/Date;", "getDateHour", "getDateString", "timeInMillis", "", "getOffsetTimeInMillis", "app_debug"})
    public static final class Companion {
        
        public final long getOffsetTimeInMillis() {
            return 0L;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getDateString(long timeInMillis) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getDateHour(@org.jetbrains.annotations.NotNull()
        java.util.Date date) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getDateFormatForSections(@org.jetbrains.annotations.NotNull()
        java.util.Date date) {
            return null;
        }
        
        public final boolean eventWillHappendAtTime(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.models.EventPOJO eventPOJO, @org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.enums.EventTimeFilterEnum eventTimeFilterEnum) {
            return false;
        }
        
        private Companion() {
            super();
        }
    }
}