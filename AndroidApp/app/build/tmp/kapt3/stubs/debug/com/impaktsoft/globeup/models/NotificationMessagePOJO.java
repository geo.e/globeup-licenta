package com.impaktsoft.globeup.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u00002\u00020\u0001B\u001b\b\u0016\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006R\u001c\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001c\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000e\u00a8\u0006\u000f"}, d2 = {"Lcom/impaktsoft/globeup/models/NotificationMessagePOJO;", "", "conversationPOJO", "Lcom/impaktsoft/globeup/models/ConversationPOJO;", "messagePOJO", "Lcom/impaktsoft/globeup/models/MessagePOJO;", "(Lcom/impaktsoft/globeup/models/ConversationPOJO;Lcom/impaktsoft/globeup/models/MessagePOJO;)V", "getConversationPOJO", "()Lcom/impaktsoft/globeup/models/ConversationPOJO;", "setConversationPOJO", "(Lcom/impaktsoft/globeup/models/ConversationPOJO;)V", "getMessagePOJO", "()Lcom/impaktsoft/globeup/models/MessagePOJO;", "setMessagePOJO", "(Lcom/impaktsoft/globeup/models/MessagePOJO;)V", "app_debug"})
public final class NotificationMessagePOJO {
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.models.ConversationPOJO conversationPOJO;
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.models.MessagePOJO messagePOJO;
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.models.ConversationPOJO getConversationPOJO() {
        return null;
    }
    
    public final void setConversationPOJO(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.models.ConversationPOJO p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.models.MessagePOJO getMessagePOJO() {
        return null;
    }
    
    public final void setMessagePOJO(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.models.MessagePOJO p0) {
    }
    
    public NotificationMessagePOJO(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.models.ConversationPOJO conversationPOJO, @org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.models.MessagePOJO messagePOJO) {
        super();
    }
}