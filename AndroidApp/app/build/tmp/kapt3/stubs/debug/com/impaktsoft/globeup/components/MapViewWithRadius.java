package com.impaktsoft.globeup.components;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000p\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u000f\b\u0016\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006B\u0017\b\u0016\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\tB\u001f\b\u0016\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\fB\u0017\b\u0016\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u0012\u0010\"\u001a\u00020#2\b\u0010$\u001a\u0004\u0018\u00010%H\u0016J\u0010\u0010&\u001a\u00020\'2\u0006\u0010(\u001a\u00020\u0002H\u0016J\b\u0010)\u001a\u00020\'H\u0007J\b\u0010*\u001a\u00020\'H\u0007J\b\u0010+\u001a\u00020\'H\u0007J\b\u0010,\u001a\u00020\'H\u0007J\b\u0010-\u001a\u00020\'H\u0007J\b\u0010.\u001a\u00020\'H\u0007J\u0012\u0010/\u001a\u00020\'2\b\u00100\u001a\u0004\u0018\u000101H\u0016J\u000e\u00102\u001a\u00020\'2\u0006\u00103\u001a\u00020\u0018J\u0010\u00104\u001a\u00020\'2\b\u00105\u001a\u0004\u0018\u00010!J\u000e\u00106\u001a\u00020\'2\u0006\u00107\u001a\u00020\u000bJ\b\u00108\u001a\u00020\'H\u0007R\u001c\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0016X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0017\u001a\u0004\u0018\u00010\u0018X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR\u0010\u0010\u001d\u001a\u0004\u0018\u00010\u001eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001f\u001a\u0004\u0018\u00010\u0002X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010 \u001a\u0004\u0018\u00010!X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u00069"}, d2 = {"Lcom/impaktsoft/globeup/components/MapViewWithRadius;", "Lcom/google/android/gms/maps/MapView;", "Lcom/google/android/gms/maps/OnMapReadyCallback;", "Landroidx/lifecycle/LifecycleObserver;", "var1", "Landroid/content/Context;", "(Landroid/content/Context;)V", "var2", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "var3", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "Lcom/google/android/gms/maps/GoogleMapOptions;", "(Landroid/content/Context;Lcom/google/android/gms/maps/GoogleMapOptions;)V", "coordonates", "Lcom/google/android/gms/maps/model/LatLng;", "getCoordonates", "()Lcom/google/android/gms/maps/model/LatLng;", "setCoordonates", "(Lcom/google/android/gms/maps/model/LatLng;)V", "locationManager", "Landroid/location/LocationManager;", "locationReady", "Lcom/impaktsoft/globeup/listeners/ILocationIsReady;", "getLocationReady", "()Lcom/impaktsoft/globeup/listeners/ILocationIsReady;", "setLocationReady", "(Lcom/impaktsoft/globeup/listeners/ILocationIsReady;)V", "mMap", "Lcom/impaktsoft/globeup/components/MapWithRadius;", "mMapReadyCallback", "mapBundle", "Landroid/os/Bundle;", "dispatchTouchEvent", "", "ev", "Landroid/view/MotionEvent;", "getMapAsync", "", "callback", "lifecycleOnCreate", "lifecycleOnDestroy", "lifecycleOnPause", "lifecycleOnResume", "lifecycleOnStart", "lifecycleOnStop", "onMapReady", "p0", "Lcom/google/android/gms/maps/GoogleMap;", "setLocationListener", "locationListener", "setMapBundle", "bundle", "setMapCircle", "radius", "updateMyLocation", "app_debug"})
public final class MapViewWithRadius extends com.google.android.gms.maps.MapView implements com.google.android.gms.maps.OnMapReadyCallback, androidx.lifecycle.LifecycleObserver {
    private com.impaktsoft.globeup.components.MapWithRadius mMap;
    private android.location.LocationManager locationManager;
    private com.google.android.gms.maps.OnMapReadyCallback mMapReadyCallback;
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.listeners.ILocationIsReady locationReady;
    private android.os.Bundle mapBundle;
    @org.jetbrains.annotations.Nullable()
    private com.google.android.gms.maps.model.LatLng coordonates;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.listeners.ILocationIsReady getLocationReady() {
        return null;
    }
    
    public final void setLocationReady(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.listeners.ILocationIsReady p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.google.android.gms.maps.model.LatLng getCoordonates() {
        return null;
    }
    
    public final void setCoordonates(@org.jetbrains.annotations.Nullable()
    com.google.android.gms.maps.model.LatLng p0) {
    }
    
    @java.lang.Override()
    public void getMapAsync(@org.jetbrains.annotations.NotNull()
    com.google.android.gms.maps.OnMapReadyCallback callback) {
    }
    
    @java.lang.Override()
    public void onMapReady(@org.jetbrains.annotations.Nullable()
    com.google.android.gms.maps.GoogleMap p0) {
    }
    
    public final void setLocationListener(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.listeners.ILocationIsReady locationListener) {
    }
    
    @android.annotation.SuppressLint(value = {"MissingPermission"})
    public final void updateMyLocation() {
    }
    
    public final void setMapCircle(int radius) {
    }
    
    public final void setMapBundle(@org.jetbrains.annotations.Nullable()
    android.os.Bundle bundle) {
    }
    
    @java.lang.Override()
    public boolean dispatchTouchEvent(@org.jetbrains.annotations.Nullable()
    android.view.MotionEvent ev) {
        return false;
    }
    
    @androidx.lifecycle.OnLifecycleEvent(value = androidx.lifecycle.Lifecycle.Event.ON_CREATE)
    public final void lifecycleOnCreate() {
    }
    
    @androidx.lifecycle.OnLifecycleEvent(value = androidx.lifecycle.Lifecycle.Event.ON_RESUME)
    public final void lifecycleOnResume() {
    }
    
    @androidx.lifecycle.OnLifecycleEvent(value = androidx.lifecycle.Lifecycle.Event.ON_START)
    public final void lifecycleOnStart() {
    }
    
    @androidx.lifecycle.OnLifecycleEvent(value = androidx.lifecycle.Lifecycle.Event.ON_STOP)
    public final void lifecycleOnStop() {
    }
    
    @androidx.lifecycle.OnLifecycleEvent(value = androidx.lifecycle.Lifecycle.Event.ON_PAUSE)
    public final void lifecycleOnPause() {
    }
    
    @androidx.lifecycle.OnLifecycleEvent(value = androidx.lifecycle.Lifecycle.Event.ON_DESTROY)
    public final void lifecycleOnDestroy() {
    }
    
    public MapViewWithRadius(@org.jetbrains.annotations.NotNull()
    android.content.Context var1) {
        super(null);
    }
    
    public MapViewWithRadius(@org.jetbrains.annotations.NotNull()
    android.content.Context var1, @org.jetbrains.annotations.NotNull()
    android.util.AttributeSet var2) {
        super(null);
    }
    
    public MapViewWithRadius(@org.jetbrains.annotations.NotNull()
    android.content.Context var1, @org.jetbrains.annotations.NotNull()
    android.util.AttributeSet var2, int var3) {
        super(null);
    }
    
    public MapViewWithRadius(@org.jetbrains.annotations.NotNull()
    android.content.Context var1, @org.jetbrains.annotations.NotNull()
    com.google.android.gms.maps.GoogleMapOptions var2) {
        super(null);
    }
}