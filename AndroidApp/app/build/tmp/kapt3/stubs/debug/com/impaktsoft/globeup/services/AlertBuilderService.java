package com.impaktsoft.globeup.services;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lcom/impaktsoft/globeup/services/AlertBuilderService;", "Lcom/impaktsoft/globeup/services/IAlertBuilderService;", "activityService", "Lcom/impaktsoft/globeup/services/ICurrentActivityService;", "(Lcom/impaktsoft/globeup/services/ICurrentActivityService;)V", "showAlertDialog", "", "listOfOptions", "Lcom/impaktsoft/globeup/models/AlertBuilderSettings;", "app_debug"})
public final class AlertBuilderService implements com.impaktsoft.globeup.services.IAlertBuilderService {
    private final com.impaktsoft.globeup.services.ICurrentActivityService activityService = null;
    
    @java.lang.Override()
    public void showAlertDialog(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.AlertBuilderSettings listOfOptions) {
    }
    
    public AlertBuilderService(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.services.ICurrentActivityService activityService) {
        super();
    }
}