package com.impaktsoft.globeup.services.database;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0019\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0006H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000bJ\u001d\u0010\f\u001a\u0004\u0018\u00010\r2\b\u0010\n\u001a\u0004\u0018\u00010\u0006H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000bJ!\u0010\u000e\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u00062\u0006\u0010\u000f\u001a\u00020\rH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0010R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0011"}, d2 = {"Lcom/impaktsoft/globeup/services/database/UserInfoDAO;", "Lcom/impaktsoft/globeup/services/database/IUserInfoDAO;", "fireStore", "Lcom/google/firebase/firestore/FirebaseFirestore;", "(Lcom/google/firebase/firestore/FirebaseFirestore;)V", "defaultValue", "", "userInfoCollection", "delete", "", "backendUserId", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "fetch", "Lcom/impaktsoft/globeup/models/UserInfo;", "store", "userInfo", "(Ljava/lang/String;Lcom/impaktsoft/globeup/models/UserInfo;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class UserInfoDAO implements com.impaktsoft.globeup.services.database.IUserInfoDAO {
    private final java.lang.String userInfoCollection = "UserInfo";
    private final java.lang.String defaultValue = "--";
    private final com.google.firebase.firestore.FirebaseFirestore fireStore = null;
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object store(@org.jetbrains.annotations.NotNull()
    java.lang.String backendUserId, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.UserInfo userInfo, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p2) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object fetch(@org.jetbrains.annotations.Nullable()
    java.lang.String backendUserId, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.impaktsoft.globeup.models.UserInfo> p1) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object delete(@org.jetbrains.annotations.NotNull()
    java.lang.String backendUserId, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p1) {
        return null;
    }
    
    public UserInfoDAO(@org.jetbrains.annotations.NotNull()
    com.google.firebase.firestore.FirebaseFirestore fireStore) {
        super();
    }
}