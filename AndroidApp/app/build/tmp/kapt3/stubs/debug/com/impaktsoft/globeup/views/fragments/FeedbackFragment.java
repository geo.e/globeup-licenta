package com.impaktsoft.globeup.views.fragments;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0005\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0006J\u001a\u0010\u0011\u001a\u00020\u000f2\u0006\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0016J\u0010\u0010\u0016\u001a\u00020\u000f2\u0006\u0010\u0017\u001a\u00020\u0003H\u0014R\u0014\u0010\u0005\u001a\u00020\u0006X\u0094D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR$\u0010\n\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\u0006@VX\u0094\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\b\"\u0004\b\f\u0010\r\u00a8\u0006\u0018"}, d2 = {"Lcom/impaktsoft/globeup/views/fragments/FeedbackFragment;", "Lcom/impaktsoft/globeup/views/fragments/BaseBoundFragment;", "Lcom/impaktsoft/globeup/viewmodels/FeedbackViewModel;", "Lcom/impaktsoft/globeup/databinding/FragmentFeedbackBinding;", "()V", "fragmentNameResourceID", "", "getFragmentNameResourceID", "()I", "<set-?>", "layoutId", "getLayoutId", "setLayoutId", "(I)V", "initList", "", "spanCount", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "setupDataBinding", "binding", "app_debug"})
public final class FeedbackFragment extends com.impaktsoft.globeup.views.fragments.BaseBoundFragment<com.impaktsoft.globeup.viewmodels.FeedbackViewModel, com.impaktsoft.globeup.databinding.FragmentFeedbackBinding> {
    private int layoutId;
    private final int fragmentNameResourceID = com.impaktsoft.globeup.R.string.feedback;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected int getLayoutId() {
        return 0;
    }
    
    public void setLayoutId(int p0) {
    }
    
    @java.lang.Override()
    protected int getFragmentNameResourceID() {
        return 0;
    }
    
    @java.lang.Override()
    protected void setupDataBinding(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.databinding.FragmentFeedbackBinding binding) {
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void initList(int spanCount) {
    }
    
    public FeedbackFragment() {
        super(null);
    }
}