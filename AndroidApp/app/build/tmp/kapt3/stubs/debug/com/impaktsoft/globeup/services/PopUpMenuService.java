package com.impaktsoft.globeup.services;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0006H\u0016J\b\u0010\f\u001a\u00020\nH\u0002J\b\u0010\r\u001a\u00020\nH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"}, d2 = {"Lcom/impaktsoft/globeup/services/PopUpMenuService;", "Lcom/impaktsoft/globeup/services/IPopUpMenuService;", "currentActivityService", "Lcom/impaktsoft/globeup/services/ICurrentActivityService;", "(Lcom/impaktsoft/globeup/services/ICurrentActivityService;)V", "menuSettings", "Lcom/impaktsoft/globeup/models/PopUpMenuSettingsPOJO;", "popUpMenu", "Landroid/widget/PopupMenu;", "initializeMenu", "", "settings", "setupMenuOptions", "showMenu", "app_debug"})
public final class PopUpMenuService implements com.impaktsoft.globeup.services.IPopUpMenuService {
    private android.widget.PopupMenu popUpMenu;
    private com.impaktsoft.globeup.models.PopUpMenuSettingsPOJO menuSettings;
    private final com.impaktsoft.globeup.services.ICurrentActivityService currentActivityService = null;
    
    @java.lang.Override()
    public void initializeMenu(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.PopUpMenuSettingsPOJO settings) {
    }
    
    private final void setupMenuOptions() {
    }
    
    @java.lang.Override()
    public void showMenu() {
    }
    
    public PopUpMenuService(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.services.ICurrentActivityService currentActivityService) {
        super();
    }
}