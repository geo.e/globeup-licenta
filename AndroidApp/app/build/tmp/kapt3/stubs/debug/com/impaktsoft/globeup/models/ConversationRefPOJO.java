package com.impaktsoft.globeup.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0011\b\u0016\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004B\u0007\b\u0016\u00a2\u0006\u0002\u0010\u0005R\u001c\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\u0004\u00a8\u0006\t"}, d2 = {"Lcom/impaktsoft/globeup/models/ConversationRefPOJO;", "", "conversationKey", "", "(Ljava/lang/String;)V", "()V", "getConversationKey", "()Ljava/lang/String;", "setConversationKey", "app_debug"})
public final class ConversationRefPOJO {
    @org.jetbrains.annotations.Nullable()
    private java.lang.String conversationKey;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getConversationKey() {
        return null;
    }
    
    public final void setConversationKey(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    public ConversationRefPOJO(@org.jetbrains.annotations.Nullable()
    java.lang.String conversationKey) {
        super();
    }
    
    public ConversationRefPOJO() {
        super();
    }
}