package com.impaktsoft.globeup.views.fragments;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\b&\u0018\u0000*\n\b\u0000\u0010\u0001 \u0001*\u00020\u0002*\n\b\u0001\u0010\u0003 \u0000*\u00020\u00042\b\u0012\u0004\u0012\u0002H\u00010\u0005B\u0013\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00028\u00000\u0007\u00a2\u0006\u0002\u0010\bJ&\u0010\t\u001a\u0004\u0018\u00010\n2\u0006\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0016J\u0015\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00028\u0001H$\u00a2\u0006\u0002\u0010\u0014\u00a8\u0006\u0015"}, d2 = {"Lcom/impaktsoft/globeup/views/fragments/BaseBoundFragment;", "T", "Lcom/impaktsoft/globeup/viewmodels/BaseViewModel;", "K", "Landroidx/databinding/ViewDataBinding;", "Lcom/impaktsoft/globeup/views/fragments/BaseFragment;", "vmClass", "Lkotlin/reflect/KClass;", "(Lkotlin/reflect/KClass;)V", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "setupDataBinding", "", "binding", "(Landroidx/databinding/ViewDataBinding;)V", "app_debug"})
public abstract class BaseBoundFragment<T extends com.impaktsoft.globeup.viewmodels.BaseViewModel, K extends androidx.databinding.ViewDataBinding> extends com.impaktsoft.globeup.views.fragments.BaseFragment<T> {
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    protected abstract void setupDataBinding(@org.jetbrains.annotations.NotNull()
    K binding);
    
    public BaseBoundFragment(@org.jetbrains.annotations.NotNull()
    kotlin.reflect.KClass<T> vmClass) {
        super(null);
    }
}