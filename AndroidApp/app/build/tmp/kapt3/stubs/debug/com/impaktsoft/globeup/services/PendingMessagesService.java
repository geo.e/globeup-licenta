package com.impaktsoft.globeup.services;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ!\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\fH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u001bJ1\u0010\u001c\u001a\u0012\u0012\u0004\u0012\u00020\u00190\u001dj\b\u0012\u0004\u0012\u00020\u0019`\u001e2\u0006\u0010\u001f\u001a\u00020\f2\u0006\u0010\u001a\u001a\u00020\fH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010 J!\u0010!\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\fH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u001bJ\u0018\u0010\"\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\fH\u0003J\u0018\u0010#\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\fH\u0002J\u0019\u0010$\u001a\u00020\u00172\u0006\u0010\u001f\u001a\u00020\fH\u0097@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010%R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082D\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\fX\u0082D\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\fX\u0082D\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006&"}, d2 = {"Lcom/impaktsoft/globeup/services/PendingMessagesService;", "Lcom/impaktsoft/globeup/services/IPendingMessagesService;", "databaseService", "Lcom/impaktsoft/globeup/services/database/IDatabaseService;", "locationService", "Lcom/impaktsoft/globeup/services/ILocationService;", "activityService", "Lcom/impaktsoft/globeup/services/ICurrentActivityService;", "fileStoreService", "Lcom/impaktsoft/globeup/services/IFileStoreService;", "(Lcom/impaktsoft/globeup/services/database/IDatabaseService;Lcom/impaktsoft/globeup/services/ILocationService;Lcom/impaktsoft/globeup/services/ICurrentActivityService;Lcom/impaktsoft/globeup/services/IFileStoreService;)V", "conversationCollection", "", "currentLocation", "Lcom/google/android/gms/maps/model/LatLng;", "fireStore", "Lcom/google/firebase/firestore/FirebaseFirestore;", "pendingListCollection", "sendPendingTask", "Lcom/google/android/gms/tasks/Task;", "Lcom/google/firebase/firestore/QuerySnapshot;", "senderKeyPath", "addMessageInPendingList", "", "message", "Lcom/impaktsoft/globeup/models/MessagePOJO;", "conversationKey", "(Lcom/impaktsoft/globeup/models/MessagePOJO;Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getPendingMessages", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "userKey", "(Ljava/lang/String;Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "removeMessageFromPendingList", "resendImageMessage", "resendLocationMessage", "resendPendingMessages", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class PendingMessagesService implements com.impaktsoft.globeup.services.IPendingMessagesService {
    private final java.lang.String conversationCollection = "Conversations";
    private final java.lang.String pendingListCollection = "PendingList";
    private final java.lang.String senderKeyPath = "senderKey";
    private final com.google.firebase.firestore.FirebaseFirestore fireStore = null;
    private com.google.android.gms.maps.model.LatLng currentLocation;
    private com.google.android.gms.tasks.Task<com.google.firebase.firestore.QuerySnapshot> sendPendingTask;
    private final com.impaktsoft.globeup.services.database.IDatabaseService databaseService = null;
    private final com.impaktsoft.globeup.services.ILocationService locationService = null;
    private final com.impaktsoft.globeup.services.ICurrentActivityService activityService = null;
    private final com.impaktsoft.globeup.services.IFileStoreService fileStoreService = null;
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object addMessageInPendingList(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.MessagePOJO message, @org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p2) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object removeMessageFromPendingList(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.MessagePOJO message, @org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p2) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @androidx.annotation.RequiresApi(value = android.os.Build.VERSION_CODES.P)
    @java.lang.Override()
    public java.lang.Object resendPendingMessages(@org.jetbrains.annotations.NotNull()
    java.lang.String userKey, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p1) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object getPendingMessages(@org.jetbrains.annotations.NotNull()
    java.lang.String userKey, @org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.ArrayList<com.impaktsoft.globeup.models.MessagePOJO>> p2) {
        return null;
    }
    
    private final void resendLocationMessage(com.impaktsoft.globeup.models.MessagePOJO message, java.lang.String conversationKey) {
    }
    
    @androidx.annotation.RequiresApi(value = android.os.Build.VERSION_CODES.P)
    private final void resendImageMessage(com.impaktsoft.globeup.models.MessagePOJO message, java.lang.String conversationKey) {
    }
    
    public PendingMessagesService(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.services.database.IDatabaseService databaseService, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.services.ILocationService locationService, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.services.ICurrentActivityService activityService, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.services.IFileStoreService fileStoreService) {
        super();
    }
}