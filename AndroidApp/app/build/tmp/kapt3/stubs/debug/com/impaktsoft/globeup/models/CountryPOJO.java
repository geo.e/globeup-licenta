package com.impaktsoft.globeup.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\n\u0018\u00002\u00020\u0001B\u001d\b\u0016\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006R\u001c\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001e\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u000e\u001a\u0004\b\u0004\u0010\u000b\"\u0004\b\f\u0010\r\u00a8\u0006\u000f"}, d2 = {"Lcom/impaktsoft/globeup/models/CountryPOJO;", "", "countryName", "", "isSelected", "", "(Ljava/lang/String;Ljava/lang/Boolean;)V", "getCountryName", "()Ljava/lang/String;", "setCountryName", "(Ljava/lang/String;)V", "()Ljava/lang/Boolean;", "setSelected", "(Ljava/lang/Boolean;)V", "Ljava/lang/Boolean;", "app_debug"})
public final class CountryPOJO {
    @org.jetbrains.annotations.Nullable()
    private java.lang.String countryName;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Boolean isSelected;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCountryName() {
        return null;
    }
    
    public final void setCountryName(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean isSelected() {
        return null;
    }
    
    public final void setSelected(@org.jetbrains.annotations.Nullable()
    java.lang.Boolean p0) {
    }
    
    public CountryPOJO(@org.jetbrains.annotations.Nullable()
    java.lang.String countryName, @org.jetbrains.annotations.Nullable()
    java.lang.Boolean isSelected) {
        super();
    }
}