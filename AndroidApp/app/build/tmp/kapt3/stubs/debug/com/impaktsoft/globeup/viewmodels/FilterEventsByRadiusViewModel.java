package com.impaktsoft.globeup.viewmodels;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u001f\u001a\u00020 J\u0006\u0010!\u001a\u00020 J\u0006\u0010\"\u001a\u00020 R \u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\u001a\u0010\n\u001a\u00020\u000bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\f\"\u0004\b\r\u0010\u000eR \u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00100\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0007\"\u0004\b\u0012\u0010\tR \u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00100\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0007\"\u0004\b\u0015\u0010\tR\u001c\u0010\u0016\u001a\u0004\u0018\u00010\u0017X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001bR \u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00100\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\u0007\"\u0004\b\u001e\u0010\t\u00a8\u0006#"}, d2 = {"Lcom/impaktsoft/globeup/viewmodels/FilterEventsByRadiusViewModel;", "Lcom/impaktsoft/globeup/viewmodels/BaseViewModel;", "()V", "coords", "Landroidx/lifecycle/MutableLiveData;", "Lcom/google/android/gms/maps/model/LatLng;", "getCoords", "()Landroidx/lifecycle/MutableLiveData;", "setCoords", "(Landroidx/lifecycle/MutableLiveData;)V", "isMapReady", "", "()Z", "setMapReady", "(Z)V", "mapVisibility", "", "getMapVisibility", "setMapVisibility", "progress", "getProgress", "setProgress", "scaleType", "Lcom/impaktsoft/globeup/enums/ScaleTypeEnum;", "getScaleType", "()Lcom/impaktsoft/globeup/enums/ScaleTypeEnum;", "setScaleType", "(Lcom/impaktsoft/globeup/enums/ScaleTypeEnum;)V", "spinnerVisibility", "getSpinnerVisibility", "setSpinnerVisibility", "doneClick", "", "exitClick", "locationIsReady", "app_debug"})
public final class FilterEventsByRadiusViewModel extends com.impaktsoft.globeup.viewmodels.BaseViewModel {
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Integer> progress;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Integer> spinnerVisibility;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Integer> mapVisibility;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<com.google.android.gms.maps.model.LatLng> coords;
    private boolean isMapReady;
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.enums.ScaleTypeEnum scaleType;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Integer> getProgress() {
        return null;
    }
    
    public final void setProgress(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Integer> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Integer> getSpinnerVisibility() {
        return null;
    }
    
    public final void setSpinnerVisibility(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Integer> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Integer> getMapVisibility() {
        return null;
    }
    
    public final void setMapVisibility(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Integer> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.google.android.gms.maps.model.LatLng> getCoords() {
        return null;
    }
    
    public final void setCoords(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<com.google.android.gms.maps.model.LatLng> p0) {
    }
    
    public final boolean isMapReady() {
        return false;
    }
    
    public final void setMapReady(boolean p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.enums.ScaleTypeEnum getScaleType() {
        return null;
    }
    
    public final void setScaleType(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.enums.ScaleTypeEnum p0) {
    }
    
    public final void exitClick() {
    }
    
    public final void doneClick() {
    }
    
    public final void locationIsReady() {
    }
    
    public FilterEventsByRadiusViewModel() {
        super();
    }
}