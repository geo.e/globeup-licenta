package com.impaktsoft.globeup.viewmodels;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016J\u0006\u0010\u0017\u001a\u00020\u0014J\b\u0010\u0018\u001a\u00020\u0014H\u0002J\n\u0010\u0019\u001a\u0004\u0018\u00010\u000bH\u0002J\b\u0010\u001a\u001a\u00020\u0011H\u0002J\u0006\u0010\u001b\u001a\u00020\u0014R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR \u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR \u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00110\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\r\"\u0004\b\u0012\u0010\u000f\u00a8\u0006\u001c"}, d2 = {"Lcom/impaktsoft/globeup/viewmodels/FeedbackViewModel;", "Lcom/impaktsoft/globeup/viewmodels/BaseViewModel;", "()V", "adapter", "Lcom/impaktsoft/globeup/listadapters/FeedbackAdapter;", "getAdapter", "()Lcom/impaktsoft/globeup/listadapters/FeedbackAdapter;", "setAdapter", "(Lcom/impaktsoft/globeup/listadapters/FeedbackAdapter;)V", "feedbackDescription", "Landroidx/lifecycle/MutableLiveData;", "", "getFeedbackDescription", "()Landroidx/lifecycle/MutableLiveData;", "setFeedbackDescription", "(Landroidx/lifecycle/MutableLiveData;)V", "isProgressVisible", "", "setProgressVisible", "checkCurrentButton", "", "position", "", "close", "deselectAllCategories", "getSelectedCategory", "isCategorySelected", "sendFeedback", "app_debug"})
public final class FeedbackViewModel extends com.impaktsoft.globeup.viewmodels.BaseViewModel {
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.listadapters.FeedbackAdapter adapter;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isProgressVisible;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> feedbackDescription;
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.listadapters.FeedbackAdapter getAdapter() {
        return null;
    }
    
    public final void setAdapter(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.listadapters.FeedbackAdapter p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isProgressVisible() {
        return null;
    }
    
    public final void setProgressVisible(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getFeedbackDescription() {
        return null;
    }
    
    public final void setFeedbackDescription(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    public final void checkCurrentButton(int position) {
    }
    
    public final void sendFeedback() {
    }
    
    public final void close() {
    }
    
    private final boolean isCategorySelected() {
        return false;
    }
    
    private final java.lang.String getSelectedCategory() {
        return null;
    }
    
    private final void deselectAllCategories() {
    }
    
    public FeedbackViewModel() {
        super();
    }
}