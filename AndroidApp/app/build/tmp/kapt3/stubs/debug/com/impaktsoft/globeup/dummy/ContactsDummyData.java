package com.impaktsoft.globeup.dummy;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lcom/impaktsoft/globeup/dummy/ContactsDummyData;", "", "()V", "Companion", "app_debug"})
public final class ContactsDummyData {
    @org.jetbrains.annotations.Nullable()
    private static java.lang.String imageUrl;
    @org.jetbrains.annotations.NotNull()
    private static final java.util.List<com.impaktsoft.globeup.models.UserPOJO> contactsList = null;
    public static final com.impaktsoft.globeup.dummy.ContactsDummyData.Companion Companion = null;
    
    public ContactsDummyData() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0017\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u001c\u0010\b\u001a\u0004\u0018\u00010\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\r\u00a8\u0006\u000e"}, d2 = {"Lcom/impaktsoft/globeup/dummy/ContactsDummyData$Companion;", "", "()V", "contactsList", "", "Lcom/impaktsoft/globeup/models/UserPOJO;", "getContactsList", "()Ljava/util/List;", "imageUrl", "", "getImageUrl", "()Ljava/lang/String;", "setImageUrl", "(Ljava/lang/String;)V", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getImageUrl() {
            return null;
        }
        
        public final void setImageUrl(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<com.impaktsoft.globeup.models.UserPOJO> getContactsList() {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}