package com.impaktsoft.globeup.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u000b\u0018\u00002\u00020\u0001B\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006B\u0007\b\u0016\u00a2\u0006\u0002\u0010\u0007R\u001c\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR\u001c\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000f\u00a8\u0006\u0010"}, d2 = {"Lcom/impaktsoft/globeup/models/UserEventPOJO;", "", "eventState", "Lcom/impaktsoft/globeup/enums/EventState;", "eventKey", "", "(Lcom/impaktsoft/globeup/enums/EventState;Ljava/lang/String;)V", "()V", "getEventKey", "()Ljava/lang/String;", "setEventKey", "(Ljava/lang/String;)V", "getEventState", "()Lcom/impaktsoft/globeup/enums/EventState;", "setEventState", "(Lcom/impaktsoft/globeup/enums/EventState;)V", "app_debug"})
public final class UserEventPOJO {
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.enums.EventState eventState;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String eventKey;
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.enums.EventState getEventState() {
        return null;
    }
    
    public final void setEventState(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.enums.EventState p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getEventKey() {
        return null;
    }
    
    public final void setEventKey(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    public UserEventPOJO(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.enums.EventState eventState, @org.jetbrains.annotations.NotNull()
    java.lang.String eventKey) {
        super();
    }
    
    public UserEventPOJO() {
        super();
    }
}