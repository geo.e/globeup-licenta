package com.impaktsoft.globeup.viewmodels;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\b\n\u0002\u0010\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u00109\u001a\u00020:J\u0017\u0010;\u001a\u0004\u0018\u00010\u00042\u0006\u0010<\u001a\u00020\u0004H\u0002\u00a2\u0006\u0002\u0010=J\b\u0010>\u001a\u00020:H\u0002J\b\u0010?\u001a\u00020:H\u0002R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R \u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00040\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R \u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00120\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u000e\"\u0004\b\u0014\u0010\u0010R \u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00160\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u000e\"\u0004\b\u0018\u0010\u0010R \u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u001a0\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001b\u0010\u000e\"\u0004\b\u001c\u0010\u0010R \u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00160\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001e\u0010\u000e\"\u0004\b\u001f\u0010\u0010R\u0016\u0010 \u001a\n !*\u0004\u0018\u00010\u00120\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R \u0010\"\u001a\b\u0012\u0004\u0012\u00020#0\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b$\u0010\u000e\"\u0004\b%\u0010\u0010R.\u0010&\u001a\u0016\u0012\u0004\u0012\u00020\u0004\u0018\u00010\'j\n\u0012\u0004\u0012\u00020\u0004\u0018\u0001`(X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b)\u0010*\"\u0004\b+\u0010,R \u0010-\u001a\b\u0012\u0004\u0012\u00020.0\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b-\u0010\u000e\"\u0004\b/\u0010\u0010R \u00100\u001a\b\u0012\u0004\u0012\u00020#01X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b2\u00103\"\u0004\b4\u00105R \u00106\u001a\b\u0012\u0004\u0012\u00020\u00120\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b7\u0010\u000e\"\u0004\b8\u0010\u0010\u00a8\u0006@"}, d2 = {"Lcom/impaktsoft/globeup/viewmodels/EditEventViewModel;", "Lcom/impaktsoft/globeup/viewmodels/BaseViewModel;", "()V", "c", "", "getC", "()I", "setC", "(I)V", "currentEvent", "Lcom/impaktsoft/globeup/models/EventPOJO;", "currentImageIndex", "Landroidx/lifecycle/MutableLiveData;", "getCurrentImageIndex", "()Landroidx/lifecycle/MutableLiveData;", "setCurrentImageIndex", "(Landroidx/lifecycle/MutableLiveData;)V", "datePickerCalendar", "Ljava/util/Calendar;", "getDatePickerCalendar", "setDatePickerCalendar", "eventDescription", "", "getEventDescription", "setEventDescription", "eventLocation", "Lcom/google/android/gms/maps/model/LatLng;", "getEventLocation", "setEventLocation", "eventName", "getEventName", "setEventName", "eventTimeInUTC", "kotlin.jvm.PlatformType", "eventType", "Lcom/impaktsoft/globeup/enums/EventType;", "getEventType", "setEventType", "images", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "getImages", "()Ljava/util/ArrayList;", "setImages", "(Ljava/util/ArrayList;)V", "isLoading", "", "setLoading", "spinnerEntries", "", "getSpinnerEntries", "()Ljava/util/List;", "setSpinnerEntries", "(Ljava/util/List;)V", "timePickerCalendar", "getTimePickerCalendar", "setTimePickerCalendar", "editClick", "", "getImageIndex", "imageId", "(I)Ljava/lang/Integer;", "setupEventDate", "setupFields", "app_debug"})
public final class EditEventViewModel extends com.impaktsoft.globeup.viewmodels.BaseViewModel {
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Integer> currentImageIndex;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> eventName;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> eventDescription;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<com.impaktsoft.globeup.enums.EventType> eventType;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<com.google.android.gms.maps.model.LatLng> eventLocation;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isLoading;
    @org.jetbrains.annotations.Nullable()
    private java.util.ArrayList<java.lang.Integer> images;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.Calendar> datePickerCalendar;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.Calendar> timePickerCalendar;
    @org.jetbrains.annotations.NotNull()
    private java.util.List<? extends com.impaktsoft.globeup.enums.EventType> spinnerEntries;
    private com.impaktsoft.globeup.models.EventPOJO currentEvent;
    private java.util.Calendar eventTimeInUTC;
    private int c;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Integer> getCurrentImageIndex() {
        return null;
    }
    
    public final void setCurrentImageIndex(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Integer> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getEventName() {
        return null;
    }
    
    public final void setEventName(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getEventDescription() {
        return null;
    }
    
    public final void setEventDescription(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.impaktsoft.globeup.enums.EventType> getEventType() {
        return null;
    }
    
    public final void setEventType(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<com.impaktsoft.globeup.enums.EventType> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.google.android.gms.maps.model.LatLng> getEventLocation() {
        return null;
    }
    
    public final void setEventLocation(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<com.google.android.gms.maps.model.LatLng> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isLoading() {
        return null;
    }
    
    public final void setLoading(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.ArrayList<java.lang.Integer> getImages() {
        return null;
    }
    
    public final void setImages(@org.jetbrains.annotations.Nullable()
    java.util.ArrayList<java.lang.Integer> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.Calendar> getDatePickerCalendar() {
        return null;
    }
    
    public final void setDatePickerCalendar(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.Calendar> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.Calendar> getTimePickerCalendar() {
        return null;
    }
    
    public final void setTimePickerCalendar(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.Calendar> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.impaktsoft.globeup.enums.EventType> getSpinnerEntries() {
        return null;
    }
    
    public final void setSpinnerEntries(@org.jetbrains.annotations.NotNull()
    java.util.List<? extends com.impaktsoft.globeup.enums.EventType> p0) {
    }
    
    private final void setupFields() {
    }
    
    public final int getC() {
        return 0;
    }
    
    public final void setC(int p0) {
    }
    
    public final void editClick() {
    }
    
    private final void setupEventDate() {
    }
    
    private final java.lang.Integer getImageIndex(int imageId) {
        return null;
    }
    
    public EditEventViewModel() {
        super();
    }
}