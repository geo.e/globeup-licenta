package com.impaktsoft.globeup.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000e\u0018\u00002\u00020\u0001B#\b\u0016\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0007B\u0007\b\u0016\u00a2\u0006\u0002\u0010\bR\u001c\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\u001c\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u001c\u0010\u0006\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\n\"\u0004\b\u0012\u0010\f\u00a8\u0006\u0013"}, d2 = {"Lcom/impaktsoft/globeup/models/PendingMessagePOJO;", "", "conversationKey", "", "messagePOJO", "Lcom/impaktsoft/globeup/models/MessagePOJO;", "senderKey", "(Ljava/lang/String;Lcom/impaktsoft/globeup/models/MessagePOJO;Ljava/lang/String;)V", "()V", "getConversationKey", "()Ljava/lang/String;", "setConversationKey", "(Ljava/lang/String;)V", "getMessagePOJO", "()Lcom/impaktsoft/globeup/models/MessagePOJO;", "setMessagePOJO", "(Lcom/impaktsoft/globeup/models/MessagePOJO;)V", "getSenderKey", "setSenderKey", "app_debug"})
public final class PendingMessagePOJO {
    @org.jetbrains.annotations.Nullable()
    private java.lang.String conversationKey;
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.models.MessagePOJO messagePOJO;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String senderKey;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getConversationKey() {
        return null;
    }
    
    public final void setConversationKey(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.models.MessagePOJO getMessagePOJO() {
        return null;
    }
    
    public final void setMessagePOJO(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.models.MessagePOJO p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSenderKey() {
        return null;
    }
    
    public final void setSenderKey(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    public PendingMessagePOJO(@org.jetbrains.annotations.Nullable()
    java.lang.String conversationKey, @org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.models.MessagePOJO messagePOJO, @org.jetbrains.annotations.NotNull()
    java.lang.String senderKey) {
        super();
    }
    
    public PendingMessagePOJO() {
        super();
    }
}