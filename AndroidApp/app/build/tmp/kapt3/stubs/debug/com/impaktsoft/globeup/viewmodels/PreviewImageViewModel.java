package com.impaktsoft.globeup.viewmodels;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\t"}, d2 = {"Lcom/impaktsoft/globeup/viewmodels/PreviewImageViewModel;", "Lcom/impaktsoft/globeup/viewmodels/BaseViewModel;", "()V", "messageItem", "Lcom/impaktsoft/globeup/models/MessagePOJO;", "getMessageItem", "()Lcom/impaktsoft/globeup/models/MessagePOJO;", "setMessageItem", "(Lcom/impaktsoft/globeup/models/MessagePOJO;)V", "app_debug"})
public final class PreviewImageViewModel extends com.impaktsoft.globeup.viewmodels.BaseViewModel {
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.models.MessagePOJO messageItem;
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.models.MessagePOJO getMessageItem() {
        return null;
    }
    
    public final void setMessageItem(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.models.MessagePOJO p0) {
    }
    
    public PreviewImageViewModel() {
        super();
    }
}