package com.impaktsoft.globeup.services;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0006\u001a\u00020\u0007H&J\b\u0010\b\u001a\u00020\u0007H&J1\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\f2\b\b\u0002\u0010\u000e\u001a\u00020\u0007H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000fJ\u0011\u0010\u0010\u001a\u00020\u0011H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0012J)\u0010\u0013\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\fH\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0014J\u001f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00030\n2\u0006\u0010\u000b\u001a\u00020\fH\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0016J\'\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00030\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\fH\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0014J\u0011\u0010\u0018\u001a\u00020\u0019H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0012R\u0014\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u001a"}, d2 = {"Lcom/impaktsoft/globeup/services/IBackendService;", "", "currentUser", "Lcom/impaktsoft/globeup/services/User;", "getCurrentUser", "()Lcom/impaktsoft/globeup/services/User;", "isSignedIn", "", "isSignedInAnonymously", "linkAnonymousWithCredentials", "Lcom/impaktsoft/globeup/services/BackendResultWithPayload;", "email", "", "password", "sendEmailVerification", "(Ljava/lang/String;Ljava/lang/String;ZLkotlin/coroutines/Continuation;)Ljava/lang/Object;", "logoutCurrentUser", "", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "registerWithEmailAndPassword", "(Ljava/lang/String;Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "resetPassword", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "signIn", "signInAnonymously", "Lcom/impaktsoft/globeup/services/BackendResult;", "app_debug"})
public abstract interface IBackendService {
    
    @org.jetbrains.annotations.Nullable()
    public abstract com.impaktsoft.globeup.services.User getCurrentUser();
    
    public abstract boolean isSignedIn();
    
    public abstract boolean isSignedInAnonymously();
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object signInAnonymously(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.impaktsoft.globeup.services.BackendResult> p0);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object linkAnonymousWithCredentials(@org.jetbrains.annotations.NotNull()
    java.lang.String email, @org.jetbrains.annotations.NotNull()
    java.lang.String password, boolean sendEmailVerification, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.impaktsoft.globeup.services.BackendResultWithPayload<com.impaktsoft.globeup.services.User>> p3);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object registerWithEmailAndPassword(@org.jetbrains.annotations.NotNull()
    java.lang.String email, @org.jetbrains.annotations.NotNull()
    java.lang.String password, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.impaktsoft.globeup.services.BackendResultWithPayload<com.impaktsoft.globeup.services.User>> p2);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object logoutCurrentUser(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p0);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object signIn(@org.jetbrains.annotations.NotNull()
    java.lang.String email, @org.jetbrains.annotations.NotNull()
    java.lang.String password, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.impaktsoft.globeup.services.BackendResultWithPayload<com.impaktsoft.globeup.services.User>> p2);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object resetPassword(@org.jetbrains.annotations.NotNull()
    java.lang.String email, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.impaktsoft.globeup.services.BackendResultWithPayload<com.impaktsoft.globeup.services.User>> p1);
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 3)
    public final class DefaultImpls {
    }
}