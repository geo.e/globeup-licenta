package com.impaktsoft.globeup.services;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0004\b\u0016\u0018\u00002\u00020\u0001B\u0011\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u001a\u0010\u0007\u001a\u00020\bX\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\t\"\u0004\b\n\u0010\u000b\u00a8\u0006\f"}, d2 = {"Lcom/impaktsoft/globeup/services/BackendResult;", "", "error", "Lcom/impaktsoft/globeup/models/GError;", "(Lcom/impaktsoft/globeup/models/GError;)V", "getError", "()Lcom/impaktsoft/globeup/models/GError;", "isSuccess", "", "()Z", "setSuccess", "(Z)V", "app_debug"})
public class BackendResult {
    private boolean isSuccess;
    @org.jetbrains.annotations.Nullable()
    private final com.impaktsoft.globeup.models.GError error = null;
    
    public boolean isSuccess() {
        return false;
    }
    
    public void setSuccess(boolean p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.models.GError getError() {
        return null;
    }
    
    public BackendResult(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.models.GError error) {
        super();
    }
    
    public BackendResult() {
        super();
    }
}