package com.impaktsoft.globeup.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u000b"}, d2 = {"Lcom/impaktsoft/globeup/models/GError;", "", "reason", "Lcom/impaktsoft/globeup/models/GReason;", "debugMessage", "", "(Lcom/impaktsoft/globeup/models/GReason;Ljava/lang/String;)V", "getDebugMessage", "()Ljava/lang/String;", "getReason", "()Lcom/impaktsoft/globeup/models/GReason;", "app_debug"})
public final class GError {
    @org.jetbrains.annotations.NotNull()
    private final com.impaktsoft.globeup.models.GReason reason = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String debugMessage = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.impaktsoft.globeup.models.GReason getReason() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getDebugMessage() {
        return null;
    }
    
    public GError(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.GReason reason, @org.jetbrains.annotations.NotNull()
    java.lang.String debugMessage) {
        super();
    }
}