package com.impaktsoft.globeup.enums;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\f\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000bj\u0002\b\f\u00a8\u0006\r"}, d2 = {"Lcom/impaktsoft/globeup/enums/LevelType;", "", "(Ljava/lang/String;I)V", "GREENHORN", "BRONZE1", "BRONZE2", "BRONZE3", "SILVER1", "SILVER2", "SILVER3", "GOLD1", "GOLD2", "GOLD3", "app_debug"})
public enum LevelType {
    /*public static final*/ GREENHORN /* = new GREENHORN() */,
    /*public static final*/ BRONZE1 /* = new BRONZE1() */,
    /*public static final*/ BRONZE2 /* = new BRONZE2() */,
    /*public static final*/ BRONZE3 /* = new BRONZE3() */,
    /*public static final*/ SILVER1 /* = new SILVER1() */,
    /*public static final*/ SILVER2 /* = new SILVER2() */,
    /*public static final*/ SILVER3 /* = new SILVER3() */,
    /*public static final*/ GOLD1 /* = new GOLD1() */,
    /*public static final*/ GOLD2 /* = new GOLD2() */,
    /*public static final*/ GOLD3 /* = new GOLD3() */;
    
    LevelType() {
    }
}