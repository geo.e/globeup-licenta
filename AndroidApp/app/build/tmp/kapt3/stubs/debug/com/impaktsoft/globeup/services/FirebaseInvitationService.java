package com.impaktsoft.globeup.services;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\b\u0016\u00a2\u0006\u0002\u0010\u0002J\n\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0016R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"}, d2 = {"Lcom/impaktsoft/globeup/services/FirebaseInvitationService;", "Lcom/impaktsoft/globeup/services/IFirebaseInvitationService;", "()V", "firebaseDynamicLinks", "Lcom/google/firebase/dynamiclinks/FirebaseDynamicLinks;", "generateInvitationLink", "Landroid/net/Uri;", "app_debug"})
public final class FirebaseInvitationService implements com.impaktsoft.globeup.services.IFirebaseInvitationService {
    private com.google.firebase.dynamiclinks.FirebaseDynamicLinks firebaseDynamicLinks;
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.net.Uri generateInvitationLink() {
        return null;
    }
    
    public FirebaseInvitationService() {
        super();
    }
}