package com.impaktsoft.globeup.views.fragments;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0005\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u0014\u001a\u00020\u0015J\u0006\u0010\u0016\u001a\u00020\u0015J\u0006\u0010\u0017\u001a\u00020\u0015J\u0006\u0010\u0018\u001a\u00020\u0015J2\u0010\u0019\u001a\u0012\u0012\u0004\u0012\u00020\t0\bj\b\u0012\u0004\u0012\u00020\t`\u001a2\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001d0\u001c2\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\f0\u001cJ\u0015\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\fH\u0000\u00a2\u0006\u0002\b\"J\u001a\u0010#\u001a\u00020\u00152\u0006\u0010$\u001a\u00020%2\b\u0010&\u001a\u0004\u0018\u00010\'H\u0017J \u0010(\u001a\u00020\u00152\u0016\u0010\u001e\u001a\u0012\u0012\u0004\u0012\u00020\t0\bj\b\u0012\u0004\u0012\u00020\t`\u001aH\u0003J\u0010\u0010)\u001a\u00020\u00152\u0006\u0010*\u001a\u00020\u0003H\u0014J2\u0010+\u001a\u0012\u0012\u0004\u0012\u00020\t0\bj\b\u0012\u0004\u0012\u00020\t`\u001a2\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001d0\u001c2\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\f0\u001cR\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u00020\fX\u0094D\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR$\u0010\u0010\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\f@VX\u0094\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u000e\"\u0004\b\u0012\u0010\u0013\u00a8\u0006,"}, d2 = {"Lcom/impaktsoft/globeup/views/fragments/WalletBoundFragment;", "Lcom/impaktsoft/globeup/views/fragments/BaseBoundFragment;", "Lcom/impaktsoft/globeup/viewmodels/WalletViewModel;", "Lcom/impaktsoft/globeup/databinding/FragmentWalletBinding;", "()V", "chart", "Lcom/github/mikephil/charting/charts/LineChart;", "data", "Ljava/util/ArrayList;", "Lcom/github/mikephil/charting/data/Entry;", "dataTransformated", "fragmentNameResourceID", "", "getFragmentNameResourceID", "()I", "<set-?>", "layoutId", "getLayoutId", "setLayoutId", "(I)V", "configChart", "", "configXAxis", "disableLeftAxis", "disableRightAxis", "getEntryesFromDatesAndValues", "Lkotlin/collections/ArrayList;", "dates", "", "Ljava/util/Date;", "values", "getMonthForInt", "", "num", "getMonthForInt$app_debug", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "setData", "setupDataBinding", "binding", "transform", "app_debug"})
public final class WalletBoundFragment extends com.impaktsoft.globeup.views.fragments.BaseBoundFragment<com.impaktsoft.globeup.viewmodels.WalletViewModel, com.impaktsoft.globeup.databinding.FragmentWalletBinding> {
    private int layoutId;
    private final int fragmentNameResourceID = com.impaktsoft.globeup.R.string.wallet_bound_name;
    private com.github.mikephil.charting.charts.LineChart chart;
    private java.util.ArrayList<com.github.mikephil.charting.data.Entry> dataTransformated;
    private java.util.ArrayList<com.github.mikephil.charting.data.Entry> data;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected int getLayoutId() {
        return 0;
    }
    
    public void setLayoutId(int p0) {
    }
    
    @java.lang.Override()
    protected int getFragmentNameResourceID() {
        return 0;
    }
    
    @java.lang.Override()
    protected void setupDataBinding(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.databinding.FragmentWalletBinding binding) {
    }
    
    @android.annotation.SuppressLint(value = {"ResourceAsColor"})
    @androidx.annotation.RequiresApi(value = android.os.Build.VERSION_CODES.M)
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void configChart() {
    }
    
    public final void configXAxis() {
    }
    
    public final void disableLeftAxis() {
    }
    
    public final void disableRightAxis() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getMonthForInt$app_debug(int num) {
        return null;
    }
    
    @androidx.annotation.RequiresApi(value = android.os.Build.VERSION_CODES.M)
    private final void setData(java.util.ArrayList<com.github.mikephil.charting.data.Entry> values) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.github.mikephil.charting.data.Entry> getEntryesFromDatesAndValues(@org.jetbrains.annotations.NotNull()
    java.util.List<? extends java.util.Date> dates, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.Integer> values) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.github.mikephil.charting.data.Entry> transform(@org.jetbrains.annotations.NotNull()
    java.util.List<? extends java.util.Date> dates, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.Integer> values) {
        return null;
    }
    
    public WalletBoundFragment() {
        super(null);
    }
}