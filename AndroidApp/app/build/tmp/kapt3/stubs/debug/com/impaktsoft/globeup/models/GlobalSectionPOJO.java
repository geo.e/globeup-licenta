package com.impaktsoft.globeup.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000f\u0018\u00002\u00020\u0001B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u001e\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0096\u000e\u00a2\u0006\u0010\n\u0002\u0010\u000b\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001e\u0010\f\u001a\u0004\u0018\u00010\u0006X\u0096\u000e\u00a2\u0006\u0010\n\u0002\u0010\u000b\u001a\u0004\b\r\u0010\b\"\u0004\b\u000e\u0010\nR\u001c\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0004R\u001e\u0010\u0012\u001a\u0004\u0018\u00010\u0006X\u0096\u000e\u00a2\u0006\u0010\n\u0002\u0010\u000b\u001a\u0004\b\u0013\u0010\b\"\u0004\b\u0014\u0010\n\u00a8\u0006\u0015"}, d2 = {"Lcom/impaktsoft/globeup/models/GlobalSectionPOJO;", "Lcom/impaktsoft/globeup/models/IGlobalBaseItem;", "name", "", "(Ljava/lang/String;)V", "events", "", "getEvents", "()Ljava/lang/Integer;", "setEvents", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "experience", "getExperience", "setExperience", "getName", "()Ljava/lang/String;", "setName", "rank", "getRank", "setRank", "app_debug"})
public final class GlobalSectionPOJO implements com.impaktsoft.globeup.models.IGlobalBaseItem {
    @org.jetbrains.annotations.Nullable()
    private java.lang.Integer rank;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String name;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Integer experience;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Integer events;
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Integer getRank() {
        return null;
    }
    
    @java.lang.Override()
    public void setRank(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.String getName() {
        return null;
    }
    
    @java.lang.Override()
    public void setName(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Integer getExperience() {
        return null;
    }
    
    @java.lang.Override()
    public void setExperience(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Integer getEvents() {
        return null;
    }
    
    @java.lang.Override()
    public void setEvents(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    public GlobalSectionPOJO(@org.jetbrains.annotations.NotNull()
    java.lang.String name) {
        super();
    }
}