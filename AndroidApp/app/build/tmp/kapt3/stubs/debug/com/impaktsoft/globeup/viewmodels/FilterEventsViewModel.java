package com.impaktsoft.globeup.viewmodels;

import java.lang.System;

@kotlin.Suppress(names = {"UNCHECKED_CAST"})
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J$\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u001b\u001a\u00020\u000eJ\u0006\u0010\u001c\u001a\u00020\u0017J\u0006\u0010\u001d\u001a\u00020\u0017J\u0018\u0010\u001e\u001a\u0012\u0012\u0004\u0012\u00020\u00050\u001fj\b\u0012\u0004\u0012\u00020\u0005` H\u0002R \u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR \u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\u0007\"\u0004\b\f\u0010\tR\u001c\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u001c\u0010\u0013\u001a\u0004\u0018\u00010\u000eX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0010\"\u0004\b\u0015\u0010\u0012\u00a8\u0006!"}, d2 = {"Lcom/impaktsoft/globeup/viewmodels/FilterEventsViewModel;", "Lcom/impaktsoft/globeup/viewmodels/BaseViewModel;", "()V", "eventTimeFilters", "", "Lcom/impaktsoft/globeup/models/IFilterItem;", "getEventTimeFilters", "()Ljava/util/List;", "setEventTimeFilters", "(Ljava/util/List;)V", "eventTypeFilters", "getEventTypeFilters", "setEventTypeFilters", "timeFiltersAdapter", "Lcom/impaktsoft/globeup/listadapters/FilterEventsAdapter;", "getTimeFiltersAdapter", "()Lcom/impaktsoft/globeup/listadapters/FilterEventsAdapter;", "setTimeFiltersAdapter", "(Lcom/impaktsoft/globeup/listadapters/FilterEventsAdapter;)V", "typeFiltersAdapter", "getTypeFiltersAdapter", "setTypeFiltersAdapter", "checkCurrentButton", "", "position", "", "list", "adapter", "close", "doneClick", "getCheckedItems", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "app_debug"})
public final class FilterEventsViewModel extends com.impaktsoft.globeup.viewmodels.BaseViewModel {
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.listadapters.FilterEventsAdapter typeFiltersAdapter;
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.listadapters.FilterEventsAdapter timeFiltersAdapter;
    @org.jetbrains.annotations.NotNull()
    private java.util.List<? extends com.impaktsoft.globeup.models.IFilterItem> eventTypeFilters;
    @org.jetbrains.annotations.NotNull()
    private java.util.List<? extends com.impaktsoft.globeup.models.IFilterItem> eventTimeFilters;
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.listadapters.FilterEventsAdapter getTypeFiltersAdapter() {
        return null;
    }
    
    public final void setTypeFiltersAdapter(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.listadapters.FilterEventsAdapter p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.listadapters.FilterEventsAdapter getTimeFiltersAdapter() {
        return null;
    }
    
    public final void setTimeFiltersAdapter(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.listadapters.FilterEventsAdapter p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.impaktsoft.globeup.models.IFilterItem> getEventTypeFilters() {
        return null;
    }
    
    public final void setEventTypeFilters(@org.jetbrains.annotations.NotNull()
    java.util.List<? extends com.impaktsoft.globeup.models.IFilterItem> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.impaktsoft.globeup.models.IFilterItem> getEventTimeFilters() {
        return null;
    }
    
    public final void setEventTimeFilters(@org.jetbrains.annotations.NotNull()
    java.util.List<? extends com.impaktsoft.globeup.models.IFilterItem> p0) {
    }
    
    public final void checkCurrentButton(int position, @org.jetbrains.annotations.NotNull()
    java.util.List<? extends com.impaktsoft.globeup.models.IFilterItem> list, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.listadapters.FilterEventsAdapter adapter) {
    }
    
    private final java.util.ArrayList<com.impaktsoft.globeup.models.IFilterItem> getCheckedItems() {
        return null;
    }
    
    public final void close() {
    }
    
    public final void doneClick() {
    }
    
    public FilterEventsViewModel() {
        super();
    }
}