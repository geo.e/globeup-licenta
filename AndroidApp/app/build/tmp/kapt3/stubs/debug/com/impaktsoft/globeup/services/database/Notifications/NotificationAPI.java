package com.impaktsoft.globeup.services.database.Notifications;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u0006H\'\u00a8\u0006\u0007"}, d2 = {"Lcom/impaktsoft/globeup/services/database/Notifications/NotificationAPI;", "", "sendChatNotification", "Lretrofit2/Call;", "Lokhttp3/ResponseBody;", "message", "Lcom/impaktsoft/globeup/services/database/Notifications/NotificationRequest;", "app_debug"})
public abstract interface NotificationAPI {
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "fcm/send")
    @retrofit2.http.Headers(value = {"Authorization: key=AAAALNjKgxA:APA91bHfRIi7sGXzbQCrwvu6AplZxVu6t80snBo67wuburMLKSAG5eDp3OZ-f4tN4YaMmThRIae96DMNox1qHseURKt0aDDylUuwg3Ay9sAaW8_sv-KrzqRyDLc0XOla5O2UXLLVV8q6", "Content-Type:application/json"})
    public abstract retrofit2.Call<okhttp3.ResponseBody> sendChatNotification(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.impaktsoft.globeup.services.database.Notifications.NotificationRequest message);
}