package com.impaktsoft.globeup.services.database;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\b\u00105\u001a\u000206H\u0002J\b\u00107\u001a\u00020 H\u0016R\u001b\u0010\u0007\u001a\u00020\b8VX\u0096\u0084\u0002\u00a2\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\t\u0010\nR\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0011\u001a\u00020\u00128VX\u0096\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0015\u0010\f\u001a\u0004\b\u0013\u0010\u0014R\u0014\u0010\u0016\u001a\u00020\u0017X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u001b\u0010\u001a\u001a\u00020\u001b8VX\u0096\u0084\u0002\u00a2\u0006\f\n\u0004\b\u001e\u0010\f\u001a\u0004\b\u001c\u0010\u001dR\u001b\u0010\u001f\u001a\u00020 8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b#\u0010\f\u001a\u0004\b!\u0010\"R\u0014\u0010$\u001a\u00020%X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b&\u0010\'R\u0014\u0010(\u001a\u00020)X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b*\u0010+R\u001a\u0010\u0004\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b,\u0010-\"\u0004\b.\u0010/R\u001b\u00100\u001a\u0002018VX\u0096\u0084\u0002\u00a2\u0006\f\n\u0004\b4\u0010\f\u001a\u0004\b2\u00103\u00a8\u00068"}, d2 = {"Lcom/impaktsoft/globeup/services/database/DatabaseService;", "Lcom/impaktsoft/globeup/services/database/IDatabaseService;", "connectivityService", "Lcom/impaktsoft/globeup/services/IConnectivityService;", "sharedPreferencesService", "Lcom/impaktsoft/globeup/services/ISharedPreferencesService;", "(Lcom/impaktsoft/globeup/services/IConnectivityService;Lcom/impaktsoft/globeup/services/ISharedPreferencesService;)V", "chatDAO", "Lcom/impaktsoft/globeup/services/database/IChatDAO;", "getChatDAO", "()Lcom/impaktsoft/globeup/services/database/IChatDAO;", "chatDAO$delegate", "Lkotlin/Lazy;", "getConnectivityService", "()Lcom/impaktsoft/globeup/services/IConnectivityService;", "setConnectivityService", "(Lcom/impaktsoft/globeup/services/IConnectivityService;)V", "eventDao", "Lcom/impaktsoft/globeup/services/database/IEventsDAO;", "getEventDao", "()Lcom/impaktsoft/globeup/services/database/IEventsDAO;", "eventDao$delegate", "feedbackService", "Lcom/impaktsoft/globeup/services/database/IFeedbackService;", "getFeedbackService", "()Lcom/impaktsoft/globeup/services/database/IFeedbackService;", "fireAuth", "Lcom/google/firebase/auth/FirebaseAuth;", "getFireAuth", "()Lcom/google/firebase/auth/FirebaseAuth;", "fireAuth$delegate", "fireStore", "Lcom/google/firebase/firestore/FirebaseFirestore;", "getFireStore", "()Lcom/google/firebase/firestore/FirebaseFirestore;", "fireStore$delegate", "firebaseFirebaseDynamicLinksService", "Lcom/impaktsoft/globeup/services/database/IFirebaseDynamicLinksService;", "getFirebaseFirebaseDynamicLinksService", "()Lcom/impaktsoft/globeup/services/database/IFirebaseDynamicLinksService;", "messagingService", "Lcom/impaktsoft/globeup/services/database/IMessagingService;", "getMessagingService", "()Lcom/impaktsoft/globeup/services/database/IMessagingService;", "getSharedPreferencesService", "()Lcom/impaktsoft/globeup/services/ISharedPreferencesService;", "setSharedPreferencesService", "(Lcom/impaktsoft/globeup/services/ISharedPreferencesService;)V", "userInfoDao", "Lcom/impaktsoft/globeup/services/database/IUserInfoDAO;", "getUserInfoDao", "()Lcom/impaktsoft/globeup/services/database/IUserInfoDAO;", "userInfoDao$delegate", "configureOfflineDB", "", "getFirestore", "app_debug"})
public final class DatabaseService implements com.impaktsoft.globeup.services.database.IDatabaseService {
    private final kotlin.Lazy fireStore$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy fireAuth$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final com.impaktsoft.globeup.services.database.IMessagingService messagingService = null;
    @org.jetbrains.annotations.NotNull()
    private final com.impaktsoft.globeup.services.database.IFirebaseDynamicLinksService firebaseFirebaseDynamicLinksService = null;
    @org.jetbrains.annotations.NotNull()
    private final com.impaktsoft.globeup.services.database.IFeedbackService feedbackService = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy userInfoDao$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy eventDao$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy chatDAO$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private com.impaktsoft.globeup.services.IConnectivityService connectivityService;
    @org.jetbrains.annotations.NotNull()
    private com.impaktsoft.globeup.services.ISharedPreferencesService sharedPreferencesService;
    
    private final com.google.firebase.firestore.FirebaseFirestore getFireStore() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.google.firebase.auth.FirebaseAuth getFireAuth() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.impaktsoft.globeup.services.database.IMessagingService getMessagingService() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.impaktsoft.globeup.services.database.IFirebaseDynamicLinksService getFirebaseFirebaseDynamicLinksService() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.impaktsoft.globeup.services.database.IFeedbackService getFeedbackService() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.google.firebase.firestore.FirebaseFirestore getFirestore() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.impaktsoft.globeup.services.database.IUserInfoDAO getUserInfoDao() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.impaktsoft.globeup.services.database.IEventsDAO getEventDao() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.impaktsoft.globeup.services.database.IChatDAO getChatDAO() {
        return null;
    }
    
    private final void configureOfflineDB() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.impaktsoft.globeup.services.IConnectivityService getConnectivityService() {
        return null;
    }
    
    public final void setConnectivityService(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.services.IConnectivityService p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.impaktsoft.globeup.services.ISharedPreferencesService getSharedPreferencesService() {
        return null;
    }
    
    public final void setSharedPreferencesService(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.services.ISharedPreferencesService p0) {
    }
    
    public DatabaseService(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.services.IConnectivityService connectivityService, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.services.ISharedPreferencesService sharedPreferencesService) {
        super();
    }
}