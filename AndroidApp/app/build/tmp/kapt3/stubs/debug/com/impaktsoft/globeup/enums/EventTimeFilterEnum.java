package com.impaktsoft.globeup.enums;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\b\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\b\u00a8\u0006\t"}, d2 = {"Lcom/impaktsoft/globeup/enums/EventTimeFilterEnum;", "", "(Ljava/lang/String;I)V", "TODAY", "THIS_WEEK", "NEXT_WEEK", "THIS_MONTH", "NEXT_MONTH", "ALL", "app_debug"})
public enum EventTimeFilterEnum {
    /*public static final*/ TODAY /* = new TODAY() */,
    /*public static final*/ THIS_WEEK /* = new THIS_WEEK() */,
    /*public static final*/ NEXT_WEEK /* = new NEXT_WEEK() */,
    /*public static final*/ THIS_MONTH /* = new THIS_MONTH() */,
    /*public static final*/ NEXT_MONTH /* = new NEXT_MONTH() */,
    /*public static final*/ ALL /* = new ALL() */;
    
    EventTimeFilterEnum() {
    }
}