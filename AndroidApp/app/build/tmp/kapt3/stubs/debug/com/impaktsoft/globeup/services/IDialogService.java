package com.impaktsoft.globeup.services;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J(\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\nH&J \u0010\u000b\u001a\u00020\u0003\"\b\b\u0000\u0010\f*\u00020\r2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u0002H\f0\u000fH&J\u001a\u0010\u0010\u001a\u00020\u00032\u0006\u0010\u0011\u001a\u00020\u00122\b\b\u0002\u0010\u0013\u001a\u00020\u0012H&J\u001a\u0010\u0010\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u00062\b\b\u0002\u0010\u0013\u001a\u00020\u0012H&\u00a8\u0006\u0015"}, d2 = {"Lcom/impaktsoft/globeup/services/IDialogService;", "", "hideDialog", "", "showAlertDialog", "title", "", "message", "buttonText", "clickMethod", "Lcom/impaktsoft/globeup/listeners/IClickListener;", "showDialog", "T", "Landroidx/fragment/app/Fragment;", "fragmentClass", "Lkotlin/reflect/KClass;", "showSnackbar", "stringId", "", "duration", "string", "app_debug"})
public abstract interface IDialogService {
    
    public abstract <T extends androidx.fragment.app.Fragment>void showDialog(@org.jetbrains.annotations.NotNull()
    kotlin.reflect.KClass<T> fragmentClass);
    
    public abstract void hideDialog();
    
    public abstract void showSnackbar(int stringId, int duration);
    
    public abstract void showSnackbar(@org.jetbrains.annotations.NotNull()
    java.lang.String string, int duration);
    
    public abstract void showAlertDialog(@org.jetbrains.annotations.NotNull()
    java.lang.String title, @org.jetbrains.annotations.NotNull()
    java.lang.String message, @org.jetbrains.annotations.NotNull()
    java.lang.String buttonText, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.listeners.IClickListener clickMethod);
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 3)
    public final class DefaultImpls {
    }
}