package com.impaktsoft.globeup.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\r\u0018\u00002\u00020\u0001BO\b\u0016\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u00122\u0010\u0005\u001a.\u0012\u0004\u0012\u00020\u0003\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u0007\u0018\u00010\u0006j\u0016\u0012\u0004\u0012\u00020\u0003\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u0007\u0018\u0001`\t\u00a2\u0006\u0002\u0010\nR\u001e\u0010\u0004\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u000f\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001e\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u000f\u001a\u0004\b\u0010\u0010\f\"\u0004\b\u0011\u0010\u000eRF\u0010\u0005\u001a.\u0012\u0004\u0012\u00020\u0003\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u0007\u0018\u00010\u0006j\u0016\u0012\u0004\u0012\u00020\u0003\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u0007\u0018\u0001`\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015\u00a8\u0006\u0016"}, d2 = {"Lcom/impaktsoft/globeup/models/PopUpMenuSettingsPOJO;", "", "menuId", "", "anchorViewId", "methods", "Ljava/util/HashMap;", "Lkotlin/Function0;", "", "Lkotlin/collections/HashMap;", "(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/util/HashMap;)V", "getAnchorViewId", "()Ljava/lang/Integer;", "setAnchorViewId", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "getMenuId", "setMenuId", "getMethods", "()Ljava/util/HashMap;", "setMethods", "(Ljava/util/HashMap;)V", "app_debug"})
public final class PopUpMenuSettingsPOJO {
    @org.jetbrains.annotations.Nullable()
    private java.lang.Integer menuId;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Integer anchorViewId;
    @org.jetbrains.annotations.Nullable()
    private java.util.HashMap<java.lang.Integer, kotlin.jvm.functions.Function0<kotlin.Unit>> methods;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getMenuId() {
        return null;
    }
    
    public final void setMenuId(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getAnchorViewId() {
        return null;
    }
    
    public final void setAnchorViewId(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.HashMap<java.lang.Integer, kotlin.jvm.functions.Function0<kotlin.Unit>> getMethods() {
        return null;
    }
    
    public final void setMethods(@org.jetbrains.annotations.Nullable()
    java.util.HashMap<java.lang.Integer, kotlin.jvm.functions.Function0<kotlin.Unit>> p0) {
    }
    
    public PopUpMenuSettingsPOJO(@org.jetbrains.annotations.Nullable()
    java.lang.Integer menuId, @org.jetbrains.annotations.Nullable()
    java.lang.Integer anchorViewId, @org.jetbrains.annotations.Nullable()
    java.util.HashMap<java.lang.Integer, kotlin.jvm.functions.Function0<kotlin.Unit>> methods) {
        super();
    }
}