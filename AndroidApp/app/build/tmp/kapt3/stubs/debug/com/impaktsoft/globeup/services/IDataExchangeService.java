package com.impaktsoft.globeup.services;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\u001d\u0010\u0002\u001a\u0004\u0018\u0001H\u0003\"\u0004\b\u0000\u0010\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a2\u0006\u0002\u0010\u0006J\u0018\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\u0001H&\u00a8\u0006\n"}, d2 = {"Lcom/impaktsoft/globeup/services/IDataExchangeService;", "", "get", "T", "key", "", "(Ljava/lang/String;)Ljava/lang/Object;", "put", "", "data", "app_debug"})
public abstract interface IDataExchangeService {
    
    public abstract void put(@org.jetbrains.annotations.NotNull()
    java.lang.String key, @org.jetbrains.annotations.NotNull()
    java.lang.Object data);
    
    @org.jetbrains.annotations.Nullable()
    public abstract <T extends java.lang.Object>T get(@org.jetbrains.annotations.NotNull()
    java.lang.String key);
}