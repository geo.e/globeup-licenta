package com.impaktsoft.globeup.dummy;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lcom/impaktsoft/globeup/dummy/GCoinsOffertDummyData;", "", "()V", "Companion", "app_debug"})
public final class GCoinsOffertDummyData {
    @org.jetbrains.annotations.NotNull()
    private static final java.util.List<com.impaktsoft.globeup.models.GCoinPOJO> listOfOfferts = null;
    public static final com.impaktsoft.globeup.dummy.GCoinsOffertDummyData.Companion Companion = null;
    
    public GCoinsOffertDummyData() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0017\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\b"}, d2 = {"Lcom/impaktsoft/globeup/dummy/GCoinsOffertDummyData$Companion;", "", "()V", "listOfOfferts", "", "Lcom/impaktsoft/globeup/models/GCoinPOJO;", "getListOfOfferts", "()Ljava/util/List;", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<com.impaktsoft.globeup.models.GCoinPOJO> getListOfOfferts() {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}