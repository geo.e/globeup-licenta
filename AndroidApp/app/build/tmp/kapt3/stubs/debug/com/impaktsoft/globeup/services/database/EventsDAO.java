package com.impaktsoft.globeup.services.database;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u009e\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ \u0010\"\u001a\u00020#2\u0016\u0010$\u001a\u0012\u0012\u0004\u0012\u00020%0\u001ej\b\u0012\u0004\u0012\u00020%`&H\u0002J3\u0010\'\u001a\u0012\u0012\u0004\u0012\u00020(0\u001ej\b\u0012\u0004\u0012\u00020(`&2\u0006\u0010)\u001a\u00020\u000e2\b\u0010*\u001a\u0004\u0018\u00010\u000eH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010+JM\u0010,\u001a\u0012\u0012\u0004\u0012\u00020(0\u001ej\b\u0012\u0004\u0012\u00020(`&2\u0016\u0010$\u001a\u0012\u0012\u0004\u0012\u00020%0\u001ej\b\u0012\u0004\u0012\u00020%`&2\b\u0010*\u001a\u0004\u0018\u00010\u000e2\b\u0010-\u001a\u0004\u0018\u00010.H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010/J=\u00100\u001a\u0012\u0012\u0004\u0012\u00020(0\u001ej\b\u0012\u0004\u0012\u00020(`&2\u001a\u00101\u001a\u0016\u0012\u0004\u0012\u00020\u000e\u0018\u00010\u001ej\n\u0012\u0004\u0012\u00020\u000e\u0018\u0001`&H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u00102JM\u00103\u001a\u0012\u0012\u0004\u0012\u00020(0\u001ej\b\u0012\u0004\u0012\u00020(`&2\u0016\u0010$\u001a\u0012\u0012\u0004\u0012\u00020%0\u001ej\b\u0012\u0004\u0012\u00020%`&2\b\u0010*\u001a\u0004\u0018\u00010\u000e2\b\u0010-\u001a\u0004\u0018\u00010.H\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010/J+\u00104\u001a\u0012\u0012\u0004\u0012\u00020(0\u001ej\b\u0012\u0004\u0012\u00020(`&2\b\u0010*\u001a\u0004\u0018\u00010\u000eH\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u00105J3\u00106\u001a\u0012\u0012\u0004\u0012\u0002070\u001ej\b\u0012\u0004\u0012\u000207`&2\u0006\u00108\u001a\u00020\u000e2\b\u00109\u001a\u0004\u0018\u00010\nH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010:J\u001b\u0010;\u001a\u0004\u0018\u00010\u001a2\u0006\u0010<\u001a\u00020\u000eH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u00105J!\u0010=\u001a\u00020>2\u0006\u0010?\u001a\u00020\u000e2\u0006\u0010@\u001a\u00020AH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010BJ!\u0010C\u001a\u00020>2\u0006\u0010?\u001a\u00020\u000e2\u0006\u0010@\u001a\u00020AH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010BJ\u0019\u0010D\u001a\u00020#2\u0006\u00108\u001a\u00020\u000eH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u00105J!\u0010E\u001a\u00020>2\u0006\u00108\u001a\u00020\u000e2\u0006\u0010<\u001a\u00020\u000eH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010+J\b\u0010F\u001a\u00020#H\u0016J\u0010\u0010G\u001a\u00020#2\u0006\u0010H\u001a\u00020\u001aH\u0002J\u0010\u0010I\u001a\u00020#2\u0006\u0010J\u001a\u00020KH\u0002J\u0010\u0010L\u001a\u00020#2\u0006\u0010M\u001a\u00020NH\u0002J!\u0010O\u001a\u00020#2\u0006\u0010P\u001a\u00020\u000e2\u0006\u0010Q\u001a\u00020(H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010RJ!\u0010S\u001a\u00020>2\u0006\u00108\u001a\u00020\u000e2\u0006\u0010Q\u001a\u000207H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010TJ\u0019\u0010U\u001a\u00020#2\u0006\u0010V\u001a\u00020(H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010WR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u00020\n8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\r\u001a\u00020\u000eX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u000eX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u000eX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u000eX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u000eX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u000eX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u000eX\u0082D\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0016X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001b\u001a\u0004\u0018\u00010\u001cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u001d\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020 0\u001f0\u001eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\u000eX\u0082D\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006X"}, d2 = {"Lcom/impaktsoft/globeup/services/database/EventsDAO;", "Lcom/impaktsoft/globeup/services/database/IEventsDAO;", "fireStore", "Lcom/google/firebase/firestore/FirebaseFirestore;", "connectivityService", "Lcom/impaktsoft/globeup/services/IConnectivityService;", "sharedPreferencesService", "Lcom/impaktsoft/globeup/services/ISharedPreferencesService;", "(Lcom/google/firebase/firestore/FirebaseFirestore;Lcom/impaktsoft/globeup/services/IConnectivityService;Lcom/impaktsoft/globeup/services/ISharedPreferencesService;)V", "defaultDataSource", "Lcom/google/firebase/firestore/Source;", "getDefaultDataSource", "()Lcom/google/firebase/firestore/Source;", "eventCollection", "", "eventCountryField", "eventDateField", "eventLocationField", "eventMembersCollection", "eventNameField", "eventTypeField", "eventsQuery", "Lcom/google/firebase/firestore/Query;", "fetchEventsLimitNumber", "", "fetchWithFiltersCount", "", "lastEventDocumentLoaded", "Lcom/google/firebase/firestore/DocumentSnapshot;", "tasksList", "Ljava/util/ArrayList;", "Lcom/google/android/gms/tasks/Task;", "Lcom/google/firebase/firestore/QuerySnapshot;", "userEventsCollection", "createFilterQuery", "", "filtersList", "Lcom/impaktsoft/globeup/models/IFilterItem;", "Lkotlin/collections/ArrayList;", "fetchEventByName", "Lcom/impaktsoft/globeup/models/EventPOJO;", "name", "country", "(Ljava/lang/String;Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "fetchEvents", "scaleType", "Lcom/impaktsoft/globeup/enums/ScaleTypeEnum;", "(Ljava/util/ArrayList;Ljava/lang/String;Lcom/impaktsoft/globeup/enums/ScaleTypeEnum;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "fetchEventsByKey", "listOfKeys", "(Ljava/util/ArrayList;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "fetchEventsWithFilters", "fetchEventsWithoutFilters", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "fetchUserEvents", "Lcom/impaktsoft/globeup/models/UserEventPOJO;", "userID", "dataSource", "(Ljava/lang/String;Lcom/google/firebase/firestore/Source;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getEventMembersCount", "eventId", "joinEvent", "", "eventKey", "eventMember", "Lcom/impaktsoft/globeup/models/EventMembersPOJO;", "(Ljava/lang/String;Lcom/impaktsoft/globeup/models/EventMembersPOJO;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "leaveEvent", "removeAllUserEvents", "removeUserEvent", "resetLastEventDocument", "setEventsDistanceQuery", "radiusInKM", "setEventsTimeQuery", "timeFilter", "Lcom/impaktsoft/globeup/models/EventTimeFilterPOJO;", "setEventsTypeQuery", "eventType", "Lcom/impaktsoft/globeup/enums/EventType;", "storeEvent", "backendKey", "event", "(Ljava/lang/String;Lcom/impaktsoft/globeup/models/EventPOJO;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "storeUserEvent", "(Ljava/lang/String;Lcom/impaktsoft/globeup/models/UserEventPOJO;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "updateEvent", "eventItem", "(Lcom/impaktsoft/globeup/models/EventPOJO;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class EventsDAO implements com.impaktsoft.globeup.services.database.IEventsDAO {
    private final java.lang.String eventCollection = "Events";
    private final java.lang.String userEventsCollection = "UserEvents";
    private final java.lang.String eventMembersCollection = "EventMembers";
    private final long fetchEventsLimitNumber = 4L;
    private int fetchWithFiltersCount;
    private com.google.firebase.firestore.Query eventsQuery;
    private com.google.firebase.firestore.DocumentSnapshot lastEventDocumentLoaded;
    private final java.lang.String eventDateField = "eventDate";
    private final java.lang.String eventTypeField = "eventType";
    private final java.lang.String eventCountryField = "eventCountry";
    private final java.lang.String eventLocationField = "location";
    private final java.lang.String eventNameField = "name";
    private java.util.ArrayList<com.google.android.gms.tasks.Task<com.google.firebase.firestore.QuerySnapshot>> tasksList;
    private final com.google.firebase.firestore.FirebaseFirestore fireStore = null;
    private final com.impaktsoft.globeup.services.IConnectivityService connectivityService = null;
    private final com.impaktsoft.globeup.services.ISharedPreferencesService sharedPreferencesService = null;
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object storeEvent(@org.jetbrains.annotations.NotNull()
    java.lang.String backendKey, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.EventPOJO event, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p2) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object storeUserEvent(@org.jetbrains.annotations.NotNull()
    java.lang.String userID, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.UserEventPOJO event, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.lang.Boolean> p2) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object fetchEventsByKey(@org.jetbrains.annotations.Nullable()
    java.util.ArrayList<java.lang.String> listOfKeys, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.ArrayList<com.impaktsoft.globeup.models.EventPOJO>> p1) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object fetchUserEvents(@org.jetbrains.annotations.NotNull()
    java.lang.String userID, @org.jetbrains.annotations.Nullable()
    com.google.firebase.firestore.Source dataSource, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.ArrayList<com.impaktsoft.globeup.models.UserEventPOJO>> p2) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object fetchEvents(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.impaktsoft.globeup.models.IFilterItem> filtersList, @org.jetbrains.annotations.Nullable()
    java.lang.String country, @org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.enums.ScaleTypeEnum scaleType, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.ArrayList<com.impaktsoft.globeup.models.EventPOJO>> p3) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object fetchEventByName(@org.jetbrains.annotations.NotNull()
    java.lang.String name, @org.jetbrains.annotations.Nullable()
    java.lang.String country, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.ArrayList<com.impaktsoft.globeup.models.EventPOJO>> p2) {
        return null;
    }
    
    private final void createFilterQuery(java.util.ArrayList<com.impaktsoft.globeup.models.IFilterItem> filtersList) {
    }
    
    private final void setEventsDistanceQuery(int radiusInKM) {
    }
    
    private final void setEventsTimeQuery(com.impaktsoft.globeup.models.EventTimeFilterPOJO timeFilter) {
    }
    
    private final void setEventsTypeQuery(com.impaktsoft.globeup.enums.EventType eventType) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object removeUserEvent(@org.jetbrains.annotations.NotNull()
    java.lang.String userID, @org.jetbrains.annotations.NotNull()
    java.lang.String eventId, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.lang.Boolean> p2) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object updateEvent(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.EventPOJO eventItem, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p1) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object joinEvent(@org.jetbrains.annotations.NotNull()
    java.lang.String eventKey, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.EventMembersPOJO eventMember, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.lang.Boolean> p2) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object leaveEvent(@org.jetbrains.annotations.NotNull()
    java.lang.String eventKey, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.EventMembersPOJO eventMember, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.lang.Boolean> p2) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object getEventMembersCount(@org.jetbrains.annotations.NotNull()
    java.lang.String eventId, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.lang.Integer> p1) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object removeAllUserEvents(@org.jetbrains.annotations.NotNull()
    java.lang.String userID, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p1) {
        return null;
    }
    
    @java.lang.Override()
    public void resetLastEventDocument() {
    }
    
    private final com.google.firebase.firestore.Source getDefaultDataSource() {
        return null;
    }
    
    public EventsDAO(@org.jetbrains.annotations.NotNull()
    com.google.firebase.firestore.FirebaseFirestore fireStore, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.services.IConnectivityService connectivityService, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.services.ISharedPreferencesService sharedPreferencesService) {
        super();
    }
}