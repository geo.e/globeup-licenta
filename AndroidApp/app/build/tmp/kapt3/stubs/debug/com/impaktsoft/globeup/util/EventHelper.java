package com.impaktsoft.globeup.util;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lcom/impaktsoft/globeup/util/EventHelper;", "", "()V", "Companion", "app_debug"})
public final class EventHelper {
    public static final com.impaktsoft.globeup.util.EventHelper.Companion Companion = null;
    
    public EventHelper() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J:\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0016\u0010\u0007\u001a\u0012\u0012\u0004\u0012\u00020\t0\bj\b\u0012\u0004\u0012\u00020\t`\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\f2\u0006\u0010\r\u001a\u00020\u000eH\u0002J(\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0016\u0010\u0007\u001a\u0012\u0012\u0004\u0012\u00020\t0\bj\b\u0012\u0004\u0012\u00020\t`\nH\u0002J(\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0016\u0010\u0007\u001a\u0012\u0012\u0004\u0012\u00020\t0\bj\b\u0012\u0004\u0012\u00020\t`\nH\u0002J8\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0016\u0010\u0007\u001a\u0012\u0012\u0004\u0012\u00020\t0\bj\b\u0012\u0004\u0012\u00020\t`\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\f2\u0006\u0010\r\u001a\u00020\u000eJ+\u0010\u0012\u001a\u0004\u0018\u00010\t\"\u0006\b\u0000\u0010\u0013\u0018\u00012\u0016\u0010\u0007\u001a\u0012\u0012\u0004\u0012\u00020\t0\bj\b\u0012\u0004\u0012\u00020\t`\nH\u0082\b\u00a8\u0006\u0014"}, d2 = {"Lcom/impaktsoft/globeup/util/EventHelper$Companion;", "", "()V", "eventAcceptsDistanceFilter", "", "event", "Lcom/impaktsoft/globeup/models/EventPOJO;", "filterList", "Ljava/util/ArrayList;", "Lcom/impaktsoft/globeup/models/IFilterItem;", "Lkotlin/collections/ArrayList;", "userLocation", "Lcom/google/android/gms/maps/model/LatLng;", "scaleType", "Lcom/impaktsoft/globeup/enums/ScaleTypeEnum;", "eventAcceptsTimeFilter", "eventAcceptsTypeFilter", "eventPassFilters", "getFilterType", "T", "app_debug"})
    public static final class Companion {
        
        public final boolean eventPassFilters(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.models.EventPOJO event, @org.jetbrains.annotations.NotNull()
        java.util.ArrayList<com.impaktsoft.globeup.models.IFilterItem> filterList, @org.jetbrains.annotations.Nullable()
        com.google.android.gms.maps.model.LatLng userLocation, @org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.enums.ScaleTypeEnum scaleType) {
            return false;
        }
        
        private final boolean eventAcceptsTypeFilter(com.impaktsoft.globeup.models.EventPOJO event, java.util.ArrayList<com.impaktsoft.globeup.models.IFilterItem> filterList) {
            return false;
        }
        
        private final boolean eventAcceptsTimeFilter(com.impaktsoft.globeup.models.EventPOJO event, java.util.ArrayList<com.impaktsoft.globeup.models.IFilterItem> filterList) {
            return false;
        }
        
        private final boolean eventAcceptsDistanceFilter(com.impaktsoft.globeup.models.EventPOJO event, java.util.ArrayList<com.impaktsoft.globeup.models.IFilterItem> filterList, com.google.android.gms.maps.model.LatLng userLocation, com.impaktsoft.globeup.enums.ScaleTypeEnum scaleType) {
            return false;
        }
        
        private Companion() {
            super();
        }
    }
}