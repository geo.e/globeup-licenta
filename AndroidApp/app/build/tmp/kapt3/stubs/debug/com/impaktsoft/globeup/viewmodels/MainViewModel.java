package com.impaktsoft.globeup.viewmodels;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0012\u001a\u00020\u0013H\u0002J\b\u0010\u0014\u001a\u00020\u0013H\u0016J\b\u0010\u0015\u001a\u00020\u0013H\u0002R&\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR \u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011\u00a8\u0006\u0016"}, d2 = {"Lcom/impaktsoft/globeup/viewmodels/MainViewModel;", "Lcom/impaktsoft/globeup/viewmodels/BaseViewModel;", "()V", "messagesSeenByUser", "Ljava/util/HashMap;", "", "", "getMessagesSeenByUser", "()Ljava/util/HashMap;", "setMessagesSeenByUser", "(Ljava/util/HashMap;)V", "notificationCount", "Landroidx/lifecycle/MutableLiveData;", "", "getNotificationCount", "()Landroidx/lifecycle/MutableLiveData;", "setNotificationCount", "(Landroidx/lifecycle/MutableLiveData;)V", "initUnreadMessagesCountObserver", "", "onDestroy", "updateUnreadMessagesCount", "app_debug"})
public final class MainViewModel extends com.impaktsoft.globeup.viewmodels.BaseViewModel {
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Integer> notificationCount;
    @org.jetbrains.annotations.NotNull()
    private java.util.HashMap<java.lang.String, java.lang.Boolean> messagesSeenByUser;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Integer> getNotificationCount() {
        return null;
    }
    
    public final void setNotificationCount(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Integer> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.HashMap<java.lang.String, java.lang.Boolean> getMessagesSeenByUser() {
        return null;
    }
    
    public final void setMessagesSeenByUser(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Boolean> p0) {
    }
    
    @java.lang.Override()
    public void onDestroy() {
    }
    
    private final void initUnreadMessagesCountObserver() {
    }
    
    private final void updateUnreadMessagesCount() {
    }
    
    public MainViewModel() {
        super();
    }
}