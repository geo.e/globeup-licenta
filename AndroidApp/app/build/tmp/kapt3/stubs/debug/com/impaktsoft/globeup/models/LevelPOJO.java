package com.impaktsoft.globeup.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0010\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B1\b\u0016\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\bJ\u0006\u0010\u0015\u001a\u00020\u0016R\u001c\u0010\u0007\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\u001c\u0010\u0006\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\n\"\u0004\b\u000e\u0010\fR\u001c\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\n\"\u0004\b\u0010\u0010\fR\u001c\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014\u00a8\u0006\u0017"}, d2 = {"Lcom/impaktsoft/globeup/models/LevelPOJO;", "", "levelType", "Lcom/impaktsoft/globeup/enums/LevelType;", "levelName", "", "levelExp", "levelDescription", "(Lcom/impaktsoft/globeup/enums/LevelType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getLevelDescription", "()Ljava/lang/String;", "setLevelDescription", "(Ljava/lang/String;)V", "getLevelExp", "setLevelExp", "getLevelName", "setLevelName", "getLevelType", "()Lcom/impaktsoft/globeup/enums/LevelType;", "setLevelType", "(Lcom/impaktsoft/globeup/enums/LevelType;)V", "hasDescription", "", "app_debug"})
public final class LevelPOJO {
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.enums.LevelType levelType;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String levelName;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String levelExp;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String levelDescription;
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.enums.LevelType getLevelType() {
        return null;
    }
    
    public final void setLevelType(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.enums.LevelType p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getLevelName() {
        return null;
    }
    
    public final void setLevelName(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getLevelExp() {
        return null;
    }
    
    public final void setLevelExp(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getLevelDescription() {
        return null;
    }
    
    public final void setLevelDescription(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    public final boolean hasDescription() {
        return false;
    }
    
    public LevelPOJO(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.enums.LevelType levelType, @org.jetbrains.annotations.Nullable()
    java.lang.String levelName, @org.jetbrains.annotations.Nullable()
    java.lang.String levelExp, @org.jetbrains.annotations.Nullable()
    java.lang.String levelDescription) {
        super();
    }
}