package com.impaktsoft.globeup.viewmodels;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0010\u001a\u00020\u0011R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR \u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000f\u00a8\u0006\u0012"}, d2 = {"Lcom/impaktsoft/globeup/viewmodels/GlobalViewModel;", "Lcom/impaktsoft/globeup/viewmodels/BaseViewModel;", "()V", "adapter", "Lcom/impaktsoft/globeup/listadapters/GlobalAdapter;", "getAdapter", "()Lcom/impaktsoft/globeup/listadapters/GlobalAdapter;", "setAdapter", "(Lcom/impaktsoft/globeup/listadapters/GlobalAdapter;)V", "text", "Landroidx/lifecycle/MutableLiveData;", "", "getText", "()Landroidx/lifecycle/MutableLiveData;", "setText", "(Landroidx/lifecycle/MutableLiveData;)V", "donateClick", "", "app_debug"})
public final class GlobalViewModel extends com.impaktsoft.globeup.viewmodels.BaseViewModel {
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> text;
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.listadapters.GlobalAdapter adapter;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getText() {
        return null;
    }
    
    public final void setText(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.listadapters.GlobalAdapter getAdapter() {
        return null;
    }
    
    public final void setAdapter(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.listadapters.GlobalAdapter p0) {
    }
    
    public final void donateClick() {
    }
    
    public GlobalViewModel() {
        super();
    }
}