package com.impaktsoft.globeup.views.fragments;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0005\u00a2\u0006\u0002\u0010\u0004J\b\u0010\u0016\u001a\u00020\u0017H\u0016J\u0010\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\u001a\u0010\u001b\u001a\u00020\u00172\u0006\u0010\u001c\u001a\u00020\u001d2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001aH\u0017J\b\u0010\u0014\u001a\u00020\u0017H\u0002J\u0010\u0010\u001f\u001a\u00020\u00172\u0006\u0010 \u001a\u00020\u0003H\u0014R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u00020\bX\u0094D\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR$\u0010\f\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\b@VX\u0094\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\n\"\u0004\b\u000e\u0010\u000fR\u001c\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015\u00a8\u0006!"}, d2 = {"Lcom/impaktsoft/globeup/views/fragments/FilterEventsByRadiusFragment;", "Lcom/impaktsoft/globeup/views/fragments/BaseBoundFragment;", "Lcom/impaktsoft/globeup/viewmodels/FilterEventsByRadiusViewModel;", "Lcom/impaktsoft/globeup/databinding/FragmentFilterEventsByRadiusBinding;", "()V", "MAP_VIEW_BUNDLE_KEY", "", "fragmentNameResourceID", "", "getFragmentNameResourceID", "()I", "<set-?>", "layoutId", "getLayoutId", "setLayoutId", "(I)V", "mapView", "Lcom/impaktsoft/globeup/components/MapViewWithRadius;", "getMapView", "()Lcom/impaktsoft/globeup/components/MapViewWithRadius;", "setMapView", "(Lcom/impaktsoft/globeup/components/MapViewWithRadius;)V", "onDestroy", "", "onSaveInstanceState", "outState", "Landroid/os/Bundle;", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "setupDataBinding", "binding", "app_debug"})
public final class FilterEventsByRadiusFragment extends com.impaktsoft.globeup.views.fragments.BaseBoundFragment<com.impaktsoft.globeup.viewmodels.FilterEventsByRadiusViewModel, com.impaktsoft.globeup.databinding.FragmentFilterEventsByRadiusBinding> {
    private int layoutId;
    private final int fragmentNameResourceID = com.impaktsoft.globeup.R.string.filter_events;
    private final java.lang.String MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey";
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.components.MapViewWithRadius mapView;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected int getLayoutId() {
        return 0;
    }
    
    public void setLayoutId(int p0) {
    }
    
    @java.lang.Override()
    protected int getFragmentNameResourceID() {
        return 0;
    }
    
    @java.lang.Override()
    protected void setupDataBinding(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.databinding.FragmentFilterEventsByRadiusBinding binding) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.components.MapViewWithRadius getMapView() {
        return null;
    }
    
    public final void setMapView(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.components.MapViewWithRadius p0) {
    }
    
    @android.annotation.SuppressLint(value = {"ResourceType"})
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public void onSaveInstanceState(@org.jetbrains.annotations.NotNull()
    android.os.Bundle outState) {
    }
    
    private final void setMapView() {
    }
    
    @java.lang.Override()
    public void onDestroy() {
    }
    
    public FilterEventsByRadiusFragment() {
        super(null);
    }
}