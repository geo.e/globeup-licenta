package com.impaktsoft.globeup.views.activities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0015\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\"\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\r2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0015J\b\u0010\u0011\u001a\u00020\u000bH\u0016J\u0012\u0010\u0012\u001a\u00020\u000b2\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0014J-\u0010\u0015\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u000e\u0010\u0016\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00180\u00172\u0006\u0010\u0019\u001a\u00020\u001aH\u0016\u00a2\u0006\u0002\u0010\u001bJ\b\u0010\u001c\u001a\u00020\u000bH\u0002R\u001c\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\t\u00a8\u0006\u001d"}, d2 = {"Lcom/impaktsoft/globeup/views/activities/MessengerActivity;", "Lcom/impaktsoft/globeup/views/activities/BaseActivity;", "Lcom/impaktsoft/globeup/viewmodels/MessengerActivityViewModel;", "()V", "customMessengerAppBar", "Lcom/impaktsoft/globeup/components/MyCustomMessengerAppBarView;", "getCustomMessengerAppBar", "()Lcom/impaktsoft/globeup/components/MyCustomMessengerAppBarView;", "setCustomMessengerAppBar", "(Lcom/impaktsoft/globeup/components/MyCustomMessengerAppBarView;)V", "onActivityResult", "", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "onBackPressed", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onRequestPermissionsResult", "permissions", "", "", "grantResults", "", "(I[Ljava/lang/String;[I)V", "setCustomSearchActionBar", "app_debug"})
public final class MessengerActivity extends com.impaktsoft.globeup.views.activities.BaseActivity<com.impaktsoft.globeup.viewmodels.MessengerActivityViewModel> {
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.components.MyCustomMessengerAppBarView customMessengerAppBar;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.components.MyCustomMessengerAppBarView getCustomMessengerAppBar() {
        return null;
    }
    
    public final void setCustomMessengerAppBar(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.components.MyCustomMessengerAppBarView p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void setCustomSearchActionBar() {
    }
    
    @androidx.annotation.RequiresApi(value = android.os.Build.VERSION_CODES.P)
    @java.lang.Override()
    protected void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    @java.lang.Override()
    public void onRequestPermissionsResult(int requestCode, @org.jetbrains.annotations.NotNull()
    java.lang.String[] permissions, @org.jetbrains.annotations.NotNull()
    int[] grantResults) {
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    public MessengerActivity() {
        super(null);
    }
}