package com.impaktsoft.globeup.services.database;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000j\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\f\bf\u0018\u00002\u00020\u0001J#\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\bJ!\u0010\t\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\n\u001a\u00020\u000bH\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\fJ!\u0010\r\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u000e\u001a\u00020\u000fH\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0010J!\u0010\u0011\u001a\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0013H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0014J!\u0010\u0015\u001a\u00020\u00032\u0006\u0010\u0016\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0005H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0017J!\u0010\u0018\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0019\u001a\u00020\u001aH\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u001bJ!\u0010\u001c\u001a\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0005H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0017J!\u0010\u001d\u001a\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0005H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0017J!\u0010\u001e\u001a\u00020\u001f2\u0006\u0010\u0012\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0005H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0017J\u001b\u0010 \u001a\u0004\u0018\u00010!2\u0006\u0010\u0004\u001a\u00020\u0005H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\"J)\u0010#\u001a\u0012\u0012\u0004\u0012\u00020%0$j\b\u0012\u0004\u0012\u00020%`&2\u0006\u0010\u0004\u001a\u00020\u0005H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\"J-\u0010\'\u001a\u0016\u0012\u0004\u0012\u00020(\u0018\u00010$j\n\u0012\u0004\u0012\u00020(\u0018\u0001`&2\u0006\u0010\u0012\u001a\u00020\u0005H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\"J\u001b\u0010)\u001a\u0004\u0018\u00010\u00052\u0006\u0010\u0016\u001a\u00020\u0005H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\"J\b\u0010*\u001a\u00020\u0003H&J\b\u0010+\u001a\u00020\u0003H&J\b\u0010,\u001a\u00020\u0003H&J\u0019\u0010-\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\"J\u0019\u0010.\u001a\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u0005H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\"J!\u0010/\u001a\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0005H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0017J!\u00100\u001a\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0005H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0017J+\u00101\u001a\u00020\u00032\u0006\u00102\u001a\u00020%2\u0006\u0010\u0004\u001a\u00020\u00052\b\u0010\u0006\u001a\u0004\u0018\u000103H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u00104J\u0019\u00105\u001a\u00020\u00032\u0006\u00106\u001a\u00020!H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u00107J!\u00108\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u00102\u001a\u00020%H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u00109J!\u0010:\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010;\u001a\u00020\u0005H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0017J!\u0010<\u001a\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0005H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0017J!\u0010=\u001a\u00020\u00032\u0006\u0010>\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0005H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0017\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006?"}, d2 = {"Lcom/impaktsoft/globeup/services/database/IChatDAO;", "", "addConversationMembersCountObserver", "", "conversationKey", "", "listener", "Lcom/impaktsoft/globeup/listeners/IGetConversationMembersCount;", "(Ljava/lang/String;Lcom/impaktsoft/globeup/listeners/IGetConversationMembersCount;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "addConversationMembersObserver", "membersKeyListener", "Lcom/impaktsoft/globeup/listeners/IGetMembersKeyListener;", "(Ljava/lang/String;Lcom/impaktsoft/globeup/listeners/IGetMembersKeyListener;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "addConversationMessagesObserver", "messageListener", "Lcom/impaktsoft/globeup/listeners/IGetMessagesListener;", "(Ljava/lang/String;Lcom/impaktsoft/globeup/listeners/IGetMessagesListener;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "addConversationsObserver", "userKey", "Lcom/impaktsoft/globeup/listeners/IGetConversationListener;", "(Ljava/lang/String;Lcom/impaktsoft/globeup/listeners/IGetConversationListener;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "addEventConversation", "eventKey", "(Ljava/lang/String;Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "addLastMessageCountObserver", "seenCount", "Lcom/impaktsoft/globeup/listeners/IGetLastMessageSeenCount;", "(Ljava/lang/String;Lcom/impaktsoft/globeup/listeners/IGetLastMessageSeenCount;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "addUserConversation", "addUserInConversation", "didUserSeenMessage", "", "fetchConversation", "Lcom/impaktsoft/globeup/models/ConversationPOJO;", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "fetchConversationMessages", "Ljava/util/ArrayList;", "Lcom/impaktsoft/globeup/models/MessagePOJO;", "Lkotlin/collections/ArrayList;", "fetchUserConversations", "Lcom/impaktsoft/globeup/models/ConversationRefPOJO;", "getEventConversationKey", "removeConversationMessagesCountObservers", "removeConversationsLastMessageObserver", "removeLastMessageDocument", "removeLastMessageState", "removeUserAllConversations", "removeUserConversation", "removeUserFromConversation", "sendMessage", "messagePOJO", "Lcom/impaktsoft/globeup/listeners/IMessageSentListener;", "(Lcom/impaktsoft/globeup/models/MessagePOJO;Ljava/lang/String;Lcom/impaktsoft/globeup/listeners/IMessageSentListener;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "storeConversation", "conversationPOJO", "(Lcom/impaktsoft/globeup/models/ConversationPOJO;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "updateConversationLastMessage", "(Ljava/lang/String;Lcom/impaktsoft/globeup/models/MessagePOJO;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "updateConversationName", "newName", "updateLastMessageState", "updateMessageState", "messageKey", "app_debug"})
public abstract interface IChatDAO {
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object addEventConversation(@org.jetbrains.annotations.NotNull()
    java.lang.String eventKey, @org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p2);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object addUserConversation(@org.jetbrains.annotations.NotNull()
    java.lang.String userKey, @org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p2);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object addUserInConversation(@org.jetbrains.annotations.NotNull()
    java.lang.String userKey, @org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p2);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object storeConversation(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.ConversationPOJO conversationPOJO, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p1);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object fetchConversation(@org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.impaktsoft.globeup.models.ConversationPOJO> p1);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object fetchUserConversations(@org.jetbrains.annotations.NotNull()
    java.lang.String userKey, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.ArrayList<com.impaktsoft.globeup.models.ConversationRefPOJO>> p1);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object removeUserConversation(@org.jetbrains.annotations.NotNull()
    java.lang.String userKey, @org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p2);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object removeUserFromConversation(@org.jetbrains.annotations.NotNull()
    java.lang.String userKey, @org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p2);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object sendMessage(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.MessagePOJO messagePOJO, @org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.listeners.IMessageSentListener listener, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p3);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object getEventConversationKey(@org.jetbrains.annotations.NotNull()
    java.lang.String eventKey, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.lang.String> p1);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object addConversationMessagesObserver(@org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.listeners.IGetMessagesListener messageListener, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p2);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object fetchConversationMessages(@org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.ArrayList<com.impaktsoft.globeup.models.MessagePOJO>> p1);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object updateConversationLastMessage(@org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.MessagePOJO messagePOJO, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p2);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object updateLastMessageState(@org.jetbrains.annotations.NotNull()
    java.lang.String userKey, @org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p2);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object removeLastMessageState(@org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p1);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object addLastMessageCountObserver(@org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.listeners.IGetLastMessageSeenCount seenCount, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p2);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object addConversationMembersCountObserver(@org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.listeners.IGetConversationMembersCount listener, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p2);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object addConversationMembersObserver(@org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.listeners.IGetMembersKeyListener membersKeyListener, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p2);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object updateMessageState(@org.jetbrains.annotations.NotNull()
    java.lang.String messageKey, @org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p2);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object addConversationsObserver(@org.jetbrains.annotations.NotNull()
    java.lang.String userKey, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.listeners.IGetConversationListener listener, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p2);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object didUserSeenMessage(@org.jetbrains.annotations.NotNull()
    java.lang.String userKey, @org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.lang.Boolean> p2);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object updateConversationName(@org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    java.lang.String newName, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p2);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object removeUserAllConversations(@org.jetbrains.annotations.NotNull()
    java.lang.String userKey, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p1);
    
    public abstract void removeConversationMessagesCountObservers();
    
    public abstract void removeConversationsLastMessageObserver();
    
    public abstract void removeLastMessageDocument();
}