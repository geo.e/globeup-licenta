package com.impaktsoft.globeup.bindingadapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u0004\u0018\u00010\u0004*\u00020\u0005H\u0007J\u0016\u0010\u0006\u001a\u00020\u0007*\u00020\u00052\b\u0010\b\u001a\u0004\u0018\u00010\tH\u0007J\u0016\u0010\n\u001a\u00020\u0007*\u00020\u00052\b\u0010\u000b\u001a\u0004\u0018\u00010\u0004H\u0007\u00a8\u0006\f"}, d2 = {"Lcom/impaktsoft/globeup/bindingadapters/MapBindingAdapters;", "", "()V", "getMyPosition", "Lcom/google/android/gms/maps/model/LatLng;", "Lcom/impaktsoft/globeup/components/MapViewWithPin;", "setListener", "", "listener", "Landroidx/databinding/InverseBindingListener;", "setMyPosition", "position", "app_debug"})
public final class MapBindingAdapters {
    public static final com.impaktsoft.globeup.bindingadapters.MapBindingAdapters INSTANCE = null;
    
    @androidx.databinding.BindingAdapter(value = {"cb_getPositionAttrChanged"})
    public static final void setListener(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.components.MapViewWithPin $this$setListener, @org.jetbrains.annotations.Nullable()
    androidx.databinding.InverseBindingListener listener) {
    }
    
    @androidx.databinding.BindingAdapter(value = {"cb_getPosition"})
    public static final void setMyPosition(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.components.MapViewWithPin $this$setMyPosition, @org.jetbrains.annotations.Nullable()
    com.google.android.gms.maps.model.LatLng position) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @androidx.databinding.InverseBindingAdapter(attribute = "cb_getPosition")
    public static final com.google.android.gms.maps.model.LatLng getMyPosition(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.components.MapViewWithPin $this$getMyPosition) {
        return null;
    }
    
    private MapBindingAdapters() {
        super();
    }
}