package com.impaktsoft.globeup.util;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lcom/impaktsoft/globeup/util/SearchStringUtils;", "", "()V", "Companion", "app_debug"})
public final class SearchStringUtils {
    public static final com.impaktsoft.globeup.util.SearchStringUtils.Companion Companion = null;
    
    public SearchStringUtils() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J6\u0010\u0003\u001a\u0012\u0012\u0004\u0012\u00020\u00050\u0004j\b\u0012\u0004\u0012\u00020\u0005`\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0016\u0010\t\u001a\u0012\u0012\u0004\u0012\u00020\u00050\u0004j\b\u0012\u0004\u0012\u00020\u0005`\u0006\u00a8\u0006\n"}, d2 = {"Lcom/impaktsoft/globeup/util/SearchStringUtils$Companion;", "", "()V", "searchConversationByName", "Ljava/util/ArrayList;", "Lcom/impaktsoft/globeup/models/ConversationPOJO;", "Lkotlin/collections/ArrayList;", "searchString", "", "conversationList", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.ArrayList<com.impaktsoft.globeup.models.ConversationPOJO> searchConversationByName(@org.jetbrains.annotations.NotNull()
        java.lang.String searchString, @org.jetbrains.annotations.NotNull()
        java.util.ArrayList<com.impaktsoft.globeup.models.ConversationPOJO> conversationList) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}