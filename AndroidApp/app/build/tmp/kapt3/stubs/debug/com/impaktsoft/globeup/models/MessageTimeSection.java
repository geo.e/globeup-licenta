package com.impaktsoft.globeup.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\b\u000b\u0018\u00002\u00020\u0001B\u001b\b\u0016\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006R\u001e\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u000b\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001c\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000f\u00a8\u0006\u0010"}, d2 = {"Lcom/impaktsoft/globeup/models/MessageTimeSection;", "Lcom/impaktsoft/globeup/models/IMessagePOJO;", "timeString", "", "timeLong", "", "(Ljava/lang/String;Ljava/lang/Long;)V", "getTimeLong", "()Ljava/lang/Long;", "setTimeLong", "(Ljava/lang/Long;)V", "Ljava/lang/Long;", "getTimeString", "()Ljava/lang/String;", "setTimeString", "(Ljava/lang/String;)V", "app_debug"})
public final class MessageTimeSection implements com.impaktsoft.globeup.models.IMessagePOJO {
    @org.jetbrains.annotations.Nullable()
    private java.lang.String timeString;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Long timeLong;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTimeString() {
        return null;
    }
    
    public final void setTimeString(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getTimeLong() {
        return null;
    }
    
    public final void setTimeLong(@org.jetbrains.annotations.Nullable()
    java.lang.Long p0) {
    }
    
    public MessageTimeSection(@org.jetbrains.annotations.Nullable()
    java.lang.String timeString, @org.jetbrains.annotations.Nullable()
    java.lang.Long timeLong) {
        super();
    }
}