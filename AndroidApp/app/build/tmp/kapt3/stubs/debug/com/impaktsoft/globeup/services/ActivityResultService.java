package com.impaktsoft.globeup.services;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\"\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\n2\b\u0010\f\u001a\u0004\u0018\u00010\rH\u0016J)\u0010\u000e\u001a\u00020\b2\u0006\u0010\u000f\u001a\u00020\r2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u0005\u001a\u00020\u0006H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0010R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0011"}, d2 = {"Lcom/impaktsoft/globeup/services/ActivityResultService;", "Lcom/impaktsoft/globeup/services/IActivityResultService;", "activityService", "Lcom/impaktsoft/globeup/services/ICurrentActivityService;", "(Lcom/impaktsoft/globeup/services/ICurrentActivityService;)V", "activityForResultListener", "Lcom/impaktsoft/globeup/listeners/IGetActivityForResultListener;", "onActivityForResultService", "", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "startActivityForResult", "intent", "(Landroid/content/Intent;ILcom/impaktsoft/globeup/listeners/IGetActivityForResultListener;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class ActivityResultService implements com.impaktsoft.globeup.services.IActivityResultService {
    private com.impaktsoft.globeup.listeners.IGetActivityForResultListener activityForResultListener;
    private final com.impaktsoft.globeup.services.ICurrentActivityService activityService = null;
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object startActivityForResult(@org.jetbrains.annotations.NotNull()
    android.content.Intent intent, int requestCode, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.listeners.IGetActivityForResultListener activityForResultListener, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p3) {
        return null;
    }
    
    @java.lang.Override()
    public void onActivityForResultService(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    public ActivityResultService(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.services.ICurrentActivityService activityService) {
        super();
    }
}