package com.impaktsoft.globeup.bindingadapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0013\u0010\n\u001a\u0004\u0018\u00010\u0004*\u00020\u000bH\u0007\u00a2\u0006\u0002\u0010\fJ\u0016\u0010\r\u001a\u00020\u000e*\u00020\u000b2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0007J\u0014\u0010\u0011\u001a\u00020\u000e*\u00020\u000b2\u0006\u0010\u0012\u001a\u00020\u0004H\u0007J\u0016\u0010\u0013\u001a\u00020\u000e*\u00020\u000b2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0014H\u0007R\u001e\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\t\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\u0015"}, d2 = {"Lcom/impaktsoft/globeup/bindingadapters/SeekBarBindingAdapters;", "", "()V", "lastValue", "", "getLastValue", "()Ljava/lang/Integer;", "setLastValue", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "getProgress", "Lcom/impaktsoft/globeup/components/CustomSeekBarWithTextView;", "(Lcom/impaktsoft/globeup/components/CustomSeekBarWithTextView;)Ljava/lang/Integer;", "setListener", "", "listener", "Landroidx/databinding/InverseBindingListener;", "setProgress", "value", "setScaleType", "Lcom/impaktsoft/globeup/enums/ScaleTypeEnum;", "app_debug"})
public final class SeekBarBindingAdapters {
    @org.jetbrains.annotations.Nullable()
    private static java.lang.Integer lastValue;
    public static final com.impaktsoft.globeup.bindingadapters.SeekBarBindingAdapters INSTANCE = null;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getLastValue() {
        return null;
    }
    
    public final void setLastValue(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @androidx.databinding.BindingAdapter(value = {"cb_progressAttrChanged"})
    public static final void setListener(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.components.CustomSeekBarWithTextView $this$setListener, @org.jetbrains.annotations.Nullable()
    androidx.databinding.InverseBindingListener listener) {
    }
    
    @androidx.databinding.BindingAdapter(value = {"cb_setMetricSystem"})
    public static final void setScaleType(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.components.CustomSeekBarWithTextView $this$setScaleType, @org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.enums.ScaleTypeEnum value) {
    }
    
    @androidx.databinding.BindingAdapter(value = {"cb_progress"})
    public static final void setProgress(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.components.CustomSeekBarWithTextView $this$setProgress, int value) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @androidx.databinding.InverseBindingAdapter(attribute = "cb_progress")
    public static final java.lang.Integer getProgress(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.components.CustomSeekBarWithTextView $this$getProgress) {
        return null;
    }
    
    private SeekBarBindingAdapters() {
        super();
    }
}