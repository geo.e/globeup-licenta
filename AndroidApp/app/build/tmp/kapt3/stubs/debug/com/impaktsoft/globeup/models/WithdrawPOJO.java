package com.impaktsoft.globeup.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\t\u0018\u00002\u00020\u0001B\u001b\b\u0016\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006R\u001c\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001a\u0010\u000b\u001a\u00020\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u001c\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014\u00a8\u0006\u0015"}, d2 = {"Lcom/impaktsoft/globeup/models/WithdrawPOJO;", "", "card_number", "", "withdrawType", "Lcom/impaktsoft/globeup/enums/WithdrawType;", "(Ljava/lang/String;Lcom/impaktsoft/globeup/enums/WithdrawType;)V", "getCard_number", "()Ljava/lang/String;", "setCard_number", "(Ljava/lang/String;)V", "selected", "", "getSelected", "()Z", "setSelected", "(Z)V", "getWithdrawType", "()Lcom/impaktsoft/globeup/enums/WithdrawType;", "setWithdrawType", "(Lcom/impaktsoft/globeup/enums/WithdrawType;)V", "app_debug"})
public final class WithdrawPOJO {
    private boolean selected;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String card_number;
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.enums.WithdrawType withdrawType;
    
    public final boolean getSelected() {
        return false;
    }
    
    public final void setSelected(boolean p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCard_number() {
        return null;
    }
    
    public final void setCard_number(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.enums.WithdrawType getWithdrawType() {
        return null;
    }
    
    public final void setWithdrawType(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.enums.WithdrawType p0) {
    }
    
    public WithdrawPOJO(@org.jetbrains.annotations.Nullable()
    java.lang.String card_number, @org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.enums.WithdrawType withdrawType) {
        super();
    }
}