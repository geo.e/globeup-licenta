package com.impaktsoft.globeup.services;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\b\u0010\u0005\u001a\u00020\u0006H\u0016J\b\u0010\u0007\u001a\u00020\u0006H\u0016Je\u0010\b\u001a\u00020\u0006\"\b\b\u0000\u0010\t*\u00020\n2\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u0002H\t0\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u000e2\b\u0010\u0010\u001a\u0004\u0018\u00010\u00112\b\u0010\u0012\u001a\u0004\u0018\u00010\u00112\u001a\u0010\u0013\u001a\u0016\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020\u00110\u0015\u0018\u00010\u0014H\u0016\u00a2\u0006\u0002\u0010\u0017J\u0018\u0010\u0018\u001a\u00020\u00062\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001aH\u0016J(\u0010\u0018\u001a\u00020\u0006\"\b\b\u0000\u0010\t*\u00020\u001c2\f\u0010\u001d\u001a\b\u0012\u0004\u0012\u0002H\t0\u001e2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u001f\u001a\u00020\u00062\u0006\u0010\u001b\u001a\u00020\u001aH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "}, d2 = {"Lcom/impaktsoft/globeup/services/NavigationService;", "Lcom/impaktsoft/globeup/services/INavigationService;", "currentActivityService", "Lcom/impaktsoft/globeup/services/ICurrentActivityService;", "(Lcom/impaktsoft/globeup/services/ICurrentActivityService;)V", "closeCurrentActivity", "", "closeCurrentActivityAfterTransition", "navigateToActivity", "T", "Landroid/app/Activity;", "activityClass", "Ljava/lang/Class;", "finishCurrentActivity", "", "clearBackStack", "parameterName", "", "parameterValue", "transitionPairs", "", "Landroidx/core/util/Pair;", "Landroid/view/View;", "(Ljava/lang/Class;ZZLjava/lang/String;Ljava/lang/String;[Landroidx/core/util/Pair;)V", "navigateToFragment", "transitionId", "", "navHostId", "Landroidx/fragment/app/Fragment;", "fragmentClass", "Lkotlin/reflect/KClass;", "popFragmentBackStack", "app_debug"})
public final class NavigationService implements com.impaktsoft.globeup.services.INavigationService {
    private final com.impaktsoft.globeup.services.ICurrentActivityService currentActivityService = null;
    
    @java.lang.Override()
    public <T extends android.app.Activity>void navigateToActivity(@org.jetbrains.annotations.NotNull()
    java.lang.Class<T> activityClass, boolean finishCurrentActivity, boolean clearBackStack, @org.jetbrains.annotations.Nullable()
    java.lang.String parameterName, @org.jetbrains.annotations.Nullable()
    java.lang.String parameterValue, @org.jetbrains.annotations.Nullable()
    androidx.core.util.Pair<android.view.View, java.lang.String>[] transitionPairs) {
    }
    
    @java.lang.Override()
    public void closeCurrentActivity() {
    }
    
    @java.lang.Override()
    public void navigateToFragment(int transitionId, int navHostId) {
    }
    
    @java.lang.Override()
    public <T extends androidx.fragment.app.Fragment>void navigateToFragment(@org.jetbrains.annotations.NotNull()
    kotlin.reflect.KClass<T> fragmentClass, boolean finishCurrentActivity) {
    }
    
    @java.lang.Override()
    public void popFragmentBackStack(int navHostId) {
    }
    
    @java.lang.Override()
    public void closeCurrentActivityAfterTransition() {
    }
    
    public NavigationService(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.services.ICurrentActivityService currentActivityService) {
        super();
    }
}