package com.impaktsoft.globeup.listadapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0013B\u001b\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\b\u0010\t\u001a\u00020\nH\u0016J\u0018\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u00022\u0006\u0010\u000e\u001a\u00020\nH\u0016J\u0018\u0010\u000f\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\nH\u0016R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"}, d2 = {"Lcom/impaktsoft/globeup/listadapters/WithdrawAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "list", "", "Lcom/impaktsoft/globeup/models/WithdrawPOJO;", "viewModel", "Lcom/impaktsoft/globeup/viewmodels/WithdrawViewModel;", "(Ljava/util/List;Lcom/impaktsoft/globeup/viewmodels/WithdrawViewModel;)V", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "TransactionViewHolder", "app_debug"})
public final class WithdrawAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder> {
    private java.util.List<com.impaktsoft.globeup.models.WithdrawPOJO> list;
    private com.impaktsoft.globeup.viewmodels.WithdrawViewModel viewModel;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public androidx.recyclerview.widget.RecyclerView.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.RecyclerView.ViewHolder holder, int position) {
    }
    
    public WithdrawAdapter(@org.jetbrains.annotations.NotNull()
    java.util.List<com.impaktsoft.globeup.models.WithdrawPOJO> list, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.viewmodels.WithdrawViewModel viewModel) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u000b"}, d2 = {"Lcom/impaktsoft/globeup/listadapters/WithdrawAdapter$TransactionViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "applicationBinding", "Lcom/impaktsoft/globeup/databinding/WithdrawCellBinding;", "(Lcom/impaktsoft/globeup/listadapters/WithdrawAdapter;Lcom/impaktsoft/globeup/databinding/WithdrawCellBinding;)V", "getApplicationBinding", "()Lcom/impaktsoft/globeup/databinding/WithdrawCellBinding;", "bind", "", "withdrawItem", "Lcom/impaktsoft/globeup/models/WithdrawPOJO;", "app_debug"})
    public final class TransactionViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private final com.impaktsoft.globeup.databinding.WithdrawCellBinding applicationBinding = null;
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.models.WithdrawPOJO withdrawItem) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.impaktsoft.globeup.databinding.WithdrawCellBinding getApplicationBinding() {
            return null;
        }
        
        public TransactionViewHolder(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.databinding.WithdrawCellBinding applicationBinding) {
            super(null);
        }
    }
}