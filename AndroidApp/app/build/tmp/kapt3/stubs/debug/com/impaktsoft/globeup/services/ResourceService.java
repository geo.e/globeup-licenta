package com.impaktsoft.globeup.services;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\tH\u0016J\u0012\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\f\u001a\u00020\rH\u0016R\u0016\u0010\u0003\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"}, d2 = {"Lcom/impaktsoft/globeup/services/ResourceService;", "Lcom/impaktsoft/globeup/services/IResourceService;", "()V", "weakResources", "Ljava/lang/ref/WeakReference;", "Landroid/content/res/Resources;", "initWithApplication", "", "application", "Landroid/app/Application;", "stringForId", "", "id", "", "app_debug"})
public final class ResourceService implements com.impaktsoft.globeup.services.IResourceService {
    private java.lang.ref.WeakReference<android.content.res.Resources> weakResources;
    
    @java.lang.Override()
    public void initWithApplication(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.String stringForId(int id) {
        return null;
    }
    
    public ResourceService() {
        super();
    }
}