package com.impaktsoft.globeup.services;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0006\n\u0002\b\u0005\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u0005H&J\u0018\u0010\u0006\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u0005H&J\u001a\u0010\u0007\u001a\u0004\u0018\u00010\u00042\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\tH&J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u000b\u001a\u00020\u0004H&J\u0012\u0010\f\u001a\u0004\u0018\u00010\u00042\u0006\u0010\r\u001a\u00020\u0004H&\u00a8\u0006\u000e"}, d2 = {"Lcom/impaktsoft/globeup/services/IMapService;", "", "getAllCountriesCode", "Ljava/util/ArrayList;", "", "Lkotlin/collections/ArrayList;", "getAllCountriesName", "getCountryCode", "lat", "", "long", "countryName", "getCountryName", "countryCode", "app_debug"})
public abstract interface IMapService {
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.String getCountryCode(double lat, double p1_1663806);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.String getCountryCode(@org.jetbrains.annotations.NotNull()
    java.lang.String countryName);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.String getCountryName(@org.jetbrains.annotations.NotNull()
    java.lang.String countryCode);
    
    @org.jetbrains.annotations.NotNull()
    public abstract java.util.ArrayList<java.lang.String> getAllCountriesName();
    
    @org.jetbrains.annotations.NotNull()
    public abstract java.util.ArrayList<java.lang.String> getAllCountriesCode();
}