package com.impaktsoft.globeup.viewmodels;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0010\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0019\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u0007H\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010!J\u000e\u0010\"\u001a\u00020\u001f2\u0006\u0010#\u001a\u00020\u0007J\u0011\u0010$\u001a\u00020\u001fH\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010%J\b\u0010&\u001a\u0004\u0018\u00010\u0004J\b\u0010\'\u001a\u00020\u001fH\u0002J\u0011\u0010(\u001a\u00020\u001fH\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010%J\u0011\u0010)\u001a\u00020\u001fH\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010%J\b\u0010*\u001a\u00020\u001fH\u0002J\b\u0010+\u001a\u00020\u001fH\u0002J\u000e\u0010,\u001a\u00020\u001f2\u0006\u0010-\u001a\u00020\u0013J\u0010\u0010.\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u0007H\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R \u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\u000bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\f\"\u0004\b\r\u0010\u000eR \u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\t0\u000bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\f\"\u0004\b\u0010\u0010\u000eR&\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\t0\u0012X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R \u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00190\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\u001b\"\u0004\b\u001c\u0010\u001d\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006/"}, d2 = {"Lcom/impaktsoft/globeup/viewmodels/MessagesBoundViewModel;", "Lcom/impaktsoft/globeup/viewmodels/BaseViewModel;", "()V", "conversationsAdapter", "Lcom/impaktsoft/globeup/listadapters/ConversationsAdapter;", "conversationsList", "Ljava/util/ArrayList;", "Lcom/impaktsoft/globeup/models/ConversationPOJO;", "isFetchDone", "", "isPlaceholderVisible", "Landroidx/lifecycle/MutableLiveData;", "()Landroidx/lifecycle/MutableLiveData;", "setPlaceholderVisible", "(Landroidx/lifecycle/MutableLiveData;)V", "isProgressBarVisible", "setProgressBarVisible", "messagesSeenByUser", "Ljava/util/HashMap;", "", "getMessagesSeenByUser", "()Ljava/util/HashMap;", "setMessagesSeenByUser", "(Ljava/util/HashMap;)V", "userConversationsList", "Lcom/impaktsoft/globeup/models/ConversationRefPOJO;", "getUserConversationsList", "()Ljava/util/ArrayList;", "setUserConversationsList", "(Ljava/util/ArrayList;)V", "checkConversationLastMessageState", "", "conversation", "(Lcom/impaktsoft/globeup/models/ConversationPOJO;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "conversationClick", "conversationPOJO", "fetchConversations", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getAdapter", "initConversations", "initConversationsObserver", "initLastMessagesObserver", "sortConversationListByLastMessageSeen", "sortConversationListByTime", "sortList", "searchString", "updateConversationList", "app_debug"})
public final class MessagesBoundViewModel extends com.impaktsoft.globeup.viewmodels.BaseViewModel {
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<com.impaktsoft.globeup.models.ConversationRefPOJO> userConversationsList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isProgressBarVisible;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isPlaceholderVisible;
    @org.jetbrains.annotations.NotNull()
    private java.util.HashMap<java.lang.String, java.lang.Boolean> messagesSeenByUser;
    private com.impaktsoft.globeup.listadapters.ConversationsAdapter conversationsAdapter;
    private java.util.ArrayList<com.impaktsoft.globeup.models.ConversationPOJO> conversationsList;
    private boolean isFetchDone;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.impaktsoft.globeup.models.ConversationRefPOJO> getUserConversationsList() {
        return null;
    }
    
    public final void setUserConversationsList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.impaktsoft.globeup.models.ConversationRefPOJO> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isProgressBarVisible() {
        return null;
    }
    
    public final void setProgressBarVisible(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isPlaceholderVisible() {
        return null;
    }
    
    public final void setPlaceholderVisible(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.HashMap<java.lang.String, java.lang.Boolean> getMessagesSeenByUser() {
        return null;
    }
    
    public final void setMessagesSeenByUser(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.listadapters.ConversationsAdapter getAdapter() {
        return null;
    }
    
    public final void conversationClick(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.ConversationPOJO conversationPOJO) {
    }
    
    public final void sortList(@org.jetbrains.annotations.NotNull()
    java.lang.String searchString) {
    }
    
    private final void initConversations() {
    }
    
    private final void updateConversationList(com.impaktsoft.globeup.models.ConversationPOJO conversation) {
    }
    
    private final void sortConversationListByTime() {
    }
    
    private final void sortConversationListByLastMessageSeen() {
    }
    
    public MessagesBoundViewModel() {
        super();
    }
}