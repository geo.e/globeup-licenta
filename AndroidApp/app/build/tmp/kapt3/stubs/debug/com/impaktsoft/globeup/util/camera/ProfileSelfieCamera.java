package com.impaktsoft.globeup.util.camera;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0002J\u0016\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012J\b\u0010\u0013\u001a\u00020\u0004H\u0002J\u0010\u0010\u0014\u001a\u00020\b2\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u001f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\n0\u00162\u0006\u0010\u000b\u001a\u00020\fH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0017J\u0010\u0010\u0018\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u0012H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082.\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0019"}, d2 = {"Lcom/impaktsoft/globeup/util/camera/ProfileSelfieCamera;", "", "()V", "capture", "Landroidx/camera/core/ImageCapture;", "initialized", "", "preview", "Landroidx/camera/core/Preview;", "getFileForProfilePicture", "Ljava/io/File;", "context", "Landroid/content/Context;", "setup", "", "lifecycleOwner", "Landroidx/lifecycle/LifecycleOwner;", "previewTexture", "Landroid/view/TextureView;", "setupImageCapture", "setupPreview", "takePicture", "Lcom/google/android/gms/tasks/Task;", "(Landroid/content/Context;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "updateTransform", "app_debug"})
public final class ProfileSelfieCamera {
    private androidx.camera.core.Preview preview;
    private androidx.camera.core.ImageCapture capture;
    private boolean initialized;
    
    public final void setup(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LifecycleOwner lifecycleOwner, @org.jetbrains.annotations.NotNull()
    android.view.TextureView previewTexture) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object takePicture(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.google.android.gms.tasks.Task<java.io.File>> p1) {
        return null;
    }
    
    private final androidx.camera.core.Preview setupPreview(android.view.TextureView previewTexture) {
        return null;
    }
    
    private final androidx.camera.core.ImageCapture setupImageCapture() {
        return null;
    }
    
    private final void updateTransform(android.view.TextureView previewTexture) {
    }
    
    private final java.io.File getFileForProfilePicture(android.content.Context context) {
        return null;
    }
    
    public ProfileSelfieCamera() {
        super();
    }
}