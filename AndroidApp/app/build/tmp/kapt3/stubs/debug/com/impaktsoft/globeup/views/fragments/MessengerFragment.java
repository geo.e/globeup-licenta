package com.impaktsoft.globeup.views.fragments;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0005\u00a2\u0006\u0002\u0010\u0004J\u001a\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016J\u0010\u0010\u0014\u001a\u00020\u000f2\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J\u0010\u0010\u0017\u001a\u00020\u000f2\u0006\u0010\u0018\u001a\u00020\u0003H\u0014R\u0014\u0010\u0005\u001a\u00020\u0006X\u0094D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR$\u0010\n\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\u0006@VX\u0094\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\b\"\u0004\b\f\u0010\r\u00a8\u0006\u0019"}, d2 = {"Lcom/impaktsoft/globeup/views/fragments/MessengerFragment;", "Lcom/impaktsoft/globeup/views/fragments/BaseBoundFragment;", "Lcom/impaktsoft/globeup/viewmodels/MessengerViewModel;", "Lcom/impaktsoft/globeup/databinding/FragmentMessengerBinding;", "()V", "fragmentNameResourceID", "", "getFragmentNameResourceID", "()I", "<set-?>", "layoutId", "getLayoutId", "setLayoutId", "(I)V", "onViewCreated", "", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "setActionBar", "conversationItem", "Lcom/impaktsoft/globeup/models/ConversationPOJO;", "setupDataBinding", "binding", "app_debug"})
public final class MessengerFragment extends com.impaktsoft.globeup.views.fragments.BaseBoundFragment<com.impaktsoft.globeup.viewmodels.MessengerViewModel, com.impaktsoft.globeup.databinding.FragmentMessengerBinding> {
    private int layoutId;
    private final int fragmentNameResourceID = com.impaktsoft.globeup.R.string.title_messages;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected int getLayoutId() {
        return 0;
    }
    
    public void setLayoutId(int p0) {
    }
    
    @java.lang.Override()
    protected int getFragmentNameResourceID() {
        return 0;
    }
    
    @java.lang.Override()
    protected void setupDataBinding(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.databinding.FragmentMessengerBinding binding) {
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void setActionBar(com.impaktsoft.globeup.models.ConversationPOJO conversationItem) {
    }
    
    public MessengerFragment() {
        super(null);
    }
}