package com.impaktsoft.globeup.dummy;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lcom/impaktsoft/globeup/dummy/FilterDummyData;", "", "()V", "Companion", "app_debug"})
public final class FilterDummyData {
    @org.jetbrains.annotations.NotNull()
    private static final java.util.List<com.impaktsoft.globeup.models.IFilterItem> eventTypeFilters = null;
    @org.jetbrains.annotations.NotNull()
    private static final java.util.List<com.impaktsoft.globeup.models.IFilterItem> eventTimeFilters = null;
    public static final com.impaktsoft.globeup.dummy.FilterDummyData.Companion Companion = null;
    
    public FilterDummyData() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rR\u0017\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0017\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\u0007\u00a8\u0006\u000e"}, d2 = {"Lcom/impaktsoft/globeup/dummy/FilterDummyData$Companion;", "", "()V", "eventTimeFilters", "", "Lcom/impaktsoft/globeup/models/IFilterItem;", "getEventTimeFilters", "()Ljava/util/List;", "eventTypeFilters", "getEventTypeFilters", "resetFilter", "", "filterName", "", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<com.impaktsoft.globeup.models.IFilterItem> getEventTypeFilters() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<com.impaktsoft.globeup.models.IFilterItem> getEventTimeFilters() {
            return null;
        }
        
        public final void resetFilter(@org.jetbrains.annotations.NotNull()
        java.lang.String filterName) {
        }
        
        private Companion() {
            super();
        }
    }
}