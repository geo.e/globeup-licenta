package com.impaktsoft.globeup.services.database;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\n\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0016R\u0010\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"}, d2 = {"Lcom/impaktsoft/globeup/services/database/FirebaseDynamicLinksService;", "Lcom/impaktsoft/globeup/services/database/IFirebaseDynamicLinksService;", "firebaseDynamicLinks", "Lcom/google/firebase/dynamiclinks/FirebaseDynamicLinks;", "(Lcom/google/firebase/dynamiclinks/FirebaseDynamicLinks;)V", "generateInvitationLink", "Landroid/net/Uri;", "app_debug"})
public final class FirebaseDynamicLinksService implements com.impaktsoft.globeup.services.database.IFirebaseDynamicLinksService {
    private com.google.firebase.dynamiclinks.FirebaseDynamicLinks firebaseDynamicLinks;
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.net.Uri generateInvitationLink() {
        return null;
    }
    
    public FirebaseDynamicLinksService(@org.jetbrains.annotations.NotNull()
    com.google.firebase.dynamiclinks.FirebaseDynamicLinks firebaseDynamicLinks) {
        super();
    }
}