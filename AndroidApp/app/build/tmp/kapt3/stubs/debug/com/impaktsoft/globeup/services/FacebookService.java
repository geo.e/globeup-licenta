package com.impaktsoft.globeup.services;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\b\u0016\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0007\u001a\u00020\u0004H\u0016J\u0018\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rH\u0016J\b\u0010\u000e\u001a\u00020\u000fH\u0016J\b\u0010\u0010\u001a\u00020\tH\u0016J\"\u0010\u0011\u001a\u00020\t2\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00132\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0016R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"}, d2 = {"Lcom/impaktsoft/globeup/services/FacebookService;", "Lcom/impaktsoft/globeup/services/IFacebookService;", "()V", "UserId", "", "callbackManager", "Lcom/facebook/CallbackManager;", "getUserId", "initLoginManager", "", "onSuccessListener", "Lcom/impaktsoft/globeup/listeners/IOnSuccessListener;", "onFailureListener", "Lcom/impaktsoft/globeup/listeners/IOnFailListener;", "isUserLoggedIn", "", "logoutIfPossible", "onActivityResult", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "app_debug"})
public final class FacebookService implements com.impaktsoft.globeup.services.IFacebookService {
    private com.facebook.CallbackManager callbackManager;
    private java.lang.String UserId;
    
    @java.lang.Override()
    public void logoutIfPossible() {
    }
    
    @java.lang.Override()
    public boolean isUserLoggedIn() {
        return false;
    }
    
    @java.lang.Override()
    public void initLoginManager(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.listeners.IOnSuccessListener onSuccessListener, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.listeners.IOnFailListener onFailureListener) {
    }
    
    @java.lang.Override()
    public void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String getUserId() {
        return null;
    }
    
    public FacebookService() {
        super();
    }
}