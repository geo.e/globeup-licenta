package com.impaktsoft.globeup.listadapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001#B%\u0012\u0016\u0010\u0003\u001a\u0012\u0012\u0004\u0012\u00020\u00050\u0004j\b\u0012\u0004\u0012\u00020\u0005`\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\tJ\b\u0010\u0019\u001a\u00020\rH\u0016J\u0006\u0010\u001a\u001a\u00020\u001bJ\u0018\u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u001d\u001a\u00020\u00022\u0006\u0010\u001e\u001a\u00020\rH\u0016J\u0018\u0010\u001f\u001a\u00020\u00022\u0006\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020\rH\u0016R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R*\u0010\u0003\u001a\u0012\u0012\u0004\u0012\u00020\u00050\u0004j\b\u0012\u0004\u0012\u00020\u0005`\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u0011\u0010\u0015\u001a\u00020\u0016\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006$"}, d2 = {"Lcom/impaktsoft/globeup/listadapters/FilterAppliedAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "list", "Ljava/util/ArrayList;", "Lcom/impaktsoft/globeup/models/IFilterItem;", "Lkotlin/collections/ArrayList;", "viewModel", "Lcom/impaktsoft/globeup/viewmodels/EventsViewModel;", "(Ljava/util/ArrayList;Lcom/impaktsoft/globeup/viewmodels/EventsViewModel;)V", "applicationBinding", "Lcom/impaktsoft/globeup/databinding/FilterAppliedCellBinding;", "cellImageSize", "", "cellTextSize", "", "cellWidth", "getList", "()Ljava/util/ArrayList;", "setList", "(Ljava/util/ArrayList;)V", "spanSizeLookup", "Landroidx/recyclerview/widget/GridLayoutManager$SpanSizeLookup;", "getSpanSizeLookup", "()Landroidx/recyclerview/widget/GridLayoutManager$SpanSizeLookup;", "getItemCount", "getViewWidth", "", "onBindViewHolder", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "FilterViewHolder", "app_debug"})
public final class FilterAppliedAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder> {
    private com.impaktsoft.globeup.databinding.FilterAppliedCellBinding applicationBinding;
    private int cellWidth;
    private float cellTextSize;
    private int cellImageSize;
    @org.jetbrains.annotations.NotNull()
    private final androidx.recyclerview.widget.GridLayoutManager.SpanSizeLookup spanSizeLookup = null;
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<com.impaktsoft.globeup.models.IFilterItem> list;
    private final com.impaktsoft.globeup.viewmodels.EventsViewModel viewModel = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.recyclerview.widget.GridLayoutManager.SpanSizeLookup getSpanSizeLookup() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public androidx.recyclerview.widget.RecyclerView.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.RecyclerView.ViewHolder holder, int position) {
    }
    
    public final void getViewWidth() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.impaktsoft.globeup.models.IFilterItem> getList() {
        return null;
    }
    
    public final void setList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.impaktsoft.globeup.models.IFilterItem> p0) {
    }
    
    public FilterAppliedAdapter(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.impaktsoft.globeup.models.IFilterItem> list, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.viewmodels.EventsViewModel viewModel) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fR\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\r"}, d2 = {"Lcom/impaktsoft/globeup/listadapters/FilterAppliedAdapter$FilterViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "applicationBinding", "Lcom/impaktsoft/globeup/databinding/FilterAppliedCellBinding;", "(Lcom/impaktsoft/globeup/listadapters/FilterAppliedAdapter;Lcom/impaktsoft/globeup/databinding/FilterAppliedCellBinding;)V", "getApplicationBinding", "()Lcom/impaktsoft/globeup/databinding/FilterAppliedCellBinding;", "setApplicationBinding", "(Lcom/impaktsoft/globeup/databinding/FilterAppliedCellBinding;)V", "bind", "", "item", "Lcom/impaktsoft/globeup/models/IFilterItem;", "app_debug"})
    public final class FilterViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private com.impaktsoft.globeup.databinding.FilterAppliedCellBinding applicationBinding;
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.models.IFilterItem item) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.impaktsoft.globeup.databinding.FilterAppliedCellBinding getApplicationBinding() {
            return null;
        }
        
        public final void setApplicationBinding(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.databinding.FilterAppliedCellBinding p0) {
        }
        
        public FilterViewHolder(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.databinding.FilterAppliedCellBinding applicationBinding) {
            super(null);
        }
    }
}