package com.impaktsoft.globeup.services.database;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u000f\u001a\u00020\f2\u0006\u0010\u0010\u001a\u00020\u0006H\u0016J\u0010\u0010\u0011\u001a\u00020\f2\u0006\u0010\u0010\u001a\u00020\u0006H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u0016\u0010\b\u001a\n \n*\u0004\u0018\u00010\t0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"}, d2 = {"Lcom/impaktsoft/globeup/services/database/MessagingService;", "Lcom/impaktsoft/globeup/services/database/IMessagingService;", "firebaseMessaging", "Lcom/google/firebase/messaging/FirebaseMessaging;", "(Lcom/google/firebase/messaging/FirebaseMessaging;)V", "FCM_API", "", "TAG", "apiService", "Lcom/impaktsoft/globeup/services/database/Notifications/NotificationAPI;", "kotlin.jvm.PlatformType", "sendNotificationMessage", "", "notificationMessage", "Lcom/impaktsoft/globeup/models/NotificationMessagePOJO;", "subscribeToTopic", "topicId", "unsubscribeFromATopic", "app_debug"})
public final class MessagingService implements com.impaktsoft.globeup.services.database.IMessagingService {
    private final java.lang.String TAG = "SubscribeToTopic";
    private final java.lang.String FCM_API = "https://fcm.googleapis.com/";
    private final com.impaktsoft.globeup.services.database.Notifications.NotificationAPI apiService = null;
    private final com.google.firebase.messaging.FirebaseMessaging firebaseMessaging = null;
    
    @java.lang.Override()
    public void subscribeToTopic(@org.jetbrains.annotations.NotNull()
    java.lang.String topicId) {
    }
    
    @java.lang.Override()
    public void unsubscribeFromATopic(@org.jetbrains.annotations.NotNull()
    java.lang.String topicId) {
    }
    
    @java.lang.Override()
    public void sendNotificationMessage(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.NotificationMessagePOJO notificationMessage) {
    }
    
    public MessagingService(@org.jetbrains.annotations.NotNull()
    com.google.firebase.messaging.FirebaseMessaging firebaseMessaging) {
        super();
    }
}