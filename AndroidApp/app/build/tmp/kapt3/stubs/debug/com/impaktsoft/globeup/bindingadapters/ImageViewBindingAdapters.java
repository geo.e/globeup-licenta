package com.impaktsoft.globeup.bindingadapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000l\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0007J\u0016\u0010\t\u001a\u00020\u0004*\u00020\u00062\b\u0010\n\u001a\u0004\u0018\u00010\u000bH\u0007J\u001e\u0010\f\u001a\u00020\u0004*\u00020\u00062\b\u0010\f\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\r\u001a\u00020\u000eH\u0007J\u0014\u0010\u000f\u001a\u00020\u0004*\u00020\u00062\u0006\u0010\u0010\u001a\u00020\u0011H\u0007J\u0014\u0010\u0012\u001a\u00020\u0004*\u00020\u00062\u0006\u0010\u0012\u001a\u00020\u0013H\u0007J\u0014\u0010\u0014\u001a\u00020\u0004*\u00020\u00062\u0006\u0010\u0015\u001a\u00020\u0016H\u0007J \u0010\u0017\u001a\u00020\u0004*\u00020\u00062\b\u0010\u0018\u001a\u0004\u0018\u00010\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u0019H\u0007J\u0014\u0010\u001b\u001a\u00020\u0004*\u00020\u00062\u0006\u0010\u001c\u001a\u00020\u001dH\u0007J\u0014\u0010\u001e\u001a\u00020\u0004*\u00020\u00062\u0006\u0010\u001f\u001a\u00020 H\u0007J\u0014\u0010\u001e\u001a\u00020\u0004*\u00020\u00062\u0006\u0010!\u001a\u00020\"H\u0007J\u0014\u0010#\u001a\u00020\u0004*\u00020\u00062\u0006\u0010$\u001a\u00020%H\u0007J\u0014\u0010&\u001a\u00020\u0004*\u00020\u00062\u0006\u0010\u0010\u001a\u00020\u0011H\u0007\u00a8\u0006\'"}, d2 = {"Lcom/impaktsoft/globeup/bindingadapters/ImageViewBindingAdapters;", "", "()V", "setTransactionImage", "", "imageView", "Landroid/widget/ImageView;", "transactionType", "Lcom/impaktsoft/globeup/enums/TransactionType;", "file", "filePath", "", "imageUrl", "placeHolder", "Landroid/graphics/drawable/Drawable;", "messageImageUrl", "messagePOJO", "Lcom/impaktsoft/globeup/models/MessagePOJO;", "messageState", "Lcom/impaktsoft/globeup/models/LastMessageState;", "setActionImage", "action", "Lcom/impaktsoft/globeup/models/ActionPOJO;", "setEventImageTint", "itemEventState", "Lcom/impaktsoft/globeup/enums/EventState;", "currentViewEventState", "setFieldImageTint", "boolean", "", "setImage", "withdrawPOJO", "Lcom/impaktsoft/globeup/models/WithdrawPOJO;", "imageResource", "", "setLevelImage", "levelPOJO", "Lcom/impaktsoft/globeup/models/LevelPOJO;", "setVisibilityDependingOnState", "app_debug"})
public final class ImageViewBindingAdapters {
    public static final com.impaktsoft.globeup.bindingadapters.ImageViewBindingAdapters INSTANCE = null;
    
    @androidx.databinding.BindingAdapter(requireAll = true, value = {"cb_imageUrl", "cb_placeholder"})
    public static final void imageUrl(@org.jetbrains.annotations.NotNull()
    android.widget.ImageView $this$imageUrl, @org.jetbrains.annotations.Nullable()
    java.lang.String imageUrl, @org.jetbrains.annotations.NotNull()
    android.graphics.drawable.Drawable placeHolder) {
    }
    
    @androidx.databinding.BindingAdapter(value = {"cb_filePath"})
    public static final void file(@org.jetbrains.annotations.NotNull()
    android.widget.ImageView $this$file, @org.jetbrains.annotations.Nullable()
    java.lang.String filePath) {
    }
    
    @android.annotation.SuppressLint(value = {"ResourceAsColor"})
    @androidx.annotation.RequiresApi(value = android.os.Build.VERSION_CODES.M)
    @androidx.databinding.BindingAdapter(value = {"cb_eventImageTint", "cb_currentViewEventState"})
    public static final void setEventImageTint(@org.jetbrains.annotations.NotNull()
    android.widget.ImageView $this$setEventImageTint, @org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.enums.EventState itemEventState, @org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.enums.EventState currentViewEventState) {
    }
    
    @androidx.annotation.RequiresApi(value = android.os.Build.VERSION_CODES.M)
    @androidx.databinding.BindingAdapter(value = {"cb_FieldImageTint"})
    public static final void setFieldImageTint(@org.jetbrains.annotations.NotNull()
    android.widget.ImageView $this$setFieldImageTint, boolean p1_32355860) {
    }
    
    @androidx.annotation.RequiresApi(value = android.os.Build.VERSION_CODES.M)
    @androidx.databinding.BindingAdapter(value = {"cb_transcationImage"})
    public static final void setTransactionImage(@org.jetbrains.annotations.NotNull()
    android.widget.ImageView imageView, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.enums.TransactionType transactionType) {
    }
    
    @androidx.annotation.RequiresApi(value = android.os.Build.VERSION_CODES.M)
    @androidx.databinding.BindingAdapter(value = {"cb_actionImage"})
    public static final void setActionImage(@org.jetbrains.annotations.NotNull()
    android.widget.ImageView $this$setActionImage, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.ActionPOJO action) {
    }
    
    @androidx.annotation.RequiresApi(value = android.os.Build.VERSION_CODES.M)
    @androidx.databinding.BindingAdapter(value = {"cb_levelImage"})
    public static final void setLevelImage(@org.jetbrains.annotations.NotNull()
    android.widget.ImageView $this$setLevelImage, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.LevelPOJO levelPOJO) {
    }
    
    @androidx.annotation.RequiresApi(value = android.os.Build.VERSION_CODES.M)
    @androidx.databinding.BindingAdapter(value = {"cb_withdrawImage"})
    public static final void setImage(@org.jetbrains.annotations.NotNull()
    android.widget.ImageView $this$setImage, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.WithdrawPOJO withdrawPOJO) {
    }
    
    @androidx.databinding.BindingAdapter(value = {"cb_messageImageUrl"})
    public static final void messageImageUrl(@org.jetbrains.annotations.NotNull()
    android.widget.ImageView $this$messageImageUrl, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.MessagePOJO messagePOJO) {
    }
    
    @androidx.databinding.BindingAdapter(value = {"cb_messageState"})
    public static final void messageState(@org.jetbrains.annotations.NotNull()
    android.widget.ImageView $this$messageState, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.LastMessageState messageState) {
    }
    
    @androidx.databinding.BindingAdapter(value = {"cb_imageResource"})
    public static final void setImage(@org.jetbrains.annotations.NotNull()
    android.widget.ImageView $this$setImage, int imageResource) {
    }
    
    @androidx.databinding.BindingAdapter(value = {"cb_isMessageImageVisible"})
    public static final void setVisibilityDependingOnState(@org.jetbrains.annotations.NotNull()
    android.widget.ImageView $this$setVisibilityDependingOnState, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.MessagePOJO messagePOJO) {
    }
    
    private ImageViewBindingAdapters() {
        super();
    }
}