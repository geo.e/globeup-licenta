package com.impaktsoft.globeup.services;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0002\u0010\u0000\n\u0002\b\t\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u001d\u0010\u000b\u001a\u0004\u0018\u0001H\f\"\u0004\b\u0000\u0010\f2\u0006\u0010\r\u001a\u00020\u0005H\u0016\u00a2\u0006\u0002\u0010\u000eJ\u0018\u0010\u000f\u001a\u00020\u00102\u0006\u0010\r\u001a\u00020\u00052\u0006\u0010\u0011\u001a\u00020\u0006H\u0016R&\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\n\u00a8\u0006\u0012"}, d2 = {"Lcom/impaktsoft/globeup/services/DataExchangeService;", "Lcom/impaktsoft/globeup/services/IDataExchangeService;", "()V", "exchangeMap", "", "", "", "getExchangeMap", "()Ljava/util/Map;", "setExchangeMap", "(Ljava/util/Map;)V", "get", "T", "key", "(Ljava/lang/String;)Ljava/lang/Object;", "put", "", "data", "app_debug"})
public final class DataExchangeService implements com.impaktsoft.globeup.services.IDataExchangeService {
    @org.jetbrains.annotations.NotNull()
    private java.util.Map<java.lang.String, java.lang.Object> exchangeMap;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.Map<java.lang.String, java.lang.Object> getExchangeMap() {
        return null;
    }
    
    public final void setExchangeMap(@org.jetbrains.annotations.NotNull()
    java.util.Map<java.lang.String, java.lang.Object> p0) {
    }
    
    @java.lang.Override()
    public void put(@org.jetbrains.annotations.NotNull()
    java.lang.String key, @org.jetbrains.annotations.NotNull()
    java.lang.Object data) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @kotlin.Suppress(names = {"UNCHECKED_CAST"})
    @java.lang.Override()
    public <T extends java.lang.Object>T get(@org.jetbrains.annotations.NotNull()
    java.lang.String key) {
        return null;
    }
    
    public DataExchangeService() {
        super();
    }
}