package com.impaktsoft.globeup.viewmodels;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0014\u001a\u00020\u0015J\u0006\u0010\u0016\u001a\u00020\u0015J\u0006\u0010\u0017\u001a\u00020\u0015J\u0006\u0010\u0018\u001a\u00020\u0015J\u0006\u0010\u0019\u001a\u00020\u0015J\u000e\u0010\u001a\u001a\u00020\u00152\u0006\u0010\u001b\u001a\u00020\u000bR \u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\u001c\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR \u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00110\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0007\"\u0004\b\u0013\u0010\t\u00a8\u0006\u001c"}, d2 = {"Lcom/impaktsoft/globeup/viewmodels/ImageCaptureViewModel;", "Lcom/impaktsoft/globeup/viewmodels/BaseViewModel;", "()V", "hidePreviewView", "Landroidx/lifecycle/MutableLiveData;", "", "getHidePreviewView", "()Landroidx/lifecycle/MutableLiveData;", "setHidePreviewView", "(Landroidx/lifecycle/MutableLiveData;)V", "imageFile", "Ljava/io/File;", "getImageFile", "()Ljava/io/File;", "setImageFile", "(Ljava/io/File;)V", "imagePath", "", "getImagePath", "setImagePath", "accept", "", "captureImageFailed", "deletePreviousPhotoFile", "imageCaptured", "retry", "setMyImageFile", "file", "app_debug"})
public final class ImageCaptureViewModel extends com.impaktsoft.globeup.viewmodels.BaseViewModel {
    @org.jetbrains.annotations.Nullable()
    private java.io.File imageFile;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> imagePath;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> hidePreviewView;
    
    @org.jetbrains.annotations.Nullable()
    public final java.io.File getImageFile() {
        return null;
    }
    
    public final void setImageFile(@org.jetbrains.annotations.Nullable()
    java.io.File p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getImagePath() {
        return null;
    }
    
    public final void setImagePath(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getHidePreviewView() {
        return null;
    }
    
    public final void setHidePreviewView(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    public final void setMyImageFile(@org.jetbrains.annotations.NotNull()
    java.io.File file) {
    }
    
    public final void imageCaptured() {
    }
    
    public final void captureImageFailed() {
    }
    
    public final void retry() {
    }
    
    public final void accept() {
    }
    
    public final void deletePreviousPhotoFile() {
    }
    
    public ImageCaptureViewModel() {
        super();
    }
}