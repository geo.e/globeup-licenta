package com.impaktsoft.globeup.viewmodels;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u0006\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020,J\u000e\u0010-\u001a\u00020*2\u0006\u0010.\u001a\u00020,J\u0006\u0010/\u001a\u00020*R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0011\u001a\u00020\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\f\"\u0004\b\u0013\u0010\u000eR \u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00160\u0015X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001aR\u001c\u0010\u001b\u001a\u0004\u0018\u00010\u0016X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u001d\"\u0004\b\u001e\u0010\u001fR \u0010 \u001a\b\u0012\u0004\u0012\u00020\n0!X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010#\"\u0004\b$\u0010%R \u0010&\u001a\b\u0012\u0004\u0012\u00020\u00100!X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\'\u0010#\"\u0004\b(\u0010%\u00a8\u00060"}, d2 = {"Lcom/impaktsoft/globeup/viewmodels/WithdrawViewModel;", "Lcom/impaktsoft/globeup/viewmodels/BaseViewModel;", "()V", "adapter", "Lcom/impaktsoft/globeup/listadapters/WithdrawAdapter;", "getAdapter", "()Lcom/impaktsoft/globeup/listadapters/WithdrawAdapter;", "setAdapter", "(Lcom/impaktsoft/globeup/listadapters/WithdrawAdapter;)V", "currentGcoins", "", "getCurrentGcoins", "()Ljava/lang/String;", "setCurrentGcoins", "(Ljava/lang/String;)V", "gcoinValue", "", "gcoinValueString", "getGcoinValueString", "setGcoinValueString", "myListOfWithdraws", "", "Lcom/impaktsoft/globeup/models/WithdrawPOJO;", "getMyListOfWithdraws", "()Ljava/util/List;", "setMyListOfWithdraws", "(Ljava/util/List;)V", "selectedWithdraw", "getSelectedWithdraw", "()Lcom/impaktsoft/globeup/models/WithdrawPOJO;", "setSelectedWithdraw", "(Lcom/impaktsoft/globeup/models/WithdrawPOJO;)V", "total_value_string", "Landroidx/lifecycle/MutableLiveData;", "getTotal_value_string", "()Landroidx/lifecycle/MutableLiveData;", "setTotal_value_string", "(Landroidx/lifecycle/MutableLiveData;)V", "withdraw_value", "getWithdraw_value", "setWithdraw_value", "selectPayMethod", "", "position", "", "setWithdrawValue", "value", "withdrawClick", "app_debug"})
public final class WithdrawViewModel extends com.impaktsoft.globeup.viewmodels.BaseViewModel {
    @org.jetbrains.annotations.NotNull()
    private java.lang.String currentGcoins;
    private double gcoinValue;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String gcoinValueString;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Double> withdraw_value;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> total_value_string;
    @org.jetbrains.annotations.NotNull()
    private java.util.List<com.impaktsoft.globeup.models.WithdrawPOJO> myListOfWithdraws;
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.models.WithdrawPOJO selectedWithdraw;
    @org.jetbrains.annotations.NotNull()
    private com.impaktsoft.globeup.listadapters.WithdrawAdapter adapter;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getCurrentGcoins() {
        return null;
    }
    
    public final void setCurrentGcoins(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getGcoinValueString() {
        return null;
    }
    
    public final void setGcoinValueString(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Double> getWithdraw_value() {
        return null;
    }
    
    public final void setWithdraw_value(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Double> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getTotal_value_string() {
        return null;
    }
    
    public final void setTotal_value_string(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.impaktsoft.globeup.models.WithdrawPOJO> getMyListOfWithdraws() {
        return null;
    }
    
    public final void setMyListOfWithdraws(@org.jetbrains.annotations.NotNull()
    java.util.List<com.impaktsoft.globeup.models.WithdrawPOJO> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.models.WithdrawPOJO getSelectedWithdraw() {
        return null;
    }
    
    public final void setSelectedWithdraw(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.models.WithdrawPOJO p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.impaktsoft.globeup.listadapters.WithdrawAdapter getAdapter() {
        return null;
    }
    
    public final void setAdapter(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.listadapters.WithdrawAdapter p0) {
    }
    
    public final void setWithdrawValue(int value) {
    }
    
    public final void selectPayMethod(int position) {
    }
    
    public final void withdrawClick() {
    }
    
    public WithdrawViewModel() {
        super();
    }
}