package com.impaktsoft.globeup.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\f\u0018\u00002\u00020\u0001BI\b\u0016\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00122\u0010\u0005\u001a.\u0012\u0004\u0012\u00020\u0007\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\b\u0018\u00010\u0006j\u0016\u0012\u0004\u0012\u00020\u0007\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\b\u0018\u0001`\n\u00a2\u0006\u0002\u0010\u000bRF\u0010\f\u001a.\u0012\u0004\u0012\u00020\u0007\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\b\u0018\u00010\u0006j\u0016\u0012\u0004\u0012\u00020\u0007\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\b\u0018\u0001`\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R$\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0015\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014\u00a8\u0006\u0016"}, d2 = {"Lcom/impaktsoft/globeup/models/AlertBuilderSettings;", "", "itemsName", "", "", "itemAction", "Ljava/util/HashMap;", "", "Lkotlin/Function0;", "", "Lkotlin/collections/HashMap;", "([Ljava/lang/CharSequence;Ljava/util/HashMap;)V", "itemActions", "getItemActions", "()Ljava/util/HashMap;", "setItemActions", "(Ljava/util/HashMap;)V", "getItemsName", "()[Ljava/lang/CharSequence;", "setItemsName", "([Ljava/lang/CharSequence;)V", "[Ljava/lang/CharSequence;", "app_debug"})
public final class AlertBuilderSettings {
    @org.jetbrains.annotations.Nullable()
    private java.lang.CharSequence[] itemsName;
    @org.jetbrains.annotations.Nullable()
    private java.util.HashMap<java.lang.String, kotlin.jvm.functions.Function0<kotlin.Unit>> itemActions;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.CharSequence[] getItemsName() {
        return null;
    }
    
    public final void setItemsName(@org.jetbrains.annotations.Nullable()
    java.lang.CharSequence[] p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.HashMap<java.lang.String, kotlin.jvm.functions.Function0<kotlin.Unit>> getItemActions() {
        return null;
    }
    
    public final void setItemActions(@org.jetbrains.annotations.Nullable()
    java.util.HashMap<java.lang.String, kotlin.jvm.functions.Function0<kotlin.Unit>> p0) {
    }
    
    public AlertBuilderSettings(@org.jetbrains.annotations.NotNull()
    java.lang.CharSequence[] itemsName, @org.jetbrains.annotations.Nullable()
    java.util.HashMap<java.lang.String, kotlin.jvm.functions.Function0<kotlin.Unit>> itemAction) {
        super();
    }
}