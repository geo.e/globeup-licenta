package com.impaktsoft.globeup.components;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000p\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\f\n\u0002\u0010\u0007\n\u0002\b\u0005\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u000f\b\u0016\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006B\u0017\b\u0016\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\tB\u001f\b\u0016\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\fJ\u0010\u0010%\u001a\u00020&2\u0006\u0010\'\u001a\u00020(H\u0016J\u0006\u0010)\u001a\u00020*J\u0006\u0010+\u001a\u00020*J\b\u0010,\u001a\u00020*H\u0007J\b\u0010-\u001a\u00020*H\u0007J\b\u0010.\u001a\u00020*H\u0007J\b\u0010/\u001a\u00020*H\u0007J\b\u00100\u001a\u00020*H\u0007J\b\u00101\u001a\u00020*H\u0007J\u0012\u00102\u001a\u00020*2\b\u00103\u001a\u0004\u0018\u00010\u0016H\u0016J\u001f\u00104\u001a\u00020*2\u0006\u00105\u001a\u00020\u001e2\n\b\u0002\u00106\u001a\u0004\u0018\u000107\u00a2\u0006\u0002\u00108J\u0010\u00109\u001a\u00020*2\b\u0010:\u001a\u0004\u0018\u00010\u001cJ\b\u0010;\u001a\u00020*H\u0007R\u001c\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0015\u001a\u0004\u0018\u00010\u0016X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001aR\u0010\u0010\u001b\u001a\u0004\u0018\u00010\u001cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u001d\u001a\u0004\u0018\u00010\u001eX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010 \"\u0004\b!\u0010\"R\u0010\u0010#\u001a\u0004\u0018\u00010$X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006<"}, d2 = {"Lcom/impaktsoft/globeup/components/MapViewWithPin;", "Landroid/widget/FrameLayout;", "Lcom/google/android/gms/maps/OnMapReadyCallback;", "Landroidx/lifecycle/LifecycleObserver;", "var1", "Landroid/content/Context;", "(Landroid/content/Context;)V", "var2", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "var3", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "getLocationListener", "Lcom/impaktsoft/globeup/listeners/IGetLocationListener;", "getGetLocationListener", "()Lcom/impaktsoft/globeup/listeners/IGetLocationListener;", "setGetLocationListener", "(Lcom/impaktsoft/globeup/listeners/IGetLocationListener;)V", "locationManager", "Landroid/location/LocationManager;", "mMap", "Lcom/google/android/gms/maps/GoogleMap;", "getMMap", "()Lcom/google/android/gms/maps/GoogleMap;", "setMMap", "(Lcom/google/android/gms/maps/GoogleMap;)V", "mapBundle", "Landroid/os/Bundle;", "mapPosition", "Lcom/google/android/gms/maps/model/LatLng;", "getMapPosition", "()Lcom/google/android/gms/maps/model/LatLng;", "setMapPosition", "(Lcom/google/android/gms/maps/model/LatLng;)V", "mapView", "Lcom/google/android/gms/maps/MapView;", "dispatchTouchEvent", "", "ev", "Landroid/view/MotionEvent;", "getMapAsync", "", "initLocationListener", "lifecycleOnCreate", "lifecycleOnDestroy", "lifecycleOnPause", "lifecycleOnResume", "lifecycleOnStart", "lifecycleOnStop", "onMapReady", "p0", "setCameraPosition", "coords", "zoom", "", "(Lcom/google/android/gms/maps/model/LatLng;Ljava/lang/Float;)V", "setMapBundle", "bundle", "updateMyLocation", "app_debug"})
public final class MapViewWithPin extends android.widget.FrameLayout implements com.google.android.gms.maps.OnMapReadyCallback, androidx.lifecycle.LifecycleObserver {
    @org.jetbrains.annotations.Nullable()
    private com.google.android.gms.maps.GoogleMap mMap;
    private android.location.LocationManager locationManager;
    private android.os.Bundle mapBundle;
    private com.google.android.gms.maps.MapView mapView;
    @org.jetbrains.annotations.Nullable()
    private com.google.android.gms.maps.model.LatLng mapPosition;
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.listeners.IGetLocationListener getLocationListener;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final com.google.android.gms.maps.GoogleMap getMMap() {
        return null;
    }
    
    public final void setMMap(@org.jetbrains.annotations.Nullable()
    com.google.android.gms.maps.GoogleMap p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.google.android.gms.maps.model.LatLng getMapPosition() {
        return null;
    }
    
    public final void setMapPosition(@org.jetbrains.annotations.Nullable()
    com.google.android.gms.maps.model.LatLng p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.listeners.IGetLocationListener getGetLocationListener() {
        return null;
    }
    
    public final void setGetLocationListener(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.listeners.IGetLocationListener p0) {
    }
    
    @java.lang.Override()
    public void onMapReady(@org.jetbrains.annotations.Nullable()
    com.google.android.gms.maps.GoogleMap p0) {
    }
    
    public final void setCameraPosition(@org.jetbrains.annotations.NotNull()
    com.google.android.gms.maps.model.LatLng coords, @org.jetbrains.annotations.Nullable()
    java.lang.Float zoom) {
    }
    
    @java.lang.Override()
    public boolean dispatchTouchEvent(@org.jetbrains.annotations.NotNull()
    android.view.MotionEvent ev) {
        return false;
    }
    
    @android.annotation.SuppressLint(value = {"MissingPermission"})
    public final void updateMyLocation() {
    }
    
    public final void initLocationListener() {
    }
    
    public final void getMapAsync() {
    }
    
    public final void setMapBundle(@org.jetbrains.annotations.Nullable()
    android.os.Bundle bundle) {
    }
    
    @androidx.lifecycle.OnLifecycleEvent(value = androidx.lifecycle.Lifecycle.Event.ON_CREATE)
    public final void lifecycleOnCreate() {
    }
    
    @androidx.lifecycle.OnLifecycleEvent(value = androidx.lifecycle.Lifecycle.Event.ON_RESUME)
    public final void lifecycleOnResume() {
    }
    
    @androidx.lifecycle.OnLifecycleEvent(value = androidx.lifecycle.Lifecycle.Event.ON_START)
    public final void lifecycleOnStart() {
    }
    
    @androidx.lifecycle.OnLifecycleEvent(value = androidx.lifecycle.Lifecycle.Event.ON_STOP)
    public final void lifecycleOnStop() {
    }
    
    @androidx.lifecycle.OnLifecycleEvent(value = androidx.lifecycle.Lifecycle.Event.ON_PAUSE)
    public final void lifecycleOnPause() {
    }
    
    @androidx.lifecycle.OnLifecycleEvent(value = androidx.lifecycle.Lifecycle.Event.ON_DESTROY)
    public final void lifecycleOnDestroy() {
    }
    
    public MapViewWithPin(@org.jetbrains.annotations.NotNull()
    android.content.Context var1) {
        super(null);
    }
    
    public MapViewWithPin(@org.jetbrains.annotations.NotNull()
    android.content.Context var1, @org.jetbrains.annotations.NotNull()
    android.util.AttributeSet var2) {
        super(null);
    }
    
    public MapViewWithPin(@org.jetbrains.annotations.NotNull()
    android.content.Context var1, @org.jetbrains.annotations.NotNull()
    android.util.AttributeSet var2, int var3) {
        super(null);
    }
}