package com.impaktsoft.globeup.viewmodels;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0014\u001a\u00020\u0015J\u0010\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u0017\u001a\u00020\u0018H\u0007J\b\u0010\u0019\u001a\u0004\u0018\u00010\nJ\b\u0010\u001a\u001a\u0004\u0018\u00010\u0013J\u0016\u0010\u001b\u001a\u0012\u0012\u0004\u0012\u00020\u00050\u0004j\b\u0012\u0004\u0012\u00020\u0005`\u001cJ\u0006\u0010\u001d\u001a\u00020\rJ\b\u0010\u001e\u001a\u00020\u0015H\u0016J\b\u0010\u001f\u001a\u00020\u0015H\u0016J\u000e\u0010 \u001a\u00020\u00152\u0006\u0010!\u001a\u00020\u0007J\u000e\u0010\"\u001a\u00020\u00152\u0006\u0010!\u001a\u00020\u0007R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00070\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R \u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006#"}, d2 = {"Lcom/impaktsoft/globeup/viewmodels/SelectMembersViewModel;", "Lcom/impaktsoft/globeup/viewmodels/BaseViewModel;", "()V", "contacts", "Ljava/util/ArrayList;", "Lcom/impaktsoft/globeup/models/ContactPOJO;", "myUsers", "Lcom/impaktsoft/globeup/models/UserPOJO;", "selectedUsers", "selectedUsersAdapter", "Lcom/impaktsoft/globeup/listadapters/SelectedUsersAdapter;", "selectedUsersSize", "Landroidx/lifecycle/MutableLiveData;", "", "getSelectedUsersSize", "()Landroidx/lifecycle/MutableLiveData;", "setSelectedUsersSize", "(Landroidx/lifecycle/MutableLiveData;)V", "usersContactsAdapter", "Lcom/impaktsoft/globeup/listadapters/GroupContactsAdapter;", "createGroupClick", "", "filterList", "string", "", "getSelectedAdapter", "getUsersAdapter", "getUsersAsContactForSelect", "Lkotlin/collections/ArrayList;", "getUsersSize", "onCreate", "onDestroy", "selectUser", "user", "unselectUser", "app_debug"})
public final class SelectMembersViewModel extends com.impaktsoft.globeup.viewmodels.BaseViewModel {
    private com.impaktsoft.globeup.listadapters.GroupContactsAdapter usersContactsAdapter;
    private com.impaktsoft.globeup.listadapters.SelectedUsersAdapter selectedUsersAdapter;
    private java.util.ArrayList<com.impaktsoft.globeup.models.UserPOJO> selectedUsers;
    private java.util.ArrayList<com.impaktsoft.globeup.models.UserPOJO> myUsers;
    private java.util.ArrayList<com.impaktsoft.globeup.models.ContactPOJO> contacts;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Integer> selectedUsersSize;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Integer> getSelectedUsersSize() {
        return null;
    }
    
    public final void setSelectedUsersSize(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Integer> p0) {
    }
    
    public final int getUsersSize() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.impaktsoft.globeup.models.ContactPOJO> getUsersAsContactForSelect() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.listadapters.GroupContactsAdapter getUsersAdapter() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.listadapters.SelectedUsersAdapter getSelectedAdapter() {
        return null;
    }
    
    public final void createGroupClick() {
    }
    
    public final void unselectUser(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.UserPOJO user) {
    }
    
    public final void selectUser(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.UserPOJO user) {
    }
    
    @org.greenrobot.eventbus.Subscribe(threadMode = org.greenrobot.eventbus.ThreadMode.MAIN)
    public final void filterList(@org.jetbrains.annotations.NotNull()
    java.lang.String string) {
    }
    
    @java.lang.Override()
    public void onCreate() {
    }
    
    @java.lang.Override()
    public void onDestroy() {
    }
    
    public SelectMembersViewModel() {
        super();
    }
}