package com.impaktsoft.globeup.util;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0007J\u0018\u0010\t\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\fH\u0002J\u000e\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010\u00a8\u0006\u0011"}, d2 = {"Lcom/impaktsoft/globeup/util/ImageUtils;", "", "()V", "getBitmap", "Landroid/graphics/Bitmap;", "activity", "Landroid/app/Activity;", "imageUri", "Landroid/net/Uri;", "rotateImage", "source", "angle", "", "rotateImageIfNecessary", "", "photoPath", "", "app_debug"})
public final class ImageUtils {
    public static final com.impaktsoft.globeup.util.ImageUtils INSTANCE = null;
    
    @org.jetbrains.annotations.Nullable()
    @androidx.annotation.RequiresApi(value = android.os.Build.VERSION_CODES.P)
    public final android.graphics.Bitmap getBitmap(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity, @org.jetbrains.annotations.Nullable()
    android.net.Uri imageUri) {
        return null;
    }
    
    public final void rotateImageIfNecessary(@org.jetbrains.annotations.NotNull()
    java.lang.String photoPath) {
    }
    
    private final android.graphics.Bitmap rotateImage(android.graphics.Bitmap source, float angle) {
        return null;
    }
    
    private ImageUtils() {
        super();
    }
}