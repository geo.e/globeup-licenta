package com.impaktsoft.globeup.viewmodels;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010 \n\u0002\b\b\n\u0002\u0010\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J!\u00108\u001a\u0002092\u0006\u0010:\u001a\u00020\u00132\u0006\u0010;\u001a\u00020\u0013H\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010<J\u0006\u0010=\u001a\u000209J!\u0010>\u001a\u0002092\u0006\u0010:\u001a\u00020\u00132\u0006\u0010;\u001a\u00020\u0013H\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010<J\b\u0010?\u001a\u000209H\u0002R \u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR \u0010\n\u001a\b\u0012\u0004\u0012\u00020\u000b0\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\u0007\"\u0004\b\r\u0010\tR \u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0007\"\u0004\b\u0011\u0010\tR \u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00130\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0007\"\u0004\b\u0015\u0010\tR \u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00170\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0007\"\u0004\b\u0019\u0010\tR \u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00130\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001b\u0010\u0007\"\u0004\b\u001c\u0010\tR\u0016\u0010\u001d\u001a\n \u001e*\u0004\u0018\u00010\u000f0\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R \u0010\u001f\u001a\b\u0012\u0004\u0012\u00020 0\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b!\u0010\u0007\"\u0004\b\"\u0010\tR.\u0010#\u001a\u0016\u0012\u0004\u0012\u00020\u000b\u0018\u00010$j\n\u0012\u0004\u0012\u00020\u000b\u0018\u0001`%X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b&\u0010\'\"\u0004\b(\u0010)R \u0010*\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b*\u0010\u0007\"\u0004\b+\u0010\tR \u0010,\u001a\b\u0012\u0004\u0012\u00020\u000b0\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b-\u0010\u0007\"\u0004\b.\u0010\tR \u0010/\u001a\b\u0012\u0004\u0012\u00020 00X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b1\u00102\"\u0004\b3\u00104R \u00105\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b6\u0010\u0007\"\u0004\b7\u0010\t\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006@"}, d2 = {"Lcom/impaktsoft/globeup/viewmodels/AddEventViewModel;", "Lcom/impaktsoft/globeup/viewmodels/BaseViewModel;", "()V", "createEventOnFacebook", "Landroidx/lifecycle/MutableLiveData;", "", "getCreateEventOnFacebook", "()Landroidx/lifecycle/MutableLiveData;", "setCreateEventOnFacebook", "(Landroidx/lifecycle/MutableLiveData;)V", "currentImageIndex", "", "getCurrentImageIndex", "setCurrentImageIndex", "datePickerCalendar", "Ljava/util/Calendar;", "getDatePickerCalendar", "setDatePickerCalendar", "eventDescription", "", "getEventDescription", "setEventDescription", "eventLocation", "Lcom/google/android/gms/maps/model/LatLng;", "getEventLocation", "setEventLocation", "eventName", "getEventName", "setEventName", "eventTimeInUTC", "kotlin.jvm.PlatformType", "eventType", "Lcom/impaktsoft/globeup/enums/EventType;", "getEventType", "setEventType", "images", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "getImages", "()Ljava/util/ArrayList;", "setImages", "(Ljava/util/ArrayList;)V", "isAddProcessRunning", "setAddProcessRunning", "progress", "getProgress", "setProgress", "spinnerEntries", "", "getSpinnerEntries", "()Ljava/util/List;", "setSpinnerEntries", "(Ljava/util/List;)V", "timePickerCalendar", "getTimePickerCalendar", "setTimePickerCalendar", "addEventInDB", "", "eventKey", "userKey", "(Ljava/lang/String;Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "createClick", "initEventConversation", "setupEventDate", "app_debug"})
public final class AddEventViewModel extends com.impaktsoft.globeup.viewmodels.BaseViewModel {
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Integer> currentImageIndex;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> eventName;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> eventDescription;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<com.impaktsoft.globeup.enums.EventType> eventType;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<com.google.android.gms.maps.model.LatLng> eventLocation;
    @org.jetbrains.annotations.Nullable()
    private java.util.ArrayList<java.lang.Integer> images;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.Calendar> datePickerCalendar;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.Calendar> timePickerCalendar;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isAddProcessRunning;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> createEventOnFacebook;
    private java.util.Calendar eventTimeInUTC;
    @org.jetbrains.annotations.NotNull()
    private java.util.List<? extends com.impaktsoft.globeup.enums.EventType> spinnerEntries;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Integer> progress;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Integer> getCurrentImageIndex() {
        return null;
    }
    
    public final void setCurrentImageIndex(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Integer> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getEventName() {
        return null;
    }
    
    public final void setEventName(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getEventDescription() {
        return null;
    }
    
    public final void setEventDescription(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.impaktsoft.globeup.enums.EventType> getEventType() {
        return null;
    }
    
    public final void setEventType(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<com.impaktsoft.globeup.enums.EventType> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.google.android.gms.maps.model.LatLng> getEventLocation() {
        return null;
    }
    
    public final void setEventLocation(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<com.google.android.gms.maps.model.LatLng> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.ArrayList<java.lang.Integer> getImages() {
        return null;
    }
    
    public final void setImages(@org.jetbrains.annotations.Nullable()
    java.util.ArrayList<java.lang.Integer> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.Calendar> getDatePickerCalendar() {
        return null;
    }
    
    public final void setDatePickerCalendar(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.Calendar> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.Calendar> getTimePickerCalendar() {
        return null;
    }
    
    public final void setTimePickerCalendar(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.Calendar> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isAddProcessRunning() {
        return null;
    }
    
    public final void setAddProcessRunning(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getCreateEventOnFacebook() {
        return null;
    }
    
    public final void setCreateEventOnFacebook(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.impaktsoft.globeup.enums.EventType> getSpinnerEntries() {
        return null;
    }
    
    public final void setSpinnerEntries(@org.jetbrains.annotations.NotNull()
    java.util.List<? extends com.impaktsoft.globeup.enums.EventType> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Integer> getProgress() {
        return null;
    }
    
    public final void setProgress(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Integer> p0) {
    }
    
    public final void createClick() {
    }
    
    private final void setupEventDate() {
    }
    
    public AddEventViewModel() {
        super();
    }
}