package com.impaktsoft.globeup;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0010\u001a\u00020\u0011H\u0016R\u001b\u0010\u0003\u001a\u00020\u00048BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\u0005\u0010\u0006R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u000b\u001a\u00020\f8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u000f\u0010\b\u001a\u0004\b\r\u0010\u000e\u00a8\u0006\u0012"}, d2 = {"Lcom/impaktsoft/globeup/App;", "Landroid/app/Application;", "()V", "activityService", "Lcom/impaktsoft/globeup/services/ICurrentActivityService;", "getActivityService", "()Lcom/impaktsoft/globeup/services/ICurrentActivityService;", "activityService$delegate", "Lkotlin/Lazy;", "appModule", "Lorg/koin/core/module/Module;", "resourceService", "Lcom/impaktsoft/globeup/services/IResourceService;", "getResourceService", "()Lcom/impaktsoft/globeup/services/IResourceService;", "resourceService$delegate", "onCreate", "", "app_debug"})
public final class App extends android.app.Application {
    private final org.koin.core.module.Module appModule = null;
    private final kotlin.Lazy activityService$delegate = null;
    private final kotlin.Lazy resourceService$delegate = null;
    
    private final com.impaktsoft.globeup.services.ICurrentActivityService getActivityService() {
        return null;
    }
    
    private final com.impaktsoft.globeup.services.IResourceService getResourceService() {
        return null;
    }
    
    @java.lang.Override()
    public void onCreate() {
    }
    
    public App() {
        super();
    }
}