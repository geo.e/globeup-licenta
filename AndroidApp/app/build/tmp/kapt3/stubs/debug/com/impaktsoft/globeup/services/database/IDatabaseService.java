package com.impaktsoft.globeup.services.database;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u001e\u001a\u00020\u001fH&R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\b\u0010\tR\u0012\u0010\n\u001a\u00020\u000bX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\f\u0010\rR\u0012\u0010\u000e\u001a\u00020\u000fX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0010\u0010\u0011R\u0012\u0010\u0012\u001a\u00020\u0013X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0014\u0010\u0015R\u0012\u0010\u0016\u001a\u00020\u0017X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0018\u0010\u0019R\u0012\u0010\u001a\u001a\u00020\u001bX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u001c\u0010\u001d\u00a8\u0006 "}, d2 = {"Lcom/impaktsoft/globeup/services/database/IDatabaseService;", "", "chatDAO", "Lcom/impaktsoft/globeup/services/database/IChatDAO;", "getChatDAO", "()Lcom/impaktsoft/globeup/services/database/IChatDAO;", "eventDao", "Lcom/impaktsoft/globeup/services/database/IEventsDAO;", "getEventDao", "()Lcom/impaktsoft/globeup/services/database/IEventsDAO;", "feedbackService", "Lcom/impaktsoft/globeup/services/database/IFeedbackService;", "getFeedbackService", "()Lcom/impaktsoft/globeup/services/database/IFeedbackService;", "fireAuth", "Lcom/google/firebase/auth/FirebaseAuth;", "getFireAuth", "()Lcom/google/firebase/auth/FirebaseAuth;", "firebaseFirebaseDynamicLinksService", "Lcom/impaktsoft/globeup/services/database/IFirebaseDynamicLinksService;", "getFirebaseFirebaseDynamicLinksService", "()Lcom/impaktsoft/globeup/services/database/IFirebaseDynamicLinksService;", "messagingService", "Lcom/impaktsoft/globeup/services/database/IMessagingService;", "getMessagingService", "()Lcom/impaktsoft/globeup/services/database/IMessagingService;", "userInfoDao", "Lcom/impaktsoft/globeup/services/database/IUserInfoDAO;", "getUserInfoDao", "()Lcom/impaktsoft/globeup/services/database/IUserInfoDAO;", "getFirestore", "Lcom/google/firebase/firestore/FirebaseFirestore;", "app_debug"})
public abstract interface IDatabaseService {
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.impaktsoft.globeup.services.database.IUserInfoDAO getUserInfoDao();
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.impaktsoft.globeup.services.database.IEventsDAO getEventDao();
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.impaktsoft.globeup.services.database.IChatDAO getChatDAO();
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.google.firebase.auth.FirebaseAuth getFireAuth();
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.impaktsoft.globeup.services.database.IMessagingService getMessagingService();
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.impaktsoft.globeup.services.database.IFirebaseDynamicLinksService getFirebaseFirebaseDynamicLinksService();
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.impaktsoft.globeup.services.database.IFeedbackService getFeedbackService();
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.google.firebase.firestore.FirebaseFirestore getFirestore();
}