package com.impaktsoft.globeup.services;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0017J\b\u0010\u0005\u001a\u00020\u0006H\u0016J\b\u0010\u000f\u001a\u00020\fH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"}, d2 = {"Lcom/impaktsoft/globeup/services/LocationService;", "Lcom/impaktsoft/globeup/services/ILocationService;", "activityService", "Lcom/impaktsoft/globeup/services/ICurrentActivityService;", "(Lcom/impaktsoft/globeup/services/ICurrentActivityService;)V", "isRunning", "", "locationManager", "Landroid/location/LocationManager;", "mLocationListener", "Landroid/location/LocationListener;", "getLocation", "", "locationListener", "Lcom/impaktsoft/globeup/listeners/IGetLocationListener;", "removeUpdates", "app_debug"})
public final class LocationService implements com.impaktsoft.globeup.services.ILocationService {
    private android.location.LocationManager locationManager;
    private android.location.LocationListener mLocationListener;
    private boolean isRunning;
    private final com.impaktsoft.globeup.services.ICurrentActivityService activityService = null;
    
    @android.annotation.SuppressLint(value = {"MissingPermission"})
    @java.lang.Override()
    public void getLocation(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.listeners.IGetLocationListener locationListener) {
    }
    
    @java.lang.Override()
    public void removeUpdates() {
    }
    
    @java.lang.Override()
    public boolean isRunning() {
        return false;
    }
    
    public LocationService(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.services.ICurrentActivityService activityService) {
        super();
    }
}