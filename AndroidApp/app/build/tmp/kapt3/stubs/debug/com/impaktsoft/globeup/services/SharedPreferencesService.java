package com.impaktsoft.globeup.services;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0012\u0010\u0011\u001a\u0004\u0018\u00010\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J)\u0010\u0015\u001a\u0004\u0018\u0001H\u0016\"\u0004\b\u0000\u0010\u00162\u0006\u0010\u0017\u001a\u00020\u00122\n\u0010\u0018\u001a\u0006\u0012\u0002\b\u00030\u0019H\u0016\u00a2\u0006\u0002\u0010\u001aJ\u0018\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u0017\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0016R\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010\u00a8\u0006\u001d"}, d2 = {"Lcom/impaktsoft/globeup/services/SharedPreferencesService;", "Lcom/impaktsoft/globeup/services/ISharedPreferencesService;", "currentActivityService", "Lcom/impaktsoft/globeup/services/ICurrentActivityService;", "(Lcom/impaktsoft/globeup/services/ICurrentActivityService;)V", "activity", "Landroid/app/Activity;", "getActivity", "()Landroid/app/Activity;", "myGson", "Lcom/google/gson/Gson;", "sharedPref", "Landroid/content/SharedPreferences;", "getSharedPref", "()Landroid/content/SharedPreferences;", "setSharedPref", "(Landroid/content/SharedPreferences;)V", "getJsonOfT", "", "myObj", "", "readFromSharedPref", "T", "key", "objClass", "Ljava/lang/Class;", "(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;", "writeInSharedPref", "", "app_debug"})
public final class SharedPreferencesService implements com.impaktsoft.globeup.services.ISharedPreferencesService {
    @org.jetbrains.annotations.Nullable()
    private android.content.SharedPreferences sharedPref;
    @org.jetbrains.annotations.Nullable()
    private final android.app.Activity activity = null;
    private final com.google.gson.Gson myGson = null;
    private final com.impaktsoft.globeup.services.ICurrentActivityService currentActivityService = null;
    
    @org.jetbrains.annotations.Nullable()
    public final android.content.SharedPreferences getSharedPref() {
        return null;
    }
    
    public final void setSharedPref(@org.jetbrains.annotations.Nullable()
    android.content.SharedPreferences p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.app.Activity getActivity() {
        return null;
    }
    
    @java.lang.Override()
    public void writeInSharedPref(@org.jetbrains.annotations.NotNull()
    java.lang.String key, @org.jetbrains.annotations.NotNull()
    java.lang.Object myObj) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public <T extends java.lang.Object>T readFromSharedPref(@org.jetbrains.annotations.NotNull()
    java.lang.String key, @org.jetbrains.annotations.NotNull()
    java.lang.Class<?> objClass) {
        return null;
    }
    
    private final java.lang.String getJsonOfT(java.lang.Object myObj) {
        return null;
    }
    
    public SharedPreferencesService(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.services.ICurrentActivityService currentActivityService) {
        super();
    }
}