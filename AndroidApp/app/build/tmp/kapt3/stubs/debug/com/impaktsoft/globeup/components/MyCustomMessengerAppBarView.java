package com.impaktsoft.globeup.components;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fJ\u000e\u0010\u0010\u001a\u00020\r2\u0006\u0010\u0011\u001a\u00020\u0012J\u000e\u0010\u0013\u001a\u00020\r2\u0006\u0010\u0014\u001a\u00020\u0015J\u000e\u0010\u0013\u001a\u00020\r2\u0006\u0010\u0016\u001a\u00020\u0012J\u000e\u0010\u0017\u001a\u00020\r2\u0006\u0010\u0018\u001a\u00020\u0012R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"}, d2 = {"Lcom/impaktsoft/globeup/components/MyCustomMessengerAppBarView;", "Landroid/widget/FrameLayout;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "backImageActionBar", "Landroid/widget/ImageView;", "nameTextView", "Landroid/widget/TextView;", "profileImage", "statusImage", "statusTextView", "setBackClick", "", "clickMethod", "Lcom/impaktsoft/globeup/listeners/IClickListener;", "setName", "name", "", "setProfileImage", "image", "", "imageUrl", "setStatus", "status", "app_debug"})
public final class MyCustomMessengerAppBarView extends android.widget.FrameLayout {
    private android.widget.TextView nameTextView;
    private android.widget.TextView statusTextView;
    private android.widget.ImageView backImageActionBar;
    private android.widget.ImageView profileImage;
    private android.widget.ImageView statusImage;
    private java.util.HashMap _$_findViewCache;
    
    public final void setName(@org.jetbrains.annotations.NotNull()
    java.lang.String name) {
    }
    
    public final void setStatus(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    public final void setProfileImage(@org.jetbrains.annotations.NotNull()
    java.lang.String imageUrl) {
    }
    
    public final void setProfileImage(int image) {
    }
    
    public final void setBackClick(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.listeners.IClickListener clickMethod) {
    }
    
    public MyCustomMessengerAppBarView(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super(null);
    }
}