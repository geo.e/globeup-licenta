package com.impaktsoft.globeup.views.activities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b&\u0018\u0000*\n\b\u0000\u0010\u0001 \u0001*\u00020\u0002*\n\b\u0001\u0010\u0003 \u0000*\u00020\u00042\b\u0012\u0004\u0012\u0002H\u00010\u0005B\u0013\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00028\u00000\u0007\u00a2\u0006\u0002\u0010\bJ\u0012\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0014J\u0015\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u0012\u001a\u00028\u0001H$\u00a2\u0006\u0002\u0010\u0013R\u0012\u0010\t\u001a\u00020\nX\u00a4\u0004\u00a2\u0006\u0006\u001a\u0004\b\u000b\u0010\f\u00a8\u0006\u0014"}, d2 = {"Lcom/impaktsoft/globeup/views/activities/BaseBoundActivity;", "T", "Lcom/impaktsoft/globeup/viewmodels/BaseViewModel;", "K", "Landroidx/databinding/ViewDataBinding;", "Lcom/impaktsoft/globeup/views/activities/BaseActivity;", "vmClass", "Lkotlin/reflect/KClass;", "(Lkotlin/reflect/KClass;)V", "layoutId", "", "getLayoutId", "()I", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "setupDataBinding", "binding", "(Landroidx/databinding/ViewDataBinding;)V", "app_debug"})
public abstract class BaseBoundActivity<T extends com.impaktsoft.globeup.viewmodels.BaseViewModel, K extends androidx.databinding.ViewDataBinding> extends com.impaktsoft.globeup.views.activities.BaseActivity<T> {
    private java.util.HashMap _$_findViewCache;
    
    protected abstract int getLayoutId();
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    protected abstract void setupDataBinding(@org.jetbrains.annotations.NotNull()
    K binding);
    
    public BaseBoundActivity(@org.jetbrains.annotations.NotNull()
    kotlin.reflect.KClass<T> vmClass) {
        super(null);
    }
}