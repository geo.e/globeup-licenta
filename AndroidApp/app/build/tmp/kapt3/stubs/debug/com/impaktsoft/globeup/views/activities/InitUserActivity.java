package com.impaktsoft.globeup.views.activities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0005\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0003H\u0014R\u0014\u0010\u0005\u001a\u00020\u00068TX\u0094\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0007\u0010\b\u00a8\u0006\f"}, d2 = {"Lcom/impaktsoft/globeup/views/activities/InitUserActivity;", "Lcom/impaktsoft/globeup/views/activities/BaseBoundActivity;", "Lcom/impaktsoft/globeup/viewmodels/InitUserViewModel;", "Lcom/impaktsoft/globeup/databinding/ActivityInitUserBinding;", "()V", "layoutId", "", "getLayoutId", "()I", "setupDataBinding", "", "binding", "app_debug"})
public final class InitUserActivity extends com.impaktsoft.globeup.views.activities.BaseBoundActivity<com.impaktsoft.globeup.viewmodels.InitUserViewModel, com.impaktsoft.globeup.databinding.ActivityInitUserBinding> {
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected int getLayoutId() {
        return 0;
    }
    
    @java.lang.Override()
    protected void setupDataBinding(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.databinding.ActivityInitUserBinding binding) {
    }
    
    public InitUserActivity() {
        super(null);
    }
}