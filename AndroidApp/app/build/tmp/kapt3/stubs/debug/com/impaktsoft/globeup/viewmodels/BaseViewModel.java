package com.impaktsoft.globeup.viewmodels;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u00fa\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0015\n\u0002\b\u0006\b&\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J\u0011\u0010z\u001a\u00020{H\u0084@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010|J\b\u0010}\u001a\u00020DH\u0004J%\u0010~\u001a\u00020{2\u0007\u0010\u007f\u001a\u00030\u0080\u00012\b\u0010\u0081\u0001\u001a\u00030\u0080\u00012\n\u0010\u0082\u0001\u001a\u0005\u0018\u00010\u0083\u0001J\u0007\u0010\u0084\u0001\u001a\u00020{J\t\u0010\u0085\u0001\u001a\u00020{H\u0017J\t\u0010\u0086\u0001\u001a\u00020{H\u0017J\t\u0010\u0087\u0001\u001a\u00020{H\u0017J3\u0010\u0088\u0001\u001a\u00020{2\u0007\u0010\u007f\u001a\u00030\u0080\u00012\u0011\u0010\u0089\u0001\u001a\f\u0012\u0007\b\u0001\u0012\u00030\u008b\u00010\u008a\u00012\b\u0010\u008c\u0001\u001a\u00030\u008d\u0001\u00a2\u0006\u0003\u0010\u008e\u0001J\t\u0010\u008f\u0001\u001a\u00020{H\u0017J\t\u0010\u0090\u0001\u001a\u00020{H\u0017J\t\u0010\u0091\u0001\u001a\u00020{H\u0017J\u0012\u0010\u0092\u0001\u001a\u00020{H\u0084@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010|R\u001b\u0010\u0005\u001a\u00020\u00068DX\u0084\u0084\u0002\u00a2\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\bR\u001b\u0010\u000b\u001a\u00020\f8DX\u0084\u0084\u0002\u00a2\u0006\f\n\u0004\b\u000f\u0010\n\u001a\u0004\b\r\u0010\u000eR\u001b\u0010\u0010\u001a\u00020\u00118DX\u0084\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0014\u0010\n\u001a\u0004\b\u0012\u0010\u0013R\u001b\u0010\u0015\u001a\u00020\u00168DX\u0084\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0019\u0010\n\u001a\u0004\b\u0017\u0010\u0018R\u001b\u0010\u001a\u001a\u00020\u001b8DX\u0084\u0084\u0002\u00a2\u0006\f\n\u0004\b\u001e\u0010\n\u001a\u0004\b\u001c\u0010\u001dR\u001b\u0010\u001f\u001a\u00020 8DX\u0084\u0084\u0002\u00a2\u0006\f\n\u0004\b#\u0010\n\u001a\u0004\b!\u0010\"R\u001b\u0010$\u001a\u00020%8DX\u0084\u0084\u0002\u00a2\u0006\f\n\u0004\b(\u0010\n\u001a\u0004\b&\u0010\'R\u001b\u0010)\u001a\u00020*8DX\u0084\u0084\u0002\u00a2\u0006\f\n\u0004\b-\u0010\n\u001a\u0004\b+\u0010,R\u001b\u0010.\u001a\u00020/8DX\u0084\u0084\u0002\u00a2\u0006\f\n\u0004\b2\u0010\n\u001a\u0004\b0\u00101R\u001b\u00103\u001a\u0002048DX\u0084\u0084\u0002\u00a2\u0006\f\n\u0004\b7\u0010\n\u001a\u0004\b5\u00106R\u001b\u00108\u001a\u0002098DX\u0084\u0084\u0002\u00a2\u0006\f\n\u0004\b<\u0010\n\u001a\u0004\b:\u0010;R\u001b\u0010=\u001a\u00020>8DX\u0084\u0084\u0002\u00a2\u0006\f\n\u0004\bA\u0010\n\u001a\u0004\b?\u0010@R \u0010B\u001a\b\u0012\u0004\u0012\u00020D0CX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bB\u0010E\"\u0004\bF\u0010GR\u001b\u0010H\u001a\u00020I8DX\u0084\u0084\u0002\u00a2\u0006\f\n\u0004\bL\u0010\n\u001a\u0004\bJ\u0010KR\u001b\u0010M\u001a\u00020N8DX\u0084\u0084\u0002\u00a2\u0006\f\n\u0004\bQ\u0010\n\u001a\u0004\bO\u0010PR\u001b\u0010R\u001a\u00020S8DX\u0084\u0084\u0002\u00a2\u0006\f\n\u0004\bV\u0010\n\u001a\u0004\bT\u0010UR\u001b\u0010W\u001a\u00020X8DX\u0084\u0084\u0002\u00a2\u0006\f\n\u0004\b[\u0010\n\u001a\u0004\bY\u0010ZR\u001b\u0010\\\u001a\u00020]8DX\u0084\u0084\u0002\u00a2\u0006\f\n\u0004\b`\u0010\n\u001a\u0004\b^\u0010_R\u001b\u0010a\u001a\u00020b8DX\u0084\u0084\u0002\u00a2\u0006\f\n\u0004\be\u0010\n\u001a\u0004\bc\u0010dR\u001b\u0010f\u001a\u00020g8DX\u0084\u0084\u0002\u00a2\u0006\f\n\u0004\bj\u0010\n\u001a\u0004\bh\u0010iR\u001b\u0010k\u001a\u00020l8DX\u0084\u0084\u0002\u00a2\u0006\f\n\u0004\bo\u0010\n\u001a\u0004\bm\u0010nR\u001b\u0010p\u001a\u00020q8DX\u0084\u0084\u0002\u00a2\u0006\f\n\u0004\bt\u0010\n\u001a\u0004\br\u0010sR\u001b\u0010u\u001a\u00020v8DX\u0084\u0084\u0002\u00a2\u0006\f\n\u0004\by\u0010\n\u001a\u0004\bw\u0010x\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0093\u0001"}, d2 = {"Lcom/impaktsoft/globeup/viewmodels/BaseViewModel;", "Landroidx/lifecycle/ViewModel;", "Lorg/koin/core/KoinComponent;", "Landroidx/lifecycle/LifecycleObserver;", "()V", "activityResultService", "Lcom/impaktsoft/globeup/services/IActivityResultService;", "getActivityResultService", "()Lcom/impaktsoft/globeup/services/IActivityResultService;", "activityResultService$delegate", "Lkotlin/Lazy;", "activityService", "Lcom/impaktsoft/globeup/services/ICurrentActivityService;", "getActivityService", "()Lcom/impaktsoft/globeup/services/ICurrentActivityService;", "activityService$delegate", "alertBuilderService", "Lcom/impaktsoft/globeup/services/IAlertBuilderService;", "getAlertBuilderService", "()Lcom/impaktsoft/globeup/services/IAlertBuilderService;", "alertBuilderService$delegate", "backPressService", "Lcom/impaktsoft/globeup/services/IOnBackPressService;", "getBackPressService", "()Lcom/impaktsoft/globeup/services/IOnBackPressService;", "backPressService$delegate", "backend", "Lcom/impaktsoft/globeup/services/IBackendService;", "getBackend", "()Lcom/impaktsoft/globeup/services/IBackendService;", "backend$delegate", "connectivityService", "Lcom/impaktsoft/globeup/services/IConnectivityService;", "getConnectivityService", "()Lcom/impaktsoft/globeup/services/IConnectivityService;", "connectivityService$delegate", "dataExchangeService", "Lcom/impaktsoft/globeup/services/IDataExchangeService;", "getDataExchangeService", "()Lcom/impaktsoft/globeup/services/IDataExchangeService;", "dataExchangeService$delegate", "database", "Lcom/impaktsoft/globeup/services/database/IDatabaseService;", "getDatabase", "()Lcom/impaktsoft/globeup/services/database/IDatabaseService;", "database$delegate", "dialogService", "Lcom/impaktsoft/globeup/services/IDialogService;", "getDialogService", "()Lcom/impaktsoft/globeup/services/IDialogService;", "dialogService$delegate", "facebookService", "Lcom/impaktsoft/globeup/services/IFacebookService;", "getFacebookService", "()Lcom/impaktsoft/globeup/services/IFacebookService;", "facebookService$delegate", "fileStoreService", "Lcom/impaktsoft/globeup/services/IFileStoreService;", "getFileStoreService", "()Lcom/impaktsoft/globeup/services/IFileStoreService;", "fileStoreService$delegate", "firebaseInvitationService", "Lcom/impaktsoft/globeup/services/IFirebaseInvitationService;", "getFirebaseInvitationService", "()Lcom/impaktsoft/globeup/services/IFirebaseInvitationService;", "firebaseInvitationService$delegate", "isHudVisible", "Landroidx/lifecycle/MutableLiveData;", "", "()Landroidx/lifecycle/MutableLiveData;", "setHudVisible", "(Landroidx/lifecycle/MutableLiveData;)V", "locationService", "Lcom/impaktsoft/globeup/services/ILocationService;", "getLocationService", "()Lcom/impaktsoft/globeup/services/ILocationService;", "locationService$delegate", "mapService", "Lcom/impaktsoft/globeup/services/IMapService;", "getMapService", "()Lcom/impaktsoft/globeup/services/IMapService;", "mapService$delegate", "navigation", "Lcom/impaktsoft/globeup/services/INavigationService;", "getNavigation", "()Lcom/impaktsoft/globeup/services/INavigationService;", "navigation$delegate", "pendingService", "Lcom/impaktsoft/globeup/services/IPendingMessagesService;", "getPendingService", "()Lcom/impaktsoft/globeup/services/IPendingMessagesService;", "pendingService$delegate", "permissionService", "Lcom/impaktsoft/globeup/services/IPermissionService;", "getPermissionService", "()Lcom/impaktsoft/globeup/services/IPermissionService;", "permissionService$delegate", "popUpMenuService", "Lcom/impaktsoft/globeup/services/IPopUpMenuService;", "getPopUpMenuService", "()Lcom/impaktsoft/globeup/services/IPopUpMenuService;", "popUpMenuService$delegate", "rateInPlayStoreService", "Lcom/impaktsoft/globeup/services/IRateInPlayStore;", "getRateInPlayStoreService", "()Lcom/impaktsoft/globeup/services/IRateInPlayStore;", "rateInPlayStoreService$delegate", "resourceService", "Lcom/impaktsoft/globeup/services/IResourceService;", "getResourceService", "()Lcom/impaktsoft/globeup/services/IResourceService;", "resourceService$delegate", "sharedPreferencesService", "Lcom/impaktsoft/globeup/services/ISharedPreferencesService;", "getSharedPreferencesService", "()Lcom/impaktsoft/globeup/services/ISharedPreferencesService;", "sharedPreferencesService$delegate", "showGoogleMapService", "Lcom/impaktsoft/globeup/services/IShowGoogleMapService;", "getShowGoogleMapService", "()Lcom/impaktsoft/globeup/services/IShowGoogleMapService;", "showGoogleMapService$delegate", "hideHud", "", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "isCurrentUserAnonymous", "onActivityForResult", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "onBackPressed", "onCreate", "onDestroy", "onPause", "onRequestPermissionsResult", "permissions", "", "", "grantResults", "", "(I[Ljava/lang/String;[I)V", "onResume", "onStart", "onStop", "showHud", "app_debug"})
public abstract class BaseViewModel extends androidx.lifecycle.ViewModel implements org.koin.core.KoinComponent, androidx.lifecycle.LifecycleObserver {
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy navigation$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy backend$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy database$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy dialogService$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy permissionService$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy fileStoreService$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy dataExchangeService$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy resourceService$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy mapService$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy sharedPreferencesService$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy popUpMenuService$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy locationService$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy pendingService$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy showGoogleMapService$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy alertBuilderService$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy activityService$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy activityResultService$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy connectivityService$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy backPressService$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy firebaseInvitationService$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy rateInPlayStoreService$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy facebookService$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isHudVisible;
    
    @org.jetbrains.annotations.NotNull()
    protected final com.impaktsoft.globeup.services.INavigationService getNavigation() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final com.impaktsoft.globeup.services.IBackendService getBackend() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final com.impaktsoft.globeup.services.database.IDatabaseService getDatabase() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final com.impaktsoft.globeup.services.IDialogService getDialogService() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final com.impaktsoft.globeup.services.IPermissionService getPermissionService() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final com.impaktsoft.globeup.services.IFileStoreService getFileStoreService() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final com.impaktsoft.globeup.services.IDataExchangeService getDataExchangeService() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final com.impaktsoft.globeup.services.IResourceService getResourceService() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final com.impaktsoft.globeup.services.IMapService getMapService() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final com.impaktsoft.globeup.services.ISharedPreferencesService getSharedPreferencesService() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final com.impaktsoft.globeup.services.IPopUpMenuService getPopUpMenuService() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final com.impaktsoft.globeup.services.ILocationService getLocationService() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final com.impaktsoft.globeup.services.IPendingMessagesService getPendingService() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final com.impaktsoft.globeup.services.IShowGoogleMapService getShowGoogleMapService() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final com.impaktsoft.globeup.services.IAlertBuilderService getAlertBuilderService() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final com.impaktsoft.globeup.services.ICurrentActivityService getActivityService() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final com.impaktsoft.globeup.services.IActivityResultService getActivityResultService() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final com.impaktsoft.globeup.services.IConnectivityService getConnectivityService() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final com.impaktsoft.globeup.services.IOnBackPressService getBackPressService() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final com.impaktsoft.globeup.services.IFirebaseInvitationService getFirebaseInvitationService() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final com.impaktsoft.globeup.services.IRateInPlayStore getRateInPlayStoreService() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final com.impaktsoft.globeup.services.IFacebookService getFacebookService() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isHudVisible() {
        return null;
    }
    
    public final void setHudVisible(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    protected final java.lang.Object showHud(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p0) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    protected final java.lang.Object hideHud(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p0) {
        return null;
    }
    
    protected final boolean isCurrentUserAnonymous() {
        return false;
    }
    
    @androidx.lifecycle.OnLifecycleEvent(value = androidx.lifecycle.Lifecycle.Event.ON_CREATE)
    public void onCreate() {
    }
    
    @androidx.lifecycle.OnLifecycleEvent(value = androidx.lifecycle.Lifecycle.Event.ON_RESUME)
    public void onResume() {
    }
    
    @androidx.lifecycle.OnLifecycleEvent(value = androidx.lifecycle.Lifecycle.Event.ON_START)
    public void onStart() {
    }
    
    @androidx.lifecycle.OnLifecycleEvent(value = androidx.lifecycle.Lifecycle.Event.ON_PAUSE)
    public void onPause() {
    }
    
    @androidx.lifecycle.OnLifecycleEvent(value = androidx.lifecycle.Lifecycle.Event.ON_STOP)
    public void onStop() {
    }
    
    @androidx.lifecycle.OnLifecycleEvent(value = androidx.lifecycle.Lifecycle.Event.ON_DESTROY)
    public void onDestroy() {
    }
    
    public final void onRequestPermissionsResult(int requestCode, @org.jetbrains.annotations.NotNull()
    java.lang.String[] permissions, @org.jetbrains.annotations.NotNull()
    int[] grantResults) {
    }
    
    public final void onActivityForResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    public final void onBackPressed() {
    }
    
    public BaseViewModel() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public org.koin.core.Koin getKoin() {
        return null;
    }
}