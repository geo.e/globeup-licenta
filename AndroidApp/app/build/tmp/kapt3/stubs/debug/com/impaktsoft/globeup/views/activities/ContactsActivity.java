package com.impaktsoft.globeup.views.activities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\b\u0010\u000e\u001a\u00020\u000fH\u0016J\u0012\u0010\u0010\u001a\u00020\u000f2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0014J\u0006\u0010\u0013\u001a\u00020\u000fR\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0006\u001a\u00020\u0007X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR\u0010\u0010\f\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"}, d2 = {"Lcom/impaktsoft/globeup/views/activities/ContactsActivity;", "Lcom/impaktsoft/globeup/views/activities/BaseActivity;", "Lcom/impaktsoft/globeup/viewmodels/ContactsViewModel;", "()V", "customSearchAppBar", "Lcom/impaktsoft/globeup/components/MyCustomSearchAppBarView;", "getStringListener", "Lcom/impaktsoft/globeup/listeners/IGetStringListener;", "getGetStringListener", "()Lcom/impaktsoft/globeup/listeners/IGetStringListener;", "setGetStringListener", "(Lcom/impaktsoft/globeup/listeners/IGetStringListener;)V", "navHostFragment", "Landroidx/fragment/app/Fragment;", "onBackPressed", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "setCustomSearchActionBar", "app_debug"})
public final class ContactsActivity extends com.impaktsoft.globeup.views.activities.BaseActivity<com.impaktsoft.globeup.viewmodels.ContactsViewModel> {
    private com.impaktsoft.globeup.components.MyCustomSearchAppBarView customSearchAppBar;
    private androidx.fragment.app.Fragment navHostFragment;
    @org.jetbrains.annotations.NotNull()
    public com.impaktsoft.globeup.listeners.IGetStringListener getStringListener;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final com.impaktsoft.globeup.listeners.IGetStringListener getGetStringListener() {
        return null;
    }
    
    public final void setGetStringListener(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.listeners.IGetStringListener p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void setCustomSearchActionBar() {
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    public ContactsActivity() {
        super(null);
    }
}