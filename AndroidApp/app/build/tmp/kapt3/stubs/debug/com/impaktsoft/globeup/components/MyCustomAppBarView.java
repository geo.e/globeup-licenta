package com.impaktsoft.globeup.components;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0007J\b\u0010\r\u001a\u0004\u0018\u00010\tJ\u000e\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0005J\u000e\u0010\u0011\u001a\u00020\u000f2\u0006\u0010\u0012\u001a\u00020\u0013J\u000e\u0010\u0014\u001a\u00020\u000f2\u0006\u0010\u0015\u001a\u00020\u0013R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"}, d2 = {"Lcom/impaktsoft/globeup/components/MyCustomAppBarView;", "Landroid/widget/FrameLayout;", "context", "Landroid/content/Context;", "titleResourceId", "", "subtitleResourceId", "(Landroid/content/Context;Ljava/lang/Integer;Ljava/lang/Integer;)V", "backImageActionBar", "Landroid/widget/ImageView;", "subTitleActionBar", "Landroid/widget/TextView;", "titleActionBar", "getBackImage", "setBackVisibility", "", "visibility", "setSubTitle", "subtitle", "", "setTitle", "title", "app_debug"})
public final class MyCustomAppBarView extends android.widget.FrameLayout {
    private android.widget.ImageView backImageActionBar;
    private android.widget.TextView titleActionBar;
    private android.widget.TextView subTitleActionBar;
    private java.util.HashMap _$_findViewCache;
    
    public final void setTitle(@org.jetbrains.annotations.NotNull()
    java.lang.String title) {
    }
    
    public final void setSubTitle(@org.jetbrains.annotations.NotNull()
    java.lang.String subtitle) {
    }
    
    public final void setBackVisibility(int visibility) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.ImageView getBackImage() {
        return null;
    }
    
    public MyCustomAppBarView(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    java.lang.Integer titleResourceId, @org.jetbrains.annotations.Nullable()
    java.lang.Integer subtitleResourceId) {
        super(null);
    }
}