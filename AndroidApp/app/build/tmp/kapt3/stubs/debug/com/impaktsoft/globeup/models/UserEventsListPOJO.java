package com.impaktsoft.globeup.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u001f\b\u0016\u0012\u0016\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u0005\u00a2\u0006\u0002\u0010\u0006B\u0007\b\u0016\u00a2\u0006\u0002\u0010\u0007R.\u0010\b\u001a\u0016\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003j\n\u0012\u0004\u0012\u00020\u0004\u0018\u0001`\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\u0006\u00a8\u0006\f"}, d2 = {"Lcom/impaktsoft/globeup/models/UserEventsListPOJO;", "", "userEventPOJO", "Ljava/util/ArrayList;", "Lcom/impaktsoft/globeup/models/UserEventPOJO;", "Lkotlin/collections/ArrayList;", "(Ljava/util/ArrayList;)V", "()V", "userEventsList", "getUserEventsList", "()Ljava/util/ArrayList;", "setUserEventsList", "app_debug"})
public final class UserEventsListPOJO {
    @org.jetbrains.annotations.Nullable()
    private java.util.ArrayList<com.impaktsoft.globeup.models.UserEventPOJO> userEventsList;
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.ArrayList<com.impaktsoft.globeup.models.UserEventPOJO> getUserEventsList() {
        return null;
    }
    
    public final void setUserEventsList(@org.jetbrains.annotations.Nullable()
    java.util.ArrayList<com.impaktsoft.globeup.models.UserEventPOJO> p0) {
    }
    
    public UserEventsListPOJO(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.impaktsoft.globeup.models.UserEventPOJO> userEventPOJO) {
        super();
    }
    
    public UserEventsListPOJO() {
        super();
    }
}