package com.impaktsoft.globeup.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0014\u0018\u00002\u00020\u0001B/\b\u0016\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\tR\u001e\u0010\b\u001a\u0004\u0018\u00010\u0007X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u000e\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\rR\u001e\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u000e\u001a\u0004\b\u000f\u0010\u000b\"\u0004\b\u0010\u0010\rR\u001c\u0010\u0011\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u001c\u0010\u0016\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001a\u00a8\u0006\u001b"}, d2 = {"Lcom/impaktsoft/globeup/models/TransactionPOJO;", "", "tranzitionType", "Lcom/impaktsoft/globeup/enums/TransactionType;", "tranzitionDate", "", "gcoins", "", "cash", "(Lcom/impaktsoft/globeup/enums/TransactionType;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V", "getCash", "()Ljava/lang/Integer;", "setCash", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "getGcoins", "setGcoins", "transactionDate", "getTransactionDate", "()Ljava/lang/String;", "setTransactionDate", "(Ljava/lang/String;)V", "transactionType", "getTransactionType", "()Lcom/impaktsoft/globeup/enums/TransactionType;", "setTransactionType", "(Lcom/impaktsoft/globeup/enums/TransactionType;)V", "app_debug"})
public final class TransactionPOJO {
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.enums.TransactionType transactionType;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String transactionDate;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Integer gcoins;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Integer cash;
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.enums.TransactionType getTransactionType() {
        return null;
    }
    
    public final void setTransactionType(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.enums.TransactionType p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTransactionDate() {
        return null;
    }
    
    public final void setTransactionDate(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getGcoins() {
        return null;
    }
    
    public final void setGcoins(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getCash() {
        return null;
    }
    
    public final void setCash(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    public TransactionPOJO(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.enums.TransactionType tranzitionType, @org.jetbrains.annotations.Nullable()
    java.lang.String tranzitionDate, @org.jetbrains.annotations.Nullable()
    java.lang.Integer gcoins, @org.jetbrains.annotations.Nullable()
    java.lang.Integer cash) {
        super();
    }
}