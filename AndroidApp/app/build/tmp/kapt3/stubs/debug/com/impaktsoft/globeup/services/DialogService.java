package com.impaktsoft.globeup.services;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\b\u0010\u0007\u001a\u00020\bH\u0016J(\u0010\t\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\r\u001a\u00020\u000b2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J \u0010\u0010\u001a\u00020\b\"\b\b\u0000\u0010\u0011*\u00020\u00122\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u0002H\u00110\u0014H\u0016J\u0018\u0010\u0015\u001a\u00020\b2\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0017H\u0016J\u0018\u0010\u0015\u001a\u00020\b2\u0006\u0010\u0019\u001a\u00020\u000b2\u0006\u0010\u0018\u001a\u00020\u0017H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"}, d2 = {"Lcom/impaktsoft/globeup/services/DialogService;", "Lcom/impaktsoft/globeup/services/IDialogService;", "navigation", "Lcom/impaktsoft/globeup/services/INavigationService;", "currentActivityService", "Lcom/impaktsoft/globeup/services/ICurrentActivityService;", "(Lcom/impaktsoft/globeup/services/INavigationService;Lcom/impaktsoft/globeup/services/ICurrentActivityService;)V", "hideDialog", "", "showAlertDialog", "title", "", "message", "buttonText", "clickMethod", "Lcom/impaktsoft/globeup/listeners/IClickListener;", "showDialog", "T", "Landroidx/fragment/app/Fragment;", "fragmentClass", "Lkotlin/reflect/KClass;", "showSnackbar", "stringId", "", "duration", "string", "app_debug"})
public final class DialogService implements com.impaktsoft.globeup.services.IDialogService {
    private final com.impaktsoft.globeup.services.INavigationService navigation = null;
    private final com.impaktsoft.globeup.services.ICurrentActivityService currentActivityService = null;
    
    @java.lang.Override()
    public <T extends androidx.fragment.app.Fragment>void showDialog(@org.jetbrains.annotations.NotNull()
    kotlin.reflect.KClass<T> fragmentClass) {
    }
    
    @java.lang.Override()
    public void hideDialog() {
    }
    
    @java.lang.Override()
    public void showSnackbar(int stringId, int duration) {
    }
    
    @java.lang.Override()
    public void showSnackbar(@org.jetbrains.annotations.NotNull()
    java.lang.String string, int duration) {
    }
    
    @java.lang.Override()
    public void showAlertDialog(@org.jetbrains.annotations.NotNull()
    java.lang.String title, @org.jetbrains.annotations.NotNull()
    java.lang.String message, @org.jetbrains.annotations.NotNull()
    java.lang.String buttonText, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.listeners.IClickListener clickMethod) {
    }
    
    public DialogService(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.services.INavigationService navigation, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.services.ICurrentActivityService currentActivityService) {
        super();
    }
}