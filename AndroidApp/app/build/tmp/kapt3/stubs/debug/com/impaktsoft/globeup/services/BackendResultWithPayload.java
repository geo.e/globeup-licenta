package com.impaktsoft.globeup.services;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u0002B\u001b\u0012\b\u0010\u0003\u001a\u0004\u0018\u00018\u0000\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006R\u0015\u0010\u0003\u001a\u0004\u0018\u00018\u0000\u00a2\u0006\n\n\u0002\u0010\t\u001a\u0004\b\u0007\u0010\b\u00a8\u0006\n"}, d2 = {"Lcom/impaktsoft/globeup/services/BackendResultWithPayload;", "T", "Lcom/impaktsoft/globeup/services/BackendResult;", "payload", "error", "Lcom/impaktsoft/globeup/models/GError;", "(Ljava/lang/Object;Lcom/impaktsoft/globeup/models/GError;)V", "getPayload", "()Ljava/lang/Object;", "Ljava/lang/Object;", "app_debug"})
public final class BackendResultWithPayload<T extends java.lang.Object> extends com.impaktsoft.globeup.services.BackendResult {
    @org.jetbrains.annotations.Nullable()
    private final T payload = null;
    
    @org.jetbrains.annotations.Nullable()
    public final T getPayload() {
        return null;
    }
    
    public BackendResultWithPayload(@org.jetbrains.annotations.Nullable()
    T payload, @org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.models.GError error) {
        super(null);
    }
}