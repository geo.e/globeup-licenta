package com.impaktsoft.globeup.views.activities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0005\u00a2\u0006\u0002\u0010\u0004J\u0012\u0010\u0017\u001a\u00020\u00182\b\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0015J\u0012\u0010\u001b\u001a\u00020\u001c2\b\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0016J\u0010\u0010\u001f\u001a\u00020\u001c2\u0006\u0010 \u001a\u00020!H\u0016J\u0010\u0010\"\u001a\u00020\u00182\u0006\u0010#\u001a\u00020\u0003H\u0014R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0094\u000e\u00a2\u0006\u0010\n\u0002\u0010\r\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\u0014\u0010\u000e\u001a\u00020\bX\u0094D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u001c\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016\u00a8\u0006$"}, d2 = {"Lcom/impaktsoft/globeup/views/activities/AddEventActivity;", "Lcom/impaktsoft/globeup/views/activities/BaseBoundActivity;", "Lcom/impaktsoft/globeup/viewmodels/AddEventViewModel;", "Lcom/impaktsoft/globeup/databinding/ActivityAddEventBinding;", "()V", "MAP_VIEW_BUNDLE_KEY", "", "activityTitleResourceId", "", "getActivityTitleResourceId", "()Ljava/lang/Integer;", "setActivityTitleResourceId", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "layoutId", "getLayoutId", "()I", "mapView", "Lcom/impaktsoft/globeup/components/MapViewWithPin;", "getMapView", "()Lcom/impaktsoft/globeup/components/MapViewWithPin;", "setMapView", "(Lcom/impaktsoft/globeup/components/MapViewWithPin;)V", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "onCreateOptionsMenu", "", "menu", "Landroid/view/Menu;", "onOptionsItemSelected", "item", "Landroid/view/MenuItem;", "setupDataBinding", "binding", "app_debug"})
public final class AddEventActivity extends com.impaktsoft.globeup.views.activities.BaseBoundActivity<com.impaktsoft.globeup.viewmodels.AddEventViewModel, com.impaktsoft.globeup.databinding.ActivityAddEventBinding> {
    @org.jetbrains.annotations.Nullable()
    private java.lang.Integer activityTitleResourceId;
    private final int layoutId = com.impaktsoft.globeup.R.layout.activity_add_event;
    private final java.lang.String MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey";
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.components.MapViewWithPin mapView;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    protected java.lang.Integer getActivityTitleResourceId() {
        return null;
    }
    
    @java.lang.Override()
    protected void setActivityTitleResourceId(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @java.lang.Override()
    protected int getLayoutId() {
        return 0;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.components.MapViewWithPin getMapView() {
        return null;
    }
    
    public final void setMapView(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.components.MapViewWithPin p0) {
    }
    
    @java.lang.Override()
    protected void setupDataBinding(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.databinding.ActivityAddEventBinding binding) {
    }
    
    @androidx.annotation.RequiresApi(value = android.os.Build.VERSION_CODES.M)
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public boolean onCreateOptionsMenu(@org.jetbrains.annotations.Nullable()
    android.view.Menu menu) {
        return false;
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    public AddEventActivity() {
        super(null);
    }
}