package com.impaktsoft.globeup.views.activities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0006\u0010\u0004\u001a\u00020\u0005J\u0012\u0010\u0006\u001a\u00020\u00052\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0014\u00a8\u0006\t"}, d2 = {"Lcom/impaktsoft/globeup/views/activities/SettingsActivity;", "Lcom/impaktsoft/globeup/views/activities/BaseActivity;", "Lcom/impaktsoft/globeup/viewmodels/SettingsActivityViewModel;", "()V", "initDataBinding", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "app_debug"})
public final class SettingsActivity extends com.impaktsoft.globeup.views.activities.BaseActivity<com.impaktsoft.globeup.viewmodels.SettingsActivityViewModel> {
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void initDataBinding() {
    }
    
    public SettingsActivity() {
        super(null);
    }
}