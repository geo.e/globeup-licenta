package com.impaktsoft.globeup.services;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\u0018\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\tH&J\b\u0010\n\u001a\u00020\u000bH&J\b\u0010\f\u001a\u00020\u0005H&J\"\u0010\r\u001a\u00020\u00052\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u000f2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012H&\u00a8\u0006\u0013"}, d2 = {"Lcom/impaktsoft/globeup/services/IFacebookService;", "", "getUserId", "", "initLoginManager", "", "onSuccessListener", "Lcom/impaktsoft/globeup/listeners/IOnSuccessListener;", "onFailureListener", "Lcom/impaktsoft/globeup/listeners/IOnFailListener;", "isUserLoggedIn", "", "logoutIfPossible", "onActivityResult", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "app_debug"})
public abstract interface IFacebookService {
    
    public abstract void logoutIfPossible();
    
    public abstract boolean isUserLoggedIn();
    
    public abstract void initLoginManager(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.listeners.IOnSuccessListener onSuccessListener, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.listeners.IOnFailListener onFailureListener);
    
    public abstract void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data);
    
    @org.jetbrains.annotations.NotNull()
    public abstract java.lang.String getUserId();
}