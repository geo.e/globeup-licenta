package com.impaktsoft.globeup.viewmodels;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000~\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010%\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000f\b&\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J!\u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020,2\u0006\u0010-\u001a\u00020.H\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010/J\u0010\u00100\u001a\u00020\u001b2\u0006\u00101\u001a\u00020.H\u0002J\u000e\u00102\u001a\u00020*2\u0006\u00101\u001a\u00020.J-\u00103\u001a\u00020*2\u0018\u00104\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u000207\u0012\u0004\u0012\u00020\u000406052\u0006\u00108\u001a\u00020\u0004\u00a2\u0006\u0002\u00109J\u0010\u0010:\u001a\u0004\u0018\u00010,2\u0006\u00101\u001a\u00020.J\u0014\u0010;\u001a\u0004\u0018\u00010#2\b\u0010<\u001a\u0004\u0018\u00010\u0004H\u0004J\u0006\u0010=\u001a\u00020>J\b\u0010?\u001a\u0004\u0018\u00010@J\u0018\u0010A\u001a\u00020*2\u0006\u0010-\u001a\u00020.2\u0006\u0010+\u001a\u00020,H\u0002J\u000e\u0010B\u001a\u00020*2\u0006\u00101\u001a\u00020.J\u000e\u0010C\u001a\u00020*2\u0006\u00101\u001a\u00020.J\b\u0010D\u001a\u00020\u001bH\u0002J\u0015\u0010E\u001a\u0004\u0018\u00010\u001b2\u0006\u0010F\u001a\u00020\u0004\u00a2\u0006\u0002\u0010GJ\u000e\u0010H\u001a\u00020*2\u0006\u00101\u001a\u00020.J#\u0010I\u001a\u00020*2\u0006\u0010+\u001a\u00020,2\b\u0010J\u001a\u0004\u0018\u00010#H\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010KJ!\u0010L\u001a\u00020*2\u0006\u00101\u001a\u00020.2\u0006\u0010M\u001a\u00020\u001bH\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010NR\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\"\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\r\u0018\u00010\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R \u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00140\u0013X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018R\u001a\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u001b0\u001aX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u001c\u001a\u0004\u0018\u00010\u001dX\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001e\u0010\u001f\"\u0004\b \u0010!R \u0010\"\u001a\b\u0012\u0004\u0012\u00020#0\u0013X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b$\u0010\u0016\"\u0004\b%\u0010\u0018R\u001c\u0010&\u001a\u0004\u0018\u00010\u001dX\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\'\u0010\u001f\"\u0004\b(\u0010!\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006O"}, d2 = {"Lcom/impaktsoft/globeup/viewmodels/BaseEventsViewModel;", "Lcom/impaktsoft/globeup/viewmodels/BaseViewModel;", "()V", "eventConversationKey", "", "eventInteractionJob", "Lkotlinx/coroutines/Job;", "getEventInteractionJob", "()Lkotlinx/coroutines/Job;", "setEventInteractionJob", "(Lkotlinx/coroutines/Job;)V", "eventsAdapter", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "getEventsAdapter", "()Landroidx/recyclerview/widget/RecyclerView$Adapter;", "setEventsAdapter", "(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V", "eventsFromDatabaseList", "Ljava/util/ArrayList;", "Lcom/impaktsoft/globeup/models/IEventsItem;", "getEventsFromDatabaseList", "()Ljava/util/ArrayList;", "setEventsFromDatabaseList", "(Ljava/util/ArrayList;)V", "eventsSupportingModification", "", "", "finishAction", "Lcom/impaktsoft/globeup/listeners/IClickListener;", "getFinishAction", "()Lcom/impaktsoft/globeup/listeners/IClickListener;", "setFinishAction", "(Lcom/impaktsoft/globeup/listeners/IClickListener;)V", "myUserEventList", "Lcom/impaktsoft/globeup/models/UserEventPOJO;", "getMyUserEventList", "setMyUserEventList", "startAction", "getStartAction", "setStartAction", "addNewState", "", "eventState", "Lcom/impaktsoft/globeup/enums/EventState;", "eventPOJO", "Lcom/impaktsoft/globeup/models/EventPOJO;", "(Lcom/impaktsoft/globeup/enums/EventState;Lcom/impaktsoft/globeup/models/EventPOJO;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "checkEventsExistInMyList", "event", "donateForEvent", "eventClick", "pairs", "", "Landroidx/core/util/Pair;", "Landroid/view/View;", "itemJson", "([Landroidx/core/util/Pair;Ljava/lang/String;)V", "getEventState", "getMyUserEventPOJO", "uid", "getScaleType", "Lcom/impaktsoft/globeup/enums/ScaleTypeEnum;", "getUserCurrentLocation", "Lcom/google/android/gms/maps/model/LatLng;", "interactWithEvent", "interestedForEvent", "inviteToEvent", "isEventJobActive", "isEventModifying", "eventUid", "(Ljava/lang/String;)Ljava/lang/Boolean;", "joinEvent", "removeCurrentState", "userEvent", "(Lcom/impaktsoft/globeup/enums/EventState;Lcom/impaktsoft/globeup/models/UserEventPOJO;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "setCellSpinnerVisibility", "isSpinnerVisible", "(Lcom/impaktsoft/globeup/models/EventPOJO;ZLkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public abstract class BaseEventsViewModel extends com.impaktsoft.globeup.viewmodels.BaseViewModel {
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<com.impaktsoft.globeup.models.UserEventPOJO> myUserEventList;
    @org.jetbrains.annotations.Nullable()
    private androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder> eventsAdapter;
    @org.jetbrains.annotations.Nullable()
    private kotlinx.coroutines.Job eventInteractionJob;
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<com.impaktsoft.globeup.models.IEventsItem> eventsFromDatabaseList;
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.listeners.IClickListener startAction;
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.listeners.IClickListener finishAction;
    private java.util.Map<java.lang.String, java.lang.Boolean> eventsSupportingModification;
    private java.lang.String eventConversationKey;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.impaktsoft.globeup.models.UserEventPOJO> getMyUserEventList() {
        return null;
    }
    
    public final void setMyUserEventList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.impaktsoft.globeup.models.UserEventPOJO> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder> getEventsAdapter() {
        return null;
    }
    
    public final void setEventsAdapter(@org.jetbrains.annotations.Nullable()
    androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    protected final kotlinx.coroutines.Job getEventInteractionJob() {
        return null;
    }
    
    protected final void setEventInteractionJob(@org.jetbrains.annotations.Nullable()
    kotlinx.coroutines.Job p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final java.util.ArrayList<com.impaktsoft.globeup.models.IEventsItem> getEventsFromDatabaseList() {
        return null;
    }
    
    protected final void setEventsFromDatabaseList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.impaktsoft.globeup.models.IEventsItem> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    protected final com.impaktsoft.globeup.listeners.IClickListener getStartAction() {
        return null;
    }
    
    protected final void setStartAction(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.listeners.IClickListener p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    protected final com.impaktsoft.globeup.listeners.IClickListener getFinishAction() {
        return null;
    }
    
    protected final void setFinishAction(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.listeners.IClickListener p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean isEventModifying(@org.jetbrains.annotations.NotNull()
    java.lang.String eventUid) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.impaktsoft.globeup.enums.ScaleTypeEnum getScaleType() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.google.android.gms.maps.model.LatLng getUserCurrentLocation() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.enums.EventState getEventState(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.EventPOJO event) {
        return null;
    }
    
    public final void eventClick(@org.jetbrains.annotations.NotNull()
    androidx.core.util.Pair<android.view.View, java.lang.String>[] pairs, @org.jetbrains.annotations.NotNull()
    java.lang.String itemJson) {
    }
    
    public final void interestedForEvent(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.EventPOJO event) {
    }
    
    public final void joinEvent(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.EventPOJO event) {
    }
    
    public final void inviteToEvent(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.EventPOJO event) {
    }
    
    public final void donateForEvent(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.EventPOJO event) {
    }
    
    @org.jetbrains.annotations.Nullable()
    protected final com.impaktsoft.globeup.models.UserEventPOJO getMyUserEventPOJO(@org.jetbrains.annotations.Nullable()
    java.lang.String uid) {
        return null;
    }
    
    private final void interactWithEvent(com.impaktsoft.globeup.models.EventPOJO eventPOJO, com.impaktsoft.globeup.enums.EventState eventState) {
    }
    
    private final boolean isEventJobActive() {
        return false;
    }
    
    private final boolean checkEventsExistInMyList(com.impaktsoft.globeup.models.EventPOJO event) {
        return false;
    }
    
    public BaseEventsViewModel() {
        super();
    }
}