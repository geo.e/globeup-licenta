package com.impaktsoft.globeup.views.fragments;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\f\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\b&\u0018\u0000*\n\b\u0000\u0010\u0001 \u0001*\u00020\u00022\u00020\u0003B\u0013\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0014J\u0012\u0010\u0018\u001a\u00020\u00152\b\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0016J&\u0010\u001b\u001a\u0004\u0018\u00010\u00172\u0006\u0010\u001c\u001a\u00020\u001d2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001f2\b\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0016J\b\u0010 \u001a\u00020\u0015H\u0016J\b\u0010!\u001a\u00020\u0015H\u0016J\u0006\u0010\"\u001a\u00020\u0015J \u0010#\u001a\u00020\u00152\b\u0010$\u001a\u0004\u0018\u00010%2\u0006\u0010&\u001a\u00020\b2\u0006\u0010\'\u001a\u00020\bR\u0012\u0010\u0007\u001a\u00020\bX\u00a4\u0004\u00a2\u0006\u0006\u001a\u0004\b\t\u0010\nR\u0012\u0010\u000b\u001a\u00020\bX\u00a4\u0004\u00a2\u0006\u0006\u001a\u0004\b\f\u0010\nR\u001b\u0010\r\u001a\u00028\u00008DX\u0084\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u000e\u0010\u000fR\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006("}, d2 = {"Lcom/impaktsoft/globeup/views/fragments/BaseFragment;", "T", "Lcom/impaktsoft/globeup/viewmodels/BaseViewModel;", "Landroidx/fragment/app/Fragment;", "vmClass", "Lkotlin/reflect/KClass;", "(Lkotlin/reflect/KClass;)V", "fragmentNameResourceID", "", "getFragmentNameResourceID", "()I", "layoutId", "getLayoutId", "mViewModel", "getMViewModel", "()Lcom/impaktsoft/globeup/viewmodels/BaseViewModel;", "mViewModel$delegate", "Lkotlin/Lazy;", "navHostFragment", "navHostMain", "layoutInflated", "", "root", "Landroid/view/View;", "onActivityCreated", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onDestroy", "onResume", "setActionBar", "setCustomActionBarFunctionality", "customActionBar", "Lcom/impaktsoft/globeup/components/MyCustomAppBarView;", "backStackCount", "stackLimit", "app_debug"})
public abstract class BaseFragment<T extends com.impaktsoft.globeup.viewmodels.BaseViewModel> extends androidx.fragment.app.Fragment {
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy mViewModel$delegate = null;
    private androidx.fragment.app.Fragment navHostFragment;
    private androidx.fragment.app.Fragment navHostMain;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    protected final T getMViewModel() {
        return null;
    }
    
    protected abstract int getLayoutId();
    
    protected abstract int getFragmentNameResourceID();
    
    protected void layoutInflated(@org.jetbrains.annotations.NotNull()
    android.view.View root) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onActivityCreated(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public void onResume() {
    }
    
    @java.lang.Override()
    public void onDestroy() {
    }
    
    public final void setActionBar() {
    }
    
    public final void setCustomActionBarFunctionality(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.components.MyCustomAppBarView customActionBar, int backStackCount, int stackLimit) {
    }
    
    public BaseFragment(@org.jetbrains.annotations.NotNull()
    kotlin.reflect.KClass<T> vmClass) {
        super();
    }
}