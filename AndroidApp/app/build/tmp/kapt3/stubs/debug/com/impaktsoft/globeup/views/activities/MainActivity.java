package com.impaktsoft.globeup.views.activities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\b\u0010\u0006\u001a\u00020\u0007H\u0016J\u0012\u0010\b\u001a\u00020\u00072\b\u0010\t\u001a\u0004\u0018\u00010\nH\u0014J\u0010\u0010\u000b\u001a\u00020\u00072\u0006\u0010\f\u001a\u00020\rH\u0002R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"}, d2 = {"Lcom/impaktsoft/globeup/views/activities/MainActivity;", "Lcom/impaktsoft/globeup/views/activities/BaseActivity;", "Lcom/impaktsoft/globeup/viewmodels/MainViewModel;", "()V", "mBinding", "Lcom/impaktsoft/globeup/databinding/NewMessageNotificationCellBinding;", "onBackPressed", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "setUnreadMessagesNotificationBinding", "navView", "Lcom/google/android/material/bottomnavigation/BottomNavigationView;", "app_debug"})
public final class MainActivity extends com.impaktsoft.globeup.views.activities.BaseActivity<com.impaktsoft.globeup.viewmodels.MainViewModel> {
    private com.impaktsoft.globeup.databinding.NewMessageNotificationCellBinding mBinding;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    private final void setUnreadMessagesNotificationBinding(com.google.android.material.bottomnavigation.BottomNavigationView navView) {
    }
    
    public MainActivity() {
        super(null);
    }
}