package com.impaktsoft.globeup.services.database;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0090\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\f\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J#\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u00062\b\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010 J!\u0010!\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u00062\u0006\u0010\"\u001a\u00020#H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010$J!\u0010%\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u00062\u0006\u0010&\u001a\u00020\'H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010(J!\u0010)\u001a\u00020\u001c2\u0006\u0010*\u001a\u00020\u00062\u0006\u0010\u001e\u001a\u00020+H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010,J!\u0010-\u001a\u00020\u001c2\u0006\u0010.\u001a\u00020\u00062\u0006\u0010\u001d\u001a\u00020\u0006H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010/J!\u00100\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u00062\u0006\u00101\u001a\u000202H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u00103J!\u00104\u001a\u00020\u001c2\u0006\u0010*\u001a\u00020\u00062\u0006\u0010\u001d\u001a\u00020\u0006H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010/J!\u00105\u001a\u00020\u001c2\u0006\u0010*\u001a\u00020\u00062\u0006\u0010\u001d\u001a\u00020\u0006H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010/J!\u00106\u001a\u0002072\u0006\u0010*\u001a\u00020\u00062\u0006\u0010\u001d\u001a\u00020\u0006H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010/J\u001b\u00108\u001a\u0004\u0018\u0001092\u0006\u0010\u001d\u001a\u00020\u0006H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010:J)\u0010;\u001a\u0012\u0012\u0004\u0012\u00020<0\u000bj\b\u0012\u0004\u0012\u00020<`=2\u0006\u0010\u001d\u001a\u00020\u0006H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010:J-\u0010>\u001a\u0016\u0012\u0004\u0012\u00020?\u0018\u00010\u000bj\n\u0012\u0004\u0012\u00020?\u0018\u0001`=2\u0006\u0010*\u001a\u00020\u0006H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010:J\u001b\u0010@\u001a\u0004\u0018\u00010\u00062\u0006\u0010.\u001a\u00020\u0006H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010:J\b\u0010A\u001a\u00020\u001cH\u0016J\b\u0010B\u001a\u00020\u001cH\u0016J\b\u0010C\u001a\u00020\u001cH\u0016J\u0019\u0010D\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u0006H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010:J\u0019\u0010E\u001a\u00020\u001c2\u0006\u0010*\u001a\u00020\u0006H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010:J!\u0010F\u001a\u00020\u001c2\u0006\u0010*\u001a\u00020\u00062\u0006\u0010\u001d\u001a\u00020\u0006H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010/J!\u0010G\u001a\u00020\u001c2\u0006\u0010*\u001a\u00020\u00062\u0006\u0010\u001d\u001a\u00020\u0006H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010/J+\u0010H\u001a\u00020\u001c2\u0006\u0010I\u001a\u00020<2\u0006\u0010\u001d\u001a\u00020\u00062\b\u0010\u001e\u001a\u0004\u0018\u00010JH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010KJ\u0019\u0010L\u001a\u00020\u001c2\u0006\u0010M\u001a\u000209H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010NJ!\u0010O\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u00062\u0006\u0010I\u001a\u00020<H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010PJ!\u0010Q\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u00062\u0006\u0010R\u001a\u00020\u0006H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010/J!\u0010S\u001a\u00020\u001c2\u0006\u0010*\u001a\u00020\u00062\u0006\u0010\u001d\u001a\u00020\u0006H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010/J!\u0010T\u001a\u00020\u001c2\u0006\u0010U\u001a\u00020\u00062\u0006\u0010\u001d\u001a\u00020\u0006H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010/R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0017\u001a\u0004\u0018\u00010\u0018X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006V"}, d2 = {"Lcom/impaktsoft/globeup/services/database/ChatDAO;", "Lcom/impaktsoft/globeup/services/database/IChatDAO;", "fireStore", "Lcom/google/firebase/firestore/FirebaseFirestore;", "(Lcom/google/firebase/firestore/FirebaseFirestore;)V", "conversationCollection", "", "conversationNameField", "conversationObserver", "Lcom/google/firebase/firestore/ListenerRegistration;", "conversationsLastMessageObservers", "Ljava/util/ArrayList;", "eventConversationsCollection", "lastMessageDocumentLoaded", "Lcom/google/firebase/firestore/DocumentSnapshot;", "lastMessageField", "lastMessageObserver", "lastMessageStateField", "membersCountObserver", "membersObserver", "membersPath", "messageDateField", "messagesPath", "messagesQuery", "Lcom/google/firebase/firestore/Query;", "userConversationPath", "userConversationsCollection", "addConversationMembersCountObserver", "", "conversationKey", "listener", "Lcom/impaktsoft/globeup/listeners/IGetConversationMembersCount;", "(Ljava/lang/String;Lcom/impaktsoft/globeup/listeners/IGetConversationMembersCount;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "addConversationMembersObserver", "membersKeyListener", "Lcom/impaktsoft/globeup/listeners/IGetMembersKeyListener;", "(Ljava/lang/String;Lcom/impaktsoft/globeup/listeners/IGetMembersKeyListener;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "addConversationMessagesObserver", "messageListener", "Lcom/impaktsoft/globeup/listeners/IGetMessagesListener;", "(Ljava/lang/String;Lcom/impaktsoft/globeup/listeners/IGetMessagesListener;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "addConversationsObserver", "userKey", "Lcom/impaktsoft/globeup/listeners/IGetConversationListener;", "(Ljava/lang/String;Lcom/impaktsoft/globeup/listeners/IGetConversationListener;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "addEventConversation", "eventKey", "(Ljava/lang/String;Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "addLastMessageCountObserver", "seenCount", "Lcom/impaktsoft/globeup/listeners/IGetLastMessageSeenCount;", "(Ljava/lang/String;Lcom/impaktsoft/globeup/listeners/IGetLastMessageSeenCount;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "addUserConversation", "addUserInConversation", "didUserSeenMessage", "", "fetchConversation", "Lcom/impaktsoft/globeup/models/ConversationPOJO;", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "fetchConversationMessages", "Lcom/impaktsoft/globeup/models/MessagePOJO;", "Lkotlin/collections/ArrayList;", "fetchUserConversations", "Lcom/impaktsoft/globeup/models/ConversationRefPOJO;", "getEventConversationKey", "removeConversationMessagesCountObservers", "removeConversationsLastMessageObserver", "removeLastMessageDocument", "removeLastMessageState", "removeUserAllConversations", "removeUserConversation", "removeUserFromConversation", "sendMessage", "messagePOJO", "Lcom/impaktsoft/globeup/listeners/IMessageSentListener;", "(Lcom/impaktsoft/globeup/models/MessagePOJO;Ljava/lang/String;Lcom/impaktsoft/globeup/listeners/IMessageSentListener;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "storeConversation", "conversationPOJO", "(Lcom/impaktsoft/globeup/models/ConversationPOJO;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "updateConversationLastMessage", "(Ljava/lang/String;Lcom/impaktsoft/globeup/models/MessagePOJO;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "updateConversationName", "newName", "updateLastMessageState", "updateMessageState", "messageKey", "app_debug"})
public final class ChatDAO implements com.impaktsoft.globeup.services.database.IChatDAO {
    private final java.lang.String conversationCollection = "Conversations";
    private final java.lang.String eventConversationsCollection = "EventConversations";
    private final java.lang.String userConversationsCollection = "UserConversations";
    private final java.lang.String membersPath = "Members";
    private final java.lang.String messagesPath = "Messages";
    private final java.lang.String userConversationPath = "ConversationsDocumets";
    private final java.lang.String messageDateField = "creationTimeUTC";
    private final java.lang.String lastMessageStateField = "LastMessageSeenBy";
    private final java.lang.String lastMessageField = "lastMessage";
    private final java.lang.String conversationNameField = "name";
    private com.google.firebase.firestore.ListenerRegistration conversationObserver;
    private com.google.firebase.firestore.ListenerRegistration lastMessageObserver;
    private com.google.firebase.firestore.ListenerRegistration membersCountObserver;
    private com.google.firebase.firestore.ListenerRegistration membersObserver;
    private com.google.firebase.firestore.Query messagesQuery;
    private com.google.firebase.firestore.DocumentSnapshot lastMessageDocumentLoaded;
    private final java.util.ArrayList<com.google.firebase.firestore.ListenerRegistration> conversationsLastMessageObservers = null;
    private final com.google.firebase.firestore.FirebaseFirestore fireStore = null;
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object addEventConversation(@org.jetbrains.annotations.NotNull()
    java.lang.String eventKey, @org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p2) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object addUserConversation(@org.jetbrains.annotations.NotNull()
    java.lang.String userKey, @org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p2) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object addUserInConversation(@org.jetbrains.annotations.NotNull()
    java.lang.String userKey, @org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p2) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object storeConversation(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.ConversationPOJO conversationPOJO, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p1) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object fetchConversation(@org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.impaktsoft.globeup.models.ConversationPOJO> p1) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object fetchUserConversations(@org.jetbrains.annotations.NotNull()
    java.lang.String userKey, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.ArrayList<com.impaktsoft.globeup.models.ConversationRefPOJO>> p1) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object removeUserConversation(@org.jetbrains.annotations.NotNull()
    java.lang.String userKey, @org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p2) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object removeUserFromConversation(@org.jetbrains.annotations.NotNull()
    java.lang.String userKey, @org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p2) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object sendMessage(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.MessagePOJO messagePOJO, @org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.listeners.IMessageSentListener listener, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p3) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object getEventConversationKey(@org.jetbrains.annotations.NotNull()
    java.lang.String eventKey, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.lang.String> p1) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object addConversationMessagesObserver(@org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.listeners.IGetMessagesListener messageListener, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p2) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object fetchConversationMessages(@org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.ArrayList<com.impaktsoft.globeup.models.MessagePOJO>> p1) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object updateConversationLastMessage(@org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.MessagePOJO messagePOJO, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p2) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object updateLastMessageState(@org.jetbrains.annotations.NotNull()
    java.lang.String userKey, @org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p2) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object removeLastMessageState(@org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p1) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object addLastMessageCountObserver(@org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.listeners.IGetLastMessageSeenCount seenCount, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p2) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object addConversationMembersCountObserver(@org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.listeners.IGetConversationMembersCount listener, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p2) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object addConversationMembersObserver(@org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.listeners.IGetMembersKeyListener membersKeyListener, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p2) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object updateMessageState(@org.jetbrains.annotations.NotNull()
    java.lang.String messageKey, @org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p2) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object addConversationsObserver(@org.jetbrains.annotations.NotNull()
    java.lang.String userKey, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.listeners.IGetConversationListener listener, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p2) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object didUserSeenMessage(@org.jetbrains.annotations.NotNull()
    java.lang.String userKey, @org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.lang.Boolean> p2) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object updateConversationName(@org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    java.lang.String newName, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p2) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object removeUserAllConversations(@org.jetbrains.annotations.NotNull()
    java.lang.String userKey, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p1) {
        return null;
    }
    
    @java.lang.Override()
    public void removeConversationMessagesCountObservers() {
    }
    
    @java.lang.Override()
    public void removeConversationsLastMessageObserver() {
    }
    
    @java.lang.Override()
    public void removeLastMessageDocument() {
    }
    
    public ChatDAO(@org.jetbrains.annotations.NotNull()
    com.google.firebase.firestore.FirebaseFirestore fireStore) {
        super();
    }
}