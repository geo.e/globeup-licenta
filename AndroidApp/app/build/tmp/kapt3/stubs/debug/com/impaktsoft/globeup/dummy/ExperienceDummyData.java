package com.impaktsoft.globeup.dummy;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lcom/impaktsoft/globeup/dummy/ExperienceDummyData;", "", "()V", "Companion", "app_debug"})
public final class ExperienceDummyData {
    private static final java.util.ArrayList<com.impaktsoft.globeup.models.ExperiencePOJO> subList1 = null;
    private static final java.util.ArrayList<com.impaktsoft.globeup.models.ExperiencePOJO> subList2 = null;
    private static final java.util.ArrayList<com.impaktsoft.globeup.models.ExperiencePOJO> subList3 = null;
    @org.jetbrains.annotations.NotNull()
    private static final java.util.ArrayList<java.util.ArrayList<com.impaktsoft.globeup.models.ExperiencePOJO>> experienceListPOJO = null;
    public static final com.impaktsoft.globeup.dummy.ExperienceDummyData.Companion Companion = null;
    
    public ExperienceDummyData() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u001d\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u001e\u0010\b\u001a\u0012\u0012\u0004\u0012\u00020\u00050\u0004j\b\u0012\u0004\u0012\u00020\u0005`\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\n\u001a\u0012\u0012\u0004\u0012\u00020\u00050\u0004j\b\u0012\u0004\u0012\u00020\u0005`\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u000b\u001a\u0012\u0012\u0004\u0012\u00020\u00050\u0004j\b\u0012\u0004\u0012\u00020\u0005`\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\f"}, d2 = {"Lcom/impaktsoft/globeup/dummy/ExperienceDummyData$Companion;", "", "()V", "experienceListPOJO", "Ljava/util/ArrayList;", "Lcom/impaktsoft/globeup/models/ExperiencePOJO;", "getExperienceListPOJO", "()Ljava/util/ArrayList;", "subList1", "Lkotlin/collections/ArrayList;", "subList2", "subList3", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.ArrayList<java.util.ArrayList<com.impaktsoft.globeup.models.ExperiencePOJO>> getExperienceListPOJO() {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}