package com.impaktsoft.globeup.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u000f\u0018\u00002\u00020\u0001B-\b\u0016\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0010\b\u0002\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\tR\"\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\rR\u001e\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0012\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u001c\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016\u00a8\u0006\u0017"}, d2 = {"Lcom/impaktsoft/globeup/models/ConfigCountryModel;", "", "buttonName", "", "blockActionIfCountryNotSelected", "", "action", "Lkotlin/Function0;", "", "(Ljava/lang/String;Ljava/lang/Boolean;Lkotlin/jvm/functions/Function0;)V", "getAction", "()Lkotlin/jvm/functions/Function0;", "setAction", "(Lkotlin/jvm/functions/Function0;)V", "getBlockActionIfCountryNotSelected", "()Ljava/lang/Boolean;", "setBlockActionIfCountryNotSelected", "(Ljava/lang/Boolean;)V", "Ljava/lang/Boolean;", "getButtonName", "()Ljava/lang/String;", "setButtonName", "(Ljava/lang/String;)V", "app_debug"})
public final class ConfigCountryModel {
    @org.jetbrains.annotations.Nullable()
    private java.lang.String buttonName;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Boolean blockActionIfCountryNotSelected;
    @org.jetbrains.annotations.Nullable()
    private kotlin.jvm.functions.Function0<kotlin.Unit> action;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getButtonName() {
        return null;
    }
    
    public final void setButtonName(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean getBlockActionIfCountryNotSelected() {
        return null;
    }
    
    public final void setBlockActionIfCountryNotSelected(@org.jetbrains.annotations.Nullable()
    java.lang.Boolean p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final kotlin.jvm.functions.Function0<kotlin.Unit> getAction() {
        return null;
    }
    
    public final void setAction(@org.jetbrains.annotations.Nullable()
    kotlin.jvm.functions.Function0<kotlin.Unit> p0) {
    }
    
    public ConfigCountryModel(@org.jetbrains.annotations.Nullable()
    java.lang.String buttonName, @org.jetbrains.annotations.Nullable()
    java.lang.Boolean blockActionIfCountryNotSelected, @org.jetbrains.annotations.Nullable()
    kotlin.jvm.functions.Function0<kotlin.Unit> action) {
        super();
    }
}