package com.impaktsoft.globeup.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0012\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u00002\u00020\u0001B\'\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\bB\u0007\b\u0016\u00a2\u0006\u0002\u0010\tR \u0010\n\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR\u001a\u0010\u0010\u001a\u00020\u0011X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u001a\u0010\u0016\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001aR\u001c\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001b\u0010\u0018\"\u0004\b\u001c\u0010\u001aR\u001a\u0010\u001d\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001e\u0010\u0018\"\u0004\b\u001f\u0010\u001aR\u001c\u0010 \u001a\u0004\u0018\u00010\u000bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b!\u0010\r\"\u0004\b\"\u0010\u000fR\u001a\u0010#\u001a\u00020$X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b%\u0010&\"\u0004\b\'\u0010(R\u001c\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b)\u0010*\"\u0004\b+\u0010,\u00a8\u0006-"}, d2 = {"Lcom/impaktsoft/globeup/models/MessagePOJO;", "Lcom/impaktsoft/globeup/models/IMessagePOJO;", "messageKey", "", "type", "Lcom/impaktsoft/globeup/enums/MessageType;", "jsonObject", "sender", "(Ljava/lang/String;Lcom/impaktsoft/globeup/enums/MessageType;Ljava/lang/String;Ljava/lang/String;)V", "()V", "creationTimeUTC", "Ljava/util/Date;", "getCreationTimeUTC", "()Ljava/util/Date;", "setCreationTimeUTC", "(Ljava/util/Date;)V", "deletionTimeUTC", "", "getDeletionTimeUTC", "()J", "setDeletionTimeUTC", "(J)V", "jsonMessage", "getJsonMessage", "()Ljava/lang/String;", "setJsonMessage", "(Ljava/lang/String;)V", "getMessageKey", "setMessageKey", "senderKey", "getSenderKey", "setSenderKey", "serverCreationTimeUTC", "getServerCreationTimeUTC", "setServerCreationTimeUTC", "state", "Lcom/impaktsoft/globeup/enums/MessageState;", "getState", "()Lcom/impaktsoft/globeup/enums/MessageState;", "setState", "(Lcom/impaktsoft/globeup/enums/MessageState;)V", "getType", "()Lcom/impaktsoft/globeup/enums/MessageType;", "setType", "(Lcom/impaktsoft/globeup/enums/MessageType;)V", "app_debug"})
public final class MessagePOJO implements com.impaktsoft.globeup.models.IMessagePOJO {
    @org.jetbrains.annotations.Nullable()
    private java.lang.String messageKey;
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.enums.MessageType type;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String jsonMessage;
    @org.jetbrains.annotations.Nullable()
    @com.google.firebase.firestore.ServerTimestamp()
    private java.util.Date creationTimeUTC;
    @org.jetbrains.annotations.Nullable()
    private java.util.Date serverCreationTimeUTC;
    private long deletionTimeUTC;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String senderKey;
    @org.jetbrains.annotations.NotNull()
    private com.impaktsoft.globeup.enums.MessageState state;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMessageKey() {
        return null;
    }
    
    public final void setMessageKey(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.enums.MessageType getType() {
        return null;
    }
    
    public final void setType(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.enums.MessageType p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getJsonMessage() {
        return null;
    }
    
    public final void setJsonMessage(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.Date getCreationTimeUTC() {
        return null;
    }
    
    public final void setCreationTimeUTC(@org.jetbrains.annotations.Nullable()
    java.util.Date p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.Date getServerCreationTimeUTC() {
        return null;
    }
    
    public final void setServerCreationTimeUTC(@org.jetbrains.annotations.Nullable()
    java.util.Date p0) {
    }
    
    public final long getDeletionTimeUTC() {
        return 0L;
    }
    
    public final void setDeletionTimeUTC(long p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getSenderKey() {
        return null;
    }
    
    public final void setSenderKey(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.impaktsoft.globeup.enums.MessageState getState() {
        return null;
    }
    
    public final void setState(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.enums.MessageState p0) {
    }
    
    public MessagePOJO(@org.jetbrains.annotations.NotNull()
    java.lang.String messageKey, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.enums.MessageType type, @org.jetbrains.annotations.NotNull()
    java.lang.String jsonObject, @org.jetbrains.annotations.NotNull()
    java.lang.String sender) {
        super();
    }
    
    public MessagePOJO() {
        super();
    }
}