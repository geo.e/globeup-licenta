package com.impaktsoft.globeup.components;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0007J\u000e\u0010\u0015\u001a\u00020\u00122\u0006\u0010\u0016\u001a\u00020\u0017J\u001f\u0010\u0018\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u001a\u00a2\u0006\u0002\u0010\u001bR\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u000b\u001a\u00020\f8DX\u0084\u0084\u0002\u00a2\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\r\u0010\u000e\u00a8\u0006\u001c"}, d2 = {"Lcom/impaktsoft/globeup/components/MapWithRadius;", "Lorg/koin/core/KoinComponent;", "mMap", "Lcom/google/android/gms/maps/GoogleMap;", "context", "Landroid/content/Context;", "(Lcom/google/android/gms/maps/GoogleMap;Landroid/content/Context;)V", "circle", "Lcom/google/android/gms/maps/model/Circle;", "circleOptions", "Lcom/google/android/gms/maps/model/CircleOptions;", "sharedPreferencesService", "Lcom/impaktsoft/globeup/services/ISharedPreferencesService;", "getSharedPreferencesService", "()Lcom/impaktsoft/globeup/services/ISharedPreferencesService;", "sharedPreferencesService$delegate", "Lkotlin/Lazy;", "createCircleRadius", "", "coords", "Lcom/google/android/gms/maps/model/LatLng;", "setCircleRadius", "progress", "", "setPinAndCameraPosition", "zoom", "", "(Lcom/google/android/gms/maps/model/LatLng;Ljava/lang/Float;)V", "app_debug"})
public final class MapWithRadius implements org.koin.core.KoinComponent {
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy sharedPreferencesService$delegate = null;
    private com.google.android.gms.maps.model.CircleOptions circleOptions;
    private com.google.android.gms.maps.model.Circle circle;
    private final com.google.android.gms.maps.GoogleMap mMap = null;
    private final android.content.Context context = null;
    
    @org.jetbrains.annotations.NotNull()
    protected final com.impaktsoft.globeup.services.ISharedPreferencesService getSharedPreferencesService() {
        return null;
    }
    
    public final void setPinAndCameraPosition(@org.jetbrains.annotations.NotNull()
    com.google.android.gms.maps.model.LatLng coords, @org.jetbrains.annotations.Nullable()
    java.lang.Float zoom) {
    }
    
    @android.annotation.TargetApi(value = android.os.Build.VERSION_CODES.M)
    public final void createCircleRadius(@org.jetbrains.annotations.NotNull()
    com.google.android.gms.maps.model.LatLng coords) {
    }
    
    public final void setCircleRadius(int progress) {
    }
    
    public MapWithRadius(@org.jetbrains.annotations.NotNull()
    com.google.android.gms.maps.GoogleMap mMap, @org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public org.koin.core.Koin getKoin() {
        return null;
    }
}