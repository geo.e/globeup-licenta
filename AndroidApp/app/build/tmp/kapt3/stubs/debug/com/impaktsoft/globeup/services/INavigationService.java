package com.impaktsoft.globeup.services;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\b\u0010\u0004\u001a\u00020\u0003H&Jo\u0010\u0005\u001a\u00020\u0003\"\b\b\u0000\u0010\u0006*\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00060\t2\b\b\u0002\u0010\n\u001a\u00020\u000b2\b\b\u0002\u0010\f\u001a\u00020\u000b2\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u000e2\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u000e2\u001c\b\u0002\u0010\u0010\u001a\u0016\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u000e0\u0012\u0018\u00010\u0011H&\u00a2\u0006\u0002\u0010\u0014J\u0018\u0010\u0015\u001a\u00020\u00032\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0017H&J(\u0010\u0015\u001a\u00020\u0003\"\b\b\u0000\u0010\u0006*\u00020\u00192\f\u0010\u001a\u001a\b\u0012\u0004\u0012\u0002H\u00060\u001b2\u0006\u0010\n\u001a\u00020\u000bH&J\u0010\u0010\u001c\u001a\u00020\u00032\u0006\u0010\u0018\u001a\u00020\u0017H&\u00a8\u0006\u001d"}, d2 = {"Lcom/impaktsoft/globeup/services/INavigationService;", "", "closeCurrentActivity", "", "closeCurrentActivityAfterTransition", "navigateToActivity", "T", "Landroid/app/Activity;", "activityClass", "Ljava/lang/Class;", "finishCurrentActivity", "", "clearBackStack", "parameterName", "", "parameterValue", "transitionPairs", "", "Landroidx/core/util/Pair;", "Landroid/view/View;", "(Ljava/lang/Class;ZZLjava/lang/String;Ljava/lang/String;[Landroidx/core/util/Pair;)V", "navigateToFragment", "transitionId", "", "navHostId", "Landroidx/fragment/app/Fragment;", "fragmentClass", "Lkotlin/reflect/KClass;", "popFragmentBackStack", "app_debug"})
public abstract interface INavigationService {
    
    public abstract <T extends android.app.Activity>void navigateToActivity(@org.jetbrains.annotations.NotNull()
    java.lang.Class<T> activityClass, boolean finishCurrentActivity, boolean clearBackStack, @org.jetbrains.annotations.Nullable()
    java.lang.String parameterName, @org.jetbrains.annotations.Nullable()
    java.lang.String parameterValue, @org.jetbrains.annotations.Nullable()
    androidx.core.util.Pair<android.view.View, java.lang.String>[] transitionPairs);
    
    public abstract void closeCurrentActivity();
    
    public abstract void navigateToFragment(int transitionId, int navHostId);
    
    public abstract <T extends androidx.fragment.app.Fragment>void navigateToFragment(@org.jetbrains.annotations.NotNull()
    kotlin.reflect.KClass<T> fragmentClass, boolean finishCurrentActivity);
    
    public abstract void popFragmentBackStack(int navHostId);
    
    public abstract void closeCurrentActivityAfterTransition();
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 3)
    public final class DefaultImpls {
    }
}