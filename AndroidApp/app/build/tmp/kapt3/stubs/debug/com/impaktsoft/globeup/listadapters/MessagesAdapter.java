package com.impaktsoft.globeup.listadapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u0000 !2\b\u0012\u0004\u0012\u00020\u00020\u0001:\t!\"#$%&\'()B-\u0012\u0016\u0010\u0003\u001a\u0012\u0012\u0004\u0012\u00020\u00050\u0004j\b\u0012\u0004\u0012\u00020\u0005`\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\b\u0010\u0016\u001a\u00020\u0017H\u0016J\u0010\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u0019\u001a\u00020\u0017H\u0016J\u0018\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u00022\u0006\u0010\u0019\u001a\u00020\u0017H\u0016J\u0018\u0010\u001d\u001a\u00020\u00022\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u0017H\u0016R\u001a\u0010\u0007\u001a\u00020\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR.\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0004j\n\u0012\u0004\u0012\u00020\u0005\u0018\u0001`\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015\u00a8\u0006*"}, d2 = {"Lcom/impaktsoft/globeup/listadapters/MessagesAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "myList", "Ljava/util/ArrayList;", "Lcom/impaktsoft/globeup/models/IMessagePOJO;", "Lkotlin/collections/ArrayList;", "currentUserID", "", "viewModel", "Lcom/impaktsoft/globeup/viewmodels/MessengerViewModel;", "(Ljava/util/ArrayList;Ljava/lang/String;Lcom/impaktsoft/globeup/viewmodels/MessengerViewModel;)V", "getCurrentUserID", "()Ljava/lang/String;", "setCurrentUserID", "(Ljava/lang/String;)V", "getMyList", "()Ljava/util/ArrayList;", "setMyList", "(Ljava/util/ArrayList;)V", "getViewModel", "()Lcom/impaktsoft/globeup/viewmodels/MessengerViewModel;", "getItemCount", "", "getItemViewType", "position", "onBindViewHolder", "", "holder", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "Companion", "FriendImageMessageViewHolder", "FriendLocationMessageViewHolder", "FriendTextMessageViewHolder", "MessageStateViewHolder", "MessageTimeSectionViewHolder", "UserImageMessageViewHolder", "UserLocationMessageViewHolder", "UserTextMessageViewHolder", "app_debug"})
public final class MessagesAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder> {
    @org.jetbrains.annotations.NotNull()
    private java.lang.String currentUserID;
    @org.jetbrains.annotations.Nullable()
    private java.util.ArrayList<com.impaktsoft.globeup.models.IMessagePOJO> myList;
    @org.jetbrains.annotations.NotNull()
    private final com.impaktsoft.globeup.viewmodels.MessengerViewModel viewModel = null;
    private static final int TYPE_MESSAGE_TEXT_FRIEND = 0;
    private static final int TYPE_MESSAGE_TEXT_USER = 1;
    private static final int TYPE_MESSAGE_IMAGE_FRIEND = 2;
    private static final int TYPE_MESSAGE_IMAGE_USER = 3;
    private static final int TYPE_MESSAGE_LOCATION_FRIEND = 4;
    private static final int TYPE_MESSAGE_LOCATION_USER = 5;
    private static final int TYPE_MESSAGE_STATE = 6;
    private static final int TYPE_MESSAGE_SECTION = 7;
    public static final com.impaktsoft.globeup.listadapters.MessagesAdapter.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getCurrentUserID() {
        return null;
    }
    
    public final void setCurrentUserID(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.ArrayList<com.impaktsoft.globeup.models.IMessagePOJO> getMyList() {
        return null;
    }
    
    public final void setMyList(@org.jetbrains.annotations.Nullable()
    java.util.ArrayList<com.impaktsoft.globeup.models.IMessagePOJO> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public androidx.recyclerview.widget.RecyclerView.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public int getItemViewType(int position) {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.RecyclerView.ViewHolder holder, int position) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.impaktsoft.globeup.viewmodels.MessengerViewModel getViewModel() {
        return null;
    }
    
    public MessagesAdapter(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.impaktsoft.globeup.models.IMessagePOJO> myList, @org.jetbrains.annotations.NotNull()
    java.lang.String currentUserID, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.viewmodels.MessengerViewModel viewModel) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fR\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\r"}, d2 = {"Lcom/impaktsoft/globeup/listadapters/MessagesAdapter$UserTextMessageViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "applicationBinding", "Lcom/impaktsoft/globeup/databinding/UserTextMessageCellBinding;", "(Lcom/impaktsoft/globeup/listadapters/MessagesAdapter;Lcom/impaktsoft/globeup/databinding/UserTextMessageCellBinding;)V", "getApplicationBinding", "()Lcom/impaktsoft/globeup/databinding/UserTextMessageCellBinding;", "setApplicationBinding", "(Lcom/impaktsoft/globeup/databinding/UserTextMessageCellBinding;)V", "bind", "", "item", "Lcom/impaktsoft/globeup/models/MessagePOJO;", "app_debug"})
    public final class UserTextMessageViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private com.impaktsoft.globeup.databinding.UserTextMessageCellBinding applicationBinding;
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.models.MessagePOJO item) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.impaktsoft.globeup.databinding.UserTextMessageCellBinding getApplicationBinding() {
            return null;
        }
        
        public final void setApplicationBinding(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.databinding.UserTextMessageCellBinding p0) {
        }
        
        public UserTextMessageViewHolder(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.databinding.UserTextMessageCellBinding applicationBinding) {
            super(null);
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fR\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\r"}, d2 = {"Lcom/impaktsoft/globeup/listadapters/MessagesAdapter$FriendTextMessageViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "applicationBinding", "Lcom/impaktsoft/globeup/databinding/FriendTextMessageCellBinding;", "(Lcom/impaktsoft/globeup/listadapters/MessagesAdapter;Lcom/impaktsoft/globeup/databinding/FriendTextMessageCellBinding;)V", "getApplicationBinding", "()Lcom/impaktsoft/globeup/databinding/FriendTextMessageCellBinding;", "setApplicationBinding", "(Lcom/impaktsoft/globeup/databinding/FriendTextMessageCellBinding;)V", "bind", "", "item", "Lcom/impaktsoft/globeup/models/MessagePOJO;", "app_debug"})
    public final class FriendTextMessageViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private com.impaktsoft.globeup.databinding.FriendTextMessageCellBinding applicationBinding;
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.models.MessagePOJO item) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.impaktsoft.globeup.databinding.FriendTextMessageCellBinding getApplicationBinding() {
            return null;
        }
        
        public final void setApplicationBinding(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.databinding.FriendTextMessageCellBinding p0) {
        }
        
        public FriendTextMessageViewHolder(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.databinding.FriendTextMessageCellBinding applicationBinding) {
            super(null);
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fR\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\r"}, d2 = {"Lcom/impaktsoft/globeup/listadapters/MessagesAdapter$UserLocationMessageViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "applicationBinding", "Lcom/impaktsoft/globeup/databinding/UserLocationMessageCellBinding;", "(Lcom/impaktsoft/globeup/listadapters/MessagesAdapter;Lcom/impaktsoft/globeup/databinding/UserLocationMessageCellBinding;)V", "getApplicationBinding", "()Lcom/impaktsoft/globeup/databinding/UserLocationMessageCellBinding;", "setApplicationBinding", "(Lcom/impaktsoft/globeup/databinding/UserLocationMessageCellBinding;)V", "bind", "", "item", "Lcom/impaktsoft/globeup/models/MessagePOJO;", "app_debug"})
    public final class UserLocationMessageViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private com.impaktsoft.globeup.databinding.UserLocationMessageCellBinding applicationBinding;
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.models.MessagePOJO item) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.impaktsoft.globeup.databinding.UserLocationMessageCellBinding getApplicationBinding() {
            return null;
        }
        
        public final void setApplicationBinding(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.databinding.UserLocationMessageCellBinding p0) {
        }
        
        public UserLocationMessageViewHolder(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.databinding.UserLocationMessageCellBinding applicationBinding) {
            super(null);
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fR\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\r"}, d2 = {"Lcom/impaktsoft/globeup/listadapters/MessagesAdapter$FriendLocationMessageViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "applicationBinding", "Lcom/impaktsoft/globeup/databinding/FriendLocationMessageCellBinding;", "(Lcom/impaktsoft/globeup/listadapters/MessagesAdapter;Lcom/impaktsoft/globeup/databinding/FriendLocationMessageCellBinding;)V", "getApplicationBinding", "()Lcom/impaktsoft/globeup/databinding/FriendLocationMessageCellBinding;", "setApplicationBinding", "(Lcom/impaktsoft/globeup/databinding/FriendLocationMessageCellBinding;)V", "bind", "", "item", "Lcom/impaktsoft/globeup/models/MessagePOJO;", "app_debug"})
    public final class FriendLocationMessageViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private com.impaktsoft.globeup.databinding.FriendLocationMessageCellBinding applicationBinding;
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.models.MessagePOJO item) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.impaktsoft.globeup.databinding.FriendLocationMessageCellBinding getApplicationBinding() {
            return null;
        }
        
        public final void setApplicationBinding(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.databinding.FriendLocationMessageCellBinding p0) {
        }
        
        public FriendLocationMessageViewHolder(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.databinding.FriendLocationMessageCellBinding applicationBinding) {
            super(null);
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fR\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\r"}, d2 = {"Lcom/impaktsoft/globeup/listadapters/MessagesAdapter$UserImageMessageViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "applicationBinding", "Lcom/impaktsoft/globeup/databinding/UserImageMessageCellBinding;", "(Lcom/impaktsoft/globeup/listadapters/MessagesAdapter;Lcom/impaktsoft/globeup/databinding/UserImageMessageCellBinding;)V", "getApplicationBinding", "()Lcom/impaktsoft/globeup/databinding/UserImageMessageCellBinding;", "setApplicationBinding", "(Lcom/impaktsoft/globeup/databinding/UserImageMessageCellBinding;)V", "bind", "", "item", "Lcom/impaktsoft/globeup/models/MessagePOJO;", "app_debug"})
    public final class UserImageMessageViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private com.impaktsoft.globeup.databinding.UserImageMessageCellBinding applicationBinding;
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.models.MessagePOJO item) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.impaktsoft.globeup.databinding.UserImageMessageCellBinding getApplicationBinding() {
            return null;
        }
        
        public final void setApplicationBinding(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.databinding.UserImageMessageCellBinding p0) {
        }
        
        public UserImageMessageViewHolder(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.databinding.UserImageMessageCellBinding applicationBinding) {
            super(null);
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fR\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\r"}, d2 = {"Lcom/impaktsoft/globeup/listadapters/MessagesAdapter$FriendImageMessageViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "applicationBinding", "Lcom/impaktsoft/globeup/databinding/FriendImageMessageCellBinding;", "(Lcom/impaktsoft/globeup/listadapters/MessagesAdapter;Lcom/impaktsoft/globeup/databinding/FriendImageMessageCellBinding;)V", "getApplicationBinding", "()Lcom/impaktsoft/globeup/databinding/FriendImageMessageCellBinding;", "setApplicationBinding", "(Lcom/impaktsoft/globeup/databinding/FriendImageMessageCellBinding;)V", "bind", "", "item", "Lcom/impaktsoft/globeup/models/MessagePOJO;", "app_debug"})
    public final class FriendImageMessageViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private com.impaktsoft.globeup.databinding.FriendImageMessageCellBinding applicationBinding;
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.models.MessagePOJO item) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.impaktsoft.globeup.databinding.FriendImageMessageCellBinding getApplicationBinding() {
            return null;
        }
        
        public final void setApplicationBinding(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.databinding.FriendImageMessageCellBinding p0) {
        }
        
        public FriendImageMessageViewHolder(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.databinding.FriendImageMessageCellBinding applicationBinding) {
            super(null);
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fR\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\r"}, d2 = {"Lcom/impaktsoft/globeup/listadapters/MessagesAdapter$MessageStateViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "applicationBinding", "Lcom/impaktsoft/globeup/databinding/UserMessageStateBinding;", "(Lcom/impaktsoft/globeup/listadapters/MessagesAdapter;Lcom/impaktsoft/globeup/databinding/UserMessageStateBinding;)V", "getApplicationBinding", "()Lcom/impaktsoft/globeup/databinding/UserMessageStateBinding;", "setApplicationBinding", "(Lcom/impaktsoft/globeup/databinding/UserMessageStateBinding;)V", "bind", "", "item", "Lcom/impaktsoft/globeup/models/LastMessageState;", "app_debug"})
    public final class MessageStateViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private com.impaktsoft.globeup.databinding.UserMessageStateBinding applicationBinding;
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.models.LastMessageState item) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.impaktsoft.globeup.databinding.UserMessageStateBinding getApplicationBinding() {
            return null;
        }
        
        public final void setApplicationBinding(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.databinding.UserMessageStateBinding p0) {
        }
        
        public MessageStateViewHolder(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.databinding.UserMessageStateBinding applicationBinding) {
            super(null);
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fR\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\r"}, d2 = {"Lcom/impaktsoft/globeup/listadapters/MessagesAdapter$MessageTimeSectionViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "applicationBinding", "Lcom/impaktsoft/globeup/databinding/MessangerTimeSectionBinding;", "(Lcom/impaktsoft/globeup/listadapters/MessagesAdapter;Lcom/impaktsoft/globeup/databinding/MessangerTimeSectionBinding;)V", "getApplicationBinding", "()Lcom/impaktsoft/globeup/databinding/MessangerTimeSectionBinding;", "setApplicationBinding", "(Lcom/impaktsoft/globeup/databinding/MessangerTimeSectionBinding;)V", "bind", "", "item", "Lcom/impaktsoft/globeup/models/MessageTimeSection;", "app_debug"})
    public final class MessageTimeSectionViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private com.impaktsoft.globeup.databinding.MessangerTimeSectionBinding applicationBinding;
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.models.MessageTimeSection item) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.impaktsoft.globeup.databinding.MessangerTimeSectionBinding getApplicationBinding() {
            return null;
        }
        
        public final void setApplicationBinding(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.databinding.MessangerTimeSectionBinding p0) {
        }
        
        public MessageTimeSectionViewHolder(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.databinding.MessangerTimeSectionBinding applicationBinding) {
            super(null);
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\b\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\f"}, d2 = {"Lcom/impaktsoft/globeup/listadapters/MessagesAdapter$Companion;", "", "()V", "TYPE_MESSAGE_IMAGE_FRIEND", "", "TYPE_MESSAGE_IMAGE_USER", "TYPE_MESSAGE_LOCATION_FRIEND", "TYPE_MESSAGE_LOCATION_USER", "TYPE_MESSAGE_SECTION", "TYPE_MESSAGE_STATE", "TYPE_MESSAGE_TEXT_FRIEND", "TYPE_MESSAGE_TEXT_USER", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}