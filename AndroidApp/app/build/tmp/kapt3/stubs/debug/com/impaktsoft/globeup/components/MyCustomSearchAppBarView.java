package com.impaktsoft.globeup.components;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0006\u0010\u0011\u001a\u00020\u0012J\u0006\u0010\u0013\u001a\u00020\u0014J\u0006\u0010\u0015\u001a\u00020\u0014J\u0006\u0010\u0016\u001a\u00020\u0014J\u000e\u0010\u0017\u001a\u00020\u00142\u0006\u0010\u0018\u001a\u00020\u0019J\u000e\u0010\u001a\u001a\u00020\u00142\u0006\u0010\u001b\u001a\u00020\u001cJ\u000e\u0010\u001d\u001a\u00020\u00142\u0006\u0010\u001b\u001a\u00020\u001cJ\u000e\u0010\u001e\u001a\u00020\u00142\u0006\u0010\u001b\u001a\u00020\u001cJ\u000e\u0010\u001f\u001a\u00020\u00142\u0006\u0010\u001b\u001a\u00020\u001cJ\u000e\u0010 \u001a\u00020\u00142\u0006\u0010!\u001a\u00020\"J\u000e\u0010#\u001a\u00020\u00142\u0006\u0010\u001b\u001a\u00020\u001cJ\u000e\u0010$\u001a\u00020\u00142\u0006\u0010%\u001a\u00020\"J\u0006\u0010&\u001a\u00020\u0014R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\'"}, d2 = {"Lcom/impaktsoft/globeup/components/MyCustomSearchAppBarView;", "Landroid/widget/FrameLayout;", "context", "Landroid/content/Context;", "listener", "Lcom/impaktsoft/globeup/listeners/IGetStringListener;", "(Landroid/content/Context;Lcom/impaktsoft/globeup/listeners/IGetStringListener;)V", "backImageActionBar", "Landroid/widget/ImageView;", "searchEditText", "Landroid/widget/EditText;", "searchImageActionBar", "searchNameLayout", "Landroid/widget/LinearLayout;", "subTitleActionBar", "Landroid/widget/TextView;", "titleActionBar", "checkSearchOptionsVisibility", "", "hideActionBarName", "", "onStop", "removeSearchText", "setBackClickListener", "clickMethod", "Lcom/impaktsoft/globeup/listeners/IClickListener;", "setBackImageVisibility", "visibility", "", "setEditTextVisibility", "setLayoutNameVisibility", "setSearchImageVisibility", "setSubTitle", "subtitle", "", "setSubtitleVisibility", "setTitle", "title", "showActionBarName", "app_debug"})
public final class MyCustomSearchAppBarView extends android.widget.FrameLayout {
    private android.widget.LinearLayout searchNameLayout;
    private android.widget.EditText searchEditText;
    private android.widget.ImageView backImageActionBar;
    private android.widget.ImageView searchImageActionBar;
    private android.widget.TextView titleActionBar;
    private android.widget.TextView subTitleActionBar;
    private java.util.HashMap _$_findViewCache;
    
    public final void setBackClickListener(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.listeners.IClickListener clickMethod) {
    }
    
    public final void hideActionBarName() {
    }
    
    public final void showActionBarName() {
    }
    
    public final void removeSearchText() {
    }
    
    public final boolean checkSearchOptionsVisibility() {
        return false;
    }
    
    public final void setTitle(@org.jetbrains.annotations.NotNull()
    java.lang.String title) {
    }
    
    public final void setSubTitle(@org.jetbrains.annotations.NotNull()
    java.lang.String subtitle) {
    }
    
    public final void setEditTextVisibility(int visibility) {
    }
    
    public final void setLayoutNameVisibility(int visibility) {
    }
    
    public final void setSearchImageVisibility(int visibility) {
    }
    
    public final void setSubtitleVisibility(int visibility) {
    }
    
    public final void setBackImageVisibility(int visibility) {
    }
    
    public final void onStop() {
    }
    
    public MyCustomSearchAppBarView(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.listeners.IGetStringListener listener) {
        super(null);
    }
}