package com.impaktsoft.globeup.services;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH&R\u001a\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u00a6\u000e\u00a2\u0006\f\u001a\u0004\b\u0004\u0010\u0005\"\u0004\b\u0006\u0010\u0007\u00a8\u0006\f"}, d2 = {"Lcom/impaktsoft/globeup/services/ICurrentActivityService;", "", "activity", "Landroid/app/Activity;", "getActivity", "()Landroid/app/Activity;", "setActivity", "(Landroid/app/Activity;)V", "initWithApplication", "", "application", "Landroid/app/Application;", "app_debug"})
public abstract interface ICurrentActivityService {
    
    public abstract void initWithApplication(@org.jetbrains.annotations.NotNull()
    android.app.Application application);
    
    @org.jetbrains.annotations.Nullable()
    public abstract android.app.Activity getActivity();
    
    public abstract void setActivity(@org.jetbrains.annotations.Nullable()
    android.app.Activity p0);
}