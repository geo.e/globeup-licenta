package com.impaktsoft.globeup.services.database;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\f\bf\u0018\u00002\u00020\u0001J3\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u00052\u0006\u0010\u0006\u001a\u00020\u00072\b\u0010\b\u001a\u0004\u0018\u00010\u0007H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJQ\u0010\n\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u00052\u0016\u0010\u000b\u001a\u0012\u0012\u0004\u0012\u00020\f0\u0003j\b\u0012\u0004\u0012\u00020\f`\u00052\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00072\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u000eH\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000fJ=\u0010\u0010\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u00052\u001a\u0010\u0011\u001a\u0016\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0003j\n\u0012\u0004\u0012\u00020\u0007\u0018\u0001`\u0005H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0012J5\u0010\u0013\u001a\u0012\u0012\u0004\u0012\u00020\u00140\u0003j\b\u0012\u0004\u0012\u00020\u0014`\u00052\u0006\u0010\u0015\u001a\u00020\u00072\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0018J\u001b\u0010\u0019\u001a\u0004\u0018\u00010\u001a2\u0006\u0010\u001b\u001a\u00020\u0007H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u001cJ!\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u00072\u0006\u0010 \u001a\u00020!H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\"J!\u0010#\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u00072\u0006\u0010 \u001a\u00020!H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\"J\u0019\u0010$\u001a\u00020%2\u0006\u0010\u0015\u001a\u00020\u0007H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u001cJ!\u0010&\u001a\u00020\u001e2\u0006\u0010\u0015\u001a\u00020\u00072\u0006\u0010\u001b\u001a\u00020\u0007H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJ\b\u0010\'\u001a\u00020%H&J!\u0010(\u001a\u00020%2\u0006\u0010)\u001a\u00020\u00072\u0006\u0010*\u001a\u00020\u0004H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010+J!\u0010,\u001a\u00020\u001e2\u0006\u0010\u0015\u001a\u00020\u00072\u0006\u0010*\u001a\u00020\u0014H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010-J\u0019\u0010.\u001a\u00020%2\u0006\u0010/\u001a\u00020\u0004H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u00100\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u00061"}, d2 = {"Lcom/impaktsoft/globeup/services/database/IEventsDAO;", "", "fetchEventByName", "Ljava/util/ArrayList;", "Lcom/impaktsoft/globeup/models/EventPOJO;", "Lkotlin/collections/ArrayList;", "name", "", "country", "(Ljava/lang/String;Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "fetchEvents", "filtersList", "Lcom/impaktsoft/globeup/models/IFilterItem;", "scaleType", "Lcom/impaktsoft/globeup/enums/ScaleTypeEnum;", "(Ljava/util/ArrayList;Ljava/lang/String;Lcom/impaktsoft/globeup/enums/ScaleTypeEnum;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "fetchEventsByKey", "listOfKeys", "(Ljava/util/ArrayList;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "fetchUserEvents", "Lcom/impaktsoft/globeup/models/UserEventPOJO;", "userID", "dataSource", "Lcom/google/firebase/firestore/Source;", "(Ljava/lang/String;Lcom/google/firebase/firestore/Source;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getEventMembersCount", "", "eventId", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "joinEvent", "", "eventKey", "eventMember", "Lcom/impaktsoft/globeup/models/EventMembersPOJO;", "(Ljava/lang/String;Lcom/impaktsoft/globeup/models/EventMembersPOJO;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "leaveEvent", "removeAllUserEvents", "", "removeUserEvent", "resetLastEventDocument", "storeEvent", "backendKey", "event", "(Ljava/lang/String;Lcom/impaktsoft/globeup/models/EventPOJO;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "storeUserEvent", "(Ljava/lang/String;Lcom/impaktsoft/globeup/models/UserEventPOJO;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "updateEvent", "eventItem", "(Lcom/impaktsoft/globeup/models/EventPOJO;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public abstract interface IEventsDAO {
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object storeEvent(@org.jetbrains.annotations.NotNull()
    java.lang.String backendKey, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.EventPOJO event, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p2);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object storeUserEvent(@org.jetbrains.annotations.NotNull()
    java.lang.String userID, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.UserEventPOJO event, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.lang.Boolean> p2);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object fetchEventsByKey(@org.jetbrains.annotations.Nullable()
    java.util.ArrayList<java.lang.String> listOfKeys, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.ArrayList<com.impaktsoft.globeup.models.EventPOJO>> p1);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object fetchUserEvents(@org.jetbrains.annotations.NotNull()
    java.lang.String userID, @org.jetbrains.annotations.Nullable()
    com.google.firebase.firestore.Source dataSource, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.ArrayList<com.impaktsoft.globeup.models.UserEventPOJO>> p2);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object fetchEvents(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.impaktsoft.globeup.models.IFilterItem> filtersList, @org.jetbrains.annotations.Nullable()
    java.lang.String country, @org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.enums.ScaleTypeEnum scaleType, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.ArrayList<com.impaktsoft.globeup.models.EventPOJO>> p3);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object fetchEventByName(@org.jetbrains.annotations.NotNull()
    java.lang.String name, @org.jetbrains.annotations.Nullable()
    java.lang.String country, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.ArrayList<com.impaktsoft.globeup.models.EventPOJO>> p2);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object removeUserEvent(@org.jetbrains.annotations.NotNull()
    java.lang.String userID, @org.jetbrains.annotations.NotNull()
    java.lang.String eventId, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.lang.Boolean> p2);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object updateEvent(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.EventPOJO eventItem, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p1);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object joinEvent(@org.jetbrains.annotations.NotNull()
    java.lang.String eventKey, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.EventMembersPOJO eventMember, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.lang.Boolean> p2);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object leaveEvent(@org.jetbrains.annotations.NotNull()
    java.lang.String eventKey, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.EventMembersPOJO eventMember, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.lang.Boolean> p2);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object getEventMembersCount(@org.jetbrains.annotations.NotNull()
    java.lang.String eventId, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.lang.Integer> p1);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object removeAllUserEvents(@org.jetbrains.annotations.NotNull()
    java.lang.String userID, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p1);
    
    public abstract void resetLastEventDocument();
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 3)
    public final class DefaultImpls {
    }
}