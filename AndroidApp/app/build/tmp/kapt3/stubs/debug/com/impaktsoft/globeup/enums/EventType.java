package com.impaktsoft.globeup.enums;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0007\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007\u00a8\u0006\b"}, d2 = {"Lcom/impaktsoft/globeup/enums/EventType;", "", "(Ljava/lang/String;I)V", "GarbageCollect", "Recycling", "HelpPeople", "PlantTrees", "Unknown", "app_debug"})
public enum EventType {
    /*public static final*/ GarbageCollect /* = new GarbageCollect() */,
    /*public static final*/ Recycling /* = new Recycling() */,
    /*public static final*/ HelpPeople /* = new HelpPeople() */,
    /*public static final*/ PlantTrees /* = new PlantTrees() */,
    /*public static final*/ Unknown /* = new Unknown() */;
    
    EventType() {
    }
}