package com.impaktsoft.globeup.viewmodels;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0012\u0018\u0000 *2\u00020\u0001:\u0001*B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010!\u001a\u00020\u0019J\u000e\u0010\"\u001a\u00020\u00192\u0006\u0010#\u001a\u00020\u000bJ\b\u0010$\u001a\u00020\u0019H\u0002J\b\u0010%\u001a\u00020\u0019H\u0016J\u0006\u0010&\u001a\u00020\u0019J\u000e\u0010\'\u001a\u00020\u00192\u0006\u0010(\u001a\u00020\u0016J\b\u0010)\u001a\u00020\u0019H\u0002R \u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR \u0010\n\u001a\b\u0012\u0004\u0012\u00020\u000b0\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\u0007\"\u0004\b\r\u0010\tR\u001c\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013R\u0014\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00160\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\"\u0010\u0017\u001a\n\u0012\u0004\u0012\u00020\u0019\u0018\u00010\u0018X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\u001b\"\u0004\b\u001c\u0010\u001dR \u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001e\u0010\u0007\"\u0004\b\u001f\u0010\tR\u0010\u0010 \u001a\u0004\u0018\u00010\u0016X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006+"}, d2 = {"Lcom/impaktsoft/globeup/viewmodels/SelectCountryViewModel;", "Lcom/impaktsoft/globeup/viewmodels/BaseViewModel;", "()V", "blockActionIfCountryNotSelected", "Landroidx/lifecycle/MutableLiveData;", "", "getBlockActionIfCountryNotSelected", "()Landroidx/lifecycle/MutableLiveData;", "setBlockActionIfCountryNotSelected", "(Landroidx/lifecycle/MutableLiveData;)V", "buttonText", "", "getButtonText", "setButtonText", "countryAdapter", "Lcom/impaktsoft/globeup/listadapters/CountryAdapter;", "getCountryAdapter", "()Lcom/impaktsoft/globeup/listadapters/CountryAdapter;", "setCountryAdapter", "(Lcom/impaktsoft/globeup/listadapters/CountryAdapter;)V", "countryList", "Ljava/util/ArrayList;", "Lcom/impaktsoft/globeup/models/CountryPOJO;", "currentAction", "Lkotlin/Function0;", "", "getCurrentAction", "()Lkotlin/jvm/functions/Function0;", "setCurrentAction", "(Lkotlin/jvm/functions/Function0;)V", "isCloseButtonVisible", "setCloseButtonVisible", "selectedCountry", "close", "filterList", "searchString", "initCountryList", "onResume", "saveSelectedCountry", "selectCountry", "countryPOJO", "unselectPreviousCountry", "Companion", "app_debug"})
public final class SelectCountryViewModel extends com.impaktsoft.globeup.viewmodels.BaseViewModel {
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.listadapters.CountryAdapter countryAdapter;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> buttonText;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> blockActionIfCountryNotSelected;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isCloseButtonVisible;
    @org.jetbrains.annotations.Nullable()
    private kotlin.jvm.functions.Function0<kotlin.Unit> currentAction;
    private java.util.ArrayList<com.impaktsoft.globeup.models.CountryPOJO> countryList;
    private com.impaktsoft.globeup.models.CountryPOJO selectedCountry;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String countryKey = "countryKey";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String currentCountryKey = "currentCountryKey";
    public static final com.impaktsoft.globeup.viewmodels.SelectCountryViewModel.Companion Companion = null;
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.listadapters.CountryAdapter getCountryAdapter() {
        return null;
    }
    
    public final void setCountryAdapter(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.listadapters.CountryAdapter p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getButtonText() {
        return null;
    }
    
    public final void setButtonText(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getBlockActionIfCountryNotSelected() {
        return null;
    }
    
    public final void setBlockActionIfCountryNotSelected(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isCloseButtonVisible() {
        return null;
    }
    
    public final void setCloseButtonVisible(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final kotlin.jvm.functions.Function0<kotlin.Unit> getCurrentAction() {
        return null;
    }
    
    public final void setCurrentAction(@org.jetbrains.annotations.Nullable()
    kotlin.jvm.functions.Function0<kotlin.Unit> p0) {
    }
    
    @java.lang.Override()
    public void onResume() {
    }
    
    private final void initCountryList() {
    }
    
    public final void close() {
    }
    
    public final void saveSelectedCountry() {
    }
    
    public final void selectCountry(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.CountryPOJO countryPOJO) {
    }
    
    private final void unselectPreviousCountry() {
    }
    
    public final void filterList(@org.jetbrains.annotations.NotNull()
    java.lang.String searchString) {
    }
    
    public SelectCountryViewModel() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lcom/impaktsoft/globeup/viewmodels/SelectCountryViewModel$Companion;", "", "()V", "countryKey", "", "currentCountryKey", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}