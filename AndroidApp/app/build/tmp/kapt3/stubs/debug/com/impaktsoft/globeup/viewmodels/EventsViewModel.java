package com.impaktsoft.globeup.viewmodels;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0011\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0011\u0010#\u001a\u00020$H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010%J \u0010&\u001a\u00020$2\u0016\u0010\'\u001a\u0012\u0012\u0004\u0012\u00020(0\u0013j\b\u0012\u0004\u0012\u00020(`)H\u0002J\u0006\u0010*\u001a\u00020$J\b\u0010+\u001a\u00020$H\u0002J\u0017\u0010,\u001a\b\u0012\u0004\u0012\u00020\u00160-H\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010%J\b\u0010.\u001a\u00020$H\u0002J\b\u0010/\u001a\u00020$H\u0002J\b\u00100\u001a\u00020$H\u0002J\b\u00101\u001a\u00020$H\u0002J\u0006\u00102\u001a\u00020$J\b\u00103\u001a\u00020$H\u0002J\u001d\u00104\u001a\u00020$2\n\b\u0002\u00105\u001a\u0004\u0018\u000106H\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u00107J\u0006\u00108\u001a\u00020$J\u0006\u00109\u001a\u00020$J\u0011\u0010:\u001a\u00020$H\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010%J\u0011\u0010;\u001a\u00020$H\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010%J\b\u0010<\u001a\u00020$H\u0002J\u0006\u0010=\u001a\u00020$J\b\u0010>\u001a\u00020$H\u0016J\b\u0010?\u001a\u00020$H\u0016J\u000e\u0010@\u001a\u00020$2\u0006\u0010A\u001a\u00020\u0014J\u0011\u0010B\u001a\u00020$\"\u0006\b\u0000\u0010C\u0018\u0001H\u0082\bJ\b\u0010D\u001a\u00020$H\u0002J\u0006\u0010E\u001a\u00020$J\b\u0010F\u001a\u00020$H\u0002R \u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\u000e\u0010\n\u001a\u00020\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\f\u001a\u0004\u0018\u00010\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u0014\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00140\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0015\u001a\u00020\u0016X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0017\"\u0004\b\u0018\u0010\u0019R \u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00160\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\u0007\"\u0004\b\u001b\u0010\tR \u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00160\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u0007\"\u0004\b\u001d\u0010\tR\u0010\u0010\u001e\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R \u0010\u001f\u001a\b\u0012\u0004\u0012\u00020 0\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b!\u0010\u0007\"\u0004\b\"\u0010\t\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006G"}, d2 = {"Lcom/impaktsoft/globeup/viewmodels/EventsViewModel;", "Lcom/impaktsoft/globeup/viewmodels/BaseEventsViewModel;", "()V", "currentCountryBinding", "Landroidx/lifecycle/MutableLiveData;", "", "getCurrentCountryBinding", "()Landroidx/lifecycle/MutableLiveData;", "setCurrentCountryBinding", "(Landroidx/lifecycle/MutableLiveData;)V", "currentCountryCode", "currentUserId", "filterAppliedAdapter", "Lcom/impaktsoft/globeup/listadapters/FilterAppliedAdapter;", "getFilterAppliedAdapter", "()Lcom/impaktsoft/globeup/listadapters/FilterAppliedAdapter;", "setFilterAppliedAdapter", "(Lcom/impaktsoft/globeup/listadapters/FilterAppliedAdapter;)V", "filterList", "Ljava/util/ArrayList;", "Lcom/impaktsoft/globeup/models/IFilterItem;", "isFetchMoreEventsEnable", "", "()Z", "setFetchMoreEventsEnable", "(Z)V", "isPlaceHolderMessageVisible", "setPlaceHolderMessageVisible", "isProgressBarVisible", "setProgressBarVisible", "lastVMNameVisited", "loadingPosition", "", "getLoadingPosition", "setLoadingPosition", "add100Events", "", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "addEvents", "eventsList", "Lcom/impaktsoft/globeup/models/EventPOJO;", "Lkotlin/collections/ArrayList;", "addNewEventClicked", "applyFilterEvents", "askLocationPermission", "Lcom/google/android/gms/tasks/Task;", "checkCountryHasChanged", "checkRadiusFilter", "checkTypeAndTimeFilter", "checkUserEventsForUpdates", "chooseCountryClick", "clearEventsFromDBLists", "fetchEvents", "scaleType", "Lcom/impaktsoft/globeup/enums/ScaleTypeEnum;", "(Lcom/impaktsoft/globeup/enums/ScaleTypeEnum;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "filterEventsByRadiusClicked", "filterEventsByTypeOrTimeClicked", "initCurrentUserInfo", "initEventsList", "initListAdapters", "loadMoreEvents", "onDestroy", "onResume", "removeFilter", "eventTypeFilterPOJO", "removeFilterType", "T", "removeLoadingElementFromList", "searchByNameClicked", "showRegisterAlert", "app_debug"})
public final class EventsViewModel extends com.impaktsoft.globeup.viewmodels.BaseEventsViewModel {
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.listadapters.FilterAppliedAdapter filterAppliedAdapter;
    private boolean isFetchMoreEventsEnable;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Integer> loadingPosition;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> currentCountryBinding;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isProgressBarVisible;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isPlaceHolderMessageVisible;
    private java.lang.String lastVMNameVisited;
    private java.util.ArrayList<com.impaktsoft.globeup.models.IFilterItem> filterList;
    private java.lang.String currentUserId;
    private java.lang.String currentCountryCode;
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.listadapters.FilterAppliedAdapter getFilterAppliedAdapter() {
        return null;
    }
    
    public final void setFilterAppliedAdapter(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.listadapters.FilterAppliedAdapter p0) {
    }
    
    public final boolean isFetchMoreEventsEnable() {
        return false;
    }
    
    public final void setFetchMoreEventsEnable(boolean p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Integer> getLoadingPosition() {
        return null;
    }
    
    public final void setLoadingPosition(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Integer> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getCurrentCountryBinding() {
        return null;
    }
    
    public final void setCurrentCountryBinding(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isProgressBarVisible() {
        return null;
    }
    
    public final void setProgressBarVisible(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isPlaceHolderMessageVisible() {
        return null;
    }
    
    public final void setPlaceHolderMessageVisible(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @java.lang.Override()
    public void onResume() {
    }
    
    @java.lang.Override()
    public void onDestroy() {
    }
    
    public final void loadMoreEvents() {
    }
    
    public final void addNewEventClicked() {
    }
    
    public final void searchByNameClicked() {
    }
    
    public final void filterEventsByTypeOrTimeClicked() {
    }
    
    public final void filterEventsByRadiusClicked() {
    }
    
    public final void removeFilter(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.IFilterItem eventTypeFilterPOJO) {
    }
    
    public final void chooseCountryClick() {
    }
    
    private final void initListAdapters() {
    }
    
    private final void checkUserEventsForUpdates() {
    }
    
    private final void checkCountryHasChanged() {
    }
    
    private final void checkRadiusFilter() {
    }
    
    private final void checkTypeAndTimeFilter() {
    }
    
    private final void clearEventsFromDBLists() {
    }
    
    private final void applyFilterEvents() {
    }
    
    private final void removeLoadingElementFromList() {
    }
    
    private final void addEvents(java.util.ArrayList<com.impaktsoft.globeup.models.EventPOJO> eventsList) {
    }
    
    private final void showRegisterAlert() {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object add100Events(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p0) {
        return null;
    }
    
    public EventsViewModel() {
        super();
    }
}