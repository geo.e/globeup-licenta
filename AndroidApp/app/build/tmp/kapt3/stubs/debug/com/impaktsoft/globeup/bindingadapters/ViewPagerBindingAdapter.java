package com.impaktsoft.globeup.bindingadapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u001a\u0010\u0003\u001a\u00020\u0004*\u00020\u00052\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007H\u0007J$\u0010\t\u001a\u00020\u0004*\u00020\u00052\u0016\u0010\n\u001a\u0012\u0012\u0004\u0012\u00020\b0\u000bj\b\u0012\u0004\u0012\u00020\b`\fH\u0007J\u0014\u0010\r\u001a\u00020\u0004*\u00020\u00052\u0006\u0010\u000e\u001a\u00020\bH\u0007\u00a8\u0006\u000f"}, d2 = {"Lcom/impaktsoft/globeup/bindingadapters/ViewPagerBindingAdapter;", "", "()V", "getCurrentItem", "", "Landroidx/viewpager/widget/ViewPager;", "currentItem", "Landroidx/lifecycle/MutableLiveData;", "", "setAdapter", "images", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "swipeTo", "position", "app_debug"})
public final class ViewPagerBindingAdapter {
    public static final com.impaktsoft.globeup.bindingadapters.ViewPagerBindingAdapter INSTANCE = null;
    
    @androidx.databinding.BindingAdapter(value = {"cb_swiperImages"})
    public static final void setAdapter(@org.jetbrains.annotations.NotNull()
    androidx.viewpager.widget.ViewPager $this$setAdapter, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<java.lang.Integer> images) {
    }
    
    @androidx.databinding.BindingAdapter(value = {"cb_swipeToPosition"})
    public static final void swipeTo(@org.jetbrains.annotations.NotNull()
    androidx.viewpager.widget.ViewPager $this$swipeTo, int position) {
    }
    
    @androidx.databinding.BindingAdapter(value = {"cb_swiperCurrentImage"})
    public static final void getCurrentItem(@org.jetbrains.annotations.NotNull()
    androidx.viewpager.widget.ViewPager $this$getCurrentItem, @org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Integer> currentItem) {
    }
    
    private ViewPagerBindingAdapter() {
        super();
    }
}