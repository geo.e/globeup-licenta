package com.impaktsoft.globeup.bindingadapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0007J\u0010\u0010\u0007\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\tH\u0007J\u0010\u0010\u0007\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\u000bH\u0007J\u0010\u0010\f\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\u000eH\u0007J\u0010\u0010\u000f\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\tH\u0007J\u0010\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u0012H\u0007\u00a8\u0006\u0013"}, d2 = {"Lcom/impaktsoft/globeup/bindingadapters/BindingConvert;", "", "()V", "isHoster", "", "eventState", "Lcom/impaktsoft/globeup/enums/EventState;", "isOnline", "conversationPOJO", "Lcom/impaktsoft/globeup/models/ConversationPOJO;", "userPOJO", "Lcom/impaktsoft/globeup/models/UserPOJO;", "isSelected", "contactState", "Lcom/impaktsoft/globeup/enums/ContactState;", "messageIsRead", "notificationIsShowed", "number", "", "app_debug"})
public final class BindingConvert {
    public static final com.impaktsoft.globeup.bindingadapters.BindingConvert INSTANCE = null;
    
    public static final boolean isHoster(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.enums.EventState eventState) {
        return false;
    }
    
    public static final boolean isOnline(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.ConversationPOJO conversationPOJO) {
        return false;
    }
    
    public static final boolean isOnline(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.UserPOJO userPOJO) {
        return false;
    }
    
    public static final boolean isSelected(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.enums.ContactState contactState) {
        return false;
    }
    
    public static final boolean messageIsRead(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.ConversationPOJO conversationPOJO) {
        return false;
    }
    
    public static final boolean notificationIsShowed(int number) {
        return false;
    }
    
    private BindingConvert() {
        super();
    }
}