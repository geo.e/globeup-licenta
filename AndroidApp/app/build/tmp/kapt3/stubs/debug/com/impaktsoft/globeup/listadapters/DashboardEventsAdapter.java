package com.impaktsoft.globeup.listadapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 \u00142\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0003\u0014\u0015\u0016B\u001b\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\b\u0010\t\u001a\u00020\nH\u0016J\u0010\u0010\u000b\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\nH\u0016J\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00022\u0006\u0010\f\u001a\u00020\nH\u0016J\u0018\u0010\u0010\u001a\u00020\u00022\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\nH\u0016R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"}, d2 = {"Lcom/impaktsoft/globeup/listadapters/DashboardEventsAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "arrayList", "", "Lcom/impaktsoft/globeup/models/IEventsItem;", "viewModel", "Lcom/impaktsoft/globeup/viewmodels/DashboardViewModel;", "(Ljava/util/List;Lcom/impaktsoft/globeup/viewmodels/DashboardViewModel;)V", "getItemCount", "", "getItemViewType", "position", "onBindViewHolder", "", "holder", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "Companion", "EventSectionViewHolder", "EventViewHolder", "app_debug"})
public final class DashboardEventsAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder> {
    private java.util.List<? extends com.impaktsoft.globeup.models.IEventsItem> arrayList;
    private final com.impaktsoft.globeup.viewmodels.DashboardViewModel viewModel = null;
    private static final int EventSection = 0;
    private static final int Event = 1;
    public static final com.impaktsoft.globeup.listadapters.DashboardEventsAdapter.Companion Companion = null;
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public int getItemViewType(int position) {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public androidx.recyclerview.widget.RecyclerView.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.RecyclerView.ViewHolder holder, int position) {
    }
    
    public DashboardEventsAdapter(@org.jetbrains.annotations.NotNull()
    java.util.List<? extends com.impaktsoft.globeup.models.IEventsItem> arrayList, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.viewmodels.DashboardViewModel viewModel) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fR\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\r"}, d2 = {"Lcom/impaktsoft/globeup/listadapters/DashboardEventsAdapter$EventSectionViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "applicationBinding", "Lcom/impaktsoft/globeup/databinding/EventCellSectionBinding;", "(Lcom/impaktsoft/globeup/listadapters/DashboardEventsAdapter;Lcom/impaktsoft/globeup/databinding/EventCellSectionBinding;)V", "getApplicationBinding", "()Lcom/impaktsoft/globeup/databinding/EventCellSectionBinding;", "setApplicationBinding", "(Lcom/impaktsoft/globeup/databinding/EventCellSectionBinding;)V", "bind", "", "item", "Lcom/impaktsoft/globeup/models/EventSectionPOJO;", "app_debug"})
    public final class EventSectionViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private com.impaktsoft.globeup.databinding.EventCellSectionBinding applicationBinding;
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.models.EventSectionPOJO item) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.impaktsoft.globeup.databinding.EventCellSectionBinding getApplicationBinding() {
            return null;
        }
        
        public final void setApplicationBinding(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.databinding.EventCellSectionBinding p0) {
        }
        
        public EventSectionViewHolder(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.databinding.EventCellSectionBinding applicationBinding) {
            super(null);
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fR\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\r"}, d2 = {"Lcom/impaktsoft/globeup/listadapters/DashboardEventsAdapter$EventViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "applicationBinding", "Lcom/impaktsoft/globeup/databinding/DashboardEventCellBinding;", "(Lcom/impaktsoft/globeup/listadapters/DashboardEventsAdapter;Lcom/impaktsoft/globeup/databinding/DashboardEventCellBinding;)V", "getApplicationBinding", "()Lcom/impaktsoft/globeup/databinding/DashboardEventCellBinding;", "setApplicationBinding", "(Lcom/impaktsoft/globeup/databinding/DashboardEventCellBinding;)V", "bind", "", "item", "Lcom/impaktsoft/globeup/models/EventPOJO;", "app_debug"})
    public final class EventViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private com.impaktsoft.globeup.databinding.DashboardEventCellBinding applicationBinding;
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.models.EventPOJO item) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.impaktsoft.globeup.databinding.DashboardEventCellBinding getApplicationBinding() {
            return null;
        }
        
        public final void setApplicationBinding(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.databinding.DashboardEventCellBinding p0) {
        }
        
        public EventViewHolder(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.databinding.DashboardEventCellBinding applicationBinding) {
            super(null);
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lcom/impaktsoft/globeup/listadapters/DashboardEventsAdapter$Companion;", "", "()V", "Event", "", "EventSection", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}