package com.impaktsoft.globeup.bindingadapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\u00020\u0004*\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0007J\u0014\u0010\b\u001a\u00020\u0004*\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0007J\u0014\u0010\f\u001a\u00020\u0004*\u00020\u00052\u0006\u0010\f\u001a\u00020\u000bH\u0007J\u0014\u0010\r\u001a\u00020\u0004*\u00020\t2\u0006\u0010\u000e\u001a\u00020\u000fH\u0007J\u0014\u0010\u0010\u001a\u00020\u0004*\u00020\t2\u0006\u0010\u0006\u001a\u00020\u0011H\u0007J\u0014\u0010\u0012\u001a\u00020\u0004*\u00020\t2\u0006\u0010\u0006\u001a\u00020\u0013H\u0007\u00a8\u0006\u0014"}, d2 = {"Lcom/impaktsoft/globeup/bindingadapters/RecyclerViewBindingAdapter;", "", "()V", "OnRefresh", "", "Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;", "viewModel", "Lcom/impaktsoft/globeup/viewmodels/MessengerViewModel;", "clearPool", "Landroidx/recyclerview/widget/RecyclerView;", "isClearAllow", "", "isRefreshing", "scrollToPosition", "position", "", "setScrollMethod", "Lcom/impaktsoft/globeup/viewmodels/EventsViewModel;", "setScrollMethodInFilterByName", "Lcom/impaktsoft/globeup/viewmodels/SearchEventsByNameViewModel;", "app_debug"})
public final class RecyclerViewBindingAdapter {
    public static final com.impaktsoft.globeup.bindingadapters.RecyclerViewBindingAdapter INSTANCE = null;
    
    @androidx.databinding.BindingAdapter(value = {"cb_scrollToAddEvents"})
    public static final void setScrollMethod(@org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.RecyclerView $this$setScrollMethod, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.viewmodels.EventsViewModel viewModel) {
    }
    
    @androidx.databinding.BindingAdapter(value = {"cb_scrollToFilterEvents"})
    public static final void setScrollMethodInFilterByName(@org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.RecyclerView $this$setScrollMethodInFilterByName, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.viewmodels.SearchEventsByNameViewModel viewModel) {
    }
    
    @androidx.databinding.BindingAdapter(value = {"cb_onRefresh"})
    public static final void OnRefresh(@org.jetbrains.annotations.NotNull()
    androidx.swiperefreshlayout.widget.SwipeRefreshLayout $this$OnRefresh, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.viewmodels.MessengerViewModel viewModel) {
    }
    
    @androidx.databinding.BindingAdapter(value = {"cb_isRefreshing"})
    public static final void isRefreshing(@org.jetbrains.annotations.NotNull()
    androidx.swiperefreshlayout.widget.SwipeRefreshLayout $this$isRefreshing, boolean isRefreshing) {
    }
    
    @androidx.databinding.BindingAdapter(value = {"cb_scrollToPosition"})
    public static final void scrollToPosition(@org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.RecyclerView $this$scrollToPosition, int position) {
    }
    
    @androidx.databinding.BindingAdapter(value = {"cb_clearPool"})
    public static final void clearPool(@org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.RecyclerView $this$clearPool, boolean isClearAllow) {
    }
    
    private RecyclerViewBindingAdapter() {
        super();
    }
}