package com.impaktsoft.globeup.services;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0004H\u0016J\b\u0010\b\u001a\u00020\u0006H\u0016R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lcom/impaktsoft/globeup/services/OnBackPressService;", "Lcom/impaktsoft/globeup/services/IOnBackPressService;", "()V", "onBackPressListener", "Lcom/impaktsoft/globeup/listeners/IOnBackPressedListener;", "initOnBackPressListener", "", "listener", "onBackPressed", "app_debug"})
public final class OnBackPressService implements com.impaktsoft.globeup.services.IOnBackPressService {
    private com.impaktsoft.globeup.listeners.IOnBackPressedListener onBackPressListener;
    
    @java.lang.Override()
    public void initOnBackPressListener(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.listeners.IOnBackPressedListener listener) {
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    public OnBackPressService() {
        super();
    }
}