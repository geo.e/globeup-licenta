package com.impaktsoft.globeup.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0013\u0018\u00002\u00020\u0001B9\b\u0016\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\tR\u001e\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0096\u000e\u00a2\u0006\u0010\n\u0002\u0010\u000e\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\rR\u001e\u0010\u0004\u001a\u0004\u0018\u00010\u0003X\u0096\u000e\u00a2\u0006\u0010\n\u0002\u0010\u000e\u001a\u0004\b\u000f\u0010\u000b\"\u0004\b\u0010\u0010\rR\u001e\u0010\u0007\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u000e\u001a\u0004\b\u0011\u0010\u000b\"\u0004\b\u0012\u0010\rR\u001c\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u001e\u0010\b\u001a\u0004\u0018\u00010\u0003X\u0096\u000e\u00a2\u0006\u0010\n\u0002\u0010\u000e\u001a\u0004\b\u0017\u0010\u000b\"\u0004\b\u0018\u0010\r\u00a8\u0006\u0019"}, d2 = {"Lcom/impaktsoft/globeup/models/GlobalGroupPOJO;", "Lcom/impaktsoft/globeup/models/IGlobalBaseItem;", "events", "", "experience", "name", "", "members", "rank", "(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V", "getEvents", "()Ljava/lang/Integer;", "setEvents", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "getExperience", "setExperience", "getMembers", "setMembers", "getName", "()Ljava/lang/String;", "setName", "(Ljava/lang/String;)V", "getRank", "setRank", "app_debug"})
public final class GlobalGroupPOJO implements com.impaktsoft.globeup.models.IGlobalBaseItem {
    @org.jetbrains.annotations.Nullable()
    private java.lang.Integer rank;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Integer events;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Integer experience;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String name;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Integer members;
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Integer getRank() {
        return null;
    }
    
    @java.lang.Override()
    public void setRank(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Integer getEvents() {
        return null;
    }
    
    @java.lang.Override()
    public void setEvents(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Integer getExperience() {
        return null;
    }
    
    @java.lang.Override()
    public void setExperience(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.String getName() {
        return null;
    }
    
    @java.lang.Override()
    public void setName(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getMembers() {
        return null;
    }
    
    public final void setMembers(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    public GlobalGroupPOJO(@org.jetbrains.annotations.Nullable()
    java.lang.Integer events, @org.jetbrains.annotations.Nullable()
    java.lang.Integer experience, @org.jetbrains.annotations.Nullable()
    java.lang.String name, @org.jetbrains.annotations.Nullable()
    java.lang.Integer members, @org.jetbrains.annotations.Nullable()
    java.lang.Integer rank) {
        super();
    }
}