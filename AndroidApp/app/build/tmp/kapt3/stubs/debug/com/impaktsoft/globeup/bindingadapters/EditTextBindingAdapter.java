package com.impaktsoft.globeup.bindingadapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\u00020\u0004*\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0007J\u0014\u0010\b\u001a\u00020\u0004*\u00020\u00052\u0006\u0010\u0006\u001a\u00020\tH\u0007\u00a8\u0006\n"}, d2 = {"Lcom/impaktsoft/globeup/bindingadapters/EditTextBindingAdapter;", "", "()V", "getWithdraw", "", "Landroid/widget/EditText;", "viewModel", "Lcom/impaktsoft/globeup/viewmodels/WithdrawViewModel;", "searchByName", "Lcom/impaktsoft/globeup/viewmodels/SearchEventsByNameViewModel;", "app_debug"})
public final class EditTextBindingAdapter {
    public static final com.impaktsoft.globeup.bindingadapters.EditTextBindingAdapter INSTANCE = null;
    
    @androidx.databinding.BindingAdapter(value = {"cb_withdrawText"})
    public static final void getWithdraw(@org.jetbrains.annotations.NotNull()
    android.widget.EditText $this$getWithdraw, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.viewmodels.WithdrawViewModel viewModel) {
    }
    
    @android.annotation.TargetApi(value = android.os.Build.VERSION_CODES.M)
    @androidx.databinding.BindingAdapter(value = {"cb_searchByName"})
    public static final void searchByName(@org.jetbrains.annotations.NotNull()
    android.widget.EditText $this$searchByName, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.viewmodels.SearchEventsByNameViewModel viewModel) {
    }
    
    private EditTextBindingAdapter() {
        super();
    }
}