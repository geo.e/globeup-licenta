package com.impaktsoft.globeup.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u000b\u0018\u00002\u00020\u0001B\u001b\b\u0016\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0005B\u0007\b\u0016\u00a2\u0006\u0002\u0010\u0006R\u001e\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u000b\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001e\u0010\u0004\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u000b\u001a\u0004\b\f\u0010\b\"\u0004\b\r\u0010\n\u00a8\u0006\u000e"}, d2 = {"Lcom/impaktsoft/globeup/models/TimeSpanPOJO;", "", "hour", "", "minute", "(Ljava/lang/Integer;Ljava/lang/Integer;)V", "()V", "getHour", "()Ljava/lang/Integer;", "setHour", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "getMinute", "setMinute", "app_debug"})
public final class TimeSpanPOJO {
    @org.jetbrains.annotations.Nullable()
    private java.lang.Integer hour;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Integer minute;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getHour() {
        return null;
    }
    
    public final void setHour(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getMinute() {
        return null;
    }
    
    public final void setMinute(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    public TimeSpanPOJO(@org.jetbrains.annotations.Nullable()
    java.lang.Integer hour, @org.jetbrains.annotations.Nullable()
    java.lang.Integer minute) {
        super();
    }
    
    public TimeSpanPOJO() {
        super();
    }
}