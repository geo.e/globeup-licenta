package com.impaktsoft.globeup.listadapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u001e\u001fB%\u0012\u0016\u0010\u0003\u001a\u0012\u0012\u0004\u0012\u00020\u00050\u0004j\b\u0012\u0004\u0012\u00020\u0005`\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\tJ\b\u0010\u0011\u001a\u00020\u000bH\u0016J\u0010\u0010\u0012\u001a\u00020\u000b2\u0006\u0010\u0013\u001a\u00020\u000bH\u0016J\u0018\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u000bH\u0016J\u0018\u0010\u0017\u001a\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u000bH\u0016J\u001e\u0010\u001b\u001a\u00020\u00152\u0016\u0010\u001c\u001a\u0012\u0012\u0004\u0012\u00020\u001d0\u0004j\b\u0012\u0004\u0012\u00020\u001d`\u0006R\u000e\u0010\n\u001a\u00020\u000bX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u000bX\u0082D\u00a2\u0006\u0002\n\u0000R*\u0010\u0003\u001a\u0012\u0012\u0004\u0012\u00020\u00050\u0004j\b\u0012\u0004\u0012\u00020\u0005`\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "}, d2 = {"Lcom/impaktsoft/globeup/listadapters/SearchByNameEventsAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "eventsList", "Ljava/util/ArrayList;", "Lcom/impaktsoft/globeup/models/IEventsItem;", "Lkotlin/collections/ArrayList;", "viewModel", "Lcom/impaktsoft/globeup/viewmodels/BaseEventsViewModel;", "(Ljava/util/ArrayList;Lcom/impaktsoft/globeup/viewmodels/BaseEventsViewModel;)V", "VIEW_TYPE_ITEM", "", "VIEW_TYPE_LOADING", "getEventsList", "()Ljava/util/ArrayList;", "setEventsList", "(Ljava/util/ArrayList;)V", "getItemCount", "getItemViewType", "position", "onBindViewHolder", "", "holder", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "updateEventList", "newList", "Lcom/impaktsoft/globeup/models/EventPOJO;", "EventViewHolder", "LoadingViewHolder", "app_debug"})
public final class SearchByNameEventsAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder> {
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<com.impaktsoft.globeup.models.IEventsItem> eventsList;
    private final com.impaktsoft.globeup.viewmodels.BaseEventsViewModel viewModel = null;
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public androidx.recyclerview.widget.RecyclerView.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemViewType(int position) {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.RecyclerView.ViewHolder holder, int position) {
    }
    
    public final void updateEventList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.impaktsoft.globeup.models.EventPOJO> newList) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.impaktsoft.globeup.models.IEventsItem> getEventsList() {
        return null;
    }
    
    public final void setEventsList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.impaktsoft.globeup.models.IEventsItem> p0) {
    }
    
    public SearchByNameEventsAdapter(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.impaktsoft.globeup.models.IEventsItem> eventsList, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.viewmodels.BaseEventsViewModel viewModel) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fR\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\r"}, d2 = {"Lcom/impaktsoft/globeup/listadapters/SearchByNameEventsAdapter$EventViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "applicationBinding", "Lcom/impaktsoft/globeup/databinding/EventCellBinding;", "(Lcom/impaktsoft/globeup/listadapters/SearchByNameEventsAdapter;Lcom/impaktsoft/globeup/databinding/EventCellBinding;)V", "getApplicationBinding", "()Lcom/impaktsoft/globeup/databinding/EventCellBinding;", "setApplicationBinding", "(Lcom/impaktsoft/globeup/databinding/EventCellBinding;)V", "bind", "", "item", "Lcom/impaktsoft/globeup/models/EventPOJO;", "app_debug"})
    public final class EventViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private com.impaktsoft.globeup.databinding.EventCellBinding applicationBinding;
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.models.EventPOJO item) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.impaktsoft.globeup.databinding.EventCellBinding getApplicationBinding() {
            return null;
        }
        
        public final void setApplicationBinding(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.databinding.EventCellBinding p0) {
        }
        
        public EventViewHolder(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.databinding.EventCellBinding applicationBinding) {
            super(null);
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004\u00a8\u0006\u0005"}, d2 = {"Lcom/impaktsoft/globeup/listadapters/SearchByNameEventsAdapter$LoadingViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Lcom/impaktsoft/globeup/listadapters/SearchByNameEventsAdapter;Landroid/view/View;)V", "app_debug"})
    public final class LoadingViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        
        public LoadingViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View itemView) {
            super(null);
        }
    }
}