package com.impaktsoft.globeup.viewmodels;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u001a\u001a\u00020\u001bJ\b\u0010\u001c\u001a\u00020\u001bH\u0002J\b\u0010\u001d\u001a\u00020\u001bH\u0002J\u0006\u0010\u001e\u001a\u00020\u001bJ\u000e\u0010\u001f\u001a\u00020\u001b2\u0006\u0010 \u001a\u00020\u0015R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR \u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR \u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00110\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\r\"\u0004\b\u0013\u0010\u000fR \u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00150\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\r\"\u0004\b\u0016\u0010\u000fR \u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00150\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\r\"\u0004\b\u0019\u0010\u000f\u00a8\u0006!"}, d2 = {"Lcom/impaktsoft/globeup/viewmodels/EventDetailViewModel;", "Lcom/impaktsoft/globeup/viewmodels/BaseEventsViewModel;", "()V", "currentEventItem", "Lcom/impaktsoft/globeup/models/EventPOJO;", "getCurrentEventItem", "()Lcom/impaktsoft/globeup/models/EventPOJO;", "setCurrentEventItem", "(Lcom/impaktsoft/globeup/models/EventPOJO;)V", "currentEventMembers", "Landroidx/lifecycle/MutableLiveData;", "", "getCurrentEventMembers", "()Landroidx/lifecycle/MutableLiveData;", "setCurrentEventMembers", "(Landroidx/lifecycle/MutableLiveData;)V", "currentUserEventState", "Lcom/impaktsoft/globeup/enums/EventState;", "getCurrentUserEventState", "setCurrentUserEventState", "isSpinnerVisible", "", "setSpinnerVisible", "mapVisible", "getMapVisible", "setMapVisible", "backImageClicked", "", "defineEventInteractionsActions", "initCurrentEvent", "openChat", "setMapVisibility", "boolean", "app_debug"})
public final class EventDetailViewModel extends com.impaktsoft.globeup.viewmodels.BaseEventsViewModel {
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> mapVisible;
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.models.EventPOJO currentEventItem;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<com.impaktsoft.globeup.enums.EventState> currentUserEventState;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Integer> currentEventMembers;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isSpinnerVisible;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getMapVisible() {
        return null;
    }
    
    public final void setMapVisible(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.models.EventPOJO getCurrentEventItem() {
        return null;
    }
    
    public final void setCurrentEventItem(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.models.EventPOJO p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.impaktsoft.globeup.enums.EventState> getCurrentUserEventState() {
        return null;
    }
    
    public final void setCurrentUserEventState(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<com.impaktsoft.globeup.enums.EventState> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Integer> getCurrentEventMembers() {
        return null;
    }
    
    public final void setCurrentEventMembers(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Integer> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isSpinnerVisible() {
        return null;
    }
    
    public final void setSpinnerVisible(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    public final void setMapVisibility(boolean p0_32355860) {
    }
    
    public final void backImageClicked() {
    }
    
    public final void openChat() {
    }
    
    private final void initCurrentEvent() {
    }
    
    private final void defineEventInteractionsActions() {
    }
    
    public EventDetailViewModel() {
        super();
    }
}