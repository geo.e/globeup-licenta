package com.impaktsoft.globeup.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0011\b\u0016\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004R\u001c\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\u0004\u00a8\u0006\b"}, d2 = {"Lcom/impaktsoft/globeup/models/LastMessageState;", "Lcom/impaktsoft/globeup/models/IMessagePOJO;", "messageState", "Lcom/impaktsoft/globeup/enums/MessageState;", "(Lcom/impaktsoft/globeup/enums/MessageState;)V", "getMessageState", "()Lcom/impaktsoft/globeup/enums/MessageState;", "setMessageState", "app_debug"})
public final class LastMessageState implements com.impaktsoft.globeup.models.IMessagePOJO {
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.enums.MessageState messageState;
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.enums.MessageState getMessageState() {
        return null;
    }
    
    public final void setMessageState(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.enums.MessageState p0) {
    }
    
    public LastMessageState(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.enums.MessageState messageState) {
        super();
    }
}