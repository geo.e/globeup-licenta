package com.impaktsoft.globeup.bindingadapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u0004\u0018\u00010\u0001*\u00020\u0004H\u0007J\u001c\u0010\u0005\u001a\u00020\u0006*\u00020\u00042\u000e\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\bH\u0007J\u0016\u0010\t\u001a\u00020\u0006*\u00020\u00042\b\u0010\n\u001a\u0004\u0018\u00010\u000bH\u0007J\u0016\u0010\f\u001a\u00020\u0006*\u00020\u00042\b\u0010\r\u001a\u0004\u0018\u00010\u0001H\u0007\u00a8\u0006\u000e"}, d2 = {"Lcom/impaktsoft/globeup/bindingadapters/SpinnerViewBindingAdapters;", "", "()V", "getSelectedValue", "Landroid/widget/Spinner;", "setEntries", "", "entries", "", "setInverseBindingListener", "inverseBindingListener", "Landroidx/databinding/InverseBindingListener;", "setSelectedValue", "selectedValue", "app_debug"})
public final class SpinnerViewBindingAdapters {
    public static final com.impaktsoft.globeup.bindingadapters.SpinnerViewBindingAdapters INSTANCE = null;
    
    @androidx.databinding.BindingAdapter(value = {"cb_entries"})
    public static final void setEntries(@org.jetbrains.annotations.NotNull()
    android.widget.Spinner $this$setEntries, @org.jetbrains.annotations.Nullable()
    java.util.List<? extends java.lang.Object> entries) {
    }
    
    @androidx.databinding.BindingAdapter(value = {"cb_selectedValue"})
    public static final void setSelectedValue(@org.jetbrains.annotations.NotNull()
    android.widget.Spinner $this$setSelectedValue, @org.jetbrains.annotations.Nullable()
    java.lang.Object selectedValue) {
    }
    
    @androidx.databinding.BindingAdapter(value = {"cb_selectedValueAttrChanged"})
    public static final void setInverseBindingListener(@org.jetbrains.annotations.NotNull()
    android.widget.Spinner $this$setInverseBindingListener, @org.jetbrains.annotations.Nullable()
    androidx.databinding.InverseBindingListener inverseBindingListener) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @androidx.databinding.InverseBindingAdapter(attribute = "cb_selectedValue")
    public static final java.lang.Object getSelectedValue(@org.jetbrains.annotations.NotNull()
    android.widget.Spinner $this$getSelectedValue) {
        return null;
    }
    
    private SpinnerViewBindingAdapters() {
        super();
    }
}