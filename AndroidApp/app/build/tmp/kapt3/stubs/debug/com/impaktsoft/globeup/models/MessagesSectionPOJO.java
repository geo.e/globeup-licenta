package com.impaktsoft.globeup.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0011\b\u0016\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004R\u001c\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\u0004R \u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000e\u00a8\u0006\u000f"}, d2 = {"Lcom/impaktsoft/globeup/models/MessagesSectionPOJO;", "", "header", "Lcom/impaktsoft/globeup/models/MessageTimeSection;", "(Lcom/impaktsoft/globeup/models/MessageTimeSection;)V", "getHeader", "()Lcom/impaktsoft/globeup/models/MessageTimeSection;", "setHeader", "messagesList", "Ljava/util/ArrayList;", "Lcom/impaktsoft/globeup/models/IMessagePOJO;", "getMessagesList", "()Ljava/util/ArrayList;", "setMessagesList", "(Ljava/util/ArrayList;)V", "app_debug"})
public final class MessagesSectionPOJO {
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.models.MessageTimeSection header;
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<com.impaktsoft.globeup.models.IMessagePOJO> messagesList;
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.models.MessageTimeSection getHeader() {
        return null;
    }
    
    public final void setHeader(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.models.MessageTimeSection p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.impaktsoft.globeup.models.IMessagePOJO> getMessagesList() {
        return null;
    }
    
    public final void setMessagesList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.impaktsoft.globeup.models.IMessagePOJO> p0) {
    }
    
    public MessagesSectionPOJO(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.models.MessageTimeSection header) {
        super();
    }
}