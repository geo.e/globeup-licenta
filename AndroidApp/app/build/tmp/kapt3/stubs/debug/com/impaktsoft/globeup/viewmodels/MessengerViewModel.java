package com.impaktsoft.globeup.viewmodels;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0094\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010.\u001a\u00020/2\u0006\u00100\u001a\u000201H\u0002J\u0010\u00102\u001a\u00020/2\u0006\u00100\u001a\u000201H\u0002J\b\u00103\u001a\u00020/H\u0002J\u0017\u00104\u001a\b\u0012\u0004\u0012\u00020\u001705H\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u00106J\u0017\u00107\u001a\b\u0012\u0004\u0012\u00020\u001705H\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u00106J\u0017\u00108\u001a\b\u0012\u0004\u0012\u00020\u001705H\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u00106J!\u00109\u001a\u00020/2\u0006\u00100\u001a\u0002012\n\b\u0002\u0010:\u001a\u0004\u0018\u00010\u0004H\u0002\u00a2\u0006\u0002\u0010;J\b\u0010<\u001a\u00020/H\u0002J\u0017\u0010=\u001a\u00020/2\n\b\u0002\u0010>\u001a\u0004\u0018\u00010\u0017\u00a2\u0006\u0002\u0010?J\u0014\u0010@\u001a\u0004\u0018\u00010*2\b\u0010A\u001a\u0004\u0018\u00010BH\u0002J\b\u0010C\u001a\u00020/H\u0002J\u0012\u0010D\u001a\u0004\u0018\u00010B2\u0006\u00100\u001a\u000201H\u0002J0\u0010E\u001a*\u0012\u0004\u0012\u00020\u0004\u0012\n\u0012\b\u0012\u0004\u0012\u00020/0F0\u0013j\u0014\u0012\u0004\u0012\u00020\u0004\u0012\n\u0012\b\u0012\u0004\u0012\u00020/0F`GH\u0002J\u0010\u0010H\u001a\u0004\u0018\u00010\u00152\u0006\u0010I\u001a\u00020\u0014J\b\u0010J\u001a\u00020/H\u0002J\b\u0010K\u001a\u00020/H\u0002J\b\u0010L\u001a\u00020/H\u0002J\b\u0010M\u001a\u00020/H\u0002J\b\u0010N\u001a\u00020/H\u0002J\b\u0010O\u001a\u00020/H\u0016J\b\u0010P\u001a\u00020/H\u0016J\b\u0010Q\u001a\u00020/H\u0016J\u000e\u0010R\u001a\u00020/2\u0006\u00100\u001a\u000201J\b\u0010S\u001a\u00020/H\u0002J)\u0010T\u001a\u00020/2\u0006\u0010U\u001a\u00020V2\u0006\u0010W\u001a\u00020X2\n\b\u0002\u0010Y\u001a\u0004\u0018\u00010\u0017H\u0002\u00a2\u0006\u0002\u0010ZJ\b\u0010[\u001a\u00020/H\u0002J\u0006\u0010\\\u001a\u00020/J\u0019\u0010-\u001a\u00020/2\n\b\u0002\u0010>\u001a\u0004\u0018\u00010\u0017H\u0002\u00a2\u0006\u0002\u0010?J\u000e\u0010]\u001a\u00020/2\u0006\u0010^\u001a\u00020_J\b\u0010`\u001a\u00020/H\u0002J\u0006\u0010a\u001a\u00020/J\b\u0010b\u001a\u00020/H\u0002J\b\u0010c\u001a\u00020/H\u0002J\b\u0010d\u001a\u00020/H\u0003J\b\u0010e\u001a\u00020/H\u0002J\b\u0010f\u001a\u00020/H\u0002J\u0011\u0010g\u001a\u00020/H\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u00106J\u0010\u0010h\u001a\u00020/2\u0006\u00100\u001a\u000201H\u0002J\b\u0010i\u001a\u00020/H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0006\u001a\u00020\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR\u001c\u0010\f\u001a\u0004\u0018\u00010\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u001a\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0014\u0012\u0004\u0012\u00020\u00150\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u000e\u00a2\u0006\u0002\n\u0000R \u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00170\u0019X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u001a\"\u0004\b\u001b\u0010\u001cR\u0014\u0010\u001d\u001a\u00020\u00178BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\u001d\u0010\u001eR\u000e\u0010\u001f\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R \u0010!\u001a\b\u0012\u0004\u0012\u00020\u00140\u0019X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010\u001a\"\u0004\b#\u0010\u001cR\u000e\u0010$\u001a\u00020\u0017X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010%\u001a\b\u0012\u0004\u0012\u00020\'0&X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010(\u001a\b\u0012\u0004\u0012\u00020\'0&X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010)\u001a\b\u0012\u0004\u0012\u00020*0&X\u0082\u000e\u00a2\u0006\u0002\n\u0000R \u0010+\u001a\b\u0012\u0004\u0012\u00020\u00040\u0019X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b,\u0010\u001a\"\u0004\b-\u0010\u001c\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006j"}, d2 = {"Lcom/impaktsoft/globeup/viewmodels/MessengerViewModel;", "Lcom/impaktsoft/globeup/viewmodels/BaseViewModel;", "()V", "SELECT_CAMERA", "", "SELECT_IMAGE", "adapter", "Lcom/impaktsoft/globeup/listadapters/MessagesAdapter;", "getAdapter", "()Lcom/impaktsoft/globeup/listadapters/MessagesAdapter;", "setAdapter", "(Lcom/impaktsoft/globeup/listadapters/MessagesAdapter;)V", "conversationItem", "Lcom/impaktsoft/globeup/models/ConversationPOJO;", "getConversationItem", "()Lcom/impaktsoft/globeup/models/ConversationPOJO;", "setConversationItem", "(Lcom/impaktsoft/globeup/models/ConversationPOJO;)V", "conversationMembersInfo", "Ljava/util/HashMap;", "", "Lcom/impaktsoft/globeup/models/UserInfo;", "hasPassedFirstCallbackLastMsgObs", "", "isFetchingMessages", "Landroidx/lifecycle/MutableLiveData;", "()Landroidx/lifecycle/MutableLiveData;", "setFetchingMessages", "(Landroidx/lifecycle/MutableLiveData;)V", "isLastMessageStateEnable", "()Z", "membersCount", "messageSeenCount", "messageText", "getMessageText", "setMessageText", "messagesHasFetched", "messagesList", "Ljava/util/ArrayList;", "Lcom/impaktsoft/globeup/models/IMessagePOJO;", "messagesListDB", "messagesSections", "Lcom/impaktsoft/globeup/models/MessagesSectionPOJO;", "recyclerViewPosition", "getRecyclerViewPosition", "setRecyclerViewPosition", "addMessageLocally", "", "message", "Lcom/impaktsoft/globeup/models/MessagePOJO;", "addMessageToHisSection", "addMessagesInSections", "askCameraPermission", "Lcom/google/android/gms/tasks/Task;", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "askLocationPermission", "askReadExternalPermission", "createDateSection", "index", "(Lcom/impaktsoft/globeup/models/MessagePOJO;Ljava/lang/Integer;)V", "fetchConversationMembers", "fetchMessages", "pullToFetch", "(Ljava/lang/Boolean;)V", "findDateSection", "messageDate", "Ljava/util/Date;", "formatMessagesList", "getMessageDate", "getPopUpMenuOptions", "Lkotlin/Function0;", "Lkotlin/collections/HashMap;", "getUserInfo", "userKey", "initConversation", "initConversationMembersCountObserver", "initConversationMembersObserver", "initLastMessageObserver", "initMessagesObserver", "onCreate", "onDestroy", "onResume", "previewImage", "removeLastMessageState", "sendImageMessage", "image", "Landroid/graphics/Bitmap;", "imageUri", "Landroid/net/Uri;", "imageFromGallery", "(Landroid/graphics/Bitmap;Landroid/net/Uri;Ljava/lang/Boolean;)V", "sendLocation", "sendMessages", "showDestinationOnGoogleMap", "destination", "Lcom/google/android/gms/maps/model/LatLng;", "showLastMessageState", "showMenu", "sortSectionList", "sortSectionsMessagesList", "takeImage", "takePhoto", "updateLastMessageState", "updateLastMessageStateInDB", "updateMessage", "uploadPictureFromGallery", "app_debug"})
public final class MessengerViewModel extends com.impaktsoft.globeup.viewmodels.BaseViewModel {
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> messageText;
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.models.ConversationPOJO conversationItem;
    @org.jetbrains.annotations.NotNull()
    private com.impaktsoft.globeup.listadapters.MessagesAdapter adapter;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isFetchingMessages;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Integer> recyclerViewPosition;
    private boolean messagesHasFetched;
    private boolean hasPassedFirstCallbackLastMsgObs;
    private java.util.ArrayList<com.impaktsoft.globeup.models.IMessagePOJO> messagesList;
    private java.util.ArrayList<com.impaktsoft.globeup.models.IMessagePOJO> messagesListDB;
    private java.util.HashMap<java.lang.String, com.impaktsoft.globeup.models.UserInfo> conversationMembersInfo;
    private java.util.ArrayList<com.impaktsoft.globeup.models.MessagesSectionPOJO> messagesSections;
    private int membersCount;
    private int messageSeenCount;
    private final int SELECT_IMAGE = 4;
    private final int SELECT_CAMERA = 5;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getMessageText() {
        return null;
    }
    
    public final void setMessageText(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.models.ConversationPOJO getConversationItem() {
        return null;
    }
    
    public final void setConversationItem(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.models.ConversationPOJO p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.impaktsoft.globeup.listadapters.MessagesAdapter getAdapter() {
        return null;
    }
    
    public final void setAdapter(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.listadapters.MessagesAdapter p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isFetchingMessages() {
        return null;
    }
    
    public final void setFetchingMessages(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Integer> getRecyclerViewPosition() {
        return null;
    }
    
    public final void setRecyclerViewPosition(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Integer> p0) {
    }
    
    @java.lang.Override()
    public void onCreate() {
    }
    
    @java.lang.Override()
    public void onResume() {
    }
    
    @java.lang.Override()
    public void onDestroy() {
    }
    
    public final void showMenu() {
    }
    
    public final void sendMessages() {
    }
    
    public final void showDestinationOnGoogleMap(@org.jetbrains.annotations.NotNull()
    com.google.android.gms.maps.model.LatLng destination) {
    }
    
    public final void previewImage(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.MessagePOJO message) {
    }
    
    private final void sendLocation() {
    }
    
    private final void takePhoto() {
    }
    
    @android.annotation.SuppressLint(value = {"SimpleDateFormat"})
    private final void takeImage() {
    }
    
    private final void uploadPictureFromGallery() {
    }
    
    private final void sendImageMessage(android.graphics.Bitmap image, android.net.Uri imageUri, java.lang.Boolean imageFromGallery) {
    }
    
    private final void initConversation() {
    }
    
    private final void initMessagesObserver() {
    }
    
    private final void initConversationMembersCountObserver() {
    }
    
    private final void initConversationMembersObserver() {
    }
    
    private final void initLastMessageObserver() {
    }
    
    private final java.util.HashMap<java.lang.Integer, kotlin.jvm.functions.Function0<kotlin.Unit>> getPopUpMenuOptions() {
        return null;
    }
    
    private final void updateLastMessageState() {
    }
    
    public final void fetchMessages(@org.jetbrains.annotations.Nullable()
    java.lang.Boolean pullToFetch) {
    }
    
    private final void fetchConversationMembers() {
    }
    
    private final boolean isLastMessageStateEnable() {
        return false;
    }
    
    private final void setRecyclerViewPosition(java.lang.Boolean pullToFetch) {
    }
    
    private final void showLastMessageState() {
    }
    
    private final void removeLastMessageState() {
    }
    
    private final void sortSectionsMessagesList() {
    }
    
    private final void updateMessage(com.impaktsoft.globeup.models.MessagePOJO message) {
    }
    
    private final void addMessagesInSections() {
    }
    
    private final void formatMessagesList() {
    }
    
    private final void addMessageToHisSection(com.impaktsoft.globeup.models.MessagePOJO message) {
    }
    
    private final void createDateSection(com.impaktsoft.globeup.models.MessagePOJO message, java.lang.Integer index) {
    }
    
    private final com.impaktsoft.globeup.models.MessagesSectionPOJO findDateSection(java.util.Date messageDate) {
        return null;
    }
    
    private final void sortSectionList() {
    }
    
    private final java.util.Date getMessageDate(com.impaktsoft.globeup.models.MessagePOJO message) {
        return null;
    }
    
    private final void addMessageLocally(com.impaktsoft.globeup.models.MessagePOJO message) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.models.UserInfo getUserInfo(@org.jetbrains.annotations.NotNull()
    java.lang.String userKey) {
        return null;
    }
    
    public MessengerViewModel() {
        super();
    }
}