package com.impaktsoft.globeup.services;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\u0015\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0002J\u0016\u0010\u0010\u001a\u00020\u00112\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0013H\u0002J-\u0010\u0014\u001a\u00020\u00112\u0006\u0010\t\u001a\u00020\u00072\u000e\u0010\u0012\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u000f0\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0016\u00a2\u0006\u0002\u0010\u0018J+\u0010\u0019\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u001b0\u00130\u001a2\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0013H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u001cR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0007X\u0082D\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u001d"}, d2 = {"Lcom/impaktsoft/globeup/services/PermissionService;", "Lcom/impaktsoft/globeup/services/IPermissionService;", "activityService", "Lcom/impaktsoft/globeup/services/ICurrentActivityService;", "(Lcom/impaktsoft/globeup/services/ICurrentActivityService;)V", "permissionRequests", "", "", "Lcom/impaktsoft/globeup/services/PermissionRequest;", "requestCode", "checkPermissionStatus", "Lcom/impaktsoft/globeup/services/PermissionStatus;", "context", "Landroid/content/Context;", "permission", "", "ensurePermissionDefinedInManifest", "", "permissions", "", "onRequestPermissionsResult", "", "grantResults", "", "(I[Ljava/lang/String;[I)V", "requestPermissionStatusAsync", "Lcom/google/android/gms/tasks/Task;", "Lcom/impaktsoft/globeup/services/PermissionResponse;", "(Ljava/util/List;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class PermissionService implements com.impaktsoft.globeup.services.IPermissionService {
    private final int requestCode = 29;
    private final java.util.Map<java.lang.Integer, com.impaktsoft.globeup.services.PermissionRequest> permissionRequests = null;
    private final com.impaktsoft.globeup.services.ICurrentActivityService activityService = null;
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object requestPermissionStatusAsync(@org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> permissions, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.google.android.gms.tasks.Task<java.util.List<com.impaktsoft.globeup.services.PermissionResponse>>> p1) {
        return null;
    }
    
    @java.lang.Override()
    public void onRequestPermissionsResult(int requestCode, @org.jetbrains.annotations.NotNull()
    java.lang.String[] permissions, @org.jetbrains.annotations.NotNull()
    int[] grantResults) {
    }
    
    private final com.impaktsoft.globeup.services.PermissionStatus checkPermissionStatus(android.content.Context context, java.lang.String permission) {
        return null;
    }
    
    private final void ensurePermissionDefinedInManifest(java.util.List<java.lang.String> permissions) {
    }
    
    public PermissionService(@org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.services.ICurrentActivityService activityService) {
        super();
    }
}