package com.impaktsoft.globeup.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000e\u0018\u00002\u00020\u0001B5\b\u0016\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0016\u0010\u0004\u001a\u0012\u0012\u0004\u0012\u00020\u00060\u0005j\b\u0012\u0004\u0012\u00020\u0006`\u0007\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\nR\u001c\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001c\u0010\b\u001a\u0004\u0018\u00010\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R \u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016\u00a8\u0006\u0017"}, d2 = {"Lcom/impaktsoft/globeup/models/EventDetailPOJO;", "", "eventPOJO", "Lcom/impaktsoft/globeup/models/EventPOJO;", "userEventList", "Ljava/util/ArrayList;", "Lcom/impaktsoft/globeup/models/UserEventPOJO;", "Lkotlin/collections/ArrayList;", "eventState", "Lcom/impaktsoft/globeup/enums/EventState;", "(Lcom/impaktsoft/globeup/models/EventPOJO;Ljava/util/ArrayList;Lcom/impaktsoft/globeup/enums/EventState;)V", "getEventPOJO", "()Lcom/impaktsoft/globeup/models/EventPOJO;", "setEventPOJO", "(Lcom/impaktsoft/globeup/models/EventPOJO;)V", "getEventState", "()Lcom/impaktsoft/globeup/enums/EventState;", "setEventState", "(Lcom/impaktsoft/globeup/enums/EventState;)V", "getUserEventList", "()Ljava/util/ArrayList;", "setUserEventList", "(Ljava/util/ArrayList;)V", "app_debug"})
public final class EventDetailPOJO {
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.models.EventPOJO eventPOJO;
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.enums.EventState eventState;
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<com.impaktsoft.globeup.models.UserEventPOJO> userEventList;
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.models.EventPOJO getEventPOJO() {
        return null;
    }
    
    public final void setEventPOJO(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.models.EventPOJO p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.enums.EventState getEventState() {
        return null;
    }
    
    public final void setEventState(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.enums.EventState p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.impaktsoft.globeup.models.UserEventPOJO> getUserEventList() {
        return null;
    }
    
    public final void setUserEventList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.impaktsoft.globeup.models.UserEventPOJO> p0) {
    }
    
    public EventDetailPOJO(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.models.EventPOJO eventPOJO, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.impaktsoft.globeup.models.UserEventPOJO> userEventList, @org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.enums.EventState eventState) {
        super();
    }
}