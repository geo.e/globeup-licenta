package com.impaktsoft.globeup.services;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\n\u0010\u0002\u001a\u0004\u0018\u00010\u0003H&\u00a8\u0006\u0004"}, d2 = {"Lcom/impaktsoft/globeup/services/IFirebaseInvitationService;", "", "generateInvitationLink", "Landroid/net/Uri;", "app_debug"})
public abstract interface IFirebaseInvitationService {
    
    @org.jetbrains.annotations.Nullable()
    public abstract android.net.Uri generateInvitationLink();
}