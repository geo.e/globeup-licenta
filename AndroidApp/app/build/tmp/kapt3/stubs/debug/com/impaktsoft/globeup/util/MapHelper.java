package com.impaktsoft.globeup.util;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lcom/impaktsoft/globeup/util/MapHelper;", "", "()V", "Companion", "app_debug"})
public final class MapHelper {
    private static final double metersPerKm = 1000.0;
    private static final double kmInDegreesLat = 0.009043717329571148;
    private static final double kmInDegreeLong = 0.0;
    private static final double metersPerMile = 1609.44;
    public static final com.impaktsoft.globeup.util.MapHelper.Companion Companion = null;
    
    public MapHelper() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0006\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u001e\u0010\b\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\rJ\u0016\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\nJ\u0016\u0010\u0013\u001a\u00020\n2\u0006\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u0004J\u0016\u0010\u0016\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\nR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"}, d2 = {"Lcom/impaktsoft/globeup/util/MapHelper$Companion;", "", "()V", "kmInDegreeLong", "", "kmInDegreesLat", "metersPerKm", "metersPerMile", "getDistanceBetweenCoords", "coords1", "Lcom/google/android/gms/maps/model/LatLng;", "coords2", "scaleType", "Lcom/impaktsoft/globeup/enums/ScaleTypeEnum;", "getGreaterPoint", "Lcom/google/firebase/firestore/GeoPoint;", "radiusInKm", "", "centerPoint", "getLatLng", "lat", "lng", "getLesserPoint", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final com.google.firebase.firestore.GeoPoint getGreaterPoint(int radiusInKm, @org.jetbrains.annotations.NotNull()
        com.google.android.gms.maps.model.LatLng centerPoint) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.google.firebase.firestore.GeoPoint getLesserPoint(int radiusInKm, @org.jetbrains.annotations.NotNull()
        com.google.android.gms.maps.model.LatLng centerPoint) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.google.android.gms.maps.model.LatLng getLatLng(double lat, double lng) {
            return null;
        }
        
        public final double getDistanceBetweenCoords(@org.jetbrains.annotations.NotNull()
        com.google.android.gms.maps.model.LatLng coords1, @org.jetbrains.annotations.NotNull()
        com.google.android.gms.maps.model.LatLng coords2, @org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.enums.ScaleTypeEnum scaleType) {
            return 0.0;
        }
        
        private Companion() {
            super();
        }
    }
}