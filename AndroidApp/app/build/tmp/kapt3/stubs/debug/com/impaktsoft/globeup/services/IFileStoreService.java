package com.impaktsoft.globeup.services;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0012\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J!\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0007J-\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\f2\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u000eH\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000f\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0010"}, d2 = {"Lcom/impaktsoft/globeup/services/IFileStoreService;", "", "uploadConversationMessageImage", "", "conversationKey", "imageByteArray", "", "(Ljava/lang/String;[BLkotlin/coroutines/Continuation;)Ljava/lang/Object;", "uploadFile", "Lcom/impaktsoft/globeup/services/FileUploadResponse;", "path", "file", "Ljava/io/File;", "progress", "Lcom/impaktsoft/globeup/services/IProgress;", "(Ljava/lang/String;Ljava/io/File;Lcom/impaktsoft/globeup/services/IProgress;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public abstract interface IFileStoreService {
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object uploadFile(@org.jetbrains.annotations.NotNull()
    java.lang.String path, @org.jetbrains.annotations.NotNull()
    java.io.File file, @org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.services.IProgress progress, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.impaktsoft.globeup.services.FileUploadResponse> p3);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object uploadConversationMessageImage(@org.jetbrains.annotations.NotNull()
    java.lang.String conversationKey, @org.jetbrains.annotations.NotNull()
    byte[] imageByteArray, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.lang.String> p2);
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 3)
    public final class DefaultImpls {
    }
}