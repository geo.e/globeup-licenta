package com.impaktsoft.globeup.listadapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001fB\u001b\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\b\u0010\u0015\u001a\u00020\fH\u0016J\u0006\u0010\u0016\u001a\u00020\u0017J\u0018\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u0019\u001a\u00020\u00022\u0006\u0010\u001a\u001a\u00020\fH\u0016J\u0018\u0010\u001b\u001a\u00020\u00022\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\fH\u0016R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u000f\u001a\u00020\u00108FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0011\u0010\u0012R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "}, d2 = {"Lcom/impaktsoft/globeup/listadapters/FeedbackAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "arrayList", "", "Lcom/impaktsoft/globeup/models/FeedbackPOJO;", "viewModel", "Lcom/impaktsoft/globeup/viewmodels/FeedbackViewModel;", "(Ljava/util/List;Lcom/impaktsoft/globeup/viewmodels/FeedbackViewModel;)V", "cellTextSize", "", "cellWidth", "", "myApplicationBinding", "Lcom/impaktsoft/globeup/databinding/FeedbackCellBinding;", "spanSizeLookup", "Landroidx/recyclerview/widget/GridLayoutManager$SpanSizeLookup;", "getSpanSizeLookup", "()Landroidx/recyclerview/widget/GridLayoutManager$SpanSizeLookup;", "spanSizeLookup$delegate", "Lkotlin/Lazy;", "getItemCount", "getViewWidth", "", "onBindViewHolder", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "MyViewHolder", "app_debug"})
public final class FeedbackAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder> {
    private com.impaktsoft.globeup.databinding.FeedbackCellBinding myApplicationBinding;
    private int cellWidth;
    private float cellTextSize;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy spanSizeLookup$delegate = null;
    private final java.util.List<com.impaktsoft.globeup.models.FeedbackPOJO> arrayList = null;
    private final com.impaktsoft.globeup.viewmodels.FeedbackViewModel viewModel = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.recyclerview.widget.GridLayoutManager.SpanSizeLookup getSpanSizeLookup() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public androidx.recyclerview.widget.RecyclerView.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.RecyclerView.ViewHolder holder, int position) {
    }
    
    public final void getViewWidth() {
    }
    
    public FeedbackAdapter(@org.jetbrains.annotations.NotNull()
    java.util.List<com.impaktsoft.globeup.models.FeedbackPOJO> arrayList, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.viewmodels.FeedbackViewModel viewModel) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fR\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\r"}, d2 = {"Lcom/impaktsoft/globeup/listadapters/FeedbackAdapter$MyViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "applicationBinding", "Lcom/impaktsoft/globeup/databinding/FeedbackCellBinding;", "(Lcom/impaktsoft/globeup/listadapters/FeedbackAdapter;Lcom/impaktsoft/globeup/databinding/FeedbackCellBinding;)V", "getApplicationBinding", "()Lcom/impaktsoft/globeup/databinding/FeedbackCellBinding;", "setApplicationBinding", "(Lcom/impaktsoft/globeup/databinding/FeedbackCellBinding;)V", "bind", "", "item", "Lcom/impaktsoft/globeup/models/FeedbackPOJO;", "app_debug"})
    public final class MyViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private com.impaktsoft.globeup.databinding.FeedbackCellBinding applicationBinding;
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.models.FeedbackPOJO item) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.impaktsoft.globeup.databinding.FeedbackCellBinding getApplicationBinding() {
            return null;
        }
        
        public final void setApplicationBinding(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.databinding.FeedbackCellBinding p0) {
        }
        
        public MyViewHolder(@org.jetbrains.annotations.NotNull()
        com.impaktsoft.globeup.databinding.FeedbackCellBinding applicationBinding) {
            super(null);
        }
    }
}