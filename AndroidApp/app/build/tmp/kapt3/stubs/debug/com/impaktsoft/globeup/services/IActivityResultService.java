package com.impaktsoft.globeup.services;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\"\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00052\b\u0010\u0007\u001a\u0004\u0018\u00010\bH&J)\u0010\t\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\b2\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u000b\u001a\u00020\fH\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\r\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u000e"}, d2 = {"Lcom/impaktsoft/globeup/services/IActivityResultService;", "", "onActivityForResultService", "", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "startActivityForResult", "intent", "activityForResultListener", "Lcom/impaktsoft/globeup/listeners/IGetActivityForResultListener;", "(Landroid/content/Intent;ILcom/impaktsoft/globeup/listeners/IGetActivityForResultListener;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public abstract interface IActivityResultService {
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object startActivityForResult(@org.jetbrains.annotations.NotNull()
    android.content.Intent intent, int requestCode, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.listeners.IGetActivityForResultListener activityForResultListener, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p3);
    
    public abstract void onActivityForResultService(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data);
}