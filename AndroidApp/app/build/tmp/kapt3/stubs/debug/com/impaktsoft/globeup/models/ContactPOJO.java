package com.impaktsoft.globeup.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u00002\u00020\u0001B\u001b\b\u0016\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006R\u001c\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001c\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000e\u00a8\u0006\u000f"}, d2 = {"Lcom/impaktsoft/globeup/models/ContactPOJO;", "", "user", "Lcom/impaktsoft/globeup/models/UserPOJO;", "selected", "Lcom/impaktsoft/globeup/enums/ContactState;", "(Lcom/impaktsoft/globeup/models/UserPOJO;Lcom/impaktsoft/globeup/enums/ContactState;)V", "getSelected", "()Lcom/impaktsoft/globeup/enums/ContactState;", "setSelected", "(Lcom/impaktsoft/globeup/enums/ContactState;)V", "getUser", "()Lcom/impaktsoft/globeup/models/UserPOJO;", "setUser", "(Lcom/impaktsoft/globeup/models/UserPOJO;)V", "app_debug"})
public final class ContactPOJO {
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.models.UserPOJO user;
    @org.jetbrains.annotations.Nullable()
    private com.impaktsoft.globeup.enums.ContactState selected;
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.models.UserPOJO getUser() {
        return null;
    }
    
    public final void setUser(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.models.UserPOJO p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.impaktsoft.globeup.enums.ContactState getSelected() {
        return null;
    }
    
    public final void setSelected(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.enums.ContactState p0) {
    }
    
    public ContactPOJO(@org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.models.UserPOJO user, @org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.enums.ContactState selected) {
        super();
    }
}