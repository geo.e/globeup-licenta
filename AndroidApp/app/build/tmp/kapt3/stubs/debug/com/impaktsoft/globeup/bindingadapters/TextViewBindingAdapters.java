package com.impaktsoft.globeup.bindingadapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0006\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u001c\u0010\u0003\u001a\u00020\u0004*\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0007H\u0007J\u0014\u0010\t\u001a\u00020\u0004*\u00020\u00052\u0006\u0010\n\u001a\u00020\u000bH\u0007J\u0014\u0010\f\u001a\u00020\u0004*\u00020\u00052\u0006\u0010\r\u001a\u00020\u000eH\u0007J\u0014\u0010\u000f\u001a\u00020\u0004*\u00020\u00052\u0006\u0010\u0010\u001a\u00020\u000bH\u0007J&\u0010\u0011\u001a\u00020\u0004*\u00020\u00052\u0006\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0007J\u0014\u0010\u0018\u001a\u00020\u0004*\u00020\u00052\u0006\u0010\u0019\u001a\u00020\u001aH\u0007J \u0010\u001b\u001a\u00020\u0004*\u00020\u00052\b\u0010\u001c\u001a\u0004\u0018\u00010\u001d2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001dH\u0007J\u0014\u0010\u001f\u001a\u00020\u0004*\u00020\u00052\u0006\u0010 \u001a\u00020!H\u0007J\u0014\u0010\"\u001a\u00020\u0004*\u00020\u00052\u0006\u0010\u0010\u001a\u00020\u000bH\u0007J\u0014\u0010#\u001a\u00020\u0004*\u00020\u00052\u0006\u0010$\u001a\u00020%H\u0007J\u0014\u0010&\u001a\u00020\u0004*\u00020\u00052\u0006\u0010$\u001a\u00020%H\u0007J\u0014\u0010\'\u001a\u00020\u0004*\u00020\u00052\u0006\u0010(\u001a\u00020)H\u0007J\u0014\u0010*\u001a\u00020\u0004*\u00020\u00052\u0006\u0010+\u001a\u00020,H\u0007J\u0014\u0010-\u001a\u00020\u0004*\u00020\u00052\u0006\u0010.\u001a\u00020/H\u0007J\u0014\u00100\u001a\u00020\u0004*\u00020\u00052\u0006\u0010.\u001a\u00020/H\u0007\u00a8\u00061"}, d2 = {"Lcom/impaktsoft/globeup/bindingadapters/TextViewBindingAdapters;", "", "()V", "setAddress", "", "Landroid/widget/TextView;", "lat", "", "long", "setConversationLastMessageText", "lastMessage", "Lcom/impaktsoft/globeup/models/MessagePOJO;", "setConversationNameText", "conversation", "Lcom/impaktsoft/globeup/models/ConversationPOJO;", "setConversationTimeText", "messagePOJO", "setDistanceText", "eventLocation", "Lcom/google/firebase/firestore/GeoPoint;", "userLocation", "Lcom/google/android/gms/maps/model/LatLng;", "scaleType", "Lcom/impaktsoft/globeup/enums/ScaleTypeEnum;", "setEventDateText", "date", "", "setEventStateTextColor", "itemEventState", "Lcom/impaktsoft/globeup/enums/EventState;", "viewEventState", "setGcoinTextColor", "gCoinPOJO", "Lcom/impaktsoft/globeup/models/GCoinPOJO;", "setMessageText", "setTextColor", "isMessageSeen", "", "setTextStyle", "setTranscationTextColor", "transactionState", "Lcom/impaktsoft/globeup/enums/TransactionType;", "setUsersCount", "count", "", "setWithdrawTextCell", "withdrawPOJO", "Lcom/impaktsoft/globeup/models/WithdrawPOJO;", "setWithdrawTextColor", "app_debug"})
public final class TextViewBindingAdapters {
    public static final com.impaktsoft.globeup.bindingadapters.TextViewBindingAdapters INSTANCE = null;
    
    @android.annotation.TargetApi(value = android.os.Build.VERSION_CODES.M)
    @androidx.databinding.BindingAdapter(value = {"android:textColor", "cb_currentViewEventState"})
    public static final void setEventStateTextColor(@org.jetbrains.annotations.NotNull()
    android.widget.TextView $this$setEventStateTextColor, @org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.enums.EventState itemEventState, @org.jetbrains.annotations.Nullable()
    com.impaktsoft.globeup.enums.EventState viewEventState) {
    }
    
    @android.annotation.TargetApi(value = android.os.Build.VERSION_CODES.M)
    @androidx.databinding.BindingAdapter(value = {"android:textColor"})
    public static final void setTranscationTextColor(@org.jetbrains.annotations.NotNull()
    android.widget.TextView $this$setTranscationTextColor, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.enums.TransactionType transactionState) {
    }
    
    @androidx.annotation.RequiresApi(value = android.os.Build.VERSION_CODES.M)
    @androidx.databinding.BindingAdapter(value = {"cb_conversationMessageColor"})
    public static final void setTextColor(@org.jetbrains.annotations.NotNull()
    android.widget.TextView $this$setTextColor, boolean isMessageSeen) {
    }
    
    @androidx.databinding.BindingAdapter(value = {"cb_conversationMessageTextStyle"})
    public static final void setTextStyle(@org.jetbrains.annotations.NotNull()
    android.widget.TextView $this$setTextStyle, boolean isMessageSeen) {
    }
    
    @android.annotation.TargetApi(value = android.os.Build.VERSION_CODES.M)
    @androidx.databinding.BindingAdapter(value = {"android:textColor"})
    public static final void setGcoinTextColor(@org.jetbrains.annotations.NotNull()
    android.widget.TextView $this$setGcoinTextColor, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.GCoinPOJO gCoinPOJO) {
    }
    
    @android.annotation.TargetApi(value = android.os.Build.VERSION_CODES.M)
    @androidx.databinding.BindingAdapter(value = {"android:textColor"})
    public static final void setWithdrawTextColor(@org.jetbrains.annotations.NotNull()
    android.widget.TextView $this$setWithdrawTextColor, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.WithdrawPOJO withdrawPOJO) {
    }
    
    @android.annotation.TargetApi(value = android.os.Build.VERSION_CODES.M)
    @androidx.databinding.BindingAdapter(value = {"android:text"})
    public static final void setConversationNameText(@org.jetbrains.annotations.NotNull()
    android.widget.TextView $this$setConversationNameText, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.ConversationPOJO conversation) {
    }
    
    @android.annotation.TargetApi(value = android.os.Build.VERSION_CODES.M)
    @androidx.databinding.BindingAdapter(value = {"android:text"})
    public static final void setConversationLastMessageText(@org.jetbrains.annotations.NotNull()
    android.widget.TextView $this$setConversationLastMessageText, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.MessagePOJO lastMessage) {
    }
    
    @android.annotation.TargetApi(value = android.os.Build.VERSION_CODES.M)
    @androidx.databinding.BindingAdapter(value = {"cb_messageDate"})
    public static final void setConversationTimeText(@org.jetbrains.annotations.NotNull()
    android.widget.TextView $this$setConversationTimeText, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.MessagePOJO messagePOJO) {
    }
    
    @android.annotation.TargetApi(value = android.os.Build.VERSION_CODES.M)
    @androidx.databinding.BindingAdapter(value = {"cb_eventDate"})
    public static final void setEventDateText(@org.jetbrains.annotations.NotNull()
    android.widget.TextView $this$setEventDateText, long date) {
    }
    
    @android.annotation.TargetApi(value = android.os.Build.VERSION_CODES.M)
    @android.annotation.SuppressLint(value = {"SetTextI18n"})
    @androidx.databinding.BindingAdapter(value = {"android:text"})
    public static final void setWithdrawTextCell(@org.jetbrains.annotations.NotNull()
    android.widget.TextView $this$setWithdrawTextCell, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.WithdrawPOJO withdrawPOJO) {
    }
    
    @android.annotation.TargetApi(value = android.os.Build.VERSION_CODES.M)
    @androidx.databinding.BindingAdapter(value = {"cb_messageText"})
    public static final void setMessageText(@org.jetbrains.annotations.NotNull()
    android.widget.TextView $this$setMessageText, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.models.MessagePOJO messagePOJO) {
    }
    
    @android.annotation.TargetApi(value = android.os.Build.VERSION_CODES.M)
    @androidx.databinding.BindingAdapter(requireAll = true, value = {"cb_addressLat", "cb_addressLong"})
    public static final void setAddress(@org.jetbrains.annotations.NotNull()
    android.widget.TextView $this$setAddress, double lat, double p2_1663806) {
    }
    
    @android.annotation.TargetApi(value = android.os.Build.VERSION_CODES.M)
    @android.annotation.SuppressLint(value = {"SetTextI18n"})
    @androidx.databinding.BindingAdapter(value = {"cb_usersCount"})
    public static final void setUsersCount(@org.jetbrains.annotations.NotNull()
    android.widget.TextView $this$setUsersCount, int count) {
    }
    
    @android.annotation.TargetApi(value = android.os.Build.VERSION_CODES.M)
    @android.annotation.SuppressLint(value = {"SetTextI18n"})
    @androidx.databinding.BindingAdapter(requireAll = true, value = {"cb_eventLocation", "cb_userLocation", "cb_scaleType"})
    public static final void setDistanceText(@org.jetbrains.annotations.NotNull()
    android.widget.TextView $this$setDistanceText, @org.jetbrains.annotations.NotNull()
    com.google.firebase.firestore.GeoPoint eventLocation, @org.jetbrains.annotations.Nullable()
    com.google.android.gms.maps.model.LatLng userLocation, @org.jetbrains.annotations.NotNull()
    com.impaktsoft.globeup.enums.ScaleTypeEnum scaleType) {
    }
    
    private TextViewBindingAdapters() {
        super();
    }
}