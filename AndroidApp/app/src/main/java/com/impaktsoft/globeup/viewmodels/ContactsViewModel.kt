package com.impaktsoft.globeup.viewmodels

import com.impaktsoft.globeup.R

class ContactsViewModel :BaseViewModel()
{
    fun getBack()
    {
        navigation.popFragmentBackStack(R.id.nav_host_contacts)
    }
}