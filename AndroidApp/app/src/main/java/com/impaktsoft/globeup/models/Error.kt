package com.impaktsoft.globeup.models

class GError(val reason: GReason, val debugMessage: String)

enum class GReason {
    Unknown,
    Exception,
    NullValue,
    UsernameCollision,
    WeakPassword,
    EmailBadlyFormatted,
    NoConnection
}