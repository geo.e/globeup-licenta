package com.impaktsoft.globeup.viewmodels

import android.content.Intent
import android.util.Log
import android.view.View
import androidx.core.util.Pair
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.model.LatLng
import com.impaktsoft.globeup.enums.EventState
import com.impaktsoft.globeup.enums.ScaleTypeEnum
import com.impaktsoft.globeup.listeners.IClickListener
import com.impaktsoft.globeup.models.EventMembersPOJO
import com.impaktsoft.globeup.models.EventPOJO
import com.impaktsoft.globeup.models.IEventsItem
import com.impaktsoft.globeup.models.UserEventPOJO
import com.impaktsoft.globeup.services.CurrentActivityService
import com.impaktsoft.globeup.util.SharedPreferencesKeys
import com.impaktsoft.globeup.views.activities.EventActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

abstract class BaseEventsViewModel : BaseViewModel() {

    var myUserEventList = ArrayList<UserEventPOJO>()
    var eventsAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>? = null

    protected var eventInteractionJob: Job? = null
    protected var eventsFromDatabaseList = ArrayList<IEventsItem>()
    protected var startAction: IClickListener? = null
    protected var finishAction: IClickListener? = null

    private var eventsSupportingModification: MutableMap<String, Boolean> = mutableMapOf()
    private var eventConversationKey: String? = null

    fun isEventModifying(eventUid: String): Boolean? {
        val isModifying = eventsSupportingModification[eventUid]

        if (isModifying != null) {
            return isModifying
        }
        return false
    }

    fun getScaleType() : ScaleTypeEnum
    {
        var scaleType = sharedPreferencesService.readFromSharedPref<ScaleTypeEnum>(
            SharedPreferencesKeys.metricsSystemKey,
            ScaleTypeEnum::class.java)
        if(scaleType == null)
        {
            scaleType = ScaleTypeEnum.KILOMETERS
        }

        return scaleType
    }


    fun getUserCurrentLocation(): LatLng? {

        return sharedPreferencesService
            .readFromSharedPref<LatLng>(
                SharedPreferencesKeys.userLocationKey,
                LatLng::class.java
            )
    }

    fun getEventState(event: EventPOJO): EventState? {

        return if (checkEventsExistInMyList(event)) {
            getMyUserEventPOJO(event.uid)?.eventState
        } else {
            EventState.UNKNOWN
        }
    }


    fun eventClick(pairs: Array<Pair<View, String>>, itemJson: String) {

        if (eventInteractionJob != null && eventInteractionJob?.isActive!!) return
        dataExchangeService.put(EventsViewModel::class.qualifiedName!!, itemJson)

        navigation.navigateToActivity(
            EventActivity::class.java,
            false,
            false,
            null,
            null,
            pairs
        )
    }

    fun interestedForEvent(event: EventPOJO) {
        interactWithEvent(event, EventState.INTERESTED)
    }

    fun joinEvent(event: EventPOJO) {
        interactWithEvent(event, EventState.JOINED)
    }

    fun inviteToEvent(event: EventPOJO) {
        val dynamicLink = firebaseInvitationService.generateInvitationLink()
        val sendIntent = Intent()
        val msg = "Hey, check this out: $dynamicLink"
        sendIntent.action = Intent.ACTION_SEND
        sendIntent.putExtra(Intent.EXTRA_TEXT, msg)
        sendIntent.type = "text/plain"
        activityService.activity?.startActivity(Intent.createChooser(sendIntent,"Share link"))

    }

    fun donateForEvent(event: EventPOJO) {
        Log.d("testTag", event.name);
    }


    protected fun getMyUserEventPOJO(uid: String?): UserEventPOJO? {
        for (event in myUserEventList) {
            if (event.eventKey.equals(uid)) return event
        }
        return null
    }

    private fun interactWithEvent(eventPOJO: EventPOJO, eventState: EventState) {

        if (isEventJobActive()) return

        eventInteractionJob = viewModelScope.launch(Dispatchers.IO) {

            setCellSpinnerVisibility(eventPOJO, true)

            eventConversationKey = database.chatDAO.getEventConversationKey(eventPOJO.uid!!)

            if (checkEventsExistInMyList(eventPOJO)) {

                val myUserEvent = getMyUserEventPOJO(eventPOJO.uid)
                if (myUserEvent?.eventState == eventState) {
                    removeCurrentState(eventState, myUserEvent)
                } else {
                    removeCurrentState(eventState, myUserEvent)
                    addNewState(eventState, eventPOJO)
                }
            } else {
                addNewState(eventState, eventPOJO)
            }

            setCellSpinnerVisibility(eventPOJO, false)
        }
    }

    private suspend fun removeCurrentState(
        eventState: EventState,
        userEvent: UserEventPOJO?
    ) {
        val currentUserId = backend.currentUser?.uid
        val removedWithSuccess = database.eventDao.removeUserEvent(
            currentUserId!!,
            userEvent?.eventKey!!
        )
        if (eventState == EventState.JOINED) {
            database.eventDao.leaveEvent(
                userEvent.eventKey!!,
                EventMembersPOJO(currentUserId)
            )
            database.chatDAO.removeUserConversation(currentUserId, eventConversationKey!!)
            database.chatDAO.removeUserFromConversation(currentUserId, eventConversationKey!!)
            database.messagingService.unsubscribeFromATopic(eventConversationKey!!)
        }

        withContext(Dispatchers.Main)
        {
            if (removedWithSuccess) {
                myUserEventList.remove(userEvent)
            }
        }
    }

    private suspend fun addNewState(
        eventState: EventState,
        eventPOJO: EventPOJO
    ) {
        val currentUserId = backend.currentUser!!.uid
        val isUserStoredSuccessfully = database.eventDao.storeUserEvent(
            currentUserId,
            UserEventPOJO(eventState, eventPOJO.uid!!)
        )

        var joinedWithSuccess = true

        if (eventState == EventState.JOINED) {
            joinedWithSuccess = database.eventDao.joinEvent(
                eventPOJO.uid!!,
                EventMembersPOJO(currentUserId)
            )

            database.chatDAO.addUserConversation(currentUserId, eventConversationKey!!)
            database.chatDAO.addUserInConversation(currentUserId, eventConversationKey!!)
            database.messagingService.subscribeToTopic(eventConversationKey!!)

        }

        withContext(Dispatchers.Main)
        {

            if (isUserStoredSuccessfully && joinedWithSuccess) {
                myUserEventList.add(UserEventPOJO(eventState, eventPOJO.uid!!))
            }
        }
    }

    private suspend fun setCellSpinnerVisibility(event: EventPOJO, isSpinnerVisible: Boolean) {
        withContext(Dispatchers.Main) {
            eventsSupportingModification[event.uid!!] = isSpinnerVisible
            eventsAdapter?.notifyItemChanged(eventsFromDatabaseList.indexOf(event))
            if (isSpinnerVisible) {
                if (startAction != null) {
                    startAction?.clicked()
                }
            } else {
                if (finishAction != null) {
                    finishAction?.clicked()
                }
            }
        }
    }

    private fun isEventJobActive(): Boolean {
        if (eventInteractionJob != null) return eventInteractionJob?.isActive!!
        return false
    }

    private fun checkEventsExistInMyList(event: EventPOJO): Boolean {
        for (eventItem in myUserEventList) {
            if (eventItem.eventKey.equals(event.uid)) return true
        }
        return false
    }
}