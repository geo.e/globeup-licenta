package com.impaktsoft.globeup.bindingadapters

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.res.ColorStateList
import android.graphics.Typeface
import android.os.Build
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.google.gson.Gson
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.enums.EventState
import com.impaktsoft.globeup.enums.MessageType
import com.impaktsoft.globeup.enums.TransactionType
import com.impaktsoft.globeup.models.*
import com.impaktsoft.globeup.util.DateHelper
import android.location.Geocoder
import android.location.Address
import android.util.Log
import androidx.annotation.RequiresApi
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.firestore.GeoPoint
import com.impaktsoft.globeup.enums.ScaleTypeEnum
import com.impaktsoft.globeup.util.MapHelper
import java.lang.Exception
import java.util.*
import kotlin.math.roundToInt


object TextViewBindingAdapters {

    @TargetApi(Build.VERSION_CODES.M)
    @BindingAdapter("android:textColor", "cb_currentViewEventState")
    @JvmStatic
    fun TextView.setEventStateTextColor(itemEventState: EventState?, viewEventState: EventState?) {
        if (itemEventState == viewEventState || itemEventState == EventState.HOSTING) {

            this.setTextColor(ColorStateList.valueOf(context.getColor(R.color.colorPrimary)))
        } else {
            this.setTextColor(ColorStateList.valueOf(context.getColor(R.color.colorPrimaryDark)))
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @BindingAdapter("android:textColor")
    @JvmStatic
    fun TextView.setTranscationTextColor(transactionState: TransactionType) {
        if (transactionState == TransactionType.EVENT_DONATION || transactionState == TransactionType.GLOBAL_DONATION) {
            this.setTextColor(ColorStateList.valueOf(context.getColor(R.color.red_1)))

        } else if (transactionState == TransactionType.GETTING_GCOIN || transactionState == TransactionType.WITHDRAW) {
            this.setTextColor(ColorStateList.valueOf(context.getColor(R.color.colorAccent)))

        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    @BindingAdapter("cb_conversationMessageColor")
    @JvmStatic
    fun TextView.setTextColor(isMessageSeen: Boolean) {
        if (isMessageSeen) {
            this.setTextColor(ColorStateList.valueOf(context.getColor(R.color.colorPrimaryDark)))

        } else {
            this.setTextColor(ColorStateList.valueOf(context.getColor(R.color.black)))
        }
    }

    @BindingAdapter("cb_conversationMessageTextStyle")
    @JvmStatic
    fun TextView.setTextStyle(isMessageSeen: Boolean) {
        if (isMessageSeen) {
            this.setTypeface(null, Typeface.NORMAL)

        } else {
            this.setTypeface(this.typeface, Typeface.BOLD)
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @BindingAdapter("android:textColor")
    @JvmStatic
    fun TextView.setGcoinTextColor(gCoinPOJO: GCoinPOJO) {
        if (gCoinPOJO.selected) {
            this.setTextColor(ColorStateList.valueOf(context.getColor(R.color.white)))

        } else {
            this.setTextColor(ColorStateList.valueOf(context.getColor(R.color.black)))

        }

    }

    @TargetApi(Build.VERSION_CODES.M)
    @BindingAdapter("android:textColor")
    @JvmStatic
    fun TextView.setWithdrawTextColor(withdrawPOJO: WithdrawPOJO) {
        if (withdrawPOJO.selected) {
            this.setTextColor(ColorStateList.valueOf(context.getColor(R.color.white)))

        } else {
            this.setTextColor(ColorStateList.valueOf(context.getColor(R.color.grey)))

        }

    }

    @TargetApi(Build.VERSION_CODES.M)
    @BindingAdapter("android:text")
    @JvmStatic
    fun TextView.setConversationNameText(conversation: ConversationPOJO) {
        if (conversation.name != "") {
            this.text = conversation.name
        } else {
            //TODO find out other user name to desplay
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @BindingAdapter("android:text")
    @JvmStatic
    fun TextView.setConversationLastMessageText(lastMessage: MessagePOJO) {

        when {
            lastMessage.type == MessageType.ImageMessage -> this.text = "Image"
            lastMessage.type == MessageType.LocationMessage -> this.text = "Location"
            lastMessage.type == MessageType.TextMessage -> {

                val messagePOJO =
                    Gson().fromJson(lastMessage.jsonMessage, MessageTextPOJO::class.java)
                this.text = messagePOJO.text
            }
        }

    }

    @TargetApi(Build.VERSION_CODES.M)
    @BindingAdapter("cb_messageDate")
    @JvmStatic
    fun TextView.setConversationTimeText(messagePOJO: MessagePOJO) {

        if (messagePOJO.creationTimeUTC == null) {
            this.text = DateHelper.getDateHour(messagePOJO.serverCreationTimeUTC!!)
        } else {
            this.text = DateHelper.getDateHour(messagePOJO.creationTimeUTC!!)
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @BindingAdapter("cb_eventDate")
    @JvmStatic
    fun TextView.setEventDateText(date: Long) {

        val myLocationOffsetTime = DateHelper.getOffsetTimeInMillis()
        this.text = DateHelper.getDateString(date)
    }

    @SuppressLint("SetTextI18n")
    @TargetApi(Build.VERSION_CODES.M)
    @BindingAdapter("android:text")
    @JvmStatic
    fun TextView.setWithdrawTextCell(withdrawPOJO: WithdrawPOJO) {

        val cardNumb = withdrawPOJO.card_number
        this.text = "**** " + cardNumb?.subSequence(cardNumb.length - 4, cardNumb.length)
    }

    @TargetApi(Build.VERSION_CODES.M)
    @BindingAdapter("cb_messageText")
    @JvmStatic
    fun TextView.setMessageText(messagePOJO: MessagePOJO) {

        val message = Gson().fromJson(messagePOJO.jsonMessage, MessageTextPOJO::class.java)
        this.text = message.text
    }

    @TargetApi(Build.VERSION_CODES.M)
    @BindingAdapter(value = ["cb_addressLat", "cb_addressLong"], requireAll = true)
    @JvmStatic
    fun TextView.setAddress(lat: Double, long: Double) {

        if (lat == 0.0 && long == 0.0) {
            this.text = context.resources.getText(R.string.invalid_address)
        }
        val addresses: List<Address>
        val geocoder: Geocoder = Geocoder(context, Locale.getDefault())

        try {
            addresses = geocoder.getFromLocation(lat, long, 1)
            this.text = addresses[0].getAddressLine(0)
        } catch (e: Exception) {
            Log.d("Invalid address binding", "$lat $long");
        }
    }


    @SuppressLint("SetTextI18n")
    @TargetApi(Build.VERSION_CODES.M)
    @BindingAdapter("cb_usersCount")
    @JvmStatic
    fun TextView.setUsersCount(count: Int) {

        if (count >= 0)
            this.text = count.toString() + " " + context.getString(R.string.users)
    }


    @SuppressLint("SetTextI18n")
    @TargetApi(Build.VERSION_CODES.M)
    @BindingAdapter(value = ["cb_eventLocation", "cb_userLocation","cb_scaleType"], requireAll = true)
    @JvmStatic
    fun TextView.setDistanceText(eventLocation: GeoPoint, userLocation: LatLng?,scaleType: ScaleTypeEnum) {

        if (userLocation == null) return
        var scaleTypeString : String = ""
        if(scaleType == ScaleTypeEnum.KILOMETERS)
            scaleTypeString = context.getString(R.string.kilometers)
        else
            scaleTypeString = context.getString(R.string.miles)

        var distance = MapHelper.getDistanceBetweenCoords(
            LatLng(eventLocation.latitude, eventLocation.longitude),
            userLocation,scaleType)

        var distanceString : String = ""
        if(distance > 1)
        {
            distanceString = distance.toInt().toString()
        }
        else
        {
            distanceString = ((distance * 100).roundToInt() / 100.0).toString()//2 decimals
        }
        this.text = distanceString + scaleTypeString
    }

}