package com.impaktsoft.globeup.views.fragments

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.components.MyCustomSearchAppBarView
import com.impaktsoft.globeup.databinding.FragmentSelectGroupMembersBinding
import com.impaktsoft.globeup.listeners.IClickListener
import com.impaktsoft.globeup.viewmodels.SelectMembersViewModel
import kotlinx.android.synthetic.main.fragment_select_group_members.*

class SelectGroupMembersFragment :
    BaseBoundFragment<SelectMembersViewModel, FragmentSelectGroupMembersBinding>(
        SelectMembersViewModel::class
    ) {

    override var layoutId: Int = R.layout.fragment_select_group_members
    override val fragmentNameResourceID: Int = R.string.choose

    override fun setupDataBinding(binding: FragmentSelectGroupMembersBinding) {
        binding.viewModel = mViewModel
    }

    private var myCustomSearchAppBarView: MyCustomSearchAppBarView? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setLists()
        setCustomSearchActionBar()
    }


    fun setCustomSearchActionBar() {

        myCustomSearchAppBarView =
            (activity as AppCompatActivity).supportActionBar!!.customView as MyCustomSearchAppBarView

        if (myCustomSearchAppBarView != null) {
            myCustomSearchAppBarView!!.setTitle(getString(R.string.new_group))
            //set start string
            myCustomSearchAppBarView!!.setSubTitle(
                getString(
                    R.string.selected_contacts
                    , 0,
                    mViewModel.getUsersSize()
                )
            )
            setActionBarSubtitle()
        }
    }

    fun setActionBarSubtitle() {
        mViewModel.selectedUsersSize.observe(this, Observer {
            myCustomSearchAppBarView!!.setSubTitle(
                getString(
                    R.string.selected_contacts
                    , it,
                    mViewModel.getUsersSize()
                )
            )
        })
    }

    fun setLists() {

        val verticalLayoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        select_contacts_list.layoutManager = verticalLayoutManager
        select_contacts_list.adapter = mViewModel.getUsersAdapter()

        val lineSeparator = DividerItemDecoration(
            select_contacts_list.context,
            verticalLayoutManager.orientation
        )

        select_contacts_list.addItemDecoration(lineSeparator)


        val horizontalLayoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        selected_list.layoutManager = horizontalLayoutManager
        selected_list.adapter = mViewModel.getSelectedAdapter()

    }

    override fun onStop() {
        super.onStop()
        myCustomSearchAppBarView?.onStop()
    }

}
