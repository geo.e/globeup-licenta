package com.impaktsoft.globeup.models

class CountryPOJO {
    var countryName: String? = null
    var isSelected: Boolean? = false

    constructor(countryName: String?, isSelected: Boolean?=null) {
        this.countryName = countryName
        if(isSelected!=null)this.isSelected = isSelected
    }
}