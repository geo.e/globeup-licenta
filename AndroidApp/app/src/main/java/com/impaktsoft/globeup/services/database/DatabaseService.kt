package com.impaktsoft.globeup.services.database

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.messaging.FirebaseMessaging
import com.impaktsoft.globeup.services.IConnectivityService
import com.impaktsoft.globeup.services.ISharedPreferencesService

interface IDatabaseService {
    val userInfoDao: IUserInfoDAO
    val eventDao: IEventsDAO
    val chatDAO: IChatDAO
    val fireAuth: FirebaseAuth
    val messagingService: IMessagingService
    val firebaseFirebaseDynamicLinksService : IFirebaseDynamicLinksService
    val feedbackService : IFeedbackService
    fun getFirestore(): FirebaseFirestore
}

class DatabaseService(
    var connectivityService: IConnectivityService,
    var sharedPreferencesService: ISharedPreferencesService
) : IDatabaseService {
    private val fireStore: FirebaseFirestore by lazy { FirebaseFirestore.getInstance() }
    override val fireAuth: FirebaseAuth by lazy { FirebaseAuth.getInstance() }
    override val messagingService: IMessagingService = MessagingService(FirebaseMessaging.getInstance())
    override val firebaseFirebaseDynamicLinksService : IFirebaseDynamicLinksService = FirebaseDynamicLinksService(
        FirebaseDynamicLinks.getInstance())
    override val feedbackService: IFeedbackService = FeedbackService(fireStore)

    override fun getFirestore(): FirebaseFirestore {
        return fireStore
    }

    override val userInfoDao: IUserInfoDAO by lazy { UserInfoDAO(fireStore) }
    override val eventDao: IEventsDAO by lazy {
        EventsDAO(
            fireStore,
            connectivityService,
            sharedPreferencesService
        )
    }
    override val chatDAO: IChatDAO by lazy { ChatDAO(fireStore) }

    init {
        configureOfflineDB()
    }

    private fun configureOfflineDB() {
        val settings = FirebaseFirestoreSettings.Builder()
            .setPersistenceEnabled(true)
            .setCacheSizeBytes(FirebaseFirestoreSettings.CACHE_SIZE_UNLIMITED)
            .build()

        fireStore.firestoreSettings = settings
    }
}