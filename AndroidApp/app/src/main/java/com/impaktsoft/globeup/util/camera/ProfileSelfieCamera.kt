package com.impaktsoft.globeup.util.camera

import android.content.Context
import android.content.ContextWrapper
import android.graphics.Matrix
import android.util.Log
import android.util.Rational
import android.util.Size
import android.view.Surface
import android.view.TextureView
import android.view.ViewGroup
import androidx.camera.core.*
import androidx.lifecycle.LifecycleOwner
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.impaktsoft.globeup.AppConfig
import java.io.File
import java.lang.Exception
import java.util.*
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

class ProfileSelfieCamera {

    private lateinit var preview: Preview
    private lateinit var capture: ImageCapture

    private var initialized = false

    fun setup(lifecycleOwner: LifecycleOwner, previewTexture: TextureView) {
        preview = setupPreview(previewTexture)
        capture = setupImageCapture()

        CameraX.bindToLifecycle(lifecycleOwner, preview, capture)
        initialized = true
    }

    suspend fun takePicture(context: Context): Task<File> {
        if(!initialized) {
            throw Exception("Please setActionBar up the camera before you want to capture a picture.")
        }

        val file = getFileForProfilePicture(context)

        return Tasks.forResult(suspendCoroutine<File> {
            capture.takePicture(file,
                object : ImageCapture.OnImageSavedListener {
                    override fun onError(
                        error: ImageCapture.UseCaseError,
                        message: String, exc: Throwable?
                    ) {
                        exc?.printStackTrace()

                        if (exc != null) {
                            it.resumeWith(Result.failure(exc))
                        } else {
                            it.resumeWithException(Exception(message))
                        }
                    }

                    override fun onImageSaved(file: File) {
                        Log.d("ProfileSelfieCamera", file.absolutePath)
                        it.resumeWith(Result.success(file))
                    }
                })
        })
    }

    private fun setupPreview(previewTexture: TextureView): Preview {
        val previewConfig = PreviewConfig.Builder().apply {
            setTargetAspectRatio(Rational(1, 1))
            setTargetResolution(Size(640, 640))
            setLensFacing(CameraX.LensFacing.FRONT)
        }.build()

        // Build the viewfinder use case
        val preview = Preview(previewConfig)
        preview.setOnPreviewOutputUpdateListener {

            // To update the SurfaceTexture, we have to remove it and re-add it
            val parent = previewTexture.parent as ViewGroup
            parent.removeView(previewTexture)
            parent.addView(previewTexture, 0)

            previewTexture.surfaceTexture = it.surfaceTexture
            updateTransform(previewTexture)
        }

        return preview
    }

    private fun setupImageCapture(): ImageCapture {
        val imageCaptureConfig = ImageCaptureConfig.Builder()
            .apply {
                setTargetAspectRatio(Rational(1, 1))
                setLensFacing(CameraX.LensFacing.FRONT)
                // We don't setActionBar a resolution for image capture; instead, we
                // select a capture mode which will infer the appropriate
                // resolution based on aspect ration and requested mode
                setCaptureMode(ImageCapture.CaptureMode.MIN_LATENCY)
            }.build()

        // Build the image capture use case and attach button click listener
        return ImageCapture(imageCaptureConfig)
    }

    private fun updateTransform(previewTexture: TextureView) {
        val matrix = Matrix()
        // Compute the center of the view finder
        val centerX = previewTexture.width / 2f
        val centerY = previewTexture.height / 2f

        // Correct preview output to account for display rotation
        val rotationDegrees = when (previewTexture.display.rotation) {
            Surface.ROTATION_0 -> 0
            Surface.ROTATION_90 -> 90
            Surface.ROTATION_180 -> 180
            Surface.ROTATION_270 -> 270
            else -> return
        }

        matrix.postRotate(-rotationDegrees.toFloat(), centerX, centerY)

        // Finally, apply transformations to our TextureView
        previewTexture.setTransform(matrix)
    }

    private fun getFileForProfilePicture(context: Context): File {
        val cw = ContextWrapper(context)
        val directory = cw.getDir(AppConfig.ProfileImageFolder, Context.MODE_PRIVATE)
        if (!directory.exists()) {
            directory.mkdir()
        }

        val uuid = UUID.randomUUID()
        return File(directory, "${uuid}_${System.currentTimeMillis()}.jpg")
    }
}
