package com.impaktsoft.globeup.listadapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.impaktsoft.globeup.databinding.LevelCellBinding
import com.impaktsoft.globeup.models.LevelPOJO

class LevelAdapter(private val list: List<LevelPOJO>
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)

        val applicationBinding =
            LevelCellBinding.inflate(layoutInflater, parent, false)
        return ActionViewHolder(applicationBinding)

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val userPOJO = list[position]
        (holder as ActionViewHolder).bind(userPOJO)
    }

    inner class ActionViewHolder(var applicationBinding: LevelCellBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(item: LevelPOJO) {

            applicationBinding.levelItem = item
        }
    }
}