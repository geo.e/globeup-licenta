package com.impaktsoft.globeup.views.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.databinding.ActivityGetCouponBinding
import com.impaktsoft.globeup.viewmodels.GetCouponViewModel

class GetCouponActivity : BaseBoundActivity<GetCouponViewModel,ActivityGetCouponBinding>(GetCouponViewModel::class) {

    override var activityTitleResourceId: Int?=R.string.title_activity_get_coupon

    override val layoutId: Int= R.layout.activity_get_coupon

    override fun setupDataBinding(binding: ActivityGetCouponBinding) {
        binding.viewModel=mViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.exit_action_bar_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.settings_exit -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
