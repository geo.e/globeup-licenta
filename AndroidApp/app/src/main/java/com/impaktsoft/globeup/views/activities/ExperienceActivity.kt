package com.impaktsoft.globeup.views.activities

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.listadapters.ExperienceAdapter
import com.impaktsoft.globeup.databinding.ActivityExperienceBinding
import com.impaktsoft.globeup.dummy.ExperienceDummyData
import com.impaktsoft.globeup.viewmodels.ExperienceViewModel
import kotlinx.android.synthetic.main.activity_experience.*

class ExperienceActivity :
    BaseBoundActivity<ExperienceViewModel, ActivityExperienceBinding>(ExperienceViewModel::class) {

    override val layoutId: Int = R.layout.activity_experience

    override fun setupDataBinding(binding: ActivityExperienceBinding) {
        binding.viewModel = mViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        experience_list_view.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        experience_list_view.adapter =
            ExperienceAdapter(ExperienceDummyData.experienceListPOJO, this)

    }
}
