package com.impaktsoft.globeup.enums

enum class EventTimeFilterEnum {
    TODAY,THIS_WEEK,NEXT_WEEK,THIS_MONTH,NEXT_MONTH,ALL
}