package com.impaktsoft.globeup.listadapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.impaktsoft.globeup.databinding.ContactsCellBinding
import com.impaktsoft.globeup.databinding.ContactsForSelectCellBinding
import com.impaktsoft.globeup.models.ContactPOJO
import com.impaktsoft.globeup.models.UserPOJO
import com.impaktsoft.globeup.viewmodels.SelectMembersViewModel

class GroupContactsAdapter (private val viewModel:SelectMembersViewModel,
    private var arrayList: ArrayList<ContactPOJO>
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)

        val applicationBinding =
            ContactsForSelectCellBinding.inflate(layoutInflater, parent, false)
        return ContactsViewHolder(applicationBinding)

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val contactPOJO = arrayList[position]
        (holder as ContactsViewHolder).bind(contactPOJO)
    }

    inner class ContactsViewHolder(var applicationBinding: ContactsForSelectCellBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(item: ContactPOJO) {

            applicationBinding.contact = item
            applicationBinding.contactLayout.setOnClickListener {
                viewModel.selectUser(item.user!!)
            }
        }
    }
    fun setList(auxList:ArrayList<ContactPOJO>)
    {
        arrayList=auxList
        notifyDataSetChanged()
    }
}