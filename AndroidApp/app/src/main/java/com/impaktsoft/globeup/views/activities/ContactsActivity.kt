package com.impaktsoft.globeup.views.activities

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.ActionBar
import androidx.fragment.app.Fragment
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.components.MyCustomSearchAppBarView
import com.impaktsoft.globeup.listeners.IClickListener
import com.impaktsoft.globeup.listeners.IGetStringListener
import com.impaktsoft.globeup.viewmodels.ContactsViewModel
import org.greenrobot.eventbus.EventBus

class ContactsActivity : BaseActivity<ContactsViewModel>(ContactsViewModel::class) {

    private var customSearchAppBar: MyCustomSearchAppBarView? = null
    private var navHostFragment: Fragment? = null
    lateinit var getStringListener:IGetStringListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contacts)

        setCustomSearchActionBar()

        navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_contacts)
    }

    fun setCustomSearchActionBar() {
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setDisplayShowCustomEnabled(true)

        customSearchAppBar = MyCustomSearchAppBarView(this,object :IGetStringListener{
            override fun getCharSequence(string: String) {

                EventBus.getDefault().post(string)
            }

        })
        customSearchAppBar?.setBackClickListener(object : IClickListener {
            override fun clicked() {

                val backStackEntryCount = navHostFragment?.childFragmentManager?.backStackEntryCount
                if (backStackEntryCount == 0) {
                    finish()
                } else {
                    mViewModel.getBack()
                }
            }
        })

        supportActionBar?.customView = customSearchAppBar
    }

    override fun onBackPressed() {
        if (customSearchAppBar != null) {
            if (customSearchAppBar!!.checkSearchOptionsVisibility()) {
                customSearchAppBar?.showActionBarName()
                customSearchAppBar?.removeSearchText()
            } else {
                val backStackEntryCount = navHostFragment?.childFragmentManager?.backStackEntryCount
                if (backStackEntryCount == 0) {
                    finish()
                } else {
                    mViewModel.getBack()
                }
            }
        }
    }
}
