package com.impaktsoft.globeup.models

import com.impaktsoft.globeup.enums.EventTimeFilterEnum

class EventTimeFilterPOJO : IFilterItem {

    var eventTime: EventTimeFilterEnum? = null

    constructor(title: String?, eventTime: EventTimeFilterEnum, checked: Boolean? = null) : super(
        title,
        checked
    ) {
        this.eventTime = eventTime
    }
}