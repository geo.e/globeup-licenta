package com.impaktsoft.globeup.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.impaktsoft.globeup.listadapters.EventsAdapter
import com.impaktsoft.globeup.models.EventLoadingPOJO
import com.impaktsoft.globeup.models.EventPOJO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SearchEventsByNameViewModel : BaseEventsViewModel() {

    var isFetchMoreEventsEnable = false
    var isFetchingProcessRunning =MutableLiveData<Boolean>(false)
    var isPlaceHolderMessageVisible =MutableLiveData<Boolean>(false)

    private var currentCountryBinding = MutableLiveData<String>()
    private var currentUserId: String? = null
    private var currentCountryCode = ""
    private var currentSearchString = ""

    var loadingPosition = MutableLiveData(0)

    init {
        eventsAdapter = EventsAdapter(eventsFromDatabaseList, this)
        initUserEventsList()
    }

    private fun initUserEventsList() {
        viewModelScope.launch(Dispatchers.IO) {
            if (!backend.isSignedInAnonymously()) {
                initCurrentInfo()
                myUserEventList = database.eventDao.fetchUserEvents(currentUserId!!)
            }
        }
    }

    private suspend fun initCurrentInfo() {
        currentUserId = backend.currentUser!!.uid
        currentCountryCode = database.userInfoDao.fetch(currentUserId!!)!!.country!!
        withContext(Dispatchers.Main)
        {
            currentCountryBinding.value = mapService.getCountryName(currentCountryCode)
        }
    }

    fun fetchEventsByString(searchName: String) {

        try {
            if (searchName != currentSearchString) {
                loadingPosition.value = 0
                isFetchingProcessRunning.value=true
                database.eventDao.resetLastEventDocument()
            }
            currentSearchString = searchName

            viewModelScope.launch(Dispatchers.IO) {

                val eventsList: ArrayList<EventPOJO> =
                    database.eventDao.fetchEventByName(searchName, currentCountryCode)

                viewModelScope.launch(Dispatchers.Main) {
                    eventsFromDatabaseList.clear()
                    eventsFromDatabaseList.addAll(eventsList)
                    isPlaceHolderMessageVisible.value = eventsList.isNullOrEmpty()
                    eventsAdapter?.notifyDataSetChanged()
                    isFetchMoreEventsEnable = false
                    isFetchingProcessRunning.value=false
                }
            }

        } catch (e: Exception) {


        }
    }

    fun loadMoreEvents() {
        isFetchMoreEventsEnable = true

        eventsFromDatabaseList.add(EventLoadingPOJO())
        loadingPosition.value = eventsFromDatabaseList.size - 1
        eventsAdapter?.notifyItemChanged(loadingPosition.value!!)

        fetchEventsByString(currentSearchString)
    }

    fun backClick() {
        navigation.closeCurrentActivity()
    }
}