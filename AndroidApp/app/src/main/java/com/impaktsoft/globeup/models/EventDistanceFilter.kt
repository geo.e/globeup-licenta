package com.impaktsoft.globeup.models

class EventDistanceFilter : IFilterItem {

    var distance: Int? = null

    constructor(title: String?, distance: Int?, checked: Boolean? = null) : super(
        title,
        checked
    ) {
        this.distance = distance
    }
}