package com.impaktsoft.globeup.listadapters

import android.graphics.Paint
import android.graphics.Rect
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.ViewTreeObserver
import androidx.core.view.marginLeft
import androidx.core.view.marginRight
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.impaktsoft.globeup.databinding.FeedbackCellBinding
import com.impaktsoft.globeup.models.FeedbackPOJO
import com.impaktsoft.globeup.viewmodels.FeedbackViewModel

class FeedbackAdapter(
    private val arrayList: List<FeedbackPOJO>,
    private val viewModel: FeedbackViewModel
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var myApplicationBinding: FeedbackCellBinding? = null
    private var cellWidth: Int = 0
    private var cellTextSize = 0f;

    val spanSizeLookup: GridLayoutManager.SpanSizeLookup by lazy {
        object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                val bounds = Rect()
                val paint = Paint()

                paint.textSize = cellTextSize

                paint.getTextBounds(
                    arrayList.get(position).title,
                    0,
                    arrayList.get(position).title!!.length,
                    bounds
                )

                var width = bounds.width()
                return width + cellWidth
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val applicationBinding = FeedbackCellBinding.inflate(layoutInflater, parent, false)
        myApplicationBinding=applicationBinding
        getViewWidth()
        return MyViewHolder(applicationBinding)
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = arrayList[position]
        (holder as MyViewHolder).bind(item)
    }

    inner class MyViewHolder(var applicationBinding: FeedbackCellBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {
        fun bind(item: FeedbackPOJO) {
            applicationBinding.itemName = item.title
            applicationBinding.checked = item.checked
            applicationBinding.cellButton.setOnClickListener {
                viewModel.checkCurrentButton(adapterPosition)
            }
        }
    }

    fun getViewWidth() {
        myApplicationBinding!!.cellButton.viewTreeObserver.addOnGlobalLayoutListener(object :
            ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {

                if (myApplicationBinding != null) {
                    myApplicationBinding!!.cellButton.viewTreeObserver.removeOnGlobalLayoutListener(
                        this
                    )

                    val horizontalPadding =
                        myApplicationBinding!!.cellButton.paddingLeft + myApplicationBinding!!.cellButton.paddingRight
                    val horizontalMargin =
                        myApplicationBinding!!.cellButton.marginLeft + myApplicationBinding!!.cellButton.marginRight

                    cellTextSize = myApplicationBinding!!.cellButton.textSize
                    cellWidth = horizontalMargin + horizontalPadding + cellTextSize.toInt()

                }
            }
        })

    }

}