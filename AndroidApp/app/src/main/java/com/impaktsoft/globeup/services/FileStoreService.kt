package com.impaktsoft.globeup.services

import android.graphics.Bitmap
import android.net.Uri
import android.util.Log
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageTask
import com.google.firebase.storage.UploadTask
import com.impaktsoft.globeup.models.GError
import com.impaktsoft.globeup.models.GReason
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import java.io.ByteArrayOutputStream
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

interface IProgress {
    fun report(progress: Double)
}

interface IFileStoreService {
    suspend fun uploadFile(
        path: String,
        file: File,
        progress: IProgress? = null
    ): FileUploadResponse

    suspend fun uploadConversationMessageImage(
        conversationKey: String,
        imageByteArray: ByteArray
    ):String
}

class FileStoreService : IFileStoreService {
    private val storage by lazy { FirebaseStorage.getInstance() }
    private val conversationMessageImagesPath = "ConversationImages"

    override suspend fun uploadFile(
        path: String,
        file: File,
        progress: IProgress?
    ): FileUploadResponse {

        val fileReference = storage.reference.child(path)
        val putFileTask = fileReference.putFile(Uri.fromFile(file))
        putFileTask.addOnProgressListener {
            progress?.report(it.bytesTransferred.toDouble() / it.totalByteCount.toDouble())
        }
        val uploadTask = putFileTask.await()

        return if (uploadTask.task.isSuccessful) {
            val uri = fileReference.downloadUrl.await()
            FileUploadResponse(uri.toString())
        } else {
            FileUploadResponse(
                null,
                GError(GReason.Unknown, "Failed to upload file ${file.absoluteFile}")
            )
        }
    }

    override suspend fun uploadConversationMessageImage(
        conversationKey: String,
        imageByteArray: ByteArray
    ): String {

        val randomKey = UUID.randomUUID().toString()
        var imageUrl = ""

        val uploadTask =
            storage.reference.child(conversationMessageImagesPath).child(conversationKey)
                .child(randomKey).putBytes(imageByteArray)
                .await()

        uploadTask.storage.downloadUrl.addOnSuccessListener {
            imageUrl=it.toString()
        }.await()

        return imageUrl
    }
}

data class FileUploadResponse(val url: String? = null, val error: GError? = null)