package com.impaktsoft.globeup.dummy

import com.google.android.gms.maps.model.LatLng
import com.impaktsoft.globeup.enums.EventState
import com.impaktsoft.globeup.models.EventPOJO
import com.impaktsoft.globeup.models.EventSectionPOJO
import com.impaktsoft.globeup.models.IEventsItem


class EventsDummyData {

    companion object {

        /* val EVENTS: List<IEventsItem> = listOf(
            EventSectionPOJO("Events I'm hosting"),
            EventPOJO( "Marti ora 2", "event 0", "sibiu", "10km", LatLng(45.759624, 21.296025),EventState.HOSTING),
            EventPOJO( "Marti ora 2", "event 1", "sibiu", "130km",LatLng(45.759624, 21.296025), EventState.HOSTING),
            EventSectionPOJO("Upcoming EVENTS..."),
            EventPOJO( "Marti ora 3", "event 2", "sibiu", "102km",LatLng(45.759624, 21.296025), EventState.JOINED),
            EventPOJO( "Marti ora 4", "event 3", "sibiu", "1013km",LatLng(45.759624, 21.296025), EventState.JOINED),
            EventPOJO( "Marti ora 5", "event 4", "sibiu", "1340km",LatLng(45.759624, 21.296025), EventState.JOINED),
            EventSectionPOJO("Events I'm interested"),
            EventPOJO( "Marti ora 3", "event 2", "sibiu", "102km",LatLng(45.759624, 21.296025), EventState.INTERESTED),
            EventPOJO( "Marti ora 4", "event 3", "sibiu", "1013km",LatLng(45.759624, 21.296025), EventState.INTERESTED),
            EventPOJO( "Marti ora 5", "event 4", "sibiu", "1340km",LatLng(45.759624, 21.296025),EventState.INTERESTED)
        )

        val EVENTS_LIST_2:List<IEventsItem> = listOf(
            EventPOJO( "Marti ora 2", "event 0", "sibiu", "10km",LatLng(45.759624, 21.296025), EventState.UNKNOWN),
            EventPOJO( "Marti ora 2", "event 1", "sibiu", "130km",LatLng(45.759624, 21.296025), EventState.UNKNOWN),
            EventPOJO( "Marti ora 3", "event 2", "sibiu", "102km",LatLng(45.759624, 21.296025), EventState.UNKNOWN),
            EventPOJO( "Marti ora 4", "event 3", "sibiu", "1013km",LatLng(45.759624, 21.296025), EventState.UNKNOWN),
            EventPOJO( "Marti ora 5", "event 4", "sibiu", "1340km",LatLng(45.759624, 21.296025), EventState.UNKNOWN)
        )
    }*/
    }
}