package com.impaktsoft.globeup.models


data class UserInfo(
    var email: String? = null,
    var name: String? = null,
    var profileImage: String? = null,
    var country: String? = null
)