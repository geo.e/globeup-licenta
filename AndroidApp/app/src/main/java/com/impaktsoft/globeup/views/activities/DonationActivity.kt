package com.impaktsoft.globeup.views.activities

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import com.airbnb.lottie.LottieAnimationView
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.databinding.ActivityDonationBinding
import com.impaktsoft.globeup.viewmodels.DonationViewModel

class DonationActivity :
    BaseBoundActivity<DonationViewModel, ActivityDonationBinding>(DonationViewModel::class) {

    override val layoutId: Int = R.layout.activity_donation


    override fun onCreate(savedInstanceState: Bundle?) {
        activityTitleResourceId = R.string.donation_done_name

        super.onCreate(savedInstanceState)

        layoutInflated()

    }

    override fun setupDataBinding(binding: ActivityDonationBinding) {
        binding.viewModel = mViewModel
    }


    fun layoutInflated() {
        val globeAnimation = findViewById<LottieAnimationView>(R.id.animation_view_donate);
        globeAnimation.progress = 0.6f
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.exit_action_bar_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.settings_exit -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
