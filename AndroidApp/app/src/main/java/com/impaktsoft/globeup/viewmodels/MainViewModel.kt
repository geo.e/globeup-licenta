package com.impaktsoft.globeup.viewmodels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.impaktsoft.globeup.listeners.IGetConversationListener
import com.impaktsoft.globeup.listeners.IGetLastMessageSeenCount
import com.impaktsoft.globeup.models.ConversationPOJO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception

class MainViewModel : BaseViewModel() {

    var notificationCount = MutableLiveData<Int>()
    var messagesSeenByUser = HashMap<String, Boolean>()

    init {
        if (connectivityService.isInternetAvailable())
            initUnreadMessagesCountObserver()
    }

    override fun onDestroy() {
        if (connectivityService.isInternetAvailable())
            database.chatDAO.removeConversationsLastMessageObserver()
        super.onDestroy()
    }

    private fun initUnreadMessagesCountObserver() {
        viewModelScope.launch(Dispatchers.IO) {

            database.chatDAO.addConversationsObserver(backend.currentUser?.uid!!,
                object : IGetConversationListener {
                    override fun getConversation(conversation: ConversationPOJO) {
                        viewModelScope.launch (Dispatchers.IO){
                            database.chatDAO.addLastMessageCountObserver(conversation.key,object :
                                IGetLastMessageSeenCount
                            {
                                override fun getLastMessageSeenCount(seenCount: Int) {
                                    viewModelScope.launch(Dispatchers.IO) {

                                        val didUserSeenMessage = database.chatDAO.didUserSeenMessage(
                                            backend.currentUser!!.uid,
                                            conversation.key
                                        )
                                        if (conversation.lastMessage?.senderKey == backend.currentUser!!.uid) {
                                            messagesSeenByUser[conversation.key] = true
                                        } else {
                                            messagesSeenByUser[conversation.key] = didUserSeenMessage
                                        }
                                        withContext(Dispatchers.Main)
                                        {
                                            try {
                                                updateUnreadMessagesCount()
                                            }
                                            catch (e:Exception)
                                            {
                                                Log.d("Error in MainViewModel","initConversationObserver")
                                                updateUnreadMessagesCount()
                                            }
                                        }
                                    }
                                }

                            })
                        }
                    }
                })
        }
    }

    private fun updateUnreadMessagesCount() {
        var count = 0
        val iterator = messagesSeenByUser.iterator()
        while (iterator.hasNext()) {
            if (!iterator.next().value) count++
        }
        notificationCount.value = count
    }

}