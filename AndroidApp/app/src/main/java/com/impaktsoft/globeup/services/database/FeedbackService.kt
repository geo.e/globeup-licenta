package com.impaktsoft.globeup.services.database

import com.google.firebase.firestore.FirebaseFirestore
import com.impaktsoft.globeup.models.FeedbackModel
import io.opencensus.internal.StringUtils
import kotlinx.coroutines.tasks.await
import java.util.*

interface IFeedbackService {

    suspend fun sendFeedback(feedbackModel: FeedbackModel)
}

class FeedbackService(private val fireStore: FirebaseFirestore) : IFeedbackService {

    private val feedbackCollectionPath = "Feedbacks"

    override suspend fun sendFeedback(feedbackModel: FeedbackModel) {
        val randomKey = UUID.randomUUID().toString()
        fireStore.collection(feedbackCollectionPath)
            .document(feedbackModel.category!!)
            .collection(randomKey)
            .document(randomKey)
            .set(feedbackModel)
            .addOnFailureListener { throw it }
            .await()
    }

}