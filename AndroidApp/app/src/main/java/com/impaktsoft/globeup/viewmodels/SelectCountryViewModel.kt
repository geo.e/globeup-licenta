package com.impaktsoft.globeup.viewmodels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.listadapters.CountryAdapter
import com.impaktsoft.globeup.models.ConfigCountryModel
import com.impaktsoft.globeup.models.CountryPOJO
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

class SelectCountryViewModel : BaseViewModel() {

    companion object {
        const val countryKey = "countryKey"
        const val currentCountryKey = "currentCountryKey"
    }

    var countryAdapter: CountryAdapter? = null
    var buttonText = MutableLiveData<String>()
    var blockActionIfCountryNotSelected = MutableLiveData<Boolean>()
    var isCloseButtonVisible = MutableLiveData<Boolean>()
    var currentAction: (() -> Unit)? = null

    private var countryList = ArrayList<CountryPOJO>()
    private var selectedCountry: CountryPOJO? = null

    init {
        initCountryList()
    }

    override fun onResume() {
        val config = dataExchangeService.get<ConfigCountryModel>(this::class.qualifiedName!!)
        if (config != null) {
            buttonText.value = config.buttonName
            blockActionIfCountryNotSelected.value = config.blockActionIfCountryNotSelected
            isCloseButtonVisible.value = !config.blockActionIfCountryNotSelected!!
            currentAction = config.action

        }
        super.onResume()
    }

    private fun initCountryList() {

        val countriesName = mapService.getAllCountriesName()
        for (countryName in countriesName) {
            countryList.add(CountryPOJO(countryName))
        }
        countryAdapter = CountryAdapter(countryList, this)
    }

    fun close() {
        try {
            navigation.closeCurrentActivity()
        }
        catch (e:Exception)
        {
        }

    }

    fun saveSelectedCountry() {

        if (selectedCountry == null) {
            dialogService.showSnackbar(R.string.app_name)
            return
        }

        dataExchangeService.put(countryKey, selectedCountry!!)
        sharedPreferencesService.writeInSharedPref(currentCountryKey,selectedCountry!!)
        if (currentAction == null) {
            navigation.closeCurrentActivity()
        } else {
            currentAction?.invoke()
        }
    }

    fun selectCountry(countryPOJO: CountryPOJO) {

        unselectPreviousCountry()
        countryPOJO.isSelected = true
        selectedCountry = countryPOJO
        blockActionIfCountryNotSelected.value = false
        countryAdapter?.notifyItemChanged(countryAdapter?.countryList!!.indexOf(countryPOJO))
    }

    private fun unselectPreviousCountry() {
        for (country in countryList) {
            if (country.isSelected!!) {
                country.isSelected = false
                countryAdapter?.notifyItemChanged(countryList.indexOf(country))
                return
            }
        }
    }

    fun filterList(searchString: String) {
        val auxCountriesList = java.util.ArrayList<CountryPOJO>()
        for (item in countryList) {

            var name = item.countryName?.toLowerCase()

            var myString = searchString.toLowerCase()

            if (name!!.contains(myString) ||
                myString.isEmpty()
            ) {
                auxCountriesList.add(item)
            }

        }
        countryAdapter!!.filterList(auxCountriesList)
    }
}