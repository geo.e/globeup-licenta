package com.impaktsoft.globeup.util

class SharedPreferencesKeys {
    companion object
    {
        const val userLocationKey="userLocationKey"
        const val metricsSystemKey = "metricsSystemKey"
        const val didUserLogOutKey = "didUserLogOutKey"
    }
}