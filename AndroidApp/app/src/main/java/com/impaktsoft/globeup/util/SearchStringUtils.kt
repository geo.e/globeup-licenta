package com.impaktsoft.globeup.util

import com.impaktsoft.globeup.models.ConversationPOJO

class SearchStringUtils {
    companion object
    {
        fun searchConversationByName(searchString:String,
                                     conversationList:ArrayList<ConversationPOJO>):ArrayList<ConversationPOJO>
        {
            val newConversationList = ArrayList<ConversationPOJO>()
            conversationList.forEach {
                if(it.name.toLowerCase().contains(searchString.toLowerCase()))
                {
                    newConversationList.add(it)
                }
            }
            return newConversationList
        }
    }
}