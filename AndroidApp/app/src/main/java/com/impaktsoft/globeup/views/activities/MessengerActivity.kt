package com.impaktsoft.globeup.views.activities

import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.appcompat.app.ActionBar
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.components.MyCustomMessengerAppBarView
import com.impaktsoft.globeup.util.KeyHelper
import com.impaktsoft.globeup.viewmodels.MessengerActivityViewModel


class MessengerActivity :
    BaseActivity<MessengerActivityViewModel>(MessengerActivityViewModel::class) {

    var customMessengerAppBar: MyCustomMessengerAppBarView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_messenger)
        val conversationJson = intent.getStringExtra(KeyHelper.conversationExtrasKey)
        if(conversationJson!=null)
        {
            mViewModel.updateMessengerConversation(conversationJson)
        }
        setCustomSearchActionBar()
    }

    private fun setCustomSearchActionBar() {
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setDisplayShowCustomEnabled(true)

        customMessengerAppBar = MyCustomMessengerAppBarView(this)

        supportActionBar?.customView = customMessengerAppBar
    }

    @RequiresApi(Build.VERSION_CODES.P)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        mViewModel.onActivityForResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        mViewModel.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onBackPressed() {
        mViewModel.onBackPressed()
        super.onBackPressed()
    }
}
