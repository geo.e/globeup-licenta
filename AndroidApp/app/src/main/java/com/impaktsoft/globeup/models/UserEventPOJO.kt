package com.impaktsoft.globeup.models

import com.impaktsoft.globeup.enums.EventState

class UserEventPOJO {
    var eventState: EventState? = null
    var eventKey: String? = ""

    constructor(eventState: EventState, eventKey: String) {
        this.eventState = eventState
        this.eventKey = eventKey
    }

    constructor() {}
}