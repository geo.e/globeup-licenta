package com.impaktsoft.globeup.services.database

import android.util.Log
import com.google.firebase.messaging.FirebaseMessaging
import com.impaktsoft.globeup.models.NotificationMessagePOJO
import com.impaktsoft.globeup.services.database.Notifications.Client
import com.impaktsoft.globeup.services.database.Notifications.NotificationAPI
import com.impaktsoft.globeup.services.database.Notifications.NotificationRequest
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback

interface IMessagingService {
    fun subscribeToTopic(topicId: String)
    fun unsubscribeFromATopic(topicId: String)
    fun sendNotificationMessage(notificationMessage: NotificationMessagePOJO)
}

class MessagingService(private val firebaseMessaging: FirebaseMessaging) : IMessagingService {

    private val TAG = "SubscribeToTopic"
    private val FCM_API = "https://fcm.googleapis.com/"

    private val apiService =
        Client.getClient(FCM_API)!!.create(NotificationAPI::class.java)

    override fun subscribeToTopic(topicId: String) {
        try {
            firebaseMessaging
                .subscribeToTopic(topicId)
                .addOnSuccessListener {
                    Log.d(TAG, "succes subscribe to topic")
                }
                .addOnFailureListener {
                    Log.d(TAG, "fail subscribe to a topic")
                }
        } catch (e: Exception) {
            Log.d("AppError", e.message)
        }
    }

    override fun unsubscribeFromATopic(topicId: String) {
        firebaseMessaging.unsubscribeFromTopic(topicId)
            .addOnSuccessListener {
                Log.d(TAG, "succes unsubscribe to topic")
            }
            .addOnFailureListener {
                Log.d(TAG, "fail unsubscribe to a topic")
            }
    }

    override fun sendNotificationMessage(
        notificationMessage: NotificationMessagePOJO
    ) {
        val messageNotification =
            NotificationRequest(notificationMessage, notificationMessage.conversationPOJO?.key)
        try {
            apiService.sendChatNotification(messageNotification)
                .enqueue(object : Callback<ResponseBody> {
                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        Log.d("retrofitTest", "fail")
                    }

                    override fun onResponse(
                        call: Call<ResponseBody>,
                        response: retrofit2.Response<ResponseBody>
                    ) {
                        Log.d(
                            "SendNotification",
                            response.isSuccessful.toString() + " " + response.body()
                        );
                    }
                })
        } catch (e: java.lang.Exception) {
            Log.d("SendNotification", "Fail sent message: $e.")
        }
    }
}