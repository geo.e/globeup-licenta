package com.impaktsoft.globeup.viewmodels

import androidx.lifecycle.MutableLiveData
import com.impaktsoft.globeup.R
import java.io.File

class ImageCaptureViewModel : BaseViewModel() {

    var imageFile: File? = null
    var imagePath = MutableLiveData<String>()
    var hidePreviewView=MutableLiveData<Boolean>()

    fun setMyImageFile(file: File) {
        imageFile = file
        imagePath.value=file.absolutePath
    }

    fun imageCaptured() {
        dataExchangeService.put(RegisterViewModel::class.qualifiedName!!, imageFile!!)
        navigation.closeCurrentActivity()
    }

    fun captureImageFailed() {
        dialogService.showSnackbar(R.string.err_capture_image_failed)
    }

    fun retry() {
        hidePreviewView.value=true
    }

    fun accept() {

        imageCaptured()
    }

    fun deletePreviousPhotoFile() {

    }
}
