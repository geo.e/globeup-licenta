package com.impaktsoft.globeup.util

import android.app.Activity
import android.content.Context
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.ImageDecoder
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import androidx.annotation.RequiresApi
import java.io.FileOutputStream
import java.io.IOException


object ImageUtils {

    @RequiresApi(Build.VERSION_CODES.P)
    fun getBitmap(activity: Activity, imageUri: Uri?): Bitmap? {
        var bitmap = when {
            Build.VERSION.SDK_INT < 28 -> MediaStore.Images.Media.getBitmap(
                activity.contentResolver,
                imageUri
            )
            else -> {
                val source =
                    ImageDecoder.createSource(
                        activity.contentResolver!!,
                        imageUri!!
                    )
                ImageDecoder.decodeBitmap(source)
            }
        }
        return bitmap
    }

    fun rotateImageIfNecessary(photoPath: String) {
        val ei = ExifInterface(photoPath)

        val orientation = ei.getAttributeInt(
            ExifInterface.TAG_ORIENTATION,
            ExifInterface.ORIENTATION_UNDEFINED
        )

        var bitmap: Bitmap = BitmapFactory.decodeFile(photoPath)
        var rotatedBitmap: Bitmap?
        var overWrite = true

        rotatedBitmap = when (orientation) {

            ExifInterface.ORIENTATION_ROTATE_90 -> rotateImage(bitmap, 90f)

            ExifInterface.ORIENTATION_ROTATE_180 -> rotateImage(bitmap, 180f)

            ExifInterface.ORIENTATION_ROTATE_270 -> rotateImage(bitmap, 270f)

            ExifInterface.ORIENTATION_NORMAL -> {
                overWrite = false
                bitmap
            }
            else -> {
                overWrite = false
                bitmap
            }
        }

        if(overWrite) {
            try {
                FileOutputStream(photoPath).use { out ->
                    rotatedBitmap.compress(
                        Bitmap.CompressFormat.JPEG,
                        80,
                        out
                    )
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private fun rotateImage(source: Bitmap, angle: Float): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(
            source, 0, 0, source.width, source.height,
            matrix, true
        )
    }
}