package com.impaktsoft.globeup.viewmodels

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.content.FileProvider
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.gson.Gson
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.enums.MessageState
import com.impaktsoft.globeup.enums.MessageType
import com.impaktsoft.globeup.listadapters.MessagesAdapter
import com.impaktsoft.globeup.listeners.*
import com.impaktsoft.globeup.models.*
import com.impaktsoft.globeup.services.PermissionStatus
import com.impaktsoft.globeup.util.DateHelper
import com.impaktsoft.globeup.util.FileUtils
import com.impaktsoft.globeup.util.GoogleUtils
import com.impaktsoft.globeup.util.ImageUtils
import com.impaktsoft.globeup.views.activities.PreviewImageActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import java.io.ByteArrayOutputStream
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.coroutines.suspendCoroutine


class MessengerViewModel : BaseViewModel() {

    var messageText = MutableLiveData<String>()
    var conversationItem: ConversationPOJO? = null
    var adapter: MessagesAdapter
    var isFetchingMessages = MutableLiveData<Boolean>()
    var recyclerViewPosition = MutableLiveData<Int>()

    private var messagesHasFetched = false
    private var hasPassedFirstCallbackLastMsgObs = false

    private var messagesList = ArrayList<IMessagePOJO>()
    private var messagesListDB = ArrayList<IMessagePOJO>()
    private var conversationMembersInfo = HashMap<String, UserInfo>()
    private var messagesSections = ArrayList<MessagesSectionPOJO>()
    private var membersCount: Int = 1
    private var messageSeenCount: Int = 0

    private val SELECT_IMAGE = 4
    private val SELECT_CAMERA = 5

    init {
        initConversation()

        adapter = MessagesAdapter(messagesList, backend.currentUser!!.uid, this)

        fetchMessages()
        initMessagesObserver()
        initConversationMembersCountObserver()
        initLastMessageObserver()
        initConversationMembersObserver()

        viewModelScope.launch(Dispatchers.IO) {
            pendingService.resendPendingMessages(backend.currentUser!!.uid)
        }

    }

    override fun onCreate() {
        popUpMenuService.initializeMenu(
            PopUpMenuSettingsPOJO(
                R.menu.chat_menu,
                R.id.chat_options_button,
                getPopUpMenuOptions()
            )
        )
        super.onCreate()
    }

    override fun onResume() {
        database.messagingService.unsubscribeFromATopic(conversationItem?.key!!)
        super.onResume()
    }

    override fun onDestroy() {
        database.chatDAO.removeConversationMessagesCountObservers()
        database.chatDAO.removeLastMessageDocument()
        database.messagingService.subscribeToTopic(conversationItem?.key!!)
        super.onDestroy()
    }

    fun showMenu() {
        popUpMenuService.showMenu()
    }

    fun sendMessages() {
        if (messageText.value.isNullOrEmpty()) return

        val messageKey = UUID.randomUUID().toString()

        val messageTextPOJO = MessageTextPOJO(messageText.value!!)
        val messageJson = Gson().toJson(messageTextPOJO)
        val currentUserUID = backend.currentUser?.uid

        val messageItem = MessagePOJO(
            messageKey,
            MessageType.TextMessage,
            messageJson,
            currentUserUID!!
        )

        removeLastMessageState()
        addMessageLocally(messageItem)
        addMessageToHisSection(messageItem)
        showLastMessageState()

        database.messagingService.sendNotificationMessage(
            NotificationMessagePOJO(
                conversationItem,
                messageItem
            )
        )

        viewModelScope.launch(Dispatchers.IO) {

            database.chatDAO.sendMessage(
                messageItem,
                conversationItem!!.key, null
            )
        }

        messageText.value = ""
    }

    fun showDestinationOnGoogleMap(destination: LatLng) {
        showGoogleMapService.showMapActivity(destination)
    }

    fun previewImage(message: MessagePOJO) {
        dataExchangeService.put(PreviewImageViewModel::class.qualifiedName!!, message)
        navigation.navigateToActivity(PreviewImageActivity::class.java)
    }

    private fun sendLocation() {
        viewModelScope.launch(Dispatchers.Main) {
            val hasLocationPermission = askLocationPermission().await()

            if (hasLocationPermission) {

                val messageKey = UUID.randomUUID().toString()

                val messageLocationPOJO = MessageLocationPOJO()
                val messageJson = Gson().toJson(messageLocationPOJO)
                val currentUserUID = backend.currentUser?.uid

                val messageItem = MessagePOJO(
                    messageKey,
                    MessageType.LocationMessage,
                    messageJson,
                    currentUserUID!!
                )

                messageItem.creationTimeUTC = Date()

                viewModelScope.launch(Dispatchers.IO) {
                    pendingService.addMessageInPendingList(messageItem, conversationItem!!.key)
                }

                removeLastMessageState()
                addMessageLocally(messageItem)
                addMessageToHisSection(messageItem)
                showLastMessageState()

                locationService.getLocation(object : IGetLocationListener {
                    override fun getLocation(coords: LatLng?) {

                        val imageUrl = GoogleUtils.getMapImageURL(coords!!, 200, 200)
                        messageLocationPOJO.imageUrl = imageUrl
                        messageLocationPOJO.latitude = coords.latitude
                        messageLocationPOJO.longitude = coords.longitude
                        val messageJson = Gson().toJson(messageLocationPOJO)
                        messageItem.jsonMessage = messageJson
                        messageItem.creationTimeUTC = null

                        viewModelScope.launch(Dispatchers.IO) {

                            database.chatDAO.sendMessage(
                                messageItem,
                                conversationItem!!.key, object : IMessageSentListener {
                                    override fun messageHasBeenSend(messageKey: String) {
                                        Log.d("LocationMessage", "Send with success")
                                    }
                                }
                            )
                            pendingService.removeMessageFromPendingList(
                                messageItem,
                                conversationItem!!.key
                            )
                        }
                    }
                })
            } else {
                dialogService.showSnackbar(R.string.read_external_permission_message)
            }
        }
    }

    private fun takePhoto() {
        val methodsMap = HashMap<String, () -> Unit>()
        val itemsName = arrayOf<CharSequence>("Camera", "Gallery", "Cancel")
        methodsMap["Camera"] = ::takeImage
        methodsMap["Gallery"] = ::uploadPictureFromGallery
        methodsMap["Cancel"] = {}

        val alertBuilderSettings = AlertBuilderSettings(itemsName, methodsMap)
        alertBuilderService.showAlertDialog(alertBuilderSettings)
    }

    @SuppressLint("SimpleDateFormat")
    private fun takeImage() {
        viewModelScope.launch(Dispatchers.Main) {
            val hasCameraPermission = askCameraPermission().await()
            if (hasCameraPermission) {

                val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
                val storageDir: File =
                    activityService.activity?.getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
                var imageUriString: String?
                val imageFile = File.createTempFile(
                    "JPEG_${timeStamp}_",
                    ".jpg",
                    storageDir
                ).apply { imageUriString = absolutePath }

                val uri: Uri = FileProvider.getUriForFile(
                    activityService.activity!!,
                    "com.impaktsoft.globeup.fileprovider",
                    imageFile
                )
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                cameraIntent.putExtra(
                    MediaStore.EXTRA_OUTPUT, uri
                )

                viewModelScope.launch(Dispatchers.Main) {
                    activityResultService.startActivityForResult(cameraIntent, SELECT_CAMERA,
                        object : IGetActivityForResultListener {
                            @RequiresApi(Build.VERSION_CODES.P)
                            override fun activityForResult(
                                requestCode: Int,
                                resultCode: Int,
                                data: Intent?
                            ) {
                                if (resultCode == Activity.RESULT_OK) {
                                    if (requestCode == SELECT_CAMERA) {
                                        val bitmap =
                                            ImageUtils.getBitmap(
                                                activityService.activity!!,
                                                Uri.fromFile(File(imageUriString))
                                            )
                                        sendImageMessage(bitmap!!, Uri.parse(imageUriString))
                                    }
                                }
                            }
                        })
                }
            } else {
                dialogService.showSnackbar(R.string.camera_permission_message)
            }
        }
    }

    private fun uploadPictureFromGallery() {
        viewModelScope.launch(Dispatchers.Main) {
            val hasReadExternalPermission = askReadExternalPermission().await()
            if (hasReadExternalPermission) {
                val intent =
                    Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                viewModelScope.launch(Dispatchers.Main) {
                    activityResultService.startActivityForResult(
                        intent,
                        SELECT_IMAGE,
                        object : IGetActivityForResultListener {
                            @RequiresApi(Build.VERSION_CODES.P)
                            override fun activityForResult(
                                requestCode: Int,
                                resultCode: Int,
                                data: Intent?
                            ) {

                                if (resultCode == Activity.RESULT_OK) {
                                    if (requestCode == SELECT_IMAGE) {
                                        val imageUri = data!!.data;

                                        val bitmap =
                                            ImageUtils.getBitmap(
                                                activityService.activity!!,
                                                imageUri
                                            )
                                        sendImageMessage(bitmap!!, imageUri!!, true)
                                    }
                                }
                            }
                        })
                }
            } else {
                dialogService.showSnackbar(R.string.location_permission_message)
            }
        }
    }

    private fun sendImageMessage(image: Bitmap, imageUri: Uri, imageFromGallery: Boolean? = null) {

        val messageKey = UUID.randomUUID().toString()
        val messageImagePOJO = MessageImagePOJO()
        var messageJson = Gson().toJson(messageImagePOJO)
        val currentUserUID = backend.currentUser?.uid

        val messageItem = MessagePOJO(
            messageKey,
            MessageType.ImageMessage,
            messageJson,
            currentUserUID!!
        )

        val baos = ByteArrayOutputStream()
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val imageByteArray = baos.toByteArray()

        if (imageFromGallery != null && imageFromGallery) {
            messageImagePOJO.imageUri =
                FileUtils.getRealPathFromURI(activityService.activity!!, imageUri)
        } else {
            messageImagePOJO.imageUri = imageUri.path
        }

        messageJson = Gson().toJson(messageImagePOJO)
        messageItem.jsonMessage = messageJson
        messageItem.creationTimeUTC = null

        removeLastMessageState()
        addMessageLocally(messageItem)
        addMessageToHisSection(messageItem)
        adapter.notifyDataSetChanged()
        showLastMessageState()

        viewModelScope.launch(Dispatchers.IO) {
            pendingService.addMessageInPendingList(messageItem, conversationItem!!.key)
        }

        viewModelScope.launch(Dispatchers.IO) {

            val imageUrl =
                fileStoreService.uploadConversationMessageImage(
                    conversationItem!!.key,
                    imageByteArray
                )

            messageImagePOJO.imageUrl = imageUrl
            messageJson = Gson().toJson(messageImagePOJO)
            messageItem.jsonMessage = messageJson
            messageItem.creationTimeUTC = null
            database.chatDAO.sendMessage(
                messageItem,
                conversationItem!!.key, null
            )
            pendingService.removeMessageFromPendingList(messageItem, conversationItem!!.key)
        }
    }

    private fun initConversation() {
        val conversationJson =
            dataExchangeService.get<String>(MessengerViewModel::class.qualifiedName!!)
                ?: return
        conversationItem =
            Gson().fromJson(conversationJson, ConversationPOJO::class.java)
    }

    private fun initMessagesObserver() {
        viewModelScope.launch(Dispatchers.IO) {
            database.chatDAO.addConversationMessagesObserver(
                conversationItem!!.key,
                object : IGetMessagesListener {
                    override fun getMessage(message: MessagePOJO) {

                        if (messagesHasFetched) {

                            if (message.senderKey != backend.currentUser!!.uid) {

                                removeLastMessageState()
                                addMessageLocally(message)
                                addMessageToHisSection(message)

                                viewModelScope.launch(Dispatchers.IO) {
                                    updateLastMessageStateInDB()
                                }

                                recyclerViewPosition.value = messagesList.size - 1
                            } else {
                                if (message.creationTimeUTC != null) {
                                    updateMessage(message)
                                    sortSectionsMessagesList()
                                }
                            }
                        }
                    }
                }
            )
        }
    }

    private fun initConversationMembersCountObserver() {

        viewModelScope.launch(Dispatchers.IO) {
            database.chatDAO.addConversationMembersCountObserver(conversationItem!!.key,
                object : IGetConversationMembersCount {
                    override fun getConversationMembersCount(count: Int) {
                        membersCount = count
                    }
                })
        }
    }

    private fun initConversationMembersObserver() {
        viewModelScope.launch(Dispatchers.IO) {
            database.chatDAO.addConversationMembersObserver(conversationItem!!.key,
                object : IGetMembersKeyListener {
                    override fun getMembersKeyListener(membersKey: ArrayList<ConversationMemberPOJO>) {

                        val iterator = membersKey.iterator()
                        while (iterator.hasNext()) {
                            val conversationMember = iterator.next()
                            viewModelScope.launch(Dispatchers.IO) {
                                if (conversationMember.userId != backend.currentUser!!.uid) {
                                    val user = database.userInfoDao.fetch(conversationMember.userId)
                                    Log.d("pulamea", conversationMember.userId + " " + user?.name)
                                    conversationMembersInfo[conversationMember.userId!!] = user!!
                                }
                            }
                        }
                    }
                })
        }
    }

    private fun initLastMessageObserver() {
        viewModelScope.launch(Dispatchers.IO) {
            database.chatDAO.addLastMessageCountObserver(conversationItem!!.key,
                object : IGetLastMessageSeenCount {
                    override fun getLastMessageSeenCount(seenCount: Int) {
                        messageSeenCount = seenCount

                        if (!hasPassedFirstCallbackLastMsgObs) {
                            hasPassedFirstCallbackLastMsgObs = true
                        } else {

                            if (messagesList.size >= 2) {
                                if (messagesList[messagesList.size - 2] is MessagePOJO && messagesList[messagesList.size - 1] is LastMessageState) {
                                    if ((messagesList[messagesList.size - 2] as MessagePOJO).senderKey ==
                                        backend.currentUser!!.uid
                                    ) {
                                        updateLastMessageState()
                                    }
                                }
                            }
                        }
                    }

                })
        }
    }

    private fun getPopUpMenuOptions(): HashMap<Int, () -> Unit> {
        val methodsMap = HashMap<Int, () -> Unit>()
        methodsMap[R.id.location_send] = ::sendLocation
        methodsMap[R.id.image_send] = ::takePhoto
        return methodsMap
    }

    private fun updateLastMessageState() {
        var lastMessageState = MessageState.NOT_SEND
        if (messageSeenCount > 0) {
            lastMessageState = MessageState.RECEIVED
        }
        Log.d("pulamea seen", membersCount.toString() + " " + messageSeenCount)
        if (membersCount - messageSeenCount == 1 && messageSeenCount != 0) {
            lastMessageState = MessageState.SEEN
        }
        (messagesList[messagesList.size - 1] as LastMessageState).messageState =
            lastMessageState
        Log.d("pulamea", lastMessageState.toString()+" "+(messagesList[messagesList.size-1] is LastMessageState))
        adapter.notifyItemChanged(messagesList.size - 1)
    }

    private suspend fun askLocationPermission(): Task<Boolean> {

        return Tasks.forResult(suspendCoroutine<Boolean> {
            viewModelScope.launch(Dispatchers.Main) {
                val permissionsResult = permissionService.requestPermissionStatusAsync(
                    listOf(
                        Manifest.permission.ACCESS_FINE_LOCATION
                    )
                ).await()
                var grantedCounter = 0

                for (result in permissionsResult) {
                    if (result.permissionStatus == PermissionStatus.Granted) {
                        grantedCounter++
                    }
                }

                if (grantedCounter == permissionsResult.size) {
                    it.resumeWith(Result.success(true))
                } else {
                    it.resumeWith(Result.success(false))
                }
            }
        })
    }

    private suspend fun askReadExternalPermission(): Task<Boolean> {
        return Tasks.forResult(suspendCoroutine<Boolean> {
            viewModelScope.launch(Dispatchers.Main) {
                val permissionsResult = permissionService.requestPermissionStatusAsync(
                    listOf(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    )
                ).await()
                var grantedCounter = 0
                for (result in permissionsResult) {
                    if (result.permissionStatus == PermissionStatus.Granted) {
                        grantedCounter++
                    }
                }

                if (grantedCounter == permissionsResult.size) {
                    it.resumeWith(Result.success(true))
                } else {
                    it.resumeWith(Result.success(false))
                }
            }
        })
    }

    private suspend fun askCameraPermission(): Task<Boolean> {
        return Tasks.forResult(suspendCoroutine<Boolean> {
            viewModelScope.launch(Dispatchers.Main) {
                val permissionsResult = permissionService.requestPermissionStatusAsync(
                    listOf(
                        Manifest.permission.CAMERA
                    )
                ).await()
                var grantedCounter = 0

                for (result in permissionsResult) {
                    if (result.permissionStatus == PermissionStatus.Granted) {
                        grantedCounter++
                    }
                }

                if (grantedCounter == permissionsResult.size) {
                    it.resumeWith(Result.success(true))
                } else {
                    it.resumeWith(Result.success(false))

                }
            }
        })
    }

    fun fetchMessages(pullToFetch: Boolean? = null) {

        isFetchingMessages.value = true
        viewModelScope.launch(Dispatchers.IO) {

            val newMessages = database.chatDAO.fetchConversationMessages(conversationItem!!.key)
            val pendingMessages =
                pendingService.getPendingMessages(
                    backend.currentUser!!.uid,
                    conversationItem!!.key
                )

            viewModelScope.launch(Dispatchers.Main) {
                if (!messagesHasFetched) messagesHasFetched = true

                val auxList = ArrayList<IMessagePOJO>()
                auxList.addAll(newMessages)
                auxList.addAll(pendingMessages)

                removeLastMessageState()

                if (!auxList.isNullOrEmpty() && auxList.last() is MessagePOJO && (auxList.last() as MessagePOJO).senderKey != backend.currentUser!!.uid) {
                    viewModelScope.launch(Dispatchers.IO) {
                        updateLastMessageStateInDB()
                    }
                }

                messagesListDB.clear()
                messagesList.clear()
                messagesListDB.addAll(auxList)
                fetchConversationMembers()
                addMessagesInSections()
                adapter.notifyDataSetChanged()

                if (isLastMessageStateEnable) {
                    showLastMessageState()
                    updateLastMessageState()
                }

                sortSectionsMessagesList()
                isFetchingMessages.value = false
                setRecyclerViewPosition(pullToFetch)
            }
        }
    }

    private fun fetchConversationMembers() {
        messagesListDB.forEach {
            if (it is MessagePOJO)
                if (conversationMembersInfo[it.senderKey] == null) {
                    viewModelScope.launch(Dispatchers.IO) {
                        if (it.senderKey != backend.currentUser!!.uid) {
                            val user = database.userInfoDao.fetch(it.senderKey)
                            conversationMembersInfo[it.senderKey] = user!!

                            withContext(Dispatchers.Main) { adapter.notifyDataSetChanged() }
                        }
                    }
                }
        }
    }

    private val isLastMessageStateEnable
        get() = (messagesList.isNotEmpty() && messagesList.last() is MessagePOJO
                && (messagesList.last() as MessagePOJO).senderKey == backend.currentUser!!.uid)


    private fun setRecyclerViewPosition(pullToFetch: Boolean? = null) {
        if (!messagesList.isNullOrEmpty()) {
            if (pullToFetch != null) {
                recyclerViewPosition.value = 0
            } else {
                recyclerViewPosition.value = messagesList.size - 1
            }
        }
    }

    private suspend fun updateLastMessageStateInDB() {
        database.chatDAO.updateLastMessageState(
            backend.currentUser!!.uid,
            conversationItem!!.key
        )
    }

    private fun showLastMessageState() {
        messagesList.add(LastMessageState(MessageState.NOT_SEND))
        adapter.notifyItemChanged(messagesList.size - 1)
        recyclerViewPosition.value = messagesList.size - 1
    }

    private fun removeLastMessageState() {
        if (messagesList.size == 0) return
        if (messagesList[messagesList.size - 1] is LastMessageState) {
            messagesList.removeAt(messagesList.size - 1)
            adapter.notifyItemChanged(messagesList.size - 1)
        }
    }

    private fun sortSectionsMessagesList() {

        removeLastMessageState()
        messagesSections.forEach {
            it.messagesList.sortBy {
                (it as MessagePOJO).creationTimeUTC?.time
            }
        }

        messagesList.clear()
        formatMessagesList()

        if (messagesList.isNotEmpty() && (messagesList.last() as MessagePOJO).senderKey == backend.currentUser!!.uid) {
            showLastMessageState()
            updateLastMessageState()
        }

        adapter.notifyDataSetChanged()
    }

    private fun updateMessage(message: MessagePOJO) {

        messagesSections.forEach { it ->
            val messagePOJO =
                it.messagesList.find { (it is MessagePOJO) && it.messageKey == message.messageKey }
            if (messagePOJO != null) {
                (messagePOJO as MessagePOJO).serverCreationTimeUTC =
                    message.creationTimeUTC
                if (messagePOJO.type == MessageType.ImageMessage) {
                    val messageImage =
                        Gson().fromJson(messagePOJO.jsonMessage, MessageImagePOJO::class.java)
                    val messageImageFromServer =
                        Gson().fromJson(message.jsonMessage, MessageImagePOJO::class.java)

                    messageImage.imageUrl = messageImageFromServer.imageUrl
                    messagePOJO.jsonMessage = Gson().toJson(messageImage)
                }
            }

        }
        val messagePOJO =
            messagesList.find { (it is MessagePOJO) && it.messageKey == message.messageKey }

        if (messagePOJO == null) return

        (messagePOJO as MessagePOJO).creationTimeUTC = message.creationTimeUTC
    }

    private fun addMessagesInSections() {
        messagesListDB.forEach {
            addMessageToHisSection(it as MessagePOJO)
        }
        sortSectionList()
        formatMessagesList()
    }

    private fun formatMessagesList() {
        messagesSections.forEach { it ->
            messagesList.add(MessageTimeSection(it.header?.timeString,it.header?.timeLong))
            it.messagesList.forEach {
                messagesList.add(it)
            }
        }
    }

    private fun addMessageToHisSection(message: MessagePOJO) {
        if (messagesSections.isNullOrEmpty()) {
            createDateSection(message, 0)
        } else {
            val messageDate: Date?
            if (message.creationTimeUTC == null) {
                messageDate = message.serverCreationTimeUTC
            } else {
                messageDate = message.creationTimeUTC
            }

            val messageSection = findDateSection(messageDate)

            if (messageSection != null) {
                messageSection.messagesList.add(message)
            } else {
                createDateSection(message)
            }
        }
    }

    private fun createDateSection(message: MessagePOJO, index: Int? = null) {
        var messageDate: Date? = null
        if (message.creationTimeUTC == null)
            messageDate = message.serverCreationTimeUTC
        else
            messageDate = message.creationTimeUTC
        val timeString = DateHelper.getDateFormatForSections(messageDate!!)
        messagesSections.add(MessagesSectionPOJO(MessageTimeSection(timeString,messageDate.time)))

        if (index != null) {
            messagesSections[index].messagesList.add(message)
        } else {
            messagesSections.last().messagesList.add(message)
        }
    }

    private fun findDateSection(messageDate: Date?): MessagesSectionPOJO? {
        return messagesSections.find {
            it.header!!.timeString == DateHelper.getDateFormatForSections(
                messageDate!!
            )
        }
    }

    private fun sortSectionList() {

        messagesSections.sortBy { it.header?.timeLong }
    }

    private fun getMessageDate(message: MessagePOJO): Date? {
        if (message.creationTimeUTC == null) {
            return message.serverCreationTimeUTC
        } else {
            return message.creationTimeUTC
        }
    }

    private fun addMessageLocally(message: MessagePOJO) {
        val messageDate: Date? = getMessageDate(message)

        if (findDateSection(messageDate) == null) {
            messagesList.add(
                MessageTimeSection(
                    DateHelper.getDateFormatForSections(
                        messageDate!!
                    ),
                    messageDate.time
                )
            )
            messagesList.add(message)
            adapter.notifyDataSetChanged()
        } else {
            messagesList.add(message)
            adapter.notifyItemChanged(messagesList.size - 1)
        }
    }

    fun getUserInfo(userKey: String): UserInfo? {
        return conversationMembersInfo[userKey]
    }

}
