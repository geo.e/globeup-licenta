package com.impaktsoft.globeup.views.fragments

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.RadioGroup
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.databinding.FragmentSettingsBinding
import com.impaktsoft.globeup.viewmodels.SettingsViewModel
import kotlinx.android.synthetic.main.fragment_settings.*

class SettingsFragment :
    BaseBoundFragment<SettingsViewModel, FragmentSettingsBinding>(SettingsViewModel::class) {

    override var layoutId: Int = R.layout.fragment_settings
    override val fragmentNameResourceID: Int = R.string.settings_name

    override fun setupDataBinding(binding: FragmentSettingsBinding) {
        binding.viewModel = mViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.exit_action_bar_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when (item.itemId) {
            R.id.settings_exit -> {
                activity!!.finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
