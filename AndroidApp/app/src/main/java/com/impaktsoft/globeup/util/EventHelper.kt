package com.impaktsoft.globeup.util

import com.google.android.gms.maps.model.LatLng
import com.impaktsoft.globeup.enums.ScaleTypeEnum
import com.impaktsoft.globeup.models.*

class EventHelper {

    companion object {

        fun eventPassFilters(
            event: EventPOJO,
            filterList: ArrayList<IFilterItem>,
            userLocation: LatLng?,
            scaleType: ScaleTypeEnum): Boolean {
            return eventAcceptsTimeFilter(event, filterList)
                    && eventAcceptsTypeFilter(event, filterList)
                    && eventAcceptsDistanceFilter(event, filterList, userLocation,scaleType)
        }

        private fun eventAcceptsTypeFilter(
            event: EventPOJO,
            filterList: ArrayList<IFilterItem>
        ): Boolean {
            val filterType = getFilterType<EventTypeFilterPOJO>(filterList) ?: return true

            if (event.eventType == (filterType as EventTypeFilterPOJO).eventType
            ) {
                return true
            }
            return false
        }

        private fun eventAcceptsTimeFilter(
            event: EventPOJO,
            filterList: ArrayList<IFilterItem>
        ): Boolean {
            val filterType = getFilterType<EventTimeFilterPOJO>(filterList)

            if (filterType == null) return true
            if (DateHelper.eventWillHappendAtTime(
                    event,
                    (filterType as EventTimeFilterPOJO).eventTime!!
                )
            ) {
                return true
            }
            return false
        }

        private fun eventAcceptsDistanceFilter(
            event: EventPOJO,
            filterList: ArrayList<IFilterItem>,
            userLocation: LatLng?,
            scaleType: ScaleTypeEnum
        ): Boolean {
            if(userLocation==null) return true

            val filterType = getFilterType<EventDistanceFilter>(filterList)
            if (filterType == null) return true

            val eventCoords =
                MapHelper.getLatLng(event.location!!.latitude, event.location!!.longitude)

            if (MapHelper.getDistanceBetweenCoords(
                    userLocation,
                    eventCoords,scaleType) <= (filterType as EventDistanceFilter).distance!!
            ) {
                return true
            }

            return false
        }

        private inline fun <reified T> getFilterType(filterList: ArrayList<IFilterItem>): IFilterItem? {
            filterList.forEach {
                if (it is T) {
                    return it
                }
            }
            return null
        }
    }
}