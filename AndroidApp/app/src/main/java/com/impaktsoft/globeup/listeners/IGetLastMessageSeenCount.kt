package com.impaktsoft.globeup.listeners

import com.impaktsoft.globeup.enums.MessageState

interface IGetLastMessageSeenCount {
    fun getLastMessageSeenCount(seenCount:Int)
}