package com.impaktsoft.globeup.dummy

import com.google.firebase.firestore.auth.User
import com.impaktsoft.globeup.models.UserPOJO

class ContactsDummyData {
    companion object
    {
        var imageUrl : String? = "https://firebasestorage.googleapis.com/v0/b/pocfirebaseloginregister.appspot.com/o/UsersPhotos%2F-LnS5Ct1NHLlm3n5E3CJ?alt=media&token=46ad8d18-1427-4a2e-967b-bfb344552e56"

        val contactsList= listOf(
            UserPOJO("1234","g@g.com","Renato", imageUrl!!),
            UserPOJO("1234","g1@g.com","Cipi", imageUrl!!),
            UserPOJO("1234","g2@g.com","Geo", imageUrl!!),
            UserPOJO("1234","g3@g.com","Ion", imageUrl!!)
            )
    }
}