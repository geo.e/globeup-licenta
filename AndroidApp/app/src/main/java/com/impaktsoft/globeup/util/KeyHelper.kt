package com.impaktsoft.globeup.util

class KeyHelper {
    companion object{
        const val conversationExtrasKey = "conversationItem"
        const val messagePOJOKey = "messagePOJO"
        const val conversationPOJOKey = "conversationPOJO"
    }
}