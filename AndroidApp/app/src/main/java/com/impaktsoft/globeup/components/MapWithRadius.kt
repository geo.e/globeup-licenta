package com.impaktsoft.globeup.components

import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import com.google.android.gms.maps.CameraUpdate
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Circle
import com.google.android.gms.maps.model.CircleOptions
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.enums.ScaleTypeEnum
import com.impaktsoft.globeup.services.ISharedPreferencesService
import com.impaktsoft.globeup.util.SharedPreferencesKeys
import org.koin.core.KoinComponent
import org.koin.core.inject


class MapWithRadius(private val mMap: GoogleMap, private val context: Context):KoinComponent {

    protected val sharedPreferencesService: ISharedPreferencesService by inject()

    private var circleOptions = CircleOptions()
    private var circle: Circle? = null

    //set name
    fun setPinAndCameraPosition(coords: LatLng, zoom: Float? = null) {
        //TODO don t forget to set a name
        mMap.addMarker(MarkerOptions().position(coords).title("My current position"))
        var cameraUpdate: CameraUpdate?
        if (zoom != null) {
            cameraUpdate = CameraUpdateFactory.newLatLngZoom(coords, zoom)
        } else {
            cameraUpdate = CameraUpdateFactory.newLatLngZoom(coords, 10f)
        }

        mMap.animateCamera(cameraUpdate)
    }


    @TargetApi(Build.VERSION_CODES.M)
    fun createCircleRadius(coords: LatLng) {
        circleOptions.center(coords)

        circleOptions.fillColor(context.getColor(R.color.transparentColorAccent))

        circleOptions.strokeWidth(0f)

        mMap.addCircle(circleOptions)
    }

    fun setCircleRadius(progress: Int) {
        var  currentMetricSystemSelected = sharedPreferencesService.readFromSharedPref<ScaleTypeEnum>(
            SharedPreferencesKeys.metricsSystemKey,
            ScaleTypeEnum::class.java)
        if(currentMetricSystemSelected == null)
        {
            currentMetricSystemSelected = ScaleTypeEnum.KILOMETERS
        }

        //1609.44 to get radius in MILES
        //*1000 to get radius in KM
        var metersPerPrefferedUnit = 1000.0
        if(currentMetricSystemSelected == ScaleTypeEnum.MILES)
            metersPerPrefferedUnit = 1609.44


        circleOptions.radius(progress.toDouble() * metersPerPrefferedUnit)
        if (circle != null) {
            circle!!.remove()
        }
        circle = mMap.addCircle(circleOptions)
    }
}