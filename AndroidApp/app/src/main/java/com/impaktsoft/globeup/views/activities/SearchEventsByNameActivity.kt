package com.impaktsoft.globeup.views.activities

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.databinding.ActivitySearchEventsBinding
import com.impaktsoft.globeup.viewmodels.SearchEventsByNameViewModel
import kotlinx.android.synthetic.main.activity_search_events.*

class SearchEventsByNameActivity : BaseActivity<SearchEventsByNameViewModel>(SearchEventsByNameViewModel::class) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initDataBinding()
        eventsList.adapter = mViewModel.eventsAdapter
        eventsList.layoutManager =
            LinearLayoutManager(this, RecyclerView.VERTICAL, false)
    }

    private fun initDataBinding() {
        DataBindingUtil.setContentView<ActivitySearchEventsBinding>(
            this, R.layout.activity_search_events
        ).apply {
            this.setLifecycleOwner(this@SearchEventsByNameActivity)
            this.viewModel = mViewModel;
        }
    }
}
