package com.impaktsoft.globeup.listeners

interface IMessageSentListener {
    fun messageHasBeenSend(messageKey:String)
}