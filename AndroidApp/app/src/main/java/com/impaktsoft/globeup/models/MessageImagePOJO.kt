package com.impaktsoft.globeup.models

class MessageImagePOJO {
    var imageUrl:String=""
    var imageUri : String?= null

    constructor(imageUrl:String,imageUriJson: String){
        this.imageUrl=imageUrl
        this.imageUri=imageUriJson
    }
    constructor(){}
}