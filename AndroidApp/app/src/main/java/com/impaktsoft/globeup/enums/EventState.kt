package com.impaktsoft.globeup.enums

enum class EventState {
    JOINED,INTERESTED,UNKNOWN,HOSTING
}