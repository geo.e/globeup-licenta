package com.impaktsoft.globeup.components

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.*
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.listeners.IClickListener
import com.impaktsoft.globeup.listeners.IGetStringListener

class MyCustomSearchAppBarView(context: Context,listener: IGetStringListener) :
    FrameLayout(context) {

    private var searchNameLayout: LinearLayout? = null
    private var searchEditText: EditText? = null
    private var backImageActionBar: ImageView? = null
    private var searchImageActionBar: ImageView? = null
    private var titleActionBar: TextView? = null
    private var subTitleActionBar: TextView? = null


    init {
        addView(inflate(context, R.layout.custom_search_action_bar, null))
        backImageActionBar = findViewById<ImageView>(R.id.search_back_arrow)
        searchImageActionBar = findViewById<ImageView>(R.id.search_image)
        titleActionBar = findViewById<TextView>(R.id.title_search_action_bar)
        subTitleActionBar = findViewById<TextView>(R.id.subtitle_search_action_bar)
        searchEditText = findViewById<EditText>(R.id.search_edit_text)
        searchNameLayout = findViewById<LinearLayout>(R.id.search_names_layout)

        searchImageActionBar!!.setOnClickListener {
            hideActionBarName()
        }

        searchEditText?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                listener.getCharSequence(p0.toString())
            }

        })
    }

    fun setBackClickListener(clickMethod: IClickListener) {
        backImageActionBar!!.setOnClickListener {

            if (checkSearchOptionsVisibility()) {
                showActionBarName()
            } else {
                clickMethod.clicked()
            }
        }
    }

    fun hideActionBarName() {
        setEditTextVisibility(View.VISIBLE)
        setLayoutNameVisibility(View.GONE)
        setSearchImageVisibility(View.GONE)
    }

    fun showActionBarName() {
        setEditTextVisibility(View.GONE)
        setLayoutNameVisibility(View.VISIBLE)
        setSearchImageVisibility(View.VISIBLE)
    }

    fun removeSearchText()
    {
        searchEditText?.setText("")

    }

    fun checkSearchOptionsVisibility(): Boolean {
        return searchEditText!!.visibility == View.VISIBLE
    }

    fun setTitle(title: String) {
        titleActionBar!!.text = title
    }

    fun setSubTitle(subtitle: String) {
        setSubtitleVisibility(View.VISIBLE)
        subTitleActionBar!!.text = subtitle
    }

    fun setEditTextVisibility(visibility: Int) {
        searchEditText!!.visibility = visibility
    }

    fun setLayoutNameVisibility(visibility: Int) {
        searchNameLayout!!.visibility = visibility
    }

    fun setSearchImageVisibility(visibility: Int) {
        searchImageActionBar!!.visibility = visibility
    }

    fun setSubtitleVisibility(visibility: Int) {
        subTitleActionBar!!.visibility = visibility
    }

    fun setBackImageVisibility(visibility: Int) {
        backImageActionBar!!.visibility = visibility
    }

    fun onStop()
    {
        if(searchEditText?.visibility==View.VISIBLE)
        {
            removeSearchText()
            setEditTextVisibility(View.GONE)
            setLayoutNameVisibility(View.VISIBLE)
            setSearchImageVisibility(View.VISIBLE)
        }
    }
}

