package com.impaktsoft.globeup.services.database

import android.util.Log
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.firebase.firestore.*
import com.impaktsoft.globeup.enums.EventState
import com.impaktsoft.globeup.enums.EventTimeFilterEnum
import com.impaktsoft.globeup.enums.EventType
import com.impaktsoft.globeup.enums.ScaleTypeEnum
import com.impaktsoft.globeup.models.*
import com.impaktsoft.globeup.services.IConnectivityService
import com.impaktsoft.globeup.services.ISharedPreferencesService
import com.impaktsoft.globeup.util.DateHelper
import com.impaktsoft.globeup.util.EventHelper
import com.impaktsoft.globeup.util.MapHelper
import com.impaktsoft.globeup.util.SharedPreferencesKeys
import kotlinx.coroutines.tasks.await
import java.util.*
import kotlin.collections.ArrayList


interface IEventsDAO {

    suspend fun storeEvent(backendKey: String, event: EventPOJO)
    suspend fun storeUserEvent(userID: String, event: UserEventPOJO): Boolean
    suspend fun fetchEventsByKey(listOfKeys: ArrayList<String>?): ArrayList<EventPOJO>
    suspend fun fetchUserEvents(
        userID: String,
        dataSource: Source? = null
    ): ArrayList<UserEventPOJO>

    suspend fun fetchEvents(
        filtersList: ArrayList<IFilterItem>,
        country: String? = null,
        scaleType: ScaleTypeEnum? = null
    ): ArrayList<EventPOJO>

    suspend fun fetchEventByName(name: String, country: String?): ArrayList<EventPOJO>
    suspend fun removeUserEvent(userID: String, eventId: String): Boolean
    suspend fun updateEvent(eventItem: EventPOJO)
    suspend fun joinEvent(eventKey: String, eventMember: EventMembersPOJO): Boolean
    suspend fun leaveEvent(eventKey: String, eventMember: EventMembersPOJO): Boolean
    suspend fun getEventMembersCount(eventId: String): Int?
    suspend fun removeAllUserEvents(userID: String)
    fun resetLastEventDocument()

}

class EventsDAO(
    private val fireStore: FirebaseFirestore,
    private val connectivityService: IConnectivityService,
    private val sharedPreferencesService: ISharedPreferencesService
) : IEventsDAO {

    private val eventCollection: String = "Events"
    private val userEventsCollection: String = "UserEvents"
    private val eventMembersCollection: String = "EventMembers"
    private val fetchEventsLimitNumber: Long = 4
    private var fetchWithFiltersCount: Int = 1

    private var eventsQuery: Query? = null
    private var lastEventDocumentLoaded: DocumentSnapshot? = null

    private val eventDateField = "eventDate"
    private val eventTypeField = "eventType"
    private val eventCountryField = "eventCountry"
    private val eventLocationField = "location"
    private val eventNameField = "name"

    private var tasksList = ArrayList<Task<QuerySnapshot>>()

    override suspend fun storeEvent(backendKey: String, event: EventPOJO) {

        fireStore.collection(eventCollection)
            .document(backendKey).set(event).await()
    }

    override suspend fun storeUserEvent(userID: String, event: UserEventPOJO): Boolean {

        val listOfUserEvents: ArrayList<UserEventPOJO> = fetchUserEvents(userID)
        listOfUserEvents.add(event)
        val task = fireStore.collection(userEventsCollection)
            .document(userID)
            .set(UserEventsListPOJO(listOfUserEvents))

        if (connectivityService.isInternetAvailable()) {
            task.await()
            return task.isSuccessful
        }
        return true
    }

    override suspend fun fetchEventsByKey(listOfKeys: ArrayList<String>?): ArrayList<EventPOJO> {
        val listOfEvents = ArrayList<EventPOJO>()
        for (index in 0 until listOfKeys!!.size) {

            fireStore.collection(eventCollection)
                .document(listOfKeys[index])
                .get()
                .addOnSuccessListener {
                    val eventItem = it.toObject(EventPOJO::class.java)
                    if (eventItem != null) {
                        listOfEvents.add(eventItem)
                    }
                }
                .addOnFailureListener {
                    throw it
                }
                .await()

        }
        return listOfEvents
    }


    override suspend fun fetchUserEvents(
        userID: String,
        dataSource: Source?
    ): ArrayList<UserEventPOJO> {

        val listOfEvents = ArrayList<UserEventPOJO>()
        var source = defaultDataSource

        if (dataSource != null) source = dataSource
        fireStore.collection(userEventsCollection)
            .document(userID)
            .get(source)
            .addOnSuccessListener {
                if (it != null) {
                    val userEventsList =
                        (it.toObject(UserEventsListPOJO::class.java))?.userEventsList
                    if (userEventsList != null)
                        listOfEvents.addAll(userEventsList)
                }
            }
            .addOnFailureListener {
                throw it
            }
            .await()

        return listOfEvents
    }

    override suspend fun fetchEvents(
        filtersList: ArrayList<IFilterItem>,
        country: String?,
        scaleType: ScaleTypeEnum?
    ): ArrayList<EventPOJO> {
        val eventsList: ArrayList<EventPOJO>
        eventsList = try {
            if (filtersList.isNullOrEmpty()) {
                fetchEventsWithoutFilters(country)
            } else {
                fetchEventsWithFilters(filtersList, country, scaleType)
            }
        } catch (e: Exception) {
            throw e
        }

        return eventsList
    }

    override suspend fun fetchEventByName(
        name: String,
        country: String?
    ): ArrayList<EventPOJO> {

        val listOfEvents = ArrayList<EventPOJO>()

        eventsQuery = fireStore.collection(eventCollection)
            .whereGreaterThanOrEqualTo(eventNameField, name)
            .orderBy(eventNameField)
            .limit(fetchEventsLimitNumber * fetchWithFiltersCount)

        eventsQuery!!
            .get()
            .addOnSuccessListener {
                for (querySnapshot in it) {
                    val event = querySnapshot.toObject(EventPOJO::class.java)
                    listOfEvents.add(event)
                }
            }
            .addOnCompleteListener {
                fetchWithFiltersCount++
            }
            .addOnFailureListener {
                throw it
            }
            .await()
        return listOfEvents
    }

    private suspend fun fetchEventsWithoutFilters(
        country: String?
    ): ArrayList<EventPOJO> {
        if (country == null) return ArrayList()

        val listOfEvents = ArrayList<EventPOJO>()

        eventsQuery = if (lastEventDocumentLoaded == null) {
            fireStore.collection(eventCollection)
                .whereEqualTo(eventCountryField, country)
                .orderBy(eventDateField)
                .limit(fetchEventsLimitNumber)
        } else {
            fireStore.collection(eventCollection)
                .whereEqualTo(eventCountryField, country)
                .orderBy(eventDateField)
                .startAfter(lastEventDocumentLoaded!!)
                .limit(fetchEventsLimitNumber)
        }

        eventsQuery!!
            .get()
            .addOnSuccessListener {
                for (querySnapshot in it) {
                    val event = querySnapshot.toObject(EventPOJO::class.java)
                    listOfEvents.add(event)
                }
                if (it.size() > 0) {
                    lastEventDocumentLoaded = it.documents[it.size() - 1]
                }
            }
            .addOnFailureListener {
                throw it
            }
            .await()

        return listOfEvents
    }

    private suspend fun fetchEventsWithFilters(
        filtersList: ArrayList<IFilterItem>,
        country: String?,
        scaleType: ScaleTypeEnum?
    ): ArrayList<EventPOJO> {
        val listOfEvents = ArrayList<EventPOJO>()

        eventsQuery =
            fireStore.collection(eventCollection)
                .whereEqualTo(eventCountryField, country)
                .limit(fetchEventsLimitNumber * fetchWithFiltersCount)

        createFilterQuery(filtersList)

        val allTasks =
            Tasks.whenAllSuccess<QuerySnapshot>(tasksList)

        allTasks.addOnSuccessListener {
            for (queryDocumentSnapshot in it) {
                for (querySnapshot in queryDocumentSnapshot) {
                    val event = querySnapshot.toObject(EventPOJO::class.java)

                    if (EventHelper.eventPassFilters(
                            event, filtersList, sharedPreferencesService.readFromSharedPref<LatLng>(
                                SharedPreferencesKeys.userLocationKey,
                                LatLng::class.java
                            ),
                            scaleType!!
                        )
                    ) {
                        listOfEvents.add(event)
                    }
                }
            }
        }
            .addOnCompleteListener {
                fetchWithFiltersCount++
                tasksList.clear()
            }
            .addOnFailureListener {
                throw it
            }.await()

        return listOfEvents
    }

    private fun createFilterQuery(filtersList: ArrayList<IFilterItem>) {
        filtersList.forEach {
            when (it) {
                is EventTimeFilterPOJO -> {
                    setEventsTimeQuery(it)
                }
                is EventTypeFilterPOJO -> {
                    setEventsTypeQuery(it.eventType!!)
                }
                is EventDistanceFilter -> {
                    setEventsDistanceQuery(it.distance!!)
                }
            }
        }
    }

    private fun setEventsDistanceQuery(radiusInKM: Int) {
        val myCurrentUserLocation =
            sharedPreferencesService
                .readFromSharedPref<LatLng>(
                    SharedPreferencesKeys.userLocationKey,
                    LatLng::class.java
                )
        if (myCurrentUserLocation == null) {
            Log.d("Error", "Current User Location is null in SharedPref")
        }

        val center = LatLng(
            myCurrentUserLocation!!.latitude,
            myCurrentUserLocation.longitude
        )
        val greaterPoint = MapHelper.getGreaterPoint(radiusInKM, center)
        val lesserPoint = MapHelper.getLesserPoint(radiusInKM, center)

        val distanceQuery =
            eventsQuery?.whereGreaterThan(
                eventLocationField,
                GeoPoint(lesserPoint.latitude, lesserPoint.longitude)
            )!!
                .whereLessThan(
                    eventLocationField,
                    GeoPoint(
                        greaterPoint.latitude,
                        greaterPoint.longitude
                    )
                ).get()

        tasksList.add(distanceQuery)
    }

    private fun setEventsTimeQuery(timeFilter: EventTimeFilterPOJO) {

        val timeZone = TimeZone.getTimeZone("UTC")

        val deadlineCalendar = Calendar.getInstance(timeZone)

        val currentTimeCalendar = Calendar.getInstance()
        currentTimeCalendar.timeInMillis = System.currentTimeMillis()
        currentTimeCalendar.timeInMillis -= DateHelper.getOffsetTimeInMillis()

        val timeTask: Task<QuerySnapshot>?

        when (timeFilter.eventTime) {
            EventTimeFilterEnum.TODAY -> {

                deadlineCalendar.set(Calendar.HOUR_OF_DAY, 23)
                deadlineCalendar.set(Calendar.MINUTE, 59)
                deadlineCalendar.set(Calendar.SECOND, 59)
                deadlineCalendar.timeInMillis -= DateHelper.getOffsetTimeInMillis()

                timeTask = eventsQuery!!
                    .whereGreaterThanOrEqualTo(eventDateField, System.currentTimeMillis())
                    .whereLessThan(eventDateField, deadlineCalendar.timeInMillis).get()
            }

            EventTimeFilterEnum.THIS_WEEK -> {

                val weekOfYear = deadlineCalendar.get(Calendar.WEEK_OF_YEAR) + 1

                deadlineCalendar.set(
                    Calendar.WEEK_OF_YEAR,
                    weekOfYear
                )
                deadlineCalendar.set(Calendar.HOUR_OF_DAY, 0)
                deadlineCalendar.set(Calendar.MINUTE, 0)
                deadlineCalendar.set(Calendar.SECOND, 0)

                deadlineCalendar.timeInMillis -= DateHelper.getOffsetTimeInMillis()

                timeTask = eventsQuery!!
                    .whereGreaterThanOrEqualTo(eventDateField, System.currentTimeMillis())
                    .whereLessThan(eventDateField, deadlineCalendar.timeInMillis).get()

            }
            EventTimeFilterEnum.NEXT_WEEK -> {

                val currentWeek = Calendar.getInstance().get(Calendar.WEEK_OF_YEAR)
                deadlineCalendar.set(
                    Calendar.WEEK_OF_YEAR,
                    currentWeek + 2
                )
                deadlineCalendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY)
                deadlineCalendar.set(Calendar.HOUR_OF_DAY, 0)
                deadlineCalendar.set(Calendar.MINUTE, 0)
                deadlineCalendar.set(Calendar.SECOND, 0)
                deadlineCalendar.timeInMillis -= DateHelper.getOffsetTimeInMillis()

                val beginningOfTheNextWeekCalendar =
                    Calendar.getInstance(TimeZone.getTimeZone("UTC"))
                beginningOfTheNextWeekCalendar.set(
                    Calendar.WEEK_OF_YEAR,
                    currentWeek + 1
                )
                beginningOfTheNextWeekCalendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY)
                beginningOfTheNextWeekCalendar.set(Calendar.HOUR_OF_DAY, 0)
                beginningOfTheNextWeekCalendar.set(Calendar.MINUTE, 0)
                beginningOfTheNextWeekCalendar.set(Calendar.SECOND, 0)
                beginningOfTheNextWeekCalendar.timeInMillis -= DateHelper.getOffsetTimeInMillis()

                timeTask = eventsQuery!!
                    .whereLessThan(eventDateField, deadlineCalendar.timeInMillis)
                    .whereGreaterThan(eventDateField, beginningOfTheNextWeekCalendar.timeInMillis)
                    .get()
            }
            EventTimeFilterEnum.THIS_MONTH -> {

                deadlineCalendar.set(Calendar.MONTH, deadlineCalendar.get(Calendar.MONTH) + 1)
                deadlineCalendar.set(Calendar.DAY_OF_MONTH, 1)
                deadlineCalendar.set(Calendar.HOUR_OF_DAY, 0)
                deadlineCalendar.set(Calendar.MINUTE, 0)
                deadlineCalendar.set(Calendar.SECOND, 0)
                deadlineCalendar.timeInMillis -= DateHelper.getOffsetTimeInMillis()

                timeTask = eventsQuery!!
                    .whereGreaterThanOrEqualTo(eventDateField, currentTimeCalendar.timeInMillis)
                    .whereLessThan(eventDateField, deadlineCalendar.timeInMillis).get()
            }

            EventTimeFilterEnum.NEXT_MONTH -> {
                deadlineCalendar.set(Calendar.MONTH, deadlineCalendar.get(Calendar.MONTH) + 1)
                deadlineCalendar.set(Calendar.DAY_OF_MONTH, 1)
                deadlineCalendar.set(Calendar.HOUR_OF_DAY, 0)
                deadlineCalendar.set(Calendar.MINUTE, 0)
                deadlineCalendar.set(Calendar.SECOND, 0)
                deadlineCalendar.timeInMillis -= DateHelper.getOffsetTimeInMillis()

                timeTask = eventsQuery!!
                    .whereGreaterThanOrEqualTo(eventDateField, deadlineCalendar.timeInMillis).get()
            }
            else -> timeTask =
                eventsQuery!!
                    .whereLessThan(eventDateField, System.currentTimeMillis()).get()
        }

        tasksList.add(timeTask)
    }

    private fun setEventsTypeQuery(eventType: EventType) {
        val typeTask = eventsQuery!!.whereEqualTo(eventTypeField, eventType).get()
        tasksList.add(typeTask)
    }

    override suspend fun removeUserEvent(userID: String, eventId: String): Boolean {

        val listOfUserEvents: ArrayList<UserEventPOJO> = fetchUserEvents(userID)
        try {
            for (event in listOfUserEvents) {
                if (event.eventKey.equals(eventId)) {
                    val task: Task<Void>?
                    listOfUserEvents.remove(event)

                    task = fireStore.collection(userEventsCollection)
                        .document(userID)
                        .set(UserEventsListPOJO(listOfUserEvents))

                    if (connectivityService.isInternetAvailable()) {
                        task.await()
                        return task.isSuccessful
                    }
                    return true
                }
            }
        } catch (e: Exception) {
            Log.d("Error remove user event", e.message.toString())
        }
        return false
    }

    override suspend fun updateEvent(eventItem: EventPOJO) {
        fireStore.collection(eventCollection)
            .document(eventItem.uid!!)
            .set(eventItem)
            .addOnFailureListener {
                Log.d("pulamea", it.message.toString())
                throw it
            }
    }

    override suspend fun joinEvent(eventKey: String, eventMember: EventMembersPOJO): Boolean {
        val task = fireStore.collection(eventCollection)
            .document(eventKey)
            .collection(eventMembersCollection)
            .document(eventMember.userKey!!)
            .set(eventMember)
            .addOnFailureListener { throw it }

        if (connectivityService.isInternetAvailable()) {
            task.await()
            return task.isSuccessful
        }
        return true
    }

    override suspend fun leaveEvent(eventKey: String, eventMember: EventMembersPOJO): Boolean {
        val task = fireStore.collection(eventCollection)
            .document(eventKey)
            .collection(eventMembersCollection)
            .document(eventMember.userKey!!)
            .delete()
            .addOnFailureListener { throw it }

        if (connectivityService.isInternetAvailable()) {
            task.await()
            return task.isSuccessful
        }
        return true

    }

    override suspend fun getEventMembersCount(eventId: String): Int? {
        val querySnapshot = fireStore.collection(eventCollection)
            .document(eventId)
            .collection(eventMembersCollection)
            .get().await()

        return querySnapshot?.documents?.count() ?: -1
    }

    override suspend fun removeAllUserEvents(userID: String) {
        fetchUserEvents(userID).forEach {
            if(it.eventState != EventState.HOSTING) {
                removeUserEvent(userID, it.eventKey!!)
                leaveEvent(it.eventKey!!, EventMembersPOJO(userID))
            }
        }
    }

    override fun resetLastEventDocument() {
        lastEventDocumentLoaded = null
        fetchWithFiltersCount = 1
    }

    private val defaultDataSource: Source
        get() {
            if (!connectivityService.isInternetAvailable()) {
                return Source.CACHE
            }
            return Source.DEFAULT
        }
}



