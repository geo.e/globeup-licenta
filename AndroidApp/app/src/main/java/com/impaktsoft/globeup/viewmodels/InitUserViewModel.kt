package com.impaktsoft.globeup.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.models.CountryPOJO
import com.impaktsoft.globeup.models.GReason
import com.impaktsoft.globeup.models.UserInfo
import com.impaktsoft.globeup.views.activities.MainActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception

class InitUserViewModel : BaseViewModel() {

    var placeholderText = MutableLiveData<String>()
    var isPlaceholderVisible = MutableLiveData<Boolean>()
    var isProgressBarVisible = MutableLiveData<Boolean>(false)
    var isRetryButtonVisible = MutableLiveData<Boolean>()
    var userCountry: CountryPOJO? = null

    private var addInDbHasFailed = false

    init {
        userCountry = dataExchangeService.get<CountryPOJO>(SelectCountryViewModel.countryKey)
        checkUserConnection()
    }


    fun retryConnection() {
        if (addInDbHasFailed) {
            viewModelScope.launch(Dispatchers.IO) {
                withContext(Dispatchers.Main)
                {
                    isProgressBarVisible.value = true
                }
                addUserInDB()
            }
        } else {
            showPlaceholder(false, "", false)
            checkUserConnection()
        }
    }

    private fun checkUserConnection() {

        viewModelScope.launch(Dispatchers.IO) {
            val result = backend.signInAnonymously()
            if (result.isSuccess) {
                withContext(Dispatchers.Main)
                {
                    isProgressBarVisible.value = true
                }
                addUserInDB()
            } else {
                if (result.error?.reason == GReason.NoConnection) {
                    withContext(Dispatchers.Main) {

                        showPlaceholder(
                            true,
                            resourceService.stringForId(R.string.err_no_internet),
                            true
                        )

                        dialogService.showSnackbar(R.string.err_no_internet)
                    }
                } else {
                    withContext(Dispatchers.Main) {
                        showPlaceholder(true, result.error?.debugMessage, true)
                    }
                }
            }
        }
    }

    private suspend fun addUserInDB() {
        val userInfo = UserInfo(
            "",
            resourceService.stringForId(R.string.anonymous),
            null,
            mapService.getCountryCode(userCountry?.countryName!!)
        )
        try {
            database.userInfoDao.store(backend.currentUser?.uid!!, userInfo)
            withContext(Dispatchers.Main)
            {
                isProgressBarVisible.value=false
                navigateToDashboard()
            }
        } catch (e: Exception) {
            showPlaceholder(true, resourceService.stringForId(R.string.warning_text), true)
        }
    }

    private fun showPlaceholder(
        isPlaceHolderVisible: Boolean,
        placeHolderString: String?,
        isRetryButtonVisible: Boolean? = null
    ) {
        this.isPlaceholderVisible.value = isPlaceHolderVisible

        placeholderText.value = placeHolderString

        if (isRetryButtonVisible != null) {
            this.isRetryButtonVisible.value = isRetryButtonVisible
        }
    }

    private fun navigateToDashboard() {
        navigation.navigateToActivity(MainActivity::class.java, true)
    }
}