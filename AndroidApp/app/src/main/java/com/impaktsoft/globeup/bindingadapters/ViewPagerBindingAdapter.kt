package com.impaktsoft.globeup.bindingadapters

import android.util.Log
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import androidx.lifecycle.MutableLiveData
import androidx.viewpager.widget.ViewPager
import com.impaktsoft.globeup.listadapters.ImageSwiperAdaper

object ViewPagerBindingAdapter {

    @BindingAdapter("cb_swiperImages")
    @JvmStatic
    fun ViewPager.setAdapter(images: ArrayList<Int>) {
        this.adapter = ImageSwiperAdaper(context, images)
    }

    @BindingAdapter("cb_swipeToPosition")
    @JvmStatic
    fun ViewPager.swipeTo(position: Int) {
        this.currentItem = position
    }


    @BindingAdapter("cb_swiperCurrentImage")
    @JvmStatic
    fun ViewPager.getCurrentItem(currentItem: MutableLiveData<Int>) {

        this.setOnPageChangeListener(object:ViewPager.OnPageChangeListener
        {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
               currentItem.value=position
            }

        })
    }

}
