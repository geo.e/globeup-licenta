package com.impaktsoft.globeup.models

class GlobalUserPOJO :IGlobalBaseItem{
    override var rank:Int?=null
    override var events: Int?=null
    override var experience: Int?=null
    override var name: String?=null
    var address:String?=null


    constructor(events: Int?, experience: Int?, name: String?,rank:Int?,address: String?) {
        this.events = events
        this.experience = experience
        this.name = name
        this.address = address
        this.rank=rank
    }
}