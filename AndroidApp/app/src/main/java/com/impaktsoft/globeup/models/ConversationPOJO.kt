package com.impaktsoft.globeup.models

class ConversationPOJO {
    var lastMessage: MessagePOJO?= null
    var key:String=""
    var name:String=""
    var conversationImage:Int?=null

    constructor(key:String,name:String,lastMessage:MessagePOJO,conversationImage:Int){
        this.key=key
        this.name=name
        this.lastMessage=lastMessage
        this.conversationImage=conversationImage
    }
    constructor()
}