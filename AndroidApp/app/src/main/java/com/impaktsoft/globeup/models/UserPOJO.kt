package com.impaktsoft.globeup.models

class UserPOJO {
    //TODO remove it
    var email:String = ""
    var name:String = ""
    var imgUrl:String = ""
    var id:String = ""
    var lastOnline:Long=0

    constructor()
    {

    }

    constructor(id: String,email: String, name: String, imgUrl: String) {
        this.email = email
        this.name = name
        this.imgUrl = imgUrl
        this.id = id
        lastOnline= System.currentTimeMillis();
    }
}