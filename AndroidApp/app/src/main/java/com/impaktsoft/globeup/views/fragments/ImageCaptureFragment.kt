package com.impaktsoft.globeup.views.fragments

import android.os.Bundle
import android.view.TextureView
import android.view.View
import android.widget.ImageButton
import androidx.lifecycle.Observer
import androidx.lifecycle.viewModelScope
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.databinding.FragmentImageCaptureBinding
import com.impaktsoft.globeup.util.camera.ProfileSelfieCamera
import com.impaktsoft.globeup.viewmodels.ImageCaptureViewModel
import kotlinx.android.synthetic.main.fragment_image_capture.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext


class ImageCaptureFragment : BaseBoundFragment<ImageCaptureViewModel, FragmentImageCaptureBinding>(
    ImageCaptureViewModel::class
) {
    private lateinit var previewTexture: TextureView
    private lateinit var captureButton: ImageButton

    private lateinit var selfieCamera: ProfileSelfieCamera

    override var layoutId: Int = R.layout.fragment_image_capture
    override val fragmentNameResourceID: Int = R.string.image_capture_name

    override fun setupDataBinding(binding: FragmentImageCaptureBinding) {
        binding.viewModel = mViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        mViewModel.hidePreviewView.observe(this, Observer {
            if (preview_image_layout.visibility == View.VISIBLE) {
                hidePreviewImageLayout()
            }
        })
    }

    override fun layoutInflated(root: View) {
        previewTexture = root.findViewById(R.id.texture_view)
        captureButton = root.findViewById(R.id.capture_button)

        selfieCamera = ProfileSelfieCamera()

        captureButton.setOnClickListener {
            mViewModel.viewModelScope.launch(Dispatchers.IO) {
                try {
                    val file = selfieCamera.takePicture(activity!!.applicationContext).await()
                    withContext(Dispatchers.Main) {
                        mViewModel.setMyImageFile(file)
                        showPreviewImageLayout()
                    }
                } catch (e: Exception) {
                    mViewModel.captureImageFailed()
                }
            }
        }

        previewTexture.post {
            selfieCamera.setup(this, previewTexture)
        }

        super.layoutInflated(root)
    }


    fun showPreviewImageLayout() {
        take_picture_layout.visibility = View.GONE
        preview_image_layout.visibility = View.VISIBLE

    }

    fun hidePreviewImageLayout() {
        take_picture_layout.visibility = View.VISIBLE
        preview_image_layout.visibility = View.GONE
    }
}

