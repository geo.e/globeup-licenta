package com.impaktsoft.globeup.models

class ExperiencePOJO {
    var title:String?=null
    var exp:String?=null

    constructor(title: String?, exp: String?) {
        this.title = title
        this.exp = exp
    }
}