package com.impaktsoft.globeup.views.activities

import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.databinding.FragmentForgetPasswordBinding
import com.impaktsoft.globeup.viewmodels.ForgetPasswordViewModel
import com.impaktsoft.globeup.views.activities.BaseActivity

class ForgetPasswordActivity : BaseBoundActivity<ForgetPasswordViewModel, FragmentForgetPasswordBinding>(ForgetPasswordViewModel::class) {

    override var layoutId: Int= R.layout.fragment_forget_password
    override var activityTitleResourceId: Int? = R.string.forget_password_name

    override fun setupDataBinding(binding: FragmentForgetPasswordBinding) {
        binding.viewModel=mViewModel
    }

}
