package com.impaktsoft.globeup.services

import android.content.Context
import android.view.View
import android.widget.PopupMenu
import androidx.appcompat.app.AppCompatActivity
import com.impaktsoft.globeup.models.PopUpMenuSettingsPOJO

interface IPopUpMenuService {
    fun initializeMenu(settings: PopUpMenuSettingsPOJO)
    fun showMenu()
}

class PopUpMenuService(
    private val currentActivityService: ICurrentActivityService
) : IPopUpMenuService {

    private var popUpMenu: PopupMenu? = null
    private var menuSettings: PopUpMenuSettingsPOJO? = null

    override fun initializeMenu(settings: PopUpMenuSettingsPOJO) {
        this.menuSettings = settings
        val activity = currentActivityService.activity

        val fragmentManager = (activity as AppCompatActivity).supportFragmentManager
        val fragment = fragmentManager.fragments.last()
        val view = fragment?.view?.findViewById<View>(settings.anchorViewId!!)

        popUpMenu = PopupMenu((activity as Context), view)
        popUpMenu?.inflate(settings.menuId!!)

        setupMenuOptions()
    }

    private fun setupMenuOptions() {

        popUpMenu!!.setOnMenuItemClickListener {
            when (it.itemId) {
                it.itemId -> {
                    menuSettings?.methods!![it.itemId]!!.invoke()
                    true
                }
                else -> false
            }
        }
    }

    override fun showMenu() {
        popUpMenu?.show()
    }
}