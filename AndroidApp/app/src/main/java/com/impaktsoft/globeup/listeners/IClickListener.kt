package com.impaktsoft.globeup.listeners

interface IClickListener
{
    fun clicked()
}