package com.impaktsoft.globeup.bindingadapters

import android.annotation.SuppressLint
import android.content.res.ColorStateList
import android.graphics.drawable.Drawable
import android.os.Build
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.annotation.RequiresApi
import androidx.core.widget.ImageViewCompat
import androidx.databinding.BindingAdapter
import com.google.gson.Gson
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.enums.*
import com.impaktsoft.globeup.models.*
import com.squareup.picasso.Picasso
import java.io.File

object ImageViewBindingAdapters {
    @BindingAdapter("cb_imageUrl", "cb_placeholder", requireAll = true)
    @JvmStatic
    fun ImageView.imageUrl(imageUrl: String?, placeHolder: Drawable) {
        if (imageUrl == null) {
            this.setImageDrawable(placeHolder)
        } else {
            Picasso.get()
                .load(imageUrl)
                .placeholder(placeHolder)
                .error(placeHolder)
                .into(this)
        }
    }

    @BindingAdapter("cb_filePath")
    @JvmStatic
    fun ImageView.file(filePath: String?) {
        if (filePath != null) {
            Picasso.get()
                .load(File(filePath))
                .into(this)
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    @SuppressLint("ResourceAsColor")
    @BindingAdapter("cb_eventImageTint", "cb_currentViewEventState")
    @JvmStatic
    fun ImageView.setEventImageTint(
        itemEventState: EventState?,
        currentViewEventState: EventState?
    ) {
        if (itemEventState == currentViewEventState || itemEventState == EventState.HOSTING) {
            ImageViewCompat.setImageTintList(
                this,
                ColorStateList.valueOf(context.getColor(R.color.colorPrimary))
            )
        } else {
            ImageViewCompat.setImageTintList(
                this,
                ColorStateList.valueOf(context.getColor(R.color.colorPrimaryDark))
            )
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    @BindingAdapter("cb_FieldImageTint")
    @JvmStatic
    fun ImageView.setFieldImageTint(boolean: Boolean) {
        if (boolean) {
            ImageViewCompat.setImageTintList(
                this,
                ColorStateList.valueOf(context.getColor(R.color.colorPrimary))
            )
        } else {
            ImageViewCompat.setImageTintList(
                this,
                ColorStateList.valueOf(context.getColor(R.color.grey))
            )
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    @BindingAdapter("cb_transcationImage")
    @JvmStatic
    fun setTransactionImage(imageView: ImageView, transactionType: TransactionType) {
        if (transactionType == TransactionType.EVENT_DONATION) {
            imageView.setImageResource(R.drawable.ic_donation_for_event)
        }
        if (transactionType == TransactionType.GLOBAL_DONATION) {
            imageView.setImageResource(R.drawable.ic_donate_event)

        }
        if (transactionType == TransactionType.GETTING_GCOIN) {
            imageView.setImageResource(R.drawable.ic_gcoin)

        }
        if (transactionType == TransactionType.WITHDRAW) {
            imageView.setImageResource(R.drawable.ic_withdraw)

        }
    }


    @RequiresApi(Build.VERSION_CODES.M)
    @BindingAdapter("cb_actionImage")
    @JvmStatic
    fun ImageView.setActionImage(action: ActionPOJO) {
        if (action.actionType == ActionType.HISTORY) {
            this.setImageResource(R.drawable.ic_history)
        }
        if (action.actionType == ActionType.INVITE_FRIENDS) {
            this.setImageResource(R.drawable.ic_new_group)

        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    @BindingAdapter("cb_levelImage")
    @JvmStatic
    fun ImageView.setLevelImage(levelPOJO: LevelPOJO) {
        when (levelPOJO.levelType) {
            LevelType.GREENHORN -> {
                this.setImageResource(R.drawable.ic_award_10)
            }
            LevelType.BRONZE1 -> {
                this.setImageResource(R.drawable.ic_award_9)

            }
            LevelType.BRONZE2 -> {
                this.setImageResource(R.drawable.ic_award_8)

            }
            LevelType.BRONZE3 -> {
                this.setImageResource(R.drawable.ic_award_7)

            }
            LevelType.SILVER1 -> {
                this.setImageResource(R.drawable.ic_award_6)

            }
            LevelType.SILVER2 -> {
                this.setImageResource(R.drawable.ic_award_5)

            }
            LevelType.SILVER3 -> {
                this.setImageResource(R.drawable.ic_award_4)

            }
            LevelType.GOLD1 -> {
                this.setImageResource(R.drawable.ic_award_3)

            }
            LevelType.GOLD2 -> {
                this.setImageResource(R.drawable.ic_award_2)

            }
            LevelType.GOLD3 -> {
                this.setImageResource(R.drawable.ic_award_1)

            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    @BindingAdapter("cb_withdrawImage")
    @JvmStatic
    fun ImageView.setImage(withdrawPOJO: WithdrawPOJO) {
        //TODO sa fac pt poze cand am
        this.setImageResource(R.drawable.ic_withdraw)
    }

    @BindingAdapter("cb_messageImageUrl")
    @JvmStatic
    fun ImageView.messageImageUrl(messagePOJO: MessagePOJO) {


        var imageUrl: String? = null
        if (messagePOJO.type == MessageType.ImageMessage) {
            val json = Gson().fromJson(messagePOJO.jsonMessage, MessageImagePOJO::class.java)
            imageUrl = json.imageUrl

        } else if (messagePOJO.type == MessageType.LocationMessage) {
            val json = Gson().fromJson(messagePOJO.jsonMessage, MessageLocationPOJO::class.java)
            imageUrl = json.imageUrl

        }

        if (!imageUrl.isNullOrEmpty()) {
            Picasso.get()
                .load(imageUrl)
                .centerCrop(Gravity.CENTER)
                .fit()
                .into(this)
        } else {
            this.setImage(R.drawable.common_full_open_on_phone)
        }
    }

    @BindingAdapter("cb_messageState")
    @JvmStatic
    fun ImageView.messageState(messageState: LastMessageState) {
        when (messageState.messageState) {
            MessageState.NOT_SEND -> this.setImageResource(R.drawable.ic_not_send)
            MessageState.RECEIVED -> this.setImageResource(R.drawable.ic_send)
            MessageState.SEEN -> this.setImageResource(R.drawable.ic_seen)
        }
    }

    @BindingAdapter("cb_imageResource")
    @JvmStatic
    fun ImageView.setImage(imageResource: Int) {
        this.setImageResource(imageResource)
    }

    @BindingAdapter(value = ["cb_isMessageImageVisible"])
    @JvmStatic
    fun ImageView.setVisibilityDependingOnState(messagePOJO: MessagePOJO) {
        if (messagePOJO.type == MessageType.ImageMessage) {
            val imageMessage =
                Gson().fromJson(messagePOJO.jsonMessage, MessageImagePOJO::class.java)
            if (imageMessage.imageUrl.isEmpty()) {
                this.visibility = View.GONE
            } else {
                this.visibility = View.VISIBLE
            }
        }
    }
}