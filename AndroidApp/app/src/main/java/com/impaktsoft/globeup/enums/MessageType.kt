package com.impaktsoft.globeup.enums

enum class MessageType {
    TextMessage,LocationMessage,ImageMessage
}