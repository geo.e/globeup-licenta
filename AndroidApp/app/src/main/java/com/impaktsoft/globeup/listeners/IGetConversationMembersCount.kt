package com.impaktsoft.globeup.listeners

interface IGetConversationMembersCount {
    fun getConversationMembersCount(count:Int)
}