package com.impaktsoft.globeup.dummy

import com.impaktsoft.globeup.models.GCoinPOJO

class GCoinsOffertDummyData {
    companion object {
        val listOfOfferts = listOf(
            GCoinPOJO(1000),
            GCoinPOJO(5000),
            GCoinPOJO(10000),
            GCoinPOJO(50000),
            GCoinPOJO(70000),
            GCoinPOJO(100000)

        )
    }
}