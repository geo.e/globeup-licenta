package com.impaktsoft.globeup.views.fragments

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getColor
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IValueFormatter
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.utils.ViewPortHandler
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.databinding.FragmentWalletBinding
import com.impaktsoft.globeup.dummy.ChartDummyData
import com.impaktsoft.globeup.util.MyChartValueFormatter
import com.impaktsoft.globeup.viewmodels.WalletViewModel
import kotlinx.android.synthetic.main.fragment_wallet.*
import java.text.DateFormatSymbols
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

class WalletBoundFragment :
    BaseBoundFragment<WalletViewModel, FragmentWalletBinding>(WalletViewModel::class) {

    override var layoutId: Int = R.layout.fragment_wallet
    override val fragmentNameResourceID: Int = R.string.wallet_bound_name
    private var chart: LineChart? = null
    private var dataTransformated=ArrayList<Entry>()
    private var data=ArrayList<Entry>()

    override fun setupDataBinding(binding: FragmentWalletBinding) {
        binding.viewModel = mViewModel
    }

    @RequiresApi(Build.VERSION_CODES.M)
    @SuppressLint("ResourceAsColor")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tranzactions_list.layoutManager =
            LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        tranzactions_list.adapter = mViewModel.adapter

        chart = chart_wallet

        data=getEntryesFromDatesAndValues(ChartDummyData.dates,ChartDummyData.values)
        dataTransformated=transform(ChartDummyData.dates,ChartDummyData.values)

        configChart()
        configXAxis()
        disableLeftAxis()
        disableRightAxis()

        //setData
        setData(dataTransformated)

    }

    fun configChart() {
        chart?.description!!.isEnabled = false
        chart?.zoom(1f, 1f, 1f, 1f)
        chart?.setTouchEnabled(false)
        chart?.setBackgroundColor(Color.WHITE)
        chart?.setViewPortOffsets(30f, 30f, 30f, 30f)
        val l = chart?.legend
        l?.isEnabled = false
    }

    fun configXAxis() {

        val xAxis = chart?.xAxis
        xAxis?.position = XAxis.XAxisPosition.BOTTOM_INSIDE
        //xAxis?.textSize = 20f
        xAxis?.setDrawAxisLine(false)
        xAxis?.setDrawGridLines(false)
        xAxis?.textColor = getColor(context!!, R.color.grey)
        xAxis?.axisMinimum= 0f
        //xAxis?.valueFormatter = MyChartValueFormatter(data) as ValueFormatter

    }

    fun disableLeftAxis() {
        val leftAxis = chart?.axisLeft
        leftAxis?.isEnabled = false
    }

    fun disableRightAxis() {
        val rightAxis = chart?.axisRight
        rightAxis?.isEnabled = false
    }

    internal fun getMonthForInt(num: Int): String {
        var month = "wrong"
        val dfs = DateFormatSymbols()
        val months = dfs.months
        if (num in 0..11) {
            month = months[num]
        }

        return month.substring(0, 3)

    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun setData(values: ArrayList<Entry>) {

        // create a dataset and give it a type
        val set1 = LineDataSet(values, "DataSet 1")
        set1.axisDependency = YAxis.AxisDependency.LEFT
        set1.color = context!!.getColor(R.color.colorAccent)
        set1.setDrawFilled(true)
        val drawable = ContextCompat.getDrawable(context!!, R.drawable.fade_green)
        set1.fillDrawable = drawable
        set1.valueTextColor = context!!.getColor(R.color.colorAccent)
        set1.lineWidth = 1.5f
        //cerc
        set1.setDrawValues(true)
        set1.setDrawCircles(true)
        set1.setDrawCircleHole(true)
        set1.circleRadius = 4f
        set1.setCircleColor(context!!.getColor(R.color.colorAccent))

        // create a data object with the data sets
        val data = LineData(set1)
        data.setValueTextColor(Color.BLACK)
        data.setValueTextSize(9f)

        // set data
        chart_wallet.data = data
    }

    fun getEntryesFromDatesAndValues(dates:List<Date>,values:List<Int>): ArrayList<Entry> {
        val listOfEntryes = ArrayList<Entry>()

        for (i in 0..dates.size-1) {
            val x = dates[i].day!!
                Log.d("woooow",getMonthForInt(dates!![i].month))
                listOfEntryes.add(Entry(dates[i].time.toFloat(), values[i].toFloat()))
        }

        return listOfEntryes
    }

    fun transform(dates:List<Date>,values:List<Int>): ArrayList<Entry>
    {
        var listOfEntryes = ArrayList<Entry>()

        for (i in 0..dates.size-1) {
            val x = dates[i].time-dates.first().time
            Log.d("pulamea",TimeUnit.MILLISECONDS.toDays(x).toString())
            listOfEntryes.add(Entry(TimeUnit.MILLISECONDS.toDays(x).toFloat(), values[i].toFloat()))
        }

        return listOfEntryes

    }


}