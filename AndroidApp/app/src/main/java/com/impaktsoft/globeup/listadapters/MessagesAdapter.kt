package com.impaktsoft.globeup.listadapters

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.impaktsoft.globeup.databinding.*
import com.impaktsoft.globeup.enums.MessageType
import com.impaktsoft.globeup.models.*
import com.impaktsoft.globeup.viewmodels.MessengerViewModel

class MessagesAdapter(
    myList: ArrayList<IMessagePOJO>,
    currentUserID: String,
    val viewModel: MessengerViewModel
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var currentUserID: String = currentUserID

    companion object {

        private const val TYPE_MESSAGE_TEXT_FRIEND = 0
        private const val TYPE_MESSAGE_TEXT_USER = 1
        private const val TYPE_MESSAGE_IMAGE_FRIEND = 2
        private const val TYPE_MESSAGE_IMAGE_USER = 3
        private const val TYPE_MESSAGE_LOCATION_FRIEND = 4
        private const val TYPE_MESSAGE_LOCATION_USER = 5
        private const val TYPE_MESSAGE_STATE = 6
        private const val TYPE_MESSAGE_SECTION = 7

    }

    var myList: ArrayList<IMessagePOJO>? = myList;

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return when (viewType) {
            TYPE_MESSAGE_TEXT_FRIEND -> {
                val layoutInflater = LayoutInflater.from(parent.context)
                val applicationBinding =
                    FriendTextMessageCellBinding.inflate(layoutInflater, parent, false)
                return FriendTextMessageViewHolder(applicationBinding)
            }
            TYPE_MESSAGE_TEXT_USER -> {
                val layoutInflater = LayoutInflater.from(parent.context)
                val applicationBinding =
                    UserTextMessageCellBinding.inflate(layoutInflater, parent, false)
                return UserTextMessageViewHolder(applicationBinding)
            }

            TYPE_MESSAGE_LOCATION_FRIEND -> {
                val layoutInflater = LayoutInflater.from(parent.context)
                val applicationBinding =
                    FriendLocationMessageCellBinding.inflate(layoutInflater, parent, false)
                return FriendLocationMessageViewHolder(applicationBinding)
            }

            TYPE_MESSAGE_LOCATION_USER -> {
                val layoutInflater = LayoutInflater.from(parent.context)
                val applicationBinding =
                    UserLocationMessageCellBinding.inflate(layoutInflater, parent, false)
                return UserLocationMessageViewHolder(applicationBinding)
            }

            TYPE_MESSAGE_IMAGE_FRIEND -> {
                val layoutInflater = LayoutInflater.from(parent.context)
                val applicationBinding =
                    FriendImageMessageCellBinding.inflate(layoutInflater, parent, false)
                return FriendImageMessageViewHolder(applicationBinding)
            }

            TYPE_MESSAGE_IMAGE_USER -> {
                val layoutInflater = LayoutInflater.from(parent.context)
                val applicationBinding =
                    UserImageMessageCellBinding.inflate(layoutInflater, parent, false)
                return UserImageMessageViewHolder(applicationBinding)
            }
            TYPE_MESSAGE_STATE -> {
                val layoutInflater = LayoutInflater.from(parent.context)
                val applicationBinding =
                    UserMessageStateBinding.inflate(layoutInflater, parent, false)
                return MessageStateViewHolder(applicationBinding)
            }
            TYPE_MESSAGE_SECTION -> {
                val layoutInflater = LayoutInflater.from(parent.context)
                val applicationBinding =
                    MessangerTimeSectionBinding.inflate(layoutInflater, parent, false)
                return MessageTimeSectionViewHolder(applicationBinding)
            }
            else -> throw IllegalArgumentException("Invalid view type") as Throwable
        }
    }

    override fun getItemCount(): Int {
        return myList!!.size;
    }

    override fun getItemViewType(position: Int): Int {

        val obj = myList!![position]
        if (obj is MessagePOJO) {
            if (obj.senderKey == currentUserID) {

                if (obj.type == MessageType.TextMessage) {
                    return TYPE_MESSAGE_TEXT_USER
                } else if (obj.type == MessageType.ImageMessage) {
                    return TYPE_MESSAGE_IMAGE_USER
                } else {
                    return TYPE_MESSAGE_LOCATION_USER
                }

            } else {
                if (obj.type == MessageType.TextMessage) {
                    return TYPE_MESSAGE_TEXT_FRIEND
                } else if (obj.type == MessageType.ImageMessage) {
                    return TYPE_MESSAGE_IMAGE_FRIEND
                } else {
                    return TYPE_MESSAGE_LOCATION_FRIEND
                }
            }
        } else if (obj is LastMessageState) {
            return TYPE_MESSAGE_STATE
        } else return TYPE_MESSAGE_SECTION
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val myObj = myList!![position];
        when (holder) {

            is UserTextMessageViewHolder -> holder.bind(myObj as MessagePOJO)
            is FriendTextMessageViewHolder -> holder.bind(myObj as MessagePOJO)

            is UserImageMessageViewHolder -> holder.bind(myObj as MessagePOJO)
            is FriendImageMessageViewHolder -> holder.bind(myObj as MessagePOJO)

            is UserLocationMessageViewHolder -> holder.bind(myObj as MessagePOJO)
            is FriendLocationMessageViewHolder -> holder.bind(myObj as MessagePOJO)

            is MessageStateViewHolder -> holder.bind(myObj as LastMessageState)
            is MessageTimeSectionViewHolder -> holder.bind(myObj as MessageTimeSection)
        }
    }

    inner class UserTextMessageViewHolder(var applicationBinding: UserTextMessageCellBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(item: MessagePOJO) {
            applicationBinding.messageItem = item
        }
    }

    inner class FriendTextMessageViewHolder(var applicationBinding: FriendTextMessageCellBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(item: MessagePOJO) {

            applicationBinding.messageItem = item
            applicationBinding.userInfo = viewModel.getUserInfo(item.senderKey)
        }
    }

    inner class UserLocationMessageViewHolder(var applicationBinding: UserLocationMessageCellBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(item: MessagePOJO) {

            applicationBinding.itemMessage = item
            val locationMessage = Gson().fromJson(item.jsonMessage, MessageLocationPOJO::class.java)
            applicationBinding.locationCellLayout.setOnClickListener {
                viewModel.showDestinationOnGoogleMap(
                    LatLng(
                        locationMessage.latitude,
                        locationMessage.longitude
                    )
                )
            }

        }
    }

    inner class FriendLocationMessageViewHolder(var applicationBinding: FriendLocationMessageCellBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(item: MessagePOJO) {

            applicationBinding.userInfo = viewModel.getUserInfo(item.senderKey)

            applicationBinding.messageItem = item
            val locationMessage = Gson().fromJson(item.jsonMessage, MessageLocationPOJO::class.java)
            applicationBinding.locationCellLayout.setOnClickListener {
                viewModel.showDestinationOnGoogleMap(
                    LatLng(
                        locationMessage.latitude,
                        locationMessage.longitude
                    )
                )
            }
        }
    }

    inner class UserImageMessageViewHolder(var applicationBinding: UserImageMessageCellBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(item: MessagePOJO) {

            applicationBinding.itemMessage = item
            applicationBinding.layout1.setOnClickListener {
                viewModel.previewImage(
                    item
                )
            }
        }
    }

    inner class FriendImageMessageViewHolder(var applicationBinding: FriendImageMessageCellBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(item: MessagePOJO) {
            applicationBinding.messageItem = item
            applicationBinding.userInfo = viewModel.getUserInfo(item.senderKey)
            applicationBinding.image.setOnClickListener {
                viewModel.previewImage(
                    item
                )
            }
        }
    }

    inner class MessageStateViewHolder(var applicationBinding: UserMessageStateBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {
        fun bind(item: LastMessageState) {
            applicationBinding.itemMessage = item
        }
    }

    inner class MessageTimeSectionViewHolder(var applicationBinding: MessangerTimeSectionBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {
        fun bind(item: MessageTimeSection) {
            applicationBinding.messageSection = item
        }
    }
}
