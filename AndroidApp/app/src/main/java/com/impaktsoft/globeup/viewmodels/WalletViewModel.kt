package com.impaktsoft.globeup.viewmodels

import androidx.lifecycle.MutableLiveData
import com.impaktsoft.globeup.dummy.TransactionDummyData
import com.impaktsoft.globeup.listadapters.TransactionAdapter
import com.impaktsoft.globeup.views.activities.BuyGCoinActivity
import com.impaktsoft.globeup.views.activities.GetCouponActivity
import com.impaktsoft.globeup.views.activities.WithdrawActivity

class WalletViewModel : BaseViewModel() {

    var text = MutableLiveData<String>("This is the wallet fragment")
    var adapter:TransactionAdapter?=null

    init {
        adapter= TransactionAdapter(TransactionDummyData.transactionList)
    }

    fun buyGcoinClick()
    {
        navigation.navigateToActivity(BuyGCoinActivity::class.java,false)
    }

    fun getCouponClick()
    {
        navigation.navigateToActivity(GetCouponActivity::class.java,false)
    }
    fun withdrawClick()
    {
        navigation.navigateToActivity(WithdrawActivity::class.java,false)

    }

}