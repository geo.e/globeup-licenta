package com.impaktsoft.globeup.views.fragments

import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.databinding.FragmentDeleteAccountBinding
import com.impaktsoft.globeup.viewmodels.DeleteAccountViewModel

class DeleteAccountFragment :
    BaseBoundFragment<DeleteAccountViewModel, FragmentDeleteAccountBinding>(
        DeleteAccountViewModel::class
    ) {

    override var layoutId: Int = R.layout.fragment_delete_account
    override val fragmentNameResourceID: Int = R.string.delete_accout

    override fun setupDataBinding(binding: FragmentDeleteAccountBinding) {
        binding.viewModel = mViewModel
    }

}
