package com.impaktsoft.globeup.listeners

import com.impaktsoft.globeup.models.ConversationMemberPOJO

interface IGetMembersKeyListener {
    fun getMembersKeyListener(membersKey:ArrayList<ConversationMemberPOJO>)
}