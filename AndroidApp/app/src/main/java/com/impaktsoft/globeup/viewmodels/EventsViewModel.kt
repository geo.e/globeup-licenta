package com.impaktsoft.globeup.viewmodels

import android.Manifest
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.firebase.firestore.GeoPoint
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.enums.EventState
import com.impaktsoft.globeup.enums.EventType
import com.impaktsoft.globeup.enums.ScaleTypeEnum
import com.impaktsoft.globeup.listadapters.EventsAdapter
import com.impaktsoft.globeup.listadapters.FilterAppliedAdapter
import com.impaktsoft.globeup.listeners.IClickListener
import com.impaktsoft.globeup.models.*
import com.impaktsoft.globeup.services.PermissionStatus
import com.impaktsoft.globeup.views.activities.AddEventActivity
import com.impaktsoft.globeup.views.activities.SearchEventsByNameActivity
import com.impaktsoft.globeup.views.fragments.FilterEventsByRadiusFragment
import com.impaktsoft.globeup.views.fragments.FilterEventsFragment
import com.impaktsoft.globeup.views.activities.RegisterActivity
import com.impaktsoft.globeup.views.fragments.SelectCountryFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList
import kotlin.coroutines.suspendCoroutine

class EventsViewModel : BaseEventsViewModel() {

    var filterAppliedAdapter: FilterAppliedAdapter? = null
    var isFetchMoreEventsEnable = false
    var loadingPosition = MutableLiveData(0)
    var currentCountryBinding = MutableLiveData<String>()
    var isProgressBarVisible = MutableLiveData<Boolean>(false)
    var isPlaceHolderMessageVisible = MutableLiveData<Boolean>(false)

    private var lastVMNameVisited: String? = null
    private var filterList = ArrayList<IFilterItem>()
    private var currentUserId: String? = null
    private var currentCountryCode = ""

    init {

        initListAdapters()
        isProgressBarVisible.value = true
        database.eventDao.resetLastEventDocument()
        viewModelScope.launch(Dispatchers.IO) {
            initCurrentUserInfo()
            initEventsList()
        }
    }

    override fun onResume() {

        checkCountryHasChanged()
        checkRadiusFilter()
        checkTypeAndTimeFilter()
        checkUserEventsForUpdates()

        lastVMNameVisited = null
    }

    override fun onDestroy() {
        database.eventDao.resetLastEventDocument()
        super.onDestroy()
    }

    fun loadMoreEvents() {
        isFetchMoreEventsEnable = true

        eventsFromDatabaseList.add(EventLoadingPOJO())
        loadingPosition.value = eventsFromDatabaseList.size - 1
        eventsAdapter?.notifyItemChanged(loadingPosition.value!!)

        viewModelScope.launch(Dispatchers.IO) {
            fetchEvents(getScaleType())
        }
    }

    fun addNewEventClicked() {
        if (isCurrentUserAnonymous()) {
            showRegisterAlert()
        } else {
            viewModelScope.launch(Dispatchers.Main) {
                val hasLocationPermission = askLocationPermission().await()
                if (hasLocationPermission) {
                    withContext(Dispatchers.Main)
                    { lastVMNameVisited = AddEventViewModel::class.qualifiedName!! }
                    navigation.navigateToActivity(AddEventActivity::class.java, false)
                } else {
                    dialogService.showSnackbar(R.string.location_permission_message)
                }

            }
        }
        /*viewModelScope.launch(Dispatchers.Main) {
            add100Events()
        }*/
    }

    fun searchByNameClicked() {
        navigation.navigateToActivity(SearchEventsByNameActivity::class.java, false)
    }

    fun filterEventsByTypeOrTimeClicked() {
        lastVMNameVisited = FilterEventsViewModel::class.qualifiedName
        dialogService.showDialog(FilterEventsFragment::class)
    }

    fun filterEventsByRadiusClicked() {

        viewModelScope.launch(Dispatchers.Main) {
            val hasLocationPermission = askLocationPermission().await()
            if (hasLocationPermission) {

                lastVMNameVisited = FilterEventsByRadiusViewModel::class.qualifiedName
                dialogService.showDialog(FilterEventsByRadiusFragment::class)
            } else {
                dialogService.showSnackbar(R.string.location_permission_message)
            }
        }
    }

    fun removeFilter(eventTypeFilterPOJO: IFilterItem) {
        if (filterList.size == 1) {
            filterList.clear()
        }
        filterList.remove(eventTypeFilterPOJO)
        filterAppliedAdapter?.notifyDataSetChanged()

        database.eventDao.resetLastEventDocument()

        applyFilterEvents()
    }

    fun chooseCountryClick() {
        lastVMNameVisited = SelectCountryViewModel::class.qualifiedName

        val configCountry = ConfigCountryModel(resourceService.stringForId(R.string.save), false)

        dataExchangeService.put(
            SelectCountryViewModel::class.qualifiedName!!,
            configCountry
        )
        dialogService.showDialog(SelectCountryFragment::class)
    }

    private fun initListAdapters() {
        eventsAdapter = EventsAdapter(eventsFromDatabaseList, this)
        filterAppliedAdapter = FilterAppliedAdapter(filterList, this)
    }

    private fun checkUserEventsForUpdates() {
        viewModelScope.launch(Dispatchers.IO) {

            myUserEventList = database.eventDao
                .fetchUserEvents(
                    backend.currentUser!!.uid,
                    com.google.firebase.firestore.Source.CACHE
                )
            withContext(Dispatchers.Main) {
                eventsAdapter?.notifyDataSetChanged()
            }
        }
    }

    private suspend fun initCurrentUserInfo() {
        currentUserId = backend.currentUser!!.uid
        currentCountryCode = database.userInfoDao.fetch(currentUserId!!)!!.country!!
        withContext(Dispatchers.Main)
        {
            currentCountryBinding.value = mapService.getCountryName(currentCountryCode)
        }
    }

    private suspend fun initEventsList() {
        myUserEventList = database.eventDao.fetchUserEvents(currentUserId!!)
        fetchEvents()
    }

    private fun checkCountryHasChanged() {
        if (lastVMNameVisited.equals(SelectCountryViewModel::class.qualifiedName)) {

            val countryPOJO =
                dataExchangeService.get<CountryPOJO>(SelectCountryViewModel.countryKey)

            if (countryPOJO != null) {
                currentCountryCode = mapService.getCountryCode(countryPOJO.countryName!!)!!
                currentCountryBinding.value = countryPOJO.countryName
                clearEventsFromDBLists()
                database.eventDao.resetLastEventDocument()

                applyFilterEvents()
            }
        }
    }

    private fun checkRadiusFilter() {
        if (lastVMNameVisited.equals(FilterEventsByRadiusViewModel::class.qualifiedName)) {
            val mapFilter =
                dataExchangeService.get<IFilterItem>(EventsViewModel::class.qualifiedName!!)

            if (mapFilter == null) return
            removeFilterType<EventDistanceFilter>()

            filterList.add(mapFilter)
            filterAppliedAdapter!!.notifyDataSetChanged()

            applyFilterEvents()
        }
    }

    private fun checkTypeAndTimeFilter() {
        if (lastVMNameVisited.equals(FilterEventsViewModel::class.qualifiedName)) {

            val filters =
                dataExchangeService.get<ArrayList<IFilterItem>>(EventsViewModel::class.qualifiedName!!)

            if (filters == null) return
            removeFilterType<EventTypeFilterPOJO>()
            removeFilterType<EventTimeFilterPOJO>()

            filterList.addAll(filters)
            filterAppliedAdapter!!.notifyDataSetChanged()
            applyFilterEvents()
        }
    }

    private suspend fun fetchEvents(scaleType: ScaleTypeEnum? = null) {
        try {
            val eventsList = database.eventDao.fetchEvents(filterList, currentCountryCode,scaleType)

            viewModelScope.launch(Dispatchers.Main) {

                removeLoadingElementFromList()
                addEvents(eventsList)

                isProgressBarVisible.value = false
                isPlaceHolderMessageVisible.value = eventsFromDatabaseList.isNullOrEmpty()
                eventsAdapter?.notifyDataSetChanged()
                isFetchMoreEventsEnable = false
            }
        } catch (e: Exception) {

            withContext(Dispatchers.Main) {
                val title = resourceService.stringForId(R.string.warning_text)
                val message = resourceService.stringForId(R.string.problem_text)
                val buttonText = resourceService.stringForId(R.string.retry_text)

                dialogService.showAlertDialog(
                    title!!,
                    message!!,
                    buttonText!!,
                    object : IClickListener {
                        override fun clicked() {
                            viewModelScope.launch(Dispatchers.IO) {
                                fetchEvents(getScaleType())
                            }
                        }
                    })
            }
        }
    }

    private fun clearEventsFromDBLists() {
        eventsFromDatabaseList.clear()
        eventsAdapter?.notifyDataSetChanged()
    }

    private fun applyFilterEvents() {

        eventsFromDatabaseList.clear()
        isProgressBarVisible.value = true
        isPlaceHolderMessageVisible.value = false
        viewModelScope.launch(Dispatchers.IO) {
            fetchEvents(getScaleType())
        }
    }

    private fun removeLoadingElementFromList() {
        try {
            eventsFromDatabaseList.removeAt(loadingPosition.value!!)
            eventsAdapter?.notifyItemChanged(loadingPosition.value!!)
        } catch (e: Exception) {
            Log.d("Error", "RemoveLoadingElement:index out of bounds")
        }
    }

    private fun addEvents(eventsList: ArrayList<EventPOJO>) {
        eventsList.forEach {
            if (!eventsFromDatabaseList.contains(it)) {
                eventsFromDatabaseList.add(it)
            }
        }
    }

    private suspend fun askLocationPermission(): Task<Boolean> {

        return Tasks.forResult(suspendCoroutine<Boolean> {
            viewModelScope.launch(Dispatchers.Main) {
                val permissionsResult = permissionService.requestPermissionStatusAsync(
                    listOf(
                        Manifest.permission.ACCESS_FINE_LOCATION
                    )
                ).await()
                var grantedCounter = 0

                for (result in permissionsResult) {
                    if (result.permissionStatus == PermissionStatus.Granted) {
                        grantedCounter++
                    }
                }

                if (grantedCounter == permissionsResult.size) {
                    it.resumeWith(Result.success(true))
                } else {
                    it.resumeWith(Result.success(false))

                }
            }
        })
    }


    private inline fun <reified T> removeFilterType() {
        filterList.forEach {
            if (it is T) {
                filterList.remove(it)
                return
            }
        }
    }

    private fun showRegisterAlert() {
        val registerTitle = resourceService.stringForId(R.string.register_name)
        val message = resourceService.stringForId(R.string.register_warning_add_event)
        val buttonText = resourceService.stringForId(R.string.register_now)
        dialogService.showAlertDialog(
            registerTitle!!,
            message!!,
            buttonText!!,
            object : IClickListener {
                override fun clicked() {
                    navigation.navigateToActivity(RegisterActivity::class.java, false)
                }
            })
    }


    //TODO remove it when it s done...create mock service
    suspend fun add100Events() {

        for (i in 1..4) {
            var currentLat = 45.5246675
            var currentTime = System.currentTimeMillis()

            for (index in 0..21) {
                currentLat += 0.08
                if (index % 2 == 1) currentTime += TimeUnit.DAYS.toMillis(1)
                var eventType = EventType.Unknown
                if (i == 1) eventType = EventType.GarbageCollect
                if (i == 2) eventType = EventType.Recycling
                if (i == 3) eventType = EventType.HelpPeople
                if (i == 4) eventType = EventType.PlantTrees
                if (index > 15 && i == 2) {
                    currentTime += TimeUnit.DAYS.toMillis(12)
                }
                val currentEvent = EventPOJO(
                    R.drawable.ic_arrow_back_black_24dp,
                    "eventType$i+ $index",
                    "eventName$i",
                    eventType,
                    currentTime,
                    GeoPoint(currentLat, 22.3756806),
                    "RO"
                )
                val eventKey = UUID.randomUUID().toString()
                currentEvent.uid = eventKey

                val eventCountryCode = mapService.getCountryCode(
                    45.5246675,
                    22.3756806
                )
                database.eventDao.storeEvent(eventKey, currentEvent)
                database.eventDao.storeUserEvent(
                    backend.currentUser!!.uid,
                    UserEventPOJO(EventState.HOSTING, eventKey)
                )

                Log.d("pulamea", "gata $index")

            }
        }
        Log.d("pulamea", "gata")

    }
}