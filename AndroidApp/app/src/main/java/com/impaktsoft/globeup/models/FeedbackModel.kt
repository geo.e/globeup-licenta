package com.impaktsoft.globeup.models

class FeedbackModel {
    var category : String? = null
    var description : String? = null

    constructor(category: String?, description: String?) {
        this.category = category
        this.description = description
    }
}