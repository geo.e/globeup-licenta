package com.impaktsoft.globeup.dummy

import com.impaktsoft.globeup.enums.WithdrawType
import com.impaktsoft.globeup.models.WithdrawPOJO

class WithdrawDummyData {
    companion object
    {
        var listOfWithdraws= listOf<WithdrawPOJO>(
            WithdrawPOJO("12345678",WithdrawType.PayPal),
            WithdrawPOJO("12345679",WithdrawType.mastercard),
            WithdrawPOJO("12345670",WithdrawType.Revolut),
            WithdrawPOJO("12345671",WithdrawType.VISA)
            )
    }
}