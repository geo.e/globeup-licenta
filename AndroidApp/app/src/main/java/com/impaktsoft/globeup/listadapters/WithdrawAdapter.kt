package com.impaktsoft.globeup.listadapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.impaktsoft.globeup.databinding.WithdrawCellBinding
import com.impaktsoft.globeup.models.WithdrawPOJO
import com.impaktsoft.globeup.viewmodels.WithdrawViewModel

class WithdrawAdapter(private var list: List<WithdrawPOJO>,private var viewModel:WithdrawViewModel) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val applicationBinding =
            WithdrawCellBinding.inflate(layoutInflater, parent, false)
        return TransactionViewHolder(applicationBinding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val transactionItem = list[position]
        (holder as TransactionViewHolder).bind(transactionItem)
    }

    inner class TransactionViewHolder(val applicationBinding: WithdrawCellBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {
        fun bind(withdrawItem: WithdrawPOJO) {
            applicationBinding.itemWithdraw = withdrawItem
            applicationBinding.withdrawLayout.setOnClickListener {
                viewModel.selectPayMethod(adapterPosition)
            }
        }
    }

}