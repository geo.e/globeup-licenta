package com.impaktsoft.globeup.views.fragments

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.lottie.LottieAnimationView
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.databinding.FragmentGlobalBinding
import com.impaktsoft.globeup.viewmodels.GlobalViewModel
import kotlinx.android.synthetic.main.fragment_global.*

class GlobalFragment : BaseBoundFragment<GlobalViewModel, FragmentGlobalBinding>(GlobalViewModel::class) {

    override var layoutId: Int = R.layout.fragment_global
    override val fragmentNameResourceID: Int = R.string.global_bound_name

    override fun setupDataBinding(binding: FragmentGlobalBinding) {
        binding.viewModel = mViewModel
    }

    override fun layoutInflated(root: View) {
        val globeAnimation = root.findViewById<LottieAnimationView>(R.id.animation_view);
        globeAnimation.progress = 0.6f
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        global_recyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        global_recyclerView.adapter = mViewModel.adapter
    }

}