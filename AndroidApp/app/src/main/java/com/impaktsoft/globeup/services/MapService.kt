package com.impaktsoft.globeup.services

import android.content.Context
import android.location.Geocoder
import android.util.Log
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

interface IMapService {
    fun getCountryCode(lat: Double, long: Double): String?
    fun getCountryCode(countryName: String): String?
    fun getCountryName(countryCode: String): String?
    fun getAllCountriesName(): ArrayList<String>
    fun getAllCountriesCode(): ArrayList<String>
}

class MapService(private val context: Context) : IMapService {
    override fun getCountryCode(lat: Double, long: Double): String? {

        return try {
            val geocoder = Geocoder(context, Locale.getDefault())
            val address = geocoder.getFromLocation(lat, long, 1)
            val countryCode: String = address[0].countryCode.trim()

            if (getAllCountriesCode().contains(countryCode)) {
                countryCode
            } else null
        } catch (e: Exception) {
            Log.d("Error", "getCountryCode(lat: Double, long: Double): String?")
            null
        }
    }

    override fun getCountryCode(countryName: String): String? {
        val localeList = Locale.getAvailableLocales()
        for (myCountryCode in localeList) {
            if (myCountryCode.displayCountry == countryName)
                return myCountryCode.country
        }
        return null
    }

    override fun getCountryName(countryCode: String): String? {
        val localeList = Locale.getAvailableLocales()
        for (myCountryCode in localeList) {
            if (myCountryCode.country == countryCode)
                return myCountryCode.displayCountry
        }
        return null
    }

    override fun getAllCountriesName(): ArrayList<String> {
        val localeList = Locale.getAvailableLocales()
        var countries = ArrayList<String>()

        for (local in localeList) {

            var countryName = local.displayCountry
            if (countryName.trim().isNotEmpty() && !countries.contains(countryName)) {
                countries.add(countryName)
            }
        }

        countries.sort()

        return countries
    }

    override fun getAllCountriesCode(): ArrayList<String> {
        val localeList = Locale.getAvailableLocales()
        var countriesCode = ArrayList<String>()

        for (local in localeList) {

            var countryName = local.country
            if (!countriesCode.contains(countryName)) {
                countriesCode.add(countryName)
            }
        }

        countriesCode.sort()

        return countriesCode
    }

}