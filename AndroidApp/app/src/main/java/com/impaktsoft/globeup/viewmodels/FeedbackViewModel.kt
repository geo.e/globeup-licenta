package com.impaktsoft.globeup.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.android.material.snackbar.Snackbar
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.dummy.FeedbackDummyData
import com.impaktsoft.globeup.listadapters.FeedbackAdapter
import com.impaktsoft.globeup.models.FeedbackModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class FeedbackViewModel :BaseViewModel()
{
    var adapter:FeedbackAdapter?=null
    var isProgressVisible = MutableLiveData<Boolean>()
    var feedbackDescription = MutableLiveData<String>()

    init {
         adapter=FeedbackAdapter(FeedbackDummyData.feedbacks,this)
    }

    fun checkCurrentButton(position:Int)
    {
        for(i in 0..FeedbackDummyData.feedbacks.size-1)
        {
            if(i==position&&!FeedbackDummyData.feedbacks[i].checked!!)
            {
                FeedbackDummyData.feedbacks[i].checked=true
                adapter!!.notifyItemChanged(i)

            }
            else if(i!=position&&FeedbackDummyData.feedbacks[i].checked!!)
            {
                FeedbackDummyData.feedbacks[i].checked=false
                adapter!!.notifyItemChanged(i)
            }
        }
    }

    fun sendFeedback()
    {
        if(!isCategorySelected())
        {
            dialogService.showSnackbar(R.string.please_select_category,Snackbar.LENGTH_LONG)
            return
        }
        if(feedbackDescription.value.isNullOrEmpty())
        {
            dialogService.showSnackbar(R.string.err_add_description,Snackbar.LENGTH_LONG)
            return
        }
        isProgressVisible.value = true
        val feedbackCategory = getSelectedCategory()
        val feedbackModel = FeedbackModel(feedbackCategory,feedbackDescription.value)
        viewModelScope.launch(Dispatchers.IO) {
            database.feedbackService.sendFeedback(feedbackModel)
            withContext(Dispatchers.Main)
            {
                deselectAllCategories()
                feedbackDescription.value = ""
                isProgressVisible.value = false
                dialogService.showSnackbar(R.string.thx_for_feedback,Snackbar.LENGTH_LONG)
            }
        }
    }

    fun close()
    {
        navigation.closeCurrentActivity()
    }

    private fun isCategorySelected():Boolean
    {
        FeedbackDummyData.feedbacks.forEach {
            if (it.checked!!)return true
        }
        return false
    }
    private fun getSelectedCategory():String?
    {
        FeedbackDummyData.feedbacks.forEach {
            if (it.checked!!)return it.title
        }
        return null
    }

    private fun deselectAllCategories()
    {
        FeedbackDummyData.feedbacks.forEach {
            it.checked=false
        }
        adapter?.notifyDataSetChanged()
    }
}