package com.impaktsoft.globeup.viewmodels

import androidx.lifecycle.viewModelScope
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.models.ConfigCountryModel
import com.impaktsoft.globeup.services.IConnectivityService
import com.impaktsoft.globeup.services.ISharedPreferencesService
import com.impaktsoft.globeup.util.SharedPreferencesKeys
import com.impaktsoft.globeup.views.activities.InitUserActivity
import com.impaktsoft.globeup.views.activities.LoginActivity
import com.impaktsoft.globeup.views.activities.MainActivity
import com.impaktsoft.globeup.views.fragments.SelectCountryFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.core.inject


class SplashViewModel : BaseViewModel() {

    fun loadNextScreen() {
        viewModelScope.launch(Dispatchers.IO) {

            withContext(Dispatchers.Main)
            {
                if (backend.isSignedIn()) {
                    navigation.navigateToActivity(MainActivity::class.java, true)
                } else {
                    val didUserLogoutSometime =
                        sharedPreferencesService.readFromSharedPref<Boolean>(
                            SharedPreferencesKeys.didUserLogOutKey,
                            Boolean::class.java
                        )


                    if (didUserLogoutSometime != null && didUserLogoutSometime) {
                        navigation.navigateToActivity(LoginActivity::class.java, true)

                    } else {
                        val configCountry = ConfigCountryModel(
                            resourceService.stringForId(R.string.select_country),
                            true,
                            ::navigateToInitActivity
                        )

                        dataExchangeService.put(
                            SelectCountryViewModel::class.qualifiedName!!,
                            configCountry
                        )

                        navigation.navigateToFragment(SelectCountryFragment::class, true)
                    }
                }
            }
        }
    }

    private fun navigateToInitActivity() {
        navigation.navigateToActivity(InitUserActivity::class.java, true)
    }

}