package com.impaktsoft.globeup.dummy

import com.impaktsoft.globeup.enums.LevelType
import com.impaktsoft.globeup.models.LevelPOJO

class LevelsDummyData {
    companion object{
        val levelsList= listOf<LevelPOJO>(
            LevelPOJO(LevelType.GREENHORN,"GreenHorn","0 EXP","Everybody starting the app will have from the beginning this level start"),
            LevelPOJO(LevelType.BRONZE1,"1 Bronze star","100 EXP"),
            LevelPOJO(LevelType.BRONZE2,"2 Bronze star","500 EXP"),
            LevelPOJO(LevelType.BRONZE3,"3 Bronze star","1000 EXP"),
            LevelPOJO(LevelType.SILVER1,"1 Silver star","3000 EXP"),
            LevelPOJO(LevelType.SILVER2,"2 Silver star","5000 EXP"),
            LevelPOJO(LevelType.SILVER3,"3 Silver star","7000 EXP"),
            LevelPOJO(LevelType.GOLD1,"1 Gold star","10000 EXP"),
            LevelPOJO(LevelType.GOLD2,"2 Gold star","20000 EXP"),
            LevelPOJO(LevelType.GOLD3,"3 Gold star","30000 EXP")
        )
    }
}