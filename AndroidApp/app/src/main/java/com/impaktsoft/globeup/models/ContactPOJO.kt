package com.impaktsoft.globeup.models

import com.impaktsoft.globeup.enums.ContactState

class ContactPOJO {
    var user:UserPOJO?=null
    var selected:ContactState?=null

    constructor(user: UserPOJO?, selected: ContactState?) {
        this.user = user
        this.selected = selected
    }
}