package com.impaktsoft.globeup.listeners

interface IGetStringListener {
    fun getCharSequence(string: String)
}