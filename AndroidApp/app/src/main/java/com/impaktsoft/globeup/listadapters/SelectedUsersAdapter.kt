package com.impaktsoft.globeup.listadapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.impaktsoft.globeup.databinding.SelectedUsersCellBinding
import com.impaktsoft.globeup.listeners.IClickListener
import com.impaktsoft.globeup.listeners.IGetUserListener
import com.impaktsoft.globeup.models.UserPOJO

class SelectedUsersAdapter (
    private var arrayList: ArrayList<UserPOJO>,private val clickListenr:IGetUserListener
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)

        val applicationBinding =
            SelectedUsersCellBinding.inflate(layoutInflater, parent, false)
        return ContactsViewHolder(applicationBinding)

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val userPOJO = arrayList[position];
        (holder as ContactsViewHolder).bind(userPOJO)
    }

    inner class ContactsViewHolder(var applicationBinding: SelectedUsersCellBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(item: UserPOJO) {

            applicationBinding.userItem = item
            applicationBinding.exitImage.setOnClickListener {
                clickListenr.getUser(item)
            }
        }
    }

    fun setList(auxList:ArrayList<UserPOJO>)
    {
        arrayList=auxList
        notifyDataSetChanged()
    }
}