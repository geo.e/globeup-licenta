package com.impaktsoft.globeup.components

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.Context
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.util.AttributeSet
import android.widget.FrameLayout
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.impaktsoft.globeup.R
import android.view.MotionEvent
import com.google.android.gms.maps.GoogleMap
import com.impaktsoft.globeup.listeners.IGetLocationListener


class MapViewWithPin : FrameLayout, OnMapReadyCallback, LifecycleObserver {

    var mMap: GoogleMap? = null
    private var locationManager: LocationManager? = null
    private var mapBundle: Bundle? = null
    private var mapView: MapView?
    var mapPosition: LatLng? = null
    var getLocationListener: IGetLocationListener? = null

    constructor(var1: Context) : super(var1)
    constructor(var1: Context, var2: AttributeSet) : super(var1, var2)
    constructor(var1: Context, var2: AttributeSet, var3: Int) : super(var1, var2, var3)

    init {
        addView(inflate(context, R.layout.custom_map_with_ping, null))
        mapView = this.findViewById<MapView>(R.id.mapWithPing)
    }

    override fun onMapReady(p0: GoogleMap?) {
        if (p0 != null) {
            mMap = p0

            locationManager =
                context?.getSystemService(Context.LOCATION_SERVICE) as LocationManager

            updateMyLocation()
            initLocationListener()
        }
    }

    fun setCameraPosition(coords: LatLng, zoom: Float? = null) {

        var cameraUpdate: CameraUpdate?
        if (zoom != null) {
            cameraUpdate = CameraUpdateFactory.newLatLngZoom(coords, zoom)
        } else {
            cameraUpdate = CameraUpdateFactory.newLatLngZoom(coords, 10f)
        }

        mMap?.animateCamera(cameraUpdate)
    }

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {

        parent.requestDisallowInterceptTouchEvent(true)
        return super.dispatchTouchEvent(ev)
    }

    @SuppressLint("MissingPermission")
    fun updateMyLocation() {
        if (mapPosition==null) {
            val locationListener = object : LocationListener {
                @TargetApi(Build.VERSION_CODES.M)
                override fun onLocationChanged(p0: Location?) {

                    if (p0 != null) {
                        mapPosition = LatLng(p0.latitude, p0.longitude)
                        setCameraPosition(mapPosition!!)
                    }
                    locationManager!!.removeUpdates(this)
                    //now map is ready to show current position
                }

                override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {
                }

                override fun onProviderEnabled(p0: String?) {
                }

                override fun onProviderDisabled(p0: String?) {
                }
            }

            locationManager!!.requestLocationUpdates(
                LocationManager.GPS_PROVIDER, 0,
                0F, locationListener
            )

            locationManager!!.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER,
                0,
                0f,
                locationListener
            )
        } else {
            setCameraPosition(mapPosition!!)
        }

    }

    fun initLocationListener() {
        mMap?.setOnCameraIdleListener {
            getLocationListener?.getLocation(mMap?.cameraPosition?.target)
        }
    }

    fun getMapAsync() {
        mapView?.getMapAsync(this)
    }

    fun setMapBundle(bundle: Bundle?) {
        mapBundle = bundle

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun lifecycleOnCreate() {
        mapView?.onCreate(mapBundle)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun lifecycleOnResume() {
        mapView?.onResume()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun lifecycleOnStart() {
        mapView?.onStart()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun lifecycleOnStop() {
        mapView?.onStop()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun lifecycleOnPause() {
        mapView?.onPause()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun lifecycleOnDestroy() {
        mapView?.onDestroy()
    }


}