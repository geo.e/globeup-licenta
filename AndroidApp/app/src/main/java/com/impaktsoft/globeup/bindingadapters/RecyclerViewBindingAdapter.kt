package com.impaktsoft.globeup.bindingadapters

import android.util.Log
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.impaktsoft.globeup.viewmodels.EventsViewModel
import com.impaktsoft.globeup.viewmodels.MessengerViewModel
import com.impaktsoft.globeup.viewmodels.SearchEventsByNameViewModel

object RecyclerViewBindingAdapter {
    @BindingAdapter(value = ["cb_scrollToAddEvents"])
    @JvmStatic
    fun RecyclerView.setScrollMethod(viewModel: EventsViewModel) {
        this.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1)) {

                    if (!viewModel.isFetchMoreEventsEnable) {
                        viewModel.loadMoreEvents()
                    }
                }
            }
        })
    }

    @BindingAdapter(value = ["cb_scrollToFilterEvents"])
    @JvmStatic
    fun RecyclerView.setScrollMethodInFilterByName(viewModel: SearchEventsByNameViewModel) {
        this.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1)) {

                    if (!viewModel.isFetchMoreEventsEnable) {
                        viewModel.loadMoreEvents()
                    }
                }
            }
        })
    }

    @BindingAdapter(value = ["cb_onRefresh"])
    @JvmStatic
    fun SwipeRefreshLayout.OnRefresh(viewModel: MessengerViewModel) {
        this.setOnRefreshListener {
            if (viewModel.isFetchingMessages.value==false) {
                viewModel.fetchMessages(true)
            } else {
                this.isRefreshing = false
            }
        }
    }

    @BindingAdapter(value = ["cb_isRefreshing"])
    @JvmStatic
    fun SwipeRefreshLayout.isRefreshing(isRefreshing: Boolean) {
        this.isRefreshing = isRefreshing
    }

    @BindingAdapter(value = ["cb_scrollToPosition"])
    @JvmStatic
    fun RecyclerView.scrollToPosition(position: Int) {
        this.smoothScrollToPosition(position)
    }

    @BindingAdapter(value = ["cb_clearPool"])
    @JvmStatic
    fun RecyclerView.clearPool(isClearAllow: Boolean) {
        if (isClearAllow) {
            this.recycledViewPool.clear()
        }
    }
}