package com.impaktsoft.globeup.dummy

import com.impaktsoft.globeup.enums.TransactionType
import com.impaktsoft.globeup.models.TransactionPOJO

class TransactionDummyData {

    companion object
    {
        val transactionList= listOf<TransactionPOJO>(
            TransactionPOJO(TransactionType.EVENT_DONATION,"15Apr,15:00",-200,-5),
            TransactionPOJO(TransactionType.GLOBAL_DONATION,"16Apr,16:00",-1200,-15),
            TransactionPOJO(TransactionType.GETTING_GCOIN,"17Apr,12:00",240,5),
            TransactionPOJO(TransactionType.WITHDRAW,"18Apr,11:00",1200,7)
        )
    }
}