package com.impaktsoft.globeup.models

import java.util.*

class TimeSpanPOJO {
    var hour: Int? = null
    var minute: Int? = null

    constructor(hour: Int?, minute: Int?) {
        this.hour = hour
        this.minute = minute
    }

    constructor() {
    }
}