package com.impaktsoft.globeup.enums

enum class TransactionType {
    EVENT_DONATION,GLOBAL_DONATION,GETTING_GCOIN,WITHDRAW
}