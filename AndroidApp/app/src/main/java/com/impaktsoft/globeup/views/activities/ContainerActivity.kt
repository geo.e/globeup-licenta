package com.impaktsoft.globeup.views.activities

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.viewmodels.ContainerViewModel
import com.impaktsoft.globeup.views.fragments.BaseBoundFragment

class ContainerActivity : BaseActivity<ContainerViewModel>(ContainerViewModel::class) {
    companion object {
        const val fragmentClassNameParam = "FragmentClassName"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_container)

        val fragmentClassName = intent.getStringExtra(fragmentClassNameParam)

        if (fragmentClassName != null) {
            try {
                val fragment = Class.forName(fragmentClassName).newInstance() as Fragment
                supportFragmentManager.beginTransaction().add(R.id.fragmentContainer, fragment)
                    .commit()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        //elimin animatia de slide left
        overridePendingTransition(0, 0);
    }

    override fun onDestroy() {
        super.onDestroy()
        //elimin animatia de slide left
        overridePendingTransition(0, 0);
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        mViewModel.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
}
