package com.impaktsoft.globeup.listadapters

import android.graphics.Paint
import android.graphics.Rect
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.ViewTreeObserver
import androidx.core.view.marginLeft
import androidx.core.view.marginRight
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.impaktsoft.globeup.databinding.FilterAppliedCellBinding
import com.impaktsoft.globeup.models.EventTypeFilterPOJO
import com.impaktsoft.globeup.models.IEventsItem
import com.impaktsoft.globeup.models.IFilterItem
import com.impaktsoft.globeup.viewmodels.EventsViewModel

class FilterAppliedAdapter(
    var list: ArrayList<IFilterItem>, private val viewModel: EventsViewModel
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var applicationBinding: FilterAppliedCellBinding? = null

    private var cellWidth: Int = 0
    private var cellTextSize = 0f;
    private var cellImageSize = 0;

    val spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
        override fun getSpanSize(position: Int): Int {
            val bounds = Rect()
            val paint = Paint()

            paint.textSize = cellTextSize

            paint.getTextBounds(
                list[position].title,
                0,
                list[position].title!!.length,
                bounds
            )

            var width = bounds.width()
            return width + cellWidth

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)

        val applicationBinding =
            FilterAppliedCellBinding.inflate(layoutInflater, parent, false)
        this.applicationBinding = applicationBinding
        getViewWidth()
        return FilterViewHolder(applicationBinding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = list[position]
        (holder as FilterViewHolder).bind(item)

    }


    inner class FilterViewHolder(var applicationBinding: FilterAppliedCellBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {
        fun bind(item: IFilterItem) {
            applicationBinding.filterItem = item;

            applicationBinding.filterAppliedImage.setOnClickListener {
                viewModel.removeFilter(item)
            }
        }

    }

    fun getViewWidth() {

        applicationBinding!!.filterAppliedLayout.viewTreeObserver.addOnGlobalLayoutListener(
            object :
                ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {

                    if (applicationBinding != null) {
                        applicationBinding!!.filterAppliedLayout.viewTreeObserver.removeOnGlobalLayoutListener(
                            this
                        )

                        val parentLayout = applicationBinding!!.filterAppliedLayout
                        val parentCardView = applicationBinding!!.filterAppliedCardView
                        val horizontalPadding =
                            parentLayout.paddingLeft + parentLayout.paddingRight
                        val horizontalMargin =
                            parentLayout.marginLeft + parentLayout.marginRight
                            +parentCardView.marginLeft + parentCardView.marginRight

                        cellTextSize =
                            applicationBinding!!.filterAppliedText.textSize
                        cellImageSize =
                            applicationBinding!!.filterAppliedImage.width
                        cellWidth =
                            horizontalMargin + horizontalPadding + cellTextSize.toInt() + cellImageSize
                    }
                }
            })
    }

}