package com.impaktsoft.globeup.bindingadapters

import android.util.Log
import android.view.View
import android.widget.ProgressBar
import androidx.databinding.BindingAdapter
import com.google.gson.Gson
import com.impaktsoft.globeup.enums.MessageType
import com.impaktsoft.globeup.models.MessageImagePOJO
import com.impaktsoft.globeup.models.MessageLocationPOJO
import com.impaktsoft.globeup.models.MessagePOJO

object ProgressViewBindingAdapter {

    @BindingAdapter(value = ["cb_isProgressVisible"])
    @JvmStatic
    fun ProgressBar.setVisibilityDependingOnState(messagePOJO: MessagePOJO)
    {
        if(messagePOJO.type==MessageType.LocationMessage)
        {
            val locationMessage= Gson().fromJson(messagePOJO.jsonMessage,MessageLocationPOJO::class.java)
            if(locationMessage.imageUrl.isEmpty())
            {
                this.visibility= View.VISIBLE
            }
            else
            {
                this.visibility= View.GONE
            }
        }
        if(messagePOJO.type==MessageType.ImageMessage)
        {
            val imageMessage= Gson().fromJson(messagePOJO.jsonMessage,MessageImagePOJO::class.java)
            if(imageMessage.imageUrl.isEmpty())
            {
                this.visibility= View.VISIBLE
                Log.d("pulamea","visible")
            }
            else
            {
                this.visibility= View.GONE
                Log.d("pulamea","invisible")
            }
        }
    }
}