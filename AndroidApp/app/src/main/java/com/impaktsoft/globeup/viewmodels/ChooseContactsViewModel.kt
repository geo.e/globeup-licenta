package com.impaktsoft.globeup.viewmodels

import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.dummy.ContactsDummyData
import com.impaktsoft.globeup.listadapters.ContactsAdapter
import com.impaktsoft.globeup.models.UserPOJO
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class ChooseContactsViewModel : BaseViewModel() {
    private var adapter: ContactsAdapter? = null

    var list = ArrayList<UserPOJO>()

    init {
        list.addAll(ContactsDummyData.contactsList)
        adapter = ContactsAdapter(list)
    }

    fun getAdapter(): ContactsAdapter? {
        return adapter
    }

    fun newGroupClicked() {
        navigation.navigateToFragment(
            R.id.action_chooseContactsFragment_to_selectGroupMembersFragment,
            R.id.nav_host_contacts
        )
    }

    fun getContactsSize(): Int {
        //TODO complete the logic...
        return ContactsDummyData.contactsList.size
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun filterList(string: String) {
        val auxList = java.util.ArrayList<UserPOJO>()
        for (item in list) {
            val name = item.name

            var stringWithUpperLetter = ""

            if (!string.isNullOrEmpty()) {
                stringWithUpperLetter =
                    string.substring(0, 1).toUpperCase() + string.substring(1)
            }
            if (name.contains(string) || name.contains(stringWithUpperLetter) ||
                name.contains(string.toUpperCase()) || string.isEmpty()
            ) {
                auxList.add(item)
            }
        }
        adapter!!.setList(auxList)
    }

    override fun onCreate() {
        super.onCreate()
        EventBus.getDefault().register(this);
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }
}
