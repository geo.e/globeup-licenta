package com.impaktsoft.globeup.services

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri


interface IRateInPlayStore {
    fun askRating()
}

class RateInPlayStoreService(var activityService: ICurrentActivityService) : IRateInPlayStore {
    private val packageName = "com.impaktsoft.globeup"

    override fun askRating() {
        val uri: Uri = Uri.parse("market://details?id=$packageName")
        val goToMarket = Intent(Intent.ACTION_VIEW, uri)
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT or
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
        try {
            activityService.activity!!.startActivity(goToMarket)
        } catch (e: ActivityNotFoundException) {
            activityService.activity!!.startActivity(Intent(Intent.ACTION_VIEW,
                Uri.parse("http://play.google.com/store/apps/details?id=$packageName")))
        }
    }
}