package com.impaktsoft.globeup.listeners

import com.impaktsoft.globeup.models.UserPOJO

interface IGetUserListener
{
    fun getUser(user:UserPOJO)
}