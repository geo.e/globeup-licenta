package com.impaktsoft.globeup.listeners

import com.impaktsoft.globeup.models.EventPOJO
import java.util.ArrayList

interface IGetEventsListener {
    fun getEventsList(eventsList:ArrayList<EventPOJO>)
}