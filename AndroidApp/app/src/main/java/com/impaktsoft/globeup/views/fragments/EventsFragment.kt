package com.impaktsoft.globeup.views.fragments

import android.os.Bundle
import android.view.*
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.databinding.FragmentEventsBinding
import com.impaktsoft.globeup.viewmodels.EventsViewModel
import kotlinx.android.synthetic.main.fragment_events.*


class EventsFragment :
    BaseBoundFragment<EventsViewModel, FragmentEventsBinding>(EventsViewModel::class) {

    override var layoutId: Int = R.layout.fragment_events
    override val fragmentNameResourceID: Int = R.string.events_bound_name

    override fun setupDataBinding(binding: FragmentEventsBinding) {
        binding.viewModel = mViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        events_fragment_recyclerview.adapter = mViewModel.eventsAdapter
        events_fragment_recyclerview.layoutManager =
            LinearLayoutManager(context, RecyclerView.VERTICAL, false)

        //triggered when a view has been created
        view.viewTreeObserver.addOnGlobalLayoutListener(object :
            ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                view.viewTreeObserver.removeOnGlobalLayoutListener(this)
                initFilterList(view.width)
            }
        })
    }

    fun initFilterList(spanCount: Int) {
        val layoutManager = GridLayoutManager(context, spanCount)

        events_fragment_filter_applyed_recyclerview.layoutManager = layoutManager
        layoutManager.spanSizeLookup = mViewModel.filterAppliedAdapter!!.spanSizeLookup
        events_fragment_filter_applyed_recyclerview.adapter = mViewModel.filterAppliedAdapter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true);
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.event_fragment_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.search_events -> {
                mViewModel.searchByNameClicked()
                true
            }
            R.id.search_radius_events -> {
                mViewModel.filterEventsByRadiusClicked()
                true
            }
            R.id.filter_events -> {
                mViewModel.filterEventsByTypeOrTimeClicked()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}