package com.impaktsoft.globeup.views.fragments

import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.databinding.FragmentEditProfileBinding
import com.impaktsoft.globeup.viewmodels.EditProfileViewModel


class EditProfileFragment : BaseBoundFragment<EditProfileViewModel, FragmentEditProfileBinding>(
    EditProfileViewModel::class
) {
    override var layoutId: Int = R.layout.fragment_edit_profile
    override val fragmentNameResourceID: Int = R.string.edit_profile_name

    override fun setupDataBinding(binding: FragmentEditProfileBinding) {
        binding.viewModel = mViewModel
    }
}

