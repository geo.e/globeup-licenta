package com.impaktsoft.globeup.enums

enum class EventType {
    GarbageCollect,Recycling,HelpPeople,PlantTrees,Unknown
}