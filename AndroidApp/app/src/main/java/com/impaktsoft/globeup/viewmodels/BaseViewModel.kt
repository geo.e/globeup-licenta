package com.impaktsoft.globeup.viewmodels

import android.content.Intent
import android.util.Log
import androidx.lifecycle.*
import com.google.firebase.appinvite.FirebaseAppInvite
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.impaktsoft.globeup.services.*
import com.impaktsoft.globeup.services.database.IDatabaseService
import com.impaktsoft.globeup.services.database.MyFirebaseMessagingService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.lang.Exception

abstract class BaseViewModel : ViewModel(), KoinComponent, LifecycleObserver {

    protected val navigation: INavigationService by inject()
    protected val backend: IBackendService by inject()
    protected val database: IDatabaseService by inject()
    protected val dialogService: IDialogService by inject()
    protected val permissionService: IPermissionService by inject()
    protected val fileStoreService: IFileStoreService by inject()
    protected val dataExchangeService: IDataExchangeService by inject()
    protected val resourceService: IResourceService by inject()
    protected val mapService: IMapService by inject()
    protected val sharedPreferencesService: ISharedPreferencesService by inject()
    protected val popUpMenuService: IPopUpMenuService by inject()
    protected val locationService: ILocationService by inject()
    protected val pendingService: IPendingMessagesService by inject()
    protected val showGoogleMapService: IShowGoogleMapService by inject()
    protected val alertBuilderService: IAlertBuilderService by inject()
    protected val activityService:ICurrentActivityService  by inject()
    protected val activityResultService:IActivityResultService by inject()
    protected val connectivityService:IConnectivityService by inject()
    protected val backPressService:IOnBackPressService by inject()
    protected val firebaseInvitationService : IFirebaseInvitationService by inject()
    protected val rateInPlayStoreService : IRateInPlayStore by inject()
    protected val facebookService : IFacebookService by inject()

    var isHudVisible = MutableLiveData<Boolean>(false)

    protected suspend fun showHud() {
        withContext(Dispatchers.Main) {
            isHudVisible.value = true
        }
    }

    protected suspend fun hideHud() {
        withContext(Dispatchers.Main) {
            isHudVisible.value = false
        }
    }

    protected fun isCurrentUserAnonymous(): Boolean {
        return (backend.isSignedInAnonymously())
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    open fun onCreate() {
        Log.d("Lifecycle",this.javaClass.name + "onCreate")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    open fun onResume() {

        val intent = activityService.activity?.intent
        if (intent != null){
            //try getting invitation
            FirebaseDynamicLinks.getInstance()
                .getDynamicLink(intent)
                .addOnCompleteListener { task ->
                    if (!task.isSuccessful) {
                        // Handle error
                        // ...
                    }

                    try {
                        val invite = FirebaseAppInvite.getInvitation(task.result)
                        if (invite != null) {
                            // Handle invite
                            // ...
                        }
                    }catch (e:Exception)
                    {
                        Log.d("Error getting invite",e.message)
                    }

                }
        }

        Log.d("Lifecycle",this.javaClass.name + " onResume")

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    open fun onStart() {
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    open fun onPause() {
        Log.d("Lifecycle",this.javaClass.name + " onPause")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    open fun onStop() {
        Log.d("Lifecycle",this.javaClass.name + " onStop")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    open fun onDestroy() {
        Log.d("Lifecycle",this.javaClass.name + " onDestroy")
    }

    //Only to be called if permission was requested
    fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        permissionService.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    fun onActivityForResult(requestCode: Int, resultCode: Int, data: Intent?)
    {
        activityResultService.onActivityForResultService(requestCode,resultCode,data)
    }

    fun onBackPressed()
    {
        backPressService.onBackPressed()
    }
}