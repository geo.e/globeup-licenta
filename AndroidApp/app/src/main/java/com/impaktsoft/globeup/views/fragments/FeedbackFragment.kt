package com.impaktsoft.globeup.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.databinding.FragmentFeedbackBinding
import com.impaktsoft.globeup.viewmodels.FeedbackViewModel
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.fragment_feedback.*


class FeedbackFragment :
    BaseBoundFragment<FeedbackViewModel, FragmentFeedbackBinding>(FeedbackViewModel::class) {

    override var layoutId: Int = R.layout.fragment_feedback
    override val fragmentNameResourceID: Int = R.string.feedback

    override fun setupDataBinding(binding: FragmentFeedbackBinding) {
       binding.viewModel=mViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //triggered when a view has been created
        view.viewTreeObserver.addOnGlobalLayoutListener(object :
            ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                view.viewTreeObserver.removeOnGlobalLayoutListener(this)
                initList(view.width)
            }
        })
    }

    fun initList(spanCount:Int)
    {
        val layoutManager=GridLayoutManager(context,spanCount)

        feedback_subjects_list.layoutManager =layoutManager
        layoutManager.spanSizeLookup=mViewModel.adapter!!.spanSizeLookup
        feedback_subjects_list.adapter = mViewModel.adapter
    }
}
