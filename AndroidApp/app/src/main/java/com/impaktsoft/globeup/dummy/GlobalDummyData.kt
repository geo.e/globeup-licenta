package com.impaktsoft.globeup.dummy

import com.impaktsoft.globeup.models.GlobalGroupPOJO
import com.impaktsoft.globeup.models.GlobalSectionPOJO
import com.impaktsoft.globeup.models.GlobalUserPOJO
import com.impaktsoft.globeup.models.IGlobalBaseItem

class GlobalDummyData {
    companion object
    {
        val listOfGlobalItems:List<IGlobalBaseItem> = listOf(
            GlobalSectionPOJO("Your Rank"),
            GlobalUserPOJO(100,1000,"Geo",100,"Timisoara"),
            GlobalSectionPOJO("Leader Board"),
            GlobalUserPOJO(100,1000,"John",1,"Timisoara"),
            GlobalGroupPOJO(2000,20000,"Trupa lui bula",2,2),
            GlobalUserPOJO(10,3000,"Maria",3,"Timisoara")
        )
    }
}