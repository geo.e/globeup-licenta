package com.impaktsoft.globeup.listadapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.impaktsoft.globeup.databinding.TransactionCellBinding
import com.impaktsoft.globeup.models.TransactionPOJO

class TransactionAdapter (var list:List<TransactionPOJO>):RecyclerView.Adapter<RecyclerView.ViewHolder>()
{
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater=LayoutInflater.from(parent.context)
        val applicationBinding =
            TransactionCellBinding.inflate(layoutInflater, parent, false)
        return TransactionViewHolder(applicationBinding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val transactionItem= list[position]
        (holder as TransactionViewHolder).bind(transactionItem)
    }

    inner class TransactionViewHolder(val applicationBinding:TransactionCellBinding):RecyclerView.ViewHolder(applicationBinding.root)
    {
        fun bind(transactionItem:TransactionPOJO)
        {
         applicationBinding.transactionItem=transactionItem
        }
    }

}