package com.impaktsoft.globeup.services

import android.app.Application
import android.content.res.Resources
import java.lang.ref.WeakReference

interface IResourceService {
    fun initWithApplication(application: Application)
    fun stringForId(id: Int): String?
}

class ResourceService : IResourceService {
    private var weakResources: WeakReference<Resources>? = null

    override fun initWithApplication(application: Application) {
        weakResources = WeakReference(application.resources)
    }

    override fun stringForId(id: Int): String? {
        var string: String? = null
        var res = weakResources?.get()

        if (res != null) {
            string = res.getString(id)
        }

        return string
    }
}