package com.impaktsoft.globeup.services

import android.content.Intent
import android.os.Bundle
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.impaktsoft.globeup.listeners.IOnFailListener
import com.impaktsoft.globeup.listeners.IOnSuccessListener


interface IFacebookService {
    fun logoutIfPossible()
    fun isUserLoggedIn(): Boolean
    fun initLoginManager(onSuccessListener: IOnSuccessListener, onFailureListener: IOnFailListener)
    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)
    fun getUserId(): String
}

class FacebookService : IFacebookService {

    private var callbackManager: CallbackManager
    private var UserId: String?

    constructor() {
        UserId = ""
        callbackManager = CallbackManager.Factory.create()
    }

    override fun logoutIfPossible() {
        if(AccessToken.getCurrentAccessToken() != null)
            LoginManager.getInstance().logOut()
    }

    override fun isUserLoggedIn(): Boolean {

        val accessToken = AccessToken.getCurrentAccessToken()
        return accessToken != null && !accessToken.isExpired
    }

    override fun initLoginManager(
        onSuccessListener: IOnSuccessListener,
        onFailureListener: IOnFailListener
    ) {

        LoginManager.getInstance().registerCallback(callbackManager,
            object : FacebookCallback<LoginResult?> {
                override fun onSuccess(loginResult: LoginResult?) {

                    UserId = loginResult?.accessToken?.userId

                    val request =
                        GraphRequest.newMeRequest(loginResult?.accessToken) { `object`, response ->
                            try {

                                onSuccessListener.OnSucces(`object`)
                            } catch (e: Exception) {
                                onFailureListener.OnFailure(e)
                            }
                        }

                    val parameters = Bundle()
                    parameters.putString(
                        "fields",
                        "first_name,last_name,email,hometown,picture.type(large)"
                    )
                    request.parameters = parameters
                    request.executeAsync()
                }

                override fun onCancel() {
                    onFailureListener.OnFailure(Exception("Cancel"))
                }

                override fun onError(exception: FacebookException) {
                    onFailureListener.OnFailure(exception)
                }
            })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
    }

    override fun getUserId(): String {
        return UserId!!
    }

}