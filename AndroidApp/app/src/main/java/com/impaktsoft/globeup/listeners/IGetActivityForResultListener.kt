package com.impaktsoft.globeup.listeners

import android.content.Intent

interface IGetActivityForResultListener {
    fun activityForResult(requestCode: Int, resultCode: Int, data: Intent?)
}