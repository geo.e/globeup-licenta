package com.impaktsoft.globeup.views.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.impaktsoft.globeup.R

class ImageActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image)
    }
}
