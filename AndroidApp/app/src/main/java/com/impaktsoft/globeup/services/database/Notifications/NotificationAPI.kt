package com.impaktsoft.globeup.services.database.Notifications
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST


interface NotificationAPI {
    @Headers
        (
        "Authorization: key=AAAALNjKgxA:APA91bHfRIi7sGXzbQCrwvu6AplZxVu6t80snBo67wuburMLKSAG5eDp3OZ-f4tN4YaMmThRIae96DMNox1qHseURKt0aDDylUuwg3Ay9sAaW8_sv-KrzqRyDLc0XOla5O2UXLLVV8q6",
        "Content-Type:application/json"
        )
    @POST("fcm/send")
    fun sendChatNotification(@Body message: NotificationRequest): Call<ResponseBody>
}