package com.impaktsoft.globeup.models

class NotificationMessagePOJO {
    var conversationPOJO:ConversationPOJO? = null
    var messagePOJO:MessagePOJO? = null

    constructor(conversationPOJO: ConversationPOJO?, messagePOJO: MessagePOJO?) {
        this.conversationPOJO = conversationPOJO
        this.messagePOJO = messagePOJO
    }
}