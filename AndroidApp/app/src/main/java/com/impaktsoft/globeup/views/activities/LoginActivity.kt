package com.impaktsoft.globeup.views.activities

import android.content.Intent
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.databinding.ActivityLoginBinding
import com.impaktsoft.globeup.viewmodels.LoginViewModel

class LoginActivity : BaseBoundActivity<LoginViewModel, ActivityLoginBinding>(LoginViewModel::class) {

    override val layoutId: Int
        get() = R.layout.activity_login

    override var activityTitleResourceId: Int?
        get() = R.string.login_text
        set(value) {}

    override fun setupDataBinding(binding: ActivityLoginBinding) {
        binding.viewModel = mViewModel
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        mViewModel.onActivityResult(requestCode,resultCode,data)
        super.onActivityResult(requestCode, resultCode, data)
    }
}