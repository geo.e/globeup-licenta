package com.impaktsoft.globeup.bindingadapters

import com.impaktsoft.globeup.enums.ContactState
import com.impaktsoft.globeup.enums.EventState
import com.impaktsoft.globeup.enums.MessageState
import com.impaktsoft.globeup.models.ConversationPOJO
import com.impaktsoft.globeup.models.GlobalUserPOJO
import com.impaktsoft.globeup.models.MessagePOJO
import com.impaktsoft.globeup.models.UserPOJO

object BindingConvert {

    @JvmStatic
    fun isHoster(eventState: EventState?): Boolean {
        if (eventState != null)
            return eventState == EventState.HOSTING
        else return false
    }

    //TODO when we have user status,check if conv members count==2,get user status
    @JvmStatic
    fun isOnline(conversationPOJO: ConversationPOJO): Boolean {
        return true
    }

    //TODO when we have user status,check if conv members count==2,get user status
    @JvmStatic
    fun isOnline(userPOJO: UserPOJO): Boolean {
        return true
    }

    @JvmStatic
    fun isSelected(contactState: ContactState): Boolean {
        return contactState == ContactState.SELECTED
    }

    //TODO it is not the right method,we ill change it
    @JvmStatic
    fun messageIsRead(conversationPOJO: ConversationPOJO): Boolean {
        //TODO check if currentUser saw message and message is sent by other user
        return conversationPOJO.lastMessage!!.state == MessageState.SEEN
    }

    @JvmStatic
    fun notificationIsShowed(number: Int): Boolean {
        return number > 0
    }

}

