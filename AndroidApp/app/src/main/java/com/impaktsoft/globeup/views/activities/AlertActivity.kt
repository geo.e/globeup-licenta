package com.impaktsoft.globeup.views.activities

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.viewmodels.AlertViewModel

class AlertActivity : BaseActivity<AlertViewModel>(AlertViewModel::class) {
    companion object {
        const val fragmentClassNameParam = "FragmentClassName"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_alert)

        val fragmentClassName = intent.getStringExtra(fragmentClassNameParam)

        if (fragmentClassName != null) {
            try {
                var fragment = Class.forName(fragmentClassName).newInstance() as Fragment
                supportFragmentManager.beginTransaction().add(R.id.fragmentContainer, fragment)
                    .commit()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        //elimin animatia de slide left
        overridePendingTransition(0, 0);
    }

    override fun onDestroy() {
        super.onDestroy()
        //elimin animatia de slide left
        overridePendingTransition(0, 0);
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        mViewModel.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
}

