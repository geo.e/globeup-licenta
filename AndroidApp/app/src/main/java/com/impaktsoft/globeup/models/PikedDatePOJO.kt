package com.impaktsoft.globeup.models

import android.database.Observable
import android.widget.DatePicker
import java.util.*

class PikedDatePOJO {
    var year: Int? = null
    var month: Int? = null
    var day: Int? = null

    constructor() {
        var calendar = Calendar.getInstance()
        this.year = calendar.get(Calendar.YEAR)
        this.month = calendar.get(Calendar.MONTH)
        this.day = calendar.get(Calendar.DAY_OF_MONTH)

    }

    constructor(year: Int?, monthOfYear: Int?, dayOfMonth: Int?) {
        this.year = year
        this.month = monthOfYear
        this.day = dayOfMonth
    }

}