package com.impaktsoft.globeup.views.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.databinding.FragmentSelectCountryBinding
import com.impaktsoft.globeup.viewmodels.SelectCountryViewModel
import kotlinx.android.synthetic.main.fragment_select_country.*


class SelectCountryFragment :
    BaseBoundFragment<SelectCountryViewModel, FragmentSelectCountryBinding>(
        SelectCountryViewModel::class
    ) {
    override var layoutId: Int = R.layout.fragment_select_country
    override val fragmentNameResourceID: Int = R.string.select_country

    override fun setupDataBinding(binding: FragmentSelectCountryBinding) {
        binding.viewModel = mViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRecyclerView()
        initSearchByNameEditText()
        hideInputKeyboardWhenOpenFragment()
        hideAppBar()
    }

    private fun setupRecyclerView() {
        val layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        country_list.layoutManager = layoutManager
        country_list.adapter = mViewModel.countryAdapter
        country_list.addItemDecoration(
            DividerItemDecoration(
                country_list.context,
                DividerItemDecoration.VERTICAL
            )
        )
    }

    private fun initSearchByNameEditText() {
        select_country_edit_text.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence?, p1: Int, p2: Int, p3: Int) {
                mViewModel.filterList(charSequence.toString())
            }

        })
    }

    private fun hideInputKeyboardWhenOpenFragment() {
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    private fun hideAppBar()
    {
        (activity as AppCompatActivity?)?.supportActionBar?.hide()
    }
}
