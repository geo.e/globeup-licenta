package com.impaktsoft.globeup.listadapters

import android.location.Address
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.util.Pair
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.impaktsoft.globeup.models.EventPOJO
import com.impaktsoft.globeup.databinding.EventCellBinding
import com.impaktsoft.globeup.databinding.EventCellSectionBinding
import com.impaktsoft.globeup.enums.EventState
import com.impaktsoft.globeup.models.EventSectionPOJO
import com.impaktsoft.globeup.models.IEventsItem
import com.impaktsoft.globeup.viewmodels.DashboardViewModel
import android.location.Geocoder
import com.impaktsoft.globeup.databinding.DashboardEventCellBinding
import com.impaktsoft.globeup.models.EventDetailPOJO
import java.util.*
import kotlin.collections.ArrayList


class DashboardEventsAdapter(
    private var arrayList: List<IEventsItem>,
    private val viewModel: DashboardViewModel
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val EventSection = 0
        private const val Event = 1
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun getItemViewType(position: Int): Int {
        val event = arrayList[position]

        if (event is EventSectionPOJO) {
            return EventSection
        } else if (event is EventPOJO) {
            return Event
        }

        throw IllegalArgumentException("Unsupported view type")
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)

        return when (viewType) {
            EventSection -> {
                val applicationBinding =
                    EventCellSectionBinding.inflate(layoutInflater, parent, false)
                EventSectionViewHolder(applicationBinding)
            }

            Event -> {
                val applicationBinding =
                    DashboardEventCellBinding.inflate(layoutInflater, parent, false)
                EventViewHolder(applicationBinding)
            }

            else -> throw IllegalArgumentException("Unsupported view type for view holder")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val myObj = arrayList[position];

        when (holder) {
            is EventSectionViewHolder -> holder.bind(myObj as EventSectionPOJO)
            is EventViewHolder -> holder.bind(myObj as EventPOJO)
        }
    }


    inner class EventSectionViewHolder(var applicationBinding: EventCellSectionBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(item: EventSectionPOJO) {
            applicationBinding.sectionName = item.name
        }
    }

    inner class EventViewHolder(var applicationBinding: DashboardEventCellBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(item: EventPOJO) {
            applicationBinding.item = item
            applicationBinding.eventState = viewModel.getEventState(item.uid!!)
            applicationBinding.viewModel = viewModel

            var pairs: Array<Pair<View, String>>?
            if (viewModel.getEventState(item.uid!!) == EventState.HOSTING) {
                pairs = arrayOf(
                    Pair<View, String>(
                        applicationBinding.eventCellImage,
                        applicationBinding.eventCellImage.transitionName
                    )
                    , Pair<View, String>(
                        applicationBinding.eventName,
                        applicationBinding.eventName.transitionName
                    )
                )
            } else {
                pairs = arrayOf(
                    Pair<View, String>(
                        applicationBinding.eventCellImage,
                        applicationBinding.eventCellImage.transitionName
                    )
                    , Pair<View, String>(
                        applicationBinding.eventName,
                        applicationBinding.eventName.transitionName
                    )
                    , Pair<View, String>(
                        applicationBinding.upcmingEventStatusLayout,
                        applicationBinding.upcmingEventStatusLayout.transitionName
                    )
                )

            }

            val eventState = viewModel.getEventState(item.uid!!)
            var eventDetailItem = EventDetailPOJO(item, viewModel.myUserEventList, eventState)
            val itemJson = Gson().toJson(eventDetailItem)
            applicationBinding.eventCellClickLayout.setOnClickListener {

                viewModel.eventClick(pairs, itemJson)
            }

            applicationBinding.item = item
            if (item == arrayList.last()) {
                val layoutParams: ViewGroup.MarginLayoutParams =
                    applicationBinding.eventCellCarview.layoutParams as ViewGroup.MarginLayoutParams
                layoutParams.setMargins(
                    layoutParams.leftMargin, layoutParams.topMargin,
                    layoutParams.rightMargin, 120
                )
                applicationBinding.eventCellCarview.layoutParams = layoutParams
            }

        }
    }

}