package com.impaktsoft.globeup.viewmodels

import androidx.lifecycle.MutableLiveData
import com.impaktsoft.globeup.listadapters.GlobalAdapter
import com.impaktsoft.globeup.dummy.GlobalDummyData
import com.impaktsoft.globeup.views.activities.DonationActivity

class GlobalViewModel : BaseViewModel() {

    var text = MutableLiveData<String>("This is the global fragment")
    var adapter:GlobalAdapter?=null

    init {
        adapter= GlobalAdapter(GlobalDummyData.listOfGlobalItems)
    }

    fun donateClick()
    {
        navigation.navigateToActivity(DonationActivity::class.java,false,false,null,null)
    }

}