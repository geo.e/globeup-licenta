package com.impaktsoft.globeup.views.activities

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.recyclerview.widget.GridLayoutManager
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.databinding.ActivityBuyGcoinBinding
import com.impaktsoft.globeup.viewmodels.BuyGCoingViewModel
import kotlinx.android.synthetic.main.activity_buy_gcoin.*

class BuyGCoinActivity : BaseBoundActivity<BuyGCoingViewModel,ActivityBuyGcoinBinding>(BuyGCoingViewModel::class) {

    override var activityTitleResourceId: Int?=R.string.title_activity_buy_gcoin

    override val layoutId: Int=R.layout.activity_buy_gcoin

    override fun setupDataBinding(binding: ActivityBuyGcoinBinding) {
        binding.viewModel=mViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val layoutManager=GridLayoutManager(this,2)
        gcoins_offerts_list.layoutManager=layoutManager
        gcoins_offerts_list.adapter=mViewModel.adapter
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.exit_action_bar_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.settings_exit -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
