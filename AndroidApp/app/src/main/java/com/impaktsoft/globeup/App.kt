package com.impaktsoft.globeup

import android.app.Application
import com.impaktsoft.globeup.services.*
import com.impaktsoft.globeup.services.database.DatabaseService
import com.impaktsoft.globeup.services.database.IDatabaseService
import com.impaktsoft.globeup.viewmodels.*
import com.impaktsoft.globeup.views.activities.*
import com.impaktsoft.globeup.views.fragments.*
import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.core.qualifier.named
import org.koin.dsl.module


class App : Application() {

    private val appModule = module {

        single<ICurrentActivityService> { CurrentActivityService() }
        single<INavigationService> { NavigationService(get()) }
        single<IConnectivityService> { ConnectivityService(get()) }
        single<IDatabaseService> { DatabaseService(get(),get()) }
        single<IResourceService> { ResourceService() }
        single<IBackendService> { BackendService(get()) }
        single<IDialogService> { DialogService(get(), get()) }
        single<IPermissionService> { PermissionService(get()) }
        single<IFileStoreService> { FileStoreService() }
        single<IDataExchangeService> { DataExchangeService() }
        single<IMapService> { MapService(get()) }
        single<ISharedPreferencesService>{SharedPreferencesService(get())}
        single<IPopUpMenuService>{PopUpMenuService(get())}
        single<ILocationService> {LocationService(get())  }
        single<IPendingMessagesService> {PendingMessagesService(get(),get(),get(),get())  }
        single<IShowGoogleMapService> {ShowGoogleMapService(get())  }
        single<IAlertBuilderService> {AlertBuilderService(get())  }
        single<IActivityResultService>{ActivityResultService(get())}
        single<IOnBackPressService>{OnBackPressService()}
        single<IFirebaseInvitationService>{FirebaseInvitationService()}
        single<IRateInPlayStore>{RateInPlayStoreService(get())}
        single<IFacebookService>{FacebookService()}

        scope(named<SplashActivity>()) {
            viewModel { SplashViewModel() }
        }

        scope(named<MainActivity>()) {
            viewModel { MainViewModel() }
        }

        scope(named<GlobalFragment>()) {
            viewModel { GlobalViewModel() }
        }

        scope(named<EventsFragment>()) {
            viewModel { EventsViewModel() }
        }

        scope(named<AddEventActivity>()) {
            viewModel { AddEventViewModel() }
        }

        scope(named<DashboardFragment>()) {
            viewModel { DashboardViewModel() }
        }

        scope(named<WalletBoundFragment>()) {
            viewModel { WalletViewModel() }
        }

        scope(named<MessagesBoundFragment>()) {
            viewModel { MessagesBoundViewModel() }
        }

        scope(named<LoginActivity>()) {
            viewModel { LoginViewModel() }
        }

        scope(named<SettingsFragment>()) {
            viewModel { SettingsViewModel() }
        }

        scope(named<AuthenticationActivity>())
        {
            viewModel { AuthenticationViewModel() }
        }

        scope(named<RegisterActivity>()) {
            viewModel { RegisterViewModel() }
        }

        scope(named<EditProfileFragment>()) {
            viewModel { EditProfileViewModel() }
        }
        scope(named<AlertActivity>()) {
            viewModel { AlertViewModel() }
        }
        scope(named<FeedbackFragment>()) {
            viewModel { FeedbackViewModel() }
        }

        scope(named<ExperienceActivity>()) {
            viewModel { ExperienceViewModel() }
        }

        scope(named<EventActivity>()) {
            viewModel { EventDetailViewModel() }
        }

        scope(named<DonationActivity>()) {
            viewModel { DonationViewModel() }
        }

        scope(named<DonationDoneFragment>()) {
            viewModel { DonationDoneViewModel() }
        }

        scope(named<SearchEventsByNameActivity>())
        {
            viewModel { SearchEventsByNameViewModel() }
        }

        scope(named<FilterEventsFragment>())
        {
            viewModel { FilterEventsViewModel() }
        }

        scope(named<FilterEventsByRadiusFragment>())
        {
            viewModel { FilterEventsByRadiusViewModel() }
        }

        scope(named<SettingsActivity>())
        {
            viewModel { SettingsActivityViewModel() }
        }

        scope(named<ForgetPasswordActivity>())
        {
            viewModel { ForgetPasswordViewModel() }
        }

        scope(named<ContactsActivity>())
        {
            viewModel { ContactsViewModel() }
        }

        scope(named<ChooseContactsFragment>())
        {
            viewModel { ChooseContactsViewModel() }
        }

        scope(named<SelectGroupMembersFragment>())
        {
            viewModel { SelectMembersViewModel() }
        }

        scope(named<CreateGroupFragment>())
        {
            viewModel { CreateGroupViewModel() }
        }

        scope(named<MessengerFragment>())
        {
            viewModel { MessengerViewModel() }
        }

        scope(named<BuyGCoinActivity>())
        {
            viewModel { BuyGCoingViewModel() }
        }

        scope(named<GetCouponActivity>())
        {
            viewModel { GetCouponViewModel() }
        }

        scope(named<WithdrawActivity>())
        {
            viewModel { WithdrawViewModel() }
        }

        scope(named<SelectCountryFragment>())
        {
            viewModel { SelectCountryViewModel() }
        }

        scope(named<EditEventActivity>())
        {
            viewModel { EditEventViewModel() }
        }

        scope(named<ContainerActivity>())
        {
            viewModel { ContainerViewModel() }
        }

        scope(named<InitUserActivity>())
        {
            viewModel { InitUserViewModel() }
        }

        scope(named<MessengerActivity>())
        {
            viewModel { MessengerActivityViewModel() }
        }

        scope(named<PreviewImageActivity>())
        {
            viewModel { PreviewImageViewModel() }
        }

        scope(named<ImageCaptureFragment>()) {
            viewModel { ImageCaptureViewModel() }
        }

        scope(named<DeleteAccountFragment>()) {
            viewModel { DeleteAccountViewModel() }
        }

    }

    private val activityService: ICurrentActivityService by inject()
    private val resourceService: IResourceService by inject()

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@App)
            modules(appModule)
        }

        activityService.initWithApplication(this@App)
        resourceService.initWithApplication(this@App)
    }
}