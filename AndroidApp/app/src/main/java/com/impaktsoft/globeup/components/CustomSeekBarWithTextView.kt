package com.impaktsoft.globeup.components

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.RelativeLayout
import android.widget.SeekBar
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.lifecycle.MutableLiveData
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.enums.ScaleTypeEnum


class CustomSeekBarWithTextView : RelativeLayout {
    constructor(var1: Context) : super(var1)
    constructor(var1: Context, var2: AttributeSet) : super(var1, var2)
    constructor(var1: Context, var2: AttributeSet, var3: Int) : super(var1, var2, var3)

    private var seekBar: SeekBar? = null
    private var textView: TextView? = null
    //in km by default
    private var scaleType = ScaleTypeEnum.KILOMETERS

    var progress = MutableLiveData<Int>()
    init {

        LayoutInflater.from(context).inflate(R.layout.custom_seek_bar_with_text, this, true)
        textView=findViewById(R.id.my_custom_seekbar_text)
        seekBar=findViewById(R.id.my_custom_seekbar)

        //TODO add in xml image,LG problem...
        //seekBar?.background=(this.resources.getDrawable(R.drawable.ic_selektor_line))
    }

    fun setScaleType(value : ScaleTypeEnum)
    {
        scaleType = value
        setText(0)
    }

    @SuppressLint("SetTextI18n", "NewApi")
    fun setText(value: Int) {
        val scaleTypeString: String
        if (scaleType == ScaleTypeEnum.MILES) {
            textView!!.text = value.toString() + context.getString(R.string.miles)
            scaleTypeString = value.toString() + context.getString(R.string.miles)
        } else {
            textView!!.text = value.toString() + context.getString(R.string.kilometers)
            scaleTypeString = value.toString() + context.getString(R.string.kilometers)
        }

        val paint = Paint()
        val bounds = Rect()

        paint.textSize = textView!!.textSize

        paint.getTextBounds(
            scaleTypeString,
            0,
            scaleTypeString.length,
            bounds
        )

        val stringWidth = bounds.width()

        //seekBar width
        val width = (seekBar!!.width
                    - seekBar!!.paddingLeft
                    -seekBar!!.paddingRight)

        val thumbPos =
            seekBar!!.paddingLeft + width * seekBar!!.progress / seekBar!!.max

        textView!!.translationX =
            thumbPos.toFloat() - stringWidth / 2 + seekBar!!.thumbOffset / 2
    }



    @RequiresApi(Build.VERSION_CODES.M)
    fun setNormalSeekbarBackground()
    {
        seekBar!!.progressDrawable=ColorDrawable(context!!.getColor(R.color.grey))
        textView!!.setTextColor(context!!.getColor(R.color.grey))

    }

    fun setMyProgress(progress:Int)
    {
        seekBar!!.progress=progress
        setText(progress)
    }

    fun getMyProgress(): Int? {
        return seekBar!!.progress
    }
}
