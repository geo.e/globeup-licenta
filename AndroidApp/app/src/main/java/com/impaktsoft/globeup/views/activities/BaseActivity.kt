package com.impaktsoft.globeup.views.activities

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.impaktsoft.globeup.components.MyCustomAppBarView
import com.impaktsoft.globeup.viewmodels.BaseViewModel
import org.koin.android.scope.currentScope
import org.koin.android.viewmodel.ext.android.viewModel
import kotlin.reflect.KClass

abstract class BaseActivity<out T : BaseViewModel>(vmClass: KClass<T>) : AppCompatActivity() {

    protected val mViewModel: T by currentScope.viewModel(this, vmClass)

    protected open var activityTitleResourceId: Int? = null
    protected var activitySubtitleResourceId: Int? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        lifecycle.addObserver(mViewModel)
        setupCustomActionBar()

        super.onCreate(savedInstanceState)
    }

    private fun setupCustomActionBar() {

        if (supportActionBar != null) {
            supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
            supportActionBar?.setDisplayShowCustomEnabled(true)

            val view = MyCustomAppBarView(this,
                        activityTitleResourceId, activitySubtitleResourceId)

            supportActionBar?.customView = view
        }
    }

    override fun onDestroy() {
        //TODO error again
        try {
            lifecycle.removeObserver(mViewModel)
            super.onDestroy()

        } catch (e: Exception) {
            Log.d("Error", e.message.toString() + " err")
        }
    }
}

abstract class BaseBoundActivity<out T : BaseViewModel, in K : ViewDataBinding>(vmClass: KClass<T>) :
    BaseActivity<T>(vmClass) {

    protected abstract val layoutId: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<K>(
            this, layoutId
        ).apply {
            this.lifecycleOwner = this@BaseBoundActivity
        }


        setupDataBinding(binding)
    }

    protected abstract fun setupDataBinding(binding: K)
}
