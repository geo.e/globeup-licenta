package com.impaktsoft.globeup.views.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.components.MyCustomAppBarView
import com.impaktsoft.globeup.components.MyCustomSearchAppBarView
import com.impaktsoft.globeup.viewmodels.BaseViewModel
import org.koin.android.scope.currentScope
import org.koin.android.viewmodel.ext.android.viewModel
import kotlin.reflect.KClass


abstract class BaseFragment<out T : BaseViewModel>(vmClass: KClass<T>) : Fragment() {

    protected val mViewModel: T by currentScope.viewModel(this, vmClass)

    protected abstract val layoutId: Int
    protected abstract val fragmentNameResourceID: Int

    protected open fun layoutInflated(root: View) {}

    private var navHostFragment: Fragment? = null
    private var navHostMain: Fragment? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(layoutId, container, false)
        layoutInflated(root)
        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        lifecycle.addObserver(mViewModel)

        navHostFragment =
            activity!!.supportFragmentManager.findFragmentById(R.id.nav_host)

        navHostMain = activity!!.supportFragmentManager.findFragmentById(R.id.nav_host_main)

        super.onActivityCreated(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        setActionBar()
    }

    override fun onDestroy() {
        try {

            lifecycle.removeObserver(mViewModel)
        }catch (e:Exception){
            Log.d("onDestroyError",e.message)
        }
        super.onDestroy()
    }

    fun setActionBar() {
        if (navHostFragment != null) {
            val backStackEntryCount = navHostFragment!!.childFragmentManager.backStackEntryCount

            if ((activity as AppCompatActivity).supportActionBar?.customView != null) {
                val myCustomActionBar: MyCustomAppBarView? =
                    (activity as AppCompatActivity).supportActionBar!!.customView as MyCustomAppBarView

                if (myCustomActionBar != null) {
                    myCustomActionBar.setTitle(getString(fragmentNameResourceID))

                    setCustomActionBarFunctionality(myCustomActionBar, backStackEntryCount, 1)
                }
            }
        }

        if (navHostMain != null) {

            if ((activity as AppCompatActivity).supportActionBar!!.customView is MyCustomSearchAppBarView) {
                (activity as AppCompatActivity).supportActionBar!!.displayOptions =
                    ActionBar.DISPLAY_SHOW_CUSTOM
                (activity as AppCompatActivity).supportActionBar?.setDisplayShowCustomEnabled(true)

                val myCustomAppBarView = MyCustomAppBarView(context!!)

                (activity as AppCompatActivity).supportActionBar?.customView = myCustomAppBarView

            }
            val myCustomActionBar: MyCustomAppBarView? =
                (activity as AppCompatActivity).supportActionBar!!.customView as MyCustomAppBarView
            myCustomActionBar!!.setTitle(getString(fragmentNameResourceID))

            myCustomActionBar.setBackVisibility(View.GONE)
        }

        if(navHostFragment == null && navHostMain == null)
        {
            val customAppBar = (activity as AppCompatActivity).supportActionBar?.customView
            if(customAppBar!=null && customAppBar is MyCustomAppBarView)
            customAppBar.setTitle(getString(fragmentNameResourceID))
        }
    }

    fun setCustomActionBarFunctionality(
        customActionBar: MyCustomAppBarView?,
        backStackCount: Int,
        stackLimit: Int
    ) {
        if (customActionBar != null) {
            if (backStackCount < stackLimit) {
                customActionBar.setBackVisibility(View.GONE)
            } else {
                customActionBar.setBackVisibility(View.VISIBLE)
                customActionBar.getBackImage()!!.setOnClickListener {
                    activity!!.onBackPressed()
                }

            }
        }
    }
}

abstract class BaseBoundFragment<out T : BaseViewModel, in K : ViewDataBinding>(vmClass: KClass<T>) :
    BaseFragment<T>(vmClass) {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<K>(inflater, layoutId, container, false)
            .apply {
                this.lifecycleOwner = this@BaseBoundFragment
            }
        setupDataBinding(binding)

        val root = binding.root
        layoutInflated(root)

        return root
    }

    protected abstract fun setupDataBinding(binding: K)
}



