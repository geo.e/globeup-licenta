package com.impaktsoft.globeup.models

import com.impaktsoft.globeup.enums.ActionType

class ActionPOJO {

    var actionName:String?=null
    var actionType:ActionType?=null

    constructor(actionName: String?, actionType: ActionType?) {
        this.actionName = actionName
        this.actionType = actionType
    }
}