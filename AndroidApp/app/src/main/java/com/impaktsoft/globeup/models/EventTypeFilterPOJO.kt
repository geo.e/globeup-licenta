package com.impaktsoft.globeup.models

import com.impaktsoft.globeup.enums.EventType

class EventTypeFilterPOJO : IFilterItem {

    var eventType: EventType? = null

    constructor(title: String?, eventType: EventType, checked: Boolean? = null) : super(
        title,
        checked
    ) {
        this.eventType = eventType
    }
}