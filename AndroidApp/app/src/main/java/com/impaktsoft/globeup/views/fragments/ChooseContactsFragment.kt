package com.impaktsoft.globeup.views.fragments

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.components.MyCustomSearchAppBarView
import com.impaktsoft.globeup.databinding.FragmentChooseContactsBinding
import com.impaktsoft.globeup.viewmodels.ChooseContactsViewModel
import kotlinx.android.synthetic.main.fragment_choose_contacts.*


class ChooseContactsFragment : BaseBoundFragment<ChooseContactsViewModel,FragmentChooseContactsBinding>(ChooseContactsViewModel::class)
{
    override var layoutId: Int = R.layout.fragment_choose_contacts
    override val fragmentNameResourceID: Int = R.string.choose

    private var myCustomSearchAppBarView:MyCustomSearchAppBarView?=null

    override fun setupDataBinding(binding: FragmentChooseContactsBinding) {
        binding.viewModel=mViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setMyActionBar()

        val verticalLayoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        contacts_list.layoutManager = verticalLayoutManager
        contacts_list.adapter = mViewModel.getAdapter()

        val lineSeparator = DividerItemDecoration(
            contacts_list.context,
            verticalLayoutManager.orientation
        )

        contacts_list.addItemDecoration(lineSeparator)

    }

    private fun setMyActionBar()
    {
        myCustomSearchAppBarView =
            (activity as AppCompatActivity).supportActionBar!!.customView as MyCustomSearchAppBarView

        if(myCustomSearchAppBarView!=null) {
            myCustomSearchAppBarView!!.setTitle(getString(fragmentNameResourceID))
            myCustomSearchAppBarView!!.setSubTitle(getString(R.string.choose_contacts,mViewModel.getContactsSize()))

        }
    }

    override fun onStop() {
        super.onStop()
        myCustomSearchAppBarView?.onStop()
    }

}
