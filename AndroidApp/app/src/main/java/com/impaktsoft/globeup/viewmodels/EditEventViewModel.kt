package com.impaktsoft.globeup.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.firestore.GeoPoint
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.enums.EventType
import com.impaktsoft.globeup.listeners.IClickListener
import com.impaktsoft.globeup.models.EventPOJO
import com.impaktsoft.globeup.util.DateHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

class EditEventViewModel : BaseViewModel() {

    var currentImageIndex = MutableLiveData(0)
    var eventName = MutableLiveData<String>()
    var eventDescription = MutableLiveData<String>()
    var eventType = MutableLiveData<EventType>(EventType.Unknown)
    var eventLocation = MutableLiveData<LatLng>()
    var isLoading = MutableLiveData<Boolean>(false)
    var images: ArrayList<Int>? = ArrayList()

    var datePickerCalendar = MutableLiveData<Calendar>()
    var timePickerCalendar = MutableLiveData<Calendar>()
    var spinnerEntries = listOf(EventType.Unknown,EventType.GarbageCollect, EventType.Recycling, EventType.HelpPeople,EventType.PlantTrees)

    private var currentEvent: EventPOJO? = null
    private var eventTimeInUTC = Calendar.getInstance()

    init {
        images?.add(R.drawable.ic_award_1)
        images?.add(R.drawable.ic_award_2)
        images?.add(R.drawable.ic_award_3)
        images?.add(R.drawable.ic_award_4)
        images?.add(R.drawable.ic_award_5)

        datePickerCalendar.value = Calendar.getInstance()
        timePickerCalendar.value = Calendar.getInstance()

        currentEvent = dataExchangeService.get<EventPOJO>(EditEventViewModel::class.qualifiedName!!)
        if (currentEvent != null)
            setupFields()
    }

    private fun setupFields() {
        currentImageIndex.value = getImageIndex(currentEvent?.image!!)
        eventName.value = currentEvent?.name
        eventDescription.value = currentEvent?.desciption

        val lat = currentEvent?.location?.latitude
        val lng = currentEvent?.location?.longitude
        eventLocation.value = LatLng(lat!!, lng!!)

        eventType.value = currentEvent?.eventType
        val eventCalendar = Calendar.getInstance()
        eventCalendar.timeInMillis = currentEvent?.eventDate!!
        datePickerCalendar.value = eventCalendar
        timePickerCalendar.value = eventCalendar
    }

    var c = 0;
    fun editClick() {

        if (eventDescription.value.isNullOrEmpty()) {
            dialogService.showSnackbar(R.string.err_add_description)
            return
        }
        if (mapService.getCountryCode(
                eventLocation.value!!.latitude,
                eventLocation.value!!.longitude
            ) == null
        ) {
            dialogService.showSnackbar(R.string.err_add_valid_location)
            return
        }

        setupEventDate()

        val country = mapService.getCountryCode(
            eventLocation.value!!.latitude,
            eventLocation.value!!.longitude
        )

        val newEvent = EventPOJO(
            images?.get(currentImageIndex.value!!),
            eventName.value,
            eventDescription.value,
            eventType.value,
            eventTimeInUTC.timeInMillis,
            GeoPoint(
                eventLocation.value?.latitude!!,
                eventLocation.value?.longitude!!
            ),
            country
        )

        newEvent.uid=currentEvent?.uid

        val localTime = Calendar.getInstance()
        localTime.timeInMillis = eventTimeInUTC.timeInMillis
        val offStetTime = DateHelper.getOffsetTimeInMillis()
        eventTimeInUTC.timeInMillis -= offStetTime

        isLoading.value = true

        viewModelScope.launch(Dispatchers.IO) {

            try {
                database.eventDao.updateEvent(newEvent)
                val conversationKey=database.chatDAO.getEventConversationKey(newEvent.uid!!)
                val newConversationName = newEvent.name!! +" "+resourceService.stringForId(R.string.chat)
                database.chatDAO.updateConversationName(conversationKey!!, newConversationName)
                withContext(Dispatchers.Main) {
                    isLoading.value = false
                    navigation.closeCurrentActivity()
                }
            } catch (e: Exception) {
                withContext(Dispatchers.Main) {

                    isLoading.value=false

                    val title = resourceService.stringForId(R.string.warning_text)
                    val message = resourceService.stringForId(R.string.problem_text)
                    val buttonText = resourceService.stringForId(R.string.retry_text)

                    dialogService.showAlertDialog(title!!, message!!, buttonText!!, object :
                        IClickListener {
                        override fun clicked() {
                            editClick()
                        }
                    })
                }
            }
        }
    }

    private fun setupEventDate() {
        val year = datePickerCalendar.value!!.get(Calendar.YEAR)
        val month = datePickerCalendar.value!!.get(Calendar.MONTH)
        val day = datePickerCalendar.value!!.get(Calendar.DAY_OF_MONTH)
        val hour = timePickerCalendar.value!!.get(Calendar.HOUR_OF_DAY)
        val minute = timePickerCalendar.value!!.get(Calendar.MINUTE)

        eventTimeInUTC.set(year, month, day, hour, minute)

    }

    private fun getImageIndex(imageId: Int): Int? {
        for (it in this.images!!) {
            if (it == imageId) {
                return images?.indexOf(imageId)
            }
        }
        return 0
    }
}