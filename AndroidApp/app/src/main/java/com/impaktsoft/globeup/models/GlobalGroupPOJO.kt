package com.impaktsoft.globeup.models

class GlobalGroupPOJO :IGlobalBaseItem{
    override var rank: Int?=null
    override var events: Int?=null
    override var experience: Int?=null
    override var name: String?=null
    var members:Int?=null

    constructor(events: Int?, experience: Int?, name: String?, members: Int?,rank:Int?) {
        this.events = events
        this.experience = experience
        this.name = name
        this.members = members
        this.rank=rank
    }
}