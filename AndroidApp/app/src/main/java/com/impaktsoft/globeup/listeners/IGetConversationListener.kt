package com.impaktsoft.globeup.listeners

import com.impaktsoft.globeup.models.ConversationPOJO

interface IGetConversationListener {

    fun getConversation(conversation: ConversationPOJO)
}