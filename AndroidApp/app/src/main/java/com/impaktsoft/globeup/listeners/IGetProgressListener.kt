package com.impaktsoft.globeup.listeners

interface IGetProgressListener
{
    fun getProgress(progress:Int)
}