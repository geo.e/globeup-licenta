package com.impaktsoft.globeup.services.database.Notifications

import com.impaktsoft.globeup.models.NotificationMessagePOJO

class NotificationRequest {
    var data:NotificationMessagePOJO?=null

    var to:String?=null

    constructor(data: NotificationMessagePOJO?, to: String?) {
        this.data = data
        this.to = "/topics/"+to
    }
}