package com.impaktsoft.globeup.models

abstract class IFilterItem {
    var title: String?=null
    var checked: Boolean?=null

    constructor(title: String?, checked: Boolean?) {
        this.title = title
        this.checked = checked
    }
}