package com.impaktsoft.globeup.viewmodels

import androidx.lifecycle.viewModelScope
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.enums.ScaleTypeEnum
import com.impaktsoft.globeup.models.CloseViewModelPOJO
import com.impaktsoft.globeup.util.SharedPreferencesKeys
import com.impaktsoft.globeup.views.activities.LoginActivity
import com.impaktsoft.globeup.views.activities.RegisterActivity
import com.impaktsoft.globeup.views.fragments.DeleteAccountFragment
import com.impaktsoft.globeup.views.fragments.FeedbackFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SettingsViewModel : BaseViewModel() {

    var isUserAnonymous = isCurrentUserAnonymous()
    var currentScaleTypeSelected: ScaleTypeEnum?

    val getEditOrRegisterText: String
        get() {
            if (isUserAnonymous)
                return resourceService.stringForId(R.string.register_now)!!
            return resourceService.stringForId(R.string.edit_profile)!!
        }

    init {
        currentScaleTypeSelected = sharedPreferencesService.readFromSharedPref<ScaleTypeEnum>(
            SharedPreferencesKeys.metricsSystemKey,
            ScaleTypeEnum::class.java
        )
        if (currentScaleTypeSelected == null) {
            currentScaleTypeSelected = ScaleTypeEnum.KILOMETERS
        }
    }

    override fun onResume() {
        super.onResume()
        //handle closeMessage
        val closeItem = dataExchangeService.get<CloseViewModelPOJO>(javaClass.name)
        if(closeItem != null)
        {
            navigation.closeCurrentActivity()
        }
    }
    fun setMetricsSystem(scaleTypeEnum: ScaleTypeEnum) {
        currentScaleTypeSelected = scaleTypeEnum
        sharedPreferencesService.writeInSharedPref(
            SharedPreferencesKeys.metricsSystemKey,
            currentScaleTypeSelected!!
        )
    }


    fun editClick() {
        if (isUserAnonymous)
            navigation.navigateToActivity(RegisterActivity::class.java, false)
        else
            navigation.navigateToFragment(
                R.id.action_settingsFragment_to_editFragment,
                R.id.nav_host_settings
            )
    }

    fun inviteFriendsClick() {
    }

    fun showFeedbackDialog() {
        dialogService.showDialog(FeedbackFragment::class)
    }

    fun showDeleteDialog() {
        dialogService.showDialog(DeleteAccountFragment::class)
    }

    fun rateInPlayStore()
    {
        rateInPlayStoreService.askRating()
    }

    fun logout() {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                facebookService.logoutIfPossible()
                backend.logoutCurrentUser()
                withContext(Dispatchers.Main) {
                    sharedPreferencesService.writeInSharedPref(
                        SharedPreferencesKeys.didUserLogOutKey,
                        true
                    )
                    navigation.navigateToActivity(LoginActivity::class.java, true, true)
                }
            } catch (e: Exception) {
                dialogService.showSnackbar(e.message!!)
            }
        }
    }
}