package com.impaktsoft.globeup.models

import com.impaktsoft.globeup.enums.MessageState

class LastMessageState:IMessagePOJO {

    var messageState:MessageState?=null

    constructor(messageState: MessageState?) {
        this.messageState = messageState
    }
}