package com.impaktsoft.globeup.views.fragments

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.databinding.FragmentDashboardBinding
import com.impaktsoft.globeup.viewmodels.DashboardViewModel
import kotlinx.android.synthetic.main.fragment_dashboard.*


class DashboardFragment :
    BaseBoundFragment<DashboardViewModel, FragmentDashboardBinding>(DashboardViewModel::class) {

    override var layoutId: Int = R.layout.fragment_dashboard
    override val fragmentNameResourceID: Int = R.string.dashboard_name

    override fun setupDataBinding(binding: FragmentDashboardBinding) {
        binding.viewModel = mViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.dashboard_action_bar_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.dashboard_settings -> {
                mViewModel.settingsClick()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //init EVENTS list
        eventsList.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        eventsList.adapter = mViewModel.eventsAdapter

        //init optionsMenu
        optins_list.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        optins_list.adapter=mViewModel.actionAdapter
    }

}