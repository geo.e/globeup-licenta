package com.impaktsoft.globeup.viewmodels

class MessengerActivityViewModel:BaseViewModel() {

    fun updateMessengerConversation(conversationJson:String)
    {
        dataExchangeService.put(MessengerViewModel::class.qualifiedName!!,conversationJson)
    }
}