package com.impaktsoft.globeup.views.fragments

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.components.MyCustomSearchAppBarView
import com.impaktsoft.globeup.databinding.FragmentCreateGroupBinding
import com.impaktsoft.globeup.viewmodels.CreateGroupViewModel
import kotlinx.android.synthetic.main.fragment_create_group.*


class CreateGroupFragment : BaseBoundFragment<CreateGroupViewModel,FragmentCreateGroupBinding>(CreateGroupViewModel::class) {

    override var layoutId: Int= R.layout.fragment_create_group
    override val fragmentNameResourceID: Int = R.string.choose

    private var myCustomSearchAppBarView:MyCustomSearchAppBarView?=null

    override fun setupDataBinding(binding: FragmentCreateGroupBinding) {
        binding.viewModel=mViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val horizontalLayoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        group_users_list.layoutManager = horizontalLayoutManager as RecyclerView.LayoutManager?
        group_users_list.adapter = mViewModel.getAdapter()

        setCustomSearchActionBar()
    }


    fun setCustomSearchActionBar() {

        myCustomSearchAppBarView =
            (activity as AppCompatActivity).supportActionBar!!.customView as MyCustomSearchAppBarView

        if (myCustomSearchAppBarView != null) {
            myCustomSearchAppBarView!!.setTitle(getString(R.string.new_group))
            myCustomSearchAppBarView!!.setSubTitle(getString(R.string.create_name))
        }
    }

    override fun onStop() {
        super.onStop()
        myCustomSearchAppBarView?.onStop()
    }

}
