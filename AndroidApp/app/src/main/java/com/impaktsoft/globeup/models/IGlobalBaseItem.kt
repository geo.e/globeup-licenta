package com.impaktsoft.globeup.models

interface IGlobalBaseItem {
    var name:String?
    var experience:Int?
    var events:Int?
    var rank:Int?

}