package com.impaktsoft.globeup.viewmodels

import android.content.Intent
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.android.gms.maps.model.LatLng
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.listeners.IOnFailListener
import com.impaktsoft.globeup.listeners.IOnSuccessListener
import com.impaktsoft.globeup.models.GReason
import com.impaktsoft.globeup.util.SharedPreferencesKeys
import com.impaktsoft.globeup.views.activities.ForgetPasswordActivity
import com.impaktsoft.globeup.views.activities.MainActivity
import com.impaktsoft.globeup.views.activities.RegisterActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.json.JSONObject
import java.lang.Exception

class LoginViewModel : BaseViewModel() {

    var email = MutableLiveData<String>()
    var password = MutableLiveData<String>()

    init {
        facebookService.initLoginManager(
            object : IOnSuccessListener {
                override fun OnSucces(response: JSONObject) {

                    val name = response.getString("first_name")
                    val last_name = response.getString("last_name")
                    val email = response.getString("email")
                    val imageUrl =
                        "https://graph.facebook.com/" + facebookService.getUserId() + "/picture?width=500&height=500"
                    val coords = sharedPreferencesService.readFromSharedPref<LatLng>(
                        SharedPreferencesKeys.userLocationKey, LatLng::class.java
                    )

                    viewModelScope.launch(Dispatchers.IO) {

                        val result = backend.signIn(email, "123456")
                        if (result.isSuccess) {
                            navigation.navigateToActivity(MainActivity::class.java, true)
                        } else {
                            if (result.error?.reason == GReason.NoConnection) {
                                dialogService.showSnackbar(R.string.err_no_internet)

                            } else {
                                if (backend.currentUser == null)
                                    backend.signInAnonymously()
                                val result =
                                    backend.linkAnonymousWithCredentials(email, "123456")

                                if (result.isSuccess) {
                                    val user = result.payload
                                    if (user != null) {
                                        val userInfo = com.impaktsoft.globeup.models.UserInfo(
                                            email,
                                            "$name $last_name",
                                            imageUrl,
                                            mapService.getCountryCode(
                                                coords!!.latitude,
                                                coords.longitude
                                            )
                                        )

                                        database.userInfoDao.store(user.uid, userInfo)
                                        navigation.navigateToActivity(
                                            MainActivity::class.java,
                                            true
                                        )
                                    }
                                } else dialogService.showSnackbar(result.error!!.debugMessage)

                            }
                        }
                    }
                }
            },
            object : IOnFailListener {
                override fun OnFailure(exception: Exception) {
                    Log.d("Error", exception.message)
                }
            })
    }

    fun loginClicked() {
        //TODO make necessary checks for the email and password
        viewModelScope.launch(Dispatchers.IO) {
            if (email.value != null &&
                password.value != null
            ) {
                showHud()

                val result = backend.signIn(email.value!!, password.value!!)
                if (result.isSuccess) {
                    navigation.navigateToActivity(MainActivity::class.java, true)
                } else {
                    if (result.error?.reason == GReason.NoConnection) {
                        dialogService.showSnackbar(R.string.err_no_internet)

                    } else {
                        dialogService.showSnackbar(R.string.err_unknown_error)

                    }
                }

                hideHud()
            } else {
                dialogService.showSnackbar(R.string.err_provide_email_password)
            }
        }
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        facebookService.onActivityResult(requestCode, resultCode, data)
    }


    fun forgetPasswordClicked() {
        navigation.navigateToActivity(ForgetPasswordActivity::class.java, false)

    }

    fun registerClicked() {
        navigation.navigateToActivity(RegisterActivity::class.java, true)
    }

}