package com.impaktsoft.globeup.models

import com.google.android.gms.maps.model.LatLng

class MessageLocationPOJO {
    var imageUrl: String = ""
    var latitude: Double = 0.0
    var longitude: Double = 0.0

    constructor(imageUrl: String, latitude: Double, longitude: Double)
    {
        this.imageUrl=imageUrl
        this.latitude=latitude
        this.longitude=longitude
    }

    constructor()
}