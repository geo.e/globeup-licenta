package com.impaktsoft.globeup.listeners

interface IOnBackPressedListener {
    fun backPressHasBeenPressed();
}