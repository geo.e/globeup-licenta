package com.impaktsoft.globeup.services

import android.app.Activity
import android.app.AlertDialog
import android.view.View
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.listeners.IClickListener
import com.impaktsoft.globeup.views.activities.AlertActivity
import kotlin.reflect.KClass

interface IDialogService {
    fun <T : Fragment> showDialog(fragmentClass: KClass<T>)
    fun hideDialog()
    fun showSnackbar(stringId: Int, duration: Int = Snackbar.LENGTH_LONG)
    fun showSnackbar(string: String, duration: Int = Snackbar.LENGTH_LONG)
    fun showAlertDialog(
        title: String,
        message: String,
        buttonText: String,
        clickMethod: IClickListener)
}

class DialogService(
    private val navigation: INavigationService,
    private val currentActivityService: ICurrentActivityService
) : IDialogService {

    override fun <T : Fragment> showDialog(fragmentClass: KClass<T>) {
        navigation.navigateToActivity(
            AlertActivity::class.java,
            false,
            false,
            AlertActivity.fragmentClassNameParam,
            fragmentClass.qualifiedName
        )
    }

    override fun hideDialog() {
        val activity: Activity? = currentActivityService.activity

        if (activity != null && activity is AlertActivity) {
            activity.finish()
        }
    }

    override fun showSnackbar(stringId: Int, duration: Int) {
        val activity: Activity? = currentActivityService.activity

        if (activity != null) {
            val parentLayout = activity.findViewById<View>(android.R.id.content)
            Snackbar.make(parentLayout, stringId, duration).show()
        }
    }

    override fun showSnackbar(string: String, duration: Int) {
        val activity: Activity? = currentActivityService.activity

        if (activity != null) {
            val parentLayout = activity.findViewById<View>(android.R.id.content)
            Snackbar.make(parentLayout, string, duration).show()
        }
    }

    override fun showAlertDialog(
        title: String,
        message: String,
        buttonText: String,
        clickMethod: IClickListener
    ) {
        val builder = AlertDialog.Builder(currentActivityService.activity)
        builder.setTitle(title)
        builder.setMessage(message)
        builder.setPositiveButton(
            buttonText
        ) { dialogInterface, _ ->
            clickMethod.clicked()
            dialogInterface?.dismiss()
        }
        builder.setNegativeButton(
            R.string.cancel
        ) { dialogAlert, _ ->
            dialogAlert?.dismiss()
        }

        builder.show()
    }

}