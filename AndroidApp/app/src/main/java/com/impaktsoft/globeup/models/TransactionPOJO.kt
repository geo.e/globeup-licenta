package com.impaktsoft.globeup.models

import com.impaktsoft.globeup.enums.TransactionType

class TransactionPOJO {

    var transactionType: TransactionType?=null
    var transactionDate:String?=null
    var gcoins:Int?=null
    var cash:Int?=null

    constructor(
        tranzitionType: TransactionType?,
        tranzitionDate: String?,
        gcoins: Int?,
        cash: Int?
    ) {
        this.transactionType = tranzitionType
        this.transactionDate = tranzitionDate
        this.gcoins = gcoins
        this.cash = cash
    }
}