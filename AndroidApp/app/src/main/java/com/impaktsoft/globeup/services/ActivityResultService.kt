package com.impaktsoft.globeup.services

import android.content.Intent
import android.util.Log
import com.impaktsoft.globeup.listeners.IGetActivityForResultListener

interface IActivityResultService {
    suspend fun startActivityForResult(
        intent: Intent,
        requestCode: Int,
        activityForResultListener: IGetActivityForResultListener
    )

    fun onActivityForResultService(requestCode: Int, resultCode: Int, data: Intent?)

}

class ActivityResultService(private val activityService: ICurrentActivityService) :
    IActivityResultService {

    private var activityForResultListener: IGetActivityForResultListener? = null

    override suspend fun startActivityForResult(
        intent: Intent,
        requestCode: Int,
        activityForResultListener: IGetActivityForResultListener
    ) {
        val activity = activityService.activity
        this.activityForResultListener = activityForResultListener
        activity?.startActivityForResult(intent, requestCode)
    }

    override fun onActivityForResultService(requestCode: Int, resultCode: Int, data: Intent?) {
        this.activityForResultListener?.activityForResult(requestCode, resultCode, data)
    }
}