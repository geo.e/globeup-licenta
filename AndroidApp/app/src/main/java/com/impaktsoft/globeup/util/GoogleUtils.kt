package com.impaktsoft.globeup.util

import com.google.android.gms.maps.model.LatLng

class GoogleUtils
{
    companion object{
        fun getMapImageURL(location: LatLng, width: Int, height: Int): String {

            val googleMapsAPIKey = "AIzaSyDlW_F6pr2_6kup4pWEQC5YgO9DSf5N0Ho"
            val api = "https://maps.googleapis.com/maps/api/staticmap";
            val markers = "markers=" + location.latitude + "," + location.longitude;
            val size = "zoom=18&size=" + 600 + "x" + 400;
            val key = "key=" + googleMapsAPIKey;

            return api + "?" + markers + "&" + size + "&" + key;

        }
    }
}

//http://maps.google.com/maps/api/staticmap?u003d45.5244155,22.369841\u0026zoom\u003d15\u0026size\u003d600x400\u0026sensor\u003dfalse\u0026key\u003dAIzaSyAegfW4fJNu8j4r3z41leu5uGtEVUe_N4c
//https://maps.googleapis.com/maps/api/staticmap?markers\u003d45.5246729,22.3757427\u0026zoom\u003d18\u0026size\u003d600x400\u0026key\u003dAIzaSyAegfW4fJNu8j4r3z41leu5uGtEVUe_N4c
/*
https://maps.googleapis.com/maps/api/staticmap?size=400x400&location=47.5763831,-122.4211769
&fov=80&heading=70&pitch=0
&key=AIzaSyAegfW4fJNu8j4r3z41leu5uGtEVUe_N4c*/
//{"imageUrl":"https://maps.googleapis.com/maps/api/staticmap?markers\u003d45.5244155,22.369841\u0026zoom\u003d18\u0026size\u003d600x400\u0026key\u003dAIzaSyDlW_F6pr2_6kup4pWEQC5YgO9DSf5N0Ho","latitude":45.5244155,"longitude":22.369841}