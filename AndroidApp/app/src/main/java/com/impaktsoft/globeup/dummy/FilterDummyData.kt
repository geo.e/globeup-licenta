package com.impaktsoft.globeup.dummy

import com.impaktsoft.globeup.enums.EventTimeFilterEnum
import com.impaktsoft.globeup.enums.EventType
import com.impaktsoft.globeup.models.EventTimeFilterPOJO
import com.impaktsoft.globeup.models.EventTypeFilterPOJO
import com.impaktsoft.globeup.models.IFilterItem

class FilterDummyData {

    companion object {
        val eventTypeFilters = listOf<IFilterItem>(
            EventTypeFilterPOJO("Garbage collect", EventType.GarbageCollect, false),
            EventTypeFilterPOJO("Recycing", EventType.Recycling, false),
            EventTypeFilterPOJO("Help people", EventType.HelpPeople, false),
            EventTypeFilterPOJO("Plant trees", EventType.PlantTrees, false),
            EventTypeFilterPOJO("All", EventType.Unknown, true)
        )
        val eventTimeFilters = listOf<IFilterItem>(
            EventTimeFilterPOJO("Today", EventTimeFilterEnum.TODAY, false),
            EventTimeFilterPOJO("This week", EventTimeFilterEnum.THIS_WEEK, false),
            EventTimeFilterPOJO("Next week", EventTimeFilterEnum.NEXT_WEEK, false),
            EventTimeFilterPOJO("This month", EventTimeFilterEnum.THIS_MONTH, false),
            EventTimeFilterPOJO("Next Mounth", EventTimeFilterEnum.NEXT_MONTH, false),
            EventTimeFilterPOJO("All", EventTimeFilterEnum.ALL, true)
        )

        fun resetFilter(filterName:String)
        {
            eventTimeFilters.forEach { if (it.title.equals(filterName))
            {
                it.checked=false
                eventTimeFilters.last().checked=true
                return
            }}
            eventTypeFilters.forEach { if (it.title.equals(filterName))
            {
                it.checked=false
                eventTypeFilters.last().checked=true
                return
            }}
        }
    }
}