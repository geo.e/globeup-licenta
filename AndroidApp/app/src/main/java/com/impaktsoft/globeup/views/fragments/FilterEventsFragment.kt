package com.impaktsoft.globeup.views.fragments

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.databinding.FragmentFilterEventsBinding
import com.impaktsoft.globeup.viewmodels.FilterEventsViewModel
import kotlinx.android.synthetic.main.fragment_filter_events.*
import android.view.ViewTreeObserver

class FilterEventsFragment :
    BaseBoundFragment<FilterEventsViewModel, FragmentFilterEventsBinding>(FilterEventsViewModel::class) {

    override var layoutId: Int = R.layout.fragment_filter_events
    override val fragmentNameResourceID: Int = R.string.title_events

    override fun setupDataBinding(binding: FragmentFilterEventsBinding) {
        binding.viewModel = mViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //triggered when a view has been created
        view.viewTreeObserver.addOnGlobalLayoutListener(object :
            ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                view.viewTreeObserver.removeOnGlobalLayoutListener(this)
                initLists(view.width)
            }
        })
    }

    fun initLists(spanCounts: Int) {
        val layoutManager = GridLayoutManager(context, spanCounts)
        val layoutManager2 = GridLayoutManager(context, spanCounts)

        filter_events_events_category.layoutManager = layoutManager
        filter_events_events_category.adapter = mViewModel.typeFiltersAdapter
        layoutManager.spanSizeLookup = mViewModel.typeFiltersAdapter!!.spanSizeLookup


        filter_events_events_date.layoutManager = layoutManager2
        filter_events_events_date.adapter = mViewModel.timeFiltersAdapter
        layoutManager2.spanSizeLookup = mViewModel.timeFiltersAdapter!!.spanSizeLookup

    }

}
