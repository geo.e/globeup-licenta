package com.impaktsoft.globeup.views.activities

import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.components.MyCustomSearchAppBarView
import com.impaktsoft.globeup.viewmodels.MainViewModel
import android.view.LayoutInflater
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.bottomnavigation.BottomNavigationMenuView
import com.impaktsoft.globeup.databinding.NewMessageNotificationCellBinding
import android.content.Context
import android.util.Log
import androidx.lifecycle.Observer


class MainActivity : BaseActivity<MainViewModel>(MainViewModel::class) {

    private var mBinding: NewMessageNotificationCellBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        val navController = findNavController(R.id.nav_host_main)

        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_events,
                R.id.navigation_dashboard,
               // R.id.navigation_global,
               // R.id.navigation_wallet,
                R.id.navigation_messages
            )
        )

        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        setUnreadMessagesNotificationBinding(navView)

        mViewModel.notificationCount.observe(this, Observer {
            mBinding?.notificationCount = it
        })
    }

    override fun onBackPressed() {
        val customSearchAppBar = supportActionBar?.customView
        if (customSearchAppBar != null && customSearchAppBar is MyCustomSearchAppBarView) {
            if (customSearchAppBar.checkSearchOptionsVisibility()) {
                customSearchAppBar.showActionBarName()
                customSearchAppBar.removeSearchText()
            } else {
                super.onBackPressed()
            }
        } else {
            super.onBackPressed()
        }
    }

    private fun setUnreadMessagesNotificationBinding(navView: BottomNavigationView) {
        val bottomNavigationMenuView = navView.getChildAt(0) as BottomNavigationMenuView
        val v = bottomNavigationMenuView.getChildAt(2)
        val itemView = v as BottomNavigationItemView

        val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        mBinding = NewMessageNotificationCellBinding.inflate(inflater, itemView, true)
    }
}
