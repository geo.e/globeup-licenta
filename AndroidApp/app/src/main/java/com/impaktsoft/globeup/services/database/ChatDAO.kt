package com.impaktsoft.globeup.services.database

import android.util.Log
import com.google.firebase.firestore.*
import com.impaktsoft.globeup.enums.MessageState
import com.impaktsoft.globeup.listeners.*
import com.impaktsoft.globeup.models.*
import kotlinx.coroutines.tasks.await

interface IChatDAO {

    suspend fun addEventConversation(eventKey: String, conversationKey: String)
    suspend fun addUserConversation(userKey: String, conversationKey: String)
    suspend fun addUserInConversation(userKey: String, conversationKey: String)

    suspend fun storeConversation(conversationPOJO: ConversationPOJO)
    suspend fun fetchConversation(conversationKey: String): ConversationPOJO?

    suspend fun fetchUserConversations(userKey: String): ArrayList<ConversationRefPOJO>?

    suspend fun removeUserConversation(userKey: String, conversationKey: String)
    suspend fun removeUserFromConversation(userKey: String, conversationKey: String)

    suspend fun sendMessage(
        messagePOJO: MessagePOJO,
        conversationKey: String,
        listener: IMessageSentListener?
    )

    suspend fun getEventConversationKey(eventKey: String): String?

    suspend fun addConversationMessagesObserver(
        conversationKey: String,
        messageListener: IGetMessagesListener
    )

    suspend fun fetchConversationMessages(conversationKey: String): ArrayList<MessagePOJO>

    suspend fun updateConversationLastMessage(conversationKey: String, messagePOJO: MessagePOJO)

    suspend fun updateLastMessageState(userKey: String, conversationKey: String)
    suspend fun removeLastMessageState(conversationKey: String)

    suspend fun addLastMessageCountObserver(
        conversationKey: String,
        seenCount: IGetLastMessageSeenCount
    )

    suspend fun addConversationMembersCountObserver(
        conversationKey: String,
        listener: IGetConversationMembersCount?
    )

    suspend fun addConversationMembersObserver(
        conversationKey: String,
        membersKeyListener: IGetMembersKeyListener
    )

    suspend fun updateMessageState(messageKey: String, conversationKey: String)

    suspend fun addConversationsObserver(userKey: String, listener: IGetConversationListener)

    suspend fun didUserSeenMessage(userKey: String, conversationKey: String): Boolean

    suspend fun updateConversationName(conversationKey: String,newName : String)

    suspend fun removeUserAllConversations(userKey: String)

    fun removeConversationMessagesCountObservers()
    fun removeConversationsLastMessageObserver()
    fun removeLastMessageDocument()

}

class ChatDAO(private val fireStore: FirebaseFirestore) : IChatDAO {

    private val conversationCollection = "Conversations"
    private val eventConversationsCollection = "EventConversations"
    private val userConversationsCollection = "UserConversations"
    private val membersPath = "Members"
    private val messagesPath = "Messages"
    private val userConversationPath = "ConversationsDocumets"
    private val messageDateField = "creationTimeUTC"
    private val lastMessageStateField = "LastMessageSeenBy"
    private val lastMessageField = "lastMessage"
    private val conversationNameField = "name"

    private var conversationObserver: ListenerRegistration? = null
    private var lastMessageObserver: ListenerRegistration? = null
    private var membersCountObserver: ListenerRegistration? = null
    private var membersObserver: ListenerRegistration? = null

    private var messagesQuery: Query? = null
    private var lastMessageDocumentLoaded: DocumentSnapshot? = null

    private val conversationsLastMessageObservers = ArrayList<ListenerRegistration>()

    override suspend fun addEventConversation(eventKey: String, conversationKey: String) {
        fireStore.collection(eventConversationsCollection)
            .document(eventKey)
            .set(ConversationRefPOJO(conversationKey))
            .addOnFailureListener { throw it }
            .addOnSuccessListener { Log.d("pulamea", "succes addEvvConv") }
            .await()
    }

    override suspend fun addUserConversation(userKey: String, conversationKey: String) {
        fireStore.collection(userConversationsCollection)
            .document(userKey)
            .collection(userConversationPath)
            .document(conversationKey)
            .set(ConversationRefPOJO(conversationKey))
            .addOnFailureListener { throw it }
            .addOnSuccessListener { Log.d("pulamea", "succes addUserConv") }
            .await()
    }

    override suspend fun addUserInConversation(userKey: String, conversationKey: String) {
        fireStore.collection(conversationCollection)
            .document(conversationKey)
            .collection(membersPath)
            .document(userKey)
            .set(ConversationMemberPOJO(userKey))
            .addOnFailureListener { throw it }
            .addOnSuccessListener { Log.d("pulamea", "succes addUsInConv") }
            .await()
    }

    override suspend fun storeConversation(
        conversationPOJO: ConversationPOJO
    ) {
        fireStore.collection(conversationCollection)
            .document(conversationPOJO.key)
            .set(conversationPOJO)
            .addOnFailureListener { throw it }
            .addOnSuccessListener { Log.d("pulamea", "succes addUsInConv") }
            .await()
    }

    override suspend fun fetchConversation(conversationKey: String): ConversationPOJO? {
        var conversationPOJO: ConversationPOJO? = null
        fireStore.collection(conversationCollection)
            .document(conversationKey)
            .get()
            .addOnFailureListener {
                throw it
            }
            .addOnSuccessListener {
                conversationPOJO = it.toObject(ConversationPOJO::class.java)
            }
            .await()

        return conversationPOJO
    }

    override suspend fun fetchUserConversations(userKey: String): ArrayList<ConversationRefPOJO>? {

        val userConversationsList = ArrayList<ConversationRefPOJO>()
        fireStore.collection(userConversationsCollection)
            .document(userKey)
            .collection(userConversationPath)
            .get()
            .addOnFailureListener { throw it }
            .addOnSuccessListener {
                for (querySnapshot in it) {
                    val conversationUserPOJO =
                        querySnapshot.toObject(ConversationRefPOJO::class.java)
                    userConversationsList.add(conversationUserPOJO)
                }
            }.await()

        return userConversationsList
    }


    override suspend fun removeUserConversation(userKey: String, conversationKey: String) {
        fireStore.collection(userConversationsCollection)
            .document(userKey)
            .collection(userConversationPath)
            .document(conversationKey)
            .delete()
            .addOnFailureListener { throw it }
            .addOnSuccessListener { Log.d("pulamea", "succes addUserConv") }
            .await()
    }

    override suspend fun removeUserFromConversation(userKey: String, conversationKey: String) {
        fireStore.collection(conversationCollection)
            .document(conversationKey)
            .collection(membersPath)
            .document(userKey)
            .delete()
            .addOnFailureListener { throw it }
            .addOnSuccessListener { Log.d("pulamea", "succes addUsInConv") }
            .await()
    }

    override suspend fun sendMessage(
        messagePOJO: MessagePOJO,
        conversationKey: String,
        listener: IMessageSentListener?
    ) {

        val msgRef = fireStore.collection(conversationCollection)
            .document(conversationKey)
            .collection(messagesPath)
            .document(messagePOJO.messageKey!!)

        val lastConversationMessage = fireStore.collection(conversationCollection)
            .document(conversationKey)

        val lastSeenDocuments = ArrayList<DocumentReference>()
        fireStore.collection(conversationCollection)
            .document(conversationKey)
            .collection(lastMessageStateField)
            .get()
            .addOnSuccessListener {
                for (doc in it.documents) {
                    lastSeenDocuments.add(doc.reference)
                }
            }
            .await()

        val batch = fireStore.batch()
        batch.set(msgRef, messagePOJO)
        batch.update(lastConversationMessage, lastMessageField, messagePOJO)

        for (doc in lastSeenDocuments) {
            batch.delete(doc)
        }

        batch.commit()
            .addOnSuccessListener {
                Log.d("pulamea", "succes send")
                listener?.messageHasBeenSend(messagePOJO.messageKey!!)
            }
            .addOnCompleteListener { Log.d("pulamea", "complete send") }
            .addOnFailureListener { Log.d("pulamea", "fail send" + it.message) }
    }

    override suspend fun getEventConversationKey(eventKey: String): String? {
        var conversationKey: String? = null
        fireStore.collection(eventConversationsCollection)
            .document(eventKey)
            .get()
            .addOnSuccessListener {
                val conversation = it.toObject(ConversationRefPOJO::class.java)
                if (conversation != null) {
                    conversationKey = conversation.conversationKey
                }
            }
            .await()
        return conversationKey
    }

    override suspend fun addConversationMessagesObserver(
        conversationKey: String,
        messageListener: IGetMessagesListener
    ) {
        conversationObserver = fireStore.collection(conversationCollection)
            .document(conversationKey)
            .collection(messagesPath)
            .addSnapshotListener { querySnapshot, e ->

                if (e != null) {
                    Log.d("Error", "Listen failed.", e)
                    return@addSnapshotListener
                }

                for (doc in querySnapshot!!.documentChanges) {
                    val messageObject = doc.document.toObject(MessagePOJO::class.java)
                    messageListener.getMessage(messageObject)
                }
            }
    }

    override suspend fun fetchConversationMessages(conversationKey: String): ArrayList<MessagePOJO> {

        val listOfMessages = ArrayList<MessagePOJO>()
        messagesQuery = if (lastMessageDocumentLoaded == null) {
            fireStore.collection(conversationCollection)
                .document(conversationKey)
                .collection(messagesPath)
                .orderBy(messageDateField, Query.Direction.DESCENDING)
                .limit(5)
        } else {
            fireStore.collection(conversationCollection)
                .document(conversationKey)
                .collection(messagesPath)
                .orderBy(messageDateField, Query.Direction.DESCENDING)
                .startAfter(lastMessageDocumentLoaded!!)
                .limit(5)
        }

        messagesQuery!!
            .get()
            .addOnSuccessListener {
                for (querySnapshot in it) {
                    val message = querySnapshot.toObject(MessagePOJO::class.java)
                    listOfMessages.add(message)
                }
                if (it.size() > 0) {
                    lastMessageDocumentLoaded = it.documents[it.size() - 1]
                }
            }
            .addOnFailureListener {
                throw it
            }
            .await()

        if (listOfMessages.isNullOrEmpty()) return ArrayList()
        if (listOfMessages.size == 1) return listOfMessages
        return listOfMessages.reversed() as ArrayList<MessagePOJO>
    }

    override suspend fun updateConversationLastMessage(
        conversationKey: String,
        messagePOJO: MessagePOJO
    ) {
        fireStore.collection(conversationCollection)
            .document(conversationKey)
            .update("lastMessage", messagePOJO)
    }

    override suspend fun updateLastMessageState(userKey: String, conversationKey: String) {
        fireStore.collection(conversationCollection)
            .document(conversationKey)
            .collection(lastMessageStateField)
            .document(userKey)
            .set(VisitatorPOJO(userKey))
    }

    override suspend fun removeLastMessageState(conversationKey: String) {
        fireStore.collection(conversationCollection)
            .document(conversationKey)
            .collection(lastMessageStateField)
            .get()
            .addOnSuccessListener {
                for (doc in it.documents) {
                    doc.reference.delete()
                }
            }
            .await()
    }

    override suspend fun addLastMessageCountObserver(
        conversationKey: String,
        seenCount: IGetLastMessageSeenCount
    ) {
        lastMessageObserver =
            fireStore.collection(conversationCollection)
                .document(conversationKey)
                .collection(lastMessageStateField)
                .addSnapshotListener { querySnapshot, e ->
                    if (e != null) {
                        Log.d("Error", "Listen failed.", e)
                        return@addSnapshotListener
                    }
                    if (querySnapshot?.documents?.size == 0) {
                        seenCount.getLastMessageSeenCount(0)
                    } else {
                        seenCount.getLastMessageSeenCount(querySnapshot?.documents?.size!!)
                    }
                }
    }

    override suspend fun addConversationMembersCountObserver(
        conversationKey: String,
        listener: IGetConversationMembersCount?
    ) {
        membersCountObserver = fireStore.collection(conversationCollection)
            .document(conversationKey)
            .collection(membersPath)
            .addSnapshotListener { querySnapshot, e ->
                if (e != null) {
                    Log.d("Error", "Listen failed.", e)
                    return@addSnapshotListener
                }

                Log.d("pulamea", querySnapshot.toString())
                listener?.getConversationMembersCount(querySnapshot!!.documents.size)
            }
    }

    override suspend fun addConversationMembersObserver(
        conversationKey: String,
        membersKeyListener: IGetMembersKeyListener
    ) {
        val conversationMembers = ArrayList<ConversationMemberPOJO>()
        membersObserver = fireStore.collection(conversationCollection)
            .document(conversationKey)
            .collection(membersPath)
            .addSnapshotListener { querySnapshot, e ->
                if (e != null) {
                    Log.d("Error", "Listen failed.", e)
                    return@addSnapshotListener
                }
                conversationMembers.clear()
                querySnapshot?.forEach {
                    var conversationMember=it.toObject(ConversationMemberPOJO::class.java)
                    conversationMembers.add(conversationMember)
                }
                membersKeyListener.getMembersKeyListener(conversationMembers)
            }
    }

    override suspend fun updateMessageState(messageKey: String, conversationKey: String) {
        fireStore.collection(conversationCollection)
            .document(conversationKey)
            .collection(messagesPath)
            .document(messageKey)
            .update("state", MessageState.RECEIVED)
            .addOnSuccessListener { Log.d("pulamea", "succes update state") }
            .addOnFailureListener { Log.d("pulamea", "fail update state") }
            .await()
    }

    override suspend fun addConversationsObserver(
        userKey: String,
        listener: IGetConversationListener
    ) {
        val userConversations = fetchUserConversations(userKey)
        userConversations?.forEach {
            val listenerRegistration = fireStore.collection(conversationCollection)
                .document(it.conversationKey!!)
                .addSnapshotListener { documentSnapshot, e ->
                    if (e != null) {
                        Log.d("Error", "Listen failed.", e)
                    }
                    else{
                        listener.getConversation(documentSnapshot?.toObject(ConversationPOJO::class.java)!!)
                    }
                }
            conversationsLastMessageObservers.add(listenerRegistration)
        }
    }

    override suspend fun didUserSeenMessage(userKey: String, conversationKey: String): Boolean {
        var didUserSeenMessage = false
        fireStore.collection(conversationCollection)
            .document(conversationKey)
            .collection(lastMessageStateField)
            .document(userKey).get()
            .addOnFailureListener { throw it }
            .addOnSuccessListener {
                val visitor = it.toObject(VisitatorPOJO::class.java)
                didUserSeenMessage = visitor != null
            }
            .await()
        return didUserSeenMessage
    }

    override suspend fun updateConversationName(conversationKey: String,newName : String) {
        fireStore.collection(conversationCollection)
            .document(conversationKey)
            .update(conversationNameField,newName)
            .addOnFailureListener { throw it }
            .await()
    }

    override suspend fun removeUserAllConversations(userKey: String) {
        fetchUserConversations(userKey)?.forEach {
            removeUserConversation(userKey, it.conversationKey!!)
        }
    }

    override fun removeConversationMessagesCountObservers() {
        conversationObserver?.remove()
        lastMessageObserver?.remove()
        membersCountObserver?.remove()
        membersObserver?.remove()

    }

    override fun removeConversationsLastMessageObserver() {
        conversationsLastMessageObservers.forEach {
            it.remove()
        }
    }

    override fun removeLastMessageDocument() {
        lastMessageDocumentLoaded = null
    }

}