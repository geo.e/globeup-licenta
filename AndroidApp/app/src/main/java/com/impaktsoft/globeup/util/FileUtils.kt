package com.impaktsoft.globeup.util

import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.provider.MediaStore

class FileUtils {
    companion object
    {
        fun getRealPathFromURI(context: Context, uri: Uri?): String? {
            val cursor: Cursor =
                context.contentResolver.query(uri!!, null, null, null, null)!!
            cursor.moveToFirst()
            val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            return cursor.getString(idx)
        }
    }
}