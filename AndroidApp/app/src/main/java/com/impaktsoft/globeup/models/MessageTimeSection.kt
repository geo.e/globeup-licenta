package com.impaktsoft.globeup.models

class MessageTimeSection:IMessagePOJO {
    var timeString:String?=null
    var timeLong:Long?=null

    constructor(timeString: String?,timeLong:Long?) {
        this.timeString = timeString
        this.timeLong = timeLong
    }
}