package com.impaktsoft.globeup.listeners

import java.lang.Exception

interface IOnFailListener {
    fun OnFailure(exception: Exception)
}