package com.impaktsoft.globeup.listeners

import com.google.android.gms.maps.model.LatLng

interface IGetLocationListener
{
    fun getLocation(coords:LatLng?)
}