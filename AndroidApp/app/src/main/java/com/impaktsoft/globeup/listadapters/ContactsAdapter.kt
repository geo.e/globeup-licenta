package com.impaktsoft.globeup.listadapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.impaktsoft.globeup.databinding.ContactsCellBinding
import com.impaktsoft.globeup.models.UserPOJO

class ContactsAdapter(
    private var arrayList:ArrayList<UserPOJO>
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)

        val applicationBinding =
            ContactsCellBinding.inflate(layoutInflater, parent, false)
        return ContactsViewHolder(applicationBinding)

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val userPOJO = arrayList[position]
        (holder as ContactsViewHolder).bind(userPOJO)
    }

    inner class ContactsViewHolder(var applicationBinding: ContactsCellBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(item: UserPOJO) {

            applicationBinding.user = item
        }
    }

    fun setList(auxList:ArrayList<UserPOJO>)
    {
        arrayList=auxList
        notifyDataSetChanged()
    }
}