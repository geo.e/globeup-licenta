package com.impaktsoft.globeup.viewmodels

import androidx.lifecycle.MutableLiveData
import com.google.firebase.firestore.auth.User
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.dummy.ContactsDummyData
import com.impaktsoft.globeup.enums.ContactState
import com.impaktsoft.globeup.listadapters.GroupContactsAdapter
import com.impaktsoft.globeup.listadapters.SelectedUsersAdapter
import com.impaktsoft.globeup.listeners.IClickListener
import com.impaktsoft.globeup.listeners.IGetUserListener
import com.impaktsoft.globeup.models.ContactPOJO
import com.impaktsoft.globeup.models.UserPOJO
import kotlinx.android.synthetic.main.activity_contacts.*
import kotlinx.android.synthetic.main.activity_image.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.Collections.list

class SelectMembersViewModel : BaseViewModel() {

    private var usersContactsAdapter: GroupContactsAdapter? = null
    private var selectedUsersAdapter: SelectedUsersAdapter? = null

    private var selectedUsers = ArrayList<UserPOJO>()
    private var myUsers = ArrayList<UserPOJO>()
    private var contacts = ArrayList<ContactPOJO>()

    var selectedUsersSize=MutableLiveData<Int>()


    init {
        myUsers.addAll(ContactsDummyData.contactsList)
        contacts = getUsersAsContactForSelect()
        usersContactsAdapter = GroupContactsAdapter(this, contacts)

        selectedUsersAdapter = SelectedUsersAdapter(selectedUsers, object : IGetUserListener {
            override fun getUser(user: UserPOJO) {
                unselectUser(user)
            }
        })
    }

    fun getUsersSize():Int
    {
        return myUsers.size
    }
    fun getUsersAsContactForSelect(): ArrayList<ContactPOJO> {
        var contactForSelect = ArrayList<ContactPOJO>()
        for (user in myUsers) {
            contactForSelect.add(ContactPOJO(user, ContactState.NOT_SELECTED))
        }

        return contactForSelect
    }

    fun getUsersAdapter(): GroupContactsAdapter? {
        return usersContactsAdapter
    }

    fun getSelectedAdapter(): SelectedUsersAdapter? {
        return selectedUsersAdapter
    }

    fun createGroupClick() {
        navigation.navigateToFragment(
            R.id.action_selectGroupMembersFragment_to_createGroupFragment,
            R.id.nav_host_contacts
        )
    }

    fun unselectUser(user: UserPOJO) {
        selectedUsers.remove(user)
        selectedUsersSize.value=selectedUsers.size
        //uncheck user
        val index = myUsers.indexOf(user)
        contacts[index].selected = ContactState.NOT_SELECTED
        selectedUsersAdapter!!.notifyDataSetChanged()
        usersContactsAdapter!!.notifyDataSetChanged()
    }

    fun selectUser(user: UserPOJO) {
        if (!selectedUsers.contains(user)) {
            selectedUsers.add(user)
            selectedUsersSize.value=selectedUsers.size
            //check user
            val index = myUsers.indexOf(user)
            contacts[index].selected = ContactState.SELECTED
            selectedUsersAdapter!!.notifyDataSetChanged()
            usersContactsAdapter!!.notifyDataSetChanged()
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun filterList(string: String) {
        val auxList = java.util.ArrayList<ContactPOJO>()
        for (item in contacts) {
            val name = item.user?.name
            if (name!!.contains(string.toLowerCase()) || name.contains(string.toUpperCase()) || string.isEmpty()) {
                auxList.add(item)
            }
        }
        usersContactsAdapter?.setList(auxList)
    }

    override fun onCreate() {
        super.onCreate()
        EventBus.getDefault().register(this);
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }
}