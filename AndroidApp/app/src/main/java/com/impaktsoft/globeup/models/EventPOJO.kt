package com.impaktsoft.globeup.models

import com.google.firebase.firestore.GeoPoint
import com.impaktsoft.globeup.enums.EventType


open class EventPOJO : IEventsItem {

    override var name: String? = null

    var image: Int? = null
    var desciption: String? = null
    var eventType: EventType? = null
    var eventDate: Long? = null
    var location: GeoPoint? = null
    var uid: String? = null
    var eventCountry: String? = null

    constructor()

    constructor(
        image: Int?,
        name: String?,
        description: String?,
        eventType: EventType?,
        time: Long?,
        location: GeoPoint,
        eventCountry: String?
    ) {
        this.image = image
        this.name = name
        this.desciption = description
        this.eventType = eventType
        this.eventDate = time
        this.location = location
        this.eventCountry = eventCountry
    }

    override fun equals(other: Any?): Boolean {
        if (other is EventPOJO) {
            return other.uid.equals(this.uid)
        }
        return false
    }
}