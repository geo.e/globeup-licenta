package com.impaktsoft.globeup.services

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import com.google.android.gms.maps.model.LatLng
import com.impaktsoft.globeup.listeners.IGetLocationListener

interface ILocationService {
    fun getLocation(locationListener: IGetLocationListener)
    fun removeUpdates()
    fun isRunning():Boolean
}

class LocationService(private val activityService: ICurrentActivityService) : ILocationService {

    private var locationManager: LocationManager? = null
    private var mLocationListener: LocationListener? = null
    private var isRunning=false

    @SuppressLint("MissingPermission")
    override fun getLocation(locationListener: IGetLocationListener) {
        locationManager =
            activityService.activity?.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        isRunning=true

        mLocationListener = object : LocationListener {
            override fun onLocationChanged(p0: Location?) {
                val location = LatLng(p0!!.latitude, p0.longitude)
                val goodAccuracy = 50
                Log.d("pulamea",p0.accuracy.toString())
                if (p0.accuracy < goodAccuracy) {
                    Log.d("pulamea","got location")
                    locationListener.getLocation(location)
                    locationManager?.removeUpdates(this)
                    mLocationListener=null
                    isRunning=false
                }
            }

            override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {
            }

            override fun onProviderEnabled(p0: String?) {
            }

            override fun onProviderDisabled(p0: String?) {
            }
        }

        locationManager?.requestLocationUpdates(
            LocationManager.GPS_PROVIDER, 1000,
            0F, mLocationListener
        )
        locationManager?.requestLocationUpdates(
            LocationManager.NETWORK_PROVIDER, 1000,
            0F, mLocationListener
        )
    }

    override fun removeUpdates() {
        locationManager?.removeUpdates(mLocationListener)
        isRunning=false
    }

    override fun isRunning(): Boolean {
        return isRunning
    }
}