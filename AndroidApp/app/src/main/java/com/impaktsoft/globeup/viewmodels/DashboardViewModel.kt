package com.impaktsoft.globeup.viewmodels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.dummy.ActionDummyData
import com.impaktsoft.globeup.enums.ActionType
import com.impaktsoft.globeup.enums.EventState
import com.impaktsoft.globeup.listadapters.ActionAdapter
import com.impaktsoft.globeup.listadapters.DashboardEventsAdapter
import com.impaktsoft.globeup.models.*
import com.impaktsoft.globeup.views.activities.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception

class DashboardViewModel : BaseEventsViewModel() {

    var userImageUrl = MutableLiveData<String>()
    var userName =
        MutableLiveData<String>(resourceService.stringForId(R.string.anonymous))
    var userExperienceString = MutableLiveData<String>("25/100 XPS")
    var isShowedActionList = MutableLiveData(false)
    var isUserRegistered = MutableLiveData<Boolean>(false)
    var isProgressBarVisible = MutableLiveData<Boolean>(false)
    var isPlaceHolderMessageVisible = MutableLiveData<Boolean>(false)
    var placeholderText = MutableLiveData<String>()
    val actionAdapter = ActionAdapter(ActionDummyData.actionList, this)

    private var listOfAllEvents = ArrayList<IEventsItem>()
    private var eventsIHost = ArrayList<EventPOJO>()
    private var eventsImInteresting = ArrayList<EventPOJO>()
    private var eventsIJoin = ArrayList<EventPOJO>()
    private var hostListTitle = resourceService.stringForId(R.string.host_events_category)
    private var interestedListTitle =
        resourceService.stringForId(R.string.interested_events_category)
    private var joinListTitle = resourceService.stringForId(R.string.join_events_category)
    private var lastVMVisited: String? = null
    private var isUserSubscribedToChat = false

    init {
        eventsAdapter = DashboardEventsAdapter(eventsFromDatabaseList, this)

        viewModelScope.launch(Dispatchers.IO) {
            if(backend.currentUser != null)
                pendingService.resendPendingMessages(backend.currentUser!!.uid)
        }

    }

    override fun onResume() {
        super.onResume()

        val closeItem = dataExchangeService.get<CloseViewModelPOJO>(javaClass.name)
        if(closeItem != null)
        {
            navigation.closeCurrentActivity()
        }

        viewModelScope.launch(Dispatchers.IO) {
           try {
               uploadInfo()
           }
           catch (e:Exception)
           {
               Log.d("Error","upload fail in dashboardViewModel")
           }
        }
    }

    fun getEventState(eventUID: String): EventState {
        val eventState = EventState.UNKNOWN

        for (event in myUserEventList) {
            if (event.eventKey.equals(eventUID)) {
                return event.eventState!!
            }
        }

        return eventState
    }

    fun levelClicked() {
        navigation.navigateToActivity(LevelActivity::class.java, false)
    }

    fun loginClicked() {

        lastVMVisited = RegisterViewModel::class.qualifiedName
        navigation.navigateToActivity(RegisterActivity::class.java,false)
    }

    fun settingsClick() {
        navigation.navigateToActivity(SettingsActivity::class.java, false,false, null, null)
    }

    fun experienceClick() {
        navigation.navigateToActivity(ExperienceActivity::class.java, false,false, null, null)
    }

    fun mainActionButtonClicked() {
        isShowedActionList.value = !isShowedActionList.value!!
    }

    fun actionClicked(option: ActionType) {
        when (option) {
            ActionType.INVITE_FRIENDS -> {
                Log.d("myTest", "inviteFriends")
            }

            ActionType.HISTORY -> {
                Log.d("myTest", "history")

            }
        }
    }

    fun editClick(event: EventPOJO?) {
        dataExchangeService.put(EditEventViewModel::class.qualifiedName!!, event!!)
        navigation.navigateToActivity(EditEventActivity::class.java, false)
    }

    private suspend fun uploadInfo() {
            setCurrentUserInfo()
            fetchEvents()
    }

    private suspend fun fetchEvents() {

        withContext(Dispatchers.Main) {
            isPlaceHolderMessageVisible.value = false
            isProgressBarVisible.value = true

            myUserEventList.clear()
            listOfAllEvents.clear()
            eventsFromDatabaseList.clear()
        }

        myUserEventList.addAll(
            database.eventDao.fetchUserEvents(backend.currentUser!!.uid)
        )

        if (!isUserSubscribedToChat)subscribeToEventsChat()

        listOfAllEvents.addAll(
            database.eventDao.fetchEventsByKey(getListOfEventsKeys(myUserEventList))
        )

        withContext(Dispatchers.Main)
        {
            formatListWithCategories()
            isProgressBarVisible.value = false
            if (listOfAllEvents.isNullOrEmpty()) {
                showPlaceholder(true, resourceService.stringForId(R.string.no_events_message))
            }
            eventsAdapter?.notifyDataSetChanged()
        }
    }

    private suspend fun setCurrentUserInfo() {

        withContext(Dispatchers.Main)
        {
            isUserRegistered.value = isCurrentUserAnonymous()
        }
        viewModelScope.launch(Dispatchers.IO) {
            val user = backend.currentUser
            val userInfo = database.userInfoDao.fetch(user!!.uid)
            if (userInfo != null) {
                withContext(Dispatchers.Main) {
                    userImageUrl.value = userInfo.profileImage
                    userName.value = userInfo.name
                }
            }
        }
    }

    private fun getListOfEventsKeys(listOfUserEvents: ArrayList<UserEventPOJO>?): ArrayList<String>? {

        val listOfKeys = ArrayList<String>()
        for (event in listOfUserEvents!!) {
            listOfKeys.add(event.eventKey!!)
        }
        return listOfKeys
    }

    private fun formatListWithCategories() {
        if (listOfAllEvents.isEmpty()) return

        eventsIHost.clear()
        eventsIJoin.clear()
        eventsImInteresting.clear()

        for (event in listOfAllEvents) {
            addEventToHisCategory(event as EventPOJO)
        }
        if (eventsIHost.isNotEmpty()) {
            eventsFromDatabaseList.add(EventSectionPOJO(hostListTitle))
            eventsFromDatabaseList.addAll(eventsIHost)
        }
        if (eventsIJoin.isNotEmpty()) {
            eventsFromDatabaseList.add(EventSectionPOJO(joinListTitle))
            eventsFromDatabaseList.addAll(eventsIJoin)
        }
        if (eventsImInteresting.isNotEmpty()) {
            eventsFromDatabaseList.add(EventSectionPOJO(interestedListTitle))
            eventsFromDatabaseList.addAll(eventsImInteresting)
        }
    }

    private fun addEventToHisCategory(event: EventPOJO?) {
        when (getEventState(event?.uid!!)) {
            EventState.HOSTING -> eventsIHost.add(event)
            EventState.INTERESTED -> eventsImInteresting.add(event)
            EventState.JOINED -> eventsIJoin.add(event)
            else -> return
        }
    }

    private fun showPlaceholder(
        isPlaceHolderVisible: Boolean,
        placeHolderString: String?
    ) {
        isPlaceHolderMessageVisible.value = isPlaceHolderVisible

        placeholderText.value = placeHolderString
    }

    private fun subscribeToEventsChat() {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                myUserEventList.forEach {
                    if (it.eventState == EventState.JOINED || it.eventState == EventState.HOSTING) {
                        val conversationKey =
                            database.chatDAO.getEventConversationKey(it.eventKey!!)
                        database.messagingService.subscribeToTopic(conversationKey!!)
                    }
                }
                withContext(Dispatchers.Main) {
                    isUserSubscribedToChat = true;
                }
            }catch (e:Exception)
            {
                Log.d("Error","subscribeToEventsChat")
            }
        }
    }
}
