package com.impaktsoft.globeup.listeners

interface ILocationIsReady {
    fun locationIsReady()
}