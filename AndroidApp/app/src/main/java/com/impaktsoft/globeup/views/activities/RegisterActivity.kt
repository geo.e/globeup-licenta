package com.impaktsoft.globeup.views.activities

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.databinding.RegisterActivityBinding
import com.impaktsoft.globeup.viewmodels.RegisterViewModel


class RegisterActivity : BaseBoundActivity<RegisterViewModel,RegisterActivityBinding>(RegisterViewModel::class) {


    override var activityTitleResourceId: Int?
        get() = R.string.register_text
        set(value) {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override val layoutId: Int
        get() = R.layout.register_activity

    override fun setupDataBinding(binding: RegisterActivityBinding) {
        binding.viewModel = mViewModel
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater

        val x = supportFragmentManager.backStackEntryCount
      //  inflater.inflate(R.menu.exit_action_bar_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.settings_exit -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
