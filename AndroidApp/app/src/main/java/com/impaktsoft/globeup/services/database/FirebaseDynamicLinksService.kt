package com.impaktsoft.globeup.services.database

import android.net.Uri
import com.google.firebase.dynamiclinks.DynamicLink
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks

interface IFirebaseDynamicLinksService
{
    fun generateInvitationLink():Uri?
}
class FirebaseDynamicLinksService : IFirebaseDynamicLinksService
{
    private var firebaseDynamicLinks : FirebaseDynamicLinks? = null

    constructor(firebaseDynamicLinks: FirebaseDynamicLinks)
    {
        this.firebaseDynamicLinks = firebaseDynamicLinks
    }

    override fun generateInvitationLink(): Uri? {

        val baseUrl = Uri.parse("https://your-custom-name.page.link")
        val domain = "https://your-app.page.link"

        val link = firebaseDynamicLinks!!
            .createDynamicLink()
            .setLink(baseUrl)
            .setDomainUriPrefix(domain)
            .setAndroidParameters(DynamicLink.AndroidParameters.Builder("com.impaktsoft.globeup").build())
            .buildDynamicLink()

        return link.uri

    }

}