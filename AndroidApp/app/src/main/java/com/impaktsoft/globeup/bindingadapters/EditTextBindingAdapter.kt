package com.impaktsoft.globeup.bindingadapters

import android.annotation.TargetApi
import android.os.Build
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.EditText
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.impaktsoft.globeup.viewmodels.SearchEventsByNameViewModel
import com.impaktsoft.globeup.viewmodels.WithdrawViewModel

object EditTextBindingAdapter {

    @BindingAdapter("cb_withdrawText")
    @JvmStatic
    fun EditText.getWithdraw(viewModel: WithdrawViewModel) {
        this.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0.isNullOrEmpty()) {
                    viewModel.setWithdrawValue(0)

                } else {
                    viewModel.setWithdrawValue(p0.toString().toInt())
                }
            }

        })
    }

    @TargetApi(Build.VERSION_CODES.M)
    @BindingAdapter("cb_searchByName")
    @JvmStatic
    fun EditText.searchByName(viewModel: SearchEventsByNameViewModel) {

        this.addTextChangedListener(object:TextWatcher{
            override fun afterTextChanged(p0: Editable?) {
                viewModel.fetchEventsByString(p0.toString())
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

        })
    }
}