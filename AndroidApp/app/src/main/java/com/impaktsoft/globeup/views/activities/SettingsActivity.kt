package com.impaktsoft.globeup.views.activities

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.databinding.ActivitySettingsBinding
import com.impaktsoft.globeup.viewmodels.SettingsActivityViewModel

class SettingsActivity : BaseActivity<SettingsActivityViewModel>(SettingsActivityViewModel::class) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initDataBinding()
    }

    fun initDataBinding() {
        DataBindingUtil.setContentView<ActivitySettingsBinding>(
            this, R.layout.activity_settings
        ).apply {
            this.setLifecycleOwner(this@SettingsActivity)
            this.viewModel = mViewModel;
        }
    }
}
