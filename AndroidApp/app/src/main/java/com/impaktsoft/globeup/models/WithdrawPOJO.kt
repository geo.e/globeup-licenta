package com.impaktsoft.globeup.models

import com.impaktsoft.globeup.enums.WithdrawType

class WithdrawPOJO {
    var selected:Boolean=false
    var card_number:String?=null
    var withdrawType:WithdrawType?=null

    constructor(card_number: String?, withdrawType: WithdrawType?) {
        this.card_number = card_number
        this.withdrawType = withdrawType
    }
}