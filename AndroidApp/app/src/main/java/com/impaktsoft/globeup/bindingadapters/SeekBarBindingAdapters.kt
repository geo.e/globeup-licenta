package com.impaktsoft.globeup.bindingadapters

import android.widget.SeekBar
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import com.impaktsoft.globeup.components.CustomSeekBarWithTextView
import com.impaktsoft.globeup.enums.ScaleTypeEnum
import kotlinx.android.synthetic.main.custom_seek_bar_with_text.view.*


object SeekBarBindingAdapters {

    var lastValue: Int? = 0

    @BindingAdapter(value = ["cb_progressAttrChanged"])
    @JvmStatic
    fun CustomSeekBarWithTextView.setListener(listener: InverseBindingListener?) {
        if (listener != null) {
            this.my_custom_seekbar.setOnSeekBarChangeListener(object :
                SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                    listener.onChange()
                }

                override fun onStartTrackingTouch(p0: SeekBar?) {
                }

                override fun onStopTrackingTouch(p0: SeekBar?) {
                }

            })
        }
    }

    @BindingAdapter("cb_setMetricSystem")
    @JvmStatic
    fun CustomSeekBarWithTextView.setScaleType(value: ScaleTypeEnum?) {
        if (value != null) {
            this.setScaleType(value)
        }
    }


    @BindingAdapter("cb_progress")
    @JvmStatic
    fun CustomSeekBarWithTextView.setProgress(value: Int) {
        if (lastValue != value) {
            this.setMyProgress(value as Int)
        }
        lastValue = value
    }

    @InverseBindingAdapter(attribute = "cb_progress")
    @JvmStatic
    fun CustomSeekBarWithTextView.getProgress(): Int? {
        return this.getMyProgress()
    }

}
