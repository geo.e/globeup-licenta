package com.impaktsoft.globeup.views.activities

import android.location.Geocoder
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.annotation.RequiresApi
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.components.MapViewWithPin
import com.impaktsoft.globeup.databinding.ActivityAddEventBinding
import com.impaktsoft.globeup.databinding.ActivityEditEventBinding
import com.impaktsoft.globeup.viewmodels.EditEventViewModel
import kotlinx.android.synthetic.main.activity_add_event.*
import java.util.*
import kotlin.collections.ArrayList

class EditEventActivity :
    BaseBoundActivity<EditEventViewModel, ActivityEditEventBinding>(EditEventViewModel::class) {
    override var activityTitleResourceId: Int? = R.string.edit_event_title

    override val layoutId: Int = R.layout.activity_edit_event

    private val MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey"
    var mapView: MapViewWithPin? = null

    override fun setupDataBinding(binding: ActivityEditEventBinding) {
        binding.viewModel = mViewModel
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var mapViewBundle: Bundle? = null

        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY)
        }


        mapView = map_with_ping
        mapView?.setMapBundle(mapViewBundle)
        mapView?.getMapAsync()
        lifecycle.addObserver(mapView!!)
        val geocoder = Geocoder(this, Locale.getDefault())
        val countries = getCountries()

    }

    private fun getCountries(): ArrayList<String> {
        val localeList = Locale.getAvailableLocales()
        var countries = ArrayList<String>()
        for (local in localeList) {
            var countryName = local.displayCountry
            if (countryName.trim().isNotEmpty() && !countries.contains(countryName)) {
                countries.add(countryName)
            }
        }
        countries.sort()

        return countries
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.exit_action_bar_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.settings_exit -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}