package com.impaktsoft.globeup.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.impaktsoft.globeup.enums.EventState
import com.impaktsoft.globeup.listeners.IClickListener
import com.impaktsoft.globeup.models.EventDetailPOJO
import com.impaktsoft.globeup.models.EventPOJO
import com.impaktsoft.globeup.models.IEventsItem
import com.impaktsoft.globeup.views.activities.MessengerActivity
import com.impaktsoft.globeup.views.fragments.MessengerFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class EventDetailViewModel : BaseEventsViewModel() {

    var mapVisible = MutableLiveData<Boolean>(false)

    var currentEventItem: EventPOJO? = null;
    var currentUserEventState = MutableLiveData<EventState>()
    var currentEventMembers = MutableLiveData(-1)
    var isSpinnerVisible = MutableLiveData<Boolean>(false)

    init {
        initCurrentEvent()
        defineEventInteractionsActions()
    }

    fun setMapVisibility(boolean: Boolean) {
        mapVisible.value = boolean
    }

    fun backImageClicked() {
        if (eventInteractionJob != null && eventInteractionJob?.isActive!!) return
        navigation.closeCurrentActivityAfterTransition()
    }

    fun openChat()
    {
        isSpinnerVisible.value = true
       viewModelScope.launch(Dispatchers.IO) {
           val eventConversationKey = database.chatDAO.getEventConversationKey(currentEventItem?.uid!!)
           val eventConversation = database.chatDAO.fetchConversation(eventConversationKey!!)
           val conversationJson = Gson().toJson(eventConversation)
          withContext(Dispatchers.Main){
              dataExchangeService.put(MessengerViewModel::class.qualifiedName!!, conversationJson!!)
              navigation.navigateToActivity(MessengerActivity::class.java, true)
              isSpinnerVisible.value = false
          }
       }
    }

    private fun initCurrentEvent() {
        val eventJson: String? = dataExchangeService.get(EventsViewModel::class.qualifiedName!!)
        val currentEventDetail = Gson().fromJson(eventJson, EventDetailPOJO::class.java)
        currentEventItem = currentEventDetail.eventPOJO
        myUserEventList = currentEventDetail.userEventList
        eventsFromDatabaseList.add(currentEventItem as IEventsItem)
        currentUserEventState.value = currentEventDetail.eventState

        viewModelScope.launch(Dispatchers.Main) {
            currentEventMembers.value =
                database.eventDao.getEventMembersCount(currentEventItem!!.uid!!)
        }
    }

    private fun defineEventInteractionsActions() {
        startAction = object : IClickListener {
            override fun clicked() {
                currentUserEventState.value = getMyUserEventPOJO(currentEventItem?.uid)?.eventState
                isSpinnerVisible.value = true
            }
        }
        finishAction = object : IClickListener {
            override fun clicked() {
                currentUserEventState.value = getMyUserEventPOJO(currentEventItem?.uid)?.eventState
                isSpinnerVisible.value = false
            }
        }
    }

}
