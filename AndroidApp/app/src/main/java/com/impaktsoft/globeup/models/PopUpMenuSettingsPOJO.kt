package com.impaktsoft.globeup.models

class PopUpMenuSettingsPOJO {

    var menuId:Int?=null
    var anchorViewId:Int?=null
    var methods:HashMap<Int,()->Unit>?=null

    constructor(menuId: Int?, anchorViewId: Int?,methods:HashMap<Int,()->Unit>?) {
        this.menuId = menuId
        this.anchorViewId = anchorViewId
        this.methods=methods
    }
}