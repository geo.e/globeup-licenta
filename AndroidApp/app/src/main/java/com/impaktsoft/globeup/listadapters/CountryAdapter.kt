package com.impaktsoft.globeup.listadapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.impaktsoft.globeup.databinding.CountryCellBinding
import com.impaktsoft.globeup.models.ConversationPOJO
import com.impaktsoft.globeup.models.CountryPOJO
import com.impaktsoft.globeup.viewmodels.SelectCountryViewModel

class CountryAdapter(
    var countryList: ArrayList<CountryPOJO>
    , private var viewModel: SelectCountryViewModel? = null
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount(): Int {
        return countryList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)

        val applicationBinding =
            CountryCellBinding.inflate(layoutInflater, parent, false)
        return CountryViewHolder(applicationBinding)

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val countryPOJO = countryList[position];
        (holder as CountryViewHolder).bind(countryPOJO)
    }

    inner class CountryViewHolder(var applicationBinding: CountryCellBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(item: CountryPOJO) {
            applicationBinding.countryItem = item
            if (viewModel != null) {
                applicationBinding.countryCellLayout.setOnClickListener {
                    viewModel?.selectCountry(item)
                }
            }
        }
    }

    fun filterList(auxList: ArrayList<CountryPOJO>) {
        countryList = auxList
        notifyDataSetChanged()
    }
}