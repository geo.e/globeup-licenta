package com.impaktsoft.globeup.listadapters

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.impaktsoft.globeup.databinding.GlobalDonationGroupCellBinding
import com.impaktsoft.globeup.databinding.GlobalDonationUserCellBinding
import com.impaktsoft.globeup.databinding.GlobalSectionCellBinding
import com.impaktsoft.globeup.models.GlobalGroupPOJO
import com.impaktsoft.globeup.models.GlobalSectionPOJO
import com.impaktsoft.globeup.models.GlobalUserPOJO
import com.impaktsoft.globeup.models.IGlobalBaseItem

class GlobalAdapter(private val list: List<IGlobalBaseItem>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        private const val User = 0
        private const val Group = 1
        private const val Section=2
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun getItemViewType(position: Int): Int {
        val item = list[position]

        when (item) {
            is GlobalUserPOJO -> return User
            is GlobalGroupPOJO -> return Group
            is GlobalSectionPOJO -> return Section
        }

        throw IllegalArgumentException("Unsupported view type")
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)

        return when (viewType) {
            User -> {
                val applicationBinding =
                    GlobalDonationUserCellBinding.inflate(layoutInflater, parent, false)
                UserViewHolder(applicationBinding)
            }

            Group -> {
                val applicationBinding =
                    GlobalDonationGroupCellBinding.inflate(layoutInflater, parent, false)
                GroupViewHolder(applicationBinding)
            }

            Section->{
                val applicationBinding =
                    GlobalSectionCellBinding.inflate(layoutInflater, parent, false)
                SectionViewHolder(applicationBinding)
            }

            else -> throw IllegalArgumentException("Unsupported view type for view holder")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val myObj = list[position];

        when (holder) {
            is UserViewHolder -> holder.bind(myObj as GlobalUserPOJO)
            is GroupViewHolder -> holder.bind(myObj as GlobalGroupPOJO)
            is SectionViewHolder ->holder.bind(myObj as GlobalSectionPOJO)
        }
    }


    inner class UserViewHolder(var applicationBinding: GlobalDonationUserCellBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(item: GlobalUserPOJO) {
            applicationBinding.user = item
        }
    }

    inner class GroupViewHolder(var applicationBinding: GlobalDonationGroupCellBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(item: GlobalGroupPOJO) {

            applicationBinding.group = item
        }
    }

    inner class SectionViewHolder(var applicationBinding: GlobalSectionCellBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(item: GlobalSectionPOJO) {

            applicationBinding.globalSection = item

        }
    }
}