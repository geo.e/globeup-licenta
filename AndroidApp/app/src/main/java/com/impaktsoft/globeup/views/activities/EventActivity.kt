package com.impaktsoft.globeup.views.activities

import android.os.Bundle
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.databinding.ActivityEventBinding
import com.impaktsoft.globeup.viewmodels.EventDetailViewModel
import kotlinx.android.synthetic.main.activity_event.*

class EventActivity :
    BaseBoundActivity<EventDetailViewModel, ActivityEventBinding>(EventDetailViewModel::class),
    OnMapReadyCallback {

    override val layoutId: Int
        get() = R.layout.activity_event

    private var mapView: MapView? = null
    private val MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey"

    override fun setupDataBinding(binding: ActivityEventBinding) {
        binding.viewModel = mViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        var mapViewBundle: Bundle? = null

        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY)
        }

        mapView = event_map_location
        mapView?.onCreate(mapViewBundle)
        mapView?.getMapAsync(this)

    }

    private var mMap: GoogleMap? = null
    override fun onMapReady(p0: GoogleMap?) {
        mMap = p0
        val coords= LatLng(mViewModel.currentEventItem!!.location!!.latitude,
            mViewModel.currentEventItem!!.location!!.longitude
        )
        mMap?.addMarker(MarkerOptions().position(coords).title("Event position"))
        val cameraUpdate: CameraUpdate? =
            CameraUpdateFactory.newLatLngZoom(coords, 12f)

        mMap?.animateCamera(cameraUpdate)
    }

    override fun onResume() {
        super.onResume()
        mapView?.onResume()
    }

    override fun onStart() {
        super.onStart()
        mapView?.onStart()

    }

    override fun onStop() {
        super.onStop()
        mapView?.onStop()

    }

    override fun onPause() {
        super.onPause()
        mapView?.onPause()

    }

    override fun onDestroy() {
        super.onDestroy()
        mapView?.onDestroy()

    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

}
