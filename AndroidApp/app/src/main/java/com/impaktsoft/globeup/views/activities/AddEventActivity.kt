package com.impaktsoft.globeup.views.activities

import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.annotation.RequiresApi
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.components.MapViewWithPin
import com.impaktsoft.globeup.databinding.ActivityAddEventBinding
import com.impaktsoft.globeup.viewmodels.AddEventViewModel
import kotlinx.android.synthetic.main.activity_add_event.*
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


class AddEventActivity :
    BaseBoundActivity<AddEventViewModel, ActivityAddEventBinding>(AddEventViewModel::class) {
    override var activityTitleResourceId: Int? = R.string.title_add_event

    override val layoutId: Int = R.layout.activity_add_event

    private val MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey"
    var mapView: MapViewWithPin? = null

    override fun setupDataBinding(binding: ActivityAddEventBinding) {
        binding.viewModel = mViewModel
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var mapViewBundle: Bundle? = null

        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY)
        }


        mapView = map_with_ping
        mapView?.setMapBundle(mapViewBundle)
        mapView?.getMapAsync()
        lifecycle.addObserver(mapView!!)
        
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.exit_action_bar_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.settings_exit -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
