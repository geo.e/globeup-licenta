package com.impaktsoft.globeup.views.fragments

import android.annotation.SuppressLint
import android.graphics.Paint
import android.graphics.Rect
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.components.MapViewWithRadius
import com.impaktsoft.globeup.databinding.FragmentFilterEventsByRadiusBinding
import com.impaktsoft.globeup.listeners.ILocationIsReady
import com.impaktsoft.globeup.viewmodels.FilterEventsByRadiusViewModel
import kotlinx.android.synthetic.main.fragment_filter_events_by_radius.*


class FilterEventsByRadiusFragment :
    BaseBoundFragment<FilterEventsByRadiusViewModel, FragmentFilterEventsByRadiusBinding>(
        FilterEventsByRadiusViewModel::class
    ) {

    override var layoutId: Int = R.layout.fragment_filter_events_by_radius
    override val fragmentNameResourceID: Int = R.string.filter_events

    override fun setupDataBinding(binding: FragmentFilterEventsByRadiusBinding) {
        binding.viewModel = mViewModel
    }

    private val MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey"
    var mapView: MapViewWithRadius? = null

    @SuppressLint("ResourceType")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var mapViewBundle: Bundle? = null

        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY)
        }

        mapView = filter_events_by_radius_map
        mapView?.setMapBundle(mapViewBundle)
        mapView?.setLocationListener(object : ILocationIsReady {
            override fun locationIsReady() {
                //hide progras barr
                mViewModel.locationIsReady()
                mViewModel.coords.value=mapView?.coordonates

                //set observable
                try {
                    mViewModel.progress.observe(activity!!, Observer {

                        if (mapView != null) {
                            mapView?.setMapCircle(it)
                        }
                    })
                } catch (e: java.lang.Exception) {
                    Log.d("Error", "mapView crash in FilterFragment " + e.message)
                }

            }
        })

        setMapView()

    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        var mapViewBundle = outState.getBundle(MAP_VIEW_BUNDLE_KEY)
        if (mapViewBundle == null) {
            mapViewBundle = Bundle()
            outState.putBundle(MAP_VIEW_BUNDLE_KEY, mapViewBundle)
        }

        mapView!!.onSaveInstanceState(mapViewBundle)
    }

    private fun setMapView() {

        lifecycle.addObserver(mapView!!)

        mapView?.getMapAsync {
        }

    }

    override fun onDestroy() {
        try {
            lifecycle.removeObserver(mapView!!)
            super.onDestroy()

        } catch (e: Exception) {
            Log.d("Error", e.message.toString() + " err")
        }
        super.onDestroy()
    }
}
