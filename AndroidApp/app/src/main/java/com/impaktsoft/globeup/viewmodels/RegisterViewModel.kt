package com.impaktsoft.globeup.viewmodels

import android.Manifest
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.android.gms.tasks.Task
import com.impaktsoft.globeup.AppConfig
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.models.ConfigCountryModel
import com.impaktsoft.globeup.models.CountryPOJO
import com.impaktsoft.globeup.models.GReason
import com.impaktsoft.globeup.services.BackendResultWithPayload
import com.impaktsoft.globeup.services.PermissionStatus
import com.impaktsoft.globeup.services.SharedPreferencesService
import com.impaktsoft.globeup.services.User
import com.impaktsoft.globeup.util.ImageUtils
import com.impaktsoft.globeup.views.activities.ImageActivity
import com.impaktsoft.globeup.views.activities.LoginActivity
import com.impaktsoft.globeup.views.activities.MainActivity
import com.impaktsoft.globeup.views.fragments.SelectCountryFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import java.io.File

class RegisterViewModel : BaseViewModel() {

    var profileImageFilePath = MutableLiveData<String>()
    var name = MutableLiveData<String>()
    var email = MutableLiveData<String>()
    var password = MutableLiveData<String>()
    var passwordRepeat = MutableLiveData<String>()
    var charactersField = MutableLiveData<Boolean>()
    var alphaNumeric = MutableLiveData<Boolean>()
    var repeatedCorrect = MutableLiveData<Boolean>()

    private var isLoading: Boolean = false

    private var lastVMVisitedName: String? = null

    private var imageFile: File? = null

    override fun onResume() {

        val imageFile = dataExchangeService.get<File>(RegisterViewModel::class.qualifiedName!!)

        if (imageFile != null) {
            this.imageFile = imageFile
            profileImageFilePath.value = imageFile.absolutePath
        }

        super.onResume()
    }

    fun loginClicked() {
        if (isLoading) return
        navigation.navigateToActivity(LoginActivity::class.java, true)
    }

    fun registerClicked() {
        if (!isLoading) {
            viewModelScope.launch(Dispatchers.IO) {
                if (email.value != null &&
                    password.value != null

                ) {
                    showHud()

                    isLoading = true

                    var imageUrl: String? = null

                    if (imageFile != null) {
                        imageUrl = uploadImage(imageFile!!)
                    }

                    val isAnonymous = database.fireAuth.currentUser?.isAnonymous

                    val result = if (isAnonymous != null && isAnonymous)
                        backend.linkAnonymousWithCredentials(email.value!!, password.value!!)
                    else
                        backend.registerWithEmailAndPassword(email.value!!, password.value!!)

                    if (result!!.isSuccess) {
                        val country = sharedPreferencesService.readFromSharedPref<CountryPOJO>(
                            SelectCountryViewModel.currentCountryKey,CountryPOJO::class.java)

                        val user = result.payload
                        if (user != null) {
                            val userInfo = com.impaktsoft.globeup.models.UserInfo(
                                email.value,
                                name.value,
                                imageUrl,
                                mapService.getCountryCode(country?.countryName!!)
                            )

                            database.userInfoDao.store(user.uid, userInfo)
                            navigation.navigateToActivity(MainActivity::class.java, true)
                        } else {
                            dialogService.showSnackbar(R.string.err_unknown_error)
                        }
                    } else {
                        when (result.error?.reason) {
                            GReason.UsernameCollision ->
                                dialogService.showSnackbar(R.string.err_username_collision)

                            GReason.WeakPassword ->
                                dialogService.showSnackbar(R.string.err_weak_password)

                            GReason.EmailBadlyFormatted ->
                                dialogService.showSnackbar(R.string.err_email_badly_formatted)

                            else ->
                                dialogService.showSnackbar(R.string.err_unknown_error)
                        }
                    }
                    isLoading = false
                    hideHud()

                } else {
                    dialogService.showSnackbar(R.string.err_provide_email_password)
                    isLoading = false
                }
            }
        }
    }

    fun takePicture() {

        if (isLoading) return

        viewModelScope.launch(Dispatchers.Main) {
            val permissionsResult = permissionService.requestPermissionStatusAsync(
                listOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA
                )
            ).await()

            var grantedCounter = 0
            for (result in permissionsResult) {
                if (result.permissionStatus == PermissionStatus.Granted) {
                    grantedCounter++
                }
            }

            if (grantedCounter == permissionsResult.size) {

                lastVMVisitedName = ImageActivity::class.qualifiedName
                navigation.navigateToActivity(ImageActivity::class.java)

            } else {
                dialogService.showSnackbar(R.string.err_camera_permission_needed)
            }
        }
    }

    private suspend fun uploadImage(file: File): String? {
        ImageUtils.rotateImageIfNecessary(file.absolutePath)

        val simplifiedPathFile = File(AppConfig.ProfileImageFolder, file.name)
        val result = fileStoreService.uploadFile(simplifiedPathFile.absolutePath, file)
        if (result.error == null) {
            if (result.url != null) {
                return result.url
            }
        } else {
            Log.d("------->", "Failed, with exception:${result.error?.debugMessage}")
        }

        return null
    }

}