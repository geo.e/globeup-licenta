package com.impaktsoft.globeup.views.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.recyclerview.widget.GridLayoutManager
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.databinding.ActivityWithdrawBinding
import com.impaktsoft.globeup.viewmodels.WithdrawViewModel
import kotlinx.android.synthetic.main.activity_withdraw.*

class WithdrawActivity : BaseBoundActivity<WithdrawViewModel,ActivityWithdrawBinding>(WithdrawViewModel::class) {
    override val layoutId: Int= R.layout.activity_withdraw

    override var activityTitleResourceId: Int?=R.string.title_activity_withdraw
    override fun setupDataBinding(binding: ActivityWithdrawBinding) {
        binding.viewModel=mViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val layoutManager= GridLayoutManager(this,2)
        withdraw_method_list.layoutManager=layoutManager
        withdraw_method_list.adapter=mViewModel.adapter
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.exit_action_bar_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.settings_exit -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
