package com.impaktsoft.globeup.viewmodels

import android.Manifest
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.impaktsoft.globeup.AppConfig
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.models.ConfigCountryModel
import com.impaktsoft.globeup.models.CountryPOJO
import com.impaktsoft.globeup.models.GReason
import com.impaktsoft.globeup.models.UserInfo
import com.impaktsoft.globeup.services.PermissionStatus
import com.impaktsoft.globeup.util.ImageUtils
import com.impaktsoft.globeup.views.activities.ImageActivity
import com.impaktsoft.globeup.views.fragments.SelectCountryFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import java.io.File
import java.lang.Exception

class EditProfileViewModel : BaseViewModel() {

    var userImageUrl = MutableLiveData<String>()
    var userName = MutableLiveData<String>()
    var country = MutableLiveData<String>()
    var profileImageFilePath = MutableLiveData<String>()

    private var imageFile: File? = null
    private var lastVMVisitedName: String? = null
    private var saveButtonIsBlocked = false
    private var userInfo: UserInfo? = null

    init {
        viewModelScope.launch(Dispatchers.IO) {
            val user = backend.currentUser
            userInfo = database.userInfoDao.fetch(user!!.uid)
            if (userInfo != null) {
                withContext(Dispatchers.Main) {
                    userImageUrl.value = userInfo?.profileImage
                    userName.value = userInfo?.name
                    country.value = mapService.getCountryName(userInfo?.country!!)
                }
            }
        }
    }

    override fun onResume() {

        if (lastVMVisitedName.equals(SelectCountryViewModel::class.qualifiedName)) {

            val selectedCountry =
                dataExchangeService.get<CountryPOJO>(SelectCountryViewModel.countryKey)
                    ?.countryName
            if (selectedCountry != null) {
                country.value = selectedCountry
            }
        } else {

            val imageFile = dataExchangeService.get<File>(RegisterViewModel::class.qualifiedName!!)

            if (imageFile != null) {
                this.imageFile = imageFile
                profileImageFilePath.value = imageFile.absolutePath
            }
        }

        super.onResume()
    }

    fun takePicture() {
        viewModelScope.launch(Dispatchers.Main) {
            val permissionsResult = permissionService.requestPermissionStatusAsync(
                listOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA
                )
            ).await()

            var grantedCounter = 0
            for (result in permissionsResult) {
                if (result.permissionStatus == PermissionStatus.Granted) {
                    grantedCounter++
                }
            }

            if (grantedCounter == permissionsResult.size) {

                lastVMVisitedName = ImageActivity::class.qualifiedName
                navigation.navigateToActivity(ImageActivity::class.java)

            } else {
                dialogService.showSnackbar(R.string.err_camera_permission_needed)
            }
        }
    }

    private suspend fun uploadImage(file: File): String? {
        ImageUtils.rotateImageIfNecessary(file.absolutePath)

        val simplifiedPathFile = File(AppConfig.ProfileImageFolder, file.name)
        val result = fileStoreService.uploadFile(simplifiedPathFile.absolutePath, file)
        if (result.error == null) {
            if (result.url != null) {
                return result.url
            }
        } else {
            Log.d("------->", "Failed, with exception:${result.error?.debugMessage}")
        }

        return null
    }


    fun chooseCountry() {

        val configCountry = ConfigCountryModel(resourceService.stringForId(R.string.save), false)
        lastVMVisitedName = SelectCountryViewModel::class.qualifiedName!!
        dataExchangeService.put(
            SelectCountryViewModel::class.qualifiedName!!,
            configCountry
        )
        dialogService.showDialog(SelectCountryFragment::class)
    }

    fun saveProfileChangesClick() {
        if (!saveButtonIsBlocked) {
            saveButtonIsBlocked = true
            if (userName.value.isNullOrEmpty()) {
                dialogService.showSnackbar(R.string.err_provide_username)
                saveButtonIsBlocked = false
                return
            }

            viewModelScope.launch(Dispatchers.IO) {
                showHud()

                var imageUrl: String? = ""

                imageUrl = if (imageFile != null) {
                    uploadImage(imageFile!!)
                } else {
                    userInfo?.profileImage
                }

                val newUserInfo = UserInfo(
                    userInfo?.email,
                    userName.value,
                    imageUrl,
                    mapService.getCountryCode(country.value!!)
                )

                database.userInfoDao.store(backend.currentUser!!.uid, newUserInfo)

                withContext(Dispatchers.Main)
                {
                    saveButtonIsBlocked = false
                    hideHud()
                    navigation.closeCurrentActivity()
                }
            }
        }
    }
}
