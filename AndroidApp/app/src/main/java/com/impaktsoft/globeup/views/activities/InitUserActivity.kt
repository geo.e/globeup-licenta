package com.impaktsoft.globeup.views.activities

import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.databinding.ActivityInitUserBinding
import com.impaktsoft.globeup.viewmodels.InitUserViewModel

class InitUserActivity : BaseBoundActivity<InitUserViewModel, ActivityInitUserBinding>(
    InitUserViewModel::class
) {

    override val layoutId: Int
        get() = R.layout.activity_init_user

    override fun setupDataBinding(binding: ActivityInitUserBinding) {
        binding.viewModel = mViewModel
    }
}
