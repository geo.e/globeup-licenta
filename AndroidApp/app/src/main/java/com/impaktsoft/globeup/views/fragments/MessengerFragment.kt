package com.impaktsoft.globeup.views.fragments

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.components.MyCustomMessengerAppBarView
import com.impaktsoft.globeup.databinding.FragmentMessengerBinding
import com.impaktsoft.globeup.listeners.IClickListener
import com.impaktsoft.globeup.models.ConversationPOJO
import com.impaktsoft.globeup.viewmodels.MessengerViewModel
import kotlinx.android.synthetic.main.fragment_messenger.*


class MessengerFragment :
    BaseBoundFragment<MessengerViewModel, FragmentMessengerBinding>(MessengerViewModel::class) {

    override var layoutId: Int = R.layout.fragment_messenger
    override val fragmentNameResourceID: Int = R.string.title_messages


    override fun setupDataBinding(binding: FragmentMessengerBinding) {
        binding.viewModel = mViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setActionBar(mViewModel.conversationItem!!)

        val layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        messages_list.layoutManager = layoutManager
        messages_list.adapter = mViewModel.adapter

    }

    private fun setActionBar(conversationItem: ConversationPOJO) {
        val myAppBar =
            (activity as AppCompatActivity).supportActionBar!!.customView as MyCustomMessengerAppBarView

        myAppBar.setName(conversationItem.name)
        myAppBar.setProfileImage(conversationItem.conversationImage!!)
        myAppBar.setBackClick(object : IClickListener {
            override fun clicked() {
                activity?.finish()
            }
        })
    }
}
