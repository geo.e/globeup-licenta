package com.impaktsoft.globeup.services

import android.text.BoringLayout
import com.google.firebase.auth.*
import com.impaktsoft.globeup.models.GError
import com.impaktsoft.globeup.models.GReason
import kotlinx.coroutines.tasks.await

interface IBackendService {
    val currentUser: User?
    fun isSignedIn(): Boolean
    fun isSignedInAnonymously(): Boolean
    suspend fun signInAnonymously(): BackendResult
    suspend fun linkAnonymousWithCredentials(
        email: String,
        password: String,
        sendEmailVerification: Boolean = true
    ): BackendResultWithPayload<User>

    suspend fun registerWithEmailAndPassword(
        email: String,
        password: String
    ): BackendResultWithPayload<User>?

    suspend fun logoutCurrentUser()
    suspend fun signIn(email: String, password: String): BackendResultWithPayload<User>

    suspend fun resetPassword(email: String): BackendResultWithPayload<User>
}

class BackendService(
    private val connectivityService: IConnectivityService
) : IBackendService {

    private val auth: FirebaseAuth by lazy { FirebaseAuth.getInstance() }
    override val currentUser: User?
        get() {
            val currentUser = auth.currentUser

            if (currentUser != null) {
                return User(currentUser.uid, currentUser.email, currentUser.isAnonymous)
            }

            return null
        }

    override fun isSignedIn(): Boolean {
        return auth.currentUser != null
    }

    override fun isSignedInAnonymously(): Boolean {
        val currentUser = auth.currentUser

        if (currentUser != null) {
            return currentUser.isAnonymous
        }

        return false
    }

    override suspend fun signInAnonymously(): BackendResult {
        var authResult: AuthResult?

        try {
            if (connectivityService.isInternetAvailable()) {
                authResult = auth.signInAnonymously().await()

            } else {
                return BackendResult(
                    GError(
                        GReason.NoConnection,
                        "no internet connection available at the moment. retry later."
                    )
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
            return if (e.message != null) {
                BackendResult(GError(GReason.Exception, e.message!!))
            } else {
                BackendResult(
                    GError(
                        GReason.Exception,
                        "Unknown Exception"
                    )
                )
            }
        }

        return if (authResult != null) {
            val currentUser = authResult.user

            if (currentUser != null) {
                BackendResult()
            } else {
                BackendResult(
                    GError(
                        GReason.NullValue,
                        "Current user after sign in continues to be null"
                    )
                )
            }
        } else {
            BackendResult(
                GError(
                    GReason.NullValue,
                    "Server response returned null"
                )
            )
        }
    }

    override suspend fun linkAnonymousWithCredentials(
        email: String,
        password: String,
        sendEmailVerification: Boolean
    ): BackendResultWithPayload<User> {
        val currentUser = auth.currentUser

        if (currentUser != null) {
            if (currentUser.isAnonymous) {
                var result: AuthResult?

                try {
                    if (connectivityService.isInternetAvailable()) {
                        val credential = EmailAuthProvider.getCredential(email, password)
                        result = currentUser.linkWithCredential(credential).await()

                    } else {
                        return BackendResultWithPayload(
                            null,
                            GError(
                                GReason.NoConnection,
                                "no internet connection available at the moment. retry later."
                            )
                        )
                    }
                } catch (e: Exception) {
                    return when (e) {
                        is FirebaseAuthUserCollisionException -> BackendResultWithPayload(
                            null,
                            GError(
                                GReason.UsernameCollision,
                                "account with this email already exists, choose other account"
                            )
                        )
                        is FirebaseAuthWeakPasswordException -> BackendResultWithPayload(
                            null,
                            GError(
                                GReason.WeakPassword,
                                "The given password is invalid. [ Password should be at least 6 characters ]"
                            )
                        )
                        is FirebaseAuthInvalidCredentialsException -> BackendResultWithPayload(
                            null,
                            GError(
                                GReason.EmailBadlyFormatted,
                                "The given password is invalid. [ Password should be at least 6 characters ]"
                            )
                        )
                        else -> {
                            e.printStackTrace()
                            return if (e.message != null) {
                                BackendResultWithPayload(
                                    null,
                                    GError(
                                        GReason.Exception,
                                        e.message!!
                                    )
                                )
                            } else {
                                BackendResultWithPayload(
                                    null,
                                    GError(
                                        GReason.Exception,
                                        "Unknown Exception"
                                    )
                                )
                            }
                        }
                    }
                }

                return if (result != null) {
                    val user = result.user
                    if (user != null) {
                        if (!user.isAnonymous) {
                            if (sendEmailVerification) {
                                user.sendEmailVerification()
                            }

                            BackendResultWithPayload(User(user.uid, user.email, user.isAnonymous))
                        } else {
                            BackendResultWithPayload<User>(
                                null,
                                GError(
                                    GReason.Unknown,
                                    "User is still anonymous. This should never happen"
                                )
                            )
                        }
                    } else {
                        BackendResultWithPayload<User>(
                            null,
                            GError(
                                GReason.NullValue,
                                "User returned null."
                            )
                        )
                    }
                } else {
                    BackendResultWithPayload<User>(
                        null,
                        GError(
                            GReason.NullValue,
                            "result was null, request failed"
                        )
                    )
                }
            } else {
                throw RuntimeException("Only anonymous users can be linked with credentials. Please check your code")
            }
        } else {
            throw RuntimeException("At this point we should already be logged in with an anonymous user. Please check your code.")
        }
    }

    override suspend fun registerWithEmailAndPassword(
        email: String,
        password: String
    ): BackendResultWithPayload<User>? {
        if (connectivityService.isInternetAvailable()) {
            val result = auth.createUserWithEmailAndPassword(email, password).await()
            val user = result.user

            if (user != null) {
                return BackendResultWithPayload(User(user!!.uid, user.email, user.isAnonymous))

            }
        }
        return null;
    }

    override suspend fun signIn(email: String, password: String): BackendResultWithPayload<User> {
        var result: AuthResult?

        try {
            if (connectivityService.isInternetAvailable()) {
                result = auth.signInWithEmailAndPassword(email, password).await()

                return if (result != null) {
                    val user = result.user
                    if (user != null) {
                        if (!user.isAnonymous) {
                            BackendResultWithPayload(User(user.uid, email, user.isAnonymous))
                        } else {
                            BackendResultWithPayload<User>(
                                null,
                                GError(
                                    GReason.Unknown,
                                    "User is anonymous. This should never happen"
                                )
                            )
                        }
                    } else {
                        BackendResultWithPayload<User>(
                            null,
                            GError(
                                GReason.NullValue,
                                "User returned null."
                            )
                        )
                    }
                } else {
                    BackendResultWithPayload<User>(
                        null,
                        GError(
                            GReason.NullValue,
                            "result was null, request failed"
                        )
                    )
                }
            } else {
                return BackendResultWithPayload<User>(
                    null,
                    GError(
                        GReason.NoConnection,
                        "no internet connection available at the moment. retry later."
                    )
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
            return if (e.message != null) {
                BackendResultWithPayload<User>(
                    null,
                    GError(GReason.Exception, e.message!!)
                )
            } else {
                BackendResultWithPayload<User>(
                    null,
                    GError(
                        GReason.Exception,
                        "Unknown Exception"
                    )
                )
            }
        }
    }

    override suspend fun resetPassword(email: String): BackendResultWithPayload<User> {

        try {
            auth.sendPasswordResetEmail(email)
                .await()
        } catch (e: java.lang.Exception) {
            if (e.message != null) {
                return BackendResultWithPayload<User>(
                    null,
                    GError(GReason.Exception, e.message!!)
                )
            }
        }
        val result =  BackendResultWithPayload<User>(null,null)
        result.isSuccess = true
        return result
    }


    override suspend fun logoutCurrentUser() {
        val user = auth.currentUser

        if (user != null && !user.isAnonymous) {
            auth.signOut()

        } else {
            throw RuntimeException("User was null or anonymous. Can not log out.")
        }
    }
}

open class BackendResult(val error: GError? = null) {
    open var isSuccess = (error == null)
}

class BackendResultWithPayload<T>(val payload: T?, error: GError? = null) : BackendResult(error)

data class User(val uid: String, val email: String?, val isAnonymous: Boolean?)



