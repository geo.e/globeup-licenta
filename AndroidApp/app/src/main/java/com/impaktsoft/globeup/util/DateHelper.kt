package com.impaktsoft.globeup.util

import android.util.Log
import com.impaktsoft.globeup.enums.EventTimeFilterEnum
import com.impaktsoft.globeup.models.EventPOJO
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class DateHelper {
    companion object {

        fun getOffsetTimeInMillis(): Long {
            val mCalendar: Calendar = Calendar.getInstance()
            val mTimeZone = mCalendar.timeZone
            val mGMTOffset = mTimeZone.getOffset(mCalendar.timeInMillis)
            return TimeUnit.MILLISECONDS.convert(mGMTOffset.toLong(), TimeUnit.MILLISECONDS)
        }

        fun getDateString(timeInMillis: Long): String {
            val simpleDateFormat = SimpleDateFormat("EE MMM dd HH:mm:ss yyyy")
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = timeInMillis;
            return simpleDateFormat.format(calendar.time);
        }

        fun getDateHour(date: Date):String
        {
            val sdf = SimpleDateFormat("HH:mm")
            return sdf.format(date)
        }

        fun getDateFormatForSections(date:Date):String
        {
            val sdf = SimpleDateFormat("dd.MMM.yyyy")
            return sdf.format(date)
        }
        fun eventWillHappendAtTime(
            eventPOJO: EventPOJO,
            eventTimeFilterEnum: EventTimeFilterEnum
        ): Boolean {


            val timeZone = TimeZone.getTimeZone("UTC")
            val eventCalendar = Calendar.getInstance(timeZone)
            eventCalendar.timeInMillis = eventPOJO.eventDate!!

            val eventDate = eventCalendar.time

            val deadlineCalendar = Calendar.getInstance(timeZone)

            val currentTimeCalendar = Calendar.getInstance()
            currentTimeCalendar.timeInMillis = System.currentTimeMillis()
            currentTimeCalendar.timeInMillis -= getOffsetTimeInMillis()

            when (eventTimeFilterEnum) {
                EventTimeFilterEnum.TODAY -> {

                    deadlineCalendar.set(Calendar.HOUR_OF_DAY, 23)
                    deadlineCalendar.set(Calendar.MINUTE, 59)
                    deadlineCalendar.set(Calendar.SECOND, 59)
                    deadlineCalendar.timeInMillis -= getOffsetTimeInMillis()

                    return (eventDate.before(deadlineCalendar.time)
                            && eventDate.after(currentTimeCalendar.time))
                }

                EventTimeFilterEnum.THIS_WEEK -> {

                    val weekOfYear = deadlineCalendar.get(Calendar.WEEK_OF_YEAR) + 1

                    deadlineCalendar.set(
                        Calendar.WEEK_OF_YEAR,
                        weekOfYear
                    )
                    deadlineCalendar.set(Calendar.HOUR_OF_DAY, 23)
                    deadlineCalendar.set(Calendar.MINUTE, 59)
                    deadlineCalendar.set(Calendar.SECOND, 59)

                    deadlineCalendar.timeInMillis -= getOffsetTimeInMillis()

                    return (eventDate.after(currentTimeCalendar.time) && eventDate.before(
                        deadlineCalendar.time
                    ))

                }
                EventTimeFilterEnum.NEXT_WEEK -> {

                    val currentWeek = Calendar.getInstance().get(Calendar.WEEK_OF_YEAR)
                    deadlineCalendar.set(
                        Calendar.WEEK_OF_YEAR,
                        currentWeek + 2
                    )
                    deadlineCalendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY)
                    deadlineCalendar.set(Calendar.HOUR_OF_DAY, 0)
                    deadlineCalendar.set(Calendar.MINUTE, 0)
                    deadlineCalendar.set(Calendar.SECOND, 0)
                    deadlineCalendar.timeInMillis -= getOffsetTimeInMillis()

                    val nextWeekCalendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
                    nextWeekCalendar.set(
                        Calendar.WEEK_OF_YEAR,
                        currentWeek + 1
                    )
                    nextWeekCalendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY)
                    nextWeekCalendar.set(Calendar.HOUR_OF_DAY, 0)
                    nextWeekCalendar.set(Calendar.MINUTE, 0)
                    nextWeekCalendar.set(Calendar.SECOND, 0)
                    nextWeekCalendar.timeInMillis -= getOffsetTimeInMillis()

                    return (eventDate.before(deadlineCalendar.time)
                            && eventDate.after(nextWeekCalendar.time))

                }
                EventTimeFilterEnum.THIS_MONTH -> {

                    deadlineCalendar.set(Calendar.MONTH, deadlineCalendar.get(Calendar.MONTH) + 1)
                    deadlineCalendar.set(Calendar.DAY_OF_MONTH, 1)
                    deadlineCalendar.set(Calendar.HOUR_OF_DAY, 0)
                    deadlineCalendar.set(Calendar.MINUTE, 0)
                    deadlineCalendar.set(Calendar.SECOND, 0)
                    deadlineCalendar.timeInMillis -= getOffsetTimeInMillis()

                    return (eventDate.after(currentTimeCalendar.time) && eventDate.before(
                        deadlineCalendar.time
                    ))
                }

                EventTimeFilterEnum.NEXT_MONTH -> {
                    deadlineCalendar.set(Calendar.MONTH, deadlineCalendar.get(Calendar.MONTH) + 1)
                    deadlineCalendar.set(Calendar.DAY_OF_MONTH, 1)
                    deadlineCalendar.set(Calendar.HOUR_OF_DAY, 0)
                    deadlineCalendar.set(Calendar.MINUTE, 0)
                    deadlineCalendar.set(Calendar.SECOND, 0)
                    deadlineCalendar.timeInMillis -= getOffsetTimeInMillis()

                    return (eventDate.after(deadlineCalendar.time))
                }
                else -> return (eventDate.before(deadlineCalendar.time))
            }

        }
    }
}