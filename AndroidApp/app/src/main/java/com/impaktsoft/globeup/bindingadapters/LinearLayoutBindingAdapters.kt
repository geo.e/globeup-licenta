package com.impaktsoft.globeup.bindingadapters

import android.annotation.TargetApi
import android.os.Build
import android.widget.LinearLayout
import androidx.databinding.BindingAdapter
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.models.GCoinPOJO
import com.impaktsoft.globeup.models.WithdrawPOJO

object LinearLayoutBindingAdapters {
    @TargetApi(Build.VERSION_CODES.M)
    @BindingAdapter(value = ["cb_background"])
    @JvmStatic
    fun LinearLayout.setBackGround(gCoinPOJO: GCoinPOJO) {
        if(gCoinPOJO.selected)
        {
            this.background=context!!.getDrawable(R.drawable.custom_blue_round_background)

        }
        else
        {
            this.background=context!!.getDrawable(R.drawable.custom_layout_border)
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @BindingAdapter(value = ["cb_background"])
    @JvmStatic
    fun LinearLayout.setBackGround(withdrawPOJO: WithdrawPOJO) {
        if(withdrawPOJO.selected)
        {
            this.background=context!!.getDrawable(R.drawable.custom_blue_round_background)

        }
        else
        {
            this.background=context!!.getDrawable(R.drawable.custom_layout_border)
        }
    }
}