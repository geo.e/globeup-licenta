package com.impaktsoft.globeup.models

class PendingMessagePOJO {
    var conversationKey:String?=null
    var messagePOJO:MessagePOJO?=null
    var senderKey:String?=null

    constructor(conversationKey: String?, messagePOJO: MessagePOJO?,senderKey:String) {
        this.conversationKey = conversationKey
        this.messagePOJO = messagePOJO
        this.senderKey=senderKey
    }

    constructor()
}