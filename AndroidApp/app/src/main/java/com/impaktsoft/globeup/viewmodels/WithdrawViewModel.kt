package com.impaktsoft.globeup.viewmodels

import androidx.lifecycle.MutableLiveData
import com.impaktsoft.globeup.dummy.WithdrawDummyData
import com.impaktsoft.globeup.listadapters.WithdrawAdapter
import com.impaktsoft.globeup.models.WithdrawPOJO

class WithdrawViewModel : BaseViewModel() {
    var currentGcoins = "1.2345Gcoins"

    private var gcoinValue = 0.01
    var gcoinValueString: String

    var withdraw_value = MutableLiveData<Double>()
    var total_value_string = MutableLiveData<String>()

    var myListOfWithdraws = WithdrawDummyData.listOfWithdraws

    var selectedWithdraw: WithdrawPOJO? = null
    var adapter = WithdrawAdapter(myListOfWithdraws, this)

    init {
        withdraw_value.value = 0.0
        gcoinValueString = "$ " + gcoinValue.toString() + "per Gcoin"
        total_value_string.value = "$ ${withdraw_value.value}"
    }

    fun setWithdrawValue(value: Int) {
        withdraw_value.value = value * gcoinValue
        total_value_string.value = "$ ${withdraw_value.value}"
    }

    fun selectPayMethod(position: Int) {
        for (i in 0 until myListOfWithdraws.size) {
            if (i == position && !myListOfWithdraws[i].selected) {
                myListOfWithdraws[i].selected = true
                selectedWithdraw = myListOfWithdraws[i]
                adapter!!.notifyItemChanged(i)

            } else if (i != position && myListOfWithdraws[i].selected) {
                myListOfWithdraws[i].selected = false
                adapter!!.notifyItemChanged(i)
            }
        }
    }

    fun withdrawClick() {
        //TODO complete the logic
    }
}