package com.impaktsoft.globeup.dummy

import com.impaktsoft.globeup.models.ExperiencePOJO

class ExperienceDummyData {
    companion object {
        private val subList1: ArrayList<ExperiencePOJO> = arrayListOf(
            ExperiencePOJO("Start and finish an event", "")
            , ExperiencePOJO("Post result and group photo on facebook", "50exp")
            , ExperiencePOJO("Attend an event around you", "10exp")


        )
        private val subList2: ArrayList<ExperiencePOJO> = arrayListOf(
            ExperiencePOJO("Invite friends to app", "1friend=50exp")
        )
        private val subList3: ArrayList<ExperiencePOJO> = arrayListOf(
            ExperiencePOJO("upgrade the anonymous account", "50exp")
        )

        val experienceListPOJO = arrayListOf(
            subList1, subList2, subList3
        )
    }
}