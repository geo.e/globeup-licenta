package com.impaktsoft.globeup.models

import com.impaktsoft.globeup.enums.EventState

class EventDetailPOJO {
    var eventPOJO: EventPOJO? = null
    var eventState: EventState? = EventState.UNKNOWN;
    var userEventList = ArrayList<UserEventPOJO>()

    constructor(eventPOJO: EventPOJO?,userEventList:ArrayList<UserEventPOJO>,eventState: EventState? = null) {
        this.eventPOJO = eventPOJO
        this.userEventList=userEventList
        if (eventState != null) this.eventState = eventState
    }
}