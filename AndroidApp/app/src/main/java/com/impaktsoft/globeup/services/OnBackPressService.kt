package com.impaktsoft.globeup.services

import com.impaktsoft.globeup.listeners.IOnBackPressedListener

interface IOnBackPressService
{
    fun initOnBackPressListener(listener : IOnBackPressedListener)
    fun onBackPressed()
}
class OnBackPressService : IOnBackPressService
{
    private var onBackPressListener:IOnBackPressedListener?=null

    override fun initOnBackPressListener(listener: IOnBackPressedListener) {
        onBackPressListener=listener
    }

    override fun onBackPressed() {
        onBackPressListener?.backPressHasBeenPressed();
    }

}