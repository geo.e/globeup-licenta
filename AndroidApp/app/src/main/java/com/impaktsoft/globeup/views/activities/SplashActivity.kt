package com.impaktsoft.globeup.views.activities

import android.animation.Animator
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import com.airbnb.lottie.LottieAnimationView
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.viewmodels.SplashViewModel


class SplashActivity : BaseActivity<SplashViewModel>(SplashViewModel::class) {

    override fun onCreate(savedInstanceState: Bundle?) {

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        setContentView(R.layout.activity_splash)
        initUI()

        super.onCreate(savedInstanceState)

    }

    fun initUI() {
        val logoAnimation = findViewById<LottieAnimationView>(R.id.animation_view);
        logoAnimation.addAnimatorListener(object : Animator.AnimatorListener {
            override fun onAnimationEnd(p0: Animator?) {
                mViewModel.loadNextScreen()
            }

            override fun onAnimationRepeat(p0: Animator?) {}
            override fun onAnimationCancel(p0: Animator?) {}
            override fun onAnimationStart(p0: Animator?) {}
        })
    }
}
