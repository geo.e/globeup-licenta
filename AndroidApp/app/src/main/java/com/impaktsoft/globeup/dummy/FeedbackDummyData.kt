package com.impaktsoft.globeup.dummy

import com.impaktsoft.globeup.models.FeedbackPOJO

class FeedbackDummyData {
    companion object {
        val feedbacks: List<FeedbackPOJO> = listOf(
            FeedbackPOJO("Slow loading",false),
            FeedbackPOJO("Consumer service",false),
            FeedbackPOJO("App Crash",false),
            FeedbackPOJO("Navigation",false),
            FeedbackPOJO("Too hard",false),
            FeedbackPOJO("Not functional",false)
        )
    }
}