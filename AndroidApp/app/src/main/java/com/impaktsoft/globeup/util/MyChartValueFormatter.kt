package com.impaktsoft.globeup.util

import android.util.Log
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.formatter.ValueFormatter
import java.text.DateFormatSymbols
import java.util.*
import com.github.mikephil.charting.utils.ViewPortHandler


class MyChartValueFormatter(val data: ArrayList<Entry>) :
    ValueFormatter() {

    var t = 0

    override fun getFormattedValue(value: Float): String {

        Log.d(
            "myTag2",
            t.toString() + " " + getMonthForInt(Date(data[t % data.size].x.toLong()).month.toInt()) + " " + t
        )
        t++

        if(t==data.size)t=0

        return getMonthForInt(Date(data[t % data.size].x.toLong()).month)


    }


    private fun getMonthForInt(num: Int): String {
        var month = "wrong"
        val dfs = DateFormatSymbols()
        val months = dfs.months
        if (num in 0..11) {
            month = months[num]
        }

        return month.substring(0, 3)

    }
}