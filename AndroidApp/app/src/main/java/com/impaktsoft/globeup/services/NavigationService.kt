package com.impaktsoft.globeup.services

import android.app.Activity
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK
import android.view.View
import androidx.core.app.ActivityOptionsCompat
import androidx.core.app.ActivityOptionsCompat.makeSceneTransitionAnimation
import androidx.core.content.IntentCompat
import androidx.core.util.Pair
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.impaktsoft.globeup.views.activities.ContainerActivity
import kotlin.reflect.KClass


interface INavigationService {

    fun <T : Activity> navigateToActivity(
        activityClass: Class<T>,
        finishCurrentActivity: Boolean = false,
        clearBackStack: Boolean = false,
        parameterName: String? = null,
        parameterValue: String? = null,
        transitionPairs: Array<Pair<View, String>>? = null
    )

    fun closeCurrentActivity()

    fun navigateToFragment(transitionId: Int, navHostId: Int)

    fun <T : Fragment> navigateToFragment(fragmentClass: KClass<T>, finishCurrentActivity: Boolean)

    fun popFragmentBackStack(navHostId: Int)

    fun closeCurrentActivityAfterTransition()

}


class NavigationService(private val currentActivityService: ICurrentActivityService) :
    INavigationService {

    override fun <T : Activity> navigateToActivity(
        activityClass: Class<T>,
        finishCurrentActivity: Boolean,
        clearBackStack: Boolean,
        parameterName: String?,
        parameterValue: String?,
        transitionPairs: Array<Pair<View, String>>?

    ) {

        val activity: Activity? = currentActivityService.activity

        if (activity != null) {
            val intent = Intent(activity, activityClass)

            var activityOptions: ActivityOptionsCompat? = null
            if (transitionPairs != null) {
                activityOptions = makeSceneTransitionAnimation(activity, *transitionPairs)
            }


            if (parameterName != null &&
                parameterValue != null
            ) {
                intent.putExtra(parameterName, parameterValue)
            }

            if (clearBackStack) {
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            }

            activity.runOnUiThread(
                Runnable {
                    if (activityOptions != null) {
                        activity.startActivity(intent, activityOptions.toBundle())
                    } else {
                        activity.startActivity(intent)
                    }

                    if (finishCurrentActivity) {
                        activity.finish()
                    }
                }
            )
        } else {
            throw Exception("Current activity was null. Please setup ICurrentActivityService correctly.")
        }
    }

    override fun closeCurrentActivity() {
        val activity: Activity? = currentActivityService.activity

        if (activity != null) {
            activity.finish()
        } else {
            throw Exception("Current activity was null. Please setup ICurrentActivityService correctly.")
        }
    }

    override fun navigateToFragment(transitionId: Int, navHostId: Int) {
        val activity: Activity? = currentActivityService.activity

        if (activity != null) {
            var navController = activity.findNavController(navHostId)
            navController.navigate(transitionId)
        } else {
            throw Exception("Current activity was null. Please setup ICurrentActivityService correctly.")
        }
    }

    override fun <T : Fragment> navigateToFragment(
        fragmentClass: KClass<T>,
        finishCurrentActivity: Boolean
    ) {
        navigateToActivity(
            ContainerActivity::class.java,
            finishCurrentActivity,
            false,
            ContainerActivity.fragmentClassNameParam,
            fragmentClass.qualifiedName
        )
    }

    override fun popFragmentBackStack(navHostId: Int) {
        val activity: Activity? = currentActivityService.activity

        if (activity != null) {
            var navController = activity.findNavController(navHostId)
            navController.popBackStack()
        } else {
            throw Exception("Current activity was null. Please setup ICurrentActivityService correctly.")
        }
    }

    override fun closeCurrentActivityAfterTransition() {
        val activity: Activity? = currentActivityService.activity
        if (activity != null) {
            activity.finishAfterTransition()
        } else {
            throw Exception("Current activity was null. Please setup ICurrentActivityService correctly.")
        }
    }
}