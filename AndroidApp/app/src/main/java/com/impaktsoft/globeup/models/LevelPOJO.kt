package com.impaktsoft.globeup.models

import com.impaktsoft.globeup.enums.LevelType

class LevelPOJO {
    var levelType:LevelType?=null
    var levelName:String?=null
    var levelExp:String?=null
    var levelDescription:String?=null

    constructor(levelType:LevelType?,levelName: String?, levelExp: String?,levelDescription: String?=null) {
        this.levelType=levelType
        this.levelName = levelName
        this.levelExp = levelExp
        this.levelDescription = levelDescription
    }

    fun hasDescription():Boolean
    {
        return levelDescription.isNullOrEmpty()
    }
}