package com.impaktsoft.globeup.listadapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.impaktsoft.globeup.databinding.ConversationCellBinding
import com.impaktsoft.globeup.models.ConversationPOJO
import com.impaktsoft.globeup.viewmodels.MessagesBoundViewModel

class ConversationsAdapter(
    private var arrayList: ArrayList<ConversationPOJO>
    , private var boundViewModel: MessagesBoundViewModel? = null
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)

        val applicationBinding =
            ConversationCellBinding.inflate(layoutInflater, parent, false)
        return ConversationViewHolder(applicationBinding)

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val conversationPOJO = arrayList[position];
        (holder as ConversationViewHolder).bind(conversationPOJO)
    }

    inner class ConversationViewHolder(var applicationBinding: ConversationCellBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(item: ConversationPOJO) {
            applicationBinding.isMessageSeen=boundViewModel!!.messagesSeenByUser[item.key]
            applicationBinding.itemConversation = item
            if (boundViewModel != null) {
                applicationBinding.conversationCellLayout.setOnClickListener {
                    boundViewModel?.conversationClick(item)
                }
            }
        }
    }

    fun filterList(auxList: ArrayList<ConversationPOJO>) {
        arrayList = auxList
        notifyDataSetChanged()
    }

}