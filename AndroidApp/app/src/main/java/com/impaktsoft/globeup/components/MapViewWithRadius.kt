package com.impaktsoft.globeup.components

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.Context
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMapOptions
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.impaktsoft.globeup.listeners.ILocationIsReady


class MapViewWithRadius : MapView, OnMapReadyCallback, LifecycleObserver {

    private var mMap: MapWithRadius? = null
    private var locationManager: LocationManager? = null
    private var mMapReadyCallback: OnMapReadyCallback? = null
    var locationReady: ILocationIsReady? = null
    private var mapBundle:Bundle?=null
    var coordonates:LatLng?=null

    constructor(var1: Context) : super(var1)
    constructor(var1: Context, var2: AttributeSet) : super(var1, var2)
    constructor(var1: Context, var2: AttributeSet, var3: Int) : super(var1, var2, var3)
    constructor(var1: Context, var2: GoogleMapOptions) : super(var1, var2)

    override fun getMapAsync(callback: OnMapReadyCallback) {
        mMapReadyCallback = callback
        super.getMapAsync(this)
    }

    override fun onMapReady(p0: GoogleMap?) {
        if (p0 != null) {
            mMap = MapWithRadius(p0, context)

            locationManager =
                context?.getSystemService(Context.LOCATION_SERVICE) as LocationManager

            updateMyLocation()
        }
    }

    fun setLocationListener(locationListener: ILocationIsReady) {
        locationReady = locationListener
    }

    @SuppressLint("MissingPermission")
    fun updateMyLocation() {
        val locationListener = object : LocationListener {
            @TargetApi(Build.VERSION_CODES.M)
            override fun onLocationChanged(p0: Location?) {

                coordonates = LatLng(p0!!.latitude, p0.longitude)

                mMap!!.setPinAndCameraPosition(coordonates!!)
                mMap!!.createCircleRadius(coordonates!!)

                locationManager!!.removeUpdates(this)
                //now map is ready to show current position
                locationReady?.locationIsReady()
            }

            override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {
            }

            override fun onProviderEnabled(p0: String?) {
            }

            override fun onProviderDisabled(p0: String?) {
            }
        }

        locationManager!!.requestLocationUpdates(
            LocationManager.GPS_PROVIDER, 0,
            0F, locationListener
        );
        locationManager!!.requestLocationUpdates(
            LocationManager.NETWORK_PROVIDER,
            0,
            0f,
            locationListener
        )

    }

    fun setMapCircle(radius: Int) {
        if (mMap != null) {
            mMap!!.setCircleRadius(radius)
        }
    }

    fun setMapBundle(bundle: Bundle?)
    {
        mapBundle=bundle
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        parent.requestDisallowInterceptTouchEvent(true)
        return super.dispatchTouchEvent(ev)
    }

    //oncreate/dc bundle

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun lifecycleOnCreate() {
        super.onCreate(mapBundle)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun lifecycleOnResume() {
        super.onResume()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun lifecycleOnStart() {
        super.onStart()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun lifecycleOnStop() {
        super.onStop()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun lifecycleOnPause() {
        super.onPause()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun lifecycleOnDestroy() {
        super.onDestroy()
    }

}