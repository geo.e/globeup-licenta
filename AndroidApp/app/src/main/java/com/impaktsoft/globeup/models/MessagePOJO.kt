package com.impaktsoft.globeup.models

import com.google.firebase.firestore.ServerTimestamp
import com.impaktsoft.globeup.enums.MessageState
import com.impaktsoft.globeup.enums.MessageType
import java.util.*

class MessagePOJO : IMessagePOJO {
    var messageKey: String? = ""
    var type: MessageType? = null
    var jsonMessage: String = ""

    @ServerTimestamp
    var creationTimeUTC: Date? = null

    var serverCreationTimeUTC: Date? = null
    var deletionTimeUTC: Long = 0
    var senderKey: String = ""
    var state: MessageState = MessageState.NOT_SEND

    constructor(messageKey: String, type: MessageType, jsonObject: String, sender: String) {
        this.messageKey = messageKey
        this.type = type
        this.jsonMessage = jsonObject
        this.senderKey = sender
        this.serverCreationTimeUTC = Date(System.currentTimeMillis())
        state = MessageState.NOT_SEND
    }

    constructor()
}