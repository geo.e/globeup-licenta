package com.impaktsoft.globeup.models

class EventSectionPOJO : IEventsItem {

    override var name: String? = null

    constructor(sectionName: String?){
        name = sectionName
    }

}