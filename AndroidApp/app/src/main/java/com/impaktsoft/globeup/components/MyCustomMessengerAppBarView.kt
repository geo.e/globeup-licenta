package com.impaktsoft.globeup.components

import android.content.Context
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.listeners.IClickListener
import com.squareup.picasso.Picasso

class MyCustomMessengerAppBarView(context: Context) : FrameLayout(context) {

    private var nameTextView: TextView? = null
    private var statusTextView: TextView? = null
    private var backImageActionBar: ImageView? = null
    private var profileImage: ImageView? = null
    private var statusImage: ImageView? = null

    init {
        addView(inflate(context, R.layout.custom_messenger_app_bar, null))
        nameTextView = findViewById(R.id.messenger_name_action_bar)
        statusTextView = findViewById(R.id.messenger_status_action_bar)
        backImageActionBar = findViewById(R.id.messenger_back_arrow)
        profileImage=findViewById(R.id.messenger_profile_image)
        statusImage=findViewById(R.id.messenger_status_image)
    }

    fun setName(name:String)
    {
        nameTextView?.text=name
    }
    fun setStatus(status:String)
    {
        statusTextView?.text=status
    }
    fun setProfileImage(imageUrl:String)
    {
        Picasso.get().load(imageUrl).into(profileImage)
    }
    fun setProfileImage(image:Int)
    {
        profileImage!!.setImageResource(image)
    }

    fun setBackClick(clickMethod: IClickListener)
    {
        backImageActionBar?.setOnClickListener {
            clickMethod.clicked()
        }
    }
}