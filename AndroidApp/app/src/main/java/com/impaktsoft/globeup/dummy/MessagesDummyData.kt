package com.impaktsoft.globeup.dummy

import com.impaktsoft.globeup.models.MessageImagePOJO
import com.impaktsoft.globeup.models.MessageLocationPOJO
import com.impaktsoft.globeup.models.MessageTextPOJO

class MessagesDummyData
{
    companion object
    {
        val messages= listOf(MessageTextPOJO("Salut"),
            MessageTextPOJO("Ce mai faci"),
            MessageTextPOJO("Renato ii cel mai tare"),
            MessageTextPOJO("Geo fraier")
        )


        val locationMsg= listOf(MessageLocationPOJO("https://maps.googleapis.com/maps/api/staticmap?markers=45.524616,22.3756153&zoom=18&size=1000x1000&key=AIzaSyAegfW4fJNu8j4r3z41leu5uGtEVUe_N4c",45.524616,22.3756153))


    }
}