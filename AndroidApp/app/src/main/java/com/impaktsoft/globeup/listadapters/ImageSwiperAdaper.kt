package com.impaktsoft.globeup.listadapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.impaktsoft.globeup.R
import kotlinx.android.synthetic.main.custom_swipe_image_cell.view.*

class ImageSwiperAdaper(var context: Context,var images: ArrayList<Int>?) : PagerAdapter() {

    private lateinit var layoutInflater: LayoutInflater

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
        return images?.size!!
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var view = layoutInflater.inflate(R.layout.custom_swipe_image_cell, null)

        var imageView = view.swipeImage;
        imageView.setImageResource(images?.get(position)!!)

        var viewPager: ViewPager = container as ViewPager
        viewPager.addView(view, 0);
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        var viewPager: ViewPager = container as ViewPager
        var view: View = `object` as View
        viewPager.removeView(view)
    }



}