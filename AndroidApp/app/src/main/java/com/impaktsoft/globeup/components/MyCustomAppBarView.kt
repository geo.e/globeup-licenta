package com.impaktsoft.globeup.components

import android.content.Context
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import com.impaktsoft.globeup.R

class MyCustomAppBarView(
    context: Context,
    titleResourceId: Int? = null,
    subtitleResourceId: Int? = null
) : FrameLayout(context) {


    private var backImageActionBar: ImageView? = null
    private var titleActionBar: TextView? = null
    private var subTitleActionBar: TextView? = null


    init {
        addView(inflate(context, R.layout.custom_back_action_bar, null))
        backImageActionBar = findViewById(R.id.back_arrow)
        titleActionBar = findViewById(R.id.title_action_bar)
        subTitleActionBar = findViewById(R.id.subtitle_action_bar)

        if (titleResourceId != null) {
            setTitle(context.getString(titleResourceId))
        }

        if (subtitleResourceId != null) {
            setSubTitle(context.getString(subtitleResourceId))
        }
    }

    fun setTitle(title: String) {
        titleActionBar?.text = title
    }

    fun setSubTitle(subtitle: String)
    {
        if (!subtitle.isEmpty()) {
            subTitleActionBar?.visibility = View.VISIBLE
            subTitleActionBar?.text = subtitle
        }
    }

    fun setBackVisibility(visibility: Int) {
        backImageActionBar!!.visibility = visibility
    }

    fun getBackImage(): ImageView? {
        return backImageActionBar;
    }
}