package com.impaktsoft.globeup.services.database

import android.util.Log
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.*
import com.impaktsoft.globeup.models.IEventsItem
import com.impaktsoft.globeup.models.UserEventPOJO
import com.impaktsoft.globeup.models.UserInfo
import kotlinx.coroutines.tasks.await
import java.lang.Exception

interface IUserInfoDAO {
    suspend fun store(backendUserId: String, userInfo: UserInfo)
    suspend fun fetch(backedUserId: String?): UserInfo?
    suspend fun delete(backendUserId: String)
}

class UserInfoDAO(
    private val fireStore: FirebaseFirestore
) : IUserInfoDAO {

    private val userInfoCollection: String = "UserInfo"
    private val defaultValue = "--"

    override suspend fun store(backendUserId: String, userInfo: UserInfo) {
        fireStore.collection(userInfoCollection)
            .document(backendUserId)
            .set(userInfo)
            .addOnFailureListener { throw it }
            .await()
    }

    override suspend fun fetch(backendUserId: String?): UserInfo? {
        var userInfo: UserInfo? = null
        if (backendUserId == null) return null
        val documentSnapshot =
            fireStore.collection(userInfoCollection).document(backendUserId).get().await()

        if (documentSnapshot != null) {
            try {
                userInfo = documentSnapshot.toObject(UserInfo::class.java)
            } catch (e: Exception) {
                Log.d("Error", e.message.toString())
            }
        }

        return userInfo
    }

    override suspend fun delete(backendUserId: String) {
        val deleteUser =
            UserInfo(defaultValue, "Deleted user", defaultValue, defaultValue)
        fireStore.collection(userInfoCollection).document(backendUserId).set(deleteUser)
            .addOnSuccessListener {
                Log.d("TAG","Success delete user")
            }
            .addOnFailureListener {
                throw it
            }
            .await()
    }
}
