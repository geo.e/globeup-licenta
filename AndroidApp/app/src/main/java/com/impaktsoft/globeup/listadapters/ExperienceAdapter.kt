package com.impaktsoft.globeup.listadapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.impaktsoft.globeup.models.ExperiencePOJO
import com.impaktsoft.globeup.databinding.ExperienceCellLayoutBinding
import com.impaktsoft.globeup.databinding.ExperienceCellBinding


class ExperienceAdapter(
    private val arrayList: ArrayList<ArrayList<ExperiencePOJO>>, private val context: Context

) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val applicationBinding = ExperienceCellLayoutBinding.inflate(layoutInflater, parent, false)
        return MyViewHolder(applicationBinding)
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = arrayList[position]
        (holder as ExperienceAdapter.MyViewHolder).bind(item)
    }

    inner class MyViewHolder(var applicationBinding: ExperienceCellLayoutBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {
        fun bind(item: ArrayList<ExperiencePOJO>) {
            for (i in 0..item.size - 1) {

                // applicationBinding.experienceCellParent.addView(inflatedLayout(item[i]))
                addViewToViewGroup(applicationBinding.experienceCellParent, item[i])
            }
        }
    }

    //TODO Renato,am modificat size la celula si nu am trecut in dimensions,ii vorba de inaltimea la experienceText
    fun addViewToViewGroup(viewGroup: ViewGroup, item: ExperiencePOJO) {
        val inflater = LayoutInflater.from(viewGroup.context)
        val binding = ExperienceCellBinding.inflate(inflater, viewGroup, true)
        binding.obj = item
    }
}