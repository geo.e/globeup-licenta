package com.impaktsoft.globeup.viewmodels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.models.CloseViewModelPOJO
import com.impaktsoft.globeup.util.SharedPreferencesKeys
import com.impaktsoft.globeup.views.activities.RegisterActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception

class DeleteAccountViewModel : BaseViewModel() {

    var password = MutableLiveData<String>()
    var passwordRepeat = MutableLiveData<String>()
    var isConfirmViewVisible = MutableLiveData<Boolean>()
    var isLoadingViewVisible = MutableLiveData<Boolean>()


    fun keepAccountClicked() {
        dialogService.hideDialog()

    }

    fun deleteAccountClicked() {
        isConfirmViewVisible.value = true
    }

    fun confirmDeleteAccountClicked() {
        if (passwordsCoincides()) {
            val email = database.fireAuth.currentUser!!.email
            val userKey = database.fireAuth.currentUser!!.uid

            isLoadingViewVisible.value = true

            viewModelScope.launch(Dispatchers.IO) {
                    database.fireAuth.signInWithEmailAndPassword(email!!, password.value!!)
                        .continueWith {
                            if (it.isSuccessful) {

                                facebookService.logoutIfPossible()
                                database.fireAuth.currentUser!!.delete()
                                    .addOnSuccessListener {
                                       viewModelScope.launch(Dispatchers.IO) {

                                           sharedPreferencesService.writeInSharedPref(
                                               SharedPreferencesKeys.didUserLogOutKey,
                                               true
                                           )

                                           database.userInfoDao.delete(userKey)
                                           database.chatDAO.removeUserAllConversations(userKey)
                                           database.eventDao.removeAllUserEvents(userKey)

                                           navigation.closeCurrentActivity()
                                           dataExchangeService.put(SettingsViewModel::class.qualifiedName!!,CloseViewModelPOJO())
                                           dataExchangeService.put(DashboardViewModel::class.qualifiedName!!,CloseViewModelPOJO())

                                           navigation.navigateToActivity(RegisterActivity::class.java)

                                       }
                                    }
                                    .addOnFailureListener {
                                        Log.d("TAGG",it.message)
                                        throw it
                                    }


                            } else {
                                dialogService.showSnackbar(it.result.toString())
                            }
                        }
            }
        }
    }

    private fun passwordsCoincides(): Boolean {
        if (password.value.isNullOrEmpty() || passwordRepeat.value.isNullOrEmpty()) {
            dialogService.showSnackbar("Please add the password")
            return false
        }
        if (!password.value.equals(passwordRepeat.value)) {
            dialogService.showSnackbar("Passwords does not coincides")
            return false
        }
        return true
    }

}