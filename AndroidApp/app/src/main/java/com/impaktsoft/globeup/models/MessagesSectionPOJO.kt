package com.impaktsoft.globeup.models

class MessagesSectionPOJO {
    var header:MessageTimeSection?=null
    var messagesList=ArrayList<IMessagePOJO>()

    constructor(header: MessageTimeSection?) {
        this.header = header
    }
}