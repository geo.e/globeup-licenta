package com.impaktsoft.globeup.viewmodels

import com.google.gson.Gson
import com.impaktsoft.globeup.dummy.FilterDummyData
import com.impaktsoft.globeup.enums.EventTimeFilterEnum
import com.impaktsoft.globeup.enums.EventType
import com.impaktsoft.globeup.listadapters.FilterEventsAdapter
import com.impaktsoft.globeup.models.EventTimeFilterPOJO
import com.impaktsoft.globeup.models.EventTypeFilterPOJO
import com.impaktsoft.globeup.models.IFilterItem

@Suppress("UNCHECKED_CAST")
class FilterEventsViewModel : BaseViewModel() {

    var typeFiltersAdapter: FilterEventsAdapter? = null
    var timeFiltersAdapter: FilterEventsAdapter? = null

    var eventTypeFilters=FilterDummyData.eventTypeFilters
    var eventTimeFilters=FilterDummyData.eventTimeFilters

    init {
        typeFiltersAdapter = FilterEventsAdapter(eventTypeFilters as List<EventTypeFilterPOJO>, this)
        timeFiltersAdapter = FilterEventsAdapter(eventTimeFilters as List<EventTypeFilterPOJO>, this)

    }

    fun checkCurrentButton(position:Int, list:List<IFilterItem>, adapter: FilterEventsAdapter)
    {
        for(i in list.indices)
        {
            if(i==position&&!list[i].checked!!)
            {
                list[i].checked=true
                adapter.notifyItemChanged(i)

            }
            else if(i!=position&&list[i].checked!!)
            {
                list[i].checked=false
                adapter.notifyItemChanged(i)
            }
        }

    }

    private fun getCheckedItems(): ArrayList<IFilterItem> {
        val listOfCheckedItems=ArrayList<IFilterItem>()

        for(item in eventTypeFilters)
        {
            if(item.checked!!&&(item as EventTypeFilterPOJO).eventType!=EventType.Unknown) listOfCheckedItems.add(item)
        }

        for(item in eventTimeFilters)
        {
            if(item.checked!!&&(item as EventTimeFilterPOJO).eventTime!=EventTimeFilterEnum.ALL)listOfCheckedItems.add(item)
        }

        return listOfCheckedItems
    }

    fun close()
    {
        navigation.closeCurrentActivity()
    }

    fun doneClick()
    {
        dataExchangeService.put(EventsViewModel::class.qualifiedName!!,getCheckedItems())
        navigation.closeCurrentActivity()
    }

}