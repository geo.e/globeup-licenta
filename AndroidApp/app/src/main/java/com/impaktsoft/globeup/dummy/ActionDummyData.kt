package com.impaktsoft.globeup.dummy

import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.enums.ActionType
import com.impaktsoft.globeup.models.ActionPOJO

class ActionDummyData {
    companion object
    {
        var actionList= listOf<ActionPOJO>(
            ActionPOJO("History", ActionType.HISTORY),
            ActionPOJO("Invite friends",ActionType.INVITE_FRIENDS)
        )
    }
}