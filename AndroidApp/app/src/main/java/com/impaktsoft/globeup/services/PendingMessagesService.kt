package com.impaktsoft.globeup.services

import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.util.Log
import androidx.annotation.RequiresApi
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.QuerySnapshot
import com.google.gson.Gson
import com.impaktsoft.globeup.enums.MessageType
import com.impaktsoft.globeup.listeners.IGetLocationListener
import com.impaktsoft.globeup.models.MessageImagePOJO
import com.impaktsoft.globeup.models.MessageLocationPOJO
import com.impaktsoft.globeup.models.MessagePOJO
import com.impaktsoft.globeup.models.PendingMessagePOJO
import com.impaktsoft.globeup.services.database.IDatabaseService
import com.impaktsoft.globeup.util.GoogleUtils
import com.impaktsoft.globeup.util.ImageUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import java.io.ByteArrayOutputStream
import java.io.File


interface IPendingMessagesService {
    suspend fun addMessageInPendingList(message: MessagePOJO, conversationKey: String)
    suspend fun removeMessageFromPendingList(message: MessagePOJO, conversationKey: String)
    suspend fun resendPendingMessages(userKey: String)
    suspend fun getPendingMessages(userKey: String, conversationKey: String): ArrayList<MessagePOJO>
}

class PendingMessagesService(
    private val databaseService: IDatabaseService,
    private val locationService: ILocationService,
    private val activityService: ICurrentActivityService,
    private val fileStoreService: IFileStoreService
) :
    IPendingMessagesService {

    private val conversationCollection = "Conversations"
    private val pendingListCollection = "PendingList"
    private val senderKeyPath = "senderKey"
    private val fireStore = databaseService.getFirestore()
    private var currentLocation: LatLng? = null
    private var sendPendingTask: Task<QuerySnapshot>?=null

    override suspend fun addMessageInPendingList(message: MessagePOJO, conversationKey: String) {

        fireStore.collection(conversationCollection)
            .document(conversationKey)
            .collection(pendingListCollection)
            .document(message.messageKey!!)
            .set(PendingMessagePOJO(conversationKey, message, message.senderKey))
            .addOnSuccessListener { Log.d("pulamea", "add in pending") }
            .addOnFailureListener { Log.d("pulamea", "fail add pending") }
            .await()
    }

    override suspend fun removeMessageFromPendingList(
        message: MessagePOJO,
        conversationKey: String
    ) {
        fireStore.collection(conversationCollection)
            .document(conversationKey)
            .collection(pendingListCollection)
            .document(message.messageKey!!)
            .delete()
            .addOnFailureListener { Log.d("pulamea", "remove pending fail") }
            .addOnSuccessListener { Log.d("pulamea", "remove pending succes") }
            .addOnCompleteListener { Log.d("pulamea", "remove pending complete") }
            .await()
    }

    @RequiresApi(Build.VERSION_CODES.P)
    override suspend fun resendPendingMessages(userKey: String) {

        if (locationService.isRunning()) return
        if((sendPendingTask!=null)&&!sendPendingTask?.isComplete!!) return
        val userConversations = databaseService.chatDAO.fetchUserConversations(userKey)

        for (conversation in userConversations!!) {
            sendPendingTask=fireStore.collection(conversationCollection)
                .document(conversation.conversationKey!!)
                .collection(pendingListCollection)
                .whereEqualTo(senderKeyPath, userKey)
                .get()
                .addOnSuccessListener {
                    for (queryDocumentSnapshot in it) {
                        val pendingMessagePOJO =
                            queryDocumentSnapshot.toObject(PendingMessagePOJO::class.java)

                        if (pendingMessagePOJO.messagePOJO!!.type == MessageType.LocationMessage) {
                            resendLocationMessage(
                                pendingMessagePOJO.messagePOJO!!,
                                pendingMessagePOJO.conversationKey!!
                            )
                        }
                        if (pendingMessagePOJO.messagePOJO!!.type == MessageType.ImageMessage) {
                            resendImageMessage(
                                pendingMessagePOJO.messagePOJO!!,
                                pendingMessagePOJO.conversationKey!!
                            )
                        }
                    }
                }
        }
    }

    override suspend fun getPendingMessages(
        userKey: String,
        conversationKey: String
    ): ArrayList<MessagePOJO> {
        val pendingMessages = ArrayList<MessagePOJO>()
        fireStore.collection(conversationCollection)
            .document(conversationKey)
            .collection(pendingListCollection)
            .whereEqualTo(senderKeyPath, userKey)
            .get()
            .addOnSuccessListener {
                for (queryDocumentSnapshot in it) {
                    val pendingMessagePOJO =
                        queryDocumentSnapshot.toObject(PendingMessagePOJO::class.java)
                    pendingMessages.add(pendingMessagePOJO.messagePOJO!!)
                }

            }.await()

        return pendingMessages
    }

    private fun resendLocationMessage(message: MessagePOJO, conversationKey: String) {
        if (currentLocation == null) {
            locationService.getLocation(object : IGetLocationListener {
                override fun getLocation(coords: LatLng?) {
                    val messageLocation =
                        Gson().fromJson(message.jsonMessage, MessageLocationPOJO::class.java)
                    messageLocation.imageUrl = GoogleUtils.getMapImageURL(coords!!, 200, 200)
                    messageLocation.longitude = coords.longitude
                    messageLocation.latitude = coords.latitude
                    val messageJson = Gson().toJson(messageLocation)
                    message.jsonMessage = messageJson

                    GlobalScope.launch(Dispatchers.IO) {
                        databaseService.chatDAO.sendMessage(
                            message,
                            conversationKey, null
                        )
                        removeMessageFromPendingList(message, conversationKey)
                    }
                }
            })
        }
    }

    @RequiresApi(Build.VERSION_CODES.P)
    private fun resendImageMessage(message: MessagePOJO, conversationKey: String) {
        val messageImage = Gson().fromJson(message.jsonMessage, MessageImagePOJO::class.java)
        var bitmap: Bitmap? = null
        val file = File(messageImage.imageUri)
        if (file.exists()) {

            bitmap = ImageUtils.getBitmap(activityService.activity!!, Uri.fromFile(file))
            Log.d("pulamea bitmap", (bitmap == null).toString())
        }
        //TODO else


        GlobalScope.launch(Dispatchers.IO) {

            val baos = ByteArrayOutputStream()
            bitmap?.compress(Bitmap.CompressFormat.JPEG, 100, baos)
            val imageByteArray = baos.toByteArray()

            messageImage.imageUrl =
                fileStoreService.uploadConversationMessageImage(conversationKey, imageByteArray)
            val messageJson = Gson().toJson(messageImage)
            message.jsonMessage = messageJson
            message.creationTimeUTC = null

            databaseService.chatDAO.sendMessage(
                message,
                conversationKey, null
            )
            removeMessageFromPendingList(message, conversationKey)
        }

    }
}