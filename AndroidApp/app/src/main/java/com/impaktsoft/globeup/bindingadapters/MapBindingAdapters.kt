package com.impaktsoft.globeup.bindingadapters

import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import com.google.android.gms.maps.model.LatLng
import com.impaktsoft.globeup.components.MapViewWithPin
import com.impaktsoft.globeup.listeners.IGetLocationListener

object MapBindingAdapters {

    @BindingAdapter(value = ["cb_getPositionAttrChanged"])
    @JvmStatic
    fun MapViewWithPin.setListener(listener: InverseBindingListener?) {
        if (listener != null) {
            this.getLocationListener = (
                    object : IGetLocationListener {
                        override fun getLocation(coords: LatLng?) {
                            mapPosition=coords
                            listener.onChange()
                        }
                    })
        }
    }

    @BindingAdapter("cb_getPosition")
    @JvmStatic
    fun MapViewWithPin.setMyPosition(position: LatLng?) {
        if (position != null) {
            this.mapPosition = position
        }
    }

    @InverseBindingAdapter(attribute = "cb_getPosition")
    @JvmStatic
    fun MapViewWithPin.getMyPosition(): LatLng? {
        return this.mapPosition

    }
}