package com.impaktsoft.globeup.services.database

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.enums.MessageType
import com.impaktsoft.globeup.models.ConversationPOJO
import com.impaktsoft.globeup.models.MessagePOJO
import com.impaktsoft.globeup.models.MessageTextPOJO
import com.impaktsoft.globeup.util.KeyHelper
import com.impaktsoft.globeup.views.activities.MessengerActivity


class MyFirebaseMessagingService : FirebaseMessagingService() {

    private val ADMIN_CHANNEL_ID = "admin_channel"

    override fun onMessageReceived(remoteMessage: RemoteMessage) {

        if (remoteMessage.data.isNotEmpty()) {

            val notificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                setupChannels(notificationManager)
            }


            val gson = GsonBuilder().setDateFormat("MMM d, yyyy HH:mm:ss").create()
            val message =
                gson.fromJson(remoteMessage.data[KeyHelper.messagePOJOKey], MessagePOJO::class.java)
            val conversation = gson.fromJson(
                remoteMessage.data[KeyHelper.conversationPOJOKey],
                ConversationPOJO::class.java
            )

            val notificationID = conversation?.key.hashCode()

            var messageText = ""
            messageText = when (message?.type) {
                MessageType.ImageMessage -> {
                    resources.getString(R.string.image_key)
                }
                MessageType.LocationMessage -> {
                    resources.getString(R.string.location_key)
                }
                else -> Gson().fromJson(message?.jsonMessage, MessageTextPOJO::class.java).text
            }

            val notificationSoundUri =
                RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val intent = Intent(this, MessengerActivity::class.java)
            intent.putExtra(KeyHelper.conversationExtrasKey, Gson().toJson(conversation))
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            val pendingIntent = PendingIntent.getActivity(
                this, System.currentTimeMillis().toInt(), intent,
                PendingIntent.FLAG_ONE_SHOT
            )

            val notificationBuilder = NotificationCompat.Builder(this, ADMIN_CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentTitle(conversation?.name)
                .setContentText(messageText)
                .setAutoCancel(true)
                .setSound(notificationSoundUri)
                .setContentIntent(pendingIntent)

            notificationManager.notify(notificationID, notificationBuilder.build())
        }
        if (remoteMessage.notification != null) {
            Log.d(
                "TAG",
                "Message Notification Body: " + remoteMessage.notification!!.body
            )
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private fun setupChannels(notificationManager: NotificationManager?) {
        val adminChannelName = "New notification"
        val adminChannelDescription = "Device to device notification"

        val adminChannel = NotificationChannel(
            ADMIN_CHANNEL_ID,
            adminChannelName,
            NotificationManager.IMPORTANCE_HIGH
        )
        adminChannel.description = adminChannelDescription
        adminChannel.enableLights(true)
        adminChannel.lightColor = Color.RED
        adminChannel.enableVibration(true)
        notificationManager?.createNotificationChannel(adminChannel)
    }
}