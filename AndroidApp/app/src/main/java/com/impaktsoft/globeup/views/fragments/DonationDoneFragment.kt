package com.impaktsoft.globeup.views.fragments

import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.databinding.FragmentDonationDoneBinding
import com.impaktsoft.globeup.viewmodels.DonationDoneViewModel


class DonationDoneFragment : BaseBoundFragment<DonationDoneViewModel, FragmentDonationDoneBinding>(
    DonationDoneViewModel::class
) {

    override var layoutId: Int = R.layout.fragment_donation_done
    override val fragmentNameResourceID: Int = R.string.donation_done_name

    override fun setupDataBinding(binding: FragmentDonationDoneBinding) {
        binding.viewModel = mViewModel
    }
}
