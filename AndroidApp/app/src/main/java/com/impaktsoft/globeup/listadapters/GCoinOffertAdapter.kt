package com.impaktsoft.globeup.listadapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.impaktsoft.globeup.databinding.GcoinOffertCellBinding
import com.impaktsoft.globeup.models.GCoinPOJO
import com.impaktsoft.globeup.viewmodels.BuyGCoingViewModel

class GCoinOffertAdapter(
    private val viewModel: BuyGCoingViewModel,
    private val arrayList: List<GCoinPOJO>
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)

        val applicationBinding =
            GcoinOffertCellBinding.inflate(layoutInflater, parent, false)
        return GcoinsViewHolder(applicationBinding)

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val contactPOJO = arrayList[position]
        (holder as GcoinsViewHolder).bind(contactPOJO)
    }

    inner class GcoinsViewHolder(var applicationBinding: GcoinOffertCellBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(item: GCoinPOJO) {

            applicationBinding.itemGcoin = item
            applicationBinding.gcoinLayout.setOnClickListener {
                viewModel.selectOffert(adapterPosition)
            }

        }
    }
}