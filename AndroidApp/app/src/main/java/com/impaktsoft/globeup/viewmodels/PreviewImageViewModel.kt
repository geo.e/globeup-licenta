package com.impaktsoft.globeup.viewmodels

import android.os.Message
import android.util.Log
import com.impaktsoft.globeup.models.MessagePOJO

class PreviewImageViewModel:BaseViewModel() {

    var messageItem:MessagePOJO?=null
    init {
        messageItem=dataExchangeService.get<MessagePOJO>(PreviewImageViewModel::class.qualifiedName!!)
    }
}