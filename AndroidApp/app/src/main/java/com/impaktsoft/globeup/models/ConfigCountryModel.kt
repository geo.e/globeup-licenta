package com.impaktsoft.globeup.models

class ConfigCountryModel {
    var buttonName: String? = null
    var blockActionIfCountryNotSelected: Boolean? = null
    var action: (() -> Unit)? = null

    constructor(
        buttonName: String?,
        blockActionIfCountryNotSelected: Boolean?,
        action: (() -> Unit)?=null
    ) {
        this.buttonName = buttonName
        this.blockActionIfCountryNotSelected = blockActionIfCountryNotSelected
        this.action=action
    }
}