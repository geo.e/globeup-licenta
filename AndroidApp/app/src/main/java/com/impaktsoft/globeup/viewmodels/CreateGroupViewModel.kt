package com.impaktsoft.globeup.viewmodels

import android.util.Log
import com.impaktsoft.globeup.dummy.ContactsDummyData
import com.impaktsoft.globeup.listadapters.SelectedUsersAdapter
import com.impaktsoft.globeup.listeners.IGetUserListener
import com.impaktsoft.globeup.models.UserPOJO
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class CreateGroupViewModel :BaseViewModel()
{

    private var adapter:SelectedUsersAdapter?=null
    private var contactsList=ArrayList<UserPOJO>()

    init {
        contactsList.addAll(ContactsDummyData.contactsList)
        adapter= SelectedUsersAdapter(contactsList,object:IGetUserListener{
            override fun getUser(user: UserPOJO) {
                unselectUser(user)
            }

        })
    }

    fun getAdapter():SelectedUsersAdapter?
    {
        return adapter
    }

    fun unselectUser(user:UserPOJO)
    {
        contactsList.remove(user)
        adapter!!.notifyDataSetChanged()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun filterList(string: String) {
        val auxList = java.util.ArrayList<UserPOJO>()
        for (item in contactsList) {
            val name = item.name

            var stringWithUpperLetter = ""

            if (!string.isNullOrEmpty()) {
                stringWithUpperLetter =
                    string.substring(0, 1).toUpperCase() + string.substring(1)
            }
            if (name.contains(string) || name.contains(stringWithUpperLetter) ||
                name.contains(string.toUpperCase()) || string.isEmpty()
            ) {
                auxList.add(item)
            }
        }
        adapter!!.setList(auxList)
    }

    override fun onCreate() {
        super.onCreate()
        EventBus.getDefault().register(this);
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }
}