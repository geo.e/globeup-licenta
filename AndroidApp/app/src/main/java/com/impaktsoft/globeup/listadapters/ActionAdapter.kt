package com.impaktsoft.globeup.listadapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.impaktsoft.globeup.databinding.FloatingButtonOptionCellBinding
import com.impaktsoft.globeup.models.ActionPOJO
import com.impaktsoft.globeup.viewmodels.DashboardViewModel

class ActionAdapter (private val list: List<ActionPOJO>,private val viewModel:DashboardViewModel
) :
RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)

        val applicationBinding =
            FloatingButtonOptionCellBinding.inflate(layoutInflater, parent, false)
        return ActionViewHolder(applicationBinding)

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val userPOJO = list[position]
        (holder as ActionViewHolder).bind(userPOJO)
    }

    inner class ActionViewHolder(var applicationBinding: FloatingButtonOptionCellBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(item: ActionPOJO) {

            applicationBinding.optionItem = item
            applicationBinding.viewModel=viewModel
        }
    }
}