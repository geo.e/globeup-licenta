package com.impaktsoft.globeup.viewmodels

import androidx.lifecycle.MutableLiveData
import com.impaktsoft.globeup.dummy.GCoinsOffertDummyData
import com.impaktsoft.globeup.listadapters.GCoinOffertAdapter
import com.impaktsoft.globeup.models.GCoinPOJO

class BuyGCoingViewModel :BaseViewModel()
{
    var currentGcoins="1.2345Gcoins"

    private var gcoinValue=0.01
    var gcoinValueString:String

    private var total_value:Double=0.0
    var total_value_string= MutableLiveData<String>()

    private var selectedOffert:GCoinPOJO?=null

    var myListOfOfferts=GCoinsOffertDummyData.listOfOfferts

    var adapter=GCoinOffertAdapter(this,myListOfOfferts)

    init {
        gcoinValueString="$ "+gcoinValue.toString()+"per Gcoin"
        total_value_string.value= "$ $total_value"
    }

    fun selectOffert(position: Int)
    {
        for(i in 0 until myListOfOfferts.size)
        {
            if(i==position&&!myListOfOfferts[i].selected)
            {
                myListOfOfferts[i].selected=true
                selectedOffert=myListOfOfferts[i]
                setTotalPrice()
                adapter!!.notifyItemChanged(i)

            }
            else if(i!=position&& myListOfOfferts[i].selected)
            {
                myListOfOfferts[i].selected=false
                adapter!!.notifyItemChanged(i)
            }
        }
    }

    fun setTotalPrice()
    {
        total_value= selectedOffert!!.value!! * gcoinValue
        total_value_string.value= "$ $total_value"

    }
}