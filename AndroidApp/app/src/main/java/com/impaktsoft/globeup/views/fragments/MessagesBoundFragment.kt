package com.impaktsoft.globeup.views.fragments

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.components.MyCustomSearchAppBarView
import com.impaktsoft.globeup.databinding.FragmentMessagesBinding
import com.impaktsoft.globeup.listeners.IGetStringListener
import com.impaktsoft.globeup.viewmodels.MessagesBoundViewModel
import kotlinx.android.synthetic.main.fragment_messages.*


class MessagesBoundFragment :
    BaseBoundFragment<MessagesBoundViewModel, FragmentMessagesBinding>(MessagesBoundViewModel::class) {

    override var layoutId: Int = R.layout.fragment_messages
    override val fragmentNameResourceID: Int = R.string.message_bound_name
    private var myCustomSearchAppBarView:MyCustomSearchAppBarView?=null

    override fun setupDataBinding(binding: FragmentMessagesBinding) {
        binding.viewModel = mViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true);
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        messages_list.layoutManager = layoutManager
        messages_list.adapter = mViewModel.getAdapter()

        val lineSeparator = DividerItemDecoration(
            messages_list.context,
            layoutManager.orientation
        )

        messages_list.addItemDecoration(lineSeparator)

    }

    override fun onResume() {
        super.onResume()

        setMyActionBar()

    }

    private fun setMyActionBar()
    {
        (activity as AppCompatActivity).supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowCustomEnabled(true)

        myCustomSearchAppBarView = MyCustomSearchAppBarView(context!!,object :IGetStringListener
        {
            override fun getCharSequence(string: String) {
               mViewModel.sortList(string)
            }
        })

        (activity as AppCompatActivity).supportActionBar?.customView = myCustomSearchAppBarView

        if(myCustomSearchAppBarView!=null) {
            myCustomSearchAppBarView?.setTitle(getString(fragmentNameResourceID))
            myCustomSearchAppBarView?.setBackImageVisibility(View.GONE)
        }
    }
}