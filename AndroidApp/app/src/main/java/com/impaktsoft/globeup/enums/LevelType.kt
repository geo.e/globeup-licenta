package com.impaktsoft.globeup.enums

enum class LevelType
{
    GREENHORN,
    BRONZE1,BRONZE2,BRONZE3,
    SILVER1,SILVER2,SILVER3,
    GOLD1,GOLD2,GOLD3
}