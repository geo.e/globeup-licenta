package com.impaktsoft.globeup.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.firestore.GeoPoint
import com.google.gson.Gson
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.enums.EventState
import com.impaktsoft.globeup.enums.EventType
import com.impaktsoft.globeup.enums.MessageType
import com.impaktsoft.globeup.models.*
import com.impaktsoft.globeup.util.DateHelper.Companion.getOffsetTimeInMillis
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*
import kotlin.collections.ArrayList


class AddEventViewModel : BaseViewModel() {

    var currentImageIndex = MutableLiveData(0)
    var eventName = MutableLiveData<String>()
    var eventDescription = MutableLiveData<String>()
    var eventType = MutableLiveData<EventType>(EventType.Unknown)
    var eventLocation = MutableLiveData<LatLng>()
    var images: ArrayList<Int>? = ArrayList()
    var datePickerCalendar = MutableLiveData<Calendar>()
    var timePickerCalendar = MutableLiveData<Calendar>()
    var isAddProcessRunning = MutableLiveData<Boolean>(false)
    var createEventOnFacebook = MutableLiveData<Boolean>()
    private var eventTimeInUTC = Calendar.getInstance()

    init {
        images?.add(R.drawable.collect_garbage)
        images?.add(R.drawable.recycling)
        images?.add(R.drawable.help_people)
        images?.add(R.drawable.plant_tree)

        datePickerCalendar.value = Calendar.getInstance()
        timePickerCalendar.value = Calendar.getInstance()
    }

    var spinnerEntries = listOf(EventType.Unknown,EventType.GarbageCollect, EventType.Recycling, EventType.HelpPeople,EventType.PlantTrees)

    var progress = MutableLiveData(0)


    fun createClick() {
        if (!backend.isSignedInAnonymously() && isAddProcessRunning.value == false) {
            if (eventName.value.isNullOrEmpty()) {
                dialogService.showSnackbar(R.string.err_add_name)
                return
            }
            if (eventDescription.value.isNullOrEmpty()) {
                dialogService.showSnackbar(R.string.err_add_description)
                return
            }
            if (mapService.getCountryCode(
                    eventLocation.value!!.latitude,
                    eventLocation.value!!.longitude
                ) == null
            ) {
                dialogService.showSnackbar(R.string.err_add_valid_location)
                return
            }

            setupEventDate()
            val eventKey = UUID.randomUUID().toString()
            val userKey = backend.currentUser!!.uid
            isAddProcessRunning.value = true

            viewModelScope.launch(Dispatchers.IO) {
                addEventInDB(eventKey, userKey)
                initEventConversation(eventKey, userKey)
                withContext(Dispatchers.Main)
                {
                    isAddProcessRunning.value = false
                    navigation.closeCurrentActivity()
                }
            }
        }
    }

    private fun setupEventDate() {
        val year = datePickerCalendar.value!!.get(Calendar.YEAR)
        val month = datePickerCalendar.value!!.get(Calendar.MONTH)
        val day = datePickerCalendar.value!!.get(Calendar.DAY_OF_MONTH)
        val hour = timePickerCalendar.value!!.get(Calendar.HOUR_OF_DAY)
        val minute = timePickerCalendar.value!!.get(Calendar.MINUTE)

        eventTimeInUTC.set(year, month, day, hour, minute)

    }

    private suspend fun addEventInDB(eventKey: String, userKey: String) {
        val country = mapService.getCountryCode(
            eventLocation.value!!.latitude,
            eventLocation.value!!.longitude
        )

        val currentEvent = EventPOJO(
            images?.get(currentImageIndex.value!!),
            eventName.value,
            eventDescription.value,
            eventType.value,
            eventTimeInUTC.timeInMillis,
            GeoPoint(
                eventLocation.value?.latitude!!,
                eventLocation.value?.longitude!!
            ),
            country
        )

        currentEvent.uid = eventKey

        val localTime = Calendar.getInstance()
        localTime.timeInMillis = eventTimeInUTC.timeInMillis
        val offStetTime = getOffsetTimeInMillis()
        eventTimeInUTC.timeInMillis -= offStetTime

        database.eventDao.storeEvent(eventKey, currentEvent)
        database.eventDao.storeUserEvent(
            userKey,
            UserEventPOJO(EventState.HOSTING, eventKey)
        )
        database.eventDao.joinEvent(eventKey, EventMembersPOJO(userKey))
    }

    private suspend fun initEventConversation(eventKey: String, userKey: String) {
        val conversationKey = UUID.randomUUID().toString()
        database.chatDAO.addUserInConversation(userKey, conversationKey)
        database.chatDAO.addEventConversation(eventKey, conversationKey)
        database.chatDAO.addUserConversation(userKey, conversationKey)

        val welcomeTextMessage =
            MessageTextPOJO(resourceService.stringForId(R.string.welcome_in_group)!!)
        val jsonMsg = Gson().toJson(welcomeTextMessage)
        val messageKey = UUID.randomUUID().toString()
        val message = MessagePOJO(messageKey, MessageType.TextMessage, jsonMsg, userKey)
        database.chatDAO.sendMessage(message, conversationKey, null)

        val conversationName = eventName.value + " " + resourceService.stringForId(R.string.chat)
        val conversationPOJO =
            ConversationPOJO(
                conversationKey, conversationName, message,
                images?.get(currentImageIndex.value!!)!!
            )

        database.chatDAO.storeConversation(conversationPOJO)
    }

}