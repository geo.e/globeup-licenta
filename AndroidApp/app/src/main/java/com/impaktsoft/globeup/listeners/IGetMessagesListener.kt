package com.impaktsoft.globeup.listeners

import com.impaktsoft.globeup.models.MessagePOJO

interface IGetMessagesListener {
    fun getMessage(message:MessagePOJO)
}