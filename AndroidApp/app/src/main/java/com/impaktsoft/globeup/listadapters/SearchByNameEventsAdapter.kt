package com.impaktsoft.globeup.listadapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.util.Pair
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.databinding.EventCellBinding
import com.impaktsoft.globeup.enums.EventState
import com.impaktsoft.globeup.models.EventDetailPOJO
import com.impaktsoft.globeup.models.EventLoadingPOJO
import com.impaktsoft.globeup.models.EventPOJO
import com.impaktsoft.globeup.models.IEventsItem
import com.impaktsoft.globeup.util.MapHelper
import com.impaktsoft.globeup.viewmodels.BaseEventsViewModel
import java.lang.Exception

class SearchByNameEventsAdapter(
    var eventsList: ArrayList<IEventsItem>,
    private val viewModel: BaseEventsViewModel
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val VIEW_TYPE_ITEM = 0
    private val VIEW_TYPE_LOADING = 1

    override fun getItemCount(): Int {
        return eventsList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)

        return if (viewType == VIEW_TYPE_ITEM) {
            val applicationBinding =
                EventCellBinding.inflate(layoutInflater, parent, false)
            EventViewHolder(applicationBinding)
        } else {
            val view: View = layoutInflater
                .inflate(R.layout.event_loading_cell, parent, false)
            LoadingViewHolder(view)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (eventsList.get(position) is EventLoadingPOJO) VIEW_TYPE_LOADING else VIEW_TYPE_ITEM
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val myObj = eventsList[position];

        if (holder is EventViewHolder) {
            (holder).bind(myObj as EventPOJO)
        }

    }

    inner class EventViewHolder(var applicationBinding: EventCellBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(item: EventPOJO) {

            val eventState = viewModel.getEventState(item);
            applicationBinding.eventCellCarview.isClickable =
                !viewModel.isEventModifying(item.uid!!)!!
            applicationBinding.item = item
            applicationBinding.viewModel = viewModel
            applicationBinding.eventState = eventState

            if (item == eventsList.last()) {
                val lastCellBottomMargin = 120;
                val layoutParams: ViewGroup.MarginLayoutParams =
                    applicationBinding.eventCellCarview.layoutParams as ViewGroup.MarginLayoutParams
                layoutParams.setMargins(
                    layoutParams.leftMargin, layoutParams.topMargin,
                    layoutParams.rightMargin, lastCellBottomMargin
                )
                applicationBinding.eventCellCarview.layoutParams = layoutParams
            }

            val pairs: Array<Pair<View, String>>?
            if (eventState == EventState.HOSTING) {
                pairs = arrayOf(
                    Pair<View, String>(
                        applicationBinding.eventCellImage,
                        applicationBinding.eventCellImage.transitionName
                    )
                    , Pair<View, String>(
                        applicationBinding.eventName,
                        applicationBinding.eventName.transitionName
                    )
                )
            } else {
                pairs = arrayOf(
                    Pair<View, String>(
                        applicationBinding.eventCellImage,
                        applicationBinding.eventCellImage.transitionName
                    )
                    , Pair<View, String>(
                        applicationBinding.eventName,
                        applicationBinding.eventName.transitionName
                    )
                    , Pair<View, String>(
                        applicationBinding.upcmingEventStatusLayout,
                        applicationBinding.upcmingEventStatusLayout.transitionName
                    )
                )

            }

            val eventDetailItem = EventDetailPOJO(item, viewModel.myUserEventList, eventState)
            val itemJson = Gson().toJson(eventDetailItem)

            applicationBinding.eventCellCarview.setOnClickListener {
                viewModel.eventClick(pairs, itemJson)
            }
        }
    }

    inner class LoadingViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {

    }

    fun updateEventList(newList: ArrayList<EventPOJO>) {
        eventsList = newList as ArrayList<IEventsItem>
        notifyDataSetChanged()
    }

}