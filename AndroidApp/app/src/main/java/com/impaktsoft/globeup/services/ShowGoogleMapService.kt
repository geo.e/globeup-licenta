package com.impaktsoft.globeup.services

import android.content.Intent
import android.net.Uri
import com.google.android.gms.maps.model.LatLng

interface IShowGoogleMapService {
    fun showMapActivity(position: LatLng)
}

class ShowGoogleMapService(private val activityService: ICurrentActivityService) :
    IShowGoogleMapService {
    override fun showMapActivity(position: LatLng) {
        val uri =
            Uri.parse(("geo:0,0?z=18&q=" + position.latitude.toString() + "," + position.longitude.toString() + "(Place)"));

        val intent = Intent(Intent.ACTION_VIEW, uri);
        intent.setPackage("com.google.android.apps.maps");

        activityService.activity?.startActivity(intent);
    }
}