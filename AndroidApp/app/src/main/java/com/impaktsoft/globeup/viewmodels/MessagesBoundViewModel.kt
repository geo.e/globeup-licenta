package com.impaktsoft.globeup.viewmodels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.impaktsoft.globeup.enums.MessageState
import com.impaktsoft.globeup.listadapters.ConversationsAdapter
import com.impaktsoft.globeup.listeners.IGetConversationListener
import com.impaktsoft.globeup.listeners.IGetLastMessageSeenCount
import com.impaktsoft.globeup.models.ConversationPOJO
import com.impaktsoft.globeup.models.ConversationRefPOJO
import com.impaktsoft.globeup.util.SearchStringUtils
import com.impaktsoft.globeup.views.activities.MessengerActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MessagesBoundViewModel : BaseViewModel() {

    var userConversationsList = ArrayList<ConversationRefPOJO>()
    var isProgressBarVisible = MutableLiveData<Boolean>()
    var isPlaceholderVisible = MutableLiveData<Boolean>()
    var messagesSeenByUser = HashMap<String, Boolean>()

    private var conversationsAdapter: ConversationsAdapter? = null
    private var conversationsList = ArrayList<ConversationPOJO>()
    private var isFetchDone = false

    init {
        conversationsAdapter = ConversationsAdapter(conversationsList, this)
        initConversations()
    }

    fun getAdapter(): ConversationsAdapter? {
        return conversationsAdapter
    }

    fun conversationClick(conversationPOJO: ConversationPOJO) {
        val conversationJson = Gson().toJson(conversationPOJO)
        dataExchangeService.put(MessengerViewModel::class.qualifiedName!!, conversationJson)
        navigation.navigateToActivity(MessengerActivity::class.java, false)
    }

    fun sortList(searchString: String) {
        if(isProgressBarVisible.value==true)return
        if (searchString.isNullOrEmpty()) conversationsAdapter?.filterList(conversationsList)
        else {
            conversationsAdapter!!.filterList(
                SearchStringUtils.searchConversationByName(
                    searchString,
                    conversationsList
                )
            )
        }
    }

    private fun initConversations() {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                fetchConversations()

                if (connectivityService.isInternetAvailable() && !userConversationsList.isNullOrEmpty())
                {
                    initConversationsObserver()
                    initLastMessagesObserver()
                }
                else {
                   withContext(Dispatchers.Main){

                       isProgressBarVisible.value = false
                       if(userConversationsList.isNullOrEmpty())
                       {
                           isPlaceholderVisible.value = true;
                       }
                       else {
                           messagesSeenByUser[conversationsList.last().key] = true
                           conversationsAdapter?.notifyDataSetChanged()
                       }
                   }
                }

            } catch (e: Exception) {
                Log.d("Error", "viewModelScope Error " + e.message)
               //initConversations()
            }
        }
    }

    private suspend fun initConversationsObserver() {

        database.chatDAO.addConversationsObserver(backend.currentUser?.uid!!,
            object : IGetConversationListener {
                override fun getConversation(conversation: ConversationPOJO) {
                    viewModelScope.launch(Dispatchers.IO) {
                        if (isFetchDone) {
                            checkConversationLastMessageState(conversation)
                        }
                    }
                }
            })
    }

    private suspend fun initLastMessagesObserver() {
        conversationsList.forEach {
            database.chatDAO.addLastMessageCountObserver(it!!.key, object :
                IGetLastMessageSeenCount {
                override fun getLastMessageSeenCount(seenCount: Int) {
                    viewModelScope.launch(Dispatchers.IO) {
                        checkConversationLastMessageState(it)
                    }
                }
            })
        }
    }

    private suspend fun checkConversationLastMessageState(conversation: ConversationPOJO)
    {
        val didUserSeenMessage = database.chatDAO.didUserSeenMessage(
            backend.currentUser!!.uid,
            conversation.key
        )
        if (conversation.lastMessage?.senderKey == backend.currentUser!!.uid) {
            messagesSeenByUser[conversation.key] = true
        } else {
            messagesSeenByUser[conversation.key] = didUserSeenMessage
        }
        viewModelScope.launch(Dispatchers.Main) {
            isProgressBarVisible.value = false
            updateConversationList(conversation)
        }
    }

    private fun updateConversationList(conversation: ConversationPOJO) {
        if (!conversationsList.any { it.key == conversation.key }) return
        val conversationToUpdate = conversationsList.find { it.key == conversation.key }
        conversationToUpdate?.lastMessage = conversation.lastMessage
        sortConversationListByTime()
        sortConversationListByLastMessageSeen()
        conversationsAdapter?.notifyDataSetChanged()
    }

    private fun sortConversationListByTime() {
        conversationsList.sortByDescending { it.lastMessage?.serverCreationTimeUTC?.time }
    }

    private fun sortConversationListByLastMessageSeen() {

        for (i in 0 until conversationsList.size) {
            for (j in i + 1 until conversationsList.size) {
                if (messagesSeenByUser[conversationsList[j].key] == false && messagesSeenByUser[conversationsList[i].key] == true) {
                    val aux = conversationsList[i]
                    conversationsList[i] = conversationsList[j]
                    conversationsList[j] = aux
                }
            }
        }
    }

    private suspend fun fetchConversations() {
        conversationsList.clear()
        userConversationsList.clear()
        withContext(Dispatchers.Main)
        {
            isProgressBarVisible.value = true
            isPlaceholderVisible.value = false
        }
        try{
            userConversationsList =
                database.chatDAO.fetchUserConversations(backend.currentUser!!.uid)!!

        }catch (e : Exception)
        {
            throw e
        }

        if(userConversationsList.isNullOrEmpty())
        {
            return
        }

        userConversationsList.forEach {
            val conversation = database.chatDAO.fetchConversation(it.conversationKey!!)
            isFetchDone = true
            if (conversation != null) {
                conversationsList.add(conversation)
            } else {
                isFetchDone = false
                conversationsList.clear()
                delay(1000)
                fetchConversations()
            }
        }
    }
}
