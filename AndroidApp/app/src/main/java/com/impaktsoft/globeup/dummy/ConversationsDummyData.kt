package com.impaktsoft.globeup.dummy

import com.impaktsoft.globeup.enums.MessageState
import com.impaktsoft.globeup.enums.MessageType
import com.impaktsoft.globeup.models.ConversationPOJO
import com.impaktsoft.globeup.models.MessagePOJO

class ConversationsDummyData {

    companion object {
        val u1: List<String> = listOf("Andrei", "Ion", "Geo")
        val u2: List<String> = listOf("Gigi", "Ion", "Geo", "Cipi")
        val u3: List<String> = listOf("Maria", "Ionna", "Pula")
        val u4: List<String> = listOf("Telu", "Nelu")

       /* var imageUrl : String? = "https://firebasestorage.googleapis.com/v0/b/pocfirebaseloginregister.appspot.com/o/UsersPhotos%2F-LnS5Ct1NHLlm3n5E3CJ?alt=media&token=46ad8d18-1427-4a2e-967b-bfb344552e56"
        val listConvs = listOf(
            ConversationPOJO("1234", u1, "Clean the forest", MessagePOJO(MessageType.ImageMessage,"image","Andrei",MessageState.SEEN),
                imageUrl!!),
            ConversationPOJO("1234", u2, "Clean the river", MessagePOJO(MessageType.LocationMessage,"location","Gigi",MessageState.SEEN),
                imageUrl!!),
            ConversationPOJO("1234", u3, "Clean the city", MessagePOJO(MessageType.TextMessage,"Curat","Maria",MessageState.SEEN),
                imageUrl!!),
            ConversationPOJO("1234", u4, "", MessagePOJO(MessageType.TextMessage,"Am curatat tot","Nelu",MessageState.SEEN),
                imageUrl!!)
        )*/

    }
}