package com.impaktsoft.globeup.views.activities

import android.opengl.Visibility
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.impaktsoft.globeup.R
import com.impaktsoft.globeup.components.MyCustomAppBarView
import com.impaktsoft.globeup.components.MyCustomMessengerAppBarView
import com.impaktsoft.globeup.databinding.ActivityPreviewImageBinding
import com.impaktsoft.globeup.generated.callback.OnClickListener
import com.impaktsoft.globeup.listeners.IClickListener
import com.impaktsoft.globeup.viewmodels.PreviewImageViewModel


class PreviewImageActivity :
    BaseBoundActivity<PreviewImageViewModel, ActivityPreviewImageBinding>(PreviewImageViewModel::class) {

    override val layoutId: Int = R.layout.activity_preview_image
    override var activityTitleResourceId: Int? = R.string.preview_image

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupAppBar()
    }

    override fun setupDataBinding(binding: ActivityPreviewImageBinding) {
        binding.viewModel = mViewModel
    }

    private fun setupAppBar() {
        val myAppBar =
            supportActionBar!!.customView as MyCustomAppBarView
        myAppBar.setBackVisibility(View.VISIBLE)
        myAppBar.getBackImage()?.setOnClickListener{
            finish()
        }
    }

}



