package com.impaktsoft.globeup.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.impaktsoft.globeup.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ForgetPasswordViewModel : BaseViewModel() {
    var email = MutableLiveData<String>()
    var isSpinnerVisible = MutableLiveData<Boolean>()

    fun sendPasswordResetRequest() {
        viewModelScope.launch(Dispatchers.IO) {
            if (email.value.isNullOrEmpty()) {
                dialogService.showSnackbar(R.string.err_provide_email_password)
            } else {
                try {

                    viewModelScope.launch(Dispatchers.Main)
                    {
                        isSpinnerVisible.value = true
                    }

                    val result = backend.resetPassword(email.value!!)
                    if (result.isSuccess) {
                        dialogService.showSnackbar(R.string.forget_password_guid)
                        navigation.closeCurrentActivity()
                    } else
                        dialogService.showSnackbar(result.error!!.debugMessage)
                } catch (e: Exception) {
                    dialogService.showSnackbar(e.message.toString())
                }
                viewModelScope.launch(Dispatchers.Main)
                {
                    isSpinnerVisible.value = false
                }
            }
        }

    }
}